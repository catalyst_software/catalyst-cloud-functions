import { firestore } from 'firebase-admin';
import moment from 'moment';
import * as functions from 'firebase-functions'


import { OnboardingValidationRequest } from '../interfaces/onboarding-validation-request';
import { validateOrgCode } from './utils/onboarding/validate-org-code';
import { sanitizeOnboardingCode } from './utils/onboarding/sanitize-onboarding-code';
import { createEntities } from './utils/onboarding/create-entities';
import { saveAthleteProfile } from './saveAthleteProfile';
import { getUserByID } from '../controllers/get-user-by-id';
import { OnboardingValidationResponse } from '../interfaces/onboarding-validation-response';
import { Config } from '../../../../shared/init/config/config';

export const validateOrganizationCode = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            try {
                const { organizationCode: code, athlete, profile }
                    = <OnboardingValidationRequest>data

                const athletUID = context.auth.token.uid
                const user = await getUserByID(athletUID)

                const athleteIndicator = {
                    runningTotalIpScore: 0,
                    // // // TODO: IG - Needed here?
                    firebaseMessagingIds: athlete.firebaseMessagingIds || [context.instanceIdToken || ''],
                    ...athlete,

                    creationTimestamp: firestore.Timestamp.fromDate(moment(user.metadata.creationTime).toDate()),
                }

                console.log('===========>>>>> athleteIndicator => ', athleteIndicator)
                console.log('athlete.creationTimestamp => ', athleteIndicator.creationTimestamp)

                const theCode = sanitizeOnboardingCode(code)

                athleteIndicator.ipScore = athleteIndicator.ipScore || 0
                athleteIndicator.runningTotalIpScore = athleteIndicator.runningTotalIpScore || 0
                let results: OnboardingValidationResponse
                if (theCode === 'INSPIRE') {
                    results = await createEntities(athleteIndicator, theCode)
                } else {
                    results = await validateOrgCode(theCode, athleteIndicator, 0)
                }

                if (profile && results.responseCode === 200) {
                    console.warn('====>>>>> SAVING THE USER PROFILE', profile)
                    const saveProfileResults: OnboardingValidationResponse = await saveAthleteProfile(user, results, athleteIndicator, profile, context.instanceIdToken || 'UNKNOWN');
                    const responseWithAthlete: OnboardingValidationResponse = { ...results, ...saveProfileResults }

                    // responseWithAthlete.data = { athlete: responseWithAthlete.data.athlete }
                    // return responseWithAthlete

                    const { athleteIndicator: toBeRemoved, ...theRestData } = responseWithAthlete.data
                    console.warn('====>>>>> theRestData', theRestData)
                    const theOnboardingResponse = {
                        ...responseWithAthlete,
                        data: {
                            ...theRestData,
                            groups: theRestData.groups.map((group) => {
                                return {
                                    ...group,
                                    creationTimestamp: group.creationTimestamp.toDate().toJSON()
                                }
                            })
                        }
                    }
                    console.log('====>>>>> theOnboardingResponse', theOnboardingResponse)

                    return theOnboardingResponse
                } else if (results.responseCode === 200) {



                    if (results.message === 'Valid Media Key') {
                        console.warn('====>>>>> Returning Media Provisioning Key', profile)
                        return results
                    } else {

                        // let themeUid: WhiteLabelThemeType =results.data. org.theme || 'default'

                        // const theTheme = await getCollectionRef(FirestoreCollection.WhiteLabelThemes).doc(themeUid)
                        //     .get().then((docSnap) => {
                        //         if (docSnap.exists) {
                        //             return getDocumentFromSnapshot(docSnap);
                        //         } else {
                        //             themeUid = 'default'

                        //             return getCollectionRef(FirestoreCollection.WhiteLabelThemes).doc(themeUid)
                        //                 .get().then((defaultDocSnap) => {
                        //                     return getDocumentFromSnapshot(defaultDocSnap);
                        //                 })
                        //         }

                        // }) as WhiteLabelTheme

                        // results.data.theme = theTheme
                        
                        console.warn('====>>>>> SKIP SAVING THE USER PROFILE', profile)
                        const { athleteIndicator: toBeRemoved, ...theRestData } = results.data
                        console.warn('====>>>>> theRestData', theRestData)
                        const theOnboardingResponse = {
                            ...results,
                            data: {
                                ...theRestData,
                                groups: theRestData.groups.map((group) => {
                                    return {
                                        ...group,
                                        creationTimestamp: group.creationTimestamp.toDate().toJSON()
                                    }
                                }),
                                subscription: {
                                    ...theRestData.subscription,
                                    commencementDate: theRestData.subscription.commencementDate
                                        ? theRestData.subscription.commencementDate.toDate().toJSON()
                                        : null,
                                    expirationDate: theRestData.subscription.expirationDate
                                        ? theRestData.subscription.expirationDate.toDate().toJSON()
                                        : null
                                }
                                // onboardingProgram: {
                                //     ...theRestData.onboardingProgram,
                                //     startDate: theRestData.onboardingProgram.startDate.toDate().toJSON()
                                // }
                            }
                        }
                        console.log('====>>>>> theOnboardingResponse', theOnboardingResponse)

                        return theOnboardingResponse
                    }

                } else {
                    console.warn('====>>>>> results', results)
                    // const { athleteIndicator: toBeRemoved, ...theRestData } = results.data
                    // console.warn('====>>>>> theRestData', theRestData)

                    const theOnboardingResponse = {
                        ...results,
                        responseCode: results.responseCode,
                        data: {
                            ...results,
                            // groups: results.groups.map((group) => {
                            //     return {
                            //         ...group,
                            //         creationTimestamp: group.creationTimestamp.toDate().toJSON()
                            //     }
                            // }),
                            // subscription: {
                            //     ...theRestData.subscription,
                            //     commencementDate: theRestData.subscription.commencementDate
                            //         ? theRestData.subscription.commencementDate.toDate().toJSON()
                            //         : null,
                            //     expirationDate: theRestData.subscription.expirationDate
                            //         ? theRestData.subscription.expirationDate.toDate().toJSON()
                            //         : null
                            // }
                        }
                    }
                    console.log('====>>>>> theOnboardingResponse', theOnboardingResponse)

                    return theOnboardingResponse
                }

            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return {
                    message:
                        'An unanticipated exception occurred in validateOrganizationCode',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in validateOrganizationCode',
                        exception: exception,
                    },
                    method: {
                        name: 'validateOrganizationCode'
                    }
                }
            }

            // console.log('groupid ', groupId)

            // return await updateAthleteIndicatorsIPScoreOnGroup(groupId).then((results) => {

            //     return results
            // }).catch((err) => {
            //     return err

            // })

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)
