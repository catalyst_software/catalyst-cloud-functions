const functions = require('firebase-functions');
import { db } from "../../shared/init/initialise-firebase";
import { FirestoreCollection } from '../../db/biometricEntries/enums/firestore-collections';
import { EmailMessage, TemplateData, replaceParts } from "../../api/app/v1/sendGrid/conrollers/email";
import { isProductionProject } from "../../api/app/v1/is-production";
import { KeyValuePair } from "../../models/notifications/interfaces/schedule-notification-payload";
import { StackDriverTemplateIDs } from "../../models/enums/lifestyle-entry-type";
const sgMail = require('@sendgrid/mail');


/**
 * Send Email.
 *
 * @param {string} message The Message to be sent.
 * @param {string} key SendGrid API Key.
 */

export const sendEmail = async (to: Array<{
  name: string;
  email: string;
}>, message: EmailMessage, key: string) => {

  const emailTemplateName = message.emailTemplateName || 'badMood';
  console.log('FETCHING TemplateData for ', emailTemplateName)

  const dbTemplateData = await getTemplateFromDB(emailTemplateName)

  if (dbTemplateData.isActive) {
    message.personalizations[0].to = to

    const config = functions.config();
    // console.log('config ?!?!?', config)
    if (isProductionProject() && config.sendgrid && config.sendgrid.key) {
      sgMail.setApiKey(config.sendgrid.key);
    }
    else if (key) {
      if (key && key !== '') {
        sgMail.setApiKey(key);
      }
      else {
        console.log('SendGrid API Key undefined')
        return {
          message: 'sendEmail',
          responseCode: 501,
          data: {},
          error: 'SendGrid API Key undefined',
          method: {
            name: 'sendEmail',
            line: 58,
          },
        };
      }
    }

    const { programs: dbPrograms, ...restDBTemplateData } = dbTemplateData;
    const { programs: customPrograms, ...restCustomTemplateData } = message.dynamic_template_data;

    const keyValuePairs: KeyValuePair[] = [
      {
        key: '{{{name}}}',
        value: restCustomTemplateData.name
      },
      {
        key: '{{{userFirstName}}}',
        value: restCustomTemplateData.userFirstName
      },
      {
        key: '{{{groupName}}}',
        value: restCustomTemplateData.groupName
      }
    ];

    if (restCustomTemplateData.numberOfAthletes) {
      keyValuePairs.push({
        key: '{{{numberOfAthletes}}}',
        value: restCustomTemplateData.numberOfAthletes.toString()
      })
    }

    if (restCustomTemplateData.dayCountNonEngagement) {
      keyValuePairs.push({
        key: '{{{dayCountNonEngagement}}}',
        value: restCustomTemplateData.dayCountNonEngagement.toString()
      })
    }
    const parsedMessage = replaceParts({ ...restDBTemplateData, ...restCustomTemplateData }, keyValuePairs);
    // console.log('parsedMessage', parsedMessage)

    const program1 = customPrograms && customPrograms[0]
      ? customPrograms[0] : dbPrograms[0];

    const program2 = customPrograms && customPrograms[1]
      ? customPrograms[1] : dbPrograms[1];

    message.templateId = message.templateId || StackDriverTemplateIDs.DoubleImageLayout;
    message.dynamic_template_data = { ...parsedMessage, program1, program2 };

    const from = {
      name: "CATALYST",
      email: "noreply@catalyst.com"
    }
    // message.personalizations[0].to[0].email = 'igouws@gmail.com'
    message.from = from
    console.log('message.to', message.personalizations[0].to)

    const sendResult = await sgMail.send({
      ...message,
    }, (resCB: any) => {
      if (resCB) {
        console.log('resCB', resCB);
      } else if (resCB) {
        console.log('resCB is undefined?!?', resCB);
      }
    })
      .then((response: { statusCode: any; }[]) => {
        // POST succeeded...
        // console.log('parsedBody', parsedBody)
        // console.log('sendEmail statusCode', response[0].statusCode);
        console.log('sendEmail statusCode', { response });
        return {
          message: 'sendEmail',
          // responseCode: response[0].statusCode || 202,
          responseCode: 202,
          data: {
            response,
          },
          method: {
            name: 'sendEmail',
            line: 58,
          },
        };
      })
      .catch((err: any) => {
        // POST failed...
        console.log('err', err);
        return {
          message: 'sendEmail',
          responseCode: 500,
          data: {},
          error: err,
          method: {
            name: 'sendEmail',
            line: 58,
          },
        };
      });

    return sendResult
  } else {
    console.log('Notifications Disabled')
    return false
  }
  // return templateDataFromDB

};
async function getTemplateFromDB(emailTemplateName: string) {
  return await db.collection(FirestoreCollection.EmailTemplates).doc(emailTemplateName)
    .get().then(async (docSnap) => {

      if (docSnap.exists) {
        return {
          uid: docSnap.id,
          ...docSnap.data() as TemplateData
        };
      } else {
        console.warn(`Email Template Data not available for ${emailTemplateName}, defaulting to badMood`);
        const template = await db.collection(FirestoreCollection.EmailTemplates).doc('badMood')
          .get().then((badMoodDocSnap) => {

            if (badMoodDocSnap.exists) {
              return {
                uid: badMoodDocSnap.id,
                ...badMoodDocSnap.data() as TemplateData
              };
            } else {
              return undefined;
            }

          });

        return template;
      }

    });
}

