firebase deploy --only functions:appEngagmentBigQueryWorker
firebase deploy --only functions:appSessionUsageMetricBigQueryWorker
firebase deploy --only functions:biometricBigQueryWorker
firebase deploy --only functions:redFlagBigQueryWorker
firebase deploy --only functions:programContentEngagmentBigQueryWorker
firebase deploy --only functions:notificationCardEngagmentBigQueryWorker
firebase deploy --only functions:ipScoreAchievementBigQueryWorker
firebase deploy --only functions:cardRenderedBigQueryWorker
firebase deploy --only functions:selfHealingEveryTwoHourWorker
firebase deploy --only functions:bigQueryDataManipulationReattemptWorker
firebase deploy --only functions:cloudTaskUsageMetricBigQueryWorker
firebase deploy --only functions:onboardingInvitationWorker
firebase deploy --only functions:manageSegmentDeletionWorker
firebase deploy --only functions:manageAthleteSegmentAdditionWorker
firebase deploy --only functions:manageNewProgramAdditionWorker
firebase deploy --only functions:scheduleNotificationCardDeliveryWorker

