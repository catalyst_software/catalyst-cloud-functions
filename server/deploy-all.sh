firebase deploy --only functions:updateIpScore
# firebase deploy --only functions:addCoachClaim
# firebase deploy --only functions:getBiomentricEntriesById
# firebase deploy --only functions:getSurveysById
firebase deploy --only functions:updateAggregates
firebase deploy --only functions:athleteBiometricHistoryOnCreate
firebase deploy --only functions:groupBiometricHistoryOnCreate
firebase deploy --only functions:notificationGooglePlay
# Event OnLog
# firebase deploy --only functions:transactionValidationErrorTrigger
# Event OnLog
# firebase deploy --only functions:iapInitialBuyFailedTrigger
# Event OnLog
# firebase deploy --only functions:eventListeners
# firebase deploy --only functions:scheduleEssentialComms
# firebase deploy --only functions:scheduleSendNotifications
# firebase deploy --only functions:schedulePushNotification
# firebase deploy --only functions:updateAthleteIpScoreForGroups
# firebase deploy --only functions:updateCoachMessaginIDsOnConfig
firebase deploy --only functions:ipScoreNonEngagement
# firebase deploy --only functions:validateOrganizationCode
# firebase deploy --only functions:setUserEmailVerified
# firebase deploy --only functions:getUserEmailVerified
# firebase deploy --only functions:setUserEmail
# firebase deploy --only functions:validateTransactionReceipt
# firebase deploy --only functions:updateAthleteSubscription
# firebase deploy --only functions:deleteUserAccount
firebase deploy --only functions:bqExport
# firebase deploy --only functions:getBQData
# firebase deploy --only functions:runGroupIpScoreAggregation
# firebase deploy --only functions:runGroupWellnessDataAggregation
# firebase deploy --only functions:updateNotificationCardMetadata
# firebase deploy --only functions:scheduleReport
# firebase deploy --only functions:bigQSqlQuery
# firebase deploy --only functions:getOembedData
# firebase deploy --only functions:updateCalendarEventInvitationStatus
firebase deploy --only functions:updateAthleteIndicatorBIO
# firebase deploy --only functions:moveAthleteToGroup
# firebase deploy --only functions:addAthleteToGroup
# firebase deploy --only functions:removeAthleteFromGroup
# firebase deploy --only functions:getDynamicGroupId
# firebase deploy --only functions:setRedFlagResloveState
# firebase deploy --only functions:createVideoConversionTask
firebase deploy --only functions:processTasks
firebase deploy --only functions:processAthletes
firebase deploy --only functions:manageAsyncJobQueueOnCreate
firebase deploy --only functions:manageAsyncJobQueueOnUpdate
firebase deploy --only functions:appEngagmentBigQueryWorker
firebase deploy --only functions:appSessionUsageMetricBigQueryWorker
firebase deploy --only functions:biometricBigQueryWorker
firebase deploy --only functions:redFlagBigQueryWorker
firebase deploy --only functions:programContentEngagmentBigQueryWorker
firebase deploy --only functions:notificationCardEngagmentBigQueryWorker
firebase deploy --only functions:ipScoreAchievementBigQueryWorker
firebase deploy --only functions:cardRenderedBigQueryWorker
firebase deploy --only functions:selfHealingEveryTwoHourWorker
firebase deploy --only functions:bigQueryDataManipulationReattemptWorker
firebase deploy --only functions:cloudTaskUsageMetricBigQueryWorker
firebase deploy --only functions:onboardingInvitationWorker
firebase deploy --only functions:manageSegmentDeletionWorker
firebase deploy --only functions:manageAthleteSegmentAdditionWorker
firebase deploy --only functions:manageNewProgramAdditionWorker
firebase deploy --only functions:scheduleNotificationCardDeliveryWorker
# gcloud functions deploy scheduleNotificationCardDeliveryHttpHandler --runtime nodejs10 --trigger-http --allow-unauthenticated


