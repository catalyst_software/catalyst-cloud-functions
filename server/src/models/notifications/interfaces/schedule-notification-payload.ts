import { PushNotificationType } from '../../enums/push-notification-type'

export interface ScheduleNotificationPayload {
    type: PushNotificationType
    token: string
    keyValuePairs?: Array<KeyValuePair>
    appIdentifier?: string;
}

export interface KeyValuePair {
    key: string;
    value: string;
    type?: string;
}

export interface NotificationTemplate {
    message: {
        notification: {
            title: string;
            body: string;
        };
        data: {
            icon: string;
            path: string;
            actionAnalytics: string;
            athleteUid: string;
            dateTime?: string;
        },
        android: {
            notification: {
                sound: string;
            }
        },
        apns: {
            payload: {
                aps: {
                    sound: string;
                };
            };
        }
        token: string;
        condition: string;
    },
    notificationType: number;
    name?: string;
    excludePush?: boolean;
    excludeEmail?: boolean;
}

export interface PushNotification {
    type: PushNotificationType
    notificationTemplate: NotificationTemplate
}
