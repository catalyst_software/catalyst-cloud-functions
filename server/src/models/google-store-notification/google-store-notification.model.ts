import { GoogleStoreNotification as GoogleStoreNotificationInterface } from './interfaces/google-store-notification';

export class GoogleStoreNotification implements GoogleStoreNotificationInterface {
    // Overides
    version:number;
    packageName: string;
    eventTimeMillis:Date;
    subscriptionNotification:
        {
            version:number;
            notificationType:number;
            purchaseToken:string;
            subscriptionId:string;
        };

    constructor(props: GoogleStoreNotificationInterface ) {
        // super(props);
    }
}

export interface GoogleStoreNotificationResponse {
    version:number;
    packageName: string;
    eventTimeMillis:Date;
    subscriptionNotification:
        {
            version:number;
            notificationType:number;
            purchaseToken:string;
            subscriptionId:string;
        };
}

export interface GoogleStoreNotificationRequest {
    athleteUid: string;
    platform: string; // 'iOS | Android'
    productIdentifier: string;
    transactionDate: Date;
    transactionIdentifier: string;
    transactionReceipt: string;
}
