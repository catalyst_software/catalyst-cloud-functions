export interface GoogleStoreNotification {
	"version":number;
	"packageName": string;
	"eventTimeMillis":Date;
	"subscriptionNotification":
		{
			"version":number;
			"notificationType":number;
			"purchaseToken":string;
			"subscriptionId":string;
		}
}
