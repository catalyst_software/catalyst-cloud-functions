
import { firestore } from "firebase-admin";

import { IpScoreConfiguration } from "../db/ipscore/models/documents/tracking/ip-score-configuration.interface";

import { OnboardingConfiguration, OrganizationTool } from "./onboarding-configuration";
import { BaseRecord } from "../db/biometricEntries/well/documents/base-record";
import { WhiteLabelThemeType } from "../api/app/v1/interfaces/WhiteLabelTheme";

export interface OrganizationInterface {
    uid: string;
    name: string;
    organizationCode?: string;
    licensesPurchased?: number;
    licensesConsumed?: number;
    numberUsersOnboarded?: number;
    contactPerson?: string;
    contactNumber?: string;

    ipScoreConfiguration: IpScoreConfiguration;
    nonEngagementConfiguration: NonEngagementConfiguration;
    commsConfiguration: CommsConfiguration;

    // currentPrograms?: Array<Program>;

    onboardingConfiguration: OnboardingConfiguration;

    coaches: Array<string>;
    orgReps: Array<string>;
    // theme: WhiteLabelThemeType;
    theme?: string;
    creationTimestamp?: firestore.Timestamp;
    contentProviderId?: string;
    parentOrganizationId?: string;
    config?: OrganizationConfigurationInterface;

    currentEvolution?: CurrentEvolutionTracking;
    orgTools?: Array<OrganizationTool>;
    redFlagNotifications?: OrganizationRedFlagNotifications;
}

export interface OrganizationRedFlagNotifications {
    processPeriod: boolean;
    processSickness: boolean;
    processInjury: boolean;
    processPain: boolean;
    processSleep: boolean;
    processMood: boolean;
    processFatigue: boolean;
    processExcessiveTraining: boolean;
}

export interface OrganizationConfigurationInterface {
    wellnessGeoCapture: WellnessGeoCaptureConfigurationInterface;
    sickSymptomTracking: Array<string>;
    validConsumedOnboardingCodes?: Array<string>;
    validConsumedMediaBundleCodes?: Array<ConsumableContentInterface>;
    validConsumedNotificationStreamCodes?: Array<ConsumableContentInterface>;
    publishedMediaBundles?: Array<MediaBundleInterface>;
    bucketSize?: number;
    groupPrefix?: string;
    storageRoot?: string;
    showLeaderboard?: boolean;
    showCommunityHub?: boolean;
    forceAutocompleteValueRefresh?: boolean;
    validThemes?: Array<string>;
    allowInvalidThemeChanges?: boolean;

    termsAndConditionsUrl?: string;
    privacyStatementUrl?: string;
    appChannelId?: string;

    restrictVideoFastForwardToFirstCompletion?: boolean;
    videoSkipTimeInSeconds?: number;

    processProgramCardRenderTasks?: boolean;
    processNotificationCardRenderTasks?: boolean;
    convertVideoStream?: string;
}

export interface CurrentEvolutionTracking {
    totalDistance?: number;
    totalDuration?: number;
    updateDate?: Date;
    numberOfParticipants?: number;
    numberOfCountries?: number;
}

export interface NonEngagementConfiguration {
    dayCountNonEngagement: number;
    utcRunHour: number;
}

export interface CommsConfiguration {
    reports: {
        instanceName: string;
        portNumber: number;
    },
    excludedNotificationTypes: Array<number>;
}

export interface ConsumableContentInterface {
    code: string;
    unrestricted?: boolean;
    licensesPurchased?: number;
    licensesConsumed?: number;
}

export interface WellnessGeoCaptureConfigurationInterface {
    mood?: boolean;
    sleep?: boolean;
    fatigue?: boolean;
    pain?: boolean;
    food?: boolean;
    training?: boolean;
    period?: boolean;
    sick?: boolean;
}

export class Organization extends BaseRecord implements OrganizationInterface {
    readonly uid: string;
    creationTimestamp?: firestore.Timestamp;
    name: string;
    readonly organizationCode: string;
    readonly licensesPurchased: number;
    readonly licensesConsumed: number;
    contactPerson: string;
    contactNumber: string;
    numberUsersOnboarded?: number;
    ipScoreConfiguration: IpScoreConfiguration;
    nonEngagementConfiguration: NonEngagementConfiguration;
    commsConfiguration: CommsConfiguration;
    // currentPrograms?: Array<Program>;

    onboardingConfiguration: OnboardingConfiguration;

    coaches: Array<string>;
    orgReps: Array<string>;
    theme: WhiteLabelThemeType;

    config: OrganizationConfigurationInterface;
    parentOrganizationId?: string;
    // constructor(props: OrganizationInterface) {

    //     // super(props);

    //     this.licensesConsumed = props.licensesConsumed || 1;
    //     this.licensesPurchased = props.licensesPurchased || 1;

    //     this.creationTimestamp = props.creationTimestamp || firestore.Timestamp.now();

    //     // TODO: ...spread props.onboardingConfiguration 
    //     this.onboardingConfiguration = props.onboardingConfiguration || {
    //         // TODO: Check when to set this
    //         autoCreateGroups: true,
    //         prePaidNumberOfMonths: 3,
    //         isPrePaid: true,
    //         subscriptionType: SubscriptionType.Basic
    //     }
    // }

    redFlagNotifications?: OrganizationRedFlagNotifications;
    currentEvolution?: CurrentEvolutionTracking;
}

export interface OrganizationConfigurationInterface {
    wellnessGeoCapture: WellnessGeoCaptureConfigurationInterface;
    sickSymptomTracking: Array<string>;
    validConsumedOnboardingCodes?: Array<string>;
    validConsumedMediaBundleCodes?: Array<ConsumableContentInterface>;
    validConsumedNotificationStreamCodes?: Array<ConsumableContentInterface>;
    publishedMediaBundles?: Array<MediaBundleInterface>;
    bucketSize?: number;
    groupPrefix?: string;
    storageRoot?: string;
    showLeaderboard?: boolean;
    showCommunityHub?: boolean;
    forceAutocompleteValueRefresh?: boolean;
    validThemes?: Array<string>;
    allowInvalidThemeChanges?: boolean;
    termsAndConditionsUrl?: string;
    privacyStatementUrl?: string;
    appChannelId?: string;
    restrictVideoFastForwardToFirstCompletion?: boolean;
    videoSkipTimeInSeconds?: number;
    processProgramCardRenderTasks?: boolean;
    processNotificationCardRenderTasks?: boolean;
}
export interface MediaBundleInterface {
    code: string;
    provisioningKey: string;
}
export interface ConsumableContentInterface {
    code: string;
    unrestricted?: boolean;
    licensesPurchased?: number;
    licensesConsumed?: number;
}
export interface WellnessGeoCaptureConfigurationInterface {
    mood?: boolean;
    sleep?: boolean;
    fatigue?: boolean;
    pain?: boolean;
    food?: boolean;
    training?: boolean;
    period?: boolean;
    sick?: boolean;
}
export interface MediaBundleInterface {
    code: string;
    provisioningKey: string;
}

