import { SubscriptionType } from './enums/enums.model';

export interface OrganizationTool {
    url: string;
    name: string;
    text: string;
};

export interface OnboardingConfiguration {
    autoCreateGroups: boolean;
    prePaidNumberOfMonths?: number;
    isPrePaid: boolean;
    subscriptionType?: SubscriptionType;

    groupName?: string;
    paidGroups?: Array<OnboardingConfiguration>;
    tools: Array<OrganizationTool>;

}