import { AthleteWeeklyAggregateInterface, AthleteMonthlyAggregateInterface, GroupMonthlyAggregateInterface, GroupWeeklyAggregateInterface } from '../../db/biometricEntries/well/aggregate/interfaces';
import { AthleteWeeklyHistoryAggregateInterface } from '../../db/biometricEntries/well/aggregate/interfaces/athlete/athlete-biometric-weekly-history-aggregate';
import { AthleteMonthlyHistoryAggregateInterface } from '../../db/biometricEntries/well/aggregate/interfaces/athlete/athlete-biometric-monthly-history-aggregate';
import { GroupMonthlyHistoryAggregateInterface } from '../../db/biometricEntries/well/aggregate/interfaces/group/group-biometric-monthly-history-aggregate';
import { GroupWeeklyHistoryAggregateInterface } from '../../db/biometricEntries/well/aggregate/interfaces/group/group-biometric-weekly-history-aggregate';

import { AthleteBiometricEntryInterface } from '../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { Athlete } from '../athlete/interfaces/athlete';
// import { Group } from '../athlete/interfaces/group';
import { GroupIndicator } from '../../db/biometricEntries/well/interfaces/group-indicator';
import { AthleteWeeklyAggregate, GroupWeeklyAggregate, AthleteWeeklyHistoryAggregate, GroupWeeklyHistoryAggregate } from '../../db/biometricEntries/models/documents/aggregates';
import { AthleteMonthlyHistoryAggregate } from '../../db/biometricEntries/models/documents/aggregates/athlete/athelete-monthly-history-aggregate.document';
import { GroupMonthlyHistoryAggregate } from '../../db/biometricEntries/models/documents/aggregates/group/group-monthly-history-aggregate.document';
import { Group } from '../group/interfaces/group';
import { BinaryBiometricHistoryAggregate } from '../../db/biometricEntries/models/documents/aggregates/binary-history-aggregate.document';
import { WellAggregateCollection } from '../../db/biometricEntries/enums/firestore-aggregate-collection';
import { IpScoreAggregateCollection } from '../../db/ipscore/enums/firestore-ipscore-aggregate-collection';
import { IpScoreAggregateHistoryCollection } from '../../db/ipscore/enums/firestore-ipscore-aggregate-history-collection';
import { AggregateDocumentDirective } from '../../db/biometricEntries/services/interfaces/aggregate-document-directive';
import { AggregateIpScoreDirective } from '../../db/ipscore/services/interfaces/aggregate-ipscore-directive';
import { WellAggregateHistoryCollection } from '../../db/biometricEntries/enums/firestore-aggregate-history-collection';
import { WellAggregateBinaryHistoryCollection } from '../../db/biometricEntries/enums/firestore-aggregate-binary-history-collection';


export type BiometricAggregateType = AthleteWeeklyAggregateInterface & AthleteMonthlyAggregateInterface | GroupMonthlyAggregateInterface & GroupWeeklyAggregateInterface;
type HistoryAggregateInterfaceType = AthleteWeeklyHistoryAggregateInterface & AthleteMonthlyHistoryAggregateInterface | GroupMonthlyHistoryAggregateInterface & GroupWeeklyHistoryAggregateInterface;
export type GeneralAggregateType = HistoryAggregateInterfaceType | BiometricAggregateType;


export type AllAggregateInterfaceType = AthleteBiometricEntryInterface & GeneralAggregateType;

// AGGREGATE DOCUMENT TYPES
export type AggregateType = AthleteWeeklyAggregate | GroupWeeklyAggregate

export type HistoryAggregateType =
    AthleteWeeklyHistoryAggregate |
    AthleteMonthlyHistoryAggregate |
    GroupWeeklyHistoryAggregate |
    GroupMonthlyHistoryAggregate

export type BinaryHistoryAggregateType = BinaryBiometricHistoryAggregate;

export type AllAggregateType = AggregateType | HistoryAggregateType | BinaryBiometricHistoryAggregate

// WEEKLIES AND MONTHLIES
export type WeeklyAggregateType = AthleteWeeklyAggregate | GroupWeeklyAggregate;
export type WeeklyHistoryAggregateType = AthleteWeeklyHistoryAggregate | GroupWeeklyHistoryAggregate;
export type MonthlyHistoryAggregateType = AthleteMonthlyHistoryAggregate | GroupMonthlyHistoryAggregate;

export type EntityType = AthleteBiometricEntryInterface | Athlete | Group | GroupIndicator;

// COLLECTION TYPES
export type WellAggregateCollectionType = WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection;
export type IPScoreAggregateCollectionType = IpScoreAggregateCollection | IpScoreAggregateHistoryCollection;
export type AllAggregateCollectionType = WellAggregateCollectionType | IPScoreAggregateCollectionType;


// AGGREGATES
export type AggregateDirectiveType = AggregateDocumentDirective | AggregateIpScoreDirective;
