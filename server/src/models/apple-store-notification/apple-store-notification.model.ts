import { AppleStoreNotification as AppleStoreNotificationInterface } from './interfaces/apple-store-notification'

export class AppleStoreNotification implements AppleStoreNotificationInterface {

    auto_renew_status: boolean
    auto_renew_product_id: string
    environment: string
    cancellation_date: Date
    cancellation_date_ms: Date
    cancellation_date_pst: Date
    latest_receipt?: any // Posted only if the notification_type is RENEWAL or INTERACTIVE_RENEWAL, and only if the renewal is successful.
    latest_receipt_info?: { // Posted only if renewal is successful. Not posted for notification_type CANCEL
        // app_item_id: number
        quantity: number
        unique_vendor_identifier: string
        original_purchase_date_ms: number
        is_in_intro_offer_period: boolean
        purchase_date_ms: number
        is_trial_period: boolean
        item_id: number
        unique_identifier: string
        original_transaction_id: string
        expires_date: string
        transaction_id: number
        bvrs: number
        web_order_line_item_id: number
        version_external_identifier: number
        bid: string
        product_id: string
        expires_date_formatted: string
        purchase_date: string
        original_purchase_date: string
    }
    latest_expired_receipt: string // Posted only if the subscription expired
    latest_expired_receipt_info: {
        original_purchase_date_pst: Date
        cancellation_date_ms: Date
        quantity: number
        cancellation_reason: number
        unique_vendor_identifier: string
        original_purchase_date_ms: Date
        expires_date_formatted: Date
        is_in_intro_offer_period: boolean
        purchase_date_ms: Date
        expires_date_formatted_pst: Date
        is_trial_period: boolean
        item_id: string
        unique_identifier: string
        original_transaction_id: string
        expires_date: Date
        app_item_id: string
        transaction_id: number
        bvrs: number
        web_order_line_item_id: string
        version_external_identifier: string
        bid: string
        cancellation_date: Date
        product_id: string
        purchase_date: Date
        cancellation_date_pst: Date
        purchase_date_pst: Date
        original_purchase_date: Date
    }
    password: string
    notification_type: string
    original_transaction_id: any
    web_order_line_item_id: string // Posted only if the notification_type is CANCEL

    constructor(props: AppleStoreNotificationInterface) {
        // super(props);
    }
}



export class AppleStoreExpiredNotification implements AppleStoreExpiredNotification {
    // Overides
   
    latest_expired_receipt_info: {
        original_purchase_date_pst: Date
        cancellation_date_ms: Date
        quantity: number
        cancellation_reason: number
        unique_vendor_identifier: string
        original_purchase_date_ms: Date
        expires_date_formatted: Date
        is_in_intro_offer_period: boolean
        purchase_date_ms: Date
        expires_date_formatted_pst: Date
        is_trial_period: boolean
        item_id: string
        unique_identifier: string
        original_transaction_id: string
        expires_date: Date
        app_item_id: string
        transaction_id: string
        bvrs: number
        web_order_line_item_id: string
        version_external_identifier: string
        bid: string
        cancellation_date: Date
        product_id: string
        purchase_date: Date
        cancellation_date_pst: Date
        purchase_date_pst: Date
        original_purchase_date: Date
    }

    constructor(props: AppleStoreNotificationInterface) {
        // super(props);
    }
}

export interface AppleStoreNotificationResponse {
    environment: string
    auto_renew_status: boolean
    web_order_line_item_id: string
    latest_expired_receipt_info: {
        original_purchase_date_pst: Date
        cancellation_date_ms: Date
        quantity: number
        cancellation_reason: number
        unique_vendor_identifier: string
        original_purchase_date_ms: Date
        expires_date_formatted: Date
        is_in_intro_offer_period: boolean
        purchase_date_ms: Date
        expires_date_formatted_pst: Date
        is_trial_period: boolean
        item_id: string
        unique_identifier: string
        original_transaction_id: string
        expires_date: Date
        app_item_id: string
        transaction_id: string
        bvrs: number
        web_order_line_item_id: string
        version_external_identifier: string
        bid: string
        cancellation_date: Date
        product_id: string
        purchase_date: Date
        cancellation_date_pst: Date
        purchase_date_pst: Date
        original_purchase_date: Date
    }
    cancellation_date_ms: Date
    latest_expired_receipt: string
    cancellation_date: Date
    password: string
    cancellation_date_pst: Date
    auto_renew_product_id: string
    notification_type: string
}

export interface AppleStoreNotificationRequest {
    environment: string
    auto_renew_status: boolean
    web_order_line_item_id: string
    latest_expired_receipt_info: {
        original_purchase_date_pst: Date
        cancellation_date_ms: Date
        quantity: number
        cancellation_reason: number
        unique_vendor_identifier: string
        original_purchase_date_ms: Date
        expires_date_formatted: Date
        is_in_intro_offer_period: boolean
        purchase_date_ms: Date
        expires_date_formatted_pst: Date
        is_trial_period: boolean
        item_id: string
        unique_identifier: string
        original_transaction_id: string
        expires_date: Date
        app_item_id: string
        transaction_id: string
        bvrs: number
        web_order_line_item_id: string
        version_external_identifier: string
        bid: string
        cancellation_date: Date
        product_id: string
        purchase_date: Date
        cancellation_date_pst: Date
        purchase_date_pst: Date
        original_purchase_date: Date
    }
    cancellation_date_ms: Date
    latest_expired_receipt: string
    cancellation_date: Date
    password: string
    cancellation_date_pst: Date
    auto_renew_product_id: string
    notification_type: string
}
