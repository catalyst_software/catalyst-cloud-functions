export interface AppleStoreNotification {
	environment: string;
	auto_renew_status: boolean;
	web_order_line_item_id: string;
	cancellation_date_ms: Date;
	latest_expired_receipt: string;
	cancellation_date: Date;
	password: string;
	cancellation_date_pst: Date;
	auto_renew_product_id:  string;
	notification_type: string;
}

export interface AppleStoreExpiredNotification extends AppleStoreNotification{
	latest_expired_receipt_info: {
		original_purchase_date_pst: Date;
		cancellation_date_ms: Date;
		quantity: number;
		cancellation_reason: number;
		unique_vendor_identifier: string;
		original_purchase_date_ms: Date;
		expires_date_formatted: Date;
		is_in_intro_offer_period: boolean;
		purchase_date_ms: Date;
		expires_date_formatted_pst: Date;
		is_trial_period: boolean;
		item_id: string;
		unique_identifier: string;
		original_transaction_id: string;
		expires_date: Date;
		app_item_id: string;
		transaction_id: string;
		bvrs: number;
		web_order_line_item_id: string;
		version_external_identifier: string;
		bid: string;
		cancellation_date: Date;
		product_id: string;
		purchase_date: Date;
		cancellation_date_pst: Date;
		purchase_date_pst: Date;
		original_purchase_date: Date;
	};
}

export interface AppleStoreLatestNotification extends AppleStoreNotification{
	latest_receipt_info: {
		original_purchase_date_pst: Date;
		cancellation_date_ms: Date;
		quantity: number;
		cancellation_reason: number;
		unique_vendor_identifier: string;
		original_purchase_date_ms: Date;
		expires_date_formatted: Date;
		is_in_intro_offer_period: boolean;
		purchase_date_ms: Date;
		expires_date_formatted_pst: Date;
		is_trial_period: boolean;
		item_id: string;
		unique_identifier: string;
		original_transaction_id: string;
		expires_date: Date;
		app_item_id: string;
		transaction_id: string;
		bvrs: number;
		web_order_line_item_id: string;
		version_external_identifier: string;
		bid: string;
		cancellation_date: Date;
		product_id: string;
		purchase_date: Date;
		cancellation_date_pst: Date;
		purchase_date_pst: Date;
		original_purchase_date: Date;
	};
}