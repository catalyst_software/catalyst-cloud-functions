import { AthleteIndicator } from "../db/biometricEntries/well/interfaces/athlete-indicator";
import { GroupIndicator } from "../db/biometricEntries/well/interfaces/group-indicator";
import { ReportRequestEntityType } from "./enums/enums.model";
import { OrganizationInterface } from "./organization.model";
import { NotificationCardType } from "./enums/UpdateNotificationCardMetadataDirective";

export enum ReportType {
    AthleteRedFlagReport = 0,
    GroupRedFlagReport = 1,
    OrganizationalRedFlagReport = 2
}

export interface ReportRequestInterface {
    reportType: ReportType;
    requestDateTime: FirebaseFirestore.Timestamp;
    requestProcessed: boolean;
    requestSuccess?: boolean;
    requestProcessedDateTime?: FirebaseFirestore.Timestamp;
    requestingUser: AthleteIndicator;

    taskId?: string;

    entity: {
        type: ReportRequestEntityType;
        entityId: string;
        entityIndicator: AthleteIndicator | GroupIndicator | OrganizationInterface;
    };

    errorLogs?: Array<string>;
}

export interface ReportRequestResponseInterface {
    success: boolean;
    message: string;
    taskId: string;
}

export type ReportEntity = {
    type: ReportRequestEntityType;
    entityId: string;
    entityIndicator: AthleteIndicator | GroupIndicator;
    groupIDs: Array<string>
};

export class ReportRequest implements ReportRequestInterface {
    readonly uid: string;
    reportType: ReportType;
    requestDateTime: FirebaseFirestore.Timestamp;
    requestEndDateTime: FirebaseFirestore.Timestamp;
    requestProcessed: boolean;
    requestSuccess?: boolean;
    requestProcessedDateTime?: FirebaseFirestore.Timestamp;
    requestingUser: AthleteIndicator;

    taskId?: string;

    entity: ReportEntity;

    errorLogs?: Array<string>;
    groupIDs?: Array<string>

    constructor() {
        // super(props);
    }
}


export interface ManageAthleteGroupRequestInterface {

    groupId: string;
    userId: string;
    coachFlag?: boolean;
    moveUserOrg?: boolean;
    orgUID: string;
    currentOrgName: string;
    organizationCode: string
}


export class ManageAthleteGroupRequest implements ManageAthleteGroupRequestInterface {

    groupId: string;
    userId: string;
    coachFlag?: boolean;
    moveUserOrg?: boolean;
    orgUID: string;
    currentOrgName: string;
    organizationCode: string


    constructor() {
        // super(props);
    }
}

export class ManageAthleteRemoveGroupRequest {

    groupId: string;
    userId: string;


    constructor() {
        // super(props);
    }
}

export interface UpdateNotificationCardRequestInterface {
    athleteUID: string
    cardUID: string;
    acknowledged: NotificationCardState;
    creationTimestamp: FirebaseFirestore.Timestamp;
}



export enum NotificationCardBehaviorType {
    Acknowledgement = 'acknowledgement',
}

export enum ContentFeedCardType {
    Program = 'program',
    Notification = 'notification',
}

export enum NotificationCardState {
    Acknowledged = 'acknowledged',
    Dismissed = 'dismissed',
    Rendered = 'rendered',
    Finalized = 'finalized',
}


export interface ContentFeedCardPackageBase {
    uid: string;
    entityId: string;
    creatorId?: string;
    creationTimestamp: Date;

    channelType: ReportRequestEntityType;
    cardType: ContentFeedCardType;
    notificationCardType?: NotificationCardType;
    notificationCardBehaviorType?: NotificationCardBehaviorType;

    title: string;
    description?: string;

    imageUrl?: string; // INSPIRE%2Fimages%2Fbc1f3586-14d4-42a7-a6a6-ce9e251bb15e.png?alt=media

    pdfFileName?: string;
    pdfUrl?: string;
    pdfFileTitle?: string;
    pdfFileGuid?: string;

    webLinkName?: string;
    webLinkUrl?: string;
    webLinkDescription?: string;

    dismissable?: boolean;
    trackViews?: boolean;

    isActive: boolean; //??

    state?: NotificationCardState;
    creatorName: string;
    entityName: string;
    recipients: Array<CardRecipient>
}

export interface NotificationCardPackageBase extends ContentFeedCardPackageBase {
    notificationCardType: NotificationCardType;
    notificationCardBehaviorType: NotificationCardBehaviorType;
    filteredUids?: Array<string>;
}

export interface VideoCardPackageBase extends NotificationCardPackageBase {
    mediaContentEntry: string | {
        title: string, //: "Journaling With Leanne",
        description: string, //: "",
        type: number, //: 1,
        strategyGuid: string, //: "24166bb0-77b4-4940-061c-932e4c48-6762",
        docGuid: string, //: "720a9493-97fb-2a40-0432-69729fdf-2ae4",
        fileGuid: string, //: "94cb49eb-828c-2515-443d-fa0c2319-7f1e",
        videoThumb: string, //: "INSPIRE%2Fvidthumbs%2F94cb49eb-828c-2515-443d-fa0c2319-7f1e.png?alt=media",
        engagement: boolean, //: false,
        version: number, //: 1,
        embed: boolean, //: true,
        tags: string[]
    },
    actualCreationTimestamp, //: "date",
    isSticky, //: true,
    isStickyStatic, //: true,
    stickyStaticExpiryDate, //: "16/09/2020"

}

export type CardRecipient = {
    uid: string;
    firstName: string;
    lastName: string;
    email: string;
    acknowledgeTimestamp: any;
    state?: NotificationCardState;
};