export enum IOSNotificationType {
    InitialBuy = 'INITIAL_BUY',
    Cancel = 'CANCEL',
    Renewal = 'RENEWAL',
    InteractiveRenewal = 'INTERACTIVE_RENEWAL',
    DidChangeRenewalRef = 'DID_CHANGE_RENEWAL_PREF',
    DidChangeRenewalStatus = 'DID_CHANGE_RENEWAL_STATUS',
    ValidatingInitialBuy = 'VALIDATING_INITIAL_BUY',
}

// INITIAL_BUY

// Occurs at the initial purchase of the subscription. Store the latest_receipt on your server as a token to verify the user’s subscription status at any time, by validating it with the App Store.

// CANCEL

// Indicates that the subscription was canceled either by Apple customer support or by the App Store when the user upgraded their subscription. The cancellation_date key contains the date and time when the subscription was canceled or upgraded.

// RENEWAL

// Indicates successful automatic renewal of an expired subscription that failed to renew in the past. Check subscription_expiraton_date to determine the next renewal date and time.

// INTERACTIVE_RENEWAL

// Indicates the customer renewed a subscription interactively, either by using your app’s interface, or on the App Store in account settings. Make service available immediately.

// DID_CHANGE_RENEWAL_PREF

// Indicates the customer made a change in their subscription plan that takes effect at the next renewal. The currently active plan is not affected.

// DID_CHANGE_RENEWAL_STATUS

// Indicates a change in the subscription renewal status. Check the timestamp for the data and time of the latest status update, and the auto_renew_status for the current renewal status.

export enum IOSSubscriptionExpirationIntent {
    CanceledByUser = 1, // User canceled the subscription
    CanceledBySystem = 2, // Billing error; for example customer’s payment information was no longer valid.
    PriceIncreaseDecline = 3, // Customer did not agree to a recent price increase
    ProductObsolete = 4, // Product was not available for purchase at the time of renewal.
    unknownError = 5, //  Unknown error
}

// This key is only present for auto-renewable subscription receipts.
// If the customer’s subscription failed to renew because the App Store was unable to complete the transaction,
// this value will reflect whether or not the App Store is still trying to renew the subscription.
export enum IOSSubscriptionRetryFlag {
    Active = 1, //  App Store has stopped attempting to renew the subscription
    Inactive = 0, // App Store is still attempting to renew the subscription
}

export enum IOSNotificationStatusType {
    success = 0,
    failure = 1,
    possibleHack = 2,
}

// This key is only present for auto-renewable subscription receipts, for active or expired subscriptions.
// The value for this key should not be interpreted as the customer’s subscription status.
// You can use this value to display an alternative subscription product in your app, for example,
// a lower level subscription plan that the customer can downgrade to from their current plan
export enum IOSSubscriptionAutoRenewStatus {
    Active = 1, // Subscription will renew at the end of the current subscription period
    Inactive = 0, // Customer has turned off automatic renewal for their subscription
}
