export enum ProgramCategoryType {
    General = 0,
    SportsNutrition,
    SportsPsychology,
    SportsScience
}