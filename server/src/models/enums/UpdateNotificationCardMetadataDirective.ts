export interface UpdateNotificationCardMetadataDirective {
    cardUid: string;
    athleteUid: string;
    notiticationCardType: NotificationCardType;
    cardCreationTimestamp: Date;
    cardAcknowledgeTimestamp: Date;
    actionType: NotificationCardMetadataActionType;
}
export enum NotificationCardMetadataActionType {
    UpdateAcknowledgmentCount = 'updateAcknowledgmentCount',
    UpdateImpressionCount = 'updateImpressionCount'
}
export enum NotificationCardType {
    Text = 'text',
    Image = 'image',
    Pdf = 'pdf',
    WebLink = 'weblink',
    Video = 'video'
}
