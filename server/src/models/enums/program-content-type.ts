export enum ProgramContentType {
    Article = 0,
    Video,
    Survey,
    Worksheet
}