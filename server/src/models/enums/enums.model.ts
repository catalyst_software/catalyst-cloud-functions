export enum OnboardingStateType {
    SplashScreen = 0,
    LoginTypeSelection = 1,
    LoginForgetPassword = 2,
    MainLogin = 3,
    SignUpEmail = 4,
    SignUpPassword = 5,
    SignUpProfile = 6,
    SignUpTermsAndConditions = 7,
    SignUpSurvey = 8,
    AthleteProfile = 9,
    Application = 10
}

export enum ProgramContentType {
    Article = 0,
    Video,
    Survey,
    Worksheet,
    Image
}


export enum ReportRequestEntityType {
    Organization = 'organization',
    Group = 'group',
    Athlete = 'athlete'
}

// export enum HealthComponentType {
//     Brain = 0, // Sleep and Mood
//     Body, // Fatigue and Pain
//     Food,  // Food
//     Training, // Training
//     IpScore,
//     Wearables,
//     Programs
// }

export enum WearableDataType {
    Steps = 0,
    Distance,
    HeartRate
}

export enum MoodLevelType {
    VeryPleasant = 0,
    Pleasant,
    Average,
    Unpleasant,
    VeryUnpleasant
}

export enum FatigueLevelType {
    NotFatigued = 0,
    ModeratelyFatigued,
    Fatigued,
    VeryFatigued,
    ExtremelyFatigued
}

// export enum PainType {
//     Injury = 0,
//     Training
// }



export enum PainLevelType {
    NoPain = 0,
    VeryLittle,
    Little,
    Painful,
    VeryPainful
}

export enum MealTimeType {
    Breakfast = 0,
    MorningSnack,
    Lunch,
    AfternoonSnack,
    Dinner
}

export enum SimpleFoodMealClassificationType {
    Unspecified = -1,
    Skipped = 1,
    Unhealthy = 3,
    Healthy = 5
}

export enum MealType {
    Breakfast = 0,
    Lunch,
    Dinner,
    Snack
}

export enum SportType {
    AFL = 0,
    AthleticsField,
    AthleticsTrack,
    Cardiovascular,
    CircuitTraining,
    Cycling,
    Diving,
    Soccer,
    Workout,
    Gymnastics,
    Hockey,
    Netball,
    Pilates,
    Recovery,
    Rugby,
    Swimming,
    Triathlon,
    WaterPolo,
    Weightlifting,
    Yoga,
    Other,
    BMX,
    Cricket,
    Skipping,
    SurfLifeSaving
}

export enum IntensityLevelType {
    Light = 1,
    Moderate = 2,
    Vigorous = 3
}

export enum PanelViewType {
    Daily = 'daily',
    Weekly = 'weekly',
    Monthly = 'monthly'
}

export enum ReportType {
    AthleteIpScoreDetailReport = 0,
    GroupIpScoreDetailReport = 1
}

export enum PushNotificationsType {
    DailyProgress = 'dailyProgress',
    Email = 'email',
    MyAthletes = 'myAthletes',
    Sync = 'sync',
    Download = 'download',
    Add = 'add',
    DatePicker = 'datePicker',
    Eye = 'eye',
    HappyFace = 'happyFace',
    SadFace = 'sadFace',
    Home = 'home',
    Chart = 'chart',
    Cross = 'cross',
    Envelope = 'envelope',
    ExitDoor = 'exitDoor',
    Gear = 'gear',
    LeftArrow = 'leftArrow',
    Avatar = 'avatar'
}

export enum IpScoreTrend {
    Decrease = -1,
    Neutral = 0,
    Increase = 1
}

export enum SubscriptionType {
    Free = 0,
    Basic,
    Premium
}
export enum SubscriptionPaymentType {
    PrePaid = 0,
    Monthly,
    Quarterly,
    Annual
}
export enum SubscriptionStatus {
    CanceledForceLogout = -1,
    Free,
    ValidInitial,
    ValidRenewed,
    Canceled,
    TrailPeriod,
}
export enum PurchaseProgramActionType {
    Subscribe = 0,
    Upgrade,
    KickItOff,
    Noop
}

