export enum PushNotificationType {
  ThanksForBasicSubscription = 0,
  ThanksForPremiumSubscription = 1,
  ProgramKickOff = 2,
  SubscriptionExpired = 3,
  SubscriptionCanceled = 4,
  SubscriptionRenewed = 5,
  SubscriptionLapsed = 6,
  SubscriptionRenewalFailed = 7,

  
  BiometricsMoodBad = 20,
  BiometricsMoodDistraught = 21,

  NewContentFeedItem = 40

}