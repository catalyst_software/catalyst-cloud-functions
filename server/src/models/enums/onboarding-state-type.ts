export enum OnboardingStateType {
    SplashScreen = 0,
    LoginTypeSelection = 1,
    LoginForgetPassword = 2,
    MainLogin = 3,
    SignUpEmail = 4,
    SignUpPassword = 5,
    SignUpProfile = 6,
    SignUpTermsAndConditions = 7,
    SignUpSurvey = 8
}