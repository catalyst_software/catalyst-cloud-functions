export enum SchedulingType {
    Hourly = 'Hourly',
    Quarterly = 'Quarterly',
    Minute = 'Minute'
}