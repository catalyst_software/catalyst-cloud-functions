// The type of notification can have the following values:

// The type of notification can have the following values:
export enum AndroidNotificationType {
    ValidatingInitialBuy = 0,
    SubscriptionRecovered = 1,
    SubscriptionRenewed = 2,
    SubscriptionCanceled = 3,
    SubscriptionPurchased = 4,
    SubscriptionOnHold = 5,
    SubscriptionInGracePeriod = 6,
    SubscriptionRestarted = 7,
    SubscriptionPriceChangeConfirmed = 8,
    SubscriptionDeferred = 9,
    SubscriptionRevoked = 12,
    SubscriptionExpired = 13,
}

export enum AndroidPaymentState {
    Pending = 0, // Payment pending
    Received = 1, // Payment received
    FreeTrial = 2, // Free trial
    PendingTypeChange = 3, // Pending deferred upgrade/downgrade
}

export enum AndroidCancelReason {
    CanceledByUser = 0, // User canceled the subscription
    CanceledBySystem = 1, // Subscription was canceled by the system, for example because of a billing problem
    ReplacedWithNewSubscription = 2, // Subscription was replaced with a new subscription
    CanceledByDeveloper = 3, // Subscription was canceled by the developer
}

export enum AndroidCancelSurveyReason {
    other = 0, // Other
    dontUseEnough = 1, // I don't use this service enough
    technicalIssues = 2, // Technical issues
    costRelatedReasons = 3, // Cost-related reasons
    foundBetterApp = 4, // I found a better app
}
export enum AndroidPurchaseType {
    Test = 0, // Test (i.e. purchased from a license testing account)
}

export interface AndroidCancelSurveyResult {
        cancelSurveyReason: AndroidCancelSurveyReason, // Check Interface
        userInputCancelReason: string,
    }
export interface AndroidPriceChange {
    newPrice: {
        priceMicros: string,
        currency: string,
    },
    // TODO: state types?!
    state: number,
}

export interface AndroidSubscriptionPurchase {
    kind: string,
    startTimeMillis: number,
    expiryTimeMillis: number,
    autoResumeTimeMillis: number, // Time at which the subscription will be automatically resumed, in milliseconds since the Epoch. Only present if the user has requested to pause the subscription.
    autoRenewing: boolean, // Whether the subscription will automatically be renewed when it reaches its current expiry time.
    priceCurrencyCode: string,
    priceAmountMicros: number,
    countryCode: string,
    paymentState: AndroidPaymentState,
    cancelReason: AndroidCancelReason, // Check Interface
    userCancellationTimeMillis: number,
    cancelSurveyResult: AndroidCancelSurveyResult, // Information provided by the user when they complete the subscription cancellation flow (cancellation reason survey).
    orderId: string,
    linkedPurchaseToken: string,
    purchaseType: number, // The type of purchase of the subscription. This field is only set if this purchase was not made using the standard in-app billing flow. Possible values are:
    priceChange: AndroidPriceChange,
    profileName: string,
    emailAddress: string,
    givenName: string,
    familyName: string,
    profileId: string,
}
