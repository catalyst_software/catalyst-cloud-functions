export enum OlapCubeEntityType {
    Athlete = 'athlete',
    Segment = 'segment'
}
