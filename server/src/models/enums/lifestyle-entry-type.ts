export enum LifestyleEntryType {
    // Brain
    Mood = 0, // Scalar
    Sleep, // Scalar
    // Body
    Fatigue, // Scalar
    Pain,
    // Food
    Food,
    // Training
    Training,

    IpScore = 6,

    Steps, // Scalar
    Distance, // Scalar
    HeartRate, // Scalar
    // RestingHeartRate

    // Additions
    Video,
    Survey,
    Worksheet,
    Period,
    Sick,
    SimpleFood,
    Weight,
    BodyMassIndex,
    Height,
    ActiveCaloriesBurned,
    RestingCaloriesBurned,
    BodyFatPercentage,
    LeanBodyMass,
    RestingHeartRate,
    HeartRateVariability,
    BloodGlucose,
    BloodPressure,
    BloodPressureSystolic,
    BloodPressureDiastolic,
    BloodOxygenSaturation,
    VO2Max,
    BodyTemperature,
    MindfulSession,
}

export enum StackDriverTemplateIDs {
    // Brain
    DoubleImageLayout = 'd-93756f51e55b4734a5b93d23e4310750', // Scalar
    TransactionEventLayout = 'd-2c90cdedb5c043168e859845898acf82', // Scalar
}
