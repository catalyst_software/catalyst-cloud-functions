
import { firestore } from 'firebase-admin';
import { AthleteIndicator } from '../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { Program } from '../../../db/programs/models/program.model';
import { OnboardingConfiguration } from '../../onboarding-configuration';
export interface Group {
    // Implementation
    uid?: string;
    groupId: string;
    groupName?: string;
    entityName?: string;
    creationTimestamp: firestore.Timestamp;

    // Extensions
    organizationId: string;
    organizationName: string;
    groupIdentifier?: string;

    currentPrograms?: Array<Program>;

    athletes?: Array<AthleteIndicator>

    logoUrl?: string;

    onboardingConfiguration?: OnboardingConfiguration;
    lastIpScoreValidationUpdate?: firestore.Timestamp;
    athleteIndex?: any;

}
