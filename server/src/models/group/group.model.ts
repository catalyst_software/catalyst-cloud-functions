
import { firestore } from 'firebase-admin';

import { Group as GroupInterface } from './interfaces/group';

import { AthleteIndicator } from '../../db/biometricEntries/well/interfaces/athlete-indicator';
import { Program } from '../../db/programs/models/program.model';
import { OnboardingConfiguration } from '../onboarding-configuration';

export class Group implements GroupInterface {
    // Overides
    readonly uid?: string;
    readonly groupId: string;
    readonly groupName?: string;
    groupIdentifier?: string;
    creationTimestamp: firestore.Timestamp;


    athletes?: Array<AthleteIndicator>;

    // Extensions - Groups Collection
    readonly organizationId: string;
    readonly organizationName: string;
    logoUrl?: string;

    currentPrograms?: Array<Program>;
    
    lastIpScoreValidationUpdate?: firestore.Timestamp;
    
    onboardingConfiguration?: OnboardingConfiguration;
    athleteIndex?: any;

    constructor(props: GroupInterface) {
        // super(props);
    }
}
