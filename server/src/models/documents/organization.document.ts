export const organizationDocument = {
    // unique key for example acme.inspire.org
    id: undefined,
    // quick human readable name for org - Acme Inc.
    name: '',
    // unique organizational code used for onboarding
    code: '',
    // number of licenses purchased
    licensesPurchased: 0,
    // number of licenses consumed
    licensesConsumed: 0,
    // contact person - first and last name
    contactPerson: '',
    // contact number
    contactNumber: ''
};

