export interface GeoPointInterface {
  latitude: number;
  longitude: number;
}
