export type PurchasedProductType = 'inapp' | 'subs';

export interface AthletePurchasedProgram {
    
    programName?: string;
    programUid?: string;

    productIdentifier: string;

    localizedTitle: string;
    localizedDescription: string;

    priceAmount: number;
    priceFormatted: string;
    priceCurrencyCode: string;

    productType: PurchasedProductType;

    subscriptionPeriod?: string;
    purchaseDate?: Date;
}
