
import { Athlete as AthleteInterface, OrganizationIndicator } from './interfaces/athlete';
import { AthleteMetadata } from './interfaces/athlete-metadata';
import { AthleteProfile } from './interfaces/athlete-profile';
import { IpScoreTracking } from '../../db/ipscore/models/documents/tracking/ip-score-tracking.interface';
import { IpScoreConfiguration } from '../../db/ipscore/models/documents/tracking/ip-score-configuration.interface';
import { AthletePurchasedProgram } from './athlete-purchased-program';
import { AthleteProvider } from './athlete-provider';
import { GroupIndicator } from '../../db/biometricEntries/well/interfaces/group-indicator';
import { AthleteAdditionalUserInfo } from './athlete-additional-user-info';
import { Program } from '../../db/programs/models/program.model';
import { OrganizationTool } from '../onboarding-configuration';
import { WhiteLabelTheme } from '../../api/app/v1/interfaces/WhiteLabelTheme';


export class Athlete implements AthleteInterface {
    entityName?: string;
    // Overides
    public uid: string;
    public email?: string;
    readonly emailVerified?: boolean;

    // Firebase User
    name?: string
    phoneNumber?: string
    anonymous: boolean
    isAnonymous: boolean
    profileImageURL?: string
    refreshToken?: string

    // Overides
    providers: Array<AthleteProvider>
    metadata: AthleteMetadata
    additionalUserInfo?: AthleteAdditionalUserInfo
    // Extensions
    organizationId?: string
    organizations: Array<OrganizationIndicator>;

    profile: AthleteProfile
    // Let's leave this as its a quick way to determine if logged in user
    // is a coach when running those security rule functions.
    isCoach?: boolean

    runningTotalIpScore?: number

    // Again quick reference when executing security rules in FS
    groups?: Array<GroupIndicator>
    includeGroupAggregation?: boolean
    claims?: any

    ipScoreConfiguration?: IpScoreConfiguration
    currentIpScoreTracking?: IpScoreTracking
    currentPrograms?: Array<Program>
    purchasedPrograms?: Array<AthletePurchasedProgram>
    transactionIds?: Array<string>

    originalSubscriptionTransactionIdentifier?: string

    // TODO: REMOVE!!!!!
    firebaseMessagingIds: Array<string>;

    userProperties: any;
    
    orgTools?: Array<OrganizationTool>;
    themes?: Array<WhiteLabelTheme>;

    constructor(props: AthleteInterface) {
        // super(props);
    }
}
