export interface AdditionalUserInfo {
    profile: Map<string, any>
    providerId: string
    username: string
    isNewUser: boolean
}

export interface AthleteAdditionalUserInfo extends AdditionalUserInfo {
    isNewUser: boolean
}
