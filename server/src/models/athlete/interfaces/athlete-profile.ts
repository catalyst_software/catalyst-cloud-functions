import { BodyComposition } from './athlete-body-composition'
import { AthleteSubscription } from './athlete-subscription'

export class AthleteProfile {
    email?: string
    firstName?: string
    lastName?: string
    fullName?: string
    bio?: string
    dob?: string
    sex?: string
    sport?: string
    location?: string
    onboardingCode?: string
    onboardingSurveryCompleted?: boolean
    imageUrl?: string
    subscription?: AthleteSubscription
    appSubscriptions?: AthleteSubscription[]
    bodyComposition?: BodyComposition

    // getFullName(): string {
    //     return `${this.firstName} ${this.lastName}`;
    // }
}
