import { AthleteClaims } from './athlete-claims';
import { firestore } from 'firebase-admin';
import { GeoPointInterface } from '../../geopoint.model';
import { WhiteLabelTheme } from '../../../api/app/v1/interfaces/WhiteLabelTheme';

export interface AthleteMetadata {
    registeredForMarketingCommunications?: boolean;
    creationTimestamp: firestore.Timestamp;
    lastSignInTimestamp?: firestore.Timestamp;
    // Extensions

    firebaseMessagingId?: string;
    claims?: AthleteClaims;
    onboardingLocation?: GeoPointInterface;
    userPropertiesValidationDate?: Date;

    // To Be Moved Here
    firebaseMessagingIds?: Array<string>;

    theme?: WhiteLabelTheme;




    storageRoot?: string;

    appConnections?: AthleteAppConnections;
    devices?: Array<AthleteDevice>;
}

export interface AthleteAppConnections {
    microsoft: boolean;
    connections?: Array<AthleteAppConnectionMetadata>;
}

export interface AthleteAppConnectionMetadata {
    type: AppConnectionType;
    status: AppConnectionStatus;
    name: string;
    description?: string;
    token?: string;
    lastTokenRefreshDate?: Date;
    scopes?: Array<string>;
    logoUrl?: string;
}

export enum AppConnectionType {
    Microsoft = 'microsoft'
}

export enum AppConnectionStatus {
    Connected = 'connected',
    Disconnected = 'disconnected'
}


export interface AthleteDevice {
    appVersion?: number;
    deviceType: string;
    manufacturer: string;
    model: string;
    os: string;
    uuid: string;
    isOriginalDevice?: boolean;
}

