import { firestore } from 'firebase-admin';

import { AthleteMetadata } from './athlete-metadata'
import { AthleteProfile } from './athlete-profile'
import { IpScoreTracking } from '../../../db/ipscore/models/documents/tracking/ip-score-tracking.interface'
import { IpScoreConfiguration } from '../../../db/ipscore/models/documents/tracking/ip-score-configuration.interface'
import { GroupIndicator } from '../../../db/biometricEntries/well/interfaces/group-indicator'
import { AthleteProvider, Provider } from '../athlete-provider'
import { AthleteAdditionalUserInfo, AdditionalUserInfo } from '../athlete-additional-user-info'
import { AthletePurchasedProgram } from '../athlete-purchased-program'
import { Program } from '../../../db/programs/models/program.model';
import { OrganizationTool } from '../../onboarding-configuration';


export interface OrganizationIndicator {
    groups?: GroupIndicator[];
    organizationId: string;
    organizationName: string;
    organizationCode?: string;
    active?: boolean;
    joinDate?: firestore.Timestamp;

    appIdentifier?: string;
    themeId?: string;
    orgTools?: OrganizationTool[];
}

export interface AthleteUserProperties {
    organizationId?: string;
    organizationName?: string;
    groupId?: string;
    groupName?: string;
    isCoach?: boolean;
    sport?: string;
    location?: string;
    dateOfBirth?: string;
    age?: number;
    lastValidationDate?: Date;
}

/**
 * The metadata of the user
 */
export interface UserMetadata {
    creationTimestamp: firestore.Timestamp;
    lastSignInTimestamp: firestore.Timestamp;
}

export interface User {
    uid: string;
    email?: string;
    emailVerified: boolean;
    name?: string;
    phoneNumber?: string;
    anonymous: boolean;
    isAnonymous: boolean; // This is used by the web API
    providers: Array<Provider>;
    profileImageURL?: string;
    metadata: UserMetadata;
    additionalUserInfo?: AdditionalUserInfo;
    /** iOS only */
    refreshToken?: string;

}
export interface Athlete {
    themes?: {uid: string} [];

    // Implementation
    uid: string
    entityName?: any;
    email?: string
    emailVerified?: boolean
    name?: string
    phoneNumber?: string
    anonymous?: boolean
    isAnonymous?: boolean
    profileImageURL?: string
    refreshToken?: string

    // Overides
    providers?: Array<AthleteProvider>
    metadata?: AthleteMetadata
    
    additionalUserInfo?: AthleteAdditionalUserInfo
    // Extensions
    organizationId?: string
    // organizationIds?: Array<string>
    // organizationName?: string;
    organizations?: Array<OrganizationIndicator>
    profile?: AthleteProfile
    // Let's leave this as its a quick way to determine if logged in user
    // is a coach when running those security rule functions.
    isCoach?: boolean
    ipScore?: number
    runningTotalIpScore?: number

    // Again quick reference when executing security rules in FS
    groups?: Array<GroupIndicator>
    includeGroupAggregation?: boolean
    claims?: any

    ipScoreConfiguration?: IpScoreConfiguration
    currentIpScoreTracking?: IpScoreTracking
    currentPrograms?: Array<Program>
    purchasedPrograms?: Array<AthletePurchasedProgram>
    transactionIds?: Array<string>

    originalSubscriptionTransactionIdentifier?: string;
    // userProperties?: AthleteUserProperties;
    
    // TODO: REMOVE!!!!!
    firebaseMessagingIds: Array<string>;
    orgTools?: Array<OrganizationTool>;

    forceThemeUpdate?: boolean
    themeId?: string;
}
