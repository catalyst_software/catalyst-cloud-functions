import { PurchasedProductType } from "../athlete-purchased-program";

export interface AthletePurchasedProgram {
    productIdentifier: string;
    localizedTitle: string;
    localizedDescription: string;
    priceAmount: number;
    priceFormatted: string;
    priceCurrencyCode: string;
    productType: PurchasedProductType;
    subscriptionPeriod?: string;
    purchaseDate?: Date;
    programName?: string;
    programUid?: string;
}
