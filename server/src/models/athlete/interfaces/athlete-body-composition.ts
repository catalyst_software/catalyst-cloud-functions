export interface BodyHeight {
    imperial: string
    metric: number
}

export interface BodyWeight {
    lbs: number
    kilos: number
}

export interface BodyComposition {
    height?: BodyHeight
    weight?: BodyWeight
    bmi?: number
}
