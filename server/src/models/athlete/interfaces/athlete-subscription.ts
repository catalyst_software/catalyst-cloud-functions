import { firestore } from 'firebase-admin';

import { SubscriptionType, SubscriptionStatus, SubscriptionPaymentType } from './../../enums/enums.model';

export interface AthleteSubscription {
    savedFrom?: string;

    type: SubscriptionType;
    status: SubscriptionStatus;
    paymentType?: SubscriptionPaymentType;
    commencementDate?: firestore.Timestamp;
    expirationDate?: firestore.Timestamp;
    originalTransaction?: Transaction;
    analytics?: AthleteSubscriptionAnalytics;
    isPrePaid?: boolean;
    appIdentifier?: string;
    productIdentifier?: string;
}

export interface AthleteSubscriptionAnalytics {
    pending?: boolean;
    action?: string;
    payload?: any;
}


export interface AthleteUpdateSubscriptionRequest {
    athleteUid: string;
    groupIds: Array<string>;
    subscription: AthleteSubscription;
    includeAthlete: boolean;
    includeGroups: boolean;
 }

export class Transaction {
    public nativeValue: any;

    public transactionState: string;
    public productIdentifier: string;
    public transactionIdentifier: string;
    public transactionDate: firestore.Timestamp;
    public transactionReceipt: string;
    public originalTransaction: Transaction;
    public dataSignature: string; /* Android Only */

    // constructor(nativeValue: any);
}

export class TransactionState {
    public static Purchased: string;
    public static Restored: string;
    public static Failed: string;
    public static Deferred: string;
    public static Purchasing: string;
    public static Refunded: string;
}