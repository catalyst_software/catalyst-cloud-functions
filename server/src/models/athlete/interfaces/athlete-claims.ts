export interface AthleteClaims {
    get(name: string): boolean;
}
