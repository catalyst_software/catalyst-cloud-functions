// import { EngagementAggregationInterface } from "./engagement-aggregate.model";
// import { AggregateDocumentType } from "../../enums/aggregate-document-type";

// export interface GroupWellnessEngagementWeeklyAggregateInterface extends EngagementAggregationInterface {
//     groupId?: string;
//     groupName?: string;
//     dailes?: Array<EngagementAggregationInterface>;
// }

// export class GroupWellnessEngagementWeeklyAggregate implements GroupWellnessEngagementWeeklyAggregateInterface {

//     id: string;
//     groupId: string;
//     groupName: string;
//     dailies: Array<EngagementAggregationInterface>;
//     organizationId: string;
//     aggregateDocumentType: AggregateDocumentType;
//     fromDate: Date;
//     toDate: Date;
//     weekNumber: number;
//     dailes: Array<EngagementAggregationInterface>;
//     sleepCount: number;
//     moodCount: number;
//     brainPercentage: number;
//     nutritionCount: number;
//     hydrationCount: number;
//     foodPercentage: number;
//     fatigueCount: number;
//     painCount: number;
//     bodyPercentage: number;
//     trainingCount: number;
//     trainingPercentage: number;
//     totalWellnessPercentage: number;

//     constructor(props: GroupWellnessEngagementWeeklyAggregateInterface) {
//         // super(props);
//     }
// }
