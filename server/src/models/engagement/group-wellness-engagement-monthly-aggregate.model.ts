// import { EngagementAggregationInterface } from "./engagement-aggregate.model";
// import { AggregateDocumentType } from "../enums/aggregate-document-type";


// export interface GroupWellnessEngagementMonthlyAggregateInterface extends EngagementAggregationInterface {
//     groupId?: string;
//     groupName?: string;
// }
// export class GroupWellnessEngagementMonthlyAggregate implements GroupWellnessEngagementMonthlyAggregateInterface {

//     id: string;
//     groupId: string;
//     groupName: string;
//     organizationId: string;
//     aggregateDocumentType: AggregateDocumentType;
//     fomDateTime?: Date;
//     eomDateTime?: Date;
//     sleepCount: number;
//     moodCount: number;
//     brainPercentage: number;
//     nutritionCount: number;
//     hydrationCount: number;
//     foodPercentage: number;
//     fatigueCount: number;
//     painCount: number;
//     bodyPercentage: number;
//     trainingCount: number;
//     trainingPercentage: number;
//     totalWellnessPercentage: number;

//     // constructor(props: GroupWellnessEngagementMonthlyAggregateInterface) {
//     //     // super(props);
//     // }
// }
