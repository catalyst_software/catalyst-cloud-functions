// import { EngagementAggregationInterface } from './engagement-aggregate.model';
// import { AggregateDocumentType } from "../../enums/aggregate-document-type";

// export interface AthleteWellnessEngagementWeeklyAggregateInterface extends EngagementAggregationInterface {
//     userId?: string;
//     fullName?: string;
//     dailies?: Array<EngagementAggregationInterface>;
// }

// export class AthleteWellnessEngagementWeeklyAggregate
//     implements AthleteWellnessEngagementWeeklyAggregateInterface {

//     id: string;
//     userId: string;
//     fullName: string;
//     organizationId: string;
//     aggregateDocumentType: AggregateDocumentType;
//     fromDate: Date;
//     toDate: Date;
//     weekNumber: number;
//     dailies: Array<EngagementAggregationInterface>;
//     sleepCount: number;
//     moodCount: number;
//     brainPercentage: number;
//     nutritionCount: number;
//     hydrationCount: number;
//     foodPercentage: number;
//     fatigueCount: number;
//     painCount: number;
//     bodyPercentage: number;
//     trainingCount: number;
//     trainingPercentage: number;
//     totalWellnessPercentage: number;

//     // constructor(props: AthleteWellnessEngagementWeeklyAggregateInterface) {
//     //     // super(props);
//     // }
// }

