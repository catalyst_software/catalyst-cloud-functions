
import { PlayStoreNotification as PlayStoreNotificationInterface, PlayStoreSubscriptionNotification } from './interfaces/play-store-notification';
import { AndroidNotificationType } from '../enums/android-notification-type';

export class PlayStoreNotification implements PlayStoreNotificationInterface {
    packageName: string;
    eventTimeMillis: number;
    subscriptionNotification: PlayStoreSubscriptionNotification;
    version: string;
    notificationType: AndroidNotificationType;
    purchaseToken: string;
    subscriptionId: string;

    constructor(props: PlayStoreNotificationInterface ) {
        // super(props);
    }
}

export interface PlayStoreNotificationResponse {
    version: string;
    notificationType: AndroidNotificationType;
    purchaseToken: string;
    subscriptionId: string;
}

export interface PlayStoreNotificationRequest {
    version: string;
    notificationType: AndroidNotificationType;
    purchaseToken: string;
    subscriptionId: string;
}
