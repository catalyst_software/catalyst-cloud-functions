import { AndroidNotificationType } from '../../enums/android-notification-type'

export interface PlayStoreSubscriptionNotification {
    version: string
    notificationType: AndroidNotificationType
    purchaseToken: string
    subscriptionId: string
}

export interface PlayStoreSubscriptionNotification {
    version: string
    notificationType: AndroidNotificationType
    purchaseToken: string
    subscriptionId: string
    // version: '1.0'
    // notificationType: 4
    // purchaseToken: 'bjhinehaofkabfacbeiokahm.AO-J1OyUXVxZXuu_hyrP86ttSRSO9pLFvOS1srR5OnqIKaCFiZtMg-0EoJgqsUi_EEa6Y1IU9_-nmhhDPYIDMvLDPmWBbDr3WS6x6sq3dT5Yb0Gu33wEMwKQqGiZ6wCljP2mPtdEPhOTkw69iJ_HsgZqK3V-xH8I9xvRlSd1Yk-x3lw3ncdT4Sw'
    // subscriptionId: 'com.inspiresportonline.dev.subscriptions'
}

export interface PlayStoreNotification {
    version: string
    notificationType: AndroidNotificationType
    purchaseToken: string
    subscriptionId: string

    packageName: string; //'com.inspiresportonline.dev'
    eventTimeMillis: number; // '1551555493865'
    subscriptionNotification: PlayStoreSubscriptionNotification
}
