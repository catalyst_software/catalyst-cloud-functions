import { CalendarEventInvitationStatus } from "../events";
export interface CalendarEventAttendee {
    uid?: string;
    photoUrl?: string;
    eventColor?: string;
    fullName?: string;
    subText?: string;
    subTextStyle?: string;
    firstLetter?: string;
    firstName: string;
    lastName: string;
    emailAddress: string;
    isOrganizer: boolean;
    invitationStatus: CalendarEventInvitationStatus;
}
