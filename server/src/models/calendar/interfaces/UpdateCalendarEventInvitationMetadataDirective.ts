import { CalendarEventInvitationStatus } from "../events";

export interface UpdateCalendarEventInvitationMetadataDirective {
    cardUid: string;
    attendeeUid: string;
    eventUid: string;
    status: CalendarEventInvitationStatus;
}
