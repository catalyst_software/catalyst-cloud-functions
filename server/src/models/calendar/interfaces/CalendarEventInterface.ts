import { CalendarEvent } from "./CalendarEvent";
// import { CalendarEvent } from './CalendarEventInterface';
export interface CalendarEventInterface extends CalendarEvent {
    location: string;
}
