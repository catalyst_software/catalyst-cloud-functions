import { ReportRequestEntityType } from "../../enums/enums.model";
import { CalendarEventReminderFrequencyType } from "../events";
import { CalendarEventInterface } from "./CalendarEventInterface";
import { CalendarEventAttendee } from "./CalendarEventAttendee";


export type EventColourVariants = {
    primary: string;
    secondary: string;
};

export interface ICalendarEvent extends CalendarEventInterface {
    // Taken from CalendarEvent in nativescript-ui-calendar
    title: string;
    startDate: Date;
    endDate: Date;
    isAllDay?: boolean;
    eventColor?: EventColourVariants;
    appColor?: any;
    // Custom Properties
    uid?: string;
    description?: string;
    channelType: ReportRequestEntityType; //ChannelType;
    entityId: string;
    entityName?: string;
    isActive: boolean;
    creatorUid: string;
    creatorName: string;
    creationTimestamp: Date;
    reminderFrequencies?: Array<CalendarEventReminderFrequencyType>;
    attendees?: Array<CalendarEventAttendee>;
}
