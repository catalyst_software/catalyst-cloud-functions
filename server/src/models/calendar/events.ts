
export const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3',
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF',
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA',
    },
};


export enum CalendarEventReminderFrequencyType {
    TenMinutesBefore = 'tenMinutesBefore',
    FifteenMinutesBefore = 'fifteenMinutesBefore',
    ThirtyMinutesBefore = 'thirtyMinutesBefore',
    OneHourBefore = 'oneHourBefore',
    TwoHoursBefore = 'twoHoursBefore',
    OneDayBefore = 'oneDayBefore'
}

export enum CalendarEventInvitationStatus {
    Pending = 'pending',
    Accepted = 'accepted',
    Declined = 'declined',
    Maybe = 'maybe'
}
