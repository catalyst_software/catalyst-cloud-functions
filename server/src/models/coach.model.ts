export interface CoachInterface {
    uid: string;
    emailAddress?: string;
    firstName?: string;
    lastName?: string;
    mobileNumber?: string;
}

export class Coach implements CoachInterface {
    uid: string;
    emailAddress: string;
    firstName: string;
    lastName: string;
    mobileNumber: string;

    // constructor(props: CoachInterface) {
    //     // super(props);
    // }
}
