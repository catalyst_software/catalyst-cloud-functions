export interface AndroidReceipt {
    productId: string;
    packageName?: string; // appIdentifier // TODO
    uid?: string;
    autoRenewing: boolean;
    cancelReason: AndroidReceipt | number;
    countryCode: string;
    expiryTimeMillis: number;
    expiryTime: Date;
    kind: string;
    orderId: string;
    originalOrderId: string;
    priceAmountMicros: number;
    priceCurrencyCode: string;
    purchaseType: number;
    startTimeMillis: number;
    startTime: Date;

    transactionId: string;
    originalTransactionId: string;
    isExpired: boolean;
    paymentState?: number
}
