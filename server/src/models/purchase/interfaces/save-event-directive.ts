import { AndroidNotificationType } from '../../enums/android-notification-type';
import { IOSNotificationType } from '../../enums/ios-notification-type';
import { AndroidReceipt } from "./android-receipt";
import { IOSReceipt } from "./ios-receipt";
import { PlatformType } from '../enums/platfom_type';
export interface SaveEventDirective {
    uid?: string;
    platform: PlatformType;
    transactionId: string;
    originalTransactionId: string;
    startDate: Date;
    expirationDate: Date;
    notificationType: AndroidNotificationType | IOSNotificationType;
    linkedPurchaseToken?: string;
    webOrderLineItemId?: string;
    androidReceipt?: AndroidReceipt;
    iosReceipt?: IOSReceipt;
}
