import { LifestyleEntryType } from "../../enums/lifestyle-entry-type";
import { RedFlagType } from "../../../api/app/v1/sendGrid/conrollers/email";

export interface SetRedFlagResloveStateRequest {
    athleteUid: string;
    creationTimestamp: string;
    flag: LifestyleEntryType;
    flagType: RedFlagType;
    redFlagDismissed: boolean;
    userFullName: string;
    value: number;
}

