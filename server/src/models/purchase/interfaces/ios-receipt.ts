// import { IOSPurchaseData } from "../purchase.model";
import { PlatformType } from "../enums/platfom_type";

export interface IOSReceipt {
    uid?: string;
    platform: PlatformType
    transactionId: number
    originalTransactionId: number
    expirationDate: Date
    // appItemId: number
    bundleId: string
    // cancellationDate: Date
    // isTrial: boolean
    itemId: number
    originalPurchaseDate: Date
    productId: string
    purchaseDate: Date
    webOrderLineItemId? : number
    unhandledProps?: any
    // quantity: number

    uniqueIdentifier?: string;
    uniqueVendorIdentifier?: string;
    isTrialPeriod?: boolean
    isInIntroOfferPeriod?:boolean

    expiredReceipt?: any
}
