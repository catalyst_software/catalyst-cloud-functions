export interface TransactionReceiptValidationRequest {
    athleteUid: string;
    platform: string; // 'iOS | Android'
    productIdentifier: string;
    transactionDate: Date;
    transactionIdentifier: string;
    transactionReceipt: string;
    packageName?: string;
    priceFormatted?: string;
    priceCurrencyCode?: string;

    useReceiptFromDb?: boolean;
    receiptID?: string;
    saveReceipt?: boolean
}
