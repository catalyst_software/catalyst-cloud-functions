export interface Purchase {

    // Implementation
    uid: string;
	athleteUid?: string;
	platform?: string; // 'iOS | Android'
	dateTime?: Date;
	productIdentifier?: string;
	transactionReceipt?: string;    
}
