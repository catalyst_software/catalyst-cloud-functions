import { Purchase as PurchaseInterface } from './interfaces/purchase'
import { IOSSubscriptionRetryFlag } from '../enums/ios-notification-type';

export class Purchase implements PurchaseInterface {
    // Overides
    readonly uid: string
    athleteUid: string
    platform: string // 'iOS | Android'
    dateTime: Date
    productIdentifier: string
    transactionReceipt: string

    constructor(props: PurchaseInterface) {
        // super(props);
    }
}

// TODO:
export class AndroidPurchaseReceipt {
    // implements PurchaseInterface {
    // Overides
    // readonly uid: string

    isSuccessful: boolean
    errorMessage: any
    payload: {

        acknowledgementState: number; // 
        acknowledgementStateString: string;
        kind: string // 'androidpublisher#subscriptionPurchase'
        startTimeMillis: number // '1551039170322'
        expiryTimeMillis: number //'1551041266268'
        autoRenewing: boolean
        priceCurrencyCode: string // 'EUR'
        priceAmountMicros: number // '1250000'
        countryCode: string // 'MT'
        paymentState: number;
        paymentStateString: string;
        cancelReason: number // 1
        orderId: string // 'GPA.3340-2776-4142-42066..5'
        purchaseType: number // 0
        purchaseTypeString: string;
        productId: string; // com.inspiresportonline.prod.subscriptions.monthly.basic
        packageName: string; // // com.inspiresportonline.dev
        // TODO: Is this a actual prop????
        linkedPurchaseToken?: any
    }

    constructor(props: PurchaseInterface) {
        // super(props);
    }
}

export class IOSPurchaseReceipt {
    auto_renew_product_id: string
    auto_renew_status: string
    expiration_intent: string
    is_in_billing_retry_period: IOSSubscriptionRetryFlag
    latest_expired_receipt_info: any
    receipt: any
    sandbox: boolean
    service: string
    status: string

    constructor(props: PurchaseInterface) {
        // super(props);
    }
}

export class IOSPurchaseData {
    transactionId: string
    originalTransactionId: string
    expirationDate: Date
    appItemId: number
    bundleId: string
    cancellationDate: Date
    isTrial: boolean
    originalPurchaseDate: Date
    productId: string
    purchaseDate: Date
    quantity: number

    constructor(props: PurchaseInterface) {
        // super(props);
    }
}

export interface TransactionReceiptValidationResponse {
    uid?: string
    athleteUid?: string
    // platform: string; // 'iOS | Android'
    // productIdentifier: string;
    // transactionDate: Date;
    // transactionIdentifier: string;
    // transactionReceipt: string;
    isValid: boolean
    message: string
    error?: any

    expirationDate?: Date
    validatedData?: any
    validatedDatas?: {
        auto_renew_status: number
        latest_expired_receipt_info: {
            original_purchase_date_pst: Date
            unique_identifier: string
            // original_transaction_id: '1000000503436015'
            // expires_date: '1550862111000'
            // transaction_id: '1000000504956688'
            quantity: number
            product_id: string
            bvrs: number
            bid: string
            unique_vendor_identifier: string
            web_order_line_item_id: number
            // original_purchase_date_ms: '1550505852000'
            expires_date_formatted: Date
            purchase_date: Date
            is_in_intro_offer_period: boolean
            // purchase_date_ms: '1550861811000'
            expires_date_formatted_pst: Date
            is_trial_period: boolean
            // purchase_date_pst: '2019-02-22 10:56:51 America/Los_Angeles'
            original_purchase_date: Date
            item_id: number

            original_purchase_date_ms: Date
            purchase_date_ms: Date
            original_transaction_id: string
            expires_date: Date
            transaction_id: string
            purchase_date_pst: Date

            cancellation_date_ms: Date // ADDITION TO AppleStoreNotification
            cancellation_reason: number // ADDITION TO AppleStoreNotification
            app_item_id: string // ADDITION TO AppleStoreNotification
            version_external_identifier: string // ADDITION TO AppleStoreNotification
            cancellation_date: Date // ADDITION TO AppleStoreNotification
            cancellation_date_pst: Date // ADDITION TO AppleStoreNotification
        }
        status: 0
        auto_renew_product_id: string //'com.inspiresportonline.dev.subscriptions'
        receipt: {
            // original_purchase_date_pst: '2019-02-18 08:04:12 America/Los_Angeles'
            // quantity: '1'
            // unique_vendor_identifier: '8D60AD58-BB01-41CA-A684-E14CBB3EB6B4'
            // bvrs: '1.9'
            // expires_date_formatted: '2019-02-19 17:33:45 Etc/GMT'
            // is_in_intro_offer_period: 'false'
            // purchase_date_ms: '1550597325215'
            // expires_date_formatted_pst: '2019-02-19 09:33:45 America/Los_Angeles'
            // is_trial_period: 'false'
            // item_id: '1453012404'
            // unique_identifier: '90518714aea0d8d20c8eba0724ffaaf0b345990b'
            // original_transaction_id: '1000000503436015'
            // expires_date: '1550597625215'
            // transaction_id: '1000000503804057'
            // web_order_line_item_id: '1000000042815947'
            // version_external_identifier: '0'
            // bid: 'com.inspiresportonline.dev'
            // product_id: 'com.inspiresportonline.dev.subscriptions'
            // purchase_date: '2019-02-19 17:28:45 Etc/GMT'
            // original_purchase_date: '2019-02-18 16:04:12 Etc/GMT'
            // purchase_date_pst: '2019-02-19 09:28:45 America/Los_Angeles'
            // original_purchase_date_ms: '1550505852000'

            original_purchase_date_pst: Date
            unique_identifier: string
            // original_transaction_id: '1000000503436015'
            // expires_date: '1550862111000'
            // transaction_id: '1000000504956688'
            quantity: number
            product_id: string
            bvrs: number
            bid: string
            unique_vendor_identifier: string
            web_order_line_item_id: number
            // original_purchase_date_ms: '1550505852000'
            expires_date_formatted: Date
            purchase_date: Date
            is_in_intro_offer_period: boolean
            // purchase_date_ms: '1550861811000'
            expires_date_formatted_pst: Date
            is_trial_period: boolean
            // purchase_date_pst: '2019-02-22 10:56:51 America/Los_Angeles'
            original_purchase_date: Date
            item_id: number

            original_purchase_date_ms: Date
            purchase_date_ms: Date
            original_transaction_id: string
            expires_date: Date
            transaction_id: string
            purchase_date_pst: Date

            version_external_identifier: string // ADDITION TO AppleStoreNotification
        }
        expiration_intent: number
        is_in_billing_retry_period: number
        sandbox: boolean
        service: string //'ios'
    }
    purchaseData?: any
    purchaseDatas?: {
        bundleId: string
        appItemId: number
        originalTransactionId: number
        transactionId: number
        productId: string
        originalPurchaseDate: number
        purchaseDate: number
        quantity: 1
        expirationDate: number
        isTrial: boolean
        cancellationDate: any// 0
    }
}


