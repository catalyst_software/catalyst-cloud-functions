import { HeaderType } from "./red-flag-reports";
export interface RedFlagSchemaOld {
    header: {
        fromDate: string;
        toDate: string;
        headerType: HeaderType;
        title: {
            organization: string;
            group: string;
            athlete: string;
        };
    };
    "superstars": {
        "include": boolean;
        "data": {
            "totalEngagements": number;
            "engagementSuperstars": Array<{
                "name": string;
                "value": number;
            }>;
        };
    };
    "training": {
        "include": boolean;
        "data": {
            "averageTrainingMinutes": Array<{
                "name": string;
                "date": number;
                "value": number;
            }>;
            "minutesPerIntensity": Array<{
                "name": string;
                "value": number;
            }>;
            "minutesPerSport": Array<{
                "name": string;
                "value": number;
            }>;
        };
    };
    "pain": {
        "include": boolean;
        "data": {
            "averagePainLevelByAthlete": Array<{
                "name": string;
                "date": number;
                "value": number;
            }>;
            "painLevelsByBodyLocation": Array<{
                "name": string;
                "value": number;
            }>;
            "painLevelsByType": Array<{
                "name": string;
                "value": number;
            }>;
        };
    };
    "painInjuryDetail": {
        "include": boolean;
        "data": {
            "athletes": Array<{
                "name": string;
                "date": number;
                "bodyLocation": string;
                "type": string;
                "painLevel": number;
            }>;
        };
    };
    brainSummary: {
        include: boolean;
        data: {
            mood: {
                average: string;
                athletes: Array<{
                    name: string;
                    value: number;
                }>;
                sumOfMoodEntries: any;
            };
            happyChappies: any;
            sleep: {
                average: string;
                athletes: Array<{
                    name: string;
                    value: number;
                }>;
            };
        };
    };
    bodySummary: {
        include: boolean;
        data: {
            "fatigue": {
                "average": string;
                "athletes": Array<{
                    "name": string;
                    "value": number;
                }>;
            };
            "pain": {
                "average": string;
                "athletes": Array<{
                    "name": string;
                    "value": number;
                }>;
            };
        };
    };
    foodSummary: {
        include: boolean;
        data: {
            "mealTypeAverages": Array<{
                "mealType": string;
                "avg": number;
            }>;
            "foodTypeCounts": Array<{
                "foodType": string;
                "count": number;
            }>;
        };
    };
    trainingSummary: {
        include: boolean;
        data: {
            training: {
                "totalTrainingMinutes": number;
                "averageSessionLength": number;
            };
            intensityLevel: [{
                "vigorous": 11;
                "moderate": 23;
            }];
            sportTypes: Array<{
                "sport": string;
                "value": number;
            }>;
            trainingSuperstars: Array<{
                "name": string;
                "value": number;
            }>;
        };
    };
    engagementSummary: {
        include: boolean;
        data: {
            totalEngagements: any;
            engagementSuperstars: any;
        };
    };
    moodTrends: {
        include: boolean;
        data: {
            "moodHistory": Array<{
                "name": string;
                "date": number;
                "value": number;
            }>;
            "averageMood": Array<{
                "name": string;
                "value": number;
            }>;
        };
    };
    sleepTrends: {
        include: boolean;
        data: {
            "sleepHistory": Array<{
                "name": string;
                "date": number;
                "value": number;
            }>;
            "averageSleep": Array<{
                "name": string;
                "value": number;
            }>;
        };
    };
    fatigueTrends: {
        include: boolean;
        data: {
            "fatigueHistory": Array<{
                "name": string;
                "date": number;
                "value": number;
            }>;
            "averageFatigue": Array<{
                "name": string;
                "value": number;
            }>;
        };
    };
    trainingTrends: {
        include: boolean;
        data: any;
    };
    painTrends: {
        include: boolean;
        data: any;
    };
}
