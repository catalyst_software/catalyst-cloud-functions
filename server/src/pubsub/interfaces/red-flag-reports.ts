import { DetailedChartMeta2 } from "../../api/app/v1/controllers/utils/reports/helpers/template-sections/types/DetailedChartMeta2";
import { DetailedChartMeta } from "../../api/app/v1/controllers/utils/reports/helpers/template-sections/types/DetailedChartMeta";

export enum ReportType {
    RedFlag = 'redFlag'
}
export enum HeaderType {
    Organization = 'organization',
    Group = 'group',
    Athlete = 'athlete',
}

export interface ReportRequestPayload {
    reportType: ReportType;
    organizationId: string;
    groupId?: string;
    athleteId?: string;
    fromDate: Date;
    toDate: Date;
    emailAddresses: Array<string>;
    schema: {
        brainSummary: boolean;
        bodySummary: boolean;
        foodSummary: boolean;
        trainingSummary: boolean;
        engagementSummary: boolean;
        moodTrends: boolean;
        sleepTrends: boolean;
        fatigueTrends: boolean;
        trainingTrends: boolean;
        painTrends: boolean;
    }
}

export enum ChartType {
    Bar = 'bar',
    HorizontalBar = 'horizontalBar',
    Pie = 'pie',
    PolarArea = 'polarArea',
    Line = 'line'
}

export enum ChartOptions {
    TrendsMixed = 'optionsTrendsMixedChart',
    PolarArea = 'optionsPolarAreaChart',
    Pie = 'optionsPieChart',
    Stacked = 'optionsStackedBarChart',
}

export interface RedFlagReportSchema {
    "header": {
        "fromDate": string;
        "toDate": string;
        "headerType": string;
        "title": {
            "organization": string;
            "team": string;
            "group": string;
            "athlete": string;
        }
    },

    redFlags: {

        brainSummary: {
            include: boolean;
            "data": {
                "useBarChart": boolean;
                "summaryData": {
                    "title": string;
                    "dataSets": Array<
                        [{
                            "subject": string;
                            "value": number;
                        }]
                    >
                },
                "redFlagGroups": Array<
                    {
                        "image": string;
                        "label": string;
                        "chartType": ChartType;
                        "chartData": {
                            "labels": string[];
                            "datasets": Array<
                                {
                                    "data": number[];
                                    "backgroundColor": string[];
                                }
                            >
                        }
                    }
                >
            }
        },
        "bodySummary": {
            "include": boolean;
            "data": {
                "useBarChart": boolean;
                "summaryData": {
                    "title": string;
                    "dataSets": Array<
                        [{
                            "subject": string;
                            "value": number;
                        }]
                    >
                },
                "redFlagGroups": Array<
                    {
                        "image": string;
                        "label": string;
                        "chartType": ChartType;
                        "chartData": {
                            "labels": string[];
                            "datasets": [
                                {
                                    "data": number[];
                                    "backgroundColor": string[];
                                }
                            ]
                        }
                    }
                >
            }
        },
        "foodSummary": {
            "include": boolean;
            "data": {
                "usePieChart": boolean;
                "summaryData": {
                    "title": string;
                    "heading": string;
                    "dataSets": Array<
                        Array<{
                            "subject": string;
                            "value": number;
                        }>
                    >
                },
                "pieChartData": {
                    "label": string;
                    "chartType": ChartType;
                    "chartData": {
                        "labels": string[];
                        "datasets": Array<{
                            "data": number[];
                            "backgroundColor": string[];
                        }>
                    }
                }
            }
        },
        "trainingSummary": {
            "include": boolean;
            "data": {
                "usePieChart": boolean;
                "summaryData": {
                    "title": string;
                    "dataSets": [
                        [{
                            "subject": string;
                            "value": number;

                        }]
                    ]
                },
                "pieChartData": {
                    "label": string;
                    "chartType": ChartType;
                    "chartData": {
                        "labels": string[];
                        "datasets": [{
                            "data": number[];
                            "backgroundColor": string[];
                        }]
                    }
                }
            }
        }
    },

    "superstars": {
        "include": boolean;
        "data": {
            "superstarsGroups": Array<
                {
                    "label": string;
                    "isSuperstars": boolean;
                    "chartType": ChartType;
                    "chartData": {
                        "labels": string[];
                        "datasets": [{
                            "data": number[];
                            "backgroundColor": string[];
                        }]
                    }
                }
            >
        }
    },

    trends: Array<
        {
            type: string;
            "include": boolean;
            "data": {
                "isTrends": boolean;
                "lineChart": {
                    "chartType": ChartType;
                    "chartData": {
                        "labels": string[];
                        "datasets": Array<
                            {
                                "data": number[];
                                "label": string;
                                "borderColor"?: string;
                                "fill": boolean;
                                "borderJoinStyle": string;
                            }
                        >
                    }
                },
                "barChart": {
                    "chartType": ChartType;
                    "chartData": {
                        "labels": string[];
                        "datasets": [{
                            "data": number[];
                            "backgroundColor": string[];
                        }]
                    }
                }
            }
        }
    >;

    "detailed": {
        "training": {
            "include": boolean;
            "data": {
                "title": string;

                "detailedChartMeta": DetailedChartMeta,

                "stackedChartMeta": {
                    title: string;
                    chartType: ChartType;
                    chartOptions: ChartOptions;
                    chartLabel?: string;
                    chartData: {
                        labels: string[];
                        datasets: Array<{
                            label: string;
                            data: Array<number>;
                            backgroundColor: string;
                        }>;
                    };
                },
                "polarChartMeta": {
                    title: string;
                    chartType: ChartType;
                    chartOptions: ChartOptions,
                    chartData: {
                        datasets: [{
                            label: string, // TODO Check This
                            data: number[],
                            backgroundColor: string | Array<string>,
                        }],
                        labels: string[]
                    }
                },
                "pieChartMeta": {
                    title: string;
                    chartType: ChartType;
                    chartOptions: ChartOptions,
                    chartData: {
                        labels: string[],
                        datasets: [{
                            data: number[],
                            backgroundColor: string | Array<string>,
                        }]
                    }
                }
            }
        },
        "pain": {
            "include": boolean;
            "data": {
                "title": string;

                "detailedChartMeta": DetailedChartMeta2,

                "stackedChartMeta": {
                    title: string;
                    chartType: ChartType;
                    chartOptions: ChartOptions;
                    chartLabel?: string;
                    chartData: {
                        labels: string[];
                        datasets: Array<{
                            label: string;
                            data: Array<number>;
                            backgroundColor: string;
                        }>;
                    };
                },
                "polarChartMeta": {
                    title: string;
                    chartType: ChartType;
                    chartOptions: ChartOptions,
                    chartData: {
                        datasets: [{
                            label: string, // TODO Check This
                            data: number[],
                            backgroundColor: string | Array<string>,
                        }],
                        labels: string[]
                    }
                },
                "pieChartMeta": {
                    title: string;
                    chartType: ChartType;
                    chartOptions: ChartOptions,
                    chartData: {
                        labels: string[],
                        datasets: [{
                            data: number[],
                            backgroundColor: string | Array<string>,
                        }]
                    }
                },

                "painInjuryDetail": {
                    "include": boolean;
                    "data": {
                        "athletes": Array<
                            {
                                "name": string;
                                "date": string;
                                "bodyLocation": string;
                                "type": string;
                                "painLevel": number;
                            }
                        >
                    }
                }
            }
        }
    },
    daysOfWeek: {
        date: Date;
        uid?: string;
        lineChartData: any;
        entries: BiometricEntrySchema[]
    }[]
}

export interface RedFlagSchema {



    "brainSummary": {
        "include": boolean;
        "data": {
            "useBarChart": boolean;
            "summaryData": {
                "title": string;
                "dataSets": Array<
                    [{
                        "subject": string;
                        "value": number;
                    }]
                >
            },
            "redFlagGroups": Array<
                {
                    "image": string;
                    "label": string;
                    "chartType": ChartType;
                    "chartData": {
                        "labels": string[];
                        "datasets": [
                            {
                                "data": number[];
                                "backgroundColor": string[];
                            }
                        ]
                    }
                }
            >
        }
    },
    "bodySummary": {
        "include": boolean;
        "data": {
            "useBarChart": boolean;
            "summaryData": {
                "title": string;
                "dataSets": Array<
                    [{
                        "subject": string;
                        "value": number;
                    }]
                >
            },
            "redFlagGroups": Array<
                {
                    "image": string;
                    "label": string;
                    "chartType": ChartType;
                    "chartData": {
                        "labels": string[];
                        "datasets": [
                            {
                                "data": number[];
                                "backgroundColor": string[];
                            }
                        ]
                    }
                }
            >
        }
    },
    "foodSummary": {
        "include": boolean;
        "data": {
            "usePieChart": boolean;
            "summaryData": {
                "title": string;
                "heading": string;
                "dataSets": Array<
                    Array<{
                        "subject": string;
                        "value": number;
                    }>
                >
            },
            "pieChartData": {
                "label": string;
                "chartType": ChartType;
                "chartData": {
                    "labels": string[];
                    "datasets": Array<{
                        "data": number[];
                        "backgroundColor": string[];
                    }>
                }
            }
        }
    },
    "trainingSummary": {
        "include": boolean;
        "data": {
            "usePieChart": boolean;
            "summaryData": {
                "title": string;
                "dataSets": [
                    [{
                        "subject": string;
                        "value": number;

                    }]
                ]
            },
            "pieChartData": {
                "label": string;
                "chartType": ChartType;
                "chartData": {
                    "labels": string[];
                    "datasets": [{
                        "data": number[];
                        "backgroundColor": string[];
                    }]
                }
            }
        }
    }


}

export interface RedFlagReportData {
    reportType: ReportType;
    organizationId: string;
    groupId?: string;
    athleteId?: string;
    fromDate: Date;
    toDate: Date;
    emailAddresses: string;
    schema: RedFlagReportSchema;

}

export interface BiometricEntrySchema {
    totalValue: number;

    analysticsFamily?: string;
    eventName?: string;

    lifestyleEntryType: string;
    lifestyleEntryTypeID?: number;

    name: string;
    userFirstName: string;
    userLastName: string;
    userFullName: string;

    organizationID?: string;

    groupID?: string;
    groupIDs?: string;

    athleteID: string;

    mealType?: string;
    foodType?: string;

    painType?: string;
    painTypeID?: number;
    bodyLocation?: string;
    bodyLocationID?: number;

    sportType?: string;
    sportTypeID?: number;
    intensityLevel?: string;

    value: number;
    highValue?: number;
    lowValue?: number;
    meanValue?: number;
    sumValue?: number;
    countValue?: number;

    creationTimestamp: string;
}

export interface BiometricScalarEntrySchema extends BiometricEntrySchema {
    value: number;
}

export interface BiometricMoodEntrySchema extends BiometricEntrySchema {
    type: 'pain',
    painType: string;
    painTypeID: number;
    bodyLocation: string;
    bodyLocationID: number;
}

export interface BiometricPainEntrySchema extends BiometricEntrySchema {
    type: 'pain',
    painType: string;
    painTypeID: number;
    bodyLocation: string;
    bodyLocationID: number;
}

export interface BiometricFoodEntrySchema extends BiometricEntrySchema {
    type: 'food',
    mealType: string;
    foodType: string;
}
export interface BiometricTrainingEntrySchema extends BiometricEntrySchema {
    type: 'training',
    sportType: string;
    sportTypeID: number;
    intensityLevel: string;
}

export interface RedflagReportsTrainingEntrySchema extends BiometricEntrySchema {
    uid: string;
    firstName: string;
    FullName: string;
    lowValue: number;
    highValue: number;
    meanValue: number;
    countValue: number;
}

export type BiometricEntryIndicator = {
    creationTimestamp: string;
    lifestyleEntryType: string;
    lifestyleEntryTypeID: number;

    painType?: string;
    painTypeID?: number;

    sportType?: string;
    sportTypeID?: number;
    intensityLevel?: string;

    value: number;

};