import { ChartType, ChartOptions } from "./red-flag-reports";

export interface RedFlagConfigSchema {
    "header": {
        "fromDate": "May 12, 2019";
        "toDate": "May 18, 2019";
        "headerType": "athlete";
        "title": {
            "organization": "CARMEL COLLEGE";
            "team": "The Rebels";
            "group": "Group 1";
            "athlete": "";
        };
    };
    "redFlags": {
        "brainSummary": {
            "include": boolean;
            "data": {
                "useBarChart": boolean;
                "redFlagGroups": Array<{
                    "image": string;
                    "chartType": ChartType;
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": string[];
                        }];
                    };
                }>;
            };
        };
        "bodySummary": {
            "include": boolean;
            "data": {
                "useBarChart": boolean;
                "redFlagGroups": Array<{
                    "image": string;
                    "chartType": ChartType;
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": string[];
                        }];
                    };
                }>;
            };
        };
        "foodSummary": {
            "include": boolean;
            "data": {
                "usePieChart": boolean;
                "pieChartData": {
                    "chartType": ChartType;
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": string[];
                        }];
                    };
                };
            };
        };
        "trainingSummary": {
            "include": boolean;
            "data": {
                "usePieChart": boolean;
                "pieChartData": {
                    "chartType": ChartType;
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": string[];
                        }];
                    };
                };
            };
        };
    };
    "superstars": {
        "include": boolean;
        "data": {
            "superstarsGroups": Array<{
                "isSuperstars": boolean;
                "chartType": ChartType;
                "chartData": {
                    "datasets": [{
                        "backgroundColor": string[];
                    }];
                };
            }>;
        };
    };


    trends: Array<
        {
            "include": boolean;
            "data": {
                "isTrends": boolean;
                "lineChart": {
                    "chartType": ChartType;
                    "chartData": {
                        "datasets": Array<
                            {
                                "borderColor": string;
                                "fill": boolean;
                                "borderJoinStyle": string;
                            }
                        >
                    }
                },
                "barChart": {
                    "chartType": ChartType;
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": string[];
                        }]
                    }
                }
            }
        }
    >;

    "detailed": {
        "training": {
            "include": boolean;
            "data": {
                "detailedChartMeta": {
                    "charts": [{
                        "chartType": ChartType;
                        "chartOptions": ChartOptions;
                        "chartData": {
                            "datasets": Array<{
                                type?: ChartType;
                                fill?: boolean;
                                backgroundColor: string;
                                borderColor?: string;
                                borderCapStyle?: string;
                                borderDash?: number[];
                                borderDashOffset?: number;
                                borderJoinStyle?: string;
                                lineTension?: number;
                                pointBackgroundColor?: string;
                                pointBorderColor?: string;
                                pointBorderWidth?: number;
                                pointHoverRadius?: number;
                                pointHoverBackgroundColor?: string;
                                pointHoverBorderColor?: string;
                                pointHoverBorderWidth?: number;
                                pointRadius?: number;
                                pointHitRadius?: number;
                                yAxisID?: string;
                            }>;
                        };
                    }];
                };
                "chartOne": {
                    "chartType": ChartType;
                    "chartOptions": ChartOptions;
                    "chartData": {
                        "datasets": [{
                            "type": string;
                            "fill": boolean;
                            "backgroundColor": string;
                            "borderColor": string;
                            "borderCapStyle": string;
                            "borderDash": number[];
                            "borderDashOffset": number;
                            "borderJoinStyle": string;
                            "lineTension": number;
                            "pointBackgroundColor": string;
                            "pointBorderColor": string;
                            "pointBorderWidth": number;
                            "pointHoverRadius": number;
                            "pointHoverBackgroundColor": string;
                            "pointHoverBorderColor": string;
                            "pointHoverBorderWidth": number;
                            "pointRadius": number;
                            "pointHitRadius": number;
                        }, {
                            "type": string;
                            "fill": boolean;
                            "backgroundColor": string;
                            "borderColor": string;
                            "borderCapStyle": string;
                            "borderDash": number[];
                            "borderDashOffset": number;
                            "borderJoinStyle": string;
                            "lineTension": number;
                            "pointBackgroundColor": string;
                            "pointBorderColor": string;
                            "pointBorderWidth": number;
                            "pointHoverRadius": number;
                            "pointHoverBackgroundColor": string;
                            "pointHoverBorderColor": string;
                            "pointHoverBorderWidth": number;
                            "pointRadius": number;
                            "pointHitRadius": number;
                        }, {
                            "backgroundColor": string;
                            "yAxisID": string;
                        }, {
                            "backgroundColor": string;
                            "yAxisID": string;
                        }, {
                            "backgroundColor": string;
                            "yAxisID": string;
                        }];
                    };
                };
                "chartTwo": {
                    "chartType": ChartType;
                    "chartOptions": ChartOptions;
                    "chartData": {
                        "datasets": [{
                            label?: string; // TODO Check This
                            "backgroundColor": string[];
                        }];
                    };
                };
                "chartThree": {
                    "chartType": ChartType;
                    "chartOptions": ChartOptions;
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": string[];
                        }];
                    };
                };
            };
        };
        "pain": {
            "include": boolean;
            "data": {
                "detailedChartMeta": {
                    "charts": [{
                        "chartType": ChartType;
                        "chartOptions": ChartOptions;
                        "chartData": {
                            "datasets": [{
                                "type": ChartType;
                                "fill": boolean;
                                "backgroundColor": string;
                                "borderColor": string;
                                "borderCapStyle": string;
                                "borderDash": number[];
                                "borderDashOffset": number;
                                "borderJoinStyle": string;
                                "lineTension": number;
                                "pointBackgroundColor": string;
                                "pointBorderColor": string;
                                "pointBorderWidth": number;
                                "pointHoverRadius": number;
                                "pointHoverBackgroundColor": string;
                                "pointHoverBorderColor": string;
                                "pointHoverBorderWidth": number;
                                "pointRadius": number;
                                "pointHitRadius": number;
                            }, {
                                "type": ChartType;
                                "fill": boolean;
                                "backgroundColor": string;
                                "borderColor": string;
                                "borderCapStyle": string;
                                "borderDash": number[];
                                "borderDashOffset": number;
                                "borderJoinStyle": string;
                                "lineTension": number;
                                "pointBackgroundColor": string;
                                "pointBorderColor": string;
                                "pointBorderWidth": number;
                                "pointHoverRadius": number;
                                "pointHoverBackgroundColor": string;
                                "pointHoverBorderColor": string;
                                "pointHoverBorderWidth": number;
                                "pointRadius": number;
                                "pointHitRadius": number;
                            }, {
                                "backgroundColor": string;
                                "yAxisID": string;
                            }, {
                                "backgroundColor": string;
                                "yAxisID": string;
                            }, {
                                "backgroundColor": string;
                                "yAxisID": string;
                            }];
                        };
                    }];
                };
                "chartOne": {
                    "chartType": ChartType;
                    "chartOptions": ChartOptions;
                    "chartData": {
                        "datasets": [{
                            "type": string;
                            "fill": boolean;
                            "backgroundColor": string;
                            "borderColor": string;
                            "borderCapStyle": string;
                            "borderDash": number[];
                            "borderDashOffset": number;
                            "borderJoinStyle": string;
                            "lineTension": number;
                            "pointBackgroundColor": string;
                            "pointBorderColor": string;
                            "pointBorderWidth": number;
                            "pointHoverRadius": number;
                            "pointHoverBackgroundColor": string;
                            "pointHoverBorderColor": string;
                            "pointHoverBorderWidth": number;
                            "pointRadius": number;
                            "pointHitRadius": number;
                        }, {
                            "type": string;
                            "fill": boolean;
                            "backgroundColor": string;
                            "borderColor": string;
                            "borderCapStyle": string;
                            "borderDash": number[];
                            "borderDashOffset": number;
                            "borderJoinStyle": string;
                            "lineTension": number;
                            "pointBackgroundColor": string;
                            "pointBorderColor": string;
                            "pointBorderWidth": number;
                            "pointHoverRadius": number;
                            "pointHoverBackgroundColor": string;
                            "pointHoverBorderColor": string;
                            "pointHoverBorderWidth": number;
                            "pointRadius": number;
                            "pointHitRadius": number;
                        }, {
                            "backgroundColor": string;
                            "yAxisID": string;
                        }, {
                            "backgroundColor": string;
                            "yAxisID": string;
                        }, {
                            "backgroundColor": string;
                            "yAxisID": string;
                        }];
                    };
                };
                "chartTwo": {
                    "chartType": ChartType;
                    "chartOptions": ChartOptions;
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": string[];
                        }];
                    };
                };
                "chartThree": {
                    "chartType": ChartType;
                    "chartOptions": ChartOptions;
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": string[];
                        }];
                    };
                };
               
            };
        };
    };
}
