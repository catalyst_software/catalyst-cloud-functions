
import moment from 'moment';
import { pubsub } from 'firebase-functions'

import { ReportRequest } from '../models/report-model';

let chosenFunction = "";

// const client = new pubsub.v1.PublisherClient({
//     // optional auth parameters.
//   });

//   var formattedTopic = client.topicPath('[PROJECT]', '[TOPIC]');
//   var data = '';
//   var messagesElement = {
//     data: data,
//   };
//   var messages = [messagesElement];
//   var request = {
//     topic: formattedTopic,
//     messages: messages,
//   };
//   client.publish(request)
//     .then(responses => {
//       var response = responses[0];
//       // doThingsWith(response)
//     })
//     .catch(err => {
//       console.error(err);
//     });

export const generateReport = pubsub
    .topic('generate-report')
    .onPublish(async (message: { data: string; }) => {
        console.log(`PubSub generate-reports triggered`);


        const utcMoment = moment.utc();

        if (message.data) {

            const dataString: ReportRequest = await JSON.parse(Buffer.from(message.data, 'base64').toString());
            console.log(`PubSub Message Data: `, dataString); // hourlyIpScoreNonEngagement
            chosenFunction = 'ipscore-non-engagement';

            const utcHour = +utcMoment.hour().toString()

            console.log(`Querying UTC hour ${utcHour}`)

            const {
                 } = dataString
                
            // const allNonEngagementAthletes = await getAllNonEngagementAthletes(startOfHour, endOfHour)


        }

        return true;
    });