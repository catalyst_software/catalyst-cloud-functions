import moment from 'moment';
import { pubsub } from 'firebase-functions'

import { db } from '../shared/init/initialise-firebase';
import { FirestoreCollection } from '../db/biometricEntries/enums/firestore-collections';

import { Organization } from '../models/organization.model';
import { Athlete } from '../models/athlete/interfaces/athlete';

import { getDocumentFromSnapshot } from '../analytics/triggers/utils/get-document-from-snapshot';
import { getGroupCoaches } from '../analytics/triggers/utils/get-athlete-coaches';
import { NotificationSeverity } from '../api/app/v1/sendGrid/conrollers/email';
import { notifyCoachesIPScoreNonEngagement } from '../api/app/v1/callables/utils/comms/notify-coaches';
import { GroupIndicator } from '../db/biometricEntries/well/interfaces/group-indicator';
import { IPScoreNonEngagementTemplate } from '../api/app/v1/sendGrid/templates/ipscore-non-engagement';
import { getOrgCollectionRef } from '../db/refs';
import { isArray } from 'lodash';
import { Config } from './../shared/init/config/config';


let chosenFunction = "";

const getNonEngagementAthletes = async (org: Organization): Promise<{
    org: Organization;
    athletes: Athlete[];
}> => {

    console.log(`getNonEngagementAthletes for ${org.name}`, org)

    const currentTime = moment(new Date()).format('hh:mm');
    const currentTimeUTC = moment.utc(new Date()).format('hh:mm');

    console.log(chosenFunction + ' CRON CLOUD FUNCTION executed at ' + currentTimeUTC + ' UTC (local client currentTime = ' + currentTime + ')');

    const latestReportDate = moment.utc().startOf('day')
        .subtract(2, 'days')
    // .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })

    console.log('latestReportDate to check (2 days) = (date) => ', latestReportDate.toDate())

    const nonEngagementAthletes = db.collection(FirestoreCollection.Athletes)
        .where('organizationId', '==', org.uid)
        .where('currentIpScoreTracking.dateTime', '<=', latestReportDate.toDate())
        .orderBy("currentIpScoreTracking.dateTime", 'desc')

    const athletes = await nonEngagementAthletes.get()
        .then((athleteQuerySnap) => {
            if (athleteQuerySnap.size) {

                console.log(`${athleteQuerySnap.size} Non Engagement Athletes found`);

                const athlete = athleteQuerySnap.docs.map((athleteDocSnap) => {
                    return getDocumentFromSnapshot(athleteDocSnap) as Athlete
                })

                return athlete
            } else {

                console.log('No Non Engagement Athletes found');

                return []
            }
        })
        .catch((err) => {

            console.error('Error Running The  Querying on Non Engagement Documents', err);
            // process.exit(203)
            throw err
        });

    return {
        org,
        athletes
    }
}

export const ipScoreNonEngagement = pubsub
    ._topicWithOptions('ipscore-non-engagement', {regions: Config.regions})
    .onPublish(async (message: { data: string; }) => {
        console.log(`PubSub ipscore-non-engagement triggered`);

        const utcMoment = moment.utc();

        if (message.data) {

            const dataString = await Buffer.from(message.data, 'base64').toString();
            console.log(`PubSub Message Data: ${dataString}`); // hourlyIpScoreNonEngagement
            chosenFunction = 'ipscore-non-engagement';

            const utcHour = +utcMoment.hour().toString()

            console.log(`Querying UTC hour ${utcHour}`)


            const allNonEngagementAthletes = await getAllNonEngagementAthletes(utcHour)
            // const allNonEngagementAthletes = await getAllNonEngagementAthletes(startOfHour, endOfHour)

            if (isArray(allNonEngagementAthletes)) {
                if (allNonEngagementAthletes.length) {

                    const response = [].concat(allNonEngagementAthletes.map(async (orgAthletes) => {
                        await persistToBQ(orgAthletes)
                        const endResult = await notifyNonEngagementFlags(orgAthletes)

                        console.warn('endResult', endResult)

                        return endResult

                    }))


                    const notificationResponses = await Promise.all(response)
                    return {
                        message: 'allNonEngagementAthletes coaches notified',
                        response: notificationResponses
                    }

                } else {

                    console.log('allNonEngagementAthletes is an empty array', allNonEngagementAthletes)
                    return {
                        message: 'allNonEngagementAthletes is an empty array',
                        allNonEngagementAthletes
                    }
                }
            } else {

                console.warn('allNonEngagementAthletes is NOT an ARRAY', allNonEngagementAthletes)

                return {
                    message: 'allNonEngagementAthletes is NOT an ARRAY',
                    allNonEngagementAthletes
                }
            }
        }

        return true;
    });

async function persistToBQ(orgAthletes: {
    org: Organization;
    athletes: Athlete[];
}) {

    const bqData: Array<{
        athleteUID: string;
        orgUID: string;
        groupUID: string;
        date: Date;
    }> = [].concat(...orgAthletes.athletes.map((athlete) => {

        const groupRes = athlete.groups.map((group) => {
            const entry = {
                athleteUID: athlete.uid,
                orgUID: orgAthletes.org.uid || athlete.organizations[0].organizationId || group.organizationId,
                orgName: orgAthletes.org.name || athlete.organizations[0].organizationName || group.organizationName,
                groupUID: group.uid || group.groupId,
                date: moment().subtract(1, 'day').toDate()
            }

            console.log(`Adding entry to BQ`, { name: athlete.name || `${athlete.profile.firstName} ${athlete.profile.lastName}`, uid: athlete.uid, entry });

            return entry
        })

        return groupRes

        // ipscoreNonEngagementDataset
    }));


    console.log('data being saved to to BQ', bqData)
    // let ipscoreNonEngagementDataset = await getDataset('ip_score_non_engagement')

    // Inserts data into a table
    // const rows = await insertRowsAsStream(
    //     'ip_score_non_engagement',
    //     'data',
    //     results)

    // console.log(`Inserted ${rows.length} rows`);
    console.log(`Inserted ${bqData.length} rows`);

    return bqData
}




async function notifyNonEngagementFlags(flattenedAthletes: {
    org: Organization;
    athletes: Athlete[];
}) {

    const athleteGroups = [].concat(...flattenedAthletes.athletes.map((ath, idx) => {

        if (!ath.groups || !ath.groups.length) {
            console.warn('ATHLETE HAS NO GROUPS')
        }
        const group = ath.groups.map((grp) => {
            return {
                uid: grp.groupId,
                ...grp
            }
        })

        console.log('MAPPED GROUP ' + idx, group)

        return group
    }))

    console.warn('athleteGroups', athleteGroups)

    const distinctGroupIDs = [...new Set(athleteGroups.map(item => { return item.groupId }))];
    // console.warn('distinctGroups', distinctGroupIDs)
    const groupedAthetes = distinctGroupIDs.map((groupId) => {

        const groupAthletes = flattenedAthletes.athletes.filter((ath) => {
            const athGroups = ath.groups
                .filter((grp) => grp.groupId === groupId)

            if (athGroups.length)
                return true
            else return false
        })


        return { groupId, athletes: groupAthletes }
    })
    const theResults = await groupedAthetes.map(async (groupAthletes) => {

        const { groupId, athletes } = groupAthletes

        const group: GroupIndicator = athleteGroups.filter(grp => grp.groupId === groupId).shift()
        if (group) {

            const coachesToEmail = await getGroupCoaches(group.uid || group.groupId)

            console.warn('Coaches EMAILED!!!!!')
            return {
                group: {
                    uid: group.groupId,
                    groupName: group.groupName,
                    groupId: group.groupId
                },
                coachesToEmail: coachesToEmail.map((coach) => {
                    return {
                        ...coach,
                        athletes:
                            athletes
                                .filter((me) => me.uid !== coach.uid)
                                .map((theAth) => {
                                    return {
                                        uid: theAth.uid,
                                        fullName: `${theAth.profile.firstName} ${theAth.profile.lastName}`,
                                        email: theAth.profile.email,
                                        currentIpScoreTracking: theAth.currentIpScoreTracking
                                    }
                                })

                    }
                })
            }


        } else {
            console.warn('GROUP NOT FOUND')
            return false
        }

    })

    return await Promise.all(theResults).then((results) => {

        return results.map(async (grouped) => {

            if (grouped !== false) {
                const { group, coachesToEmail } = grouped

                if (coachesToEmail.length) {

                    coachesToEmail.map(async (coach) => {
                        if (coach.athletes.length) {
                            const emailMessageTemplate: IPScoreNonEngagementTemplate = {
                                to: [],
                                from: {
                                    name: "iNSPIRE",
                                    email: "athleteservices@inspiresportonline.com"
                                },

                                templateId: "d-b95ae611e7124aafa1b948ca09ad3c2d",
                                // emailTemplateName: "badMood",
                                emailTemplateName: "ipScoreNonEngagement",
                                notificationSeverity: NotificationSeverity.Danger,
                                emailTemplateData: {

                                    name: group.groupName,
                                    athletes: coach.athletes.map((athlete) => {

                                        const startOfToday = moment.utc().startOf('day');
                                        const lastLoggedDay = moment(athlete.currentIpScoreTracking.dateTime.toDate()).startOf('day');
                                        const daysSinceLastEntry = startOfToday.diff(lastLoggedDay, 'days');
                                        console.log('Days Since Last Entry', daysSinceLastEntry);

                                        return {
                                            uid: athlete.uid,
                                            fullName: athlete.fullName,
                                            email: athlete.email,
                                            daysSinceLastEntry
                                        }
                                    }),
                                    numberOfAthletes: coach.athletes.length,
                                    dayCountNonEngagement: flattenedAthletes.org.nonEngagementConfiguration.dayCountNonEngagement,
                                    // subject: "Custom Email Subject",

                                    // message: "<h1>Custom messtage</h1>",

                                    showDownloadButton: false,

                                    barType: {
                                        danger: true
                                    }
                                }
                            }

                            const res = await notifyCoachesIPScoreNonEngagement(emailMessageTemplate, [coach]);

                            return res

                        } else {
                            return false
                        }

                    })



                } else {
                    // console.warn(`No Coach found for Athlete having uid ${athleteUID}`);

                    return false
                }

            }

            return grouped
        })
    }).catch((err) => {
        console.warn('oops, error', err)
        throw err
    })

}

async function getAllNonEngagementAthletes(utcHour: number): Promise<Array<{
    org: Organization;
    athletes: Array<Athlete>;
}>> {

    const allNonEngagementAthletesGroupedPromises = await getOrgCollectionRef()
        .where('nonEngagementConfiguration.utcRunHour', '==', utcHour)
        .get()
        .then(async (orgsQuerySnap: FirebaseFirestore.QuerySnapshot) => {
            if (orgsQuerySnap.size) {
                const orgs = await orgsQuerySnap.docs
                    .map((queryDocSnap: FirebaseFirestore.QueryDocumentSnapshot) => {
                        return getDocumentFromSnapshot(queryDocSnap) as Organization;
                    });
                console.log('orgs', orgs);
                return orgs;
            }
            else {
                console.log('No Organizations Found to Query');
                return [];
            }
        }).then(async (orgs: Organization[]) => {
            return await orgs.map(async (theOrg: Organization) => {
                return await getNonEngagementAthletes(theOrg);
            });
        });

    const allAthletes = await Promise.all(allNonEngagementAthletesGroupedPromises)

    return allAthletes
}

