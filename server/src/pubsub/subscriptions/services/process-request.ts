import { AggregateIpScoreDirective } from '../../../db/ipscore/services/interfaces/aggregate-ipscore-directive';
import { getOrgCollectionRef } from '../../../db/refs';

export const updateIpScoreDocument = async (
    directive: AggregateIpScoreDirective
) => {
    // Create a reference to the cities collection
    const organizationsRef = getOrgCollectionRef();

    // Create a query against the collection.
    //   const query = organizationsRef.where('state', '==', 'CA')
    const query = organizationsRef.where(
        'tasks',
        'array-contains',
        'taskName.ipScoreNonEnagagement'
    );

    return query.get().then(() => {
        return directive
    })
};


export const updateNonAggregates = () => {

    return true;


    // const obs = from(query.get().then((querySnapshot: firestore.QuerySnapshot) => {


    //     if (!querySnapshot.empty) {

    //         const docs = querySnapshot.docs//.filter(doc => doc.data().startTime >= hour || doc.data().startTime >= hour)
    //         // const docs = querySnapshot.docs

    //         if (docs.length) {

    //             return docs
    //         } else return []
    //         return docs

    //     } else return []


    // })).pipe(
    //     switchMap((docs) => {


    //         if (docs.length) {

    //             return docs.map((d) => {
    //                 const task = d.data() as ScheduledTasks

    //                 switch (task.entityType) {
    //                     // case EntityType.Athlete:

    //                     //     break;

    //                     // case EntityType.Group:

    //                     //     break;
    //                     case EntityType.Organization:
    //                         const athletesRef = db.collection(FirestoreCollection.Athletes)
    //                             .where(
    //                                 'organizationId',
    //                                 '==',
    //                                 task.entityId
    //                             )


    //                         return from(getOrganizationAthletes(athletesRef)
    //                             .then(docs => {


    //                                 const directive: AggregateIpScoreDirective = {
    //                                     message: 'Testing',
    //                                     athlete
    //                                 }
    //                                 return docs
    //                             })
    //                             .catch(err => {
    //                                 console.log(err)

    //                                 return err
    //                             }))

    //                         default:
    //                             return EMPTY

    //                 }
    //                 // console.log(task)
    //                 // return task
    //             })
    //             // .subscribe((da) => {
    //             //     return da
    //             // })
    //             // return docs
    //         } else {
    //             return []
    //         }
    //     }),
    //     // switchMap((docs) => {

    //     //     return docs
    //     //     // .forEach(element => {

    //     //     // });.subscribe()
    //     // }),
    //     // switchMap((docs2) => {

    //     //     return docs2
    //     // })
    // )

    // // obs.subscribe((data) => {
    // //     const directive: AggregateIpScoreDirective = {
    // //         message: 'Testing',
    // //         athlete
    // //     }
    // //     return directive
    // // })

    // return obs.toPromise()

    // athlete.groups = []
    // athlete.groups.push(theGroup)

    // const athleteObs: Array<Observable<AggregateIpScoreDirective>> = new Array<
    //     Observable<AggregateIpScoreDirective>
    // >()
    // // let groupeObs: Array<Observable<any>> = new Array<Observable<any>>()
    // allAggregateCollections.athlete.map(athletecollectionMeta => {
    //     const directive = {
    //         // actionType: DocMngrActionTypes.UpdateHistoryDocs,
    //         collection: athletecollectionMeta.collection,
    //         message: `Update Athlete IpScore Aggregate Docs.`,
    //         athlete,
    //     }

    //     athleteObs.push(
    //         from(
    //             updateIpScoreDocument(directive).then(updatedDoc => {
    //                 return updatedDoc
    //             })
    //         ).pipe(
    //             switchMap(
    //                 (aggregateIpScoreDirective: AggregateIpScoreDirective) => {
    //                     if (athlete.groups) {
    //                         return athlete.groups.map(group => {
    //                             let thisGroupeObs: Array<
    //                                 Observable<any>
    //                             > = new Array<Observable<any>>()
    //                             thisGroupeObs.push(
    //                                 from(
    //                                     getAthleteDocs({
    //                                         ...aggregateIpScoreDirective,
    //                                         group,
    //                                     })
    //                                 ).pipe(
    //                                     map(
    //                                         (
    //                                             docsDirective: AggregateIpScoreDirective
    //                                         ) => {
    //                                             return docsDirective
    //                                         }
    //                                     )
    //                                 )
    //                             )

    //                             const allObs = merge(...thisGroupeObs)

    //                             return allObs
    //                         })
    //                     } else return EMPTY
    //                 }
    //             ),
    //             switchMap(athleteDoc => {
    //                 athleteDoc.subscribe(
    //                     (
    //                         aggregateIpScoreDirective: AggregateIpScoreDirective
    //                     ) => {
    //                         let thisGroupPromises = []

    //                         thisGroupPromises.push(
    //                             updateIpScoreDocument(aggregateIpScoreDirective)
    //                         )
    //                     }
    //                 )

    //                 return EMPTY
    //             })
    //         )
    //     )
    // })

    // const res = merge(...athleteObs)

    // res.subscribe(dada => {
    //     console.log('hello')
    //     return dada
    // }).unsubscribe()
    // return res.toPromise()
};
