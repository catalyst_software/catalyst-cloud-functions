import { firestore } from 'firebase-admin'
import { WellAggregateCollection } from '../../../db/biometricEntries/enums/firestore-aggregate-collection';
import { WellAggregateHistoryCollection } from '../../../db/biometricEntries/enums/firestore-aggregate-history-collection';
import { IpScoreAggregateCollection } from '../../../db/ipscore/enums/firestore-ipscore-aggregate-collection';
import { IpScoreAggregateHistoryCollection } from '../../../db/ipscore/enums/firestore-ipscore-aggregate-history-collection';

export const runQuery = (
    collection:
        | WellAggregateCollection
        | WellAggregateHistoryCollection
        | IpScoreAggregateCollection
        | IpScoreAggregateHistoryCollection,
    docRef: firestore.DocumentReference
): Promise<firestore.DocumentSnapshot> => {

    debugger;
    return docRef.get().catch(() => {
        // const directive: AggregateDocumentDirective = {
        //     // entity,
        //     // TODO: FIX any
        //     collection: collection as any,
        //     actionType: DocMngrActionTypes.Error,
        //     message: `Unable to run query when querying for documents in ${collection}.  Error: ${err}`,
        //     data: err,
        //     log: { level: LogLevel.Error },
        // };
        // aggregateDocManagerSubject$.next(directive);

        debugger
        return Promise.reject(undefined)
    })
};
