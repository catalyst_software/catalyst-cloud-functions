import { pubsub } from 'firebase-functions'
import moment from 'moment'

// import { sendNotification, setAnalytics } from './../../api/app/v1/controllers/sendNotification'
import { AndroidPurchaseReceipt } from './../../models/purchase/purchase.model'

import { PlayStoreNotification } from '../../models/play-store-notification/interfaces/play-store-notification'
import { AndroidNotificationType } from '../../models/enums/android-notification-type'
import { db } from '../../shared/init/initialise-firebase'
import { updateAthleteSubscriptionStatus } from '../../api/app/v1/controllers/utils/update-subscription-object-status'
import {
    SubscriptionStatus,
    SubscriptionType,
} from '../../models/enums/enums.model'
import { PushNotificationType } from '../../models/enums/push-notification-type'
import { Athlete } from '../../models/athlete/athlete.model'
import { getAndroidRecieptFromToken } from '../../api/app/v1/controllers/getAndroidRecieptFromToken'
import { KeyValuePair } from '../../models/notifications/interfaces/schedule-notification-payload'
import { LogInfo } from '../../shared/logger/logger'
import { SaveEventDirective } from '../../models/purchase/interfaces/save-event-directive'
import { PlatformType } from '../../models/purchase/enums/platfom_type'
import { extractPersistData } from "../../api/app/v1/callables/utils/validate-transaction-receipt/extract-persist-data";
import { persistAthleteReceipt } from "../../api/app/v1/callables/utils/validate-transaction-receipt/persist-athlete-receipt";
import { getAthleteByTransactionId } from '../../api/app/v1/controllers/utils/get-athlete-by-transactionId';
import { sendNotification, setAnalytics } from '../../api/app/v1/controllers/utils/send-notification';
import { Config } from './../../shared/init/config/config';

const globalAny: any = global
globalAny.atob = require('atob')

export const sendMessage = (
    athlete: Athlete,
    type: PushNotificationType,
    actionAnalytics?: string,
    value?: string
) => {

    console.warn('Adding key: {name}')

    const keyValuePairs: KeyValuePair[] = [
        {
            key: '$NAME',
            value: value ? value : athlete.profile.fullName
        },
        {
            key: '{name}',
            value: value ? value : athlete.profile.fullName
        },
    ]

    if (actionAnalytics && actionAnalytics !== '') {
        console.log(actionAnalytics);

        keyValuePairs.push({
            key: 'actionAnalytics',
            value: actionAnalytics
        })
    }

    const token = athlete.firebaseMessagingIds && athlete.firebaseMessagingIds[0] || athlete.metadata.firebaseMessagingIds && athlete.metadata.firebaseMessagingIds[0] || athlete.metadata && athlete.metadata.firebaseMessagingId

    if (token) {
        return sendNotification(type, keyValuePairs, token).then(response => {
            if (response) {
                return true
            } else {
                return false
            }
        })
    } else {
        LogInfo(
            `The token is empty.... Message not sent for Athlete with UID ${
            athlete.uid
            }`
        )
        return Promise.resolve(false)
    }
}

export const notificationGooglePlay = pubsub
    ._topicWithOptions('notificationGooglePlay', {regions: Config.regions})
    .onPublish(async message => {

        console.log('notificationGooglePlay triggered')

        if (message.data) {

            const dataString = await Buffer.from(
                message.data,
                'base64'
            ).toString('ascii')

            const keys: {
                private_key: string
                client_email: string
            } = {
                private_key:
                    '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCkr9ZRjXhZImyJ\nExcfXVkFqNlnvfoSdfW4bTGYWDmcOad8iJvv7T3v1W14zTCQjNcKRibMeNBoDrYW\neekUsbfYOeA834ltUE/WE1w0BucRJbojZarwe0O6+1C5QrOL43Hio8Iq74PlFLKk\n9O5rOKXUHrVHwTFjS8/lY2aPUimSZYXtDBU/OBpM+2HzBeP82LacUnaecJbtlrym\nf1nifbG6nR/UWjHgMPf+lrR0Zn2a9lIUn02Acm3G/1yA+hD/u/3bSEX+TMQtmQOv\nzgvfb66LPfUVWe8q96vsATl0VWwvMagjfqh6LoGff1OjAL1uj4i5o6nPrqzEHDfa\n0kEosWBBAgMBAAECggEAC/knww6exxY4X1/msLZT9FQiGEeI4KI4XvP7ZNjXOWs8\nqNJyyecM93ykJKIAa6X9tWbDx95pwoL9TJWI82L3W45bpflXj73EzCrkq3isAIRm\na8/mtWy00CmY5Rs7HArJeyGOSppW08clNNaE5gE8lzczVVfvrAk1QHdxW66szJKc\nlD9jUF2QjHQBURy9cvEq4oO5gAbmcYsmLumjQnxEo0i6lmK/Tc3HJoypgRYHAtPD\nS7aFQyptpHur/hBg/Xiv7BTg0Mj4DiQW8Qaci/6A07ya4IK2M357Xq64dNZEezQO\neU7wf83WVHwr4883UQGt24YojcqHoUW/jHuDRwVkRQKBgQDcB+KsmSHDc99r2jgH\naf0VAg73UABtVnBEwwipAtPcFBNrKneXwLjfmlxoob/Q+jYiQ2Z+8EXfFcpJJOFC\nzHCPlTCBeTOwxf4dU8zbxtZsfmyHkj3B30DCzJGk4Ax9MSCy+ewtw/xoR6KdOhlr\nqPCKV2TMGVCXSLeWk9hv1X1rXQKBgQC/m9oQFAH0QaW7ZJIjx7u9V+CAuugckH8X\n/o1XZY1FhcxEnxFgG4QGe4dhIEXTF2vCndq+fRBEOuiRg3yCE3OLWl9Wt9sD5fce\njXVUi3daFJXdQo0dcsqtJ1Q5tXaZPoml2pVZxt+iBenh8Z85JxH9+7tc45amc1O3\nHmGQ0qZeNQKBgQCWHuMu84OvwN0MzuQvWscLkE35uqGv96u9nnvIJF+75g6hrWXP\nKfR4yu6FjOY8hJpuoiHKNdDWNh2/7eOrGaUqsZVYoQL9dvi7tbMtt+oQN+mATezI\n27NptP0hyqN6vwwaUJ4tU2xhEY8HSt6RL8B+AsaI4jS0Iy7vE4w2MSjTGQKBgHoJ\nfLjS1W/JxBH3ezC4zPVKnB3BbYaL7bbNlR49+t1122U1Xu60d8FdOht9X5uUBjld\nKu46X3rlfiz37vw2AViXRbPIxADWni9ib4FalrjT9aOH+LLx4u6n5vgegJwX/bmZ\n35ffl53tYEpdB0lyff4jL/F4rwHy4DX4brG7yOSlAoGBAIIPm2UQZPHTxazyJKVr\n1GwQQpiKuhRdR03VwqieEDr1bQi/tVzyhEZLp+Mb8NXfJNnIGRyA77iU9OUVAiN3\nKe4ewkquXavMEIlXUli5Jtljqz/3ldXHUxaMlnciGAopnBI2XoYZOyHUMfTkuROg\nUhKwZFKHM8UJ5hMxPu4NWbO2\n-----END PRIVATE KEY-----\n',
                client_email:
                    'gooleplayapi@inspire-1540046634666.iam.gserviceaccount.com',
            }

            let notification = JSON.parse(
                JSON.stringify(dataString)
            ) as PlayStoreNotification

            // console.log(`PubSub Message Data dataString: ${dataString}`)

            if (notification) {

                console.log('androidNotification', notification)

                if (!notification.packageName) {
                    notification = JSON.parse(notification as any)
                }
    
                console.log(
                    'androidNotification.packageName',
                    notification.packageName
                )

                const { packageName, subscriptionNotification } = notification
                // validateVerbose(packageName, subscriptionNotification);
    
                console.log('subscriptionNotification', subscriptionNotification)
    
                const {
                    // version,
                    notificationType,
                    purchaseToken,
                    subscriptionId,
                } = subscriptionNotification
    
                const receipt = {
                    packageName, // com.inspiresportonline.dev
                    productId: subscriptionId, // com.inspiresportonline.dev.subscriptions
                    purchaseToken,
                }
    
                await db.collection('androidNotifications').add({
                    platform: PlatformType.Android,
                    notification,
                    creationTimestamp: moment().toDate(),
                    receipt,
                })
    
                return getAndroidRecieptFromToken(keys, receipt).then(
                    async (theReceipt: AndroidPurchaseReceipt) => {
                        console.log('androidNotification - theReceipt', theReceipt)
    
                        // TODO:
                        // if (!theReceipt.isSuccessful) {
    
                        // }
                        theReceipt.payload.productId = receipt.productId
    
                        const parsedReceipt = extractPersistData(theReceipt)
                        console.log(
                            'androidNotification - parcedReceipt',
                            parsedReceipt
                        )
                        const {
                            // kind, //: 'androidpublisher#subscriptionPurchase',
                            // startTimeMillis, // '1551616565120',
                            // expiryTimeMillis, // '1551616981829',
                            // autoRenewing, // true,
                            // priceCurrencyCode, // 'PHP',
                            // priceAmountMicros, //'74000000',
                            // countryCode, // 'PH',
                            // paymentState, // 1,
                            orderId, // 'GPA.3361-7091-1006-54015',
                            // purchaseType, //  0,
    
                        } = parsedReceipt
    
                        // TODO:
                        // if (!receipt.isSuccessful) {
                        //     return receipt.errorMessage
                        // } else
                       
                        console.log(
                            '-------------------> parcedReceipt',
                            theReceipt
                        )
             
                        console.log(
                            '-------------------> notificationType',
                            notificationType
                        )
                        console.log(
                            '-------------------> purchaseToken',
                            purchaseToken
                        )
                        console.log('-------------------> orderId', orderId)
                        console.log(
                            '-------------------> orderId sliced',
                            orderId.slice(0, 24)
                        )
    
                        const theAthlete = await getAthleteByTransactionId(
                            orderId
                        )
    
                        console.log(
                            'Notification Type',
                            AndroidNotificationType[notificationType]
                        )
    
                        if (theAthlete && theAthlete.uid) {
                            console.log('THE ATHLETE UID', theAthlete.uid)
                            console.log('theReceipt', parsedReceipt)
    
                            const {
                                expiryTime: expirationDate,
                                startTime: startDate
                                // isExpired,
                            } = parsedReceipt
    
                            const receiptPersistDetails: SaveEventDirective = {
                                platform: PlatformType.Android,
                                transactionId: parsedReceipt.orderId,
                                originalTransactionId: parsedReceipt.orderId.slice(
                                    0,
                                    24
                                ),
                                startDate,
                                expirationDate,
                                notificationType: AndroidNotificationType[
                                    notificationType
                                ] as any,
                                androidReceipt: parsedReceipt,
                            }
    
                            let actionAnalytics
                            switch (notificationType) {
                                case AndroidNotificationType.SubscriptionPurchased:
                                    // A new subscription was purchased.
    
                                    // todo: Store the latest_receipt on your server as a token to verify the user’s subscription status at any time, by validating it with the App Store.
    
                                    await persistAthleteReceipt(
                                        theAthlete.uid,
                                        receiptPersistDetails
                                    )
                                    // return setTimeout(() => {
                                        return updateAthleteSubscriptionStatus(
                                            theAthlete,
                                            parsedReceipt,
                                            expirationDate,
                                            // isTrialPeriod ? SubscriptionStatus.TrailPeriod : SubscriptionStatus.ValidInitial,
                                            SubscriptionStatus.ValidInitial,
                                            packageName.indexOf('premium') > 0 ? SubscriptionType.Premium : SubscriptionType.Basic,
                                            undefined,
                                            subscriptionId,
                                            packageName,
                                            receiptPersistDetails.originalTransactionId,
                                            true,
                                            'INITIAL_BUY'
                                        )
    
                                    // }, 5000);
                                // .then((updateResult: boolean) => {
                                //     // return sendMessage(
                                //     //     theAthlete,
                                //     //     PushNotificationType.ThanksForBasicSubscription
                                //     //     // 'PURCHASED'
                                //     // )
                                // })
    
                                case AndroidNotificationType.SubscriptionCanceled:
                                    // A subscription was either voluntarily or involuntarily cancelled. For voluntary cancellation, sent when the user cancels.
    
                                    // todo: Check Cancellation Date to know the date and time when the subscription was canceled.
    
                                    const currentSubscriptionType =
                                        theAthlete.profile.subscription.type
    
                                    await persistAthleteReceipt(
                                        theAthlete.uid,
                                        receiptPersistDetails
                                    )
    
                                    // change type to free
                                    // historicalSubscriptionType === currentSubscriptionType
                                    return updateAthleteSubscriptionStatus(
                                        theAthlete,
                                        parsedReceipt,
                                        expirationDate,
                                        SubscriptionStatus.CanceledForceLogout, // as is
                                        SubscriptionType.Free,
                                        undefined,
                                        subscriptionId,
                                        packageName,
                                        receiptPersistDetails.originalTransactionId
                                    ).then((updateResult: boolean) => {
                                        if (
                                            currentSubscriptionType !==
                                            SubscriptionType.Free
                                        ) {
    
                                            actionAnalytics = setAnalytics(
                                                PushNotificationType.SubscriptionCanceled,
                                                packageName
                                            )
                                            console.log('----------------------SENDING MESSAGE TO ', theAthlete)
                                            return sendMessage(
                                                theAthlete,
                                                PushNotificationType.SubscriptionCanceled,
                                                actionAnalytics
                                            )
                                        } else {
                                            return Promise.resolve(true)
                                        }
                                    })
    
                                case AndroidNotificationType.SubscriptionRenewed:
                                    // An active subscription was renewed.
                                    // Customer renewed a subscription interactively after it lapsed, either by using your app’s interface or on the App Store in account settings. Service is made available immediately.
    
                                    // todo: Check Subscription Expiration Date to determine the next renewal date and time.
    
                                    await persistAthleteReceipt(
                                        theAthlete.uid,
                                        receiptPersistDetails
                                    )
    
                                    return updateAthleteSubscriptionStatus(
                                        theAthlete,
                                        parsedReceipt,
                                        expirationDate,
                                        SubscriptionStatus.ValidRenewed,
                                        undefined,
                                        undefined,
                                        subscriptionId,
                                        packageName,
                                        receiptPersistDetails.originalTransactionId
                                    ).then((updateResult: boolean) => {
                                        actionAnalytics = setAnalytics(
                                            PushNotificationType.SubscriptionRenewed,
                                            packageName
                                        )
                                        return sendMessage(
                                            theAthlete,
                                            PushNotificationType.SubscriptionRenewed,
                                            actionAnalytics
                                        )
                                    })
                                case AndroidNotificationType.SubscriptionInGracePeriod:
                                    // // A subscription has entered grace period (if enabled).
                                    return sendMessage(
                                        theAthlete,
                                        PushNotificationType.SubscriptionLapsed
                                    )
                                case AndroidNotificationType.SubscriptionExpired:
                                    // A subscription has expired.
                                    actionAnalytics = setAnalytics(
                                        PushNotificationType.SubscriptionExpired,
                                        packageName
                                    )
                                    return sendMessage(
                                        theAthlete,
                                        PushNotificationType.SubscriptionExpired,
                                        actionAnalytics
                                    )
    
                                case AndroidNotificationType.SubscriptionRecovered:
                                // A subscription was recovered from account hold.
    
                                case AndroidNotificationType.SubscriptionOnHold:
                                //A subscription has entered account hold (if enabled).
    
                                // case AndroidNotificationType.SubscriptionRestarted:
                                // // User has reactivated their subscription from Play > Account > Subscriptions (requires opt-in for subscription restoration)
    
                                // case AndroidNotificationType.SubscriptionPriceChangeConfirmed:
                                // // A subscription price change has successfully been confirmed by the user.
    
                                // case AndroidNotificationType.SubscriptionDeferred:
                                // // A subscription's recurrence time has been extended.
    
                                case AndroidNotificationType.SubscriptionRevoked:
                                // A subscription has been revoked from the user before the expiration time.
    
                                default:
                                    console.info(
                                        'Nitification not handled',
                                        notificationType
                                    )
                                    return true
                            }
                        } else {
                            console.log(`querySnap.empty - Can't find athlete doc`)
                            return undefined
                        }
                    }
                )
            } else {
                console.warn('notification object undefined')
            }

        } else {
            console.error('message.data => ', message.data)
            console.log('message => ', JSON.stringify(message))
        }
        return false
    })
