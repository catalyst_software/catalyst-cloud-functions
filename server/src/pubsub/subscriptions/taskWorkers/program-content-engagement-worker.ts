import { pubsub } from 'firebase-functions';
import { BigQuery } from '@google-cloud/bigquery';
import * as uuid from 'uuid';
import { ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { ProgramContentEngagementTask } from '../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { bigqueryClient } from '../../../api/app/v1/callables/utils/big-query/initBQ';
import { ProgramContentEngagementFact } from '../../../api/app/v1/callables/utils/warehouse/program-content-engagement-fact.interface';
import { getAthleteSegmentsFromBigQuery, finalizeTask } from './helpers/helpers';
import { Config } from './../../../shared/init/config/config';

export const programContentEngagmentBigQueryWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.ProgramContentEngagment, {regions: Config.regions})
    .onPublish(async (message) => {
        console.log(`Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ProgramContentEngagment}`);
        if (message.data) {
            const datasetId = 'catalystDW';
            const tableId = 'programContentEngagementFact';
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as ScheduledTask;
            task.creationTimestamp = moment(task.creationTimestamp as any).toDate();
            console.log('JSON.parse(dataBuffer.toString(utf8))', task);

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ProgramContentEngagment}`;
            logTaskQueueMessage(task, msg);

            const segments = await getAthleteSegmentsFromBigQuery(task);
            if (segments.length) {
                msg = `Found ${segments.length} segments for athlete ${task.athleteId}.  Proceeding with BigQuery update.`;
                logTaskQueueMessage(task, msg);

                // Firestore timestamps are already in utc
                const rows: Array<ProgramContentEngagementFact> = [];
                rows.push({
                    uuid: uuid.v4(),
                    content_engagement_type: (task.payload as ProgramContentEngagementTask).contentEngagementType,
                    content_type: (task.payload as ProgramContentEngagementTask).contentType,
                    global_session_id: task.payload.globalSessionID,
                    screen_session_id: task.payload.screenSessionID,
                    bundle_id: task.bundleId,
                    segment_id: null,
                    athlete_id: task.athleteId,
                    program_id: task.payload.programId,
                    content_entry_id: (task.payload as ProgramContentEngagementTask).contentEntryId,
                    created_at: BigQuery.timestamp(new Date(task.creationTimestamp)),
                    device_type: task.deviceType,
                    device_manufacturer: task.deviceManufacturer,
                    device_model: task.deviceModel,
                    device_os: task.deviceOs,
                    device_os_version: task.deviceOsVersion,
                    device_sdk_version: task.deviceSdkVersion,
                    device_uuid: task.deviceUuid
                } as ProgramContentEngagementFact);

                segments.forEach((segment_id: string) => {
                    rows.push({
                        uuid: uuid.v4(),
                        content_engagement_type: (task.payload as ProgramContentEngagementTask).contentEngagementType,
                        content_type: (task.payload as ProgramContentEngagementTask).contentType,
                        global_session_id: task.payload.globalSessionID,
                        screen_session_id: task.payload.screenSessionID,
                        bundle_id: task.bundleId,
                        segment_id,
                        athlete_id: task.athleteId,
                        program_id: task.payload.programId,
                        content_entry_id: (task.payload as ProgramContentEngagementTask).contentEntryId,
                        created_at: BigQuery.timestamp(new Date(task.creationTimestamp)),
                        device_type: task.deviceType,
                        device_manufacturer: task.deviceManufacturer,
                        device_model: task.deviceModel,
                        device_os: task.deviceOs,
                        device_os_version: task.deviceOsVersion,
                        device_sdk_version: task.deviceSdkVersion,
                        device_uuid: task.deviceUuid
                    } as ProgramContentEngagementFact);
                });

                const theRes = await bigqueryClient
                    .dataset(datasetId)
                    .table(tableId)
                    .insert(rows)
                    .then((rowCount) => {
                        return { 
                            rowCount, 
                            errors: undefined }
                    })
                    .catch((e) => {
                        console.log(e.errors);
                        return {
                            rowCount: undefined,
                            errors: e.errors
                        }
                    });

                if (theRes.rowCount !== undefined) {
                    msg = `Successfully migrated ${rows.length} ProgramContentEngagement facts to BigQuery`;
                    logTaskQueueMessage(task, msg);
                } else {
                    msg = `${theRes.errors.length} error(s) occurred when migrating ProgramContentEngagement facts to BigQuery`;
                    logTaskQueueMessage(task, msg);
                    theRes.errors.forEach((e: any) => {
                        msg = `Error: ${JSON.stringify(e)}`;
                        logTaskQueueMessage(task, msg);
                    });
                }

            } else {
                msg = `No segments found for athlete ${task.athleteId}`;
                logTaskQueueMessage(task, msg);
            }

            msg = `Task ${task.uid} completed successfully at ${new Date()}`;
            finalizeTask(task, true, msg);
            return true;
        }
        return false;
    });
