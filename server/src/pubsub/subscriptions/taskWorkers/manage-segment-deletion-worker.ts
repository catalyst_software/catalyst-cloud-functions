import { OrganizationIndicator } from './../../../models/athlete/interfaces/athlete';
import { bigqueryClient } from './../../../api/app/v1/callables/utils/big-query/initBQ';
import { ManageSegmentDeletionPayload } from './../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { pubsub } from 'firebase-functions';
import { ManageSegmentDeletionJobType, ScheduledTaskStatusType, ScheduledTaskWorkerTopicType, SelfHealingScheduleType, SelfHealingTaskWorkerType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { finalizeTask } from './helpers/helpers';
import { initialiseFirebase } from '../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../db/biometricEntries/enums/firestore-collections';
import * as fAdmin from 'firebase-admin';
import { SelfHealingTaskPayload } from '../../../db/taskQueue/interfaces/self-healing-task-payload.interface';
import { SelfHealingTask } from '../../../db/taskQueue/interfaces/self-healing-task.interface';
import { isArray, filter, cloneDeep } from 'lodash';
import { Athlete } from '../../../models/athlete/interfaces/athlete';
import { Group } from '../../../models/group/interfaces/group';
import { Config } from './../../../shared/init/config/config';

const removeAthletesFromSegmentInFirestore = async (db: fAdmin.firestore.Firestore, organizationId: string, segmentId: string, athleteIds: Array<string>): Promise<void> => {
    if (isArray(athleteIds) && athleteIds.length) {
        for (const athleteId of athleteIds) {
            // Step 1: Get Athlete document and remove group indicator
            const athlete: Athlete = await db.collection(FirestoreCollection.Athletes)
                                .doc(athleteId)
                                .get()
                                .then((doc) => {
                                    if (doc.exists) {
                                        return doc.data() as Athlete;
                                    } else {
                                        return undefined;
                                    }
                                })
                                .catch((err) => {
                                    console.error({ err });

                                    return undefined;
                                });
            if (!!athlete && isArray(athlete.groups)) {
                const newGroups = athlete.groups.filter((g: Group) => g.groupId !== segmentId);
                athlete.groups = cloneDeep(newGroups);
            }

            //Step 2: Remove group indicator from organization array on athlete object
            if (!!athlete && isArray(athlete.organizations)) {
                const org: OrganizationIndicator = athlete.organizations.find((o: OrganizationIndicator) => {
                    return o.organizationId === organizationId;
                });

                if (!!org && isArray(org.groups)) {
                    const newOrgGroups = org.groups.filter((g: Group) => g.groupId !== segmentId);
                    org.groups = cloneDeep(newOrgGroups);
                }
            }

            console.log(`Athlete: ${athlete}`);
            if (!!athlete) {
                await db.collection(FirestoreCollection.Athletes)
                        .doc(athleteId)
                        .update(athlete)
                        .then(() => {
                            console.log(`Successfully removed group indicator ${segmentId} from athlete ${athleteId} groups array.`);
                            return;
                        })
                        .catch((err) => {
                            console.error({ err });

                            return;
                        });
            }

            //Step 3: Remove athlete indicator from group subcollection 
            //        and update athlete index
            const path = `${FirestoreCollection.Groups}/${segmentId}/${FirestoreCollection.Athletes}`;
            const success = await db.collection(path)
                    .doc(athleteId)
                    .delete()
                    .then(() => {
                        console.log(`Successfully removed athlete indicator ${athleteId} from group ${segmentId} subcollection.`);
                        return true;
                    })
                    .catch((err) => {
                        console.error({ err });
                        return false;
                    });

            const group: Group = await db.collection(FirestoreCollection.Groups)
                            .doc(segmentId)
                            .get()
                            .then((doc) => {
                                if (doc.exists) {
                                    return doc.data() as Group;
                                } else {
                                    return undefined;
                                }
                            })
                            .catch((err) => {
                                console.error({ err });

                                return undefined;
                            });

            if (!!group && !!group.athleteIndex) {
                const index = cloneDeep(group.athleteIndex);
                delete index[athleteId];

                console.log(`Group: ${group}`);
                await db.collection(FirestoreCollection.Groups)
                        .doc(segmentId)
                        .update(group)
                        .then(() => {
                            console.log(`Successfully removed athlete indicator ${athleteId} from group ${segmentId} athlete index.`);
                            return;
                        })
                        .catch((err) => {
                            console.error({ err });

                            return;
                        });

            }
        }
    }

    return Promise.resolve();
}

const removeAthletesFromSegmentInBigQuery = async (db: fAdmin.firestore.Firestore, segmentId: string, athleteIds?: Array<string>): Promise<void> => {
    const datasetId = 'catalystDW';
    const tableId = 'segmentAthleteDim';

    if (isArray(athleteIds) && athleteIds.length) {
        for (const athleteId of athleteIds) {
            if (!!segmentId) {
                const sqlQuery = `DELETE FROM ${datasetId}.${tableId} WHERE segment_id = '${segmentId}' and athlete_id = '${athleteId}';`;
                console.log(`Executing BigQuery SQL: ${sqlQuery}`);
                // Query options list: https://cloud.google.com/bigquery/docs/reference/v2/jobs/query
                const options = {
                    query: sqlQuery,
                    timeoutMs: 100000, // Time out after 100 seconds.
                    useLegacySql: false, // Use standard SQL syntax for queries.
                };

                const theRes = await bigqueryClient
                        .query(options)
                        .then(() => {
                            console.log('Query success!');
                            return { 
                                success: true, 
                                error: undefined }
                        })
                        .catch(async (e) => {
                            console.log(e);
                            const success = await db.collection(FirestoreCollection.SelfHealingTaskQueue)
                                    .add({
                                            creationTimestamp: new Date(),
                                            executionDelayInMinutes: 90,
                                            scheduleType: SelfHealingScheduleType.EveryTwoHour,
                                            worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                            payload: {
                                                worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                                sql: sqlQuery
                                            } as SelfHealingTaskPayload,
                                            status: ScheduledTaskStatusType.Scheduled
                                        } as SelfHealingTask)
                                    .then(() => {
                                        console.log('BigQueryDataManipulationReattempt self healing task successfully added to firestore.');
                                        return true;
                                    })
                                    .catch((err) => {
                                        console.error(`An error occurred when attempting to to add BigQueryDataManipulationReattempt self healing task to firestore.  Error: ${err}`);
                                        return false;
                                    });

                            return {
                                success: false, 
                                error: e
                            }
                        });
            }
        }
    } else {
        if (!!segmentId) {
            const sqlQuery = `DELETE FROM ${datasetId}.${tableId} WHERE segment_id = '${segmentId}';`;
            console.log(`Executing BigQuery SQL: ${sqlQuery}`);
            // Query options list: https://cloud.google.com/bigquery/docs/reference/v2/jobs/query
            const options = {
                query: sqlQuery,
                timeoutMs: 100000, // Time out after 100 seconds.
                useLegacySql: false, // Use standard SQL syntax for queries.
            };

            const theRes = await bigqueryClient
                    .query(options)
                    .then(() => {
                        console.log('Query success');
                        return { 
                            success: true, 
                            error: undefined }
                    })
                    .catch(async (e) => {
                        console.log(e);
                        const success = await db.collection(FirestoreCollection.SelfHealingTaskQueue)
                                .add({
                                        creationTimestamp: new Date(),
                                        executionDelayInMinutes: 90,
                                        scheduleType: SelfHealingScheduleType.EveryTwoHour,
                                        worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                        payload: {
                                            worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                            sql: sqlQuery
                                        } as SelfHealingTaskPayload,
                                        status: ScheduledTaskStatusType.Scheduled
                                    } as SelfHealingTask)
                                .then(() => {
                                    console.log('BigQueryDataManipulationReattempt self healing task successfully added to firestore.');
                                    return true;
                                })
                                .catch((err) => {
                                    console.error(`An error occurred when attempting to to add BigQueryDataManipulationReattempt self healing task to firestore.  Error: ${err}`);
                                    return false;
                                });

                        return {
                            success: false, 
                            error: e
                        }
                    });
        }
    }

    return Promise.resolve();
}

const getAthleteIdsInSegment = async (db: fAdmin.firestore.Firestore, segmentId: string) : Promise<Array<string>> => {
    const path = `${FirestoreCollection.Groups}/${segmentId}/${FirestoreCollection.Athletes}`;
    return db.collection(path)
            .get()
            .then((docs) => {
                if (docs.docs.length) {
                    return docs.docs.map((d) => {
                        return d.id;
                    })
                } else {
                    return [];
                }
            })
            .catch((err) => {
                console.error( { err } );
                return [];
            })
}

const removeSegmentInFirestore = async (db: fAdmin.firestore.Firestore, organizationId: string, segmentId: string) : Promise<void> => {
    // Step 1: Get all athleteIds 
    const athleteIds = await getAthleteIdsInSegment(db, segmentId);
    console.log(`Retrieved ${athleteIds.length} athletes from group ${segmentId}`);
    
    // Step 2: Remove all athletes from segment
    await removeAthletesFromSegmentInFirestore(db, organizationId, segmentId, athleteIds);

    // Step 3: Delete the segment
    return db.collection(FirestoreCollection.Groups)
             .doc(segmentId)
             .delete()
             .then(() => {
                 console.log(`Successfully deleted group ${segmentId}`);
                 return;
             })
             .catch((err) => {
                 console.error( { err } );

                 return;
             })

}

const removeSegmentInBigQuery = async (db: fAdmin.firestore.Firestore, segmentId: string): Promise<void> => {
    const datasetId = 'catalystDW';
    const segmentTableId = 'segmentDim';
    const athleteSegmentTableId = 'segmentAthleteDim';

    let sqlQuery = `DELETE FROM ${datasetId}.${segmentTableId} WHERE segment_id = '${segmentId}';`;
    console.log(`Executing BigQuery SQL: ${sqlQuery}`);
    // Query options list: https://cloud.google.com/bigquery/docs/reference/v2/jobs/query
    let options = {
        query: sqlQuery,
        timeoutMs: 100000, // Time out after 100 seconds.
        useLegacySql: false, // Use standard SQL syntax for queries.
    };

    let theRes = await bigqueryClient
            .query(options)
            .then(() => {
                console.log('Query success!');
                return { 
                    success: true, 
                    error: undefined }
            })
            .catch(async (e) => {
                console.log(e);
                const success = await db.collection(FirestoreCollection.SelfHealingTaskQueue)
                        .add({
                                creationTimestamp: new Date(),
                                executionDelayInMinutes: 90,
                                scheduleType: SelfHealingScheduleType.EveryTwoHour,
                                worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                payload: {
                                    worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                    sql: sqlQuery
                                } as SelfHealingTaskPayload,
                                status: ScheduledTaskStatusType.Scheduled
                            } as SelfHealingTask)
                        .then(() => {
                            console.log('BigQueryDataManipulationReattempt self healing task successfully added to firestore.');
                            return true;
                        })
                        .catch((err) => {
                            console.error(`An error occurred when attempting to to add BigQueryDataManipulationReattempt self healing task to firestore.  Error: ${err}`);
                            return false;
                        });

                return {
                    success: false, 
                    error: e
                }
            });

            sqlQuery = `DELETE FROM ${datasetId}.${athleteSegmentTableId} WHERE segment_id = '${segmentId}';`;
            console.log(`Executing BigQuery SQL: ${sqlQuery}`);
            // Query options list: https://cloud.google.com/bigquery/docs/reference/v2/jobs/query
            options = {
                query: sqlQuery,
                timeoutMs: 100000, // Time out after 100 seconds.
                useLegacySql: false, // Use standard SQL syntax for queries.
            };
        
            theRes = await bigqueryClient
                    .query(options)
                    .then(() => {
                        console.log('Query success!');
                        return { 
                            success: true, 
                            error: undefined }
                    })
                    .catch(async (e) => {
                        console.log(e);
                        const success = await db.collection(FirestoreCollection.SelfHealingTaskQueue)
                                .add({
                                        creationTimestamp: new Date(),
                                        executionDelayInMinutes: 90,
                                        scheduleType: SelfHealingScheduleType.EveryTwoHour,
                                        worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                        payload: {
                                            worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                            sql: sqlQuery
                                        } as SelfHealingTaskPayload,
                                        status: ScheduledTaskStatusType.Scheduled
                                    } as SelfHealingTask)
                                .then(() => {
                                    console.log('BigQueryDataManipulationReattempt self healing task successfully added to firestore.');
                                    return true;
                                })
                                .catch((err) => {
                                    console.error(`An error occurred when attempting to to add BigQueryDataManipulationReattempt self healing task to firestore.  Error: ${err}`);
                                    return false;
                                });
        
                        return {
                            success: false, 
                            error: e
                        }
                    });
    
    return Promise.resolve();
}

const runDeleteSegment = async (db: fAdmin.firestore.Firestore, organizationId: string, segmentId: string): Promise<void> => {
    await removeSegmentInFirestore(db, organizationId, segmentId);
    await removeSegmentInBigQuery(db, segmentId);

    return Promise.resolve();
}

const runRemoveAthletesFromSegment = async (db: fAdmin.firestore.Firestore, organizationId: string, segmentId: string, athleteIds: Array<string>): Promise<void> => {
    await removeAthletesFromSegmentInFirestore(db, organizationId, segmentId, athleteIds);
    await removeAthletesFromSegmentInBigQuery(db, segmentId, athleteIds);

    return Promise.resolve();
}

export const manageSegmentDeletionWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.ManageSegmentDeletion, {regions: Config.regions})
    .onPublish(async (message) => {
        console.log(`Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ManageSegmentDeletion}`);
        if (message.data) {
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as ScheduledTask;
            task.creationTimestamp = moment(task.creationTimestamp as any).toDate();
            console.log('JSON.parse(dataBuffer.toString(utf8))', task);

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ManageSegmentDeletion}`;
            logTaskQueueMessage(task, msg);

            const payload = task.payload as ManageSegmentDeletionPayload;
            const organizationId = payload.organizationId;
            const segmentId = payload.segmentId;
            const athleteIds = payload.athleteIds;

            msg = `Task params: organizationId: ${organizationId}, segmentId: ${segmentId}, athleteIds: ${athleteIds}`;
            logTaskQueueMessage(task, msg);

            const db = initialiseFirebase('API').db; 
            try {
                switch (payload.jobType) {
                    case ManageSegmentDeletionJobType.DeleteSegment:
                        await runDeleteSegment(db, organizationId, segmentId);
                        break;
                    case ManageSegmentDeletionJobType.RemoveAthletesFromSegment:
                        await runRemoveAthletesFromSegment(db, organizationId, segmentId, athleteIds);
                        break;
                }
                
                msg = `Task ${task.uid} completed successfully at ${new Date()}`;
                finalizeTask(task, true, msg);
                return true;
            } catch (err) {
                msg = `Task ${task.uid} failed at ${new Date()}.  Error: ${ err }`;
                finalizeTask(task, false, msg);
                return false;
            } 

        }
        return false;
    });

    
