import { pubsub } from 'firebase-functions';
import * as uuid from 'uuid';
import { ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { bigqueryClient } from '../../../api/app/v1/callables/utils/big-query/initBQ';
import { CloudTaskUsageMetricTask } from '../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { BigQuery } from '@google-cloud/bigquery';
import { getAthleteSegmentsFromBigQuery, finalizeTask } from './helpers/helpers';
import { CloudTaskUsageMetricFact } from '../../../api/app/v1/callables/utils/warehouse/cloud-task-usage-metric-fact.interface';
import { Config } from './../../../shared/init/config/config';

export const cloudTaskUsageMetricBigQueryWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.CloudTaskUsageMetric, {regions: Config.regions})
    .onPublish(async (message) => {
        if (message.data) {
            const datasetId = 'catalystDW';
            const tableId = 'cloudTaskUsageMetricFact';
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as ScheduledTask;
            task.creationTimestamp = moment(task.creationTimestamp as any).toDate();
            console.log('JSON.parse(dataBuffer.toString(utf8))', task);

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.CloudTaskUsageMetric}`;
            logTaskQueueMessage(task, msg);

            const segments = await getAthleteSegmentsFromBigQuery(task);
            if (segments.length) {
                msg = `Found ${segments.length} segments for athlete ${task.athleteId}.  Proceeding with BigQuery update.`;
                logTaskQueueMessage(task, msg);

                // Firestore timestamps are already in utc
                const rows: Array<CloudTaskUsageMetricFact> = [];
                rows.push({
                        uuid: uuid.v4(),
                        cloud_task_type: (task.payload as CloudTaskUsageMetricTask).taskType as string,
                        segment_id: null,
                        athlete_id: task.athleteId,
                        created_at: BigQuery.timestamp(new Date(task.creationTimestamp)),
                        input_as_string: (task.payload as CloudTaskUsageMetricTask).inputAsString,
                        output_as_string: (task.payload as CloudTaskUsageMetricTask).outputAsString,
                        input_size_kb: (task.payload as CloudTaskUsageMetricTask).inputSizeKb,
                        output_size_kb: (task.payload as CloudTaskUsageMetricTask).outputSizeKb,
                        total_milliseconds: (task.payload as CloudTaskUsageMetricTask).totalMilliseconds,
                    } as unknown as CloudTaskUsageMetricFact);

                segments.forEach((segment_id: string) => {
                    rows.push({
                        uuid: uuid.v4(),
                        cloud_task_type: (task.payload as CloudTaskUsageMetricTask).taskType as string,
                        segment_id,
                        athlete_id: task.athleteId,
                        created_at: BigQuery.timestamp(new Date(task.creationTimestamp)),
                        input_as_string: (task.payload as CloudTaskUsageMetricTask).inputAsString,
                        output_as_string: (task.payload as CloudTaskUsageMetricTask).outputAsString,
                        input_size_kb: (task.payload as CloudTaskUsageMetricTask).inputSizeKb,
                        output_size_kb: (task.payload as CloudTaskUsageMetricTask).outputSizeKb,
                        total_milliseconds: (task.payload as CloudTaskUsageMetricTask).totalMilliseconds,
                        
                    } as unknown as CloudTaskUsageMetricFact);
                });

                const theRes = await bigqueryClient
                    .dataset(datasetId)
                    .table(tableId)
                    .insert(rows)
                    .then((rowCount) => {
                        return { 
                            rowCount, 
                            errors: undefined }
                    })
                    .catch((e) => {
                        console.log(e.errors);
                        return {
                            rowCount: undefined,
                            errors: e.errors
                        }
                    });

                if (theRes.rowCount !== undefined) {
                    msg = `Successfully migrated ${rows.length} CloudTaskUsageMetricFact facts to BigQuery`;
                    logTaskQueueMessage(task, msg);
                } else {
                    msg = `${theRes.errors.length} error(s) occurred when migrating CloudTaskUsageMetricFact facts to BigQuery`;
                    logTaskQueueMessage(task, msg);
                    theRes.errors.forEach((e: any) => {
                        msg = `Error: ${JSON.stringify(e)}`;
                        logTaskQueueMessage(task, msg);
                    });
                }

            } else {
                msg = `No segments found for athlete ${task.athleteId}`;
                logTaskQueueMessage(task, msg);
            }
            msg = `Task ${task.uid} completed successfully at ${new Date()}`;
            finalizeTask(task, true, msg);
            return true;
        }
        return false;
    });
