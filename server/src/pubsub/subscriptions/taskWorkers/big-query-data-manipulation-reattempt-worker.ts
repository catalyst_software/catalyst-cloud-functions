import { pubsub } from 'firebase-functions';
import { ScheduledTaskStatusType, ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import { SelfHealingTask } from '../../../db/taskQueue/interfaces/self-healing-task.interface';
import { FirestoreCollection } from '../../../db/biometricEntries/enums/firestore-collections';
import { isNumber } from 'lodash';
import { initialiseFirebase } from '../../../shared/init/initialise-firebase';
import moment from 'moment';
import { BigQueryDataManipulationReattemptSelfHealingTaskPayload } from '../../../db/taskQueue/interfaces/self-healing-task-payload.interface';
import { bigqueryClient } from '../../../api/app/v1/callables/utils/big-query/initBQ';
import { Config } from './../../../shared/init/config/config';

const db = initialiseFirebase('API').db;

export const bigQueryDataManipulationReattemptWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.BigQueryDataManipulationReattempt, {regions: Config.regions})
    .onPublish(async (message) => {
        if (message.data) {
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as SelfHealingTask;
            task.creationTimestamp = moment(task.creationTimestamp).toDate();
            console.log('JSON.parse(dataBuffer.toString(utf8))', task);

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.BigQueryDataManipulationReattempt}`;
            logTaskQueueMessage(task, msg);
            const payload = (task.payload as BigQueryDataManipulationReattemptSelfHealingTaskPayload);
            if (payload.sql) {
                const options = {
                    query: payload.sql,
                    timeoutMs: 100000, // Time out after 100 seconds.
                    useLegacySql: false, // Use standard SQL syntax for queries.
                  };

                console.log(`Attempting to execute sql: ${payload.sql} with GCP_PROJECT: ${process.env.GCP_PROJECT}`);

                const theTask = await bigqueryClient
                    .query(options)
                    .then(() => {
                        task.status = ScheduledTaskStatusType.Success;
                        msg = `${ScheduledTaskWorkerTopicType.BigQueryDataManipulationReattempt} self healing task executed successfully at ${new Date()}.  Task: ${JSON.stringify(task)}`;
                        logTaskQueueMessage(task, msg);
                        
                        return task;
                    })
                    .catch((e) => {
                        msg = `An error occurred when attempting to execute self healing task.  Error: ${e}`;
                        logTaskQueueMessage(task, msg);
                        task.numberOfFailedAttempts = (isNumber(task.numberOfFailedAttempts) ? task.numberOfFailedAttempts + 1 : 1);
                        task.maxNumberOfFailedAttempts = (isNumber(task.maxNumberOfFailedAttempts) ? task.maxNumberOfFailedAttempts : 10);

                        if (task.numberOfFailedAttempts >= task.maxNumberOfFailedAttempts) {
                            task.status = ScheduledTaskStatusType.Error;
                            msg = `Number of task re-execution attempts has reached max threshold: ${task.maxNumberOfFailedAttempts}`;
                            logTaskQueueMessage(task, msg);
                        }

                        return task;
                    });

                    return await db.collection(FirestoreCollection.SelfHealingTaskQueue)
                        .doc(theTask.uid)
                        .update(theTask)
                        .then(() => {
                            return true;
                        })
                        .catch((e) => {
                            console.error(e);
                            return false;
                        });
            } else {
                // TODO
                return false;
            }
        } else {
            return false;
        }
    });
