import { ManageAthleteSegmentAdditionPayload } from '../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { pubsub } from 'firebase-functions';
import { ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { finalizeTask, manageDataWarehouseSegmentDimension, manageDataWarehouseAthleteSegmentDimension } from './helpers/helpers';
import { Config } from './../../../shared/init/config/config';

export const manageAthleteSegmentAdditionWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.ManageAthleteSegmentAddition, {regions: Config.regions})
    .onPublish(async (message) => {
        console.log(`Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ManageAthleteSegmentAddition}`);
        if (message.data) {
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as ScheduledTask;
            task.creationTimestamp = moment(task.creationTimestamp as any).toDate();
            console.log('JSON.parse(dataBuffer.toString(utf8))', task);

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ManageAthleteSegmentAddition}`;
            logTaskQueueMessage(task, msg);

            const payload = task.payload as ManageAthleteSegmentAdditionPayload;
            const segmentId = payload.segmentId;
            const segmentName = payload.segmentName;
            const segmentType = payload.segmentType;
            const athleteId = payload.athleteId;
            const parentSegmentId = payload.parentSegmentId;

            msg = `Task params: segmentId: ${segmentId}, segmentName: ${segmentName}, segmentType: ${segmentType}, athleteId: ${athleteId}, parentSegmentId: ${parentSegmentId}`;
            logTaskQueueMessage(task, msg);

            try {
                await manageDataWarehouseSegmentDimension(segmentId, segmentName, parentSegmentId, segmentType);
                await manageDataWarehouseAthleteSegmentDimension(segmentId, athleteId);

                msg = `Task ${task.uid} completed successfully at ${new Date()}`;
                finalizeTask(task, true, msg);
                return true;
            } catch (err) {
                msg = `Task ${task.uid} failed at ${new Date()}.  Error: ${ err }`;
                finalizeTask(task, false, msg);
                return false;
            } 

        }
        return false;
    });

    
