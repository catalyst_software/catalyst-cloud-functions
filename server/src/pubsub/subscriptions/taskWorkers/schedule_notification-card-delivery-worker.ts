import { ScheduledTaskConfiguration } from './../../../api/app/v1/callables/utils/warehouse/olap/interfaces/shared-system-config.interface';
import { getSharedSystemConfiguration } from './../../../api/app/v1/callables/utils/warehouse/olap/get-olap-jwt-package';
import { ScheduleNotificationCardDeliveryPayload } from '../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { pubsub } from 'firebase-functions';
import { ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { finalizeTask } from './helpers/helpers';
import { Config } from '../../../shared/init/config/config';

const { CloudTasksClient } = require('@google-cloud/tasks');

export const scheduleNotificationCardDeliveryWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.ScheduleNotificationCardDelivery, {regions: Config.regions})
    .onPublish(async (message) => {
        console.log(`Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ScheduleNotificationCardDelivery}`);
        if (message.data) {
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as ScheduledTask;
            task.creationTimestamp = moment(task.creationTimestamp as any).toDate();

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ScheduleNotificationCardDelivery}`;
            logTaskQueueMessage(task, msg);

            const payload = task.payload as ScheduleNotificationCardDeliveryPayload;
            const stringifiedPayload = JSON.stringify(payload);
            msg = `Payload: ${stringifiedPayload}`;
            logTaskQueueMessage(task, msg);

            try {
                const sharedConfig = await getSharedSystemConfiguration();
                const config = sharedConfig.scheduledTasks.find((c: ScheduledTaskConfiguration) => {
                    return c.queue === 'schedule-notification-card';
                });

                if (!!config) {
                    const client = new CloudTasksClient();

                    msg = `ProjectId: ${config.projectId}, Location: ${config.location}, Queue: ${config.queue}, OidcEmail: ${ config.oidcEmail}, Http Trigger: ${config.httpTrigger}`;
                    logTaskQueueMessage(task, msg);

                    const parent = client.queuePath(config.projectId, config.location, config.queue);
                    msg = `Queue Path: ${parent}`;
                    logTaskQueueMessage(task, msg);
                    const taskBuffer = Buffer.from(stringifiedPayload, 'utf8').toString('base64'); 
                    msg = `Task Buffer: ${taskBuffer}`;
                    logTaskQueueMessage(task, msg);

                    const scheduledTask = {
                        httpRequest: {
                            httpMethod: 'POST',
                            url: config.httpTrigger,
                            body: taskBuffer,
                            // oidcToken: {
                            //     serviceAccountEmail: config.oidcEmail
                            // },
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        },
                        scheduleTime: {
                            seconds: payload.dateInSeconds,
                        }
                    };

                    // Send create task request.
                    msg = `Sending task: ${JSON.stringify(scheduledTask)}`;
                    logTaskQueueMessage(task, msg);
                    const request = {parent, task: scheduledTask};
                    const [response] = await client.createTask(request);
                    msg = `Created task ${response.name}`;
                    logTaskQueueMessage(task, msg);

                } else {
                    msg = 'Configuration for task queue schedule-notification-card not found!';
                    logTaskQueueMessage(task, msg);
                }
                msg = `Task ${task.uid} completed successfully at ${new Date()}`;
                finalizeTask(task, true, msg);
                return true;
            } catch (err) {
                msg = `Task ${task.uid} failed at ${new Date()}.  Error: ${ err }`;
                finalizeTask(task, false, msg);
                return false;
            } 

        }
        return false;
    });

    
