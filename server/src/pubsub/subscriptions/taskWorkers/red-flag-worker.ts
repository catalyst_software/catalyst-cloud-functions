import { pubsub } from 'firebase-functions';
import * as uuid from 'uuid';
import { ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { bigqueryClient } from '../../../api/app/v1/callables/utils/big-query/initBQ';
import { RedFlagTask } from '../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { RedFlagFact } from '../../../api/app/v1/callables/utils/warehouse/red-flag-raised-fact.interface';
import { BigQuery } from '@google-cloud/bigquery';
import { getAthleteSegmentsFromBigQuery, finalizeTask } from './helpers/helpers';
import { Config } from './../../../shared/init/config/config';

export const redFlagBigQueryWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.RedFlag, {regions: Config.regions})
    .onPublish(async (message) => {
        if (message.data) {
            const datasetId = 'catalystDW';
            const tableId = 'redFlagFact';
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as ScheduledTask;
            task.creationTimestamp = moment(task.creationTimestamp as any).toDate();
            console.log('JSON.parse(dataBuffer.toString(utf8))', task);

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.RedFlag}`;
            logTaskQueueMessage(task, msg);
            const resolved = (task.payload as RedFlagTask).redFlagJobType.toLowerCase() === 'update';

            const segments = await getAthleteSegmentsFromBigQuery(task);
            if (segments.length) {
                msg = `Found ${segments.length} segments for athlete ${task.athleteId}.  Proceeding with BigQuery update.`;
                logTaskQueueMessage(task, msg);

                // Firestore timestamps are already in utc
                const rows: Array<RedFlagFact> = [];
                const payload: RedFlagTask = (task.payload as RedFlagTask);

                rows.push({
                    uuid: uuid.v4(),
                    red_flag_type: payload.redFlagType,
                    red_flag_id: payload.redFlagId,
                    health_component_type: payload.healthComponentType,
                    lifestyle_entry_type: payload.lifestyleEntryType,
                    biometric_entry_id: payload.biometricEntryId,
                    global_session_id: payload.globalSessionID,
                    screen_session_id: payload.screenSessionID,
                    bundle_id: task.bundleId,
                    segment_id: null,
                    athlete_id: task.athleteId,
                    evolution_instance_id: payload.evolutionInstanceId || null,
                    created_at: BigQuery.timestamp(new Date(task.creationTimestamp)),
                    resolved,
                    device_type: task.deviceType,
                    device_manufacturer: task.deviceManufacturer,
                    device_model: task.deviceModel,
                    device_os: task.deviceOs,
                    device_os_version: task.deviceOsVersion,
                    device_sdk_version: task.deviceSdkVersion,
                    device_uuid: task.deviceUuid
                } as RedFlagFact);

                segments.forEach((segment_id: string) => {
                    rows.push({
                        uuid: uuid.v4(),
                        red_flag_type: payload.redFlagType,
                        red_flag_id: payload.redFlagId,
                        health_component_type: payload.healthComponentType,
                        lifestyle_entry_type: payload.lifestyleEntryType,
                        biometric_entry_id: payload.biometricEntryId,
                        global_session_id: payload.globalSessionID,
                        screen_session_id: payload.screenSessionID,
                        bundle_id: task.bundleId,
                        segment_id,
                        athlete_id: task.athleteId,
                        evolution_instance_id: payload.evolutionInstanceId || null,
                        created_at: BigQuery.timestamp(new Date(task.creationTimestamp)),
                        resolved,
                        device_type: task.deviceType,
                        device_manufacturer: task.deviceManufacturer,
                        device_model: task.deviceModel,
                        device_os: task.deviceOs,
                        device_os_version: task.deviceOsVersion,
                        device_sdk_version: task.deviceSdkVersion,
                        device_uuid: task.deviceUuid
                    } as RedFlagFact);
                });
                        
                const theRes = await bigqueryClient
                    .dataset(datasetId)
                    .table(tableId)
                    .insert(rows)
                    .then((rowCount) => {
                        return { 
                            rowCount, 
                            errors: undefined }
                    })
                    .catch((e) => {
                        console.log(e.errors);
                        return {
                            rowCount: undefined,
                            errors: e.errors
                        }
                    });

                if (theRes.rowCount !== undefined) {
                    msg = `Successfully migrated ${rows.length} RedFlag facts to BigQuery`;
                    logTaskQueueMessage(task, msg);
                } else {
                    msg = `${theRes.errors.length} error(s) occurred when migrating RedFlag facts to BigQuery`;
                    logTaskQueueMessage(task, msg);
                    theRes.errors.forEach((e: any) => {
                        msg = `Error: ${JSON.stringify(e)}`;
                        logTaskQueueMessage(task, msg);
                    });
                }

            } else {
                msg = `No segments found for athlete ${task.athleteId}`;
                logTaskQueueMessage(task, msg);
            }

            msg = `Task ${task.uid} completed successfully at ${new Date()}`;
            finalizeTask(task, true, msg);
            return true;
        }
        return false;
    });
