import { pubsub } from 'firebase-functions'
import { BigQuery } from '@google-cloud/bigquery'
import * as uuid from 'uuid'
import { ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums'
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface'
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage'
import moment from 'moment'
import { bigqueryClient } from '../../../api/app/v1/callables/utils/big-query/initBQ'
import { BiometricTask } from '../../../db/taskQueue/interfaces/scheduled-task-payload.interface'
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type'
import { BiometricFact } from '../../../api/app/v1/callables/utils/warehouse/biometric-fact.interface'
import { MealComponent } from '../../../db/biometricEntries/well/interfaces/meal-component'
import { TrainingComponentInterface } from '../../../db/biometricEntries/well/interfaces/training-component'
import { PainComponent } from '../../../db/biometricEntries/well/interfaces/pain-component'
import { PainType } from '../../../db/biometricEntries/well/enums/pain-type'
import { BodyLocationType } from '../../../db/biometricEntries/well/enums/body-location-type'
import { IntensityLevelType } from '../../../models/enums/enums.model'
import { getAthleteSegmentsFromBigQuery, finalizeTask } from './helpers/helpers'
import { Config } from './../../../shared/init/config/config'

const getIntensityLevelAsString = (index: number): string => {
    let level = index + ''
    if (index <= 3) {
        level = IntensityLevelType[index]
    }

    return level
}

export const biometricBigQueryWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.Biometric, {
        regions: Config.regions,
    })
    .onPublish(async message => {
        if (message.data) {
            const datasetId = 'catalystDW'
            const tableId = 'biometricFact'
            const dataBuffer = Buffer.from(message.data, 'base64')

            const task = JSON.parse(
                dataBuffer.toString('utf8')
            ) as ScheduledTask
            task.creationTimestamp = moment(
                task.creationTimestamp as any
            ).toDate()
            console.log('JSON.parse(dataBuffer.toString(utf8))', task)

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.Biometric}`
            logTaskQueueMessage(task, msg)

            const segments = await getAthleteSegmentsFromBigQuery(task)
            if (segments.length) {
                msg = `Found ${segments.length} segments for athlete ${task.athleteId}.  Proceeding with BigQuery update.`
                logTaskQueueMessage(task, msg)

                // Firestore timestamps are already in utc
                const rows: Array<BiometricFact> = []
                const payload: BiometricTask = task.payload as BiometricTask
                switch ((task.payload as BiometricTask).lifestyleEntryType) {
                    case LifestyleEntryType[LifestyleEntryType.Mood]:
                    case LifestyleEntryType[LifestyleEntryType.Sleep]:
                    case LifestyleEntryType[LifestyleEntryType.Fatigue]:
                    case LifestyleEntryType[LifestyleEntryType.Weight]:
                    case LifestyleEntryType[LifestyleEntryType.BodyMassIndex]:
                    case LifestyleEntryType[LifestyleEntryType.Sick]:
                    case LifestyleEntryType[LifestyleEntryType.Period]:
                    case LifestyleEntryType[LifestyleEntryType.Steps]:
                    case LifestyleEntryType[LifestyleEntryType.Distance]:
                    case LifestyleEntryType[LifestyleEntryType.HeartRate]:
                    case LifestyleEntryType[
                        LifestyleEntryType.ActiveCaloriesBurned
                    ]:
                    case LifestyleEntryType[
                        LifestyleEntryType.RestingCaloriesBurned
                    ]:
                    case LifestyleEntryType[
                        LifestyleEntryType.BodyFatPercentage
                    ]:
                        rows.push({
                            uuid: uuid.v4(),
                            health_component_type: payload.healthComponentType,
                            lifestyle_entry_type: payload.lifestyleEntryType,
                            biometric_entry_id: payload.biometricEntryId,
                            global_session_id: payload.globalSessionID,
                            screen_session_id: payload.screenSessionID,
                            bundle_id: task.bundleId,
                            segment_id: null,
                            athlete_id: task.athleteId,
                            evolution_instance_id:
                                payload.evolutionInstanceId || null,
                            created_at: BigQuery.timestamp(
                                new Date(task.creationTimestamp)
                            ),
                            value: payload.value,
                            device_type: task.deviceType,
                            device_manufacturer: task.deviceManufacturer,
                            device_model: task.deviceModel,
                            device_os: task.deviceOs,
                            device_os_version: task.deviceOsVersion,
                            device_sdk_version: task.deviceSdkVersion,
                            device_uuid: task.deviceUuid,
                        } as BiometricFact)

                        segments.forEach((segment_id: string) => {
                            rows.push({
                                uuid: uuid.v4(),
                                health_component_type:
                                    payload.healthComponentType,
                                lifestyle_entry_type:
                                    payload.lifestyleEntryType,
                                biometric_entry_id: payload.biometricEntryId,
                                global_session_id: payload.globalSessionID,
                                screen_session_id: payload.screenSessionID,
                                bundle_id: task.bundleId,
                                segment_id,
                                athlete_id: task.athleteId,
                                evolution_instance_id:
                                    payload.evolutionInstanceId || null,
                                created_at: BigQuery.timestamp(
                                    new Date(task.creationTimestamp)
                                ),
                                value: payload.value,
                                device_type: task.deviceType,
                                device_manufacturer: task.deviceManufacturer,
                                device_model: task.deviceModel,
                                device_os: task.deviceOs,
                                device_os_version: task.deviceOsVersion,
                                device_sdk_version: task.deviceSdkVersion,
                                device_uuid: task.deviceUuid,
                            } as BiometricFact)
                        })
                        break
                    case LifestyleEntryType[LifestyleEntryType.SimpleFood]:
                        ;(payload.components as Array<MealComponent>).forEach(
                            (c: MealComponent) => {
                                rows.push({
                                    uuid: uuid.v4(),
                                    health_component_type:
                                        payload.healthComponentType,
                                    lifestyle_entry_type:
                                        payload.lifestyleEntryType,
                                    lifestyle_entry_sub_type_1: c.mealType,
                                    biometric_entry_id:
                                        payload.biometricEntryId,
                                    global_session_id: payload.globalSessionID,
                                    screen_session_id: payload.screenSessionID,
                                    bundle_id: task.bundleId,
                                    segment_id: null,
                                    athlete_id: task.athleteId,
                                    evolution_instance_id:
                                        payload.evolutionInstanceId || null,
                                    created_at: BigQuery.timestamp(
                                        new Date(task.creationTimestamp)
                                    ),
                                    value: c.points,
                                    device_type: task.deviceType,
                                    device_manufacturer:
                                        task.deviceManufacturer,
                                    device_model: task.deviceModel,
                                    device_os: task.deviceOs,
                                    device_os_version: task.deviceOsVersion,
                                    device_sdk_version: task.deviceSdkVersion,
                                    device_uuid: task.deviceUuid,
                                } as BiometricFact)

                                segments.forEach((segment_id: string) => {
                                    rows.push({
                                        uuid: uuid.v4(),
                                        health_component_type:
                                            payload.healthComponentType,
                                        lifestyle_entry_type:
                                            payload.lifestyleEntryType,
                                        lifestyle_entry_sub_type_1: c.mealType,
                                        biometric_entry_id:
                                            payload.biometricEntryId,
                                        global_session_id:
                                            payload.globalSessionID,
                                        screen_session_id:
                                            payload.screenSessionID,
                                        bundle_id: task.bundleId,
                                        segment_id,
                                        athlete_id: task.athleteId,
                                        evolution_instance_id:
                                            payload.evolutionInstanceId || null,
                                        created_at: BigQuery.timestamp(
                                            new Date(task.creationTimestamp)
                                        ),
                                        value: c.points,
                                        device_type: task.deviceType,
                                        device_manufacturer:
                                            task.deviceManufacturer,
                                        device_model: task.deviceModel,
                                        device_os: task.deviceOs,
                                        device_os_version: task.deviceOsVersion,
                                        device_sdk_version:
                                            task.deviceSdkVersion,
                                        device_uuid: task.deviceUuid,
                                    } as BiometricFact)
                                })
                            }
                        )
                        break
                    case LifestyleEntryType[LifestyleEntryType.Training]:
                        ;(
                            payload.components as Array<TrainingComponentInterface>
                        ).forEach((c: TrainingComponentInterface) => {
                            rows.push({
                                uuid: uuid.v4(),
                                health_component_type:
                                    payload.healthComponentType,
                                lifestyle_entry_type:
                                    payload.lifestyleEntryType,
                                lifestyle_entry_sub_type_1: c.sportName,
                                lifestyle_entry_sub_type_2:
                                    getIntensityLevelAsString(c.intensityLevel),
                                biometric_entry_id: payload.biometricEntryId,
                                global_session_id: payload.globalSessionID,
                                screen_session_id: payload.screenSessionID,
                                bundle_id: task.bundleId,
                                segment_id: null,
                                athlete_id: task.athleteId,
                                evolution_instance_id:
                                    payload.evolutionInstanceId || null,
                                created_at: BigQuery.timestamp(
                                    new Date(task.creationTimestamp)
                                ),
                                value: c.minutes,
                                device_type: task.deviceType,
                                device_manufacturer: task.deviceManufacturer,
                                device_model: task.deviceModel,
                                device_os: task.deviceOs,
                                device_os_version: task.deviceOsVersion,
                                device_sdk_version: task.deviceSdkVersion,
                                device_uuid: task.deviceUuid,
                            } as BiometricFact)

                            segments.forEach((segment_id: string) => {
                                rows.push({
                                    uuid: uuid.v4(),
                                    health_component_type:
                                        payload.healthComponentType,
                                    lifestyle_entry_type:
                                        payload.lifestyleEntryType,
                                    lifestyle_entry_sub_type_1: c.sportName,
                                    lifestyle_entry_sub_type_2:
                                        getIntensityLevelAsString(
                                            c.intensityLevel
                                        ),
                                    biometric_entry_id:
                                        payload.biometricEntryId,
                                    global_session_id: payload.globalSessionID,
                                    screen_session_id: payload.screenSessionID,
                                    bundle_id: task.bundleId,
                                    segment_id,
                                    athlete_id: task.athleteId,
                                    evolution_instance_id:
                                        payload.evolutionInstanceId || null,
                                    created_at: BigQuery.timestamp(
                                        new Date(task.creationTimestamp)
                                    ),
                                    value: c.minutes,
                                    device_type: task.deviceType,
                                    device_manufacturer:
                                        task.deviceManufacturer,
                                    device_model: task.deviceModel,
                                    device_os: task.deviceOs,
                                    device_os_version: task.deviceOsVersion,
                                    device_sdk_version: task.deviceSdkVersion,
                                    device_uuid: task.deviceUuid,
                                } as BiometricFact)
                            })
                        })
                        break
                    case LifestyleEntryType[LifestyleEntryType.Pain]:
                        ;(payload.components as Array<PainComponent>).forEach(
                            (c: PainComponent) => {
                                if (c.painLevel > 0) {
                                    rows.push({
                                        uuid: uuid.v4(),
                                        health_component_type:
                                            payload.healthComponentType,
                                        lifestyle_entry_type:
                                            payload.lifestyleEntryType,
                                        lifestyle_entry_sub_type_1:
                                            PainType[c.painType],
                                        lifestyle_entry_sub_type_2:
                                            BodyLocationType[c.bodyLocation],
                                        biometric_entry_id:
                                            payload.biometricEntryId,
                                        global_session_id:
                                            payload.globalSessionID,
                                        screen_session_id:
                                            payload.screenSessionID,
                                        bundle_id: task.bundleId,
                                        segment_id: null,
                                        athlete_id: task.athleteId,
                                        evolution_instance_id:
                                            payload.evolutionInstanceId || null,
                                        created_at: BigQuery.timestamp(
                                            new Date(task.creationTimestamp)
                                        ),
                                        value: c.painLevel,
                                        device_type: task.deviceType,
                                        device_manufacturer:
                                            task.deviceManufacturer,
                                        device_model: task.deviceModel,
                                        device_os: task.deviceOs,
                                        device_os_version: task.deviceOsVersion,
                                        device_sdk_version:
                                            task.deviceSdkVersion,
                                        device_uuid: task.deviceUuid,
                                    } as BiometricFact)

                                    segments.forEach((segment_id: string) => {
                                        rows.push({
                                            uuid: uuid.v4(),
                                            health_component_type:
                                                payload.healthComponentType,
                                            lifestyle_entry_type:
                                                payload.lifestyleEntryType,
                                            lifestyle_entry_sub_type_1:
                                                PainType[c.painType],
                                            lifestyle_entry_sub_type_2:
                                                BodyLocationType[
                                                    c.bodyLocation
                                                ],
                                            biometric_entry_id:
                                                payload.biometricEntryId,
                                            global_session_id:
                                                payload.globalSessionID,
                                            screen_session_id:
                                                payload.screenSessionID,
                                            bundle_id: task.bundleId,
                                            segment_id,
                                            athlete_id: task.athleteId,
                                            evolution_instance_id:
                                                payload.evolutionInstanceId ||
                                                null,
                                            created_at: BigQuery.timestamp(
                                                new Date(task.creationTimestamp)
                                            ),
                                            value: c.painLevel,
                                            device_type: task.deviceType,
                                            device_manufacturer:
                                                task.deviceManufacturer,
                                            device_model: task.deviceModel,
                                            device_os: task.deviceOs,
                                            device_os_version:
                                                task.deviceOsVersion,
                                            device_sdk_version:
                                                task.deviceSdkVersion,
                                            device_uuid: task.deviceUuid,
                                        } as BiometricFact)
                                    })
                                }
                            }
                        )
                        break
                }

                const theRes = await bigqueryClient
                    .dataset(datasetId)
                    .table(tableId)
                    .insert(rows)
                    .then(rowCount => {
                        return {
                            rowCount,
                            errors: undefined,
                        }
                    })
                    .catch(e => {
                        console.log(e.errors)
                        return {
                            rowCount: undefined,
                            errors: e.errors,
                        }
                    })

                if (theRes.rowCount !== undefined) {
                    msg = `Successfully migrated ${rows.length} Biometric facts to BigQuery`
                    logTaskQueueMessage(task, msg)
                } else {
                    msg = `${theRes.errors.length} error(s) occurred when migrating Biometric facts to BigQuery`
                    logTaskQueueMessage(task, msg)
                    theRes.errors.forEach((e: any) => {
                        msg = `Error: ${JSON.stringify(e)}`
                        logTaskQueueMessage(task, msg)
                    })
                }
            } else {
                msg = `No segments found for athlete ${task.athleteId}`
                logTaskQueueMessage(task, msg)
            }
            msg = `Task ${task.uid} completed successfully at ${new Date()}`
            finalizeTask(task, true, msg)
            return true
        }
        return false
    })
