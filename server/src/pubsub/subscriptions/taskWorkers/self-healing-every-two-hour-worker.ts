import { pubsub } from 'firebase-functions';
import { ScheduledTaskStatusType, ScheduledTaskWorkerTopicType, SelfHealingScheduleType, SelfHealingTaskWorkerType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import { SelfHealingTask } from '../../../db/taskQueue/interfaces/self-healing-task.interface';
import { FirestoreCollection } from '../../../db/biometricEntries/enums/firestore-collections';
import { isNumber } from 'lodash';
import { PubSub } from '@google-cloud/pubsub';
import { initialiseFirebase } from '../../../shared/init/initialise-firebase';
import moment from 'moment';
import { firestore } from 'firebase-admin';
import { Config } from './../../../shared/init/config/config';

const db = initialiseFirebase('API').db;

const diffMinutes = (dt2: Date, dt1: Date) => {
    let diff =(dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;

    return Math.abs(Math.round(diff));
}

export const selfHealingEveryTwoHourWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.SelfHealingEveryTwoHour, {regions: Config.regions})
    .onPublish(async (message) => {
        const now = new Date();
        console.log(`${ScheduledTaskWorkerTopicType.SelfHealingEveryTwoHour} fired at ${now}.  Attempting to query firestore for pending self healing jobs.`);
        
        const selfHealingTasks: Array<SelfHealingTask> = 
            await db.collection(FirestoreCollection.SelfHealingTaskQueue)
                    .where('status', '==', ScheduledTaskStatusType.Scheduled)
                    .where('scheduleType', '==', SelfHealingScheduleType.EveryTwoHour)
                    .get()
                    .then((docs) => {
                        const tasks: Array<SelfHealingTask> = [];
                        const ln = docs.docs.length;
                        console.log(`Retrieved ${ln} scheduled self healing tasks.`);
                        if (ln > 0) {
                            for (const doc of docs.docs) {
                                if (doc.exists) {
                                    const task = {
                                        ...doc.data() as SelfHealingTask,
                                        uid: doc.id
                                    };
                                    console.log(`Loaded scheduled self healing task: ${task}`);
                                    tasks.push(task);
                                }
                            }
                        }
                        return tasks;
                    })
                    .catch((err) => {
                        console.error(`An error occurred when reading ${FirestoreCollection.SelfHealingTaskQueue} in firestore.  Error: ${err}`);
                        return [];
                    });
            
            if (selfHealingTasks.length) {
                for (const task of selfHealingTasks) {

                    const cts = (task.creationTimestamp as firestore.Timestamp).toDate();
                    console.log(`Creation timestamp as date: ${cts}`);
                    const diff = diffMinutes(now, cts);
                    console.log(`Now timestamp: ${now}.  Creation timestamp: ${cts}.  Calculated diff minutes: ${diff}`);
                    

                    if (diff >= task.executionDelayInMinutes) {
                        switch (task.worker) {
                            case SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt:
                                const msg = `Self healing task ${task.uid} received at ${new Date()}`;
                                logTaskQueueMessage(task, msg);

                                const pubSubClient = new PubSub();
                                task.creationTimestamp = moment(task.creationTimestamp).toISOString();

                                let messageId;
                                const taskBuffer = Buffer.from(JSON.stringify(task), 'utf8');
                                try {
                                    messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.BigQueryDataManipulationReattempt).publish(taskBuffer);
                                    console.log(`Message ${messageId} published.`);
                                } catch (err) {
                                    console.error(err);
                                }
                                break;
                            default: 
                                logTaskQueueMessage(task, `Unsupported self healing task detected: ${task.worker}.  Aborting operation`);
                                task.status = ScheduledTaskStatusType.Error;
                                task.numberOfFailedAttempts = (isNumber(task.numberOfFailedAttempts) ? ++task.numberOfFailedAttempts : 1);
                                await db.collection(FirestoreCollection.SelfHealingTaskQueue)
                                        .doc(task.uid)
                                        .update(task)
                                        .then(() => {
                                            console.log(`Self healing task updated: ${task.uid}`);
                                        })
                                        .catch((err) => {
                                            console.error(`Self healing task ${task.uid} raised error on update.  Error: ${err}`);
                                        })

                                break;
                        }
                    } else {
                        console.log(`Task ${task.uid} has not reached scheduled delay minutes ${task.executionDelayInMinutes}`);
                    }
                }
            } else {
                console.log('No scheduled self healing tasks pending.  Aborting operation.');
            }

    });
