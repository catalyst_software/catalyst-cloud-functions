import { ProgramDayEntry } from './../../../db/programs/models/interfaces/program-day-entry';
import { ManageNewProgramAdditionPayload } from './../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { ManageAthleteSegmentAdditionPayload } from '../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { pubsub } from 'firebase-functions';
import { ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { finalizeTask, manageDataWarehouseProgramContentDimension, manageDataWarehouseProgramDimension } from './helpers/helpers';
import { Config } from '../../../shared/init/config/config';
import { Program } from '../../../db/programs/models/interfaces/program';
import { isArray } from 'lodash';
import { ProgramContentType } from '../../../models/enums/enums.model';

const flattenEntries = (program: Program): Array<ProgramDayEntry> => {
    const entries: Array<ProgramDayEntry> = [];
    for (const day of program.content) {
        if (isArray(day.entries)) {
            day.entries.forEach((e: ProgramDayEntry) => {
                entries.push(e);
            });
        }
    }
    return entries;
}

export const manageNewProgramAdditionWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.ManageNewProgramAddition, {regions: Config.regions})
    .onPublish(async (message) => {
        console.log(`Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ManageNewProgramAddition}`);
        if (message.data) {
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as ScheduledTask;
            task.creationTimestamp = moment(task.creationTimestamp as any).toDate();
            console.log('JSON.parse(dataBuffer.toString(utf8))', task);

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.ManageNewProgramAddition}`;
            logTaskQueueMessage(task, msg);

            const payload = task.payload as ManageNewProgramAdditionPayload;
            const program = payload.program;

            try {
                await manageDataWarehouseProgramDimension(program.programGuid, program.name, program.categoryType);
                const entries = flattenEntries(program);
                for (const entry of entries) {
                    const type = ProgramContentType[entry.type];
                    await manageDataWarehouseProgramContentDimension(program.programGuid, program.name, program.categoryType, entry.fileGuid, entry.title, type)
                }

                msg = `Task ${task.uid} completed successfully at ${new Date()}`;
                finalizeTask(task, true, msg);
                return true;
            } catch (err) {
                msg = `Task ${task.uid} failed at ${new Date()}.  Error: ${ err }`;
                finalizeTask(task, false, msg);
                return false;
            } 

        }
        return false;
    });

    
