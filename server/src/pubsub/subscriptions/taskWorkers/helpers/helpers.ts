import { ProgramDimension } from './../../../../api/app/v1/callables/utils/warehouse/program-dimension.interface';
import { bigqueryClient } from './../../../../api/app/v1/callables/utils/big-query/initBQ';
import { AthleteSegmentCacheItem } from "../../../../api/app/v1/callables/utils/warehouse/athlete-segment-cache-item.interface";
import { ScheduledTask } from "../../../../db/taskQueue/interfaces/scheduled-task.interface";
import { getQueryResults, getTaskQueryResults } from "../../../../api/app/v1/callables/utils/big-query/get-query-results";
import { logTaskQueueMessage } from "../../../../shared/utils/taskQueueLogging/logTaskQueueMessage";
import { ScheduledTaskStatusType } from "../../../../db/taskQueue/enums/scheduled-task.enums";
import { getTaskQueueDocRef } from "../../../../db/refs";
import { isArray } from "lodash";
import { SegmentAthleteDimension } from "../../../../api/app/v1/callables/utils/warehouse/segment-athlete-dimension.interface";
import { SegmentDimension } from "../../../../api/app/v1/callables/utils/warehouse/segment-dimension.interface";
import { bqExport } from "../../bq-export";
import { SegmentType } from '../../../../api/app/v1/callables/utils/warehouse/segment-type.enum';

const athleteSegmentCache: Array<AthleteSegmentCacheItem> = [];


export const finalizeTask = (task: ScheduledTask, success: boolean, msg: string) => {
    logTaskQueueMessage(task, msg);
    task.status = (success) ? ScheduledTaskStatusType.Success : ScheduledTaskStatusType.Error;
    const taskDocRef = getTaskQueueDocRef(task.uid);
    taskDocRef.update(task)
        .then(() => {
            console.log(`Task ${task.uid} successfully updated.`);
        })
        .catch((e) => {
            console.error(`Task ${task.uid} update failed: ${e.message}`);
        });
}

export const getAthleteSegmentsFromBigQuery = async (task: ScheduledTask): Promise<Array<string>> => {
    let segments: Array<string> = [];
    // const cachedItem = athleteSegmentCache.find((item: AthleteSegmentCacheItem) => {
    //     return !!item && item.athleteId === task.athleteId;
    // });

    // if (cachedItem) {
    //     segments = cachedItem.segments;
    // } else {
    const query = `select segment_id from catalystDW.segmentAthleteDim where athlete_id = '${task.athleteId}'`;
    const rows = await getTaskQueryResults<{segment_id: string}>(task, query);
    rows.forEach((obj: {segment_id: string}) => {
        segments.push(obj.segment_id);
    });

    // athleteSegmentCache.push({
    //     athleteId: task.athleteId,
    //     segments
    // })
    // }

    return Promise.resolve(segments);
}

export const getSegmentExistsInBigQuery = async (segmentId: string): Promise<boolean> => {
    const datasetId = 'catalystDW';
    const segmentTableId = 'segmentDim';
    const query = `select segment_id from ${datasetId}.${segmentTableId} where segment_id = '${segmentId}'`;
    const rows = await getQueryResults<SegmentDimension>(query);

    return Promise.resolve((isArray(rows) && rows.length > 0));
}

export const getSegmentAthleteExistsInBigQuery = async (segmentId: string, athleteId: string): Promise<boolean> => {
    const datasetId = 'catalystDW';
    const segmentAthleteTableId = 'segmentAthleteDim';
    const query = `select segment_id from ${datasetId}.${segmentAthleteTableId} where segment_id = '${segmentId}' and athlete_id = '${athleteId}'`;
    const rows = await getQueryResults<SegmentAthleteDimension>(query);

    return Promise.resolve((isArray(rows) && rows.length > 0));
}

export const getProgramExistsInBigQuery = async (programId: string): Promise<boolean> => {
    const datasetId = 'catalystDW';
    const segmentTableId = 'programDim';
    const query = `select program_id from ${datasetId}.${segmentTableId} where program_id = '${programId}'`;
    const rows = await getQueryResults<ProgramDimension>(query);

    return Promise.resolve((isArray(rows) && rows.length > 0));
}

export const manageDataWarehouseSegmentDimension = async (segmentId: string, segmentName: string, parentSegmentId: string, segmentType: SegmentType): Promise<any> => {
    const datasetId = 'catalystDW';
    const segmentTableId = 'segmentDim';

    const segmentRows: Array<SegmentDimension> = [];
    const segmentExists = await getSegmentExistsInBigQuery(segmentId);

    if (!segmentExists) {
        console.log(`Segment ${segmentId} DOES NOT exist!`);
        segmentRows.push({
            segment_id: segmentId,
            parent_segment_id: parentSegmentId,
            segment_name: segmentName,
            segment_type: segmentType
        } as SegmentDimension);

    } else {
        console.log(`Segment ${segmentId} exists!`);
    }

    if (segmentRows.length) {
        return bigqueryClient
            .dataset(datasetId)
            .table(segmentTableId)
            .insert(segmentRows)
            .then((rowCount) => {
                return {
                    rowCount,
                    errors: undefined
                }
            })
            .catch((e) => {
                console.log(e.errors);
                return {
                    rowCount: undefined,
                    errors: e.errors
                }
            });
    } else {
        return Promise.resolve({
            rowCount: 0,
            errors: undefined
        });
    }
}

export const manageDataWarehouseAthleteSegmentDimension = async (segmentId: string, athleteId: string): Promise<any> => {
    const datasetId = 'catalystDW';
    const segmentAthleteTableId = 'segmentAthleteDim';
    const segmentAthleteRows: Array<SegmentAthleteDimension> = [];

    const exists = await getSegmentAthleteExistsInBigQuery(segmentId, athleteId);
    if (!exists) {
        segmentAthleteRows.push({
            segment_id: segmentId,
            athlete_id: athleteId
        } as SegmentAthleteDimension);
    }

    if (segmentAthleteRows.length) {
        return bigqueryClient
            .dataset(datasetId)
            .table(segmentAthleteTableId)
            .insert(segmentAthleteRows)
            .then((rowCount) => {
                return {
                    rowCount,
                    errors: undefined
                }
            })
            .catch((e) => {
                console.log(e.errors);
                return {
                    rowCount: undefined,
                    errors: e.errors
                }
            });
    } else {
        return Promise.resolve({
            rowCount: 0,
            errors: undefined
        });
    }

}


export const manageDataWarehouseProgramDimension = async (programId: string, programName: string, programCategory: string): Promise<any> => {
    const datasetId = 'catalystDW';
    const programTableId = 'programDim';

    const programRows: Array<ProgramDimension> = [];
    const programExists = await getProgramExistsInBigQuery(programId);

    if (!programExists) {
        console.log(`Program ${programId} DOES NOT exist!`);
        programRows.push({
            program_id: programId,
            program_name: programName,
            program_category: programCategory
        } as ProgramDimension);

    } else {
        console.log(`Program ${programId} exists!`);
    }

    if (programRows.length) {
        return bigqueryClient
            .dataset(datasetId)
            .table(programTableId)
            .insert(programRows)
            .then((rowCount) => {
                return {
                    rowCount,
                    errors: undefined
                }
            })
            .catch((e) => {
                console.log(e.errors);
                return {
                    rowCount: undefined,
                    errors: e.errors
                }
            });
    } else {
        return Promise.resolve({
            rowCount: 0,
            errors: undefined
        });
    }
}

export const manageDataWarehouseProgramContentDimension = async (programId: string, programName: string, programCategory: string, contentEntryId: string, contentEntryName: string, contentEntryType: string): Promise<any> => {
    const datasetId = 'catalystDW';
    const programTableId = 'programDim';

    const programRows: Array<ProgramDimension> = [];
    const programExists = await getProgramExistsInBigQuery(programId);

    if (!programExists) {
        console.log(`Program ${programId} DOES NOT exist!`);
        programRows.push({
            program_id: programId,
            program_name: programName,
            program_category: programCategory
        } as ProgramDimension);

    } else {
        console.log(`Program ${programId} exists!`);
    }

    if (programRows.length) {
        return bigqueryClient
            .dataset(datasetId)
            .table(programTableId)
            .insert(programRows)
            .then((rowCount) => {
                return {
                    rowCount,
                    errors: undefined
                }
            })
            .catch((e) => {
                console.log(e.errors);
                return {
                    rowCount: undefined,
                    errors: e.errors
                }
            });
    } else {
        return Promise.resolve({
            rowCount: 0,
            errors: undefined
        });
    }
}