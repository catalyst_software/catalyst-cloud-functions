import { pubsub } from 'firebase-functions';
import { BigQuery } from '@google-cloud/bigquery';
import * as uuid from 'uuid';
import { ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { AppEngagementFact } from '../../../api/app/v1/callables/utils/warehouse/app-engagement-fact.interface';
import { AppEngagementTask } from '../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { bigqueryClient } from '../../../api/app/v1/callables/utils/big-query/initBQ';
import { getAthleteSegmentsFromBigQuery, finalizeTask } from './helpers/helpers';
import { Config } from './../../../shared/init/config/config';

export const appEngagmentBigQueryWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.AppEngagement, {regions: Config.regions})
    .onPublish(async (message) => {
        console.log(`Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.AppEngagement}`);
        if (message.data) {
            const datasetId = 'catalystDW';
            const tableId = 'appEngagementFact';
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as ScheduledTask;
            task.creationTimestamp = moment(task.creationTimestamp as any).toDate();
            console.log('JSON.parse(dataBuffer.toString(utf8))', task);

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.AppEngagement}`;
            logTaskQueueMessage(task, msg);

            const segments = await getAthleteSegmentsFromBigQuery(task);
            if (segments.length) {
                msg = `Found ${segments.length} segments for athlete ${task.athleteId}.  Proceeding with BigQuery update.`;
                logTaskQueueMessage(task, msg);

                // Firestore timestamps are already in utc
                const rows: Array<AppEngagementFact> = [];
                rows.push({
                    uuid: uuid.v4(),
                    app_engagement_type: (task.payload as AppEngagementTask).taskType as string,
                    global_session_id: task.payload.globalSessionID,
                    screen_session_id: task.payload.screenSessionID,
                    bundle_id: task.bundleId,
                    segment_id: null,
                    athlete_id: task.athleteId,
                    program_id: task.payload.programId || null,
                    evolution_instance_id: (task.payload as AppEngagementTask).evolutionInstanceId || null,
                    created_at: BigQuery.timestamp(new Date(task.creationTimestamp)),
                    device_type: task.deviceType,
                    device_manufacturer: task.deviceManufacturer,
                    device_model: task.deviceModel,
                    device_os: task.deviceOs,
                    device_os_version: task.deviceOsVersion,
                    device_sdk_version: task.deviceSdkVersion,
                    device_uuid: task.deviceUuid
                } as unknown as AppEngagementFact);

                segments.forEach((segment_id: string) => {
                    rows.push({
                        uuid: uuid.v4(),
                        app_engagement_type: (task.payload as AppEngagementTask).taskType as string,
                        global_session_id: task.payload.globalSessionID,
                        screen_session_id: task.payload.screenSessionID,
                        bundle_id: task.bundleId,
                        segment_id,
                        athlete_id: task.athleteId,
                        program_id: task.payload.programId || null,
                        evolution_instance_id: (task.payload as AppEngagementTask).evolutionInstanceId || null,
                        created_at: BigQuery.timestamp(new Date(task.creationTimestamp)),
                        device_type: task.deviceType,
                        device_manufacturer: task.deviceManufacturer,
                        device_model: task.deviceModel,
                        device_os: task.deviceOs,
                        device_os_version: task.deviceOsVersion,
                        device_sdk_version: task.deviceSdkVersion,
                        device_uuid: task.deviceUuid
                    } as unknown as AppEngagementFact);
                });

                const theRes = await bigqueryClient
                    .dataset(datasetId)
                    .table(tableId)
                    .insert(rows)
                    .then((rowCount) => {
                        return { 
                            rowCount, 
                            errors: undefined }
                    })
                    .catch((e) => {
                        console.log(e.errors);
                        return {
                            rowCount: undefined,
                            errors: e.errors
                        }
                    });

                if (theRes.rowCount !== undefined) {
                    msg = `Successfully migrated ${rows.length} AppEngagement facts to BigQuery`;
                    logTaskQueueMessage(task, msg);
                } else {
                    msg = `${theRes.errors.length} error(s) occurred when migrating AppEngagement facts to BigQuery`;
                    logTaskQueueMessage(task, msg);
                    theRes.errors.forEach((e: any) => {
                        msg = `Error: ${JSON.stringify(e)}`;
                        logTaskQueueMessage(task, msg);
                    });
                }

            } else {
                msg = `No segments found for athlete ${task.athleteId}`;
                logTaskQueueMessage(task, msg);
            }

            msg = `Task ${task.uid} completed successfully at ${new Date()}`;
            finalizeTask(task, true, msg);
            return true;
        }
        return false;
    });
