import { getAthleteDocFromUID, getGroupDocFromUID } from './../../../db/refs/index';
import { OnboardingInviteEmailPayload } from './../../../api/app/v1/sendGrid/conrollers/email';
import { sendOnboardingInviteComms } from './../../../api/app/v1/callables/utils/comms/send-onboarding-invite-comms';
import { OrganizationInterface } from './../../../models/organization.model';
import { pubsub } from 'firebase-functions';
import * as uuid from 'uuid';
import { ScheduledTaskWorkerTopicType } from '../../../db/taskQueue/enums/scheduled-task.enums';
import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { OnboardingInvitationTask } from '../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { finalizeTask } from './helpers/helpers';
import { createToken, ConsentRequestDocument, ConsentRequestResponse } from '../../../api/app/v1/callables/get-custom-auth-token';
import { getSharedSystemConfiguration } from './../../../api/app/v1/callables/utils/warehouse/olap/get-olap-jwt-package';
import { initialiseFirebase } from '../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../db/biometricEntries/enums/firestore-collections';
import { Athlete } from '../../../models/athlete/interfaces/athlete';
import { Group } from '../../../models/group/interfaces/group';
import { getOrganizationDocFromUID } from '../../../db/refs';
import { AthleteIndicator } from '../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { EmailMessageTemplate } from '../../../api/app/v1/sendGrid/templates/email-message-template';
import { KeyValuePair } from '../../../models/notifications/interfaces/schedule-notification-payload';
import { Config } from './../../../shared/init/config/config';

const createAthleteIndicator = (ath: Athlete, org: OrganizationInterface): AthleteIndicator => {
    return {
        uid: ath.uid,
        firstName: ath.profile.firstName,
        lastName: ath.profile.lastName,
        creationTimestamp: ath.metadata.creationTimestamp,
        isIpScoreIncrease: false,
        ipScore: ath.currentIpScoreTracking.ipScore,
        isIpScoreDecrease: false,
        email: ath.profile.email,
        profileImageURL: ath.profileImageURL || ath.profile.imageUrl || '',
        bio: ath.profile.bio || ath.profile.sport || 'Athlete',
        runningTotalIpScore: ath.runningTotalIpScore,
        subscriptionType: ath.profile.subscription.type,
        orgTools: org.orgTools || ath.orgTools || [],
        sport: ath.profile.sport || '',
        includeGroupAggregation: false,
        organizationId: org.uid,
        isCoach: false,
        firebaseMessagingIds: ath.firebaseMessagingIds
    };
}

const setNotificationTemplateOptions = (templateReplacements: { email: KeyValuePair[]; notifications: KeyValuePair[]; }, groupName: string) => {
    if (!templateReplacements.notifications.length) {
        templateReplacements.notifications = [
            {
                "value": groupName,
                "key": "$GROUP_NAME"
            }
        ];
    }
}

const setEmailMessageTemplateOptions = (emailMessageTemplate: EmailMessageTemplate, sendPushNotification: boolean): EmailMessageTemplate => {
    let theEmailMessageTemplate: EmailMessageTemplate;

    if (!emailMessageTemplate) {
        theEmailMessageTemplate = {
            templateId: 'd-45b9b00534944c5aa4eadd3c3ead0b3e',
            emailTemplateName: 'groupInvite',

            includePushNotification: sendPushNotification,
            notificationPath: '',
            notificationTemplate: 'groupInvite',
        };
    } else {
        theEmailMessageTemplate.templateId = emailMessageTemplate.templateId || 'd-45b9b00534944c5aa4eadd3c3ead0b3e';
        theEmailMessageTemplate.emailTemplateName = emailMessageTemplate.emailTemplateName || 'groupInvite';


        theEmailMessageTemplate.includePushNotification = emailMessageTemplate.includePushNotification || sendPushNotification;
        theEmailMessageTemplate.notificationPath = emailMessageTemplate.notificationPath || '';
        theEmailMessageTemplate.notificationTemplate = emailMessageTemplate.notificationTemplate || 'groupInvite';
    }

    return theEmailMessageTemplate
}

const getInitials = (name: string): string => {
    let initials = '';
    if (name) {
        const parts = name.trim().split(' ');
        initials += parts[0].charAt(0).toUpperCase();
        if (parts.length > 1) {
            initials += parts[1].charAt(0).toUpperCase();
        }
    }

    return initials;
}


export const onboardingInvitationWorker = pubsub
    ._topicWithOptions(ScheduledTaskWorkerTopicType.OnboardingInvitation, {regions: Config.regions})
    .onPublish(async (message) => {
        console.log(`Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.OnboardingInvitation}`);
        if (message.data) {
            const dataBuffer = Buffer.from(message.data, 'base64');

            const task = JSON.parse(dataBuffer.toString('utf8')) as ScheduledTask;
            task.creationTimestamp = moment(task.creationTimestamp as any).toDate();
            console.log('JSON.parse(dataBuffer.toString(utf8))', task);

            let msg = `Message recieved in pubsub topic ${ScheduledTaskWorkerTopicType.OnboardingInvitation}`;
            logTaskQueueMessage(task, msg);

            const payload = task.payload as OnboardingInvitationTask;
            const athleteId = payload.athleteId;
            const email = payload.email;
            const newOrgId = payload.organizationId;
            const newGroupId = payload.groupId;
            const senderId = payload.senderId;
            const senderName = payload.senderName;
            const guid = uuid.v4();

            // Create the JWT token
            try {
                const token = await createToken(true, athleteId, true, true, guid);
                msg = `Created token: ${token}`;
                logTaskQueueMessage(task, msg);
                // Create the link
                const config = await getSharedSystemConfiguration();
                const actionUrl = `${config.onboardingWithInviteUrl}`; //?uid=${athleteId}&orgId=${newOrgId}&groupId=${newGroupId}&token=${token}`;
                msg = `Created url: ${actionUrl}`;
                logTaskQueueMessage(task, msg);
                logTaskQueueMessage(task, actionUrl);

                // Add the security object to firestore collection
                const secToken = {
                    athleteId,
                    guid,
                    valid: true,
                    consentResponse: ConsentRequestResponse.RequestPending
                } as ConsentRequestDocument;

                const db = initialiseFirebase('API').db; 
                await db.collection(FirestoreCollection.ConsentRequestQueue)
                        .doc(guid)
                        .set(secToken)
                        .then((doc) => {
                            logTaskQueueMessage(task, 'Added security token.');
                            return;
                        });

                const org: OrganizationInterface = await getOrganizationDocFromUID(newOrgId);
                const group: Group = await getGroupDocFromUID(newGroupId);
                
                // Send the email
                const emailPayload = {
                    taskID: task.uid,
                    organizationID: newOrgId,
                    organizationName: org.name,
                    groupID: newGroupId,
                    groupName: group.groupName,
                    athleteID: athleteId,
                    actionUrl,
                    token,
                    sendPushNotification: false,
                    creationTimestamp: new Date(),
                    isOnboarding: true
                } as OnboardingInviteEmailPayload;

                const templateReplacements = { email: [], notifications: [] } 
                const ath = await getAthleteDocFromUID(athleteId)

                const userToEmail: AthleteIndicator = createAthleteIndicator(ath, org)

                const theUserFullName = `${userToEmail.firstName} ${userToEmail.lastName}`;
                const apiEndpointUrl = config.growthStudio.endpoint;
                let rootUrl = apiEndpointUrl.slice(0, apiEndpointUrl.indexOf('/api'));
                rootUrl += `/onboarding/join-group/?organizationId=${newOrgId}&groupId=${newGroupId}&athleteId=${athleteId}&token=${token}&actionUrl=${actionUrl}`;
                msg = `Setting root url for invite email to: ${rootUrl}`;

                logTaskQueueMessage(task, msg);

                const onboardingPayload: OnboardingInviteEmailPayload = {
                    ...emailPayload,
                    userFirstName: userToEmail.firstName,
                    userFullName: theUserFullName,
                    userFirebaseMessagingIds: ath.firebaseMessagingIds,
                    senderFullName: senderName,
                    senderInitials: getInitials(senderName),
                    rootUrl
                }

                setNotificationTemplateOptions(templateReplacements, group.groupName);

                const theEmailMessageTemplate = setEmailMessageTemplateOptions(undefined, false);

                const response = await sendOnboardingInviteComms(
                    onboardingPayload,
                    theEmailMessageTemplate,
                    templateReplacements,
                    userToEmail,
                    athleteId,
                    theUserFullName,
                    userToEmail.firstName
                );

                msg = `Send comms respone: ${JSON.stringify(response)}`;
                logTaskQueueMessage(task, msg);

                msg = `Task ${task.uid} completed successfully at ${new Date()}`;
                finalizeTask(task, true, msg);
                return true;
            } catch (err) {
                msg = `Task ${task.uid} failed at ${new Date()}.  Error: ${ err }`;
                finalizeTask(task, false, msg);
                return false;
            } 

        }
        return false;
    });
