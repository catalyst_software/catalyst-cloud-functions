import { sendNotification } from '../../../api/app/v1/controllers/utils/send-notification';
import { PushNotificationType } from '../../../models/enums/push-notification-type';
import { Athlete } from '../../../models/athlete/athlete.model';
import { KeyValuePair } from '../../../models/notifications/interfaces/schedule-notification-payload';
import { LogInfo } from '../../../shared/logger/logger';
export const sendMessage = (athlete: Athlete, type: PushNotificationType, actionAnalytics?: string, value?: string) => {
    const keyValuePairs: KeyValuePair[] = [
        {
            key: '$NAME',
            value: value ? value : athlete.profile.fullName
        },
    ];
    // if (actionAnalytics && actionAnalytics !== '') {
    //     console.log(actionAnalytics);
    //     keyValuePairs.push({
    //         key: 'actionAnalytics',
    //         value: actionAnalytics
    //     })
    // }
    const token =  athlete.firebaseMessagingIds && athlete.firebaseMessagingIds[0] || athlete.metadata.firebaseMessagingIds && athlete.metadata.firebaseMessagingIds[0] || athlete.metadata.firebaseMessagingId || '';
    if (token) {
        return sendNotification(type, keyValuePairs, token).then(response => {
            if (response) {
                return true;
            }
            else {
                return false;
            }
        });
    }
    else {
        LogInfo(`The token is empty.... Message not sent for Athlete with UID ${athlete.uid}`);
        return Promise.resolve(false);
    }
};
