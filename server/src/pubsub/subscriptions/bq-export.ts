import { pubsub } from 'firebase-functions';
import { Config } from './../../shared/init/config/config';


export const bqExport = pubsub
    ._topicWithOptions('BQ_Export', {regions: Config.regions})
    .onPublish(async message => {
        if (message.data) {
            // const logEntry = await Buffer.from(
            //     message.data,
            //     'base64'
            // ).toString()
            const dataBuffer = Buffer.from(message.data, 'base64');

            console.log('JSON.parse(dataBuffer.toString(ascii))', JSON.parse(dataBuffer.toString('ascii')))

            const logEntry = JSON.parse(dataBuffer.toString('ascii')).protoPayload;

            console.log('logEntry', logEntry)
            console.log(`Method: ${logEntry.methodName}`);
            console.log(`Resource: ${logEntry.resourceName}`);
            console.log(`Initiator: ${logEntry.authenticationInfo.principalEmail}`);

            return true



        }
        return false
    })
