import { ContentFeedCardPackageBase } from './../models/report-model';
import { FirestoreCollection } from './../db/biometricEntries/enums/firestore-collections';
import { initialiseFirebase } from '../shared/init/initialise-firebase';
/**
 * Responds to an HTTP request from Cloud Tasks and writes a notificaiton card to the 
 * notification card registry in firestore using data
 * from the request body.
 *
 * @param {object} req Cloud Function request context.
 * @param {object} req.body The request payload.

 * @param {object} res Cloud Function response context.
 */
export const scheduleNotificationCardDeliveryHttpHandler = async (req, res) => {
    console.log('Request recieved in scheduleNotificationCardDeliveryHttpHandler');
    try {
        const dataBuffer = Buffer.from(req.body, 'base64');
        const card = JSON.parse(dataBuffer.toString('utf8')) as ContentFeedCardPackageBase;
        console.log(`Cloud task http handler scheduleNotificationCardDeliveryHttpHandler called with payload: ${JSON.stringify(card)}`);

        const db = initialiseFirebase('API').db;
        let message = `Operation Successful.  Execution timestamp: ${new Date().getTime()}`;
        const code = await db.collection(FirestoreCollection.NotificationCardRegistry)
                                .add(card)
                                .then(() => {
                                    console.log('Card successfully added');

                                    return 200;
                                })
                                .catch((err) => {
                                    message = `Operation Failed: ${err.message}`;
                                    console.error(`Card write failed: ${message}`);

                                    return 400;
                                });
        res.status(code).send(message);
    } catch(err) {
        // Any status code other than 2xx or 503 will trigger the task to retry.
        res.status(400).send(err.message);
    }

}