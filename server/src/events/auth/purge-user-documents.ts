import * as functions from 'firebase-functions'

import { purgeAthleteAggregateDocuments, cleanUpAthleteRelatedDocuments } from '../../api/app/v1/controllers/utils/purgeAggregateDocuments';
import { removeUndefinedProps } from '../../db/ipscore/services/save-aggregates';

export const purgeUserCollections = functions.auth.user().onDelete(async (user, context) => {

    console.log('context => ', context)
    console.log('Purging documetns for user => ', user)
    const athleteUID = user.uid

    // const purgeAggregateResults = await purgeAthleteAggregateDocuments(athleteUID).catch((purgeAggregateDocumentsErr) => {
    //     removeUndefinedProps(purgeAggregateDocumentsErr)
    //     console.error('Error Purging Aggregate Documents', purgeAggregateDocumentsErr)
    // })
    // if (purgeAggregateResults) {
    //     removeUndefinedProps(purgeAggregateResults)
    //     console.log('Aggregate Documents Purged', purgeAggregateResults)
    // }

    // const purgeAthleteCollectionsResults = await cleanUpAthleteRelatedDocuments(athleteUID).catch((purgeAthleteCollectionsErr) => {
    //     removeUndefinedProps(purgeAthleteCollectionsErr)
    //     console.error('Error Purging Athlete Documents', purgeAthleteCollectionsErr)
    // })
    // if (purgeAthleteCollectionsResults) {
    //     removeUndefinedProps(purgeAthleteCollectionsResults)
    //     console.log('Athlete Document and Collections Purged', purgeAthleteCollectionsResults)
    // }

});