import { db } from "../../shared/init/initialise-firebase";
import { FirestoreCollection } from '../../db/biometricEntries/enums/firestore-collections';

import { Group } from "../../models/group/group.model";
import { Athlete } from "../../models/athlete/interfaces/athlete";

import { getDocumentFromSnapshot } from "./utils/get-document-from-snapshot";

import { LogInfo } from "../../shared/logger/logger";
import { getUserFullName } from "../../api/utils/getUserFullName";
import { BinaryBiometric } from "../../db/biometricEntries/well/aggregate/interfaces/athlete/athlete-binary-biometric-aggregate";
import { LifestyleEntryType } from "../../models/enums/lifestyle-entry-type";
import { getUniqueListBy } from "../../api/app/v1/callables/utils/getUniqueListBy";
import { AthleteIndicator } from "../../db/biometricEntries/well/interfaces/athlete-indicator";

export const updateAthleteIndicatorsSickPain = async (
  athleteUID: string,
  groupUIDs: Array<string>,
  theAthlete: Athlete,
  lifestyleEntryType: LifestyleEntryType,
  values: BinaryBiometric

):
  Promise<Array<any>> => {

  console.log('======>>>>> athleteUID', athleteUID)
  console.log('======>>>>> updateAthleteIndicatorsIPScore groupUIDs.....', groupUIDs)
  if (theAthlete) {
    console.warn('updateAthleteIPScoreValue', { uid: theAthlete.uid, name: getUserFullName(theAthlete) })

  } else {
    console.warn('updateAthleteIPScoreValue', { uid: athleteUID })

  }

  const groupDocsRefs = new Array<FirebaseFirestore.DocumentReference>();

  // Set up all the doc references for the the query to grab the groups by uid
  groupUIDs.forEach((groupUID) => {
    const ref = db
      .collection(FirestoreCollection.Groups)
      .doc(groupUID);
    groupDocsRefs.push(ref);
  })

  const theRes = await db.getAll(...groupDocsRefs)
    .then(async (groupDocs: FirebaseFirestore.DocumentSnapshot[]) => {

      LogInfo(`groups.length => ${groupDocs.length}`);

      const updates = await groupDocs
        .map((groupsDocSnap) => {
          if (!groupsDocSnap.exists) {
            console.warn('Group Document Does not Exist in FireStore', groupsDocSnap.id)
          }
          return groupsDocSnap
        })
        .filter(snap => snap.exists)
        .map(async (groupSnap: FirebaseFirestore.DocumentSnapshot) => {
          const payload = {
            message: '',
            athlete: undefined,
            athleteUID,
            groupUID: groupSnap.id
          }
          try {
            if (groupSnap.exists) {
              const group = getDocumentFromSnapshot(groupSnap) as Group;


              const allGroupAthletes = group.athletes
              const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

              if (allGroupAthletes.length !== groupAthletes.length) {
                console.warn('dup athletes removed', {
                  groupAthletesWithDups: allGroupAthletes.map((a) => {
                    return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                  }),
                  groupAthletesCleaned: groupAthletes.map((a) => {
                    return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                  })
                })
              }

              let theAthleteIndicator = groupAthletes.find((athleteIndicator) => athleteIndicator.uid === athleteUID || athleteIndicator.email === (theAthlete ? theAthlete.profile.email : ''))

              const theAthleteIndicatorIndex = groupAthletes.findIndex((athleteIndicator) => athleteIndicator.uid === athleteUID || athleteIndicator.email === (theAthlete ? theAthlete.profile.email : ''))

              console.log('======>>>>> updateing Group group', { name: group.groupName, uid: group.groupId })

              if (theAthleteIndicatorIndex < 0) {

                const orgId = (group.uid || group.groupId).split('-')[0]

                theAthleteIndicator = {
                  organizationId: orgId,
                  orgTools: undefined,
                  bio: theAthlete.profile.bio,
                  // organizationName: group.organizationName || 'UNKNOWN',
                  organizations: [{
                    organizationId: orgId, organizationName: group.organizationName || 'UNKNOWN', organizationCode: 'UNKNOWN', active: true,
                    joinDate: theAthlete.metadata.creationTimestamp
                  }],
                  uid: theAthlete.uid,
                  email: theAthlete.profile.email,
                  firstName: theAthlete.profile.firstName,
                  lastName: theAthlete.profile.lastName,
                  creationTimestamp: theAthlete.metadata.creationTimestamp,
                  profileImageURL: theAthlete.profile.imageUrl,

                  isCoach: theAthlete.isCoach,
                  includeGroupAggregation: theAthlete.includeGroupAggregation,
                  ipScore: theAthlete.currentIpScoreTracking.ipScore,

                  runningTotalIpScore: theAthlete.runningTotalIpScore,

                  subscriptionType: theAthlete.profile.subscription.type
                }

                switch (lifestyleEntryType) {
                  case LifestyleEntryType.Sick:

                    if (values.off) {
                      theAthleteIndicator.sick = false
                    } else {
                      theAthleteIndicator.sick = true
                    }

                    break;

                  case LifestyleEntryType.Period:

                    if (values.off) {
                      theAthleteIndicator.period = false
                    } else {
                      theAthleteIndicator.period = true
                    }

                    break;

                  default:
                    break;
                }

                groupAthletes.push(theAthleteIndicator)

                await groupSnap.ref.update({ athletes: groupAthletes })

                payload.message = 'updateAthleteIndicatorsIPScore: Athlete Inidicator ADDED to group'

                const { uid, recentEntries, firebaseMessagingIds, ...athleteIndicatorRest } = theAthleteIndicator
                payload.athlete = athleteIndicatorRest

                console.log(payload)

                return payload

              } else {

                if (!theAthleteIndicator.uid) {
                  theAthleteIndicator.uid = athleteUID
                }

                switch (lifestyleEntryType) {
                  case LifestyleEntryType.Sick:

                    if (values.off) {
                      theAthleteIndicator.sick = false
                    } else {
                      theAthleteIndicator.sick = true
                    }

                    break;

                  case LifestyleEntryType.Period:

                    if (values.off) {
                      theAthleteIndicator.period = false
                    } else {
                      theAthleteIndicator.period = true
                    }

                    break;

                  default:
                    break;
                }

                groupAthletes[theAthleteIndicatorIndex] = theAthleteIndicator

                await groupSnap.ref.update({ athletes: groupAthletes })

                payload.message = 'updateAthleteIndicatorsIPScore: Athlete Inidicator UPDATED in group'

                const { uid, firebaseMessagingIds, ...athleteIndicatorRest } = theAthleteIndicator
                payload.athlete = athleteIndicatorRest

                console.log(payload)
                return payload

              }
            } else {
              payload.message = 'Group Document does not exist'

              return payload
            }
          } catch (error) {
            throw error
          }


        });
      // merge all grouped promises (per group)
      const athleteGroups = [].concat(...updates) as [
        {
          message: string,
          athlete: Athlete,
          athleteUID: string,
          groupUID: string
        }]
      const res = await Promise.all(athleteGroups)
      return res
    })
    .then(async (updateStatus: Array<{
      message: string;
      athleteUID: string;
      groupUID: string;
    }>) => {
      // Get The Coach Documents form the Athletes Store

      console.log(updateStatus)

      return updateStatus;
    });

  return theRes
};
