import { getUserByEmail } from './../../api/app/v1/controllers/get-user-by-email'

import { firestore } from 'firebase-admin'
import { isArray } from 'lodash'

import { db } from '../../shared/init/initialise-firebase'
import { FirestoreCollection } from '../../db/biometricEntries/enums/firestore-collections'

import { Group } from '../../models/group/group.model'
import { Athlete } from '../../models/athlete/interfaces/athlete'

import { getAthleteDocFromUID } from '../../db/refs/index'
import { getDocumentFromSnapshot } from './utils/get-document-from-snapshot'

import { LogInfo } from '../../shared/logger/logger'
import { getDateTimeWithOffset } from '../../shared/utils/moment/get-date-timewith-offset'
import { UserRecord } from 'firebase-functions/lib/providers/auth'
import moment from 'moment'
import { AthleteIndicator } from '../../db/biometricEntries/well/interfaces/athlete-indicator'
import { getUniqueListBy } from '../../api/app/v1/callables/utils/getUniqueListBy'

export const resetAthleteIndicatorsDailyIPScore = async (
    groupUIDs: Array<string>,
    runningTotalIpScore: number,
    dailyIPScore: number,
    utcOffset: number
    //  athlete?: Athlete
): Promise<
    Array<{
        message: string
        athleteUID: string
        groupUID: string
    }>
> => {
    console.log(
        '======>>>>> updateAthleteIndicatorsIPScore groupUIDs...',
        groupUIDs
    )

    const groupDocsRefs = new Array<FirebaseFirestore.DocumentReference>()

    // Set up all the doc references for the the query to grab the groups by uid
    groupUIDs.forEach(groupUID => {
        const ref = db.collection(FirestoreCollection.Groups).doc(groupUID)
        groupDocsRefs.push(ref)
    })

    const theRes = await db
        .getAll(...groupDocsRefs)
        .then(async (groupDocs: FirebaseFirestore.DocumentSnapshot[]) => {
            LogInfo(`groups.length => ${groupDocs.length}`)

            const updates = groupDocs
                .map(groupsDocSnap => {
                    if (!groupsDocSnap.exists) {
                        console.warn(
                            'Group Document Does not Exist in FireStore',
                            groupsDocSnap.id
                        )
                    }
                    return groupsDocSnap
                })
                .filter(snap => snap.exists)
                .map(async (groupSnap: FirebaseFirestore.DocumentSnapshot) => {
                    const payload = {
                        message: '',
                        // athletes: [],
                        // athleteUID,
                        groupUID: groupSnap.id,
                    }

                    if (groupSnap.exists) {
                        const group = getDocumentFromSnapshot(
                            groupSnap
                        ) as Group

                        // const now = firestore.Timestamp.now()

                        // const startOfTodayWithOffset = getDateTimeWithOffset(
                        //     now,
                        //     utcOffset || 0
                        // )
                        //     .startOf('day')
                        //     .toDate() // new Date(new Date().setHours(0, 0, 0, 0)).toJSON()
                        // const lastIpScoreValidationUpdate = group.lastIpScoreValidationUpdate
                        //     ? getDateTimeWithOffset(
                        //           group.lastIpScoreValidationUpdate,
                        //           utcOffset || 0
                        //       )
                        //           .startOf('day')
                        //           .toDate()
                        //     : moment()
                        //           .startOf('day')
                        //           .subtract(1, 'day')
                        //           .toDate()

                        // console.log(
                        //     '------->>>>>>>>>>>> lastIpScoreValidationUpdate',
                        //     lastIpScoreValidationUpdate
                        // )
                        // console.log(
                        //     '------->>>>>>>>>>>> startOfTodayWithOffset',
                        //     startOfTodayWithOffset
                        // )

                        // if (
                        //     lastIpScoreValidationUpdate !==
                        //     startOfTodayWithOffset
                        // ) {
                        console.log(
                            '------->>>>>>>>>>>> UPDATING RUNNING TOTAL IPSCORE FOR ATHLETES IN GROUP'
                        )
                        const athleteIndicatorDocs: firestore.QueryDocumentSnapshot<firestore.DocumentData>[] =
                            await db
                                .collection(FirestoreCollection.Groups)
                                .doc(group.groupId)
                                .collection(FirestoreCollection.Athletes)
                                .get()
                                .then(docs => {
                                    return docs.docs
                                })
                                .catch(err => {
                                    console.error(
                                        'Failed to load group athletes from subCollection',
                                        err
                                    )
                                    return []
                                })

                        if (athleteIndicatorDocs.length === 0) {
                            payload.message = `No athletes to process for group ${group.groupId}`
                            return payload
                        } else {
                            for (const athleteIndicatorDoc of athleteIndicatorDocs) {
                                let athleteUID = ''
                                let athInd =
                                    athleteIndicatorDoc.data() as AthleteIndicator

                                console.log(
                                    `Attempting to load athlete document ${athInd.uid}`
                                )
                                const theAthlete = await getAthleteDocFromUID(
                                    athInd.uid
                                )
                                if (theAthlete) {
                                    console.log(
                                        `Athlete document ${athInd.uid} exists`
                                    )
                                    if (
                                        athInd.runningTotalIpScore !==
                                        theAthlete.runningTotalIpScore
                                    ) {
                                        console.log(
                                            `runningTotalIpScore discrepancy for athlete ${athInd.uid}. Proceeding to update indicator.`
                                        )
                                        athInd.runningTotalIpScore =
                                            theAthlete.runningTotalIpScore
                                        await athleteIndicatorDoc.ref.update({
                                            runningTotalIpScore:
                                                theAthlete.runningTotalIpScore,
                                            bio: theAthlete.profile.bio || '',
                                            sport:
                                                theAthlete.profile.sport || '',
                                        })
                                    } else {
                                        console.log(
                                            `runningTotalIpScore discrepancy for athlete ${athInd.uid} does not exist. Skipping indicator update.`
                                        )
                                    }
                                } else {
                                    console.log(
                                        `Athlete document ${athInd.uid} does not exist.  Skipping indicator update`
                                    )
                                }
                            }

                            // await groupSnap.ref.update({
                            //     lastIpScoreValidationUpdate: startOfTodayWithOffset,
                            // })

                            payload.message =
                                'updateAthleteIndicatorsIPScore: Athlete Indicators UPDATED in group'

                            return payload
                        }
                        // } else {
                        //     payload.message = 'Group Document does not need updating based on updateAthleteIndicatorsIPScore'

                        //     return payload
                        // }
                    } else {
                        payload.message = 'Group Document does not exist'

                        return payload
                    }
                })
            // merge all grouped promises (per group)
            const athleteGroups = [].concat(...updates) as [
                {
                    message: string
                    athlete: Athlete
                    athleteUID: string
                    groupUID: string
                }
            ]
            const res = await Promise.all(athleteGroups)
            return res
        })

    console.log('Complete, return response', theRes)

    return theRes
}
