import { getUserByEmail } from '../../api/app/v1/controllers/get-user-by-email';

import { firestore } from "firebase-admin";

import { db } from "../../shared/init/initialise-firebase";
import { FirestoreCollection } from '../../db/biometricEntries/enums/firestore-collections';

import { Group } from "../../models/group/group.model";
import { Athlete } from "../../models/athlete/interfaces/athlete";

import { getAthleteDocFromUID, getAthleteDocRef } from '../../db/refs/index';
import { getDocumentFromSnapshot } from "./utils/get-document-from-snapshot";

import { LogInfo } from "../../shared/logger/logger";
import { getDateTimeWithOffset } from '../../shared/utils/moment/get-date-timewith-offset';
import { UserRecord } from 'firebase-functions/lib/providers/auth';
import moment from 'moment';
import { AthleteIndicator } from '../../db/biometricEntries/well/interfaces/athlete-indicator';
import { getUniqueListBy } from '../../api/app/v1/callables/utils/getUniqueListBy';

export const clearAthleteIndicatorsDailyIPScore = async (
  groupUIDs: Array<string>
  //  athlete?: Athlete
): Promise<Array<{
  message: string;
  athleteUID: string;
  groupUID: string;
}>> => {

  console.log('======>>>>> updateAthleteIndicatorsIPScore groupUIDs...', groupUIDs)

  const groupDocsRefs = new Array<FirebaseFirestore.DocumentReference>();

  // Set up all the doc references for the the query to grab the groups by uid
  groupUIDs.forEach((groupUID) => {
    const ref = db
      .collection(FirestoreCollection.Groups)
      .doc(groupUID);
    groupDocsRefs.push(ref);
  })

  const theRes = await db.getAll(...groupDocsRefs)
    .then(async (groupDocs: FirebaseFirestore.DocumentSnapshot[]) => {

      LogInfo(`groups.length => ${groupDocs.length}`);

      const updates = await groupDocs
        .map((groupsDocSnap) => {
          if (!groupsDocSnap.exists) {
            console.warn('Group Document Does not Exist in FireStore', groupsDocSnap.id)
          }
          return groupsDocSnap
        })
        .filter(snap => snap.exists)
        .map(async (groupSnap: FirebaseFirestore.DocumentSnapshot) => {
          const payload = {
            message: '',
            // athletes: [],
            // athleteUID,
            groupUID: groupSnap.id
          }

          if (groupSnap.exists) {

            const batch = db.batch()

            const group = getDocumentFromSnapshot(groupSnap) as Group;

            const now = firestore.Timestamp.now()


            console.log('------->>>>>>>>>>>> UPDATING IPSCORE')
            const athleteIndicatorUpdates = await Promise.all(group.athletes.map(async (athInd) => {

              let athleteUID = ""


              if (athInd.uid === undefined || athInd.uid === '') {
                console.warn('------->>>>>>>>>>>> NO UID SET ON THIS INDICATOR', athInd)

                let user: UserRecord;


                console.warn('UID NOT SET ON ATHLETE INDICATOR')
                // user = await getUserByEmail(athInd.email)

                if (user !== undefined && user.uid) {
                  athInd.uid = user.uid
                  athleteUID = athInd.uid
                }

              }

              console.log('THE INDICATOR TO BE UPDATED', athInd)



              // Update The Indicator and Group
              const dailyIPScore = 0
              const runningTotalIpScore = 0

              athInd.ipScore = dailyIPScore
              athInd.runningTotalIpScore = runningTotalIpScore

              const theAthleteDocRef = athInd.uid !== '' ? await getAthleteDocRef(athInd.uid) : undefined
              batch.update(
                theAthleteDocRef,
                {
                  'currentIpScoreTracking.ipScore': 0,
                  ipScore: null,
                  runningTotalIpScore: 0

                }
              )


              const {
                //  firebaseMessagingIds, 
                ...athleteIndicatorRest } = athInd

              return athleteIndicatorRest

            }))

            const allGroupAthletes = athleteIndicatorUpdates
            const groupAthletes = getUniqueListBy(athleteIndicatorUpdates, 'uid') as AthleteIndicator[]

            if (allGroupAthletes.length !== groupAthletes.length) {
              console.warn('dup athletes removed', {
                groupAthletesWithDups: allGroupAthletes.map((a) => {
                  return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                }),
                groupAthletesCleaned: groupAthletes.map((a) => {
                  return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                })
              })
            }

            batch.update(groupSnap.ref, { athletes: groupAthletes })

            await batch.commit()
            
            payload.message = 'updateAthleteIndicatorsIPScore: Athlete Inidicators UPDATED in group'
          } else {
            console.log('------->>>>>>>>>>>> NOT UPDATING IPSCORE')
            payload.message = 'updateAthleteIndicatorsIPScore: Athlete Inidicators ALREADY updated'
          }


          return payload

        });
      // merge all grouped promises (per group)
      const athleteGroups = [].concat(...updates) as [
        {
          message: string,
          athlete: Athlete,
          athleteUID: string,
          groupUID: string
        }]
      const res = await Promise.all(athleteGroups)
      return res
    })
    .then(async (updateStatus: Array<{
      message: string;
      athleteUID: string;
      groupUID: string;
    }>) => {
      // Get The Coach Documents form the Athletes Store

      console.log(updateStatus)

      return updateStatus;
    });


  console.log('Complete, return response', theRes)

  return theRes
};
