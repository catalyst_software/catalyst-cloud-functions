
const functions = require('firebase-functions');
import { UserDimensions } from "firebase-functions/lib/providers/analytics";
import { firestore } from 'firebase-admin';
import moment from "moment";

import { Athlete } from './../../models/athlete/athlete.model';

import { EventTypes } from "../../models/enums/event-types";
import { db } from "../../shared/init/initialise-firebase";

import { FirestoreCollection } from './../../db/biometricEntries/enums/firestore-collections';

import { sendEmail } from "./send-email";
import { EmailMessage } from "../../api/app/v1/sendGrid/conrollers/email";
import { getCoachesToEMail as getCoachesToEmail } from './utils/get-coaches-to-email';
import { getAthleteDocRef } from '../../db/refs';
import { LifestyleEntryType, StackDriverTemplateIDs } from '../../models/enums/lifestyle-entry-type';
import { Config } from './../../shared/init/config/config';


const sgKey = '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'
const handledEvents = []

const message: EmailMessage = {
  personalizations: [
    {
      to: [
        {
          name: "Ian Kaapisoft",
          email: "ian.gouws@kaapisoft.com"
        },
        {
          name: "Ian iNSPIRE",
          email: "ian@inspiresportonline.com"
        }
      ]
    }
  ],
  from: {
    name: "iNSPIRE",
    email: "athleteservices@inspiresportonline.com"
  },
  templateId: "d-93756f51e55b4734a5b93d23e4310750",
  emailTemplateName: "badMood",
  dynamic_template_data: {

    name: "Ian Gouws!!",

    // subject: "Encoding bit's!!!",
    // message: "Mail TGemplate Update>",

    showDownloadButton: false,
    barType: {
      danger: true
    }
  }
}

/**
 * Sends a notification to a Coach 
 * WHEN an athlete logs ANY very sad mood entries and
 * WHEN an athlete logs ANY < happy mood entries 3 days in a row.
 * REAL TIME
 * URGENT
 * BRAIN
 * 
 * @param {string} uid The UID of the user.
 * @param {string} userLanguage The user language in language-country format.
 */
// export const biometricsMoodDistraught = functions.analytics
//   .event(EventTypes.biometricsMoodDistraught)
//   .onLog(async (event) => {


//     // WHEN an athlete logs ANY < happy mood entries 3 days in a row
//     // THEN the coach is notified
//     // REAL TIME
//     // URGENT
//     // BRAIN

//     const user = event.user as UserDimensions;
//     let athleteUID = user.userId;
//     if (!athleteUID) {
//       console.log('user.userId => ', user.userId)
//       console.log('event.params.USER_ID => ', event.params.USER_ID)
//       athleteUID = event.params.USER_ID
//     }



//     const { ORGANIZATION_ID: orgUID } = event.params;
//     // const coachesToEmail = await prepareNotificationsSend(orgUID, athleteUID);
//     const athleteGroups = await getAthleteGroups(athleteUID, orgUID);

//     const coachesToEmail = []
//       .concat(...athleteGroups
//         .filter((group) => group.athletes
//           .filter(athlete => athlete.uid !== athleteUID && athlete.isCoach))
//         .map(group => group.athletes)
//       ) as Array<AthleteIndicator>

//     if (coachesToEmail.length) {

//       const to = coachesToEmail
//         .map((coach: AthleteIndicator) => {
//           // console.log('Emailing Coach', coach);

//           return {
//             name: coach.firstName,
//             email: coach.email
//           }
//         });

//       message.personalizations[0].to = to

//       console.log('Emailing List', to);
//       // message.dynamic_template_data.name = user.userProperties.organizationName.value // athlete.profile.fullName
//       const athlete = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
//         return {
//           uid: docSnap.id,
//           ...docSnap.data() as Athlete
//         }
//       })
//       message.dynamic_template_data.name = athlete.profile.fullName

//       const notificationType = message.emailTemplateName === 'badMood' ? PushNotificationType.BiometricsMoodBad : PushNotificationType.BiometricsMoodDistraught

//       await sendEmail(message, sgKey)
//       coachesToEmail.forEach((coach) => {
//         return sendMessage(
//           coach,
//           notificationType
//         )
//       })

//     }
//     else {
//       console.warn(`No Coach found for Athlete having uid ${athleteUID}`);
//     }
//     console.log(`${EventTypes.biometricsMoodDistraught}  event raised`)
//     logUser(user, event.params);


//     return user
//   });

/**
 * Sends a notification to a Coach 
 * WHEN an athlete logs ANY < happy mood entries 3 days in a row.
 * REAL TIME
 * URGENT
 * BRAIN
 * 
 * @param {string} uid The UID of the user.
 * @param {string} userLanguage The user language in language-country format.
 */
export const biometricsMoodOK = functions.analytics
  .event(EventTypes.biometricsMoodOK)
  .onLog(async (event) => {

    const user = event.user as UserDimensions;
    const athleteUID = user.userId;
    // const purchaseValue = event.valueInUSD;
    // const userLanguage = user.deviceInfo.userDefaultLanguage;


    const { ORGANIZATION_ID: orgUID } = event.params;
    const coachesToEmail = await getCoachesToEmail(orgUID, athleteUID);

    if (coachesToEmail.length) {
      coachesToEmail.forEach((coach) => {
        console.log('Emailing Coach', coach);
      });

      const to = coachesToEmail.map((coach) => {
        console.log('Emailing Coach', coach);

        return {
          name: `${coach.firstName} ${coach.lastName}`,
          email: coach.email
        }
      });

      // message.personalizations[0].to = to

      const athlete = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
        return {
          uid: docSnap.id,
          ...docSnap.data() as Athlete
        }
      })
      message.dynamic_template_data.name = athlete.profile.fullName

      message.dynamic_template_data.message = EventTypes.biometricTrainingPainAbove2

      await sendEmail(to, message, sgKey)
    }

    console.log(`${EventTypes.biometricsMoodOK}  event raised`)
    logUser(user, event.params);

    // The name for the new dataset
    // const datasetName = 'events_20190330';

    // // Creates the new dataset
    // bigquery
    //   .createDataset(datasetName)
    //   .then(results => {
    //     const dataset = results[0];

    //     console.log(`Dataset ${dataset.id} created.`);
    //   })
    //   .catch(err => {
    //     console.error('ERROR:', err);
    //   });
    return user
  });

/**
 * Sends a notification to a Coach 
 * WHEN an athlete logs ANY < happy mood entries 3 days in a row.
 * REAL TIME
 * URGENT
 * BODY
 * 
 * @param {string} uid The UID of the user.
 * @param {string} userLanguage The user language in language-country format.
 */
export const biometricsMoodSad = functions.analytics
  .event(EventTypes.biometricsMoodSad)
  .onLog(async (event) => {

    const user = event.user as UserDimensions;
    const athleteUID = user.userId;
    // const purchaseValue = event.valueInUSD;
    // const userLanguage = user.deviceInfo.userDefaultLanguage;


    const { ORGANIZATION_ID: orgUID } = event.params;
    const coachesToEmail = await getCoachesToEmail(orgUID, athleteUID);

    if (coachesToEmail.length) {
      coachesToEmail.forEach((coach) => {
        console.log('Emailing Coach', coach);
      });

      const to = coachesToEmail.map((coach) => {
        console.log('Emailing Coach', coach);

        return {
          name: `${coach.firstName} ${coach.lastName}`,
          email: coach.email
        }
      });

      // message.personalizations[0].to = to
      const athlete = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
        return {
          uid: docSnap.id,
          ...docSnap.data() as Athlete
        }
      })
      message.dynamic_template_data.name = athlete.profile.fullName
      message.dynamic_template_data.message = EventTypes.biometricTrainingPainAbove2

      await sendEmail(to, message, sgKey)
    }

    console.log(`${EventTypes.biometricsMoodSad}  event raised`)
    logUser(user, event.params);

    return user
  });

/**
 * Sends a notification to a Coach 
 * WHEN an athlete logs a ‘5’ for fatigue
 * REAL TIME
 * URGENT
 * BODY
 * 
 * @param {string} uid The UID of the user.
 * @param {string} userLanguage The user language in language-country format.
 */
export const biometricsFatigueExtreme = functions.analytics
  .event(EventTypes.biometricsFatigueExtreme)
  .onLog(async (event) => {

    const user = event.user as UserDimensions;
    const athleteUID = user.userId;
    // const purchaseValue = event.valueInUSD;


    const { ORGANIZATION_ID: orgUID } = event.params;
    const coachesToEmail = await getCoachesToEmail(orgUID, athleteUID);

    if (coachesToEmail.length) {
      coachesToEmail.forEach((coach) => {
        console.log('Emailing Coach', coach);
      });

      const to = coachesToEmail.map((coach) => {
        console.log('Emailing Coach', coach);

        return {
          name: `${coach.firstName} ${coach.lastName}`,
          email: coach.email
        }
      });

      // message.personalizations[0].to = to
      const athlete = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
        return {
          uid: docSnap.id,
          ...docSnap.data() as Athlete
        }
      })
      message.dynamic_template_data.name = athlete.profile.fullName
      message.dynamic_template_data.message = EventTypes.biometricsFatigueExtreme

      await sendEmail(to, message, sgKey)
    }

    console.log(`${EventTypes.biometricsFatigueExtreme} event raised`)

    logUser(user, event.params);

    return user
  });

/**
 * Sends a notification to a Coach 
 * WHEN an athlete logs any ‘injury’ pain above a '2'
 * REAL TIME
 * URGENT
 * BODY
 * 
 * @param {string} uid The UID of the user.
 * @param {string} userLanguage The user language in language-country format.
 */
export const biometricInjuryPainAbove2 = functions.analytics
  .event(EventTypes.biometricInjuryPainAbove2)
  .onLog(async (event) => {

    const user = event.user as UserDimensions;
    const athleteUID = user.userId;
    // const purchaseValue = event.valueInUSD;
    // const userLanguage = user.deviceInfo.userDefaultLanguage;



    const { ORGANIZATION_ID: orgUID } = event.params;
    const coachesToEmail = await getCoachesToEmail(orgUID, athleteUID);

    if (coachesToEmail.length) {
      coachesToEmail.forEach((coach) => {
        console.log('Emailing Coach', coach);
      });

      const to = coachesToEmail.map((coach) => {
        console.log('Emailing Coach', coach);

        return {
          name: `${coach.firstName} ${coach.lastName}`,
          email: coach.email
        }
      });

      // message.personalizations[0].to = to
      const athlete = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
        return {
          uid: docSnap.id,
          ...docSnap.data() as Athlete
        }
      })
      message.dynamic_template_data.name = athlete.profile.fullName
      message.dynamic_template_data.message = EventTypes.biometricTrainingPainAbove2

      await sendEmail(to, message, sgKey)
    }

    console.log(`${EventTypes.biometricInjuryPainAbove2} event raised`)
    logUser(user, event.params);

    return user
  });

/**
 * Sends a notification to a Coach 
 * WHEN an athlete logs ‘Training’ pain above a '2'
 * REAL TIME
 * URGENT
 * TRAINING
 * 
 * @param {string} uid The UID of the user.
 * @param {string} userLanguage The user language in language-country format.
 */
export const biometricTrainingPainAbove2 = functions.analytics
  .event(EventTypes.biometricTrainingPainAbove2)
  .onLog(async (event) => {

    const user = event.user as UserDimensions;
    console.log(`${EventTypes.biometricTrainingPainAbove2}`)
    console.log(`event`, event)

    const athleteUID = user.userId;
    // const purchaseValue = event.valueInUSD;
    // const userLanguage = user.deviceInfo.userDefaultLanguage;
    console.log(`${EventTypes.biometricTrainingPainAbove2} event raised`)
    logUser(user, event.params);

    const currentDayTimestamp = firestore.Timestamp.fromDate(new Date(new Date().setHours(0, 0, 0, 0)))
    if (athleteUID && athleteUID !== '') {
      const biometricEntriesQuerySnapshot = await db.collection(FirestoreCollection.Athletes)
        .doc(athleteUID)
        .collection(FirestoreCollection.AthleteBiometricEntries)
        .where('lifestyleEntryType', '==', LifestyleEntryType.Training)
        .where('dateTime', '>=', currentDayTimestamp)
        .orderBy("dateTime", 'desc')
        .get()


      const { ORGANIZATION_ID: orgUID } = event.params;
      const coachesToEmail = await getCoachesToEmail(orgUID, athleteUID);

      if (coachesToEmail.length) {
        coachesToEmail.forEach((coach) => {
          console.log('Emailing Coach', coach);
        });

        const to = coachesToEmail.map((coach) => {
          console.log('Emailing Coach', coach);

          return {
            name: `${coach.firstName} ${coach.lastName}`,
            email: coach.email
          }
        });

        // message.personalizations[0].to = to
        const athlete = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
          return {
            uid: docSnap.id,
            ...docSnap.data() as Athlete
          }
        })
        message.dynamic_template_data.name = athlete.profile.fullName
        message.dynamic_template_data.message = EventTypes.biometricTrainingPainAbove2

        await sendEmail(to, message, sgKey)
      }
      // Set Notification Status as Sent
      if (biometricEntriesQuerySnapshot.size > 0) {
        biometricEntriesQuerySnapshot.docs.forEach((docSnap) => {
          const doc = {
            ...docSnap.data(),
            uid: docSnap.id
          }

          console.log('Biometric Entry Doc', doc)
        })
      } else {
        console.log('No Biometric Entry Documents found')
        console.log(`where uid == ${athleteUID}`)
        console.log('where lifestyleEntryType == Training')
        console.log(`where creationTimestamp >= ${currentDayTimestamp.toDate()}`)
      }
    } else {

      console.log('uid is => ', athleteUID)
    }

    return user
  });

/**
 * Sends a notification to a Coach 
 * WHEN an athlete logs more than 2 training sessions in a day
 * REAL TIME
 * URGENT
 * TRAINING
 * 
 * @param {string} uid The UID of the user.
 * @param {string} userLanguage The user language in language-country format.
 */
export const biometricTrainingCreated = functions.analytics

  .event(EventTypes.biometricTrainingCreated)
  .onLog(async (event) => {

    const user = event.user as UserDimensions;
    console.log(`${EventTypes.biometricTrainingCreated} event raised`)
    console.log(`event`, event)

    const handledEvent = handledEvents.find((theHandledEvent) => theHandledEvent.type === EventTypes.biometricTrainingCreated + '_' + user.userId)

    if (handledEvent) {
      console.log('Notification Event Already Handled')
      console.log('handledEvent', handledEvent)

    } else {
      handledEvents.push({ type: EventTypes.biometricTrainingCreated + '_' + user.userId })
      console.log('Adding Notification Event to  Already Handled List')
    }
    // const theUser = {
    //   userId: 'hMxBcYKCujgLdf10XwkbbAetbV82',
    //   deviceInfo:
    //   {
    //     deviceCategory: 'mobile',
    //     deviceModel: 'Android SDK built for x86',
    //     platformVersion: '8.1.0',
    //     resettableDeviceId: '64b83f8e-eb4d-4972-887e-cba87436d936',
    //     userDefaultLanguage: 'en-us'
    //   },
    //   geoInfo:
    //   {
    //     city: 'Bridgwater',
    //     continent: '154',
    //     country: 'United Kingdom',
    //     region: 'England'
    //   },
    //   appInfo:
    //   {
    //     appId: 'com.inspiresportonline.dev',
    //     appInstanceId: '0263efdc398a2f5c74f7d562f4ceea75',
    //     appPlatform: 'ANDROID',
    //     appStore: 'manual_install',
    //     appVersion: '1.3'
    //   },
    //   firstOpenTime: '2019-03-30T20:37:43.431Z',
    //   userProperties:
    //   {
    //     age: { value: '0', setTime: '2019-03-30T20:40:46.627Z' },
    //     dateOfBirth: { value: '30/03/2019', setTime: '2019-03-30T20:40:46.626Z' },
    //     first_open_time: { value: '1553979600000', setTime: '2019-03-30T20:37:46.686Z' },
    //     groupId: {
    //       value: 'gGyJCpIjJ7NOlMjQGeFK-G1',
    //       setTime: '2019-03-30T20:40:46.620Z'
    //     },
    //     groupName: { value: 'INSPIRE-G1', setTime: '2019-03-30T20:40:46.620Z' },
    //     isCoach: { value: 'true', setTime: '2019-03-30T20:40:46.625Z' },
    //     location: { value: 'Australia', setTime: '2019-03-30T20:40:46.626Z' },
    //     organizationId:
    //     {
    //       value: 'gGyJCpIjJ7NOlMjQGeFK',
    //       setTime: '2019-03-30T20:40:46.619Z'
    //     },
    //     organizationName:
    //     {
    //       value: 'Ian\'s Organization',
    //       setTime: '2019-03-30T20:40:46.619Z'
    //     },
    //     sport: { value: 'AFL', setTime: '2019-03-30T20:40:46.626Z' },
    //     user_id:
    //     {
    //       value: 'hMxBcYKCujgLdf10XwkbbAetbV82',
    //       setTime: '2019-03-30T21:59:26.778Z'
    //     }
    //   },
    //   bundleInfo: { bundleSequenceId: 16, serverTimestampOffset: 3256 }
    // }

    // const theEvent = {
    //   params:
    //   {
    //     USER_ID: 'HQqa052jP6dQhpexKMIHyGROwwL2',
    //     firebase_conversion: 1,
    //     firebase_event_origin: 'app',
    //     firebase_screen_class: 'NativeScriptActivity',
    //     firebase_screen_id: -766268318630115700
    //   },
    //   name: 'BIOMETRIC_TRAINING_CREATED',
    //   reportingDate: '20190330',
    //   logTime: '2019-03-30T23:13:57.264Z',
    //   previousLogTime: '2019-03-30T22:57:31.670Z',
    //   user: theUser
    // }
    const athleteUID = user.userId;
    // const purchaseValue = event.valueInUSD;
    // const userLanguage = user.deviceInfo.userDefaultLanguage;
    logUser(user, event.params);

    const biometricEntriesDate = moment()
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).toDate()

    const currentDayTimestamp = firestore.Timestamp.fromDate(biometricEntriesDate)
    if (athleteUID && athleteUID !== '') {


      const biometricEntriesQuerySnapshot = await db.collection(FirestoreCollection.Athletes)
        .doc(athleteUID)
        .collection(FirestoreCollection.AthleteBiometricEntries)
        .where('lifestyleEntryType', '==', LifestyleEntryType.Training)
        .where('dateTime', '>=', currentDayTimestamp)
        .orderBy("dateTime", 'desc')
        .get()

      const latestReportDate = moment()
        .subtract(1, 'day')
        .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })

      const threeDaysBackDate = moment()
        .subtract(3, 'days')
        .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })

      console.log('latestReportDate = (date) => ', latestReportDate.toDate())
      console.log('threeDaysBackDate - inclusive = (date) => ', threeDaysBackDate.toDate())

      console.log(`Number of training sessions logged today for athlete with having UID = (${athleteUID}) => ${biometricEntriesQuerySnapshot.size}`)





      // Set Notification Status as Sent

      if (biometricEntriesQuerySnapshot.size === 3) {
        const { ORGANIZATION_ID: orgUID } = event.params;
        const coachesToEmail = await getCoachesToEmail(orgUID, athleteUID);

        if (coachesToEmail.length) {
          coachesToEmail.forEach((coach) => {
            console.log('Emailing Coach', coach);
          });

          const to = coachesToEmail.map((coach) => {
            console.log('Emailing Coach', coach);

            return {
              name: `${coach.firstName} ${coach.lastName}`,
              email: coach.email
            }
          });

          // message.personalizations[0].to = to
          const athlete = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
            return {
              uid: docSnap.id,
              ...docSnap.data() as Athlete
            }
          })
          message.dynamic_template_data.name = athlete.profile.fullName

          await sendEmail(to, message, sgKey)
        }

        // sendEmail(message, sgKey)

        console.warn(`Athlete logged more than 2 training sessions in a day. athleteUID = (${athleteUID}) => count: ${biometricEntriesQuerySnapshot.size}`)
      }
      // if (biometricEntriesQuerySnapshot.size > 0) {

      //   // Set Notification Status as Sent

      //   biometricEntriesQuerySnapshot.docs.forEach((docSnap) => {
      //     // const doc = {
      //     //   ...docSnap.data(),
      //     //   uid: docSnap.id
      //     // }

      //     console.log('Biometric Entry DocID', docSnap.id)
      //   })
      // } 
      else if (biometricEntriesQuerySnapshot.size < 1) {
        console.log('No Biometric Entry Documents found')
        console.log('where lifestyleEntryType == Training')
        console.log(`where creationTimestamp >= ${currentDayTimestamp.toDate()}`)
      }
    } else {

      console.log('uid is => ', athleteUID)
    }

    return user
  });

// TODO: No EventType biometrics Sleep Less Than Five Hours 
// {biometricsSleepLessThanFiveHours}
// /**
//  * Sends a notification to a Coach 
//  * WHEN an athlete logs sleep less than 5 hours
//  * REAL TIME
//  * URGENT
//  * BRAIN
//  * 
//  * @param {string} uid The UID of the user.
//  * @param {string} userLanguage The user language in language-country format.
//  */
// export const biometricsSleepLessThanFourHours = functions.analytics

//   .event(EventTypes.biometricsSleepLessThanFourHours)
//   .onLog(async (event) => {

//     const biometricEntriesQuerySnapshot = await db.collection(FirestoreCollection.Athletes)
//       .doc('')
//       .collection(FirestoreCollection.AthleteBiometricEntries)
//       .where('lifestyleEntryType', '==', LifestyleEntryType.Training)
//       .where('creationTimestamp', '>=', firestore.Timestamp.fromDate(new Date(new Date().setHours(0, 0, 0, 0))))
//       .get()

//     // Set Notification Status as Sent

//     const user = event.user as UserDimensions;
//     const uid = user.userId;
//     const purchaseValue = event.valueInUSD;
//     const userLanguage = user.deviceInfo.userDefaultLanguage;

//     console.log(`${EventTypes.biometricsSleepLessThanFourHours} - user`, user)

//     return user
//   });


/**
* CHECK THESE
* 
* @param {string} uid The UID of the user.
* @param {string} userLanguage The user language in language-country format.
*/


export const biometricMoodCreated = functions.analytics
  .event(EventTypes.biometricMoodCreated)
  .onLog(async (event) => {

    const user = event.user as UserDimensions;
    const athleteUID = user.userId;

    console.log(`${EventTypes.biometricMoodCreated} event raised`)
    logUser(user, event.params);
    // const userLanguage = user.deviceInfo.userDefaultLanguage;


    const { ORGANIZATION_ID: orgUID } = event.params;
    const coachesToEmail = await getCoachesToEmail(orgUID, athleteUID);

    if (coachesToEmail.length) {
      coachesToEmail.forEach((coach) => {
        console.log('Emailing Coach', coach);
      });

      const to = coachesToEmail.map((coach) => {
        console.log('Emailing Coach', coach);

        return {
          name: `${coach.firstName} ${coach.lastName}`,
          email: coach.email
        }
      });

      // message.personalizations[0].to = to
      const athlete = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
        return {
          uid: docSnap.id,
          ...docSnap.data() as Athlete
        }
      })
      message.dynamic_template_data.name = athlete.profile.fullName
      message.dynamic_template_data.message = EventTypes.biometricMoodCreated

      await sendEmail(to, message, sgKey)
    }

    return user
  });



export const athleteOnboarded = functions.analytics
  .event('ATHLETE_ONBOARDED')
  .onLog((event) => {

    const user = event.user as UserDimensions;
    // const uid = user.userId; // The user ID set via the setUserId API.
    // // const purchaseValue = event.valueInUSD; // Amount of the purchase in USD.
    // const userLanguage = user.deviceInfo.userDefaultLanguage; // The user language in language-country format.

    console.log('ATHLETE_ONBOARDED event raised')

    // const userDimensions = {
    //   userId: 'jD41akrB9nSXWf7fwcmCo0tGs8W2',
    //   deviceInfo:
    //   {
    //     deviceCategory: 'mobile',
    //     deviceModel: 'Android SDK built for x86',
    //     platformVersion: '8.1.0',
    //     resettableDeviceId: '64b83f8e-eb4d-4972-887e-cba87436d936',
    //     userDefaultLanguage: 'en-us'
    //   },
    //   geoInfo:
    //   {
    //     city: 'Bridgwater',
    //     continent: '154',
    //     country: 'United Kingdom',
    //     region: 'England'
    //   },
    //   appInfo:
    //   {
    //     appId: 'com.inspiresportonline.dev',
    //     appInstanceId: 'c7e6861f3bf9f07b14d2d4e890c62117',
    //     appPlatform: 'ANDROID',
    //     appStore: 'manual_install',
    //     appVersion: '1.3'
    //   },
    //   firstOpenTime: '2019-03-30T19:32:12.704Z',
    //   userProperties:
    //   {
    //     first_open_time: { value: '1553976000000', setTime: '2019-03-30T19:32:15.852Z' },
    //     user_id: {
    //       value: 'jD41akrB9nSXWf7fwcmCo0tGs8W2',
    //       setTime: '2019-03-30T19:47:22.770Z'
    //     }
    //   },
    //   bundleInfo: { bundleSequenceId: 6, serverTimestampOffset: 3149 }
    // }
    logUser(user, event.params);
    return user
  });

export const organizationCodeValidated = functions.analytics
  .event('ORGANIZATION_CODE_VALIDATED')
  .onLog((event) => {

    const user = event.user as UserDimensions;
    // const uid = user.userId;
    // const purchaseValue = event.valueInUSD;
    // const userLanguage = user.deviceInfo.userDefaultLanguage;

    console.log('ORGANIZATION_CODE_VALIDATED event raised')
    logUser(user, event.params);


    return user
  });

export const biometricStepsCreated = functions.analytics
  .event('BIOMETRIC_STEPS_CREATED')
  .onLog((event) => {

    const user = event.user as UserDimensions;
    // const uid = user.userId;
    // const purchaseValue = event.valueInUSD;
    // const userLanguage = user.deviceInfo.userDefaultLanguage;

    console.log('BIOMETRIC_STEPS_CREATED event raised')
    logUser(user, event.params);

    return user
  });


/**
* Sends a notification to a Coach 
* WHEN an athlete logs a ‘5’ for fatigue
* REAL TIME
* URGENT
* BODY
* 
* @param {string} uid The UID of the user.
* @param {string} userLanguage The user language in language-country format.
*/
export const transactionValidationError = functions.analytics
  .event(EventTypes.transactionValidationError)
  .onLog(async (event) => {

    const user = event.user as UserDimensions;
    // const purchaseValue = event.valueInUSD;
    const params = event.params as {
      [key: string]: any;
    };

    // console.log('====================> params', params)

    // return true;
    const { ACCOUNT_ID: accountUID, MESSAGE: errorMessage, TRANSACTION_IDENTIFIER: transactionIdentifier } = params;
    const coachesToEmail = [
      {
        firstName: 'Chris',
        email: 'chris@inspiresportonline.com'

      },
      {
        firstName: 'Ian',
        email: 'ian@inspiresportonline.com'

      },
      // {
      //   firstName: 'Support',
      //   email: 'support@inspiresportonline.com'

      // },
    ];

    if (coachesToEmail.length) {
      coachesToEmail.forEach((coach) => {
        console.log('Emailing Coach', coach);
      });

      const to = coachesToEmail.map((coach) => {
        console.log('Emailing Coach', coach);

        return {
          name: `${coach.firstName}`,
          email: coach.email
        }
      });

      // message.personalizations[0].to = to
      // const athlete = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
      //   return {
      //     uid: docSnap.id,
      //     ...docSnap.data() as Athlete
      //   }
      // })
      message.dynamic_template_data.name = 'user'
      message.dynamic_template_data.message = EventTypes.transactionValidationError + ' <br> userUID => ' + accountUID + '<br> transactionIdentifier => ' + transactionIdentifier + '<br> errorMessage => ' + errorMessage
      message.emailTemplateName = 'subscriptionErrors'
      message.templateId = StackDriverTemplateIDs.TransactionEventLayout
      await sendEmail(to, message, sgKey)

      await sendEmail(to, message, sgKey)
    }

    console.log(`${EventTypes.biometricsFatigueExtreme} event raised`)

    logUser(user, event.params);

    return user
  });

export const iapInitialBuyFailed = functions.analytics
  .event(EventTypes.iapInitialBuyFailed)
  .onLog(async (event) => {

    const user = event.user as UserDimensions;

    console.log('====================> event', { event })

    // return true;
    const {
      USER_ID: userUID,
      USER_FULL_NAME: fullName,
      ORGANIZATION_ID: orgUID,
      GROUP_1_ID: groupUID,
      USER_ONBOARDING_CODE: userOnboardingCode,
      PRODUCT_IDENTIFIER: productIdentifier
    } = event.params;
    const coachesToEmail = [
      // {
      //   firstName: 'Chris',
      //   email: 'chris@inspiresportonline.com'

      // },
      {
        firstName: 'Ian',
        email: 'ian@inspiresportonline.com'

      },
      // {
      //   firstName: 'Support',
      //   email: 'support@inspiresportonline.com'

      // },
    ];

    if (coachesToEmail.length) {
      coachesToEmail.forEach((coach) => {
        console.log('Emailing Coach', coach);
      });

      const to = coachesToEmail.map((coach) => {
        console.log('Emailing Coach', coach);

        return {
          name: `${coach.firstName}`,
          email: coach.email
        }
      });

      // message.personalizations[0].to = to
      // const athletse = await getAthleteDocRef(athleteUID).get().then((docSnap) => {
      //   return {
      //     uid: docSnap.id,
      //     ...docSnap.data() as Athlete
      //   }
      // })
      message.dynamic_template_data.name = fullName
      message.dynamic_template_data.message = EventTypes.iapInitialBuyFailed + `
      userUID => ${userUID}
          
      productIdentifier => ${productIdentifier}
      userOnboardingCode => ${userOnboardingCode}

      orgUID =>  ${orgUID}
      groupUID => ${groupUID},
      
      
      
      event => ${JSON.stringify(event)}`

      message.emailTemplateName = 'subscriptionErrors'
      message.templateId = StackDriverTemplateIDs.TransactionEventLayout

      await sendEmail(to, message, sgKey)
    }

    console.log(`${EventTypes.biometricsFatigueExtreme} event raised`)

    logUser(user, event.params);

    return user
  });

function logUser(user: UserDimensions, params:
  {
    // ANALYTICS_FAMILY: 'BIOMETRICS',
    // GROUP_1_ID: 'etDXg4JEXDDV9Nq547dn-G1',
    // ORGANIZATION_ID: 'etDXg4JEXDDV9Nq547dn',
    // USER_ID: 'QD8br5r2m0ZI9rqBnxowVEgqPK12',
    // firebase_conversion: 1,
    // firebase_event_origin: 'app',
    // firebase_screen_class: 'NativeScriptActivity',
    // firebase_screen_id: -1511248844441723000
    ANALYTICS_FAMILY: string,
    GROUP_1_ID: string,
    ORGANIZATION_ID: string,
    USER_ID: string,
    firebase_conversion: number,
    firebase_event_origin: string,
    firebase_screen_class: string,
    firebase_screen_id: number
  }) {

  const {
    // ANALYTICS_FAMILY: analyticsFamily,
    // GROUP_1_ID: group1ID,
    // ORGANIZATION_ID: organizationID,
    USER_ID: userID,
    // firebase_conversion: firebaseConversion,
    // firebase_event_origin: firebaseEventOrigin,
    // firebase_screen_class: firebaseScreenClass,
    // firebase_screen_id: firebaseScreenID
  } = params
  console.log('params', params)
  console.log('user', user)
  console.log('USER_ID', userID)

  // console.log('analyticsFamily', analyticsFamily)
  // console.log('userId', userID)
  // console.log('group1ID', group1ID)
  // console.log('organizationID', organizationID)
  // console.log('firebaseConversion', firebaseConversion)
  // console.log('firebaseEventOrigin', firebaseEventOrigin)
  // console.log('firebaseScreenClass', firebaseScreenClass)
  // console.log('firebaseScreenID', firebaseScreenID)

  // console.log('organizationId', user.userProperties.organizationId.value)
  // console.log('organizationName', user.userProperties.organizationName.value)
  // console.log('groupId', user.userProperties.groupId.value)
  // console.log('groupName', user.userProperties.groupName.value)
  // console.log('dateOfBirth', user.userProperties.dateOfBirth.value)

  // console.log('deviceInfo', user.deviceInfo)
  // console.log('userLanguage', userLanguage)ks
  // console.log('geoInfo', user.geoInfo)
  // console.log('city', user.geoInfo.city)
  // console.log('userProperties', user.userProperties)
  // console.log('age', user.userProperties.age.value)
  // console.log('appVersion', user.appInfo.appVersion)
  // console.log('firstOpenTime', user.firstOpenTime)
}
