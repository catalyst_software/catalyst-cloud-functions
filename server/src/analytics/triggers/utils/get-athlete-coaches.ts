import { getGroupDocRef } from './../../../db/refs/index';
import { Organization } from './../../../models/organization.model';
import { getUserByEmail } from './../../../api/app/v1/controllers/get-user-by-email';

import { db } from "../../../shared/init/initialise-firebase";
import { FirestoreCollection } from '../../../db/biometricEntries/enums/firestore-collections';

import { LogInfo } from "../../../shared/logger/logger";

import { AthleteIndicator } from '../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { Group } from "../../../models/group/group.model";

import { getDocumentFromSnapshot } from "./get-document-from-snapshot";
import { getCoachConfigDocName } from '../../../api/app/v1/callables/utils/get-coach-config-doc-name';
import { NotificationsConfig, GroupNotificationConfig } from '../../../api/app/v1/controllers/athletes';
import { getMessagingIdsFromAthleteDoc, checkIfAthleteDocExists } from './getMessagingIdsFromAthleteDoc';
import { NonEngagementConfiguration } from '../../../models/organization.model';
import { getOrgDocRef } from '../../../db/refs';

export const setDefaultConfig = (groupId) => {
  const defaultConfig: GroupNotificationConfig = {
    groupId,
    danger: {
      sendEmail: true,
      sendPushNotification: true,
      sendSMS: false,
    },
    warn: {
      sendEmail: true,
      sendPushNotification: true,
      sendSMS: false,
    }, success: {
      sendEmail: true,
      sendPushNotification: true,
      sendSMS: false,
    },
    athletes: undefined

  }
  return defaultConfig
}

export const getAthleteCoaches = async (athleteUID: string, organizationId: string, groupUIDs: Array<string>,
  onboardingCode?: string):
  Promise<Array<{
    orgConfig: Array<{ orgID: string, nonEngagementConfiguration: NonEngagementConfiguration }>,
    coachRest: AthleteIndicator,
    skipComms: boolean
  }>> => {

  const groupDocsRefs = new Array<FirebaseFirestore.DocumentReference>();

  // Set up all the doc references for the the query to grab the groups by uid

  console.log('groupUIDs', groupUIDs)
  groupUIDs.forEach((groupUID) => {

    console.log('Adding groupUID to groupDocsRefs ', groupUID)
    console.log('FirestoreCollection ', FirestoreCollection.Groups)
    const ref = db
      .collection(FirestoreCollection.Groups)
      .doc(groupUID);
    groupDocsRefs.push(ref);
  })

  console.log('groupDocsRefs path', groupDocsRefs.map((ref) => ref.path))

  return await db.getAll(...groupDocsRefs)
    .then(async (groupDocs: FirebaseFirestore.DocumentSnapshot[]) => {

      LogInfo(`groups.length => ${groupDocs.length}`);


      const orgConfig: Array<{ orgID: string, nonEngagementConfiguration: NonEngagementConfiguration }> = []

      const groupedCoaches = await groupDocs
        .map((groupsDocSnap) => {
          if (!groupsDocSnap.exists) {
            console.warn('Group Document Does not Exist in FireStore', groupsDocSnap.id)
          }
          return groupsDocSnap
        })
        .filter(snap => snap.exists)
        .map(async (doc: FirebaseFirestore.DocumentSnapshot) => {

          const group = getDocumentFromSnapshot(doc) as Group;

          LogInfo(`retreived group => ${group.groupName} {${group.uid}}`);

          const theAthleteCoaches = await filterCoaches(onboardingCode, group, athleteUID);

          const getConfigResults: Array<Promise<{
            orgConfig: Array<{ orgID: string, nonEngagementConfiguration: NonEngagementConfiguration }>,
            coachRest: AthleteIndicator,
            skipComms: boolean
          }>> = await theAthleteCoaches.map(async (coach) => {


            const athleteDocExists = await checkIfAthleteDocExists(coach.uid, coach.email)

            if (athleteDocExists) {

              const theOrgID = organizationId || group.uid.split('-')[0]

              const orgDocSnapRef = getOrgDocRef(theOrgID)

              //TB - TODO: CHANGE THIS TO ORG NOTIFICATION CONFIG
              const groupConfig = await getCoachCommsConfigForGroup(coach, orgDocSnapRef, group);
              const configSet = orgConfig.find((conf) => conf.orgID === theOrgID)

              console.warn('configSet', configSet)

              const theOrg = await orgDocSnapRef.get().then((docSnap) => getDocumentFromSnapshot(docSnap)) as Organization

              if (!configSet) {
                orgConfig.push({
                  orgID: theOrgID,
                  nonEngagementConfiguration: theOrg.nonEngagementConfiguration
                })
              }

              //TB - TODO: THIS NEEDS TO BE ORG LEVEL NOTIFICATIONS CONFIG
              coach.commsConfig = groupConfig;

              // Remove Recent Entries
              const { recentEntries, ...coachRest } = coach

              console.log('Athlete Coach: ', coachRest)

              // Set UID on coach if it's not set
              if (!coachRest.uid) {
                console.log('coach.uid', coachRest.uid)
                console.log('coach.email', coachRest.email)

                if (coachRest.email) {
                  console.log('Getting Coach UID via email address', coachRest.email)
                  const userUID = await getUserByEmail(coachRest.email).then((user) => {
                    if (user) {
                      return user.uid
                    } {
                      console.log(`Can't find coach by email`)
                      return undefined
                    }
                  }).catch((ex) => {
                    console.log('ex', ex)
                    throw ex
                  })
                  coachRest.uid = userUID
                  console.log('updated coach', coachRest)

                } else {

                  console.error('Unable to find Coach UID using email as coachRest.email => ', coachRest.uid)

                }

              }

              return { coachRest, orgConfig, skipComms: false }

            } else {
              // if (coach.email) {

              //   console.error('Unable to find Coach UID using email as coachRest.email => ', coach.email)
              // } else {

              //   console.error('Unable to find Coach UID using email as coachRest.email => ', coach.uid)
              // }

              return { coachRest: coach, orgConfig, skipComms: true }
            }

          })

          return await Promise.all(getConfigResults)
        });
      // merge all grouped promises (grouped by Org group)
      const athleteCoaches = await Promise.all(groupedCoaches) // as Array<AthleteIndicator>

      const coachesToEmailByOrg = await Promise.all([].concat(...athleteCoaches)) as Array<{
        orgConfig: Array<{ orgID: string, nonEngagementConfiguration: NonEngagementConfiguration }>,
        coachRest: AthleteIndicator,
        skipComms: boolean
      }>
      return coachesToEmailByOrg
    })
    .then(async (coachesToEmailByOrg) => {

      console.log('athleteCoaches', coachesToEmailByOrg)
      // console.log('athleteCoaches length', coachesToEmailByOrg.length)


      const athleteCoachesSansUndefined = coachesToEmailByOrg.filter(coach => coach.coachRest !== undefined);

      // console.log('athleteCoachesSansUndefined length', athleteCoachesSansUndefined.length)

      // console.log('athleteCoaches where uid === undefined', athleteCoachesSansUndefined.filter(coach => !coach.coachRest.uid).length)
      const theCoaches = athleteCoachesSansUndefined


      // console.log('athleteCoaches where uid === undefined', coachesToEmailByOrg.filter(coach => !coach.coachRest.uid).length)

      // const theCoaches = await Promise.all(athleteCoaches)

      // console.log('======>>>>> Athlete Coaches To Email', theCoaches.map((coachMap) => {
      //   const coach = coachMap.coachRest;

      //   if (coach && coach.uid) {
      //     return {
      //       uid: coach.uid,
      //       organizationId: coach.organizationId,
      //       name: `${coach.firstName} ${coach.lastName}`,
      //       email: coach.email,
      //       isCoach: coach.isCoach,
      //       firebaseMessagingIds: coach.firebaseMessagingIds,
      //       commsConfig: coach.commsConfig
      //     }
      //   } else {
      //     return { message: 'Unable to load Coach from DB', coach }
      //   }

      // }))

      return theCoaches //.filter((coach) => coach.coachRest.uid !== undefined)

    }).catch((ex) => {
      console.error('db.getAll Exception', ex)
      throw ex
    });

};


export const getGroupCoaches = async (groupUID: string, onboardingCode?: string):
  Promise<Array<AthleteIndicator>> => {

  const groupDocsRefs = new Array<FirebaseFirestore.DocumentReference>();

  // Set up all the doc references for the the query to grab the groups by uid

  console.log('groupUID', groupUID)
  // groupUIDs.forEach((groupUID) => {

  //   console.log('Adding groupUID to groupDocsRefs ', groupUID)
  //   console.log('FirestoreCollection ', FirestoreCollection.Groups)
  const ref = db
    .collection(FirestoreCollection.Groups)
    .doc(groupUID);
  groupDocsRefs.push(ref);
  // })

  // console.log('groupDocsRefs path', groupDocsRefs.map((groupRef) => groupRef.path))


  return await db.getAll(...groupDocsRefs)
    .then(async (groupDocs: FirebaseFirestore.DocumentSnapshot[]) => {

      console.log(`groups.length => ${groupDocs.length}`);

      const groupedCoaches = await groupDocs
        .map((groupsDocSnap) => {
          console.log('groupsDocSnap')
          return groupsDocSnap
        })
        .filter(snap => snap.exists)
        .map(async (doc: FirebaseFirestore.DocumentSnapshot) => {

          console.warn('getting group from doc')
          const group = getDocumentFromSnapshot(doc) as Group;

          console.log(`retreived group => ${group.groupName} {${group.uid}}`);

          const theAthleteCoaches = await filterCoaches(onboardingCode, group, '');

          console.warn(`theAthleteCoaches`);
          const getConfigResults: Array<Promise<AthleteIndicator>> = await theAthleteCoaches
            .map(async (coach) => {

              const athleteDocExists = await checkIfAthleteDocExists(coach.uid, coach.email)

              if (athleteDocExists) {

                const orgDocSnapRef = getOrgDocRef(group.organizationId || group.uid.split('-')[0])

                console.warn(`getting group config => ${group.groupName} {${group.uid}}`);

                const groupConfig = await getCoachCommsConfigForGroup(coach, orgDocSnapRef, group);

                console.log(`group config retieved => `, groupConfig);
                coach.commsConfig = groupConfig;

                // Remove Recent Entries
                const { recentEntries, ...theCoach } = coach

                console.log('Athlete Coach with Config: ', theCoach)
                // Set UID on coach if it's not set
                if (theCoach && !theCoach.uid) {
                  console.log('coach.uid', theCoach.uid)
                  console.log('coach.email', theCoach.email)

                  if (theCoach.email) {
                    console.log('Getting Coach UID via email address', theCoach.email)
                    const userUID = await getUserByEmail(theCoach.email).then((user) => {
                      if (user) {
                        return user.uid
                      } {
                        console.log(`Can't find coach by email`)
                        return undefined
                      }
                    }).catch((ex) => {
                      console.error('ex', ex)
                      throw ex
                    })
                    theCoach.uid = userUID

                    console.log('updated coach', theCoach)

                  } else {
                    console.error('Unable to find Coach UID using email as coachRest.email => ', theCoach.email)
                    return theCoach
                  }

                  return theCoach


                } else {
                  return theCoach
                }
              } else {
                if (coach.email) {

                  console.error('Unable to find Coach UID using email as coachRest.email => ', coach.email)
                } else {

                  console.error('Unable to find Coach UID using email as coachRest.email => ', coach.uid)
                }
                return undefined
              }

            })

          return await Promise.all(await getConfigResults).then((otherRestults) => {
            console.warn('getConfigResults: ', otherRestults)

            return otherRestults
          })
        });
      // merge all grouped promises (grouped by Org group)
      const athleteCoaches = await Promise.all(groupedCoaches) // as Array<AthleteIndicator>

      return [].concat(...athleteCoaches)

    })
    .then(async (athleteCoaches: Array<AthleteIndicator>) => {

      console.log('athleteCoaches', athleteCoaches)

      const athleteCoachesSansUndefined = athleteCoaches.filter(coach => coach !== undefined);

      console.log('athleteCoachesSansUndefined length', athleteCoachesSansUndefined.length)

      console.log('athleteCoaches where uid === undefined', athleteCoachesSansUndefined.filter(coach => !coach.uid).length)
      const theCoaches = await Promise.all(athleteCoachesSansUndefined)

      console.log('======>>>>> Athlete Coaches To Email', theCoaches.map((coach) => {
        if (coach && coach.uid) {
          return {
            uid: coach.uid,
            organizationId: coach.organizationId,
            name: `${coach.firstName} ${coach.lastName}`,
            email: coach.email,
            isCoach: coach.isCoach,
            firebaseMessagingIds: coach.firebaseMessagingIds,
            commsConfig: coach.commsConfig
          }
        } else {
          return { message: 'Unable to load Coach from DB', coach }
        }

      }))

      return theCoaches //.filter((coach) => coach.uid !== undefined)
    }).catch((ex) => {
      console.error('db.getAll Exception', ex)
      throw ex
    });

};


async function filterCoaches(onboardingCode: string, group: Group, athleteUID: string) {



  let athIndicators

  let athleteIndicatorsWithoutUID
  if (group.athleteIndex) {
    athIndicators = await db.collection(FirestoreCollection.Groups).doc(group.groupId).collection('athletes').get().then((querySnap) => {
      if (querySnap.size) {
        return querySnap.docs.map((queryDocSnap) => {
          return queryDocSnap.data() as AthleteIndicator
        })
      } else {
        return []
      }
    })

    athleteIndicatorsWithoutUID = athIndicators.filter((athIndicator) => !athIndicator.uid)
    athIndicators = athIndicators

  } else {
    athleteIndicatorsWithoutUID = group.athletes.filter((athIndicator) => !athIndicator.uid)

    athIndicators = group.athletes
  }




  if (athleteIndicatorsWithoutUID.length) {
    await Promise.all(athleteIndicatorsWithoutUID.map(async (athleteIndicator) => {
      athleteIndicator.uid = await getUserByEmail(athleteIndicator.email).then(userRecord => userRecord.uid)
      return athleteIndicator
    }))

    // await getGroupDocRef(group.uid || group.groupId).update({ athletes: group.athletes })

  }

  return onboardingCode && onboardingCode.toUpperCase() === 'INSPIRE'
    // Include the generic iNSPIRE user in the coach list so to receive notifications
    ? athIndicators.filter((athlete) => athlete.isCoach)
    : athIndicators.filter((athlete) => athlete.uid !== athleteUID && athlete.isCoach);



}

// TB: RED FLAG CONFIG
function getExcludedRedFlags(org: Organization): Array<number> {
  let excluded: Array<number> = [];
  const redFlagConfig = org.redFlagNotifications;
  if (redFlagConfig) {
    if (!redFlagConfig.processMood) {
      excluded = excluded.concat([20, 21]);
    }
    if (!redFlagConfig.processFatigue) {
      excluded = excluded.concat([23]);
    }
    if (!redFlagConfig.processInjury || !redFlagConfig.processPain) {
      excluded = excluded.concat([24]);
    }
    if (!redFlagConfig.processSleep) {
      excluded = excluded.concat([22]);
    }
    if (!redFlagConfig.processExcessiveTraining) {
      excluded = excluded.concat([25]);
    }
    if (!redFlagConfig.processPeriod) {
      excluded = excluded.concat([26, 27]);
    }
    if (!redFlagConfig.processSickness) {
      excluded = excluded.concat([28, 29]);
    }
  }
  console.log(`TRAVIS********* Returning excluded values: ${JSON.stringify(excluded)}`);
  return excluded;
}

async function getCoachCommsConfigForGroup(coachIndicator: AthleteIndicator, orgDocSnapRef: FirebaseFirestore.DocumentReference,
  group: Group) {

  const coach = coachIndicator
  const { uid, firstName, lastName } = coach

  const coachConfigDocName = getCoachConfigDocName(firstName, lastName, uid)
  const orgSnap = await orgDocSnapRef.get()
  const org = getDocumentFromSnapshot(orgSnap) as any as Organization

  const coachConfigDocSnap = await orgDocSnapRef
    .collection(FirestoreCollection.CoachEssentialCommsConfiguration)
    .doc(coachConfigDocName).get();

  let config = getDocumentFromSnapshot(coachConfigDocSnap) as NotificationsConfig;

  let groupConfig: GroupNotificationConfig = (config && config.groups) ? config.groups.find((groupConf) => groupConf.groupId === group.groupId) : undefined;

  if (!groupConfig) {
    groupConfig = setDefaultConfig(group.uid);
    console.log('groupConfig', groupConfig);

    const { athletes, ...restOfConfig } = groupConfig

    const coachGroupConfig: GroupNotificationConfig = {
      ...restOfConfig as any
    }

    if (!coach.firebaseMessagingIds) {
      coach.firebaseMessagingIds = await getMessagingIdsFromAthleteDoc(coach.uid, coach.email);
    }

    const { firebaseMessagingIds } = coach
    config = {
      ...config,
      firebaseMessagingIDs: firebaseMessagingIds || [],
      groups: [coachGroupConfig]
    }

    const { commsConfiguration } = org
    if (commsConfiguration && commsConfiguration.excludedNotificationTypes) {
      config.excludedNotificationTypes = commsConfiguration.excludedNotificationTypes;

    };

    await coachConfigDocSnap.ref.set(config)
  }

  coach.firebaseMessagingIds = config.firebaseMessagingIDs;

  coach.excludedNotificationTypes = getExcludedRedFlags(org) // config.excludedNotificationTypes

  return groupConfig
}
