import { Organization } from "../../../models/organization.model";
import { AthleteIndicator } from "../../../db/biometricEntries/well/interfaces/athlete-indicator";

import { getAthleteGroupsViaOrg } from "./get-athlete-groups-via-org";
import { getAthleteCoachesFromOrg } from './get-athlete-coaches-from-org';
import { getOrgDocRef } from "../../../db/refs";

export const getCoachesToEMail = async (orgUID: string, athleteUID: string): Promise<Array<AthleteIndicator>> => {

  const org = await getOrgDocRef(orgUID).get().then((docSnap) => {
      return {
        ...docSnap.data(),
        uid: docSnap.id,
      } as Organization;
    });
  const athleteGroups = await getAthleteGroupsViaOrg(athleteUID, orgUID);
  const coachesToEmail = getAthleteCoachesFromOrg(athleteUID, athleteGroups, org);

  return coachesToEmail;
};
