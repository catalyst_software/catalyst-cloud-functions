import { db } from "../../../shared/init/initialise-firebase";
import { FirestoreCollection } from '../../../db/biometricEntries/enums/firestore-collections';
import { Athlete } from '../../../models/athlete/interfaces/athlete';
import { getDocumentFromSnapshot } from "./get-document-from-snapshot";

export const getMessagingIdsFromAthleteDoc = async (uid, email) => {
  const theCoach = await db.collection(FirestoreCollection.Athletes)
    .doc(uid).get().then((athDocSnap) => {
      const coach = getDocumentFromSnapshot(athDocSnap) as Athlete;
      if (coach === undefined) {
        console.error(`No coach document found for user with email ${email} and uid ${uid}`)
      }
      return coach;
    });
  return theCoach
    ? theCoach.firebaseMessagingIds || theCoach.metadata.firebaseMessagingIds || []
    : [];
};


export const checkIfAthleteDocExists = async (uid, email) => {

  const theCoach = await db.collection(FirestoreCollection.Athletes)
    .doc(uid).get().then((athDocSnap) => {
      if (!athDocSnap.exists) {
        console.error(`No coach document found for user with email ${email} and uid ${uid}`)
      }
      return athDocSnap.exists
    });

  return theCoach
};


