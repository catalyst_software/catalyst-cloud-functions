import { Organization } from "../../../models/organization.model";
import { Group } from "../../../models/group/group.model";
import { AthleteIndicator } from "../../../db/biometricEntries/well/interfaces/athlete-indicator";

export const getAthleteCoachesFromOrg = (athleteUID: string, athleteGroups: Group[], org: Organization): Array<AthleteIndicator> => {
  const coachesToEmail: Array<AthleteIndicator> = [];

  athleteGroups.forEach((group) => {
    org.coaches.forEach((coachEmail) => {
      const coach = group.athletes.filter((athlete) => athlete.uid !== athleteUID && athlete.email === coachEmail)[0];
      if (coach) {
        if (coachesToEmail.indexOf(coach) === -1) {
          coachesToEmail.push(coach);
          console.log("Adding Coach to coachesToEmail list", coach);
        }
        else {
          console.log("This Coach already exists in the coachesToEmail list", coach);
        }
      }
    });
  });
  return coachesToEmail;
};
