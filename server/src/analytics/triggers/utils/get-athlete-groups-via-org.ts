
import { Group } from "../../../models/group/group.model";

import { getDocumentFromSnapshot } from "./get-document-from-snapshot";
import { getGroupCollectionRef } from "../../../db/refs";

export const getAthleteGroupsViaOrg = async (athleteUID, orgUID): Promise<Array<Group>> => {
  return await getGroupCollectionRef()
    .where('organizationId', '==', orgUID)
    .get().then((querySnap) => {
      const groups = querySnap.docs.map((docSnap) => {
        const group = getDocumentFromSnapshot(docSnap) as Group;
        return group;
      });
      console.warn('orgGroups', groups);
      return groups
        .filter((group) => group.athletes
          .filter((athlete) => athlete.uid === athleteUID).length > 0 ? true : false);
    });
};
