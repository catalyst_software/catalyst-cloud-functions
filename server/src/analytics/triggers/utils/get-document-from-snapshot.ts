export const getDocumentFromSnapshot = (docSnapshot: FirebaseFirestore.DocumentSnapshot) => {

  if (!docSnapshot.exists) {

    return undefined
    
  } else {
    return {
      ...docSnapshot.data(),
      uid: docSnapshot.id
    };
  }
};
