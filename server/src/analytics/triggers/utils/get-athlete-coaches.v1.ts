import { getUserByEmail } from '../../../api/app/v1/controllers/get-user-by-email';

import { db } from "../../../shared/init/initialise-firebase";
import { FirestoreCollection } from '../../../db/biometricEntries/enums/firestore-collections';

import { LogInfo } from "../../../shared/logger/logger";

import { Athlete } from '../../../models/athlete/interfaces/athlete';
import { AthleteIndicator } from '../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { Group } from "../../../models/group/group.model";

import { getDocumentFromSnapshot } from "./get-document-from-snapshot";

export const getAthleteCoaches = async (athleteUID: string, groupUIDs: Array<string>, onboardingCode?: string):
  Promise<Array<AthleteIndicator>> => {

  const groupDocsRefs = new Array<FirebaseFirestore.DocumentReference>();

  // Set up all the doc references for the the query to grab the groups by uid

  console.log('groupUIDs', groupUIDs)
  groupUIDs.forEach((groupUID) => {

    console.log('Adding groupUID to groupDocsRefs ', groupUID)
    console.log('FirestoreCollection ', FirestoreCollection.Groups)
    const ref = db
      .collection(FirestoreCollection.Groups)
      .doc(groupUID);
    groupDocsRefs.push(ref);
  })

  // console.log('groupDocsRefs', groupDocsRefs.map((ref) => {

  // }))
  console.log('groupDocsRefs path', groupDocsRefs.map((ref) => ref.path))

  return await db.getAll(...groupDocsRefs)
    .then(async (groupDocs: FirebaseFirestore.DocumentSnapshot[]) => {

      LogInfo(`groups.length => ${groupDocs.length}`);

      const groupedCoaches = await groupDocs
        .map((groupsDocSnap) => {
          if (!groupsDocSnap.exists) {
            console.warn('Group Document Does not Exist in FireStore', groupsDocSnap.id)
          }
          return groupsDocSnap
        })
        .filter(snap => snap.exists)
        .map(async (doc: FirebaseFirestore.DocumentSnapshot) => {

          const group = getDocumentFromSnapshot(doc) as Group;

          LogInfo(`retreived group => ${group.groupName} {${group.uid}}`);

          const theAthleteCoaches = onboardingCode && onboardingCode.toUpperCase() === 'INSPIRE'
            // Include the generic iNSPIRE user in the coach list so to receive notifications
            ? group.athletes.filter((athlete) => athlete.isCoach)
            : group.athletes.filter((athlete) => athlete.uid !== athleteUID && athlete.isCoach);

          return theAthleteCoaches
        });
      // merge all grouped promises (grouped by Org group)
      const athleteCoaches = await Promise.all(groupedCoaches) //as Array<AthleteIndicator>

      const rerer = await [].concat(...athleteCoaches).map((async (coach) => {

        console.log('Athlete Coach: ', coach)
        if (!coach.uid) {
          console.log('coach.uid', coach.uid)
          console.log('coach.email', coach.email)

          if (coach.email) {
            console.log('Getting Coach UID via email address', coach.email)
            const userUID = await getUserByEmail(coach.email).then((user) => {
              if (user) {
                return user.uid
              } {
                console.log(`Can't find coach by email`)
                return undefined
              }
            }).catch((ex) => {
              console.log(ex)
            })
            coach.uid = userUID
            return coach
          }
          console.log('updated coach', coach)

          return coach


        } else {
          return coach
        }
      }))


      return await Promise.all([].concat(...rerer))

      // return [].concat(...athleteCoaches)
    })
    .then(async (athleteCoaches: Array<AthleteIndicator>) => {

      console.log('athleteCoaches', athleteCoaches)
      console.log('athleteCoaches lenght', athleteCoaches.length)


      const coaches = await athleteCoaches.filter((coach) => coach.uid)
        .map(async (coachIndicator) => {
          let theCoach: Athlete = undefined
          if (coachIndicator.uid) {
            theCoach = await db.collection(FirestoreCollection.Athletes)
              .doc(coachIndicator.uid).get().then((ath) => {
                return getDocumentFromSnapshot(ath) as Athlete
              })

            coachIndicator.firebaseMessagingIds = theCoach.firebaseMessagingIds || theCoach.metadata.firebaseMessagingIds
            // coaches.push(coachIndicator)
            return coachIndicator
          } else {

            console.log('coachIndicator.uid', coachIndicator.uid)
            return coachIndicator
          }

        })

      console.log('athleteCoaches where uid !== undefined', coaches)
      console.log('athleteCoaches where uid !== undefined lenght', coaches.length)
      const theCoaches = await Promise.all(coaches)

      console.log('======>>>>> Athlete Coaches To Email', theCoaches.map((coach) => {
        if (coach && coach.uid) {
          return {
            uid: coach.uid,
            organizationId: coach.organizationId,
            name: `${coach.firstName} ${coach.lastName}`,
            email: coach.email,
            isCoach: coach.isCoach,
            firebaseMessagingIds: coach.firebaseMessagingIds
          }
        } else {
          return { message: 'Unable to load Coach from DB', coach }
        }

      }))

      return theCoaches.filter((coach) => coach.uid !== undefined)
    }).catch((ex) => {
      console.error('db.getAll Exception', ex)
      throw ex
    });

};
