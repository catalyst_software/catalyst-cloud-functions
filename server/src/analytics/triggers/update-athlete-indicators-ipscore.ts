import { db } from "../../shared/init/initialise-firebase";
import { FirestoreCollection } from '../../db/biometricEntries/enums/firestore-collections';

import { Group } from "../../models/group/group.model";
import { Athlete } from "../../models/athlete/interfaces/athlete";
import { SubscriptionType } from "../../models/enums/enums.model";

import { getAthleteDocFromUID } from './../../db/refs/index';
import { getDocumentFromSnapshot } from "./utils/get-document-from-snapshot";

import { LogInfo } from "../../shared/logger/logger";
import { getUserFullName } from "../../api/utils/getUserFullName";
import { getUniqueListBy } from "../../api/app/v1/callables/utils/getUniqueListBy";
import { AthleteIndicator } from "../../db/biometricEntries/well/interfaces/athlete-indicator";

export const updateAthleteIndicatorsIPScoreOnGroups = async (
  athleteUID: string,
  groupUIDs: Array<string>,
  runningTotalIpScore: number,
  dailyIPScore: number,
  utcOffset: number,
  athlete?: Athlete):
  Promise<Array<any>> => {

  console.log('======>>>>> athleteUID', athleteUID)
  console.log('======>>>>> updateAthleteIndicatorsIPScore groupUIDs....', groupUIDs)
  if (athlete) {
    console.warn('updateAthleteIPScoreValue', { uid: athlete.uid, name: getUserFullName(athlete), dailyIPScore, runningTotalIpScore })

  } else {
    console.warn('updateAthleteIPScoreValue', { uid: athleteUID, dailyIPScore, runningTotalIpScore })
  }
  debugger;
  const groupDocsRefs = new Array<FirebaseFirestore.DocumentReference>();

  // Set up all the doc references for the the query to grab the groups by uid
  groupUIDs.forEach((groupUID) => {
    const ref = db
      .collection(FirestoreCollection.Groups)
      .doc(groupUID);
    groupDocsRefs.push(ref);
  })

  const theRes = await db.getAll(...groupDocsRefs)
    .then(async (groupDocs: FirebaseFirestore.DocumentSnapshot[]) => {

      LogInfo(`groups.length => ${groupDocs.length}`);

      const updates = await groupDocs
        .map((groupsDocSnap) => {
          if (!groupsDocSnap.exists) {
            console.warn('Group Document Does not Exist in FireStore', groupsDocSnap.id)
          }
          return groupsDocSnap
        })
        .filter(snap => snap.exists)
        .map(async (groupSnap: FirebaseFirestore.DocumentSnapshot) => {
          const payload = {
            message: '',
            athlete: undefined,
            athleteUID,
            groupUID: groupSnap.id
          }
          try {
            if (groupSnap.exists) {
              const group = getDocumentFromSnapshot(groupSnap) as Group;



              const allGroupAthletes = group.athletes
              const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

              if (allGroupAthletes.length !== groupAthletes.length) {
                console.warn('dup athletes removed', {
                  groupAthletesWithDups: allGroupAthletes.map((a) => {
                    return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                  }),
                  groupAthletesCleaned: groupAthletes.map((a) => {
                    return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                  })
                })
              }


              let theAthleteIndicator = groupAthletes.find((athleteIndicator) => athleteIndicator.uid === athleteUID || athleteIndicator.email === (athlete ? athlete.profile.email : ''))

              const theAthleteIndicatorIndex = groupAthletes.findIndex((athleteIndicator) => athleteIndicator.uid === athleteUID || athleteIndicator.email === (athlete ? athlete.profile.email : ''))


              console.log('======>>>>> updateing Group group', { name: group.groupName, uid: group.groupId })
              if (theAthleteIndicatorIndex < 0) {

                const orgId = (group.uid || group.groupId).split('-')[0]

                const subType = SubscriptionType.Free

                // const prepaidOrgIds = [
                //   '1WeXDuIn2iPlRm59q5T9',
                //   'rPSzxsM4vAZ2aNgD15qG',
                //   'nk5wL1g5iklmfxCs3VZT'
                // ]

                // if (prepaidOrgIds.indexOf(orgId)) {
                //   subType = SubscriptionType.Basic
                // }
                // TODO: check organizationName: group.organizationName || 'UNKNOWN',
                debugger;


                let theRunningTotalIpScore = runningTotalIpScore
                let theAthlete = athlete
                if (!theAthlete) {
                  theAthlete = await getAthleteDocFromUID(athleteUID)
                }
                if (theRunningTotalIpScore === undefined) {
                  theRunningTotalIpScore = theAthlete.runningTotalIpScore !== undefined ? theAthlete.runningTotalIpScore : 0
                }

                let theBio = theAthlete.profile.bio
                const NETBALLQUSER = theAthlete.organizationId === 'bNei5o6gOiXQ7xQo4gma'
                if (
                  // theAthleteIndicator.bio !== theAthlete.profile.bio && theAthleteIndicator.bio === 'Athlete'
                  NETBALLQUSER) {
                  theBio = theAthlete.profile.bio || theAthlete.profile.sport || theAthleteIndicator.bio || 'Athlete'
                }

                if (theBio.trim() === '') {
                  theBio = 'Athlete'
                }

                theAthleteIndicator = {
                  organizationId: orgId,
                  orgTools: undefined,
                  bio: theBio,
                  // organizationName: group.organizationName || 'UNKNOWN',
                  organizations: [{
                    organizationId: orgId, organizationName: group.organizationName || 'UNKNOWN', organizationCode: 'UNKNOWN', active: true,
                    joinDate: theAthlete.metadata.creationTimestamp
                  }],
                  uid: theAthlete.uid,
                  email: theAthlete.profile.email,
                  firstName: theAthlete.profile.firstName,
                  lastName: theAthlete.profile.lastName,
                  creationTimestamp: theAthlete.metadata.creationTimestamp,
                  profileImageURL: theAthlete.profile.imageUrl,

                  isCoach: theAthlete.isCoach,
                  includeGroupAggregation: theAthlete.includeGroupAggregation,
                  ipScore: dailyIPScore,

                  runningTotalIpScore: theRunningTotalIpScore,

                  subscriptionType: subType,

                  // TODO:
                  sick: false,
                  period: false
                }

                groupAthletes.push(theAthleteIndicator)

                await groupSnap.ref.update({ athletes: groupAthletes })

                payload.message = 'updateAthleteIndicatorsIPScore: Athlete Inidicator ADDED to group'

                const { uid, recentEntries, firebaseMessagingIds, ...athleteIndicatorRest } = theAthleteIndicator
                payload.athlete = athleteIndicatorRest

                console.log(payload)

                return payload

              } else {

                if (dailyIPScore !== theAthleteIndicator.ipScore || runningTotalIpScore !== theAthleteIndicator.runningTotalIpScore) {

                  // Update The Indicator and Group

                  theAthleteIndicator.ipScore = dailyIPScore

                  if (theAthleteIndicator.runningTotalIpScore === undefined) {

                    if (runningTotalIpScore !== undefined) {

                      theAthleteIndicator.runningTotalIpScore = runningTotalIpScore

                    } else {
                      const theAthlete = await getAthleteDocFromUID(athleteUID)

                      theAthleteIndicator.runningTotalIpScore = theAthlete.runningTotalIpScore !== undefined ? theAthlete.runningTotalIpScore : 0
                    }

                  } else
                    if (runningTotalIpScore !== undefined) {

                      if (runningTotalIpScore !== theAthleteIndicator.runningTotalIpScore) {
                        console.log('updateAthleteIndicatorsIPScore = current Running IPScore on Indicator', theAthleteIndicator.runningTotalIpScore)
                      }
                      console.log('Running IPScore', runningTotalIpScore)

                      theAthleteIndicator.runningTotalIpScore = runningTotalIpScore
                    }

                  if (!theAthleteIndicator.uid) {
                    theAthleteIndicator.uid = athleteUID
                  }

                  groupAthletes[theAthleteIndicatorIndex] = theAthleteIndicator
                 
                  await groupSnap.ref.update({
                    athletes: groupAthletes.map((a) => {
                      if (a.ipScore === undefined) {
                        a.ipScore = 0
                      }
                      return a
                    })
                  })

                  payload.message = 'updateAthleteIndicatorsIPScore: Athlete Inidicator UPDATED in group'


                } else {

                  payload.message = 'updateAthleteIndicatorsIPScore: Athlete Inidicator NOT UPDATED in group, Score Unchanged'

                  console.log('>>>>>>>>>>>>>>>>>>>>>>>>> current IPScore', dailyIPScore)
                }

                const { uid, firebaseMessagingIds, ...athleteIndicatorRest } = theAthleteIndicator
                payload.athlete = athleteIndicatorRest

                console.log(payload)
                return payload

              }
            } else {
              payload.message = 'Group Document does not exist'

              return payload
            }
          } catch (error) {
            throw error
          }


        });
      // merge all grouped promises (per group)
      const athleteGroups = [].concat(...updates) as [
        {
          message: string,
          athlete: Athlete,
          athleteUID: string,
          groupUID: string
        }]
      const res = await Promise.all(athleteGroups)
      return res
    })
    .then(async (updateStatus: Array<{
      message: string;
      athleteUID: string;
      groupUID: string;
    }>) => {
      // Get The Coach Documents form the Athletes Store

      console.log(updateStatus)

      return updateStatus;
    });

  return theRes
};
