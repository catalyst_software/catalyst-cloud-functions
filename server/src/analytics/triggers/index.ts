// import { pubsub } from 'firebase-functions'

// import { setAnalytics } from '../../api/app/v1/controllers/utils/send-notification'
// import { AndroidPurchaseReceipt } from './../../models/purchase/purchase.model'

// import { PlayStoreNotification } from '../../models/play-store-notification/interfaces/play-store-notification'
// import { AndroidNotificationType } from '../../models/enums/android-notification-type'
// import { db } from '../../shared/init/initialise-firebase'
// import { updateAthleteSubscriptionStatus } from '../../api/app/v1/controllers/utils/update-program-status'




exports.helloAnalytics = event => {
  const resource = event.resource;
  console.log(`Function triggered by the following event: ${resource}`);

  const analyticsEvent = event.data.eventDim[0];
  console.log(`Name: ${analyticsEvent.name}`);
  console.log(`Timestamp: ${new Date(analyticsEvent.timestampMicros / 1000)}`);

  const userObj = event.data.userDim;
  console.log(`Device Model: ${userObj.deviceInfo.deviceModel}`);
  console.log(`Location: ${userObj.geoInfo.city}, ${userObj.geoInfo.country}`);
};
