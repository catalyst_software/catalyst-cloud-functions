import { Response } from 'express';

/**
 * The object to returned to the caller 
 *
 * @export
 * @interface ResponseObject
 */
export interface LogDirective {
    message: string;
    data?: any;
    responseCode?: number;
    error?: {
        message: string;
        // TODO: fix typing?
        exception?: any;
        stack?: any;
    };
    level?: string;
    method?: {
        name: string;
        line: number;
    },
    res?: Response
}
