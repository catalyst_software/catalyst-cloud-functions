import { AthleteBiometricEntryModel } from '../../../db/biometricEntries/well/athelete-biometric-entry.model';
import { AggregateLogLevel } from "../../../db/biometricEntries/enums/aggregate-log-level";
import { LogLevel } from "../../../db/biometricEntries/enums/log-level";

/**
 * The object to passed around
 *
 * @export
 * @interface SocketLogDirectives
 */
export interface SocketLogDirective {
    type?: AggregateLogLevel;
    message: string;
    data?: {
        message?: string;
        documentId?: string

        updates?: [{ key: string, values: any[] }];

        collection?: any; // AggregateCollection;
       
        queue?: any;
        savedValue?: any;
        entry?: AthleteBiometricEntryModel
      
        document?: any; // AthleteWeeklyAggregate | AthleteMonthlyAggregate | GroupWeeklyAggregate | GroupMonthlyAggregate;
        historyDocument?: any; // AthleteWeeklyHistoryAggregateInterface
     
        rest?: any,
    };
    level?: LogLevel;
}
