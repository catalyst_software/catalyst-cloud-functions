import { Logger } from 'winston'
import _ = require('lodash');

import { LogDirective } from './interfaces/log-directive'
import { LogLevel } from '../../db/biometricEntries/enums/log-level'

// import { initialiseLoggers } from "./service/logger-service";
// import { database } from 'firebase-admin';
import { AggregateLogLevel } from '../../db/biometricEntries/enums/aggregate-log-level'
import { SocketLogDirective } from './interfaces/socket-log-directive'
import { initialiseLoggers } from './service/logger-service'
import { convertToPlainObject } from '../utils'

export const socket = require('socket.io-client')('http://97.74.4.116:8881');

let selectedLogger: Logger;
// let forceLog = false;

/**
 * Set up logger to be used
 * @export
 * @param {Observable<any>} docManager
 * @param {winston.Logger} customLogger
 */
export const setLogger = (customLogger: Logger) => {
    // Get the logger we configured with the id from the container.
    // Now we can use the logger we got from the container.
    selectedLogger = customLogger;

    socket.on('connect', function () {
        //
    });
    socket.on('event', function () {
        //
    });
    socket.on('disconnect', function () {
        //
    })
};
/**
 * Set custom log level at runtime
 *
 * @param customLevel
 */
export const setLoggerLevel = (customLevel: string) => {
    // Get the logger we configured with the id from the container.
    // Now we can use the logger we got from the container.
    if (selectedLogger === undefined) initialiseLoggers();
    // TODO:
    else
        selectedLogger.transports.forEach(transport => {
            transport.level = customLevel
        })
};

// function writeLog(logDirective: LogDirective, logLevel: LogLevel) {
//     const { message, ...rest } = logDirective
//     socket.emit(AggregateLogLevel.Info, {
//         type: AggregateLogLevel.Info,
//         message,
//         data: {
//             rest,
//         },
//         level: logLevel ? logLevel : LogLevel.Sockets,
//     })

//     console.log({
//         type: AggregateLogLevel.Info,
//         message,
//         data: {
//             rest,
//         },
//         level: logLevel ? logLevel : LogLevel.Sockets,
//     })

//     // if (selectedLogger) {
//     //     // console.log('selectedLogger.log')
//     //     selectedLogger.log(logLevel, logDirective.message ? logDirective.message : 'N/A', logDirective.data ? logDirective.data : 'N/A')

//     // } else {
//     //     // console.log('initialiseLoggers().log')

//     //     initialiseLoggers().log(logLevel, 'initialiseLoggers -------     logger!!' + logDirective.message, logDirective.data)

//     // }
// }

export function Log(
    logMeta: SocketLogDirective,
    logLevel?: LogLevel, // | string,
    extraMeta?: any
): any {
    const { type, message, level, ...rest } = logMeta;

    const socketLogLevel = type ? type : AggregateLogLevel.Info;
    // if (process.env.development || forceLog) {
    let extraMetaData;
    if (extraMeta === undefined) {
        extraMetaData = ''
    } else extraMetaData = extraMeta;
    // const extra = isObject(extraMeta) ?

    console.log(logMeta)

    // if (rest && rest.data && !_.isEmpty(rest.data)) {
    //     console.log(`type: socketLogLevel`, [
    //         message,
    //         'data',
    //         rest.data,
    //         ...extraMetaData
    //     ])
    // } else {
    //     if (_.isEmpty(rest.data)) {
    //         console.log(`type: socketLogLevel`, [
    //             message,
    //             ...extraMetaData
    //         ])
    //     }
    //     else console.log(`type: socketLogLevel`, [
    //         message,
    //         'data',
    //         rest.data || rest,
    //         ...extraMetaData
    //     ])
    // }

    // level: logLevel ? logLevel : LogLevel.Sockets

    // }

    socket.emit(socketLogLevel, {
        type: socketLogLevel,
        message,
        data: {
            ...rest.data,
            ...extraMetaData,
        },
        level: logLevel ? logLevel : LogLevel.Sockets,
    })
}

function castToDirective(
    logMeta: string | LogDirective,
    extraMeta: any,
    logLevel?: LogLevel
): SocketLogDirective {
    try {
        if (!_.isObject(logMeta)) {
            const { type, ...data } = convertToPlainObject(extraMeta);

            const directive: SocketLogDirective = {
                type: type ? type : AggregateLogLevel.Info,
                message: logMeta as any,
                data: { ...data },
                level: logLevel ? logLevel : LogLevel.Debug,
            };

            Log(directive);

            return directive
        } else {
            let { data } = logMeta as any;

            data = data ? data : {};

            // const logMetaDirective: LogDirective = logMeta as any

            const logDirective: LogDirective = {
                data,
                // error: {
                //     message: 'string',
                //     exception: 'any',
                // },
                // method: {
                //     name: 'string',
                //     line: 6,
                // },
                responseCode: 100,
                ...(logMeta as any),
            };

            // writeLog(logDirective, LogLevel.Debug)
            Log(
                {
                    ...(logDirective as SocketLogDirective),
                    type: AggregateLogLevel.Info,
                },
                logLevel ? logLevel : LogLevel.Info
            );

            const directive: SocketLogDirective = {
                type: AggregateLogLevel.Info,
                message: logMeta as any,
                data,
                level: LogLevel.Debug,
            };

            return directive
        }
    } catch (error) {
        debugger;
        return error
    }
}

export function LogInfo(
    logMeta: LogDirective | string,
    extraMeta?: any
): boolean {
    // Log(
    console.log(logMeta, JSON.stringify(extraMeta) || '')
    if (extraMeta !== undefined) {
        castToDirective(
            logMeta,
            { ...extraMeta, type: AggregateLogLevel.Info },
            LogLevel.Debug
        )
    } else {
        castToDirective(
            logMeta,
            { type: AggregateLogLevel.Info },
            LogLevel.Debug
        )
    }

    return true
}
export function LogError(
    logMeta: LogDirective | string,
    extraMeta?: any
): boolean {
    Log(castToDirective(logMeta, extraMeta ? extraMeta : {}, LogLevel.Error));

    return true
}

// console.debug = function() {
//     function clear(o) {

//       var obj = JSON.parse(JSON.stringify(o));
//       // [!] clone

//       if (obj && typeof obj === 'object') {
//           obj.__proto__ = null;
//           // clear

//           for (let j in obj) {
//             obj[j] = clear(obj[j]); // recursive
//           }
//       }
//       return obj;
//     }
//     const  args = Array.prototype.slice.call(arguments, 0)

//     for (let i = 0, args; i < args.length; i++) {
//       args[i] = clear(args[i]);
//     }
//     console.log.apply(console, args);
//   };
