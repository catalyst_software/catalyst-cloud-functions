import { Logger, createLogger, format } from 'winston'; //https://github.com/winstonjs/winston

import { LogLevel } from '../../../db/biometricEntries/enums/log-level';

import { setLogger } from '../logger';

const { LoggingWinston } = require('@google-cloud/logging-winston');

let dbLogger;

// enum LoggerLevels {
//     general,
//     stackdriver
// }

/**
 * System wide logger
 * https://github.com/winstonjs/winston
 * 
 * @param logLevel - The Level of which logs will be logged to Stackdriver
 * @returns winston.Logger
 */
export function initialiseLoggers(
    logLevel?: string, // General logging level
    from?
): Logger {


    if (!dbLogger) {
        dbLogger = createLogger({
            level: getLogLevel(),
            format:
                format.combine(
                    format.json(),
                    format.timestamp({
                        format: 'DD-MM-YYYY HH:mm:ss'
                    }),
                    // Format of the JSON output
                    format.printf(info => `${info.timestamp} (does this ever get called?): ${info.level}: ${info.message}`)
                ),
            transports: [
                // Transport Log to Stackdriver
                new LoggingWinston({
                    labels: {
                        name: 'Inspire Functions',
                        version: '0.1.0'
                    },
                    level: getLogLevel(),
                    handleExceptions: true,
                    // serviceContext: {
                    //     service: 'inspire-service', // required to report logged errors
                    //     // to the Google Cloud Error Reporting console
                    //     version: 'inspire-version'
                    // }
                })
            ],
            exitOnError: false
        });
    }


    // dbLogger.info(
    // 'Hello form dbLogger!!!!!!!!!!',
    // { hello: 'World' } })

    // jsonPayload: {
    //     message: "Hello form dbLogger!!!!!!!!!!"
    //     metadata: {
    //         hello: "World"
    //         timestamp: "12-12-2018 00:12:39"
    //     }
    // }

    // Trace vlues only available in Stackdriver ;)
    // dbLogger.info('Log entry with custom trace value', {
    //     [LoggingWinston.LOGGING_TRACE_KEY]: 'custom-trace-value'
    // });

    setLogger(dbLogger);

    return dbLogger;

    function getLogLevel(): string {
        // Error Levels Order
        // error: 0
        // warn: 1
        // info: 2
        // verbose: 3
        // debug: 4
        // silly: 5

        // // // severity:  "ERROR"  
        // dbLogger.log(LogLevel.Error, 'dbLogger.log(error)')

        // // // severity:  "WARNING"  
        // dbLogger.log(LogLevel.Warn, 'dbLogger.log(warn)')

        // // // severity:  "INFO"  
        // dbLogger.log(LogLevel.Info, 'dbLogger.log(info)')

        // // // severity:  "DEBUG"  
        // dbLogger.log(LogLevel.Debug, 'dbLogger.log(debug)')
        // dbLogger.log(LogLevel.Verbose, 'dbLogger.log(verbose)')

        // Setting the default level: 'info'  Error Levels 1 >= { warn }
        return logLevel ? logLevel : LogLevel.Info;
    }
}
