import { Response } from 'express';

export interface ResponseObject {
    message: string;
    data?: any;
    responseCode?: number;
    error?: {
        message: string;
        // TODO: fix typing?
        exception?: any;
        stack?: any;
    };
    level?: string;
    method?: {
        name: string;
        line: number;
    };
    res?: Response;
}

