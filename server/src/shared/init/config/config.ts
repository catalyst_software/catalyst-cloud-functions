export interface CloudFunctionsConfig {
    project: ExistingGoogleProjects
    regions: string[]
    loadSecondaryProject?: boolean
    secondaryProject?: ExistingGoogleProjects
    isDebugging?: boolean
}

export enum ExistingGoogleProjects {
    Production = 'catalyst-31efe',
}

const ProdConfig = {
    project: 'catalyst-31efe',
    regions: ['us-central1'],
    loadSecondaryProject: false,
    isDebugging: true,
} as CloudFunctionsConfig

// const ProdConfigHttpCallables = {
//     project: 'catalyst-31efe',
//     regions: [''us-central1'],
//     loadSecondaryProject: true,
//     secondaryProject: ExistingGoogleProjects.Secondary1
// } as CloudFunctionsConfig;

// const DevConfig = {
//     project: 'inspire-1540046634666',
//     regions: ['us-central1'],
//     loadSecondaryProject: false
// } as CloudFunctionsConfig;

// CHANGE HERE
export const Config: CloudFunctionsConfig = ProdConfig
