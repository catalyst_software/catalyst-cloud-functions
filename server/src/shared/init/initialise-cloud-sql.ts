import { Client } from 'pg';
import { getSharedSystemConfiguration } from './../../api/app/v1/callables/utils/warehouse/olap/get-olap-jwt-package';

let intialized = false;

/**
 * Initialise Firebase Admin Node.js SDK
 * @returns db: Firestore
 * @returns fbAdmin: firebase-admin
 */
export const initialiseCloudSql = async (): Promise<Client> => {
    let db: Client;
    try {
        if (!db) {
            intialized = true;

            console.log('Cloud SQL Initializing Development Project');
            const config = await getSharedSystemConfiguration();
            const pg = config.postgres;
            db = new Client({
                host: pg.host,
                database: pg.database,
                port: pg.port,
                user: pg.username,
                password: pg.password
            });

            await db.connect();


            

        }
    } catch (err) {
        console.error(`Error occured initializing Firebase`, err);
    }

    return db;
}

// firebase functions:config:set inspire.key="-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCWcy+zrokUIXy0\nJ9n9nUNoKGqfGfWLq1DS7GDoKuFRDaAAORuqlFcCpJn3sqXxuMHFzXWL6SVJymEs\nphDiBEnVTnIGDBrWweajrBq3RcjP16gmwD9n6LchHBKUe0Tq0t2oh72Yj45ZkOkD\nuhzFVor7QLzCoAc/Fyr+55gQPxLo3hHIk5spmuNehAQe0rijFDVBlqiHLjXox6rH\nwvByHgZDPfU5a5SfQmtuCrMA6LqDVz/lwgwH2xm0WH4FhtTj5PndQ4u7kj8up3MD\n0ZUjHrlnx/eREHFY64vP7YYV50O/jqs6EdTKSY0nFV/55UAneJ2Asm+2V/3inWEx\niTJhhZwJAgMBAAECgf8+VwYeRpZe6G2cYCIHVw75t09jE8ENbWVe7QmJUXZaMzdn\n8/MgWPShI7qorUoVkD3OQlQ3kWPOVw12wYBkxE5xO9TvwDuthg1aOqdNcRM6leaN\nO7lY8orvQO2tDvDOXJGjpf3CYM1Mdvo93+Ba0PeKrm/XcHfe+1gkR1xYmhjZUK/f\n/NnTqYSeKeElSY/NNIJsQHGI6hmUZxyHL+GRtTqOMb7UrJ9HS7LXWQmml14XVXT1\nu9nFmhYv0xoDypqf0SyqCa667anmsSSiFSDJtldsJh5PEUMUx1r6bRrPS3N+HEUb\nqAZXBJprPTG+rlfM7/0PUxB40NJ6d42PiN+TtS0CgYEAxK4ucAVexXOPkiWX2/Zx\nBzLOo3rAO86IVmWWzUWRQZ7A7jLjXfB4WzQQpSgAOpIcF4dRE3MFCqG7RDlHaevC\nkttsqpXprVnZRNydrrCdeWR0N/MVtejUx692D9vYd3jgC6FOS9DU0n9LId+7uCpk\nWKbQ0Cj8i4c5pFZT5vB+HkcCgYEAw9OFwrOcj5jtTfSOvFf9lFDjobA64PZRtgMl\nZRu4UyMA2XuWV9dFkgftWfbDg07Fzxvkhq0tR9UCcm9QTZAWQ7AEFPGwh6k/3zaf\nl32oSzKYnadetv6/wqvalHlTaagWmVCKmAU739fhc/opUbudCgJWpitxMxYBFhVJ\nyBvnCy8CgYBZm85WhWvXZD4+ZNhahF6c2/4fp5ab6Q5e5qwdXBvBHPZj0FWdACml\nzAPla2MnTFh2M9Wxmc7rOCKdA8fQr3SX+lb5JZIww+XkpOtGovssSqNUtnDmz2za\nicutkAmA7VaOlfCZRE0ilpKBnXDmiJHtfhtXK8VcQMD3acmSR3H+kwKBgQCQa5Yx\ngp9LNACHRn5iHw6P7JwEaeDyehbHAkj46VmmTqFYw5GcVNIlIZ2FhLyqQVUXKj3+\nqQdE+65jUP7LRSF2aACKpbiIRGUrrLBEXFLaqoTteDsdAIHSA0Yfy96dhG6uQT0F\nYo7GxgKqsur4MUJe2FypDk7c2zSP4cFZSGK71wKBgBvPeQ3KloRPYFC5NeUL9/je\nnmr1HB6gURsdUwBCXo1QupCPUc2K4xc1IHiiFG9UV7XaxIutHx+ip2Ircri5aPi+\nsKEPmyeaTprMDZfRXlkLtXJD56jkIE+N6zRl45VhC66OpjYCGdud/ZF5UzZ0Ju7d\nNkqGorbotypQupidq94T\n-----END PRIVATE KEY-----\n" inspire.id="inspire-219716"  inspire.databaseURL:'https://inspire-219716.firebaseio.com',
