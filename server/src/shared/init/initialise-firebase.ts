import * as fAdmin from 'firebase-admin' // Firebase Admin Node.js SDK
import { isProductionProject, loadSecondaryProject, getSecondaryProject, isHCPProject } from '../../api/app/v1/is-production';

export let liveAdmin: fAdmin.app.App;
export let theMainApp: fAdmin.app.App;
export let theSecondaryApp: fAdmin.app.App;
export let admin: fAdmin.app.App;
export let realitimeDB: fAdmin.database.Database;
export let liveDB: fAdmin.firestore.Firestore; //  FirebaseFirestore.Firestore;
export let secondaryDB: fAdmin.firestore.Firestore;
export let db: fAdmin.firestore.Firestore; //  FirebaseFirestore.Firestore;
export let catalystDB: fAdmin.firestore.Firestore;

let intialized = false;

/**
 * Initialise Firebase Admin Node.js SDK
 * @returns db: Firestore
 * @returns fbAdmin: firebase-admin
 */
export function initialiseFirebase(fromWhere) {
    try {
        if (!intialized) {
            intialized = true;
            const loadSecondary = loadSecondaryProject();
            const secondaryProj = getSecondaryProject();
            const isHcp = isHCPProject();
            console.log('isProductionProject?', isProductionProject() || false);
            console.log('loadSecondaryProject?', loadSecondary || false);
            console.log('secondaryProject?', secondaryProj || '');

            let devApp: fAdmin.app.App;
            let prodApp: fAdmin.app.App;
            let secondaryApp: fAdmin.app.App;

            // // PRODUCTION
            // if (isProductionProject() || isProd) {

            console.log('Firebase Initializing for catalyst-31efe');

            prodApp = fAdmin
                .initializeApp({
                    credential: fAdmin.credential.cert({
                        projectId: 'catalyst-31efe',
                        clientEmail: 'firebase-adminsdk-5vquh@catalyst-31efe.iam.gserviceaccount.com',
                        privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCQWt7Kn3IbyhFN\noCyQ1R1g3KQ2j8576MdqNSBmoXfhVo3S8/SwPLuMmz2Umtv75IwvFzq9Yw8l/MHj\nyeQKwJazA5sjK0tKfkrLbFyEc9U47aHHQkCpPeCapNrLMUisComaIgwh3uwQfZUh\nUvnOBf1GlFJEYkNTQpP8n6EeopvW9bTylBxik71VH6+l1SNS3Yka+7Vir4PP7oNc\nYWQYnoiV5UHhjUqbKdLdwVDtwMSL9Gm51jtJnJpKA37ijZZDVj1MhjWk0TaMDhVS\nn97LcFr5jP8BETxfr3ZKxmSZH88PA8P6vCfcy009/iPfX7GbeSX1KWHvP02KzC4M\n+mvzdvHbAgMBAAECggEAAP9vzMy/WCvUZMn3pOSkOFguFmZ2Eesw/a3yVGNHVdpo\nTo37IgC5dyZXjaBoZtxUj7rEYCzAHQ4bNPzfQ6fDIueieeLBuXkYAUH1fcDjN2qF\nFMCDe+f+lnMPRBpKG05TVypnFRDaZt7HWhydt2vZLVS3INvhBUFPjo536aNMGndD\nqM5gk2OQTKVAPEAs+Uff6OuYT0FIKLqDjygsJrU381ul3tCB4D+aUVRIS6dEjy1U\n1S0Y8IgkUkgMui1ctv62CLat80pMqJCCjaeUTELhguyrbPIcIHcajh8lcF+/6J4v\nhkyuVx+MqxUY0HiII3X3oJOkxWiPZpbRDhZHoqTAUQKBgQDJsgaGw4yS4fZ3E7Rd\nB70O+cm3IwQKj08FBh7bNy58flEkt9RsDhcfXW2dxeQMINlI70Pg+33SCYpjluiW\ng1L3Hi62U37HVAKIEZEA/MOa1VIctIWrWS5AKGMz3/QQyb5Ba5wTuSqqkkfnHkDP\nteM6fZ+AHyohKazfsz5M12zPCQKBgQC3OJ7jLMtwaBzbCtTr5g5JFHoy48kw6TbI\njcsetJWJydGVRcPIFY/Wk8Re/SjkPxaaAW2N8NRE54oOK2HPg03akWZhYiJtmbdI\nN2BFUI07cq34jlye1BQhqZ7GwGkuhaRerqfpTPGQy33IlWkiUyjNNUQs8c2CwjNJ\nUaJIaBzOwwKBgAqhRVLI75m2KwUvlqvogoBNjeIHCGl+EA4Vun6XM+3/wksLHTjs\nrS9L/0D3cwNon8sGQS+UUZdGRXLmOy/jj/XSM2gMknvy44dEVb4VlEGOyJsWsIzN\nUHC1MDVFsllIKKzN/RXVBnii6QM3RvcrgORORp+8BL5msePAUKJAIE3xAoGAFFD3\n9VLLQCybPoKl935sCRh3MFtnvL+vbm+inUYC6uNxFO9GdVJGv9Fkze+ecCi6tdsA\nreONdhKCkW2oC92QbXL/+j5qeOK5mw1g5G65KmVd+xi2FnqHvO/VBk7hmuIAxurw\n/YKlqpuV1/8RSFgWRyLn61/onN4x16hwqTy6LQcCgYEAh98Vv0JHqoLiP7h922Fk\nOZ/TJgyDmB23zmvqzql5CCbl3950xJW8vxTm8j5g2iBjLQN+QFsQtSWff0xaRiER\nXU0J6ELeTcL5KdrlfwmlrEE4xPGRZQ+D03uHHSjOMpHPSdty/Jw0b24goElgHvwk\nkIXyWPef81ekP2yRoNrUfq4=\n-----END PRIVATE KEY-----\n'
                    }),
                    databaseURL: 'https://catalyst-31efe.firebaseio.com'
                },
                    'live' // this name will be used to retrieve firebase instance. E.g. live.firestore();
                );
            
            console.log(`Logging options`);
            console.log(`ProdApp options: ${JSON.stringify(prodApp.options)}`);

            console.log(`Logging name`);
            console.log(`ProdApp name: ${prodApp.name}`);

            // if (loadSecondary) {
            //     if (secondaryProj) {
            //         console.log(`Loading secondary project ${secondaryProj} given configuration settings`);
            //         secondaryApp = fAdmin
            //             .initializeApp({
            //                 credential: fAdmin.credential.cert({
            //                     projectId: 'inspire-secondary-1',
            //                     clientEmail:
            //                         'inspire-secondary-1@appspot.gserviceaccount.com',
            //                     privateKey:
            //                         '-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDHURFiLCJMoEcn\nWmSF0j6Pm/j9W8+H7mHuxBTctODJOz/tA7cbM34MPi+4kZuMgUzlTshmDuX48gzN\n1VUtIJdnU7pE/raFS1P+TemK+tOiX84P88TO5nbqIjVrNpNHJer8dm8VGYFTrgVO\nei7PpZq+FFhy+w8vUZWeaEWgeLOu/z/+leA6oYhlIuP4SdFMS2rhSyJZKWn0/J9e\n7suzs0bIR43sualcWmZ4u7kIufyKrplWRB60mJIwpOnQfAmPWDm36t3b86N7Ltw7\nHv4Ck/3aWkhHG8T7Ak0ptymopAU90I5VMW8Rjsj+uO/V3VKRSWOuWD3JWXm+jQYo\nve5nROwDAgMBAAECggEAEhqPmlEpPw3X01U8iC3T4jYSjutbQEeAvFqHDTzzUJPX\nIe9nyaALCjr5GVSbF6Su7Ve95WHjtZGWuXjFEczss5qWnVRdaQyx27QL9cRZRghZ\nrnfqdQ3Ygzpe4RbF7dJzM5c2Tmj7UYnJkEU8zJOIKKJZyP0mIJrScjRFDrxSkHRp\ncR4co1wKWEh5OpndXGRrPEHEN4D9g6XZIsJtZzJLZc/vuVTzdOeOSIuxpelBVPUh\nzySRA6aXBFVXl4uptkPuKFJn9ta15BsTLpnCrPOwryLR5P0y49lFYyy42xRflMKW\nLapXDWiNmCoVgjlVhqkc/mFvf1sHKq9LM/9kBOh4UQKBgQD+qATokd2rpQ/W6USq\npDDQ4v4jZJTfbl++kiQfO7HdFNfv97QFkFEyVQ4N1Y1FNUVmGGsX7CPX5Y09pCnN\natQfrlm+g3Ox750Fl4bB/WWl+B/SKNQ4oWcvqskWN5flWjEikh05PG0duq204Ice\nfy5W8T97l72/wwTYM+U1gr5LVwKBgQDIXkxBItgWeqCtG/X8YpZNeJhhGP9S9qCv\nsJHmHOc9Xx2cPVuPecsyA2zHXxaAE4GCMEyP9m6Xl0E/h9dbHCjahWZVZMU820La\nUqVINAqxVEXT9yeHQkoupn2g4d7pR7u8X3vLiF5ZqsGA5sO1D2IU7npNIFN0PUD+\nxDIWfUplNQKBgQDRD8kv0YiFgLY3PjxERxVYd/IhxlI999QclMeY6J4FnTYF6Hpf\nUvKF+rPWAaq/1ZMK1SMeeZuXg/nnUB2NaZWKCM3ejPZvbwKe854LJmfL0J73HtPT\nRhZ7WJ9LYQqPnPLXqdLaCVSfYoF3DBmBnPHDKL1wTsvVYVpZ0Ijs3FAzcwKBgQCs\nryiFEqStau5Dg0+TPtxO1FpvhH24YLnH8SMs8UgRyoHYf8EDfHI1HmKHyVm49Bav\nvzSc9kQF/ePL/yUJbtF+022ItxqOAmIxivzLp553iivMLLwyhF0uq2qmV0QoRRx1\nks08csDuAo5Wq4uL5kAfzhA1nGCQU6JKb4jDfSO66QKBgQDKlI2AUGP3sH7XZDMj\nAsR08ACFCBB2ZG8O2Ah0HosKwD/UFGTedDDDOtPLp+2WcTXwCCGdsS4RoFM/Plqy\nXmKh/8XtjHWW6X7NK5EAWY2GJ1DgjVuWLomCkAUYBzbIPsDlMd26EjGdfN5QWmW/\nYRyzXz4KvEt8p7R0fQzPGaQWsA==\n-----END PRIVATE KEY-----\n",',
            //                 }),
            //                 databaseURL: 'https://inspire-secondary-1.firebaseio.com',
            //             },
            //                 'secondary' // this name will be used to retrieve firebase instance. E.g. live.firestore();
            //             );
            //     } else {
            //         console.log('Cannot load secondary project.  Secondary project name not specified');
            //     }
            // } 
            // // } else 
            // if (!isProductionProject()) {
            //     // DEVELOPMENT

            //     console.log('Firebase Initializing for inspire-1540046634666');

            //     devApp = fAdmin
            //         .initializeApp({
            //             credential: fAdmin.credential.cert({
            //                 projectId: 'inspire-1540046634666',
            //                 clientEmail:
            //                     'firebase-adminsdk-yabfi@inspire-1540046634666.iam.gserviceaccount.com',
            //                 privateKey:
            //                     '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDEa8dBKl8pFlrH\nEtlOFu5Pajeg6dhmCOKwISfdO2WYEaWtM80kzGxrxfkw+ePJO4+WzigOm9FcSwhk\nMTlJovU4S1G1vP/tF3uAeNstqgQiENnDuQtbFb2FzhUFMXptkcTfBuoe2vmyn4pk\neBTn63HOUmaD6doU8HtikdQsb5uaJJCpH5VKaPCPAYtLelV/h/8eAu+1dQcoShEY\noY808KXJzzwtWnLHUBnb/s7iUQUVfWH1JP5OTujkxdM7B11KRB3JX9PsexS/+tVJ\niZVbxPIr5CzmM+fjKD/Gico/07IqyloPYzBZ0CSaTRJrEtpiebWXBMHPODsHsI9p\na1KZdkwbAgMBAAECggEAC6WQZQ7MIbWPoGo/tF+rtc3IiqQTDsnMF0GACaAx//hb\n6I8/xMTSUPmmMv8+QHploz2KJoOawGw6jSZWDUW6YKImsC4KxtYznoSMCoMgR5zU\nLMTWJYp+eGal3G74oXKZR9gzHa1CTRMks5xjMPVHBELijUPaiI9R4aBgAlqD0Xnq\n1buKaF9Mvtha3jvHsrWk0mJeyoR5SsQ5o718RVJ5k/xuS4/nc5uOz33SG/xxKyBa\nMuFomisEP92QpTbksut87/FlsQd7XgnPkp+r7UL9olGd8gByII3islpsmEhgrOPT\nmnGpCBjD9pzifTUMQLcZKCmnymfRFRevv3CS5Gk+cQKBgQD4rY8XU/IMoFPiE6FD\nbvHv8+2b0P54uka7GN49Yx7riw73EG9SmgZCTT6V2jhlxXv8kV023085RTXQdPCr\ng0s2kvIJxU4quiZeJBquG1Scji2DSlMKv9tOZNswb4K2x+ZSDZ9GTvfXj/09NDnk\n2jJMRp8K826bI7RwHp03F90+ZwKBgQDKNFN68CjRWGe3zd/LaW6yAwFa8t9yJwdk\nwgcKfKgIm+a5UkQgE8sQ7wWvGMrxSuYJFH1fHIuVFRmpQsJgfydKVHJs16ZdvDun\nvDXy6psYhvggm9pEf+xqct4702oVhaQY/Ee0K6pQgcH966VYwmz4USJj+woWJYxW\nr9cLf76MLQKBgQDP6QPOhC/F4LHhPXpBj9uVO8L32Dc5prwGJ1d/yYSLd/ruE36P\neBkti7l8vjMS25a65qohe3iYMEY639pr+1yB5z+Xba/Zx0LWyKbJ1C3cqn5g214s\niZWIqIgdqc2GlgD5r0vwE4vhXRBkAGs67DbLUOwd0sMx0BtG9kGJU1l1lwKBgQDE\nrCCGcxFAjbxUCuqh7uq8OjAXRiQP4+ZNGmu+x4Co3vqLRnj8ukPJNLNSm8rI5xDX\nxBYtbJZXay6Kc2ScdxDAO2MQerBWe7+KZoYSwB4avSyaivzBo6tP3mpJxlholpQF\nuVwE4nPF2m/Vil5I9tMGs+O/W210HRFjP6TqilXMAQKBgGfbZaGFD3xqSYt+3dA+\nyF7w2/4Bj8xzBmKDQQj2NPan/5V1cWXxGfXNqLDgmxqzsJ+cD6yhPd6t55l2yIcV\nuEWg8XhOq3l0jU+Fh8hD0lHqeFH7yxpp2ymzMjCg3nbq90h9fZqNvHzZSI2cUoCJ\n8UAojg1vDxolRkag4gmGVa7s\n-----END PRIVATE KEY-----\n',
            //             }),
            //             // credential: fAdmin.credential.cert(serviceAccount),
            //             databaseURL: 'https://inspire-1540046634666.firebaseio.com',
            //         },
            //             'dev' // this name will be used to retrieve firebase instance. E.g. live.firestore();
            //         )
            // }

            // debugger;
            // const isProd = true;
            const isProd = true;

            // if (isProd)
            //     true
                
            admin = (isProductionProject() || isProd) ? prodApp : devApp || undefined;
            theMainApp = admin;
            theSecondaryApp = loadSecondary || isHcp ? secondaryApp : admin;

            // admin.database().goOffline()

            db = admin ? admin.firestore() : undefined;
            catalystDB = db;
            // fAdmin.database().goOffline()
            liveAdmin = prodApp
            // liveAdmin.database().goOffline()
            liveDB = db;
            secondaryDB = db;
            // Will be set default to true in future release and this option will be removed
            // db.settings({ timestampsInSnapshots: true })
            // realitimeDB = fAdmin.database();

            console.info(`Firebase initialized!!!`, fromWhere)
        } else {
            // console.error(`Firebase Already initialized`, fromWhere);
        }
    } catch (err) {
        console.error(`Error occured initializing Firebase`, err)
    }
    return { admin, db, liveAdmin, liveDB, theMainApp, theSecondaryApp, secondaryDB, catalystDB }
}

// firebase functions:config:set inspire.key="-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCWcy+zrokUIXy0\nJ9n9nUNoKGqfGfWLq1DS7GDoKuFRDaAAORuqlFcCpJn3sqXxuMHFzXWL6SVJymEs\nphDiBEnVTnIGDBrWweajrBq3RcjP16gmwD9n6LchHBKUe0Tq0t2oh72Yj45ZkOkD\nuhzFVor7QLzCoAc/Fyr+55gQPxLo3hHIk5spmuNehAQe0rijFDVBlqiHLjXox6rH\nwvByHgZDPfU5a5SfQmtuCrMA6LqDVz/lwgwH2xm0WH4FhtTj5PndQ4u7kj8up3MD\n0ZUjHrlnx/eREHFY64vP7YYV50O/jqs6EdTKSY0nFV/55UAneJ2Asm+2V/3inWEx\niTJhhZwJAgMBAAECgf8+VwYeRpZe6G2cYCIHVw75t09jE8ENbWVe7QmJUXZaMzdn\n8/MgWPShI7qorUoVkD3OQlQ3kWPOVw12wYBkxE5xO9TvwDuthg1aOqdNcRM6leaN\nO7lY8orvQO2tDvDOXJGjpf3CYM1Mdvo93+Ba0PeKrm/XcHfe+1gkR1xYmhjZUK/f\n/NnTqYSeKeElSY/NNIJsQHGI6hmUZxyHL+GRtTqOMb7UrJ9HS7LXWQmml14XVXT1\nu9nFmhYv0xoDypqf0SyqCa667anmsSSiFSDJtldsJh5PEUMUx1r6bRrPS3N+HEUb\nqAZXBJprPTG+rlfM7/0PUxB40NJ6d42PiN+TtS0CgYEAxK4ucAVexXOPkiWX2/Zx\nBzLOo3rAO86IVmWWzUWRQZ7A7jLjXfB4WzQQpSgAOpIcF4dRE3MFCqG7RDlHaevC\nkttsqpXprVnZRNydrrCdeWR0N/MVtejUx692D9vYd3jgC6FOS9DU0n9LId+7uCpk\nWKbQ0Cj8i4c5pFZT5vB+HkcCgYEAw9OFwrOcj5jtTfSOvFf9lFDjobA64PZRtgMl\nZRu4UyMA2XuWV9dFkgftWfbDg07Fzxvkhq0tR9UCcm9QTZAWQ7AEFPGwh6k/3zaf\nl32oSzKYnadetv6/wqvalHlTaagWmVCKmAU739fhc/opUbudCgJWpitxMxYBFhVJ\nyBvnCy8CgYBZm85WhWvXZD4+ZNhahF6c2/4fp5ab6Q5e5qwdXBvBHPZj0FWdACml\nzAPla2MnTFh2M9Wxmc7rOCKdA8fQr3SX+lb5JZIww+XkpOtGovssSqNUtnDmz2za\nicutkAmA7VaOlfCZRE0ilpKBnXDmiJHtfhtXK8VcQMD3acmSR3H+kwKBgQCQa5Yx\ngp9LNACHRn5iHw6P7JwEaeDyehbHAkj46VmmTqFYw5GcVNIlIZ2FhLyqQVUXKj3+\nqQdE+65jUP7LRSF2aACKpbiIRGUrrLBEXFLaqoTteDsdAIHSA0Yfy96dhG6uQT0F\nYo7GxgKqsur4MUJe2FypDk7c2zSP4cFZSGK71wKBgBvPeQ3KloRPYFC5NeUL9/je\nnmr1HB6gURsdUwBCXo1QupCPUc2K4xc1IHiiFG9UV7XaxIutHx+ip2Ircri5aPi+\nsKEPmyeaTprMDZfRXlkLtXJD56jkIE+N6zRl45VhC66OpjYCGdud/ZF5UzZ0Ju7d\nNkqGorbotypQupidq94T\n-----END PRIVATE KEY-----\n" inspire.id="inspire-219716"  inspire.databaseURL:'https://inspire-219716.firebaseio.com',
