import _ from "lodash";

export const isArrayEqual = (x, y) => {
    return (x).differenceWith(y, _.isEqual).isEmpty();
};
