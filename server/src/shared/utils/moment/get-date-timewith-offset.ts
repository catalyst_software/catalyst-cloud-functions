import { firestore } from "firebase-admin";
import moment from "moment";

let logged = false

export const getDateTimeWithOffset = (timestamp: firestore.Timestamp, athleteUTCOffset: number): moment.Moment => {

  const utcDate = moment(timestamp.toDate())
  const utcDateWithOffset = utcDate.clone().add(athleteUTCOffset || 0, 'minutes')

  if (!logged) {
    logged = true

    console.log('==========>>>>>>> utcDate', utcDate.clone().toDate())
    console.warn('==========>>>>>>> athleteUTCOffset', athleteUTCOffset)
    console.log('==========>>>>>>> utcDate with offset', utcDateWithOffset.clone().toDate())
  }

  return utcDateWithOffset
}
