
import { firestore } from "firebase-admin";

import { getDateTimeWithOffset } from "./get-date-timewith-offset";

//AggregateDocumentComposer
export const getMonthToDate = (
  dateTime: firestore.Timestamp,
  utcOffset: number
): firestore.Timestamp => {

  return firestore.Timestamp.fromDate(
    getDateTimeWithOffset(dateTime, utcOffset)
      .endOf("month")
      .toDate()
  );
};

export const getMonthFromDate = (
  dateTime: firestore.Timestamp,
  utcOffset: number
): firestore.Timestamp => {

  return firestore.Timestamp.fromDate(
    getDateTimeWithOffset(dateTime, utcOffset)
      .startOf("month")
      .toDate()
  );
};

// export const getWeekToDate = (
//   dateTime: firestore.Timestamp
// ): firestore.Timestamp => {


//   return firestore.Timestamp.fromDate(
//     moment(dateTime.toDate())
//       .endOf("isoWeek")
//       .toDate()
//   );
// };

export const getWeekFromDate = (
  dateTime: firestore.Timestamp,
  utcOffset: number
): firestore.Timestamp => {


  return firestore.Timestamp.fromDate(
    getDateTimeWithOffset(dateTime, utcOffset)
      .startOf("isoWeek")
      .toDate()
  );
};

export const getWeekNumber = (
  from: firestore.Timestamp, // moment([2007, 0]);
  to: firestore.Timestamp, //moment([2008, 9]);,
  utcOffset: number
): number => {

  const weekNr = getDateTimeWithOffset(to, utcOffset).diff(
    getDateTimeWithOffset(from, utcOffset).startOf('isoWeek'),
    "week"
  );


  return weekNr
};


export const getMonthNumber = (
  from: firestore.Timestamp,
  to: firestore.Timestamp,
  utcOffset: number
): number => {

  return getDateTimeWithOffset(to, utcOffset).diff(
    getDateTimeWithOffset(from, utcOffset).startOf('month'),
    "months"
  );
};

// export const getWeekStart = (date: Date): Date => {
//   const weekNo = moment(date)
//     .startOf("isoWeek")
//     .toDate();

//   return weekNo;
// };

// export const getWeekEnd = (date: Date): Date => {
//     return moment(date).endOf("isoWeek").toDate();
// }

export const getMonthStart = (date: firestore.Timestamp, utcOffset: number): Date => {

  return getDateTimeWithOffset(date, utcOffset)
    .startOf("month")
    .toDate();
};


export const getDayStart = (date: firestore.Timestamp, utcOffset: number): Date => {

  return getDateTimeWithOffset(date, utcOffset)
    .startOf("day")
    .toDate();
};
// export const getMonthEnd = (date: Date): Date => {
//     return moment(date).endOf("month").toDate();
// }

export const getIsoWeekDay = (timestamp: firestore.Timestamp, utcOffset: number): number => {

  const utcDate = getDateTimeWithOffset(timestamp, utcOffset)

  const isoWeekday = utcDate.isoWeekday() - 1;

  console.warn('------- isoWeekday', isoWeekday)

  return isoWeekday
};

export const getDayDifference = (
  from: firestore.Timestamp,
  to: firestore.Timestamp,
  utcOffset: number
): number => {

  const count = getDateTimeWithOffset(to, utcOffset).diff(
    getDateTimeWithOffset(from, utcOffset).startOf('isoWeek'),
    "days"
  );

  return count;
};

export const getDayIndex = (dateTime: firestore.Timestamp, utcOffset: number, isMonth?: boolean) => {

  const now = getDateTimeWithOffset(dateTime, utcOffset);
  // const date = now.toDate()
  const index = isMonth
    ? now.date() - 1
    : now.isoWeekday() - 1;

  return index;
};


export const getMonthDayIndex = (timestamp: firestore.Timestamp, utcOffset: number) => {

  const utcDate = getDateTimeWithOffset(timestamp, utcOffset)

  const index = utcDate.date() - 1;

  console.warn('============= getMonthDayIndex', index)
  return index;
};


