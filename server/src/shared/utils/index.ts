
export const convertToPlainObject = (jsonObject: any) => {
    return JSON.parse(JSON.stringify(jsonObject))
};
