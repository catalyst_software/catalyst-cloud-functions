import { firestore } from 'firebase-admin';

export const timestampFromDate = (date: Date) => {
    return firestore.Timestamp.fromDate(date);
}

export const timestampFromDateString = (dateString: string) => {
    return timestampFromDate(new Date(dateString))
}