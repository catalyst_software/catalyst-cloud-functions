import { ScheduledTask } from '../../../db/taskQueue/interfaces/scheduled-task.interface';
import { isArray } from 'lodash';
import { SelfHealingTask } from '../../../db/taskQueue/interfaces/self-healing-task.interface';
import { AsyncJob } from '../../../db/asyncJobQueue/interfaces/async-job.interface';
import { AsyncJobTask } from '../../../db/asyncJobQueue/interfaces/async-job-task.interface';

export const logTaskQueueMessage = (task: ScheduledTask | SelfHealingTask | AsyncJob | AsyncJobTask, message: string, cancelConsole?: boolean) => {
    if (!isArray(task.logs)) {
        task.logs = [];
    }

    task.logs.push(message);

    if (!cancelConsole) {
        console.log(message);
    }
}
