import { WellAggregateCollection } from './biometricEntries/enums/firestore-aggregate-collection';
import { WellAggregateHistoryCollection } from './biometricEntries/enums/firestore-aggregate-history-collection';
import { WellAggregateBinaryHistoryCollection } from './biometricEntries/enums/firestore-aggregate-binary-history-collection';

export const wellAggregateCollections = {
    athlete: [
        {
            collection: WellAggregateCollection.AthleteWeekly
        },
        {
            collection: WellAggregateHistoryCollection.AthleteWeeklyHistory
        },
        {
            collection: WellAggregateHistoryCollection.AthleteMonthlyHistory
        }
    ],
    group: [
        {
            collection: WellAggregateCollection.GroupWeekly
        },
        {
            collection: WellAggregateHistoryCollection.GroupWeeklyHistory
        },
        {
            collection: WellAggregateHistoryCollection.GroupMonthlyHistory
        }
    ]
};

export const binaryAggregateCollections = {
    athlete: [
        {
            collection: WellAggregateBinaryHistoryCollection.AthleteBinaryHistory
        }
    ],
    group: [
        {
            collection: WellAggregateBinaryHistoryCollection.GroupBinaryHistory
        }
    ]
};
