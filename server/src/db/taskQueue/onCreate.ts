import { PubSub } from '@google-cloud/pubsub';
import * as functions from 'firebase-functions';
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore';
import { FirestoreCollection } from '../biometricEntries/enums/firestore-collections';
import { ScheduledTask } from './interfaces/scheduled-task.interface';
import { ScheduledTaskWorkerType, ScheduledTaskStatusType, ScheduledTaskWorkerTopicType } from './enums/scheduled-task.enums';
import { logTaskQueueMessage } from '../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import moment from 'moment';
import { Config } from './../../shared/init/config/config';


const finalizeTaskWithError = (task: ScheduledTask, taskSnapshot: DocumentSnapshot, msg: string) => {
    logTaskQueueMessage(task, msg);
    task.status = ScheduledTaskStatusType.Error;
    taskSnapshot.ref.update(task)
        .then(() => {
            console.log(`Task ${task.uid} successfully updated.`);
        })
        .catch((e) => {
            console.error(`Task ${task.uid} update failed: ${e.message}`);
        });
}

exports.manageTaskQueue = functions.firestore
    ._documentWithOptions(`${FirestoreCollection.TaskQueue}/{taskId}`, {regions: Config.regions})
    .onCreate(async (taskSnapshot: DocumentSnapshot, context) => {
        const task = <ScheduledTask>taskSnapshot.data();
        task.uid = taskSnapshot.id;
        let msg = `Task ${task.uid} received at ${new Date()}`;
        logTaskQueueMessage(task, msg);

        let messageId;
        const pubSubClient = new PubSub();
        task.creationTimestamp = (task.creationTimestamp as any).toDate().toISOString();

        const taskBuffer = Buffer.from(JSON.stringify(task), 'utf8');
        switch (task.worker) {
            case ScheduledTaskWorkerType.AppEngagement:
                try {
                    messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.AppEngagement).publish(taskBuffer);
                    console.log(`Message ${messageId} published.`);
                } catch (err) {
                    console.error(err);
                }
                break;
            case ScheduledTaskWorkerType.AppSessionUsageMetric:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.AppSessionUsageMetric).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.Biometric:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.Biometric).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.RedFlag:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.RedFlag).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.CardRendered:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.CardRendered).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.CloudTaskUsageMetric:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.CloudTaskUsageMetric).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.ProgramContentEngagment:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.ProgramContentEngagment).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.NotificationCardEngagment:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.NotificationCardEngagment).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.IpScoreAchievement:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.IpScoreAchievement).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.OnboardingInvitation:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.OnboardingInvitation).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.ManageSegmentDeletion:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.ManageSegmentDeletion).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.ManageAthleteSegmentAddition:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.ManageAthleteSegmentAddition).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.ManageNewProgramAddition:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.ManageNewProgramAddition).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            case ScheduledTaskWorkerType.ScheduleNotificationCardDelivery:
                messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.ScheduleNotificationCardDelivery).publish(taskBuffer);
                console.log(`Message ${messageId} published.`);
                break;
            default:
                task.creationTimestamp = moment(task.creationTimestamp).toDate();
                msg = `Task ${task.uid} with worker type ${task.worker} is not supported.`;
                finalizeTaskWithError(task, taskSnapshot, msg);
                break;
        }

        return true;
    });
