export enum ScheduledTaskStatusType {
    Scheduled = 'scheduled',
    Completed = 'completed',
    Success = 'success',
    Error = 'error'
}

export enum ScheduledTaskWorkerType {
    AppEngagement = 'appEngagement',
    AppSessionUsageMetric = 'appSessionUsageMetric',
    Biometric = 'biometric',
    RedFlag = 'redFlag',
    CloudTaskUsageMetric = 'cloudTaskUsageMetric',
    CardRendered = 'cardRendered',
    ProgramContentEngagment = 'programContentEngagement',
    NotificationCardEngagment = 'notificationCardEngagement',
    IpScoreAchievement = 'ipScoreAchievement',
    OnboardingInvitation = 'onboardingInvitation',
    ManageSegmentDeletion = 'manageSegmentDeletion',
    ManageAthleteSegmentAddition = 'manageAthleteSegmentAddition',
    ManageNewProgramAddition = 'manageNewProgramAddition',
    ScheduleNotificationCardDelivery = 'scheduleNotificationCardDelivery'
}

export enum ScheduledTaskWorkerTopicType {
    AppEngagement = 'task-appEngagement-topic',
    AppSessionUsageMetric = 'task-appSessionUsageMetric-topic',
    Biometric = 'task-biometric-topic',
    RedFlag = 'task-redFlag-topic',
    CardRendered = 'task-cardRendered-topic',
    CloudTaskUsageMetric = 'task-cloudTaskUsageMetric-topic',
    ProgramContentEngagment = 'task-programContentEngagement-topic',
    NotificationCardEngagment = 'task-notificationCardEngagement-topic',
    IpScoreAchievement = 'task-ipScoreAchievement-topic',
    SelfHealingEveryTwoHour = 'task-selfHealingEveryTwoHour-topic',
    BigQueryDataManipulationReattempt = 'task-bigQueryDataManipulationReattempt-topic',
    OnboardingInvitation = 'task-onboardingInvitation-topic',
    ManageSegmentDeletion = 'task-manageSegmentDeletion-topic',
    ManageAthleteSegmentAddition = 'task-manageAthleteSegmentAddition-topic',
    ManageNewProgramAddition = 'task-manageNewProgramAddition-topic',
    ScheduleNotificationCardDelivery = 'task-scheduleNotificationCardDelivery-topic'
}

export enum AppEngagementScreenType {
    ContentFeed = 'contentFeed',
    ProgramDetails = 'programDetails',
    Wellness = 'wellness',
    Profile = 'profile',
    MyDataMind = 'myDataMind',
    MyDataBody = 'myDataBody',
    MyDataFood = 'myDataFood',
    MyDataTraining = 'myDataTraining',
    MyDataWearables = 'myDataWearables',
    Undefined = 'undefined'
}

export enum AppEngagementTaskType {
    AppLaunched = 'appLaunched',
    AppSuspended = 'appSuspended',
    AppResumed = 'appResumed',
    AppExited = 'appExited',
    ContentFeedLaunched = 'contentFeedLaunched',
    ContentFeedExited = 'contentFeedExited',
    ProgramDetailsLaunched = 'programDetailsLaunched',
    ProgramDetailsExited = 'programDetailsExited',
    WellnessLaunched = 'wellnessLaunched',
    WellnessExited = 'wellnessExited',
    ProfileLaunched = 'profileLaunched',
    ProfileExited = 'profileExited',
    MyDataMindLaunched = 'myDataMindLaunched',
    MyDataMindExited = 'myDataMindExited',
    MyDataBodyLaunched = 'myDataBodyLaunched',
    MyDataBodyExited = 'myDataBodyExited',
    MyDataFoodLaunched = 'myDataFoodLaunched',
    MyDataFoodExited = 'myDataFoodExited',
    MyDataTrainingLaunched = 'myDataTrainingLaunched',
    MyDataTrainingExited = 'myDataTrainingExited',
    MyDataWearablesLaunched = 'myDataWearablesLaunched',
    MyDataWearablesExited = 'myDataWearablesExited',
    Undefined = 'undefined'
}

export enum AppSessionUsageMetricTaskType {
    TotalGlobalSessionTime = 'totalGlobalSessionTime',
    TotalSuspendedTime = 'totalSuspendedTime',
    TotalContentFeedSessionTime = 'totalContentFeedSessionTime',
    TotalProgramDetailsSessionTime = 'totalProgramDetailsSessionTime',
    TotalWellnessSessionTime = 'totalWellnessSessionTime',
    TotalProfileSessionTime = 'totalProfileSessionTime',
    TotalMyDataMindSessionTime = 'totalMyDataMindSessionTime',
    TotalMyDataBodySessionTime = 'totalMyDataBodySessionTime',
    TotalMyDataFoodSessionTime = 'totalMyDataFoodSessionTime',
    TotalMyDataTrainingSessionTime = 'totalMyDataTrainingSessionTime',
    TotalMyDataWearablesSessionTime = 'totalMyDataWearablesSessionTime',
    Undefined = 'undefined'
}

export enum CloudTaskUsageMetricTaskType {
    ConvertVideo = 'convertVideo',
    CreateVideoCard = 'createVideoCard',
    CreateImageCard = 'createImageCard',
    DeleteFile = 'deleteFile',
    Undefined = 'undefined'
}

export enum ProgramContentEngagementType {
    ProgramDetailsPageViewed = 'programDetailsPageViewed',
    ProgramCompleted = 'programCompleted',
    VideoStarted = 'videoStarted',
    VideoExitedWithoutCompletion = 'videoExitedWithoutCompletion',
    VideoCompleted = 'videoCompleted',
    SurveyStarted = 'surveyStarted',
    SurveyExitedWithoutCompletion = 'surveyExitedWithoutCompletion',
    SurveyCompleted = 'surveyCompleted',
    WorksheetViewed = 'worksheetViewed'
}

export enum SelfHealingScheduleType {
    EveryTwoHour = 'everyTwoHour'
}

export enum SelfHealingTaskWorkerType {
    BigQueryDataManipulationReattempt = 'bigQueryDataManipulationReattempt'
}

export enum BigQueryDataManipulationReattemptOperationType {
    Update = 'update',
    Delete = 'delete'
}

export enum FilterOperationType {
    Equals = 'equals',
    NotEquals = 'notEquals'
}

export enum ManageSegmentDeletionJobType {
    DeleteSegment = 'deleteSegment',
    RemoveAthletesFromSegment = 'removeAthletesFromSegment'
}