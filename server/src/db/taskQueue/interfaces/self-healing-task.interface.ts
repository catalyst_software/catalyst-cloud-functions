import { ScheduledTaskStatusType, SelfHealingScheduleType, SelfHealingTaskWorkerType } from '../enums/scheduled-task.enums';
import { ScheduledTaskPayload } from './scheduled-task-payload.interface';
import { SelfHealingTaskPayload } from './self-healing-task-payload.interface';

export interface SelfHealingTask {
    scheduleType: SelfHealingScheduleType;
    worker: SelfHealingTaskWorkerType;
    payload: SelfHealingTaskPayload;
    executionDelayInMinutes: number;
    status?: ScheduledTaskStatusType;
    maxNumberOfFailedAttempts?: number;
    numberOfFailedAttempts?: number;
    creationTimestamp?: any;
    uid?: string;
    logs?: Array<string>;
}