import { ContentFeedCardPackageBase } from './../../../models/report-model';
import { InAppMessageTemplate } from '../../../api/app/v1/callables/schedule-send-notifications';
import { SegmentType } from '../../../api/app/v1/callables/utils/warehouse/segment-type.enum';
import { Program } from '../../programs/models/interfaces/program';
import { AppEngagementTaskType, AppSessionUsageMetricTaskType, ScheduledTaskWorkerType, CloudTaskUsageMetricTaskType, ManageSegmentDeletionJobType } from '../enums/scheduled-task.enums';

export interface ScheduledTaskPayload {
    worker: ScheduledTaskWorkerType;
    globalSessionID?: string;
    screenSessionID?: string;
    athleteId?: string;
    bundleId?: string;
    programId?: string;
    evolutionInstanceId?: string;
}

export interface AppEngagementTask extends ScheduledTaskPayload {
    taskType: AppEngagementTaskType;
}

export interface AppSessionUsageMetricTask extends ScheduledTaskPayload {
    taskType: AppSessionUsageMetricTaskType;
    totalMilliseconds: number;
}

export interface CloudTaskUsageMetricTask extends ScheduledTaskPayload {
    taskType: CloudTaskUsageMetricTaskType;
    inputAsString: string;
    outputAsString: string;
    inputSizeKb: number;
    outputSizeKb: number;
    totalMilliseconds: number;
}

export interface BiometricTask extends ScheduledTaskPayload {
    healthComponentType?: string;
    lifestyleEntryType?: string;
    biometricEntryId?: string;
    value?: number;
    components?: Array<any>;
}


export interface RedFlagTask extends ScheduledTaskPayload {
    healthComponentType?: string;
    lifestyleEntryType?: string;
    biometricEntryId?: string;
    redFlagType?: string;
    redFlagId?: string;
    redFlagJobType?: string;
}

export interface IpScoreAchievementTask extends ScheduledTaskPayload {
    value?: number;
}

export interface CardRenderedTask extends ScheduledTaskPayload {
    cardType?: string;
    cardId?: string;
}

export interface ProgramContentEngagementTask extends ScheduledTaskPayload {
    contentEngagementType?: string;
    contentType?: string;
    contentEntryId?: string;
}

export interface NotificationCardEngagementTask extends ScheduledTaskPayload {
    notificationCardEngagementType?: string;
    notificationCardType?: string;
    notificationCardId?: string;
}

export interface OnboardingInvitationTask extends ScheduledTaskPayload {
    organizationId: string;
    groupId: string;
    athleteId?: string;
    email: string;
    senderId?: string;
    senderName?: string;
}

export interface ManageSegmentDeletionPayload extends ScheduledTaskPayload {
    jobType?: ManageSegmentDeletionJobType;
    organizationId?: string;
    segmentId?: string;
    athleteIds?: Array<string>;
}

export interface ManageAthleteSegmentAdditionPayload extends ScheduledTaskPayload {
    segmentId?: string;
    segmentName?: string;
    segmentType?: SegmentType;
    athleteId?: string;
    parentSegmentId?: string;
}

export interface ManageNewProgramAdditionPayload extends ScheduledTaskPayload {
    program: Program;
}

export interface ScheduleNotificationCardDeliveryPayload extends ScheduledTaskPayload {
    card: ContentFeedCardPackageBase;
    essentialCommsTemplate: InAppMessageTemplate;
    dateInSeconds: number;
}
