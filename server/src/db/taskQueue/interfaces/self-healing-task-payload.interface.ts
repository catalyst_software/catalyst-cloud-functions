import { SelfHealingTaskWorkerType, BigQueryDataManipulationReattemptOperationType, FilterOperationType } from '../enums/scheduled-task.enums';

export interface SelfHealingTaskPayload {
    worker: SelfHealingTaskWorkerType;
}

export interface BigQueryDataManipulationReattemptSelfHealingTaskPayload {
    sql?: string;
    operationType?: BigQueryDataManipulationReattemptOperationType;
    datasetId?: string;
    tableName?: string;
    conditions?: Array<BigQueryDataManipulationWhereCondition>;
}

export interface BigQueryDataManipulationWhereCondition {
    fieldName: string;
    fieldValue: any;
    condition: FilterOperationType;
    useQuotes?: boolean;
}
