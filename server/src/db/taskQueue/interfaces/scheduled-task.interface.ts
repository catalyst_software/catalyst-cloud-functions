import { ScheduledTaskStatusType, ScheduledTaskWorkerType } from '../enums/scheduled-task.enums';
import { ScheduledTaskPayload } from './scheduled-task-payload.interface';

export interface ScheduledTask {
    performAtUtc: number;
    worker: ScheduledTaskWorkerType;
    payload: ScheduledTaskPayload;
    bundleId?: string;
    segmentId?: string;
    athleteId?: string;
    status?: ScheduledTaskStatusType;
    creationTimestamp?: Date;
    timezoneOffset?: number;
    uid?: string;
    logs?: Array<string>;
    deviceType?: string;
    deviceManufacturer?: string;
    deviceModel?: string;
    deviceOs?: string;
    deviceOsVersion?: string;
    deviceSdkVersion?: string;
    deviceUuid?: string;
}