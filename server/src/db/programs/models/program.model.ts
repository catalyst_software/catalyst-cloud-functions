import { firestore } from 'firebase-admin';

import { ImageLocator } from './common/image-locator.interface';
import { ProgramIndicator } from './interfaces/program-indicator';
import { SubscriptionType } from '../../../models/enums/enums.model';
import { ProgramCategoryType } from '../../../models/enums/program-category-type';
import { ProgramDay } from './interfaces/program-day';

export class Program implements ProgramIndicator {

    readonly uid?: string;
    readonly programGuid?: string;
    readonly creationTimestamp?: firestore.Timestamp;

    readonly name?: string;
    publisher?: string;
    description?: string;

    //TODO:
    isPlaceHolder?: boolean

    images?: Array<ImageLocator>;
    imageSource?: string;

    comingSoon?: boolean;
    activeDate?: firestore.Timestamp;
    // startDate?: Timestamp;

    callToActionText: string;
    categoryType: ProgramCategoryType;
    subscriptionType: SubscriptionType;

    programDays?: number;
    isResctricted?: boolean;
    restrictedOrganizationIds?: Array<string>;

    tags?: Array<string>;

    startDate?: firestore.Timestamp;
    currentDayIndex?: number;
    content?: Array<ProgramDay>;

    contentProvisioning: any

    constructor() {
        // super(props);
    }
}
