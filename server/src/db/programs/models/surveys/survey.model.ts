import { SurveyAnswerSet } from './survey-answer-set.interface';
import { SurveyQuestion } from './survey-question.interface';
import { Survey as SurveyInterface } from '../interfaces/survey';

export class Survey implements SurveyInterface {
    uid?: string;
    strategyGuid?: string;
    docGuid?: string;
    fileGuid?: string;
    title?: string;
    version?: number;
    tags?: Array<string>;
    header?: string;
    footer?: string;
    metadata?: SurveyAnswerSet;
    questions?: Array<SurveyQuestion>;

    constructor() {
        // super(props);
    }
}
