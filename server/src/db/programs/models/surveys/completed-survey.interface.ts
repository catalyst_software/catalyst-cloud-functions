import { GeoPointInterface } from "../../../../models/geopoint.model";

export interface CompletedSurvey {
    athleteUid?: string;
    surveyUid: string;
    strategyGuid?: string;
    docGuid?: string;
    fileGuid?: string;
    version?: number;
    completionDate: Date;
    score?: number;
    scoreApplied?: boolean;
    geoPoint: GeoPointInterface;
    responses: Array<CompletedSurveyResponse>;
}

export interface CompletedSurveyResponse {
    key: number;
    value: number;
    index: number;
}
