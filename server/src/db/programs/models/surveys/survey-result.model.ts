import { SurveyAnswerSet } from './survey-answer-set.interface';
import { SurveyQuestion } from './survey-question.interface';
import { SurveyResult as SurveyResultInterface } from '../interfaces/survey-result';

export class SurveyResult implements SurveyResultInterface {
    uid?: string;
    strategyGuid?: string;
    docGuid?: string;
    fileGuid?: string;
    title?: string;
    version?: number;
    tags?: Array<string>;
    header?: string;
    footer?: string;
    metadata?: SurveyAnswerSet;
    questions?: Array<SurveyQuestion>;
    constructor() {
        // super(props);
    }
}
