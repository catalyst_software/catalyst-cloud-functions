import { SurveyAnswer } from './survey-answer.interface';

export interface SurveyQuestion {
    key: number;
    text: string;
    ignoreMetadata: boolean;
    acceptMultiple: boolean;
    answers: Array<SurveyAnswer>;
}
