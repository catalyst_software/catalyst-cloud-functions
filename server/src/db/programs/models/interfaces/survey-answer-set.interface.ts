import { SurveyAnswer } from './survey-answer.interface';

export interface SurveyAnswerSet {
    answers: Array<SurveyAnswer>;
}
