import { ProgramContentType } from "../../../../models/enums/program-content-type";

export interface ProgramDayEntry {
    title?: string;
    description?: string;
    type?: ProgramContentType;
    strategyGuid?: string;
    docGuid?: string;
    fileGuid?: string;
    version?: number;
    embed?: boolean;
    engagement?: boolean;
    hasVideo?: boolean;
    hasSurvey?: boolean;
    hasWorksheet?: boolean;
}
