import { firestore } from 'firebase-admin';

export interface SurveyResult {
    athleteUid?: string;
    completionDate?: firestore.Timestamp;
    docGuid?: string;
    fileGuid?: string;
    responses?: Array<{
        index: number;
        value: string
    }>;
    score?: number;
    scoreApplied?: boolean;
    strategyGuid?: string;
    surveyUid?: string;
    version?: number;
}
