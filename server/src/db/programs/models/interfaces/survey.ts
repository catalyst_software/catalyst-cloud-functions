import { SurveyAnswerSet } from './survey-answer-set.interface';
import { SurveyQuestion } from './survey-question.interface';
export interface Survey {
    uid?: string;
    strategyGuid?: string;
    docGuid?: string;
    fileGuid?: string;
    title?: string;
    version?: number;
    tags?: Array<string>;
    header?: string;
    footer?: string;
    metadata?: SurveyAnswerSet;
    questions?: Array<SurveyQuestion>;
}
