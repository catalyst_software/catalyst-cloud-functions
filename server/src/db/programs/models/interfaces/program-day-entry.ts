import { ProgramContentType } from "../../../../models/enums/enums.model";

export interface ProgramDayEntry {
    title?: string;
    description?: string;
    type?: any;
    programGuid?: string;
    strategyGuid?: string;
    docGuid?: string;
    fileGuid?: string;
    version?: number;
    embed?: boolean;
    engagement?: boolean;
    hasVideo?: boolean;
    hasSurvey?: boolean;
    hasWorksheet?: boolean;
    autoPlay?: boolean;
    tags?: Array<string>;
    
    // NO LONGER USED
    dayIndex?: number
}
