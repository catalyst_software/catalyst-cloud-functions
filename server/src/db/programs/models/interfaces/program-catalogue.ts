import { ProgramIndicator } from './program-indicator';
import { ImageLocator } from '../common/image-locator.interface';
import { ProgramCategoryType } from '../../../../models/enums/program-category-type';

export interface ProgramCatalogue {
    title: string;
    categoryType: ProgramCategoryType;
    images?: Array<ImageLocator>;
    imageSource?: string;
    programs?: Array<ProgramIndicator>;
}
