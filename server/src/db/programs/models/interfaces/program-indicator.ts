import { firestore } from 'firebase-admin';

import { ImageLocator } from '../common/image-locator.interface';
import { SubscriptionType } from '../../../../models/enums/enums.model';
import { ProgramDay } from './program-day';
import { ProgramCategoryType } from '../../../../models/enums/program-category-type';

export interface ProgramIndicator {
    uid?: string;
    creationTimestamp?: firestore.Timestamp;
    programGuid?: string;

    name?: string;
    publisher?: string;
    description?: string;

    images?: Array<ImageLocator>;
    imageSource?: string;

    comingSoon?: boolean;
    activeDate?: firestore.Timestamp;
    startDate?: firestore.Timestamp;

    currentDayIndex?: number;

    callToActionText: string;
    categoryType: any;
    subscriptionType: SubscriptionType;

    programDays?: number;
    isResctricted?: boolean;
    restrictedOrganizationIds?: Array<string>;

    content?: Array<ProgramDay>;
    tags?: Array<string>;
    isPlaceHolder?: boolean;

    contentProvisioning: any

}
