import { ProgramCategoryType } from "../../../../models/enums/program-category-type";

export interface AddToEntityProgramIndicator {
    programGuid?: string;
    entityId?: string;
    categoryType: ProgramCategoryType;
    categoryName?: string;
}
