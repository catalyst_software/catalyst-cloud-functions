export interface SurveyAnswer {
    index?: number;
    key?: number;
    value?: number;
    text?: string;
    selected?: boolean;
}
