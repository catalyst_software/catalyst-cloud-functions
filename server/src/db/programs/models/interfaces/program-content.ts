import { ProgramDay } from './program-day';

export interface ProgramContent {
    programGuid?: string;
    content?: Array<ProgramDay>;
}
