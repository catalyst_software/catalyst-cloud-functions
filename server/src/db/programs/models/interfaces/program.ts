

import { ProgramDay } from './program-day';
import { ProgramIndicator } from './program-indicator';
import { firestore } from 'firebase-admin';

// TODO: Check
export interface Program extends ProgramIndicator {
    // For running programs
    startDate?: firestore.Timestamp;
    currentDayIndex?: number;
    content?: Array<ProgramDay>;
}
