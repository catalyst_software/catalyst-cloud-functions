import { ProgramDayEntry } from './program-day-entry';
export interface ProgramDay {
    label?: string;
    description?: string;
    entries?: Array<ProgramDayEntry>;
    dayIndex?: number;
}

export interface ProgramContent {
    content: Array<ProgramDay>;
    programGuid: string;
}
