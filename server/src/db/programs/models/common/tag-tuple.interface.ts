export interface TagTuple {
    tag: string;
    value: boolean;
}
