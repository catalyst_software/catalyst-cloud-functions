import * as functions from 'firebase-functions';
import { theMainApp } from '../shared/init/initialise-firebase';
import { Config } from './../shared/init/config/config';


// const cors = require('cors')({ origin: true });

// const packer = require('zip-stream');
// const archive = new packer(); // OR new packer(options)

// const Archiver = require('archiver');
/**
 * this is a proper callable function
 */
export const addCoachClaim = functions.https._onCallWithOptions((data, context) => {
    // if (context.auth.token.admin) {
    //   return {
    //     error: "Request not authorized. User must be an Admin to fulfill this request " + data.email
    //   }
    // }
    const { email, claim } = data;
    return grantCoachRole(email, claim).then((userClaims) => {
        return { message: 'Claims Amended', userClaims };
    }).catch((exception) => {
        console.error(exception);
        return { message: 'An exceprion occured', error: JSON.stringify(exception) };
    });
}, {regions: Config.regions});

export async function grantCoachRole(email: string, claim?: string): Promise<any> {
    const user = await theMainApp.auth().getUserByEmail(email);
    // if (user.customClaims && (user.customClaims as any).admin === true) {
    //   return { message: 'Claim already asigned to user' };
    // }

    if (user.customClaims && (<any>user.customClaims).coach === true) {

        console.info('Coach Claim already asigned to user: ', user);
        return { message: 'Claim already asigned to user' };
    }
    console.info('adding claim');
    return theMainApp.auth().setCustomUserClaims(user.uid, {
        testing: false,
        admin: true,
        coach: true
    }).then((da) => {

        console.info('claim Added!!! => user: ', user);
        console.info('claim Added!!! => da: ', da);
        return { user, da };
    })
        .catch((error) => {

            console.error('error adding claim........!!! => error: ', error);
            throw (error);
        });
}


