'use strict';
import * as functions from 'firebase-functions'
import { FirestoreCollection } from '../biometricEntries/enums/firestore-collections';
import { Athlete } from '../../models/athlete/athlete.model';
import { initialiseLoggers } from '../../shared/logger/service/logger-service';
import { AthleteSubscription } from '../../models/athlete/interfaces/athlete-subscription';
import { getAthleteSub } from '../../api/app/v1/controllers/utils/getAthleteSub';
import { Timestamp } from '@google-cloud/firestore';
import { updateExistingAthleteForPurchasing } from '../../api/app/v1/callables/utils/validate-transaction-receipt/updateExistingAthleteForPurchasing';
import { LogInfo } from '../../shared/logger/logger';
import { getGroupDocRef } from '../refs';
import { Group } from '../../models/group/interfaces/group';
import { db } from '../../shared/init/initialise-firebase';
import { OrganizationInterface } from '../../models/organization.model';

// import { Athlete } from '../../../models/athlete/interfaces/athlete'
// import { Group } from '../../models/group/interfaces/group'

export interface AthleteDistanceAggregate {
    uid: string; //(athlete uid),
    orgUid: string;
    // userGlobalDistance: number;
    // aggregate?: {
    //   20201001: [{ distance, duration }]
    // },
    dailies: AthleteDailyAggregate[];
    totalDistance: number;
    totalDuration: number;
    lastSyncDate: Date;

}

export interface AthleteDailyAggregate {
    uid?: string
    // organizationId: string

    userId?: string
    fullName?: string

    dateTime?: Date

    distance: number;
    duration: number;
    // lifestyleEntryType?: LifestyleEntryType
    // value?: number
    // brain?: IpScoreBrainConstituent
    // body?: IpScoreBodyConstituent
    // food?: IpScoreFoodConstituent
    // training?: IpScoreTrainingConstituent
    // programs?: IpScoreProgramsConstituent
}

exports.updateOrgEvolutionData = functions.firestore
    .document(`${FirestoreCollection.AthleteDistanceAggregates}/{aggregateId}`)
    // .onWrite((change, context) 
    .onWrite(async (aggregateSnapshot: functions.Change<FirebaseFirestore.DocumentSnapshot>, context: functions.EventContext) => {
        // client.setApiKey(process.env.SENDGRID_API_KEY);

        const prev = aggregateSnapshot.before;
        const curr = aggregateSnapshot.after;
        console.log({ curr, prev })
        // client.setApiKey('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');

        console.log(
            'updateOrgEvolutionData !!!!!!!!.onCreate().process.listeners.length',
            process.listeners.length
        );
        try {
            const currAggregateDoc: AthleteDistanceAggregate = <AthleteDistanceAggregate>curr.data();

            const { orgUid } = currAggregateDoc

            if (prev && prev.exists) {

                // Upadte Running Totals

                const prevAggregateDoc: AthleteDistanceAggregate = <AthleteDistanceAggregate>prev.data();

                currAggregateDoc.uid = curr.id;
                initialiseLoggers();

                if (prevAggregateDoc && prevAggregateDoc.totalDistance !== currAggregateDoc.totalDuration) {

                    try {
                        // console.log({
                        //     prevAggregateDoc,
                        //     currAggregateDoc
                        // })
                        const theIncreasedDistance = currAggregateDoc.totalDistance - prevAggregateDoc.totalDistance || 0
                        const theIncreasedDuration = currAggregateDoc.totalDuration - prevAggregateDoc.totalDuration || 0

                        console.log('updating with increased values values', {
                            theIncreasedDistance,
                            theIncreasedDuration
                        })
                        await updateOrgEvolution({
                            orgUid,
                            distance: theIncreasedDistance,
                            duration: theIncreasedDuration
                        })

                        return true
                    } catch (e) {
                        console.log('Transaction failure:', e);

                        return e
                    }
                }

                return true;

            } else {

                // Create Our First Entry
                try {
                    // console.log({ currAggregateDoc });
                    const theTotalDistance = currAggregateDoc.totalDistance
                    const theTotalDuration = currAggregateDoc.totalDuration

                    console.log('updating with initial values', {
                        theTotalDistance,
                        theTotalDuration
                    })
                    await updateOrgEvolution({
                        orgUid,
                        distance: theTotalDistance,
                        duration: theTotalDuration
                    })

                    return true
                } catch (e) {
                    console.log('Transaction failure:', e);

                    return e
                }
            }


        } catch (err) {
            console.error(`GLOBAL CATCH -> ${err}`);
            LogInfo(`logerror`);

            return Promise.reject(
                `updateAthleteIndicatorBIO.onUpdate() execution failed! - TRY/CATCH err -> ${err}`
            )
        }
    });

async function updateOrgEvolution(data: {
    orgUid: string,
    distance: number,
    duration: number
}) {

    const {
        orgUid,
        distance,
        duration } = data;

    const orgRef = db.collection(FirestoreCollection.Organizations).doc(orgUid);

    return db.runTransaction(async (t) => {
        const doc = await t.get(orgRef);

        const theOrg = {
            ...doc.data(),
            uid: doc.id
        } as OrganizationInterface;

        let currentEvolution = theOrg.currentEvolution || 
            {
                numberOfParticipants: 0,
                numberOfCountries:  0,
                totalDistance: 0,
                totalDuration: 0,
                updateDate: new Date()
            };

        console.log({ currentEvolution });

        if (!currentEvolution) {
            currentEvolution = {
                numberOfParticipants: 0,
                numberOfCountries:  0,
                totalDistance: 0,
                totalDuration: 0,
                updateDate: new Date()
            }
        }
        currentEvolution.totalDistance += distance;
        currentEvolution.totalDuration += duration;
        currentEvolution.updateDate = new Date();

        t.update(orgRef, {
            currentEvolution
        });
    }).catch((e) => {
        console.error('Transaction failure:', e);

        return false;
    });
}

