// import { firestore } from 'firebase-admin'
// // import { from } from 'rxjs';
// import { LifestyleEntryType } from '../../models/enums/lifestyle-entry-type'
// import { DocMngrActionTypes } from '../biometricEntries/enums/document-manager-action-types'
// import { WellAggregateHistoryCollection } from '../biometricEntries/enums/firestore-aggregate-history-collection'
// import { getHistoryDocId } from '../biometricEntries/services/utils'

// import { LogInfo, LogError } from '../../shared/logger/logger'
// import { getTypes } from '../biometricEntries/services/utils/get-types'
// import { AggregateDocumentDirective } from '../biometricEntries/services/interfaces/aggregate-document-directive'
// import { saveDocument } from './save-document'
// import { createHistoryDocument } from './create-document'
// import { runQuery } from './run-query'
// import { initialiseFirebase } from '../../shared/init/initialise-firebase'
// // import { AggregateDocumentComposer } from '../biometricEntries/services/aggregate-document-composer';

// const aggregateDocumentComposer = new AggregateDocumentComposer();
// const db = initialiseFirebase('bootstrap').db;

// export const createDocuments = async (
//     collection: WellAggregateHistoryCollection,
//     entityId: string,
//     entityName: string
// ) => {
//     // : Promise<AggregateDocumentDirective[]
//     const lifestyleEntryTypekeys = Object.keys(LifestyleEntryType).filter(
//         k => typeof LifestyleEntryType[k as any] === 'string'
//     );

//     const promises = [];
//     // ["Mood", "Pain", ....]
//     // lifestyleEntryTypekeys.filter(entryType => entryType !== 'IpScore').map(type => {
//         lifestyleEntryTypekeys.map(type => {
//         // Mood

//         const entryType = +type;

//         LogInfo(LifestyleEntryType[entryType]);

//         const documentId = getHistoryDocId(entityId, entryType);

//         LogInfo(documentId);

//         // if (documentId !== '') {
//         const documentRef: firestore.DocumentReference = db
//             .collection(collection)
//             .doc(documentId);
//         const promise = runQuery(collection, documentRef).then(
//             async documentSnapshot => {
//                 let directive: AggregateDocumentDirective;

//                 // if (!documentSnapshot.exists) {
//                 console.log(
//                     `===============Creating ${collection} Document for ${
//                         LifestyleEntryType[+entryType]
//                     }`
//                 );
//                 const {
//                     healthComponentType,
//                     aggregateEntityType,
//                     aggregateDocumentType,
//                 } = getTypes(collection, +entryType);
//                 const document = createHistoryDocument(
//                     documentId,
//                     entityId,
//                     entityName,
//                     collection,
//                     healthComponentType,
//                     aggregateEntityType,
//                     aggregateDocumentType,
//                     +entryType
//                 );
//                 console.log(`document -> `, document);

//                 directive = await aggregateDocumentComposer.composeAggregateDocument(
//                     document,
//                     collection,
//                     documentId,
//                     undefined, // group
//                     undefined, // group
//                     // TODO:
//                     document as any
//                 );
//                 directive.actionType = DocMngrActionTypes.Save;
//                 console.log(`directive -> `, directive);

//                 try {
//                     const record = directive.aggregateDocument;

//                     return await saveDocument(
//                         record.toJS(),
//                         collection,
//                         documentId,
//                         db
//                     )
//                 } catch (error) {
//                     console.log(`ERROR CAUGHT -> `, error);
//                     LogError(`ERROR CAUGHT ->  ${error}`);
//                     return error
//                 }
//                 // }
//                 return { SavedDocs: { collection, entryType } }
//                 // return this.processResponse(
//                 //     undefined, collection, documentSnapshot, undefined, documentId, directive);
//             }
//         );

//         promises.push(promise)
//         // } else {
//         //     const message = `Could not get documentId for ${entityId} with LifestyleEntryType ${
//         //         LifestyleEntryType[LifestyleEntryType[entryType]]
//         //     }`
//         //     LogError(message)
//         //     return message
//         // }
//     });

//     return Promise.all(promises)
// };
