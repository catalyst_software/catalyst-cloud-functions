import { isObject } from 'lodash'

import { isHistoryCollection } from './../biometricEntries/aliases/collections/index';

import { LogInfo } from '../../shared/logger/logger'
// import { wellAggregateCollections } from '../biometricEntries/services/aggregate-collections'
// import { createDocuments } from './feth-documents'
import { Athlete } from '../../models/athlete/interfaces/athlete'
import { Group } from '../../models/group/interfaces/group'
import { isAthlete } from '../biometricEntries/aliases/interfaces'

import { wellAggregateCollections } from '../aggregate-collections'
export const createHistoryAggregateDocs = (entity: Athlete | Group) => {
    LogInfo(
        '!!!!!!!!!!!!!!!!!!!!!!!!!!!!................Checking id is group ....................................!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',
        entity
    );
    if (isObject(entity)) {
        const promises = [];
        if (isAthlete(entity)) {
            //  TODO: Check is needed
            // aggregateHistoryCollections.athlete.map(collectionMeta => {
            //     LogInfo(
            //         `..........Creating history doc for................ ${
            //             collectionMeta.collection
            //         }`,
            //         entity
            //     );

            //     const promise = createDocuments(
            //         collectionMeta.collection,
            //         entity.uid,
            //         entity.name
            //     );
            //         // .then(docs => {

            //         //     console.log(docs)
            //         // })
            //         // .catch(err => {
            //         //     console.log(err)
            //         // })

            //     promises.push(promise)
            // })
        } else {
            wellAggregateCollections.group.filter((col) => isHistoryCollection(col.collection)).map(collectionMeta => {
                LogInfo(
                    `..........Creating history doc for................ ${
                    collectionMeta.collection
                    }`,
                    entity
                );
                debugger;
                // const promise = createDocuments(
                //     collectionMeta.collection,
                //     entity.uid ? entity.uid : entity.groupId,
                //     entity.groupName
                // );
                //     // .then(docs => {
                //     //     console.log(docs)
                //     // })
                //     // .catch(err => {
                //     //     console.log(err)
                //     // })

                // promises.push(promise)
            });

            return Promise.all(promises)
        }

        return Promise.all(promises)
    } else return Promise.reject('Invalid Data')
};
