import { isObject } from 'lodash'

import { LogInfo } from '../../shared/logger/logger'
import { wellAggregateCollections } from '../aggregate-collections'
// import { createDocuments } from './feth-documents'
import { Athlete } from '../../models/athlete/interfaces/athlete'
import { Group } from '../../models/group/interfaces/group'
import { isAthlete } from '../biometricEntries/aliases/interfaces'

import { isHistoryCollection } from '../biometricEntries/aliases/collections'

export const createHistoryAggregateDocs = (entity: Athlete | Group) => {
    LogInfo(
        '!!!!!!!!!!!!!!!!!!!!!!!!!!!!................Checking id is group ....................................!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',
        entity
    );
    if (isObject(entity)) {
        const promises = [];
        if (isAthlete(entity)) {

            // const { firstName, lastName, email } = entity.profile;

            // const payload = {
            //     "email_address": email,
            //     "status": "subscribed",
            //     "merge_fields": {
            //         "FNAME": firstName,
            //         "LNAME": lastName
            //     }
            // }

            
        } else {
            wellAggregateCollections.group.filter((col) => isHistoryCollection(col.collection)).map(collectionMeta => {
                LogInfo(
                    `..........Creating history doc for................ ${
                    collectionMeta.collection
                    }`,
                    entity
                );
                    debugger;
                // const promise = createDocuments(
                //     collectionMeta.collection,
                //     entity.uid ? entity.uid : entity.groupId,
                //     entity.groupName
                // );
                // // .then(docs => {
                // //     console.log(docs)
                // // })
                // // .catch(err => {
                // //     console.log(err)
                // // })

                // promises.push(promise)
            });

            return Promise.all(promises)
        }

        return Promise.all(promises)
    } else return Promise.reject('Invalid Data')
};
