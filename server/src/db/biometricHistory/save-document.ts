import { Log, LogInfo } from '../../shared/logger/logger'
// import { LogLevel } from '../biometricEntries/enums/log-level'
import { SocketLogDirective } from '../../shared/logger/interfaces/socket-log-directive'
import { firestore } from 'firebase-admin'
import { removeUndefinedProps } from "../biometricEntries/services/helpers/save-aggregates/utils/remove-undefined-props";
import { convertToPlainObject } from '../../shared/utils';

export const saveDocument = async (
    historyDoc: any,
    athleteCollection,
    documentId,
    db
) => {
    try {
        console.log('Log Info called');
        // LogInfo(`athlete before removeUndefinedProps -> ${hisotruyDOc.toJS()}`)
        removeUndefinedProps(historyDoc);
        // LogInfo(`athlete after removeUndefinedProps -> ${hisotruyDOc}`)

        // removeUndefinedProps(aggrdoc)
        const athleteCollectionRef: firestore.CollectionReference = db.collection(
            athleteCollection
        );

        const docRef = athleteCollectionRef
            .doc(documentId)
            .set(convertToPlainObject(historyDoc));

        // LogInfo(`Running Query-> ${athleteCollection}`)

        return await docRef
            .then(result => {
                console.log(result);
                LogInfo(`${athleteCollection} Document Saved!`);
                // LogInfo(`${writeResult} is the write Result`)

                return historyDoc
            })
            .catch(err => {
                debugger;
                const ff: SocketLogDirective = {
                    message: 'Unable to save document! - err => ' + err,
                    data: {
                        rest: {
                            updatedDoc: JSON.stringify(historyDoc),
                            error: err,
                        },
                    },
                    // level: LogLevel.Error,
                };
                Log(ff);

                return { athlete: historyDoc, error: err }
            })
    } catch (err) {
        debugger;
        const ff: SocketLogDirective = {
            message: 'Unable to save document! - catch err => ' + err,
            data: {
                rest: { updatedDoc: JSON.stringify(historyDoc), error: err },
            },
            // level: LogLevel.Error,
        };
        // Log(ff)
        console.log(ff);
        return { athlete: historyDoc, error: err }
    }
};
