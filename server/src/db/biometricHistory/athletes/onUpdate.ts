'use strict';
import * as functions from 'firebase-functions'

// import { Athlete } from '../../../models/athlete/interfaces/athlete'
// import { Group } from '../../models/group/interfaces/group'

import { FirestoreCollection } from '../../biometricEntries/enums/firestore-collections'
import { LogInfo } from '../../../shared/logger/logger'
import { initialiseLoggers } from '../../../shared/logger/service/logger-service'

// const client = require('@sendgrid/client');
// import client from '@sendgrid/client';
import { getGroupDocRef } from '../../refs';
import { Group } from '../../../models/group/interfaces/group';
import { AthleteSubscription } from '../../../models/athlete/interfaces/athlete-subscription';
import { getAthleteSub } from '../../../api/app/v1/controllers/utils/getAthleteSub';
import { Athlete } from '../../../models/athlete/athlete.model';
import { Timestamp } from '@google-cloud/firestore';
import { updateExistingAthleteForPurchasing } from '../../../api/app/v1/callables/utils/validate-transaction-receipt/updateExistingAthleteForPurchasing';
import { Config } from './../../../shared/init/config/config';

// import sgMail  from '@sendgrid/mail';

exports.updateAthleteIndicatorBIO = functions.firestore
    ._documentWithOptions(`${FirestoreCollection.Athletes}/{athleteId}`, {regions: Config.regions})
    .onUpdate(async (aggregateSnapshot: functions.Change<FirebaseFirestore.DocumentSnapshot>) => {
        // client.setApiKey(process.env.SENDGRID_API_KEY);

        const prev = aggregateSnapshot.before;
        const curr = aggregateSnapshot.after;

        // client.setApiKey('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');

        console.log(
            'updateAthleteIndicatorBIO for Athlete!!!!!!!!.onCreate().process.listeners.length',
            process.listeners.length
        );
        try {

            if (prev && prev.exists) {
                const prevAthValue: Athlete = <Athlete>prev.data();
                const athlete: Athlete = <Athlete>curr.data();


                athlete.uid = curr.id;
                initialiseLoggers();
                console.log(athlete);

                if (prevAthValue && prevAthValue.profile.bio !== athlete.profile.bio) {

                    await updateIndicator(athlete);

                }
                // TODO sort sub exp
                let theSubscription: AthleteSubscription = getAthleteSub(athlete, '')
                if (
                    !(theSubscription.expirationDate.toDate().setHours(0, 0, 0, 0) >= Timestamp.now().toDate().setHours(0, 0, 0, 0)) &&
                    theSubscription.status === -2) {
                    theSubscription.status = -1
                    const theUpdatedAthlete = updateExistingAthleteForPurchasing(
                        athlete, theSubscription.type, theSubscription.status, theSubscription.paymentType,
                        theSubscription.commencementDate.toDate(), // TODO: TIMESTAMP TO DATE
                        // TODO
                        theSubscription.expirationDate.toDate(), //TODO
                        theSubscription.appIdentifier, theSubscription.productIdentifier, 'serverAthUpdate')
                    return curr.ref.set({
                        'profile.subscription': theUpdatedAthlete.profile.subscription,
                        'profile.appSubscriptions': theUpdatedAthlete.profile.appSubscriptions,
                        'currentIpScoreTracking.directive': theUpdatedAthlete.currentIpScoreTracking.directive,
                    }, { merge: true }).then(() => true).catch((error) => {
                        console.error('Unable to update athlete expired subscription', {
                            uid: athlete.uid,
                            namme: athlete.profile.firstName + ' ' + athlete.profile.lastName
                        })
                    })


                } else {
                    return true;
                }
            } else {
                const theAthlete: Athlete = <Athlete>curr.data();

                if (theAthlete && theAthlete.profile && theAthlete.profile.bio !== '') {

                    return await updateIndicator(theAthlete);

                } else {
                    return true
                }
            }


        } catch (err) {
            console.error(`GLOBAL CATCH -> ${err}`);
            LogInfo(`logerror`);

            return Promise.reject(
                `updateAthleteIndicatorBIO.onUpdate() execution failed! - TRY/CATCH err -> ${err}`
            )
        }
    });

async function updateIndicator(athlete: Athlete) {

    const res = await Promise.all(athlete.groups.map(async (grp) => {
        return await getGroupDocRef(grp.groupId)
            .get()
            .then(async (groupSnap: FirebaseFirestore.DocumentSnapshot) => {
                const theGroup = groupSnap.data() as Group;
                const ath = (theGroup.athletes || []).find((at) => at.uid === athlete.uid);
                const athIndex = (theGroup.athletes || []).findIndex((at) => at.uid === athlete.uid);
                if (ath && athIndex >= 0) {
                    // if (ath) {
                    ath.bio = athlete.profile.bio;

                    console.log('ATHLETE INDICATOR UPDATED ON GROUP', {
                        athleteUID: athlete.uid,
                        groupUID: groupSnap.id,
                        bio: theGroup.athletes[athIndex].bio
                    });

                    // theGroup.athletes[athIndex].bio = athlete.profile.bio;
                    console.log(theGroup);
                    await groupSnap.ref.update(theGroup);
                    return true;
                }
                else {
                    console.error('ATHLETE INDICATOR NOT FOUND ON GROUP', {
                        athleteUID: athlete.uid,
                        groupUID: grp.uid
                    });
                    return false;
                }
            });
    }));
    return res;
}

