import { setLoggerLevel } from './../../../../shared/logger/logger'

// Interfaces
// import { AggregateDocumentDirective } from '../services/interfaces/aggregate-document-directive';

import { firestore } from 'firebase-admin'
import { Athlete } from '../../../../models/athlete/interfaces/athlete'
import { Group } from '../../../../models/group/interfaces/group'
import { LogInfo } from '../../../../shared/logger/logger'
import { createHistoryAggregateDocs } from '../../create-history-aggregate-docs'
import { AggregateDocumentDirective } from '../../../biometricEntries/services/interfaces/aggregate-document-directive';

export class AthleteHistoryDocumentManager {
    // implements AggregateDocumentManagerInterface {

    constructor() {
        // if (!this.initialised) {
    }

    doRead(
        entity: Athlete | Group,
        from: string,
        logLevel: string
        ): Promise<Array<AggregateDocumentDirective>> { //: Observable<AggregateDocumentDirective>
    // ) {
        // try {
            LogInfo(
                'doRead().aggserv.processAggregateRequest(entry) called from ' +
                    from
            );

            setLoggerLevel(logLevel);

            // this.aggserv.handleHistoricUpdate(directive);

            console.log('New Athete! => ', entity);
            const entry = (<any>entity).entry;

            const theAthletes = [];
            for (let index = 0; index < 1; index++) {
                theAthletes.push({
                    uid: `${entry.userId}`,
                    userId: `${entry.userId}`,
                    ipScore: 0,
                    isCoach: false,
                    name: `${entry.fullName}`,
                    metadata: {
                        firebaseMessagingId:
                            'd2a3LBR3cns:APA91bEQL2wyslqTQRcgyhorKlabWUodmiGMDf3ngt9mpGpvV8_Tk1OeVLOAnfJGDaje26NWMBBMRZ6k2UwyAVyI7hMYs0gheVOuHDBhjKHSIjQIsqkMu7rW_8MVjBgUd6dPDVJlPbH7',
                        creationTimestamp: firestore.Timestamp.now(),
                    },
                    profile: {
                        bio: 'Other',
                        // email: "igouws@gmail.com",
                        firstName: `${entry.fullName}`,
                        lastName: `${entry.userId}`,
                        location: 'UK',
                        secret: '123456',
                    },
                    groups: [
                        {
                            groupId: 'group1Id',
                            groupName: 'group1',
                            creationTimestamp: firestore.Timestamp.now(),
                        },
                    ],
                })
            }

            const athletePromises = [];
            theAthletes.forEach(theAthlete => {
                const historyPromises =  createHistoryAggregateDocs(theAthlete)
                    .then(docs => {
                        console.log(docs);
                        return docs
                    })
                    .catch(err => {
                        console.log(err);
                        return err
                    });

                    athletePromises.push(historyPromises)
            });

            return Promise.all(athletePromises)
        // } catch (error) {
        //     console.log('Error Caught', error)
        // }
    }
}
