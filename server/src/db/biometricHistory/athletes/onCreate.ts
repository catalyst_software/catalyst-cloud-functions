'use strict';
import * as functions from 'firebase-functions'
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore'

import { Athlete } from '../../../models/athlete/interfaces/athlete'
// import { Group } from '../../models/group/interfaces/group'

import { FirestoreCollection } from '../../biometricEntries/enums/firestore-collections'
// import { HistoryDocumentManager } from './managers/history-document-manager'

import { createHistoryAggregateDocs } from '../create-history-aggregate-docs'
import { LogInfo } from '../../../shared/logger/logger'
import { initialiseLoggers } from '../../../shared/logger/service/logger-service'

// const client = require('@sendgrid/client');
import client from '@sendgrid/client';
import { Config } from './../../../shared/init/config/config';
// import sgMail  from '@sendgrid/mail';

exports.createAthleteHistoryAggregates = functions.firestore
    ._documentWithOptions(`${FirestoreCollection.Athletes}/{athleteId}`, {regions: Config.regions})
    .onCreate(async (aggregateSnapshot: DocumentSnapshot, context) => {
        // client.setApiKey(process.env.SENDGRID_API_KEY);

        client.setApiKey('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');

        console.log(
            'createHistoryAggregates for Athlete!!!!!!!!.onCreate().process.listeners.length',
            process.listeners.length
        );
        try {
            const athlete: Athlete = <Athlete>aggregateSnapshot.data();
            athlete.uid = aggregateSnapshot.id;
            initialiseLoggers();
            console.log(athlete);
            // return Promise.all(createHistoryAggregateDocs(athlete))
            // const aggregateDocumentManager = new HistoryDocumentManager()


            // return aggregateDocumentManager.doRead(athlete, 'from', 'info')
            const createHistoryDocsResult = await createHistoryAggregateDocs(athlete).then((docs) => {
                console.log('History Docs Created');
                return docs
            })
                .catch((err) => {

                    console.error(`createAthleteHistoryAggregates.onCreate() execution failed! - err -> ${err}`);
                    return Promise.reject(
                        `createAthleteHistoryAggregates.onCreate() execution failed! - err -> ${err}`
                    )
                })


            if (athlete.metadata.registeredForMarketingCommunications) {
                await addToMailingList(athlete).then(() => {
                    console.log('User added to the mailing list')
                }).catch(() =>
                    console.log('Error adding User to the mailing list'));
            } else {

                console.log('User NOT added to the mailing list')
            }

            return createHistoryDocsResult

        } catch (err) {
            console.error(`GLOBAL CATCH -> ${err}`);
            LogInfo(`logerror`);

            return Promise.reject(
                `createAthleteHistoryAggregates.onCreate() execution failed! - TRY/CATCH err -> ${err}`
            )
        }
    });


async function addToMailingList(athlete: Athlete) {

    // client.setApiKey('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');

    const listreq = {
        method: 'GET',
        url: '/v3/contactdb/lists'
    } as any;

    return await client.request(listreq)
        .then(async ([response, body]) => {
            console.log(response.statusCode);
            console.log(response.body);
            const { lists } = response.body as {
                lists: Array<{
                    id: number; //8993961
                    name: string; // "Test"
                    recipient_count: number; // 1
                }>;
            };

            const { email, firstName, lastName } = athlete.profile

            const conatctDetails = [
                {
                    email,
                    "first_name": firstName,
                    "last_name": lastName
                }
            ];

            const addRecpientPayload = {
                body: conatctDetails,
                method: 'POST',
                url: '/v3/contactdb/recipients'
            } as any;

            const recipientIDs = await client.request(addRecpientPayload)
                .then(async ([addRecpientResponse]) => {
                    console.log(addRecpientResponse.statusCode);
                    console.log(addRecpientResponse.body);
                    return addRecpientResponse.body[
                        // "recipient_id1",
                        // "recipient_id2"
                        'persisted_recipients'];
                });


            const listID = lists.find((list) => list.name === 'Test').id;

            const addToListPayload = {
                body: recipientIDs,
                method: 'POST',
                url: `/v3/contactdb/lists/${listID}/recipients`
            } as any

            await client.request(addToListPayload)
                .then(([addToListResponse, addToListBody]) => {
                    console.log(addToListResponse.statusCode);
                    console.log(addToListResponse.body);
                });
        }).catch((err) => {
            console.error(err);
            return err;
        });
}
// exports.createGroupHistoryAggregates = functions.firestore
//     .document(`${FirestoreCollection.Group}/{groupId}`)

//     .onCreate((aggregateSnapshot: DocumentSnapshot) => {
//         console.log(
//             'createHistoryAggregates.onCreate().process.listeners.length',
//             process.listeners.length
//         )
//         try {
//             const group: Group = <Group>aggregateSnapshot.data()

//             group.uid = aggregateSnapshot.id

//             console.log(group)
//             // const aggregateDocumentManager = new HistoryDocumentManager()
//             // return aggregateDocumentManager
//             //     .doRead(
//             //         group,
//             //         DocMngrActionTypes.CreateHistoryDocs,
//             //         'createHistoryAggregates.onCreate()',
//             //         'info'
//             //     )
//             //     .then(docDirective => {
//             //         console.log('OnCreate Promise Received', docDirective)
//             //         return docDirective
//             //     })
//             //     .catch(() => {
//             //         debugger
//             //     })

//             return Promise.resolve()
//         } catch (err) {
//             return Promise.reject(
//                 'onCreate().doRead(biometricEntry) execution failed ARGH Promise!'
//             )
//         }
//     })
