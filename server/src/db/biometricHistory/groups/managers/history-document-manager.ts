import { setLoggerLevel } from './../../../../shared/logger/logger'

// Interfaces
// import { AggregateDocumentDirective } from '../services/interfaces/aggregate-document-directive';

import { firestore } from 'firebase-admin'
import { Group } from '../../../../models/group/interfaces/group'
import { LogInfo } from '../../../../shared/logger/logger'
import { createHistoryAggregateDocs } from '../../create-history-aggregate-docs'
import { AggregateDocumentDirective } from '../../../biometricEntries/services/interfaces/aggregate-document-directive';

export class GroupHistoryDocumentManager {
    // implements AggregateDocumentManagerInterface {

    constructor() {
        // if (!this.initialised) {
    }

    /**
     * Reads the BiometricAggregateModel documets
     *
     * Creates a new Observable with the aggregateDocs Subject as the source.
     * You can do this to create customize Observer-side logic of the Subject
     * and conceal it from code that uses the Observable.
     *
     * @param {AthleteBiometricEntryModel} group
     * @returns {Observable<AggregateDocumentDirective>}
     * @memberof AggregateDocumentManager
     */
    doRead(
        group: Group,
        from: string,
        logLevel: string
        ): Promise<Array<AggregateDocumentDirective>> { //: Observable<AggregateDocumentDirective>
    // ): any {
        //: Observable<AggregateDocumentDirective>

        // debugging
        LogInfo(
            'doRead().aggserv.processAggregateRequest(entry) called from ' +
                from
        );

        setLoggerLevel(logLevel);

        // this.aggserv.handleHistoricUpdate(directive);

        // console.log('New Athete! => ', group)
        // const result = this.aggserv.processAggregateRequest(entity, type);

        // return result;
        // const theGroup = {
        //     uid: group.,
        //     groupId: 'group1Id',
        //     groupName: 'group1',
        //     creationTimestamp: firestore.Timestamp.now(),

        //     // Extensions
        //     organizationId: 'string',
        // }

        const groupPromises = [];

        groupPromises.push(createHistoryAggregateDocs(group).then((docs) =>  {
            console.log(docs)
        })
        .catch((err) =>  {
            console.log(err)
        }));

        return Promise.all(groupPromises);

        // const theGroup2 = {
        //     uid: 'group2Id',
        //     groupId: 'group2Id',
        //     groupName: 'group2',
        //     creationTimestamp: firestore.Timestamp.now(),

        //     // Extensions
        //     organizationId: 'string'
        // }
        // const result = this.aggserv.processAggregateRequest(theGroup2, DocMngrActionTypes.CreateHistoryDocs)

        const theAthletes = [];
        for (let index = 0; index < 1; index++) {
            theAthletes.push({
                uid: `winner${index}`,
                userId: `winner${index}`,
                ipScore: 0,
                isCoach: false,
                name: `winner${index}`,
                metadata: {
                    firebaseMessagingId:
                        'd2a3LBR3cns:APA91bEQL2wyslqTQRcgyhorKlabWUodmiGMDf3ngt9mpGpvV8_Tk1OeVLOAnfJGDaje26NWMBBMRZ6k2UwyAVyI7hMYs0gheVOuHDBhjKHSIjQIsqkMu7rW_8MVjBgUd6dPDVJlPbH7',
                    creationTimestamp: firestore.Timestamp.now(),
                },
                profile: {
                    bio: 'Other',
                    // email: "igouws@gmail.com",
                    firstName: `winner${index}`,
                    lastName: `winner_ln${index}`,
                    location: 'UK',
                    secret: '123456',
                },
                groups: [
                    {
                        groupId: 'group1Id',
                        groupName: 'group1',
                        creationTimestamp: firestore.Timestamp.now(),
                    },
                ],
            })
        }

        // const results = []

        theAthletes.forEach(theAthlete => {
            groupPromises.push(createHistoryAggregateDocs(theAthlete))
        })

        // return groupPromises
    }
}
