import { Subject } from 'rxjs';
import { AggregateDocumentDirective } from '../../../../biometricEntries/services/interfaces/aggregate-document-directive';
import { AthleteBiometricEntryModel } from '../../../../biometricEntries/well/athelete-biometric-entry.model';
import { Athlete } from '../../../../../models/athlete/interfaces/athlete';
import { Group } from '../../../../../models/group/interfaces/group';
import { DocMngrActionTypes } from '../../../../biometricEntries/enums/document-manager-action-types';


export interface AggregateDocumentManager {
    aggregateDocs: Subject<AggregateDocumentDirective>
    
    doRead(entity: AthleteBiometricEntryModel | Athlete | Group, type: DocMngrActionTypes, from: string, logLevel: string): void;
    // doWrite(entry: AthleteBiometricEntry, aggregateDoc: BiometricAggregateModel): void
}