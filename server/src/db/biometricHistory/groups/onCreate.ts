'use strict';
import * as functions from 'firebase-functions'
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore'

// import { Group } from '../../models/group/interfaces/group'

import { FirestoreCollection } from '../../biometricEntries/enums/firestore-collections'
// import { HistoryDocumentManager } from './managers/history-document-manager'

import { createHistoryAggregateDocs } from '../create-history-aggregate-docs'
import { LogInfo } from '../../../shared/logger/logger'
import { initialiseLoggers } from '../../../shared/logger/service/logger-service'
import { Group } from '../../../models/group/interfaces/group';
import { Config } from './../../../shared/init/config/config';

exports.createGroupHistoryAggregates = functions.firestore
    ._documentWithOptions(`${FirestoreCollection.Groups}/{groupId}`, {regions: Config.regions})
    .onCreate((aggregateSnapshot: DocumentSnapshot, context) => {
        console.log(
            'createHistoryAggregates for Group!!!!!!!!.onCreate().process.listeners.length',
            process.listeners.length
        );
        try {
            const group: Group = <Group>aggregateSnapshot.data();
            group.uid = aggregateSnapshot.id;
            initialiseLoggers();
            console.log(group);
            // return Promise.all(createHistoryAggregateDocs(group))
            // const aggregateDocumentManager = new HistoryDocumentManager()

            // return aggregateDocumentManager.doRead(group, 'from', 'info')
            return createHistoryAggregateDocs(group)
            .then((docs) =>  {
                console.log(docs);
                return Promise.resolve(
                  docs
                );
                return docs
            })
            .catch((err) =>  {
                console.error(err);
                return Promise.reject(
                    `createGroupHistoryAggregates.onCreate() execution failed! - err -> ${err}`
                );
                return err
            })
        } catch (err) {
            console.error(`GLOBAL CATCH -> ${err}`);
            LogInfo(`logerror`);

            return Promise.reject(
                `createGroupHistoryAggregates.onCreate() execution failed! - TRY/CATCH err -> ${err}`
            )
        }
    });

// exports.createGroupHistoryAggregates = functions.firestore
//     .document(`${FirestoreCollection.Group}/{groupId}`)

//     .onCreate((aggregateSnapshot: DocumentSnapshot) => {
//         console.log(
//             'createHistoryAggregates.onCreate().process.listeners.length',
//             process.listeners.length
//         )
//         try {
//             const group: Group = <Group>aggregateSnapshot.data()

//             group.uid = aggregateSnapshot.id

//             console.log(group)
//             // const aggregateDocumentManager = new HistoryDocumentManager()
//             // return aggregateDocumentManager
//             //     .doRead(
//             //         group,
//             //         DocMngrActionTypes.CreateHistoryDocs,
//             //         'createHistoryAggregates.onCreate()',
//             //         'info'
//             //     )
//             //     .then(docDirective => {
//             //         console.log('OnCreate Promise Received', docDirective)
//             //         return docDirective
//             //     })
//             //     .catch(() => {
//             //         debugger
//             //     })

//             return Promise.resolve()
//         } catch (err) {
//             return Promise.reject(
//                 'onCreate().doRead(biometricEntry) execution failed ARGH Promise!'
//             )
//         }
//     })
