import * as functions from 'firebase-functions';
import { PubSub } from '@google-cloud/pubsub';
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore';
import { FirestoreCollection } from '../biometricEntries/enums/firestore-collections';
import { AsyncJob } from './interfaces/async-job.interface';
import { logTaskQueueMessage } from '../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import { isArray } from 'lodash';
import { AsyncJobTaskStatusType } from './enums/async-job-task-status-type.enum';
import { updateAsyncJob, getVideoConvertStreamType } from './helpers/helpers';
import { AsyncJobTask } from './interfaces/async-job-task.interface';
import { AsyncJobTaskType } from './enums/async-job-task-type.enum';
import { Config } from './../../shared/init/config/config';


exports.manageAsyncJobQueueOnCreate = functions.firestore
    ._documentWithOptions(`${FirestoreCollection.AsyncJobQueue}/{jobId}`, {regions: Config.regions})
    .onCreate(async (jobSnapshot: DocumentSnapshot, context) => {
        const job = <AsyncJob>jobSnapshot.data();
        job.uid = jobSnapshot.id;
        job.creationTimestamp = (new Date()).toISOString() as any;

        let msg = `Async job ${job.uid} received at ${new Date()}`;
        logTaskQueueMessage(job, msg);

        if (isArray(job.jobTasks) && job.jobTasks.length) {
            job.jobTasks.forEach((t: AsyncJobTask) => {
                t.asyncJobDocId = job.uid;
                t.creatorId = job.creatorId;
            });
            const firstTask = job.jobTasks[0];
            firstTask.status = AsyncJobTaskStatusType.Scheduled;
            msg = `Scheduling async job task execution ${firstTask.type}`;
            logTaskQueueMessage(job, msg);
            job.percentComplete = 0;
            job.status = AsyncJobTaskStatusType.Running;

            const pubSubClient = new PubSub();
            // const info = getProjectTaskInfo(firstTask.type);
            await updateAsyncJob(job, jobSnapshot);
            const taskBuffer = Buffer.from(JSON.stringify(firstTask), 'utf8');

            let messageId: string;
            switch (firstTask.type) {
                case AsyncJobTaskType.ConvertVideo:
                    const stream = await getVideoConvertStreamType(job.organizationId);
                    try {
                        messageId = await pubSubClient.topic(stream).publish(taskBuffer);
                        console.log(`Message ${messageId} published.`);
                    } catch (err) {
                        console.error(err);
                    }
                    break;
                case AsyncJobTaskType.CreateVideoCard:
                    break;
                case AsyncJobTaskType.DeleteFile:
                    break;
            }
            
            
        }

        return true;

    });