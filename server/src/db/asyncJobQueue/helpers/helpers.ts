import { AsyncJob } from "../interfaces/async-job.interface";
import { DocumentSnapshot } from "@google-cloud/firestore";
import { AsyncJobTaskType } from "../enums/async-job-task-type.enum";
import { isProductionProject } from '../../../api/app/v1/is-production';
import { AsyncJobTask } from "../interfaces/async-job-task.interface";
import { AsyncJobTaskStatusType } from "../enums/async-job-task-status-type.enum";
import { isArray } from "lodash";
import { ProjectTaskInfo } from '../interfaces/project-task-info.interface';
import { ConvertVideoStreamType } from "../enums/convert-video-channel-type.enum";
import { getOrganizationDocument } from '../../../api/app/v1/callables/utils/warehouse/get-athlete-org-segments';
import { OrganizationInterface } from '../../../models/organization.model';

const { CloudTasksClient } = require('@google-cloud/tasks');

export const getVideoConvertStreamType = async (orgId: string): Promise<string> => {
    if (orgId) {
        return getOrganizationDocument(orgId)
            .then((org: OrganizationInterface) => {
                if (org.config && org.config.convertVideoStream) {
                    console.log(`Organization ${orgId} includes convert video strem configuration: ${org.config.convertVideoStream}`);
                    return org.config.convertVideoStream;
                } else {
                    console.log(`Organization ${orgId} DOES NOT include convert video strem configuration.`);
                    return ConvertVideoStreamType.FreeStream1;
                }
            })
            .catch((err) => {
                console.error(`An error was thrown when retrieving org document ${orgId}`);
                return ConvertVideoStreamType.FreeStream1;
            });
    } else {
        return Promise.resolve(ConvertVideoStreamType.FreeStream1);
    }
}

export const updateAsyncJob = async (job: AsyncJob, jobSnapshot: DocumentSnapshot): Promise<void> => {
    await jobSnapshot.ref.update(job)
        .then(() => {
            console.log(`Async job ${job.uid} successfully updated.`);

            return Promise.resolve();
        })
        .catch((e) => {
            console.error(`Async job ${job.uid} update failed: ${e.message}`);

            return Promise.resolve();
        });
}

export const getProjectTaskInfo = (taskType: AsyncJobTaskType): ProjectTaskInfo => {
    let projectId: string = '';
    let location: string = '';
    let queue = '';
    let service = '';
    const relativeUri = '/video_convert_request';
    const isProd = isProductionProject();

    if (isProd) {
        projectId = 'inspire-219716';
        location = 'europe-west3';
    } else {
        projectId = 'inspire-1540046634666';
        location = 'us-central1';
    }

    switch (taskType) {
        case AsyncJobTaskType.ConvertVideo:
            queue = 'convert-video-queue';
            service = 'convert-video-service';
            break;
        case AsyncJobTaskType.CreateVideoCard:
            queue = 'create-video-card-queue';
            service = 'create-video-card-service';
            break;
    }

    return {
        projectId,
        location,
        queue,
        service,
        relativeUri
    } as ProjectTaskInfo;
}

export const createQueue = async (info: ProjectTaskInfo): Promise<void> => {
    
    // Instantiates a client.
    const client = new CloudTasksClient();

    try {
        const [response] = await client.createQueue({
            // The fully qualified path to the location where the queue is created
            parent: client.locationPath(info.projectId, info.location),
            queue: {
                // The fully qualified path to the queue
                name: client.queuePath(info.projectId, info.location, info.queue),
                appEngineHttpQueue: {
                    appEngineRoutingOverride: {
                        // The App Engine service that will receive the tasks.
                        service: info.service
                    }
                }
            }
        });
        
        console.log(`Created queue ${response.name}`);
    } catch (e) {
        console.log('Queue creation failed.');
    }
      
    return Promise.resolve();
}

export const createTask = async (jobTask: AsyncJobTask, info: ProjectTaskInfo): Promise<void> => {
    // Instantiates a client.
    const client = new CloudTasksClient();

    const parent = client.queuePath(info.projectId, info.location, info.queue);

    const task = {
        appEngineHttpRequest: {
            httpMethod: 'POST',
            relativeUri: `/${info.relativeUri}`,
            appEngineRouting: {
                service: info.service
            },
            body: Buffer.from(JSON.stringify(jobTask)).toString('base64')
        }
    };

    console.log('Sending task:');
    console.log(task);
    // Send create task request.
    const request = {parent, task};
    const [response] = await client.createTask(request);
    const name = response.name;
    console.log(`Created task ${name}`);

    return Promise.resolve();
}

export const getNextTask = (task: AsyncJob) => {
    let nextTask: AsyncJobTask = undefined;
    task.jobTasks.forEach((t: AsyncJobTask) => {
        if (!!t && t.status === AsyncJobTaskStatusType.Pending && !nextTask) {
            nextTask = t;
        }
    });

    return nextTask;
}

export const getAsyncJobPercentComplete = (job: AsyncJob): number => {
    let percent = 0;
    if (isArray(job.jobTasks) && job.jobTasks.length) {
        const ln = job.jobTasks.length;
        let complete = 0;
        job.jobTasks.forEach((t: AsyncJobTask) => {
            if (!!t && t.status === AsyncJobTaskStatusType.Completed) {
                complete++;
            }
        });

        percent = (complete / ln) * 100;
    }

    return percent;
}

export const getLastTaskStatus = (task: AsyncJob): AsyncJobTaskStatusType => {
    let lastTaskIndex: number = -1;
    let index = 0;
    task.jobTasks.forEach((t: AsyncJobTask) => {
        if (!!t && t.status === AsyncJobTaskStatusType.Pending && lastTaskIndex === -1) {
            lastTaskIndex = index - 1;
            if (lastTaskIndex === -1) {
                lastTaskIndex = 0
            }
        }

        index++;
    });

    

    return task.jobTasks[lastTaskIndex].status;
}