import { AsyncJobTaskType } from "../enums/async-job-task-type.enum";
import { AsyncJobTaskStatusType } from "../enums/async-job-task-status-type.enum";
import { CreateVideoCardTaskPayload, ConvertVideoJobTaskPayload } from "./async-job-task-payloads.interface";

export interface AsyncJobTask {
    type: AsyncJobTaskType;
    status: AsyncJobTaskStatusType;
    payload: ConvertVideoJobTaskPayload | CreateVideoCardTaskPayload;
    creatorId?: string;
    asyncJobDocId?: string;
    percentComplete?: number;
    logs?: Array<string>;
}
