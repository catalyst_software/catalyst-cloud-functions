import { AsyncJobTask } from './async-job-task.interface';
import { AsyncJobTaskStatusType } from '../enums/async-job-task-status-type.enum';

export interface AsyncJob {
    uid?: string;
    creatorId: string;
    organizationId: string;
    status: AsyncJobTaskStatusType;
    jobTasks: Array<AsyncJobTask>;
    creationTimestamp?: Date;
    percentComplete?: number;
    logs?: Array<string>;
}
