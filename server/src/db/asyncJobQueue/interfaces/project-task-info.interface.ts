export interface ProjectTaskInfo {
    projectId: string;
    location: string; 
    queue: string;
    service: string;
    relativeUri: string;
}