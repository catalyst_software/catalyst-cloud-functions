import { ChannelType } from './../enums/channel-type.enum';
import { ContentFeedCardType } from '../../../models/report-model';
import { NotificationCardType } from '../../../models/enums/UpdateNotificationCardMetadataDirective';

export interface ConvertVideoJobTaskPayload {
    inputPath: string;
    inputFile: string;
    outputPath: string;
    outputFile: string;
    thumbnailOutputPath: string;
    thumbnailTimeLocation: string;
}

export interface CreateVideoCardTaskPayload {
    name: string;
    title: string;
    description: string;
    filePath: string;
    cardType?: ContentFeedCardType;
    notificationCardType?: NotificationCardType;
    channelType?: ChannelType;
    entityId?: string;
    creatorId?: string;
    creatorName?: string;
    creatorImageUrl?: string;
    creationTimestamp?: Date;
    dismissable?: boolean;
    trackViews?: boolean;
    isSticky?: boolean;
    isStickyStatic?: boolean;
    stickyStaticExpiryDate?: Date;
    stickyStaticExpiryDayCount?: number;
    mediaContentEntry?: any;
}