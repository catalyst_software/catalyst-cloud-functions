import * as functions from 'firebase-functions';
import { PubSub } from '@google-cloud/pubsub';
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore';
import { FirestoreCollection } from '../biometricEntries/enums/firestore-collections';
import { AsyncJob } from './interfaces/async-job.interface';
import { logTaskQueueMessage } from '../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import { isArray } from 'lodash';
import { AsyncJobTaskStatusType } from './enums/async-job-task-status-type.enum';
import { getNextTask, updateAsyncJob, getAsyncJobPercentComplete, getLastTaskStatus, getVideoConvertStreamType } from './helpers/helpers';
import { AsyncJobTaskType } from './enums/async-job-task-type.enum';
import { Config } from './../../shared/init/config/config';


exports.manageAsyncJobQueueOnUpdate = functions.firestore
    ._documentWithOptions(`${FirestoreCollection.AsyncJobQueue}/{jobId}`, {regions: Config.regions})
    .onUpdate(async (change: functions.Change<DocumentSnapshot>, context) => {
        const beforeJob = <AsyncJob>change.before.data();
        const afterJob = <AsyncJob>change.after.data();
        const lastTaskStatus = getLastTaskStatus(afterJob);
        let msg = '';

        if (lastTaskStatus !== AsyncJobTaskStatusType.Failed) {
            if (afterJob.status !== beforeJob.status) {

                msg = `Async job ${afterJob.uid} received at ${new Date()}`;
                logTaskQueueMessage(afterJob, msg);

                if (isArray(afterJob.jobTasks) && afterJob.jobTasks.length) {
                    const nextTask = getNextTask(afterJob);
                    
                    if (!!nextTask) {
                        afterJob.percentComplete = getAsyncJobPercentComplete(afterJob);
                        msg = `Async job percent complete ${afterJob.percentComplete }`;
                        logTaskQueueMessage(afterJob, msg);
                        nextTask.status = AsyncJobTaskStatusType.Scheduled;
                        msg = `Scheduling next async job task execution ${nextTask.type}`;
                        logTaskQueueMessage(afterJob, msg);

                        const pubSubClient = new PubSub();
                        await updateAsyncJob(afterJob, change.after);

                        const taskBuffer = Buffer.from(JSON.stringify(nextTask), 'utf8');

                        let messageId: string;
                        switch (nextTask.type) {
                            case AsyncJobTaskType.ConvertVideo:
                                const stream = await getVideoConvertStreamType(afterJob.organizationId);
                                try {
                                    messageId = await pubSubClient.topic(stream).publish(taskBuffer);
                                    console.log(`Message ${messageId} published.`);
                                } catch (err) {
                                    console.error(err);
                                }
                                break;
                            case AsyncJobTaskType.CreateVideoCard:
                                break;
                            case AsyncJobTaskType.DeleteFile:
                                break;
                        }
                    } else {
                        msg = `No pending tasks found.  Setting job status to complete for async job ${afterJob.uid}.`
                        logTaskQueueMessage(afterJob, msg);
                        afterJob.percentComplete = 100;
                        afterJob.status = AsyncJobTaskStatusType.Completed;

                        await updateAsyncJob(afterJob, change.after);
                    }
                }

                return true;
            } else {
                console.log('Async job status has not changed. Aborting job processing.');

                return true;
            }
        } else {
            msg = `Last sync job status is failed.   Aborting job processing for async job ${afterJob.uid}.`
            logTaskQueueMessage(afterJob, msg);
            afterJob.percentComplete = 100;
            afterJob.status = AsyncJobTaskStatusType.Failed;
            await updateAsyncJob(afterJob, change.after);

            return false;
        }

    });