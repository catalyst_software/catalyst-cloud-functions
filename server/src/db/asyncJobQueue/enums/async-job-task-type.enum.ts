export enum AsyncJobTaskType {
    ConvertVideo = 'convertVideo',
    CreateVideoCard = 'createVideoCard',
    DeleteFile = 'deleteFile'
}