export enum AsyncJobTaskStatusType {
    Pending = 'pending',
    Scheduled = 'scheduled',
    Running = 'running',
    Failed = 'failed',
    Completed = 'completed'
}