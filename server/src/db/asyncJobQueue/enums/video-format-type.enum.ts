export enum VideoFormatType {
    MP4 = 'mp4',
    M3U8 = 'm3u8'
}