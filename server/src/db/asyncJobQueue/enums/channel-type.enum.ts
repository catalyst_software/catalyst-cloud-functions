export enum ChannelType {
    Universal = 'universal',
    Global = 'global', // Global channel is a channel for all orgs under a given content provider
    Organization = 'organization',
    Group = 'group',
    Athlete = 'athlete'
}
