'use strict';
import * as functions from 'firebase-functions';

import { FirestoreCollection } from '../biometricEntries/enums/firestore-collections';
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore';
import { Athlete } from '../../models/athlete/athlete.model';
import { getQueryResults } from '../../api/app/v1/callables/utils/big-query/get-query-results';
import { SegmentAthleteDimension } from '../../api/app/v1/callables/utils/warehouse/segment-athlete-dimension.interface';
import { SegmentDimension } from '../../api/app/v1/callables/utils/warehouse/segment-dimension.interface';
import { isArray } from 'lodash';
import { getOrgDocFromSnapshot, getOrgDocRef } from '../refs/index';
import { SegmentType } from '../../api/app/v1/callables/utils/warehouse/segment-type.enum';
import { bigqueryClient } from '../../api/app/v1/callables/utils/big-query/initBQ';
import { AthleteDimension } from '../../api/app/v1/callables/utils/warehouse/athlete-dimension.interface';
import { initialiseCloudSql } from '../../shared/init/initialise-cloud-sql';
import { addAthleteLookupRecord, cleanString } from './../../api/app/v1/callables/get-lookup-values';
import { Client } from 'pg';
import { getSegmentAthleteExistsInBigQuery, getSegmentExistsInBigQuery } from './../../pubsub/subscriptions/taskWorkers/helpers/helpers';
import { Config } from './../../shared/init/config/config';


interface AthleteMetadataCounts {
    numberOfParticipants?: number;
    numberOfCountries?: number;
}

const getDateDimensionValueFromStringDate = (date: string): number => {
    if (date) {
        const parts = date.split('/');
        if (parts.length === 3) {
            const formatted = `${parts[2]}${parts[1]}${parts[0]}`;
            console.log(`Converted date dimension value: ${formatted}`);

            return parseInt(formatted);
        } else {
            return null;
        }
    } else {
        return null;
    }
}

const getAthleteDimension = (athlete: Athlete): AthleteDimension => {
    if (athlete) {
        return {
            athlete_id: athlete.uid,
            first_name: athlete.profile.firstName,
            last_name: athlete.profile.lastName,
            full_name: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
            local_birth_date: getDateDimensionValueFromStringDate(athlete.profile.dob),
            sex: athlete.profile.sex.toLowerCase(),
            location: athlete.profile.location,
            imageUrl: athlete.profile.imageUrl,
            sport: athlete.profile.sport || '',
            bio: athlete.profile.bio || ''
        } as AthleteDimension;
    } else {
        return undefined;
    }
}

const migrateAthleteToLookupTable = async (athlete: Athlete): Promise<void> => {
    let db: Client;
    debugger
    try {
        db = await initialiseCloudSql();
        const sql = `delete from athletes where email = '${cleanString(athlete.profile.email)}'`;
        console.log(`Running postgres existence check sql: ${sql}`)
        const res = await db.query(sql);
        await addAthleteLookupRecord(athlete, db);
    }
    catch (err) {
        console.error(err.stack);

        return Promise.resolve();
    }
}

const getAthleteMetadataCountsInBigQuery = async (): Promise<AthleteMetadataCounts> => {
    const datasetId = 'catalystDW';
    const athleteTableId = 'athleteDim';
    let query = `select count(distinct location) as count from ${datasetId}.${athleteTableId} where location is not null and location != ''`;
    let rows = await getQueryResults<number>(query);
    const numberOfCountries = (isArray(rows) && rows.length) ? rows[0]['count'] : 0;

    query = `select count(first_name) as count from ${datasetId}.${athleteTableId}`;
    rows = await getQueryResults<number>(query);
    const numberOfParticipants = (isArray(rows) && rows.length) ? rows[0]['count'] : 0;
    return Promise.resolve({
        numberOfCountries,
        numberOfParticipants
    } as AthleteMetadataCounts)
}

exports.manageAthleteCreation = functions.firestore
    ._documentWithOptions(`${FirestoreCollection.Athletes}/{athleteId}`, {regions: Config.regions})
    .onCreate(async (athleteSnapshot: DocumentSnapshot, context) => {
        const datasetId = 'catalystDW';
        const athleteTableId = 'athleteDim';
        const segmentTableId = 'segmentDim';
        const segmentAthleteTableId = 'segmentAthleteDim';

        const athlete = <Athlete>athleteSnapshot.data();
        console.log('Athlete onCreate manageAthleteCreation fired', athlete);
        await migrateAthleteToLookupTable(athlete);

        // BigQuery Stuff
        const athleteRows: Array<AthleteDimension> = [];
        const segmentRows: Array<SegmentDimension> = [];
        const segmentAthleteRows: Array<SegmentAthleteDimension> = [];
        const orgId = athlete.organizationId;
        const orgExists = await getSegmentExistsInBigQuery(orgId);

        const orgDoc = await getOrgDocFromSnapshot(orgId);
        const orgRef = await getOrgDocRef(orgId);

        if (!orgExists) {
            console.log(`Segment for for orgId ${orgId} DOES NOT exist!`);
            segmentRows.push({
                segment_id: orgId,
                parent_segment_id: orgDoc.parentOrganizationId,
                segment_name: orgDoc.name,
                segment_type: SegmentType.Organization
            } as SegmentDimension);

        } else {
            console.log(`Segment for orgId ${orgId} exists!`);
        }

        let exists = await getSegmentAthleteExistsInBigQuery(orgId, athleteSnapshot.id);
        if (!exists) {
            segmentAthleteRows.push({
                segment_id: orgId,
                athlete_id: athleteSnapshot.id
            } as SegmentAthleteDimension);
        }

        if (isArray(athlete.groups)) {
            for (const group of athlete.groups) {
                const groupExists = await getSegmentExistsInBigQuery(group.groupId);
                if (!groupExists) {
                    segmentRows.push({
                        segment_id: group.groupId,
                        parent_segment_id: orgId,
                        segment_name: group.groupName,
                        segment_type: SegmentType.Group
                    } as SegmentDimension);
                }

                segmentAthleteRows.push({
                    segment_id: group.groupId,
                    athlete_id: athleteSnapshot.id
                } as SegmentAthleteDimension);
            }
        }

        athleteRows.push(getAthleteDimension(athlete));

        let msg = '';
        let theRes = await bigqueryClient
            .dataset(datasetId)
            .table(athleteTableId)
            .insert(athleteRows)
            .then((rowCount) => {
                return {
                    rowCount,
                    errors: undefined
                }
            })
            .catch((e) => {
                console.log(e.errors);
                return {
                    rowCount: undefined,
                    errors: e.errors
                }
            });

        if (theRes.rowCount !== undefined) {
            msg = `Successfully migrated ${athleteRows.length} athlete dimension to BigQuery`;
            console.log(msg);
            if (segmentRows.length) {
                theRes = await bigqueryClient
                    .dataset(datasetId)
                    .table(segmentTableId)
                    .insert(segmentRows)
                    .then((rowCount) => {
                        return {
                            rowCount,
                            errors: undefined
                        }
                    })
                    .catch((e) => {
                        console.log(e.errors);
                        return {
                            rowCount: undefined,
                            errors: e.errors
                        }
                    });
        
        
        const counts = await getAthleteMetadataCountsInBigQuery();
        if (!orgDoc.currentEvolution) {
            orgDoc.currentEvolution = {};
        }
        orgDoc.currentEvolution.numberOfParticipants = counts.numberOfParticipants;
        orgDoc.currentEvolution.numberOfCountries = counts.numberOfCountries;

        await orgRef.update(orgDoc);

                if (theRes.rowCount !== undefined) {
                    msg = `Successfully migrated ${segmentRows.length} to segment dimension in BigQuery`;
                    console.log(msg);

                    theRes = await bigqueryClient
                        .dataset(datasetId)
                        .table(segmentAthleteTableId)
                        .insert(segmentAthleteRows)
                        .then((rowCount) => {
                            return {
                                rowCount,
                                errors: undefined
                            }
                        })
                        .catch((e) => {
                            console.log(e.errors);
                            return {
                                rowCount: undefined,
                                errors: e.errors
                            }
                        });

                    if (theRes.rowCount !== undefined) {
                        msg = `Successfully migrated ${segmentAthleteRows.length} segmentAthlete dimensions to BigQuery`;
                        console.log(msg);
                    } else {
                        msg = `${theRes.errors.length} error(s) occurred when segmentAthlete dimensions to BigQuery`;
                        console.log(msg);
                        theRes.errors.forEach((e: any) => {
                            msg = `Error: ${JSON.stringify(e)}`;
                            console.log(msg);
                        });
                    }
                } else {
                    msg = `${theRes.errors.length} error(s) occurred when migrating to segment dimension in BigQuery`;
                    console.log(msg);
                    theRes.errors.forEach((e: any) => {
                        msg = `Error: ${JSON.stringify(e)}`;
                        console.log(msg);
                    });
                }
            } else {
                if (segmentAthleteRows.length) {
                    exists = await getSegmentAthleteExistsInBigQuery(orgId, athleteSnapshot.id);

                    if (!exists) {
                        theRes = await bigqueryClient
                            .dataset(datasetId)
                            .table(segmentAthleteTableId)
                            .insert(segmentAthleteRows)
                            .then((rowCount) => {
                                return {
                                    rowCount,
                                    errors: undefined
                                }
                            })
                            .catch((e) => {
                                console.log(e.errors);
                                return {
                                    rowCount: undefined,
                                    errors: e.errors
                                }
                            });

                        if (theRes.rowCount !== undefined) {
                            msg = `Successfully migrated ${segmentAthleteRows.length} segmentAthlete dimensions to BigQuery`;
                            console.log(msg);
                        } else {
                            msg = `${theRes.errors.length} error(s) occurred when segmentAthlete dimensions to BigQuery`;
                            console.log(msg);
                            theRes.errors.forEach((e: any) => {
                                msg = `Error: ${JSON.stringify(e)}`;
                                console.log(msg);
                            });
                        }
                    }
                }
            }
        } else {
            msg = `${theRes.errors.length} error(s) occurred when migrating to athlete dimension in BigQuery`;
            console.log(msg);
            theRes.errors.forEach((e: any) => {
                msg = `Error: ${JSON.stringify(e)}`;
                console.log(msg);
            });
        }
    });