import { OrganizationInterface } from './../../models/organization.model';
import { db } from '../../shared/init/initialise-firebase';
import { firestore } from 'firebase-admin';

import { FirestoreCollection } from '../biometricEntries/enums/firestore-collections';
import { getDocumentFromSnapshot } from '../../analytics/triggers/utils/get-document-from-snapshot';
// import { Athlete } from '../../models/athlete/interfaces/athlete';
import { Group } from '../../models/group/group.model';
import { Organization } from '../../models/organization.model';
import { WellAggregateCollection } from '../biometricEntries/enums/firestore-aggregate-collection';
import { Athlete } from '../../models/athlete/athlete.model';

export const getCollectionRef = (collection: FirestoreCollection | WellAggregateCollection | string) => {
    return db.collection(collection);
};

export const getCollectionDocs = (collection: FirestoreCollection | WellAggregateCollection | string) => {
    return db.collection(collection).get();
};

export const getDocFromRef = (documentRef: firestore.DocumentReference) => {
    return documentRef.get().then((docSnap) => getDocumentFromSnapshot(docSnap));
}

export const getOrgCollectionRef = () => {
    return db.collection(FirestoreCollection.Organizations);
};

export const getGroupCollectionRef = () => {
    return db.collection(FirestoreCollection.Groups);
};

export const getAthleteCollectionRef = () => {
    return db.collection(FirestoreCollection.Athletes);
};

export const getTaskQueueCollectionRef = () => {
    return db.collection(FirestoreCollection.TaskQueue);
};

export const getOrgDocRef = (uid: string) => {
    return getOrgCollectionRef().doc(uid);
};

export const getGroupDocRef = (uid: string) => {
    return getGroupCollectionRef().doc(uid);
};

export const getAthleteDocRef = (uid: string) => {
    return getAthleteCollectionRef().doc(uid);
};

export const getTaskQueueDocRef = (uid: string) => {
    return getTaskQueueCollectionRef().doc(uid);
};

export const getOrgDocFromSnapshot = (orgUID: string) => {
    return getOrgDocRef(orgUID).get().then(docSnap => getDocumentFromSnapshot(docSnap) as Organization);
}

export const getGroupDocFromSnapshot = (groupUID: string) => {
    return getGroupDocRef(groupUID).get().then(docSnap => getDocumentFromSnapshot(docSnap) as Group);
}

export const getAthleteDocFromSnapshot = (athUID: string) => {
    return getAthleteDocRef(athUID).get().then(docSnap => getDocumentFromSnapshot(docSnap) as Athlete);
}

export const getAthleteDocFromUID = (athleteId: string) => {
    return getAthleteDocRef(athleteId).get()
        .then(docSnap => {
            return (getDocumentFromSnapshot(docSnap)) as Athlete
        }).catch((e) => {
            throw e
        });
}

export const getGroupDocFromUID = (groupId: string) => {
    return getGroupDocRef(groupId).get()
        .then(docSnap => {
            return (getDocumentFromSnapshot(docSnap)) as Group
        }).catch((e) => {
            throw e
        });
}

export const getOrganizationDocFromUID = (orgId: string) => {
    return getOrgDocRef(orgId).get()
        .then(docSnap => {
            return (getDocumentFromSnapshot(docSnap)) as OrganizationInterface;
        }).catch((e) => {
            throw e;
        });
}


export const getNotificationCardMetadataRef = () => {
    return db.collection(FirestoreCollection.NotificationCardMetadata);
};

export const getWhiteLabelCollectionRef = () => {
    return db.collection(FirestoreCollection.WhiteLabelThemes);
};

export const getWhiteLabelDocRef = (uid: string) => {
    return getWhiteLabelCollectionRef().doc(uid);
};

export const getNotificationCardRef = () => {
    return db.collection(FirestoreCollection.NotificationCardRegistry);
};