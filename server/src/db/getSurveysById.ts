import * as functions from 'firebase-functions'
import moment = require('moment')
import { isNumber } from 'lodash'

import { db } from '../shared/init/initialise-firebase'

import { CompletedSurvey } from './programs/models/interfaces/completed-survey.interface'
import { FirestoreCollection } from './biometricEntries/enums/firestore-collections'
import { Config } from './../shared/init/config/config';


export const getSurveysById = functions.https._onCallWithOptions((data, context) => {
    try {
        if (!context.auth.token.uid) {
            console.error(
                'Request not authorized. User must logged in to fulfill this request'
            )
            return {
                error:
                    'Request not authorized. User must logged in to fulfill this request ' +
                    data.email,
            }
        }
        const { athleteId } = data

        console.info('Getting Completed Surveys By Id', athleteId)
        if (athleteId) {
            return db
                .collection(FirestoreCollection.Athletes)
                .doc(athleteId)
                .collection(FirestoreCollection.AthleteCompletedSurveys)
                .get()
                .then(querySnap => {
                    if (querySnap.empty) {
                        console.info(
                            'NO documents found for user with id ',
                            athleteId
                        )
                        return []
                    } else {
                        console.info(
                            'Documents FOUND for user with id ',
                            athleteId,
                            querySnap.size
                        )
                        const surveys = []
                        querySnap.docs.map(
                            (snap: FirebaseFirestore.QueryDocumentSnapshot) => {
                                if (!snap.exists) {
                                    console.log(
                                        'snap.exists ==================== FALSE'
                                    )
                                }
                                console.log('mapping', snap.id)

                                const completedSurvey = {
                                    uid: snap.id,
                                    ...(snap.data() as CompletedSurvey),
                                }
                                console.log('completedSurvey', completedSurvey)

                                const {
                                    uid,
                                    athleteUid: userId,
                                    responses,
                                    // docGuid,
                                    score,
                                    completionDate,
                                } = completedSurvey
                                console.log('csvData!!!')

                                responses.map(surveyResponse => {
                                    const { index, value } = surveyResponse
                                    const momentDate = moment(
                                        completionDate.toDate(),
                                        'YYYY-MM-DD HH:MM:SS'
                                    )

                                    const csvDataFormat = {
                                        uid,
                                        userId,
                                        // docGuid,
                                        fullName: '',
                                        groupName: 'N/A',
                                        question: index,
                                        answer: value
                                            ? isNumber(value)
                                                ? +value
                                                : value
                                            : 'N/A',
                                        score,
                                        completionDate: momentDate.toISOString(),
                                        completionDateFormatted: momentDate.toString(),
                                    }

                                    console.log('csvData', csvDataFormat)
                                    surveys.push(csvDataFormat)
                                })
                            }
                        )

                        console.log('surveys', surveys)
                        return surveys
                    }
                })
                .catch(error => {
                    console.error(
                        'error fetching Biometric Entries for user with id ' +
                            athleteId +
                            '........!!! => error: ',
                        error
                    )
                    throw error
                })
        } else {
            console.error('.........ERROR: athleteId is not defined')
            return Promise.resolve([])
        }
    } catch (error) {
        console.error('.........CATCH ERROR: athleteId is not defined', error)
        return Promise.reject(error)
    }
}, {regions: Config.regions})
