


export enum IpScoreAggregateHistoryCollection {

    AthleteIpScoreWeeklyHistory = 'athleteIpScoreWeeklyHistoryAggregates',
    AthleteIpScoreMonthlyHistory = 'athleteIpScoreMonthlyHistoryAggregates',
    GroupIpScoreWeeklyHistory = 'groupIpScoreWeeklyHistoryAggregates',
    GroupIpScoreMonthlyHistory = 'groupIpScoreMonthlyHistoryAggregates',
}