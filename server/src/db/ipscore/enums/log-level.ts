// In order of precedence
// In order of precedence
export enum LogLevel {
    Error = 'error',
    Warn = 'warn',
    Info = 'info',
    Verbose = 'verbose',
    Debug = 'debug',
    Silly = 'silly', // 5,
    Sockets = "Sockets"
}
