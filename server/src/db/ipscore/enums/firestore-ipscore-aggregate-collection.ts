export enum IpScoreAggregateCollection {
    AthleteIpScoreWeekly = 'athleteIpScoreWeeklyAggregates',
    // AthleteIpScoreMonthly = 'athleteIpScoreMonthlyAggregates',
    GroupIpScoreWeekly = 'groupIpScoreWeeklyAggregates',
    // GroupIpScoreMonthly = 'groupIpScoreMonthlyAggregates'
}