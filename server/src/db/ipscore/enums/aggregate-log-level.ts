// Events specific to sockets

export enum AggregateLogLevel {
    ComposedDoc = "ComposedDoc",
    RetrievedDoc = "RetrievedDoc",
    SavedDoc = "SavedDoc",
    Info = "Info",
    ValueUpdate = "ValueUpdate"
}