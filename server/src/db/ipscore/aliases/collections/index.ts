import { IpScoreAggregateCollection } from "../../enums/firestore-ipscore-aggregate-collection";
import { IpScoreAggregateHistoryCollection } from "../../enums/firestore-ipscore-aggregate-history-collection";
import { WellAggregateCollection } from "../../../biometricEntries/enums/firestore-aggregate-collection";
import { WellAggregateHistoryCollection } from "../../../biometricEntries/enums/firestore-aggregate-history-collection";
import { WellAggregateBinaryHistoryCollection } from "../../../biometricEntries/enums/firestore-aggregate-binary-history-collection";
import { AllAggregateCollectionType } from "../../../../models/types/biometric-aggregate-types";

export const isIPScoreWeeklyCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection | WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    return (
        collection === IpScoreAggregateCollection.AthleteIpScoreWeekly ||
        collection === IpScoreAggregateCollection.GroupIpScoreWeekly
    )
};

export const isAthleteCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    return (
        collection === IpScoreAggregateCollection.AthleteIpScoreWeekly ||
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory
    )
};

export const isAthleteHistoryCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    const result =  collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory ||
    collection === IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory;

    return result
};

export const isWeeklyHistoryCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    return (
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory
    )
};

export const isGroupWeeklyCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    return (
        collection === IpScoreAggregateCollection.GroupIpScoreWeekly ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory
    )
};

export const isGroupCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection | WellAggregateCollection | AllAggregateCollectionType
) => {
    return (
        collection === IpScoreAggregateCollection.GroupIpScoreWeekly ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreMonthlyHistory
    )
};

export const isGroupWeeklyHistoryCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    return collection === IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory
};

export const isGroupMonthlyHistoryCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    return collection === IpScoreAggregateHistoryCollection.GroupIpScoreMonthlyHistory
};

export const isHistoryCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    const result =
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreMonthlyHistory;
    return result
};

export const isIpScoreWeeklyCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    const result =
        collection === IpScoreAggregateCollection.AthleteIpScoreWeekly ||
        collection === IpScoreAggregateCollection.GroupIpScoreWeekly ||
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory;
    return result
};
