'use strict'

import * as functions from 'firebase-functions'

import moment from 'moment'
import { firestore } from 'firebase-admin'

import { LogInfo } from '../../shared/logger/logger'
import { initialiseLoggers } from '../../shared/logger/service/logger-service'
import { Athlete } from '../../models/athlete/interfaces/athlete'
import { FirestoreCollection } from '../biometricEntries/enums/firestore-collections'
import { updateIPScoreAggregates } from './update-ip-score-aggregates'

import { updateAthleteIndicatorsIPScoreOnGroups } from '../../analytics/triggers/update-athlete-indicators-ipscore'
import { Config } from './../../shared/init/config/config'

exports.updateIpScore = functions.firestore
    ._documentWithOptions(`${FirestoreCollection.Athletes}/{userId}`, {
        regions: Config.regions,
    })
    .onUpdate(async (change, context) => {
        console.log(
            'updateIpScore for Athlete!!!!!!!! - onUpdate().process.listeners.length',
            process.listeners.length
        )
        try {
            initialiseLoggers()

            const oldValueSnapshot = change.before
            const newValueSnapshot = change.after

            const athlete: Athlete = {
                uid: newValueSnapshot.id,
                ...(newValueSnapshot.data() as Athlete),
            }

            const oldAthlete: Athlete = {
                uid: oldValueSnapshot.id,
                ...(oldValueSnapshot.data() as Athlete),
            }

            const themesToCheck = [
                // -- ALLSTAR GYM
                { themeUid: 'allstargym', orgUid: '1WeXDuIn2iPlRm59q5T9' },
                // -- ELEMENTS
                { themeUid: 'elementsyoga', orgUid: 'HMtPBDsQyLLhv9g1Uclt' },
                // -- KFIT
                { themeUid: 'kfit', orgUid: 'FC2p4MVCkTT2EtHhE0n8' },
                // -- TribePlay / Netball Q
                { themeUid: 'netballq', orgUid: 'bNei5o6gOiXQ7xQo4gma' },
                // -- Science of Fitness
                { themeUid: 'sof', orgUid: 'bgRE62Gjr8vWfNDizBye' },
                // -- TENNIS AUS
                { themeUid: 'tennisaus', orgUid: 'CmbsHPU9n99ylyfeRiz3' },
                // -- BGS
                { themeUid: 'bgs', orgUid: '1FhhuwyqNPnitlsBIVuN' },
                // -- GIRLWARRIOR
                { themeUid: 'girlwarrior', orgUid: '6GlqBwa6SlyQHapuX2nB' },
                // -- USA GYM
                { themeUid: 'usagymnastics', orgUid: '2ddziVMY5UVZZuLW2znk' },
                // -- TOUCH AUS
                { themeUid: 'touchaus', orgUid: 'MNekZyz3svjvRg1hGnN7' },
            ]

            try {
                if (
                    athlete.organizations &&
                    athlete.organizations.length &&
                    themesToCheck.find(t => t.orgUid === athlete.organizationId)
                ) {
                    const theOrgIndex = athlete.organizations.findIndex(
                        org => org.organizationId === athlete.organizationId
                    )

                    if (theOrgIndex >= 0) {
                        const theOrg = athlete.organizations[theOrgIndex]
                        const theThemeIdOnOrg = theOrg.themeId

                        if (!!theThemeIdOnOrg) {
                            const theThemeId = themesToCheck.find(
                                t => t.orgUid === athlete.organizationId
                            ).themeUid
                            if (theThemeIdOnOrg !== theThemeId) {
                                console.log('updating themeId', {
                                    athleteUid: athlete.uid,
                                    oldThemeId: theThemeIdOnOrg,
                                    replacementThemeId: theThemeId,
                                })
                                athlete.organizations[
                                    theOrgIndex
                                ].themeId = theThemeId

                                return newValueSnapshot.ref.update({
                                    organizations: athlete.organizations,
                                    themeId: theThemeId,
                                })
                            }
                        }
                    }
                }
            } catch (error) {
                console.error('Unable to update THEME', {
                    athleteUid: athlete.uid,
                    error,
                })
            }

            // if ((!oldAthlete.organizations && !!athlete.organizations && !!athlete.organizations.length && athlete.themes && athlete.themes.length)) {
            //     const athOrgIndex = athlete.organizations.findIndex((o) => o.organizationId === athlete.organizationId)

            //     if (athOrgIndex >= 0) {
            //         athlete.organizations[athOrgIndex].themeId = (athlete.themes || [{ uid: 'default' }])[0].uid
            //     }
            //     return await newValueSnapshot.ref.update({
            //         themeId: (athlete.themes || [{ uid: 'default' }])[0].uid,
            //         organizations: athlete.organizations
            //     })
            // }
            // else if (oldAthlete.organizations && oldAthlete.organizations.length && athlete.organizations[0].themeId && athlete.themes && athlete.themes.length) {

            //     const orgIndex = athlete.organizations.findIndex((o) => o.organizationId === athlete.organizationId)

            //     if (orgIndex >= 0) {
            //         const themeUID = athlete.themes[0].uid

            //         if (athlete.organizations[orgIndex].themeId !== themeUID) {
            //             athlete.organizations[orgIndex].themeId = themeUID

            //             return await newValueSnapshot.ref.update({
            //                 themeId: themeUID,
            //                 organizations: athlete.organizations
            //             })
            //         }
            //     } else {

            //     }
            //     // athlete.organizations[0].themeId = (athlete.themes || [{ uid: 'default' }])[0].uid
            // }
            // else if (oldAthlete.organizations && oldAthlete.organizations.length && !athlete.organizations[0].themeId && athlete.themes && athlete.themes.length) {

            //     const orgIndex = athlete.organizations.findIndex((o) => o.organizationId === athlete.organizationId)

            //     if (orgIndex >= 0) {
            //         const themeUID = athlete.themes[0].uid

            //         if (athlete.organizations[orgIndex].themeId !== themeUID) {
            //             athlete.organizations[orgIndex].themeId = themeUID

            //             return await newValueSnapshot.ref.update({
            //                 themeId: themeUID,
            //                 organizations: athlete.organizations
            //             })
            //         }
            //     } else {

            //     }
            //     // athlete.organizations[0].themeId = (athlete.themes || [{ uid: 'default' }])[0].uid
            // }

            // else if (oldAthlete.organizations && (oldAthlete.organizations.find((o) => o.organizationId === athlete.organizationId) && !oldAthlete.organizations.find((o) => o.organizationId === athlete.organizationId).themeId)) {
            //     // oldAthlete.organizations.find((o) => o.organizationId === athlete.organizationId).themeId = athlete.themes.uid
            // }
            console.warn(
                'oldAthlete.currentIpScoreTracking',
                oldAthlete.currentIpScoreTracking
            )
            console.warn(
                'athlete.currentIpScoreTracking',
                athlete.currentIpScoreTracking
            )
            console.warn(
                'athlete.currentIpScoreTracking.directive.updateAthleteIndicatorsOnGroups',
                athlete.currentIpScoreTracking.directive
                    .updateAthleteIndicatorsOnGroups
            )

            if (
                athlete.currentIpScoreTracking.ipScore !==
                    oldAthlete.currentIpScoreTracking.ipScore &&
                athlete.currentIpScoreTracking.directive
                    .updateAthleteIndicatorsOnGroups
            ) {
                console.warn('UPDATE ATHLETE INDICATOR IPSCORE ON GROUP')
                const groupUIDs = (athlete.groups || []).map(grp => grp.groupId)

                if (groupUIDs.length) {
                    await updateAthleteIndicatorsIPScoreOnGroups(
                        athlete.uid,
                        groupUIDs,
                        athlete.runningTotalIpScore,
                        athlete.currentIpScoreTracking.ipScore,
                        0
                    )
                } else {
                    console.warn('User is not part of any groups', {
                        uid: athlete.uid,
                        name: athlete.profile.fullName,
                    })
                }
            } else {
                console.warn(
                    'NO NEED TO UPDATE ATHLETE INDICATOR IPSCORE ON GROUP'
                )
            }

            let { dateTime } = athlete.currentIpScoreTracking

            // let theDateTime: firestore.Timestamp = dateTime

            if (typeof dateTime === 'string') {
                dateTime = firestore.Timestamp.fromDate(
                    moment(dateTime).toDate()
                )
                console.error(
                    '--------======>>>>> HAVING TO RESET dateTime STRING TO DATE!!!'
                )
                return newValueSnapshot.ref.set(
                    {
                        dateTime,
                    },
                    { merge: true }
                )
            } else {
                console.warn('--------======>>>>>  dateTime IS A DATE!!!')
            }

            // const oldAthlete: Athlete = {
            //     uid: oldValueSnapshot.id,
            //     ...oldValueSnapshot.data() as Athlete
            // };

            // console.warn('OLD VALUES', oldAthlete)
            // console.log('NEW VALUES', athlete)

            console.log('================>>>>>>>>>>>>   context   ', context)

            const { currentIpScoreTracking } = athlete

            const directiveMissing = !(
                currentIpScoreTracking || currentIpScoreTracking.directive
            )
            const directiveSetToExcecute =
                !directiveMissing && currentIpScoreTracking.directive.execute

            const execute = directiveMissing || directiveSetToExcecute

            console.log(
                '================>>>>>>>>>>>>   directive   ',
                athlete.currentIpScoreTracking.directive
            )

            if (execute) {
                console.log(
                    '----->>>>>>>> EXECUTING  athlete.currentIpScoreTracking',
                    currentIpScoreTracking
                )

                if (!currentIpScoreTracking.directive.utcOffset) {
                    currentIpScoreTracking.directive.utcOffset = 0
                }

                // return db.runTransaction(transaction => {
                //     return transaction.get(restRef).then(restDoc => {
                //         // Compute new number of ratings
                //         const { newAvgRating, newNumRatings } = handleAggregate(restDoc, ratingVal);

                //         // Update restaurant info
                //         return transaction.update(restRef, {
                //             avgRating: newAvgRating,
                //             numRatings: newNumRatings,
                //         })
                //     })
                // })
                if (!(athlete.groups && athlete.groups.length)) {
                    athlete.groups = []
                }

                // TODO:
                const utcOffset = 0

                debugger
                // TODO: CHEKC TRANSACTION
                const t: FirebaseFirestore.Transaction = undefined

                const result = updateIPScoreAggregates(
                    t,
                    athlete,
                    true,
                    utcOffset
                )
                    // const result = updateAggregates(athlete, currentIpScoreTracking.directive.utcOffset)
                    .then(async docs => {
                        console.log(
                            '----->>>>> PROMISE RESOLVED, Aggregation Complete'
                        )
                        // console.log(docs[1])
                        // console.log(docs)

                        return docs
                    })
                    .catch(err => {
                        console.log(err)

                        return err
                    })
                LogInfo('Aggregate Update handled by onUpdate()')
                return result
                return Promise.resolve(
                    'updateBiometricEntries.onCreate().doRead(biometricEntry) executed WOOT Promise!'
                )
            } else {
                console.log(
                    'EXECUTE === FALSE, updateIpScore.onUpdate().updateAggregates() execution skipped!'
                )

                // if (forceUpdate) {
                //     await getAthleteDocRef(athlete.uid).update(athlete)
                // }

                return Promise.resolve(
                    'updateIpScore.onUpdate().updateAggregates() execution skipped!'
                )
            }

            // const ratingVal = change.after.data().rating

            // // Get a reference to the restaurant
            // const restRef = db.collection('restaurants').doc(context.params.restId)

            // // Update aggregations in a transaction
            // return db.runTransaction(transaction => {
            //     return transaction.get(restRef).then(restDoc => {
            //         // Compute new number of ratings
            //         const { newAvgRating, newNumRatings } = handleAggregate(restDoc, ratingVal);

            //         // Update restaurant info
            //         return transaction.update(restRef, {
            //             avgRating: newAvgRating,
            //             numRatings: newNumRatings,
            //         })
            //     })
            // })
        } catch (err) {
            const test = { ...err }

            console.error(
                `log - updateIpScore.onCreate() execution failed! - TRY/CATCH err -> ${test}`
            )
            return Promise.reject(
                `promise - updateIpScore.onCreate() execution failed! - TRY/CATCH err -> ${JSON.stringify(
                    { ...test }
                )}`
            )
        }
    })

// exports.aggregateIpScore = functions.firestore
//     // .document('restaurants/{restId}/ratings/{ratingId}')
//     .document(`${FirestoreCollection.Athletes}/{userId}`)
//     .onUpdate(() => {
//         // Get value of the newly added rating
//     })
