import { AggregateIpScoreDirective } from "../../services/interfaces/aggregate-ipscore-directive";
import { AthleteIpScoreWeeklyAggregateInterface, GroupIpScoreWeeklyAggregateInterface } from "../../models/interfaces";
import { AthleteIpScoreWeeklyHistoryAggregateInterface } from "../../models/interfaces/athlete-ipscore-weekly-history-aggregate";
import { AthleteIpScoreMonthlyHistoryAggregateInterface } from "../../models/interfaces/athlete-ipscore-monthly-history-aggregate";
import { GroupIpScoreWeeklyHistoryAggregateInterface } from "../../models/interfaces/group-ipscore-weekly-history-aggregate";
import { GroupIpScoreMonthlyHistoryAggregateInterface } from "../../models/interfaces/group-ipscore-monthly-history-aggregate";

export interface CalculateIpScoreAggregates {

    // Athlete Weekly
    processAthleteIpScoreWeekly(directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyAggregateInterface;

    processAthleteIpScoreWeeklyHistory(directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyHistoryAggregateInterface;

    processAthleteIpScoreMonthlyHistory(directive: AggregateIpScoreDirective
    ): AthleteIpScoreMonthlyHistoryAggregateInterface;


    // Group Weekly
    processGroupIpScoreWeekly(directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyAggregateInterface;

    processGroupIpScoreWeeklyHistory(directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyHistoryAggregateInterface;

    processGroupIpScoreMonthlyHistory(directive: AggregateIpScoreDirective
    ): GroupIpScoreMonthlyHistoryAggregateInterface;
}
