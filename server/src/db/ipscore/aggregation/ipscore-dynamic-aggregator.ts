import { IpScoreAggregatorStore } from './aggregators'
import { Log, LogError } from '../../../shared/logger/logger'
import { AggregateLogLevel } from '../enums/aggregate-log-level'
import { LogLevel } from '../enums/log-level'

import { AggregateIpScoreDirective } from '../services/interfaces/aggregate-ipscore-directive'
import {
    AthleteIpScoreWeeklyAggregateInterface,
    GroupIpScoreWeeklyAggregateInterface,
} from '../models/interfaces'
import { AthleteIpScoreWeeklyHistoryAggregateInterface } from '../models/interfaces/athlete-ipscore-weekly-history-aggregate'
import { AthleteIpScoreMonthlyHistoryAggregateInterface } from '../models/interfaces/athlete-ipscore-monthly-history-aggregate'
import { GroupIpScoreWeeklyHistoryAggregateInterface } from '../models/interfaces/group-ipscore-weekly-history-aggregate'
import { GroupIpScoreMonthlyHistoryAggregateInterface } from '../models/interfaces/group-ipscore-monthly-history-aggregate'
import { CalculateIpScoreAggregates } from './interfaces/calculate-ipscore-aggregates'

export class IpScoreDynamicAggregator implements CalculateIpScoreAggregates {
    processAthleteIpScoreWeekly(
        directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyAggregateInterface {
        throw new Error('Method not implemented.')
    }

    processAthleteIpScoreWeeklyHistory(
        directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyHistoryAggregateInterface {
        throw new Error('Method not implemented.')
    }

    processAthleteIpScoreMonthlyHistory(
        directive: AggregateIpScoreDirective
    ): AthleteIpScoreMonthlyHistoryAggregateInterface {
        throw new Error('Method not implemented.')
    }

    processGroupIpScoreWeekly(
        directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyAggregateInterface {
        throw new Error('Method not implemented.')
    }

    processGroupIpScoreWeeklyHistory(
        directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyHistoryAggregateInterface {
        throw new Error('Method not implemented.')
    }

    processGroupIpScoreMonthlyHistory(
        directive: AggregateIpScoreDirective
    ): GroupIpScoreMonthlyHistoryAggregateInterface {
        throw new Error('Method not implemented.')
    }

    constructor() {
        const className = 'IpScore';
        debugger;
        if (
            IpScoreAggregatorStore[className] === undefined ||
            IpScoreAggregatorStore[className] === null
        ) {
            Log({
                type: AggregateLogLevel.Info,
                message: `Aggregator could not be found in the IpScoreAggregator Store `,
                level: LogLevel.Sockets,
                data: {
                    rest: className
                }
            });

            return undefined
        }

        return new IpScoreAggregatorStore[className]()
    }

    calculateAggregates(
    ): AggregateIpScoreDirective {
        LogError('Implemented on Aggregator.');
        throw new Error('Implemented on Aggregator.')
    }
}
