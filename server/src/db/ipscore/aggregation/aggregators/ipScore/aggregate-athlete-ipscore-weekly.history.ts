import { firestore } from 'firebase-admin'

import {
    updateIpScoreConstituent,
    calculateIpScoreFromDailies,
    recordHistoryIpScoreComponentPercentagesFromDailies,
} from './utils/index'
import { calculateIpScoreFromAggregateComponentInterface } from './utils/calculateIpScoreFromAggregateComponentInterface'

import { LogInfo } from '../../../../../shared/logger/logger'
import { getWeekNumber, getDayIndex } from '../../../../../shared/utils/moment'
import { Athlete } from '../../../../../models/athlete/interfaces/athlete'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { AthleteIpScoreWeeklyHistoryAggregateInterface } from '../../../models/interfaces/athlete-ipscore-weekly-history-aggregate'
import { getStartIndex } from './utils/getStartIndex'
import { validateTrackingCacheDays } from './validateTrackingCacheDays'
import { validateWeeklyTrackingCache } from './validateWeeklyTrackingCache'
import { validateComponentValuesLength } from './validateComponentValuesLength'
import { validateValuesLength } from './validateValuesLength'
import { has } from 'lodash'

export const aggregateAthleteIpScoreWeeklyHistory = (
    directive: AggregateIpScoreDirective
): AthleteIpScoreWeeklyHistoryAggregateInterface => {
    debugger
    const athlete: Athlete = directive.athlete
    const aggregateDoc: AthleteIpScoreWeeklyHistoryAggregateInterface = directive.aggregateDocument as AthleteIpScoreWeeklyHistoryAggregateInterface

    const dayIndex = getDayIndex(
        firestore.Timestamp.now(),
        directive.utcOffset || 0
    )
    const weekNumber = getWeekNumber(
        athlete.metadata.creationTimestamp,
        athlete.currentIpScoreTracking.dateTime,
        directive.utcOffset
    )

    const config = athlete.ipScoreConfiguration
    const currentTracking = athlete.currentIpScoreTracking

    const paths = currentTracking.directive.paths || []

    // console.log('=============>>>>>>>>>>>> Processing Athlete IpScore Weekly History Aggregate', aggregateDoc)

    LogInfo(
        `Processing AthleteIpScoreWeeklyHistory aggregate document in IpScore aggregator.`,
        {
            AggregateDocument: aggregateDoc,
            Athlete: athlete,
            WeekNumber: weekNumber,
            DayIndex: dayIndex,
            ExecuteDirective: athlete.currentIpScoreTracking.directive,
        }
    )

    validateWeeklyTrackingCache(aggregateDoc, weekNumber)

    // Array of current week IP Score aggregate component data structures
    // On daily basis
    validateTrackingCacheDays(aggregateDoc, dayIndex)

    // Array of week IP Score (numbers)
    validateValuesLength(aggregateDoc, weekNumber)

    // Array of historical week IP Score aggregate component data structures
    // Representing week percentages across all lifestyle entry types
    validateComponentValuesLength(aggregateDoc, weekNumber)

    aggregateDoc.trackingCache.dayIndex = dayIndex

    const dailyAggregate = aggregateDoc.trackingCache.days[dayIndex]

    const includeWearables =
        config.quantitativeWellness.body.wearables.include || false

    paths.forEach((path: string) => {
        let realized = 0
        let expected = 0

        const parts = path.split('.')
        switch (parts[1].toLowerCase()) {
            case 'brain':
                switch (parts[2].toLowerCase()) {
                    case 'mood':
                        realized =
                            currentTracking.quantitativeWellness.brain.mood
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.brain.mood
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.brain.mood
                        )
                        break
                    case 'sleep':
                        // console.warn('currentTracking.quantitativeWellness.brain.sleep', currentTracking.quantitativeWellness.brain.sleep)
                        // console.warn('dailyAggregate.brain.sleep', dailyAggregate.brain.sleep)
                        realized =
                            currentTracking.quantitativeWellness.brain.sleep
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.brain.sleep
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.brain.sleep
                        )
                        break
                }
                break
            case 'body':
                switch (parts[2].toLowerCase()) {
                    case 'fatigue':
                        realized =
                            currentTracking.quantitativeWellness.body.fatigue
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.body.fatigue
                                .expectedDailyEntries
                        dailyAggregate.body.fatigue.realized++
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.body.fatigue
                        )
                        break
                    case 'pain':
                        realized =
                            currentTracking.quantitativeWellness.body.pain
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.body.pain
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.body.pain
                        )
                        break
                    case 'weight':
                        realized =
                            currentTracking.quantitativeWellness.body.weight
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.body.weight
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.body.weight
                        )
                        break
                    case 'wearables':
                        if (includeWearables) {
                            realized =
                                currentTracking.quantitativeWellness.body
                                    .wearables.recordedDailyEntries
                            expected =
                                config.quantitativeWellness.body.wearables
                                    .expectedDailyEntries
                            updateIpScoreConstituent(
                                realized,
                                expected,
                                dailyAggregate.body.wearables
                            )
                        }
                        break
                }
                break
            case 'simple-food':
            case 'food':
                realized =
                    currentTracking.quantitativeWellness.food.components
                        .recordedDailyEntries
                expected =
                    config.quantitativeWellness.food.components
                        .expectedDailyEntries
                dailyAggregate.food.components.realized++
                updateIpScoreConstituent(
                    realized,
                    expected,
                    dailyAggregate.food.components
                )
                break
            case 'training':
                realized =
                    currentTracking.quantitativeWellness.training.components
                        .recordedDailyEntries
                expected =
                    config.quantitativeWellness.training.components
                        .expectedDailyEntries
                updateIpScoreConstituent(
                    realized,
                    expected,
                    dailyAggregate.training.components
                )
                break
            case 'programs':
                // dailyAggregate.programs.components.realized++;
                switch (parts[2].toLowerCase()) {
                    case 'video':
                        dailyAggregate.programs.videoConsumed =
                            currentTracking.programs.videoConsumed
                        break
                    case 'survey':
                        dailyAggregate.programs.surveyConsumed =
                            currentTracking.programs.surveyConsumed
                        break
                    case 'worksheet':
                        dailyAggregate.programs.worksheetConsumed =
                            currentTracking.programs.worksheetConsumed
                        break
                }
                if (has(currentTracking.programs, 'expectedDailyEntries')) {
                    realized = currentTracking.programs.recordedDailyEntries
                    expected = currentTracking.programs.expectedDailyEntries
                    updateIpScoreConstituent(
                        realized,
                        expected,
                        dailyAggregate.programs
                    )
                }
                break
        }
    })

    const { isPenalized, maxScorePossible } = currentTracking.directive

    dailyAggregate.value = calculateIpScoreFromAggregateComponentInterface(
        dailyAggregate,
        includeWearables,
        isPenalized,
        maxScorePossible
    )

    const startIndex = getStartIndex(
        dayIndex,
        athlete.metadata.creationTimestamp,
        directive.utcOffset,
        false
    )

    aggregateDoc.values[0] = calculateIpScoreFromDailies(
        aggregateDoc.trackingCache.days,
        dayIndex,
        startIndex
    )
    recordHistoryIpScoreComponentPercentagesFromDailies(
        aggregateDoc.componentValues[0],
        aggregateDoc.trackingCache.days,
        dayIndex,
        startIndex,
        includeWearables
    )

    aggregateDoc.lastAggregateDate = firestore.Timestamp.now()

    // console.warn('=============>>>>>>>>>>>> Updted Athlete IpScore Weekly History Aggregate', aggregateDoc)

    return aggregateDoc
}
