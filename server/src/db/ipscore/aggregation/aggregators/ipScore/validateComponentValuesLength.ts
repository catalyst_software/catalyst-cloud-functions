import { cloneDeep, isArray } from "lodash";

import { AthleteIpScoreWeeklyHistoryAggregateInterface } from "../../../models/interfaces/athlete-ipscore-weekly-history-aggregate";
import { createAggregateComponentInterface } from "./utils/createAggregateComponentInterface";
import { GroupIpScoreMonthlyHistoryAggregateInterface } from "../../../models/interfaces/group-ipscore-monthly-history-aggregate";

export const validateComponentValuesLength = (
    aggregateDoc: AthleteIpScoreWeeklyHistoryAggregateInterface | GroupIpScoreMonthlyHistoryAggregateInterface,
    index: number) => {

    if (!isArray(aggregateDoc.componentValues)) {
        aggregateDoc.componentValues = [];
    }
    if (aggregateDoc.componentValues.length <= index) {
        const existingComponentValues = cloneDeep(aggregateDoc.componentValues);
        for (let i = aggregateDoc.componentValues.length; i <= index; i++) {
            existingComponentValues.unshift(createAggregateComponentInterface());
        }
        aggregateDoc.componentValues = existingComponentValues;
    }
};
