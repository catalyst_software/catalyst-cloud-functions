import { cloneDeep, isArray } from 'lodash';

import { createAggregateComponentInterface } from "./utils/createAggregateComponentInterface";
import { GroupIpScoreWeeklyHistoryAggregateInterface } from '../../../models/interfaces/group-ipscore-weekly-history-aggregate';

export const validateComponentValues = (aggregateDoc: GroupIpScoreWeeklyHistoryAggregateInterface, index: number) => {
    if (!isArray(aggregateDoc.componentValues)) {
        aggregateDoc.componentValues = [];
    }
    if (aggregateDoc.componentValues.length <= index) {
        const existingComponentValues = cloneDeep(aggregateDoc.componentValues);
        for (let i = aggregateDoc.componentValues.length; i <= index; i++) {
            existingComponentValues.unshift(createAggregateComponentInterface());
        }
        aggregateDoc.componentValues = existingComponentValues;
    }
};
