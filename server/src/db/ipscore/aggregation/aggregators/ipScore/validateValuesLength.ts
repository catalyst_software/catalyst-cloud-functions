import { cloneDeep, isArray } from "lodash";
import { AthleteIpScoreWeeklyHistoryAggregateInterface } from '../../../models/interfaces/athlete-ipscore-weekly-history-aggregate';
export const validateValuesLength = (aggregateDoc: AthleteIpScoreWeeklyHistoryAggregateInterface, index: number) => {
    
    if (!isArray(aggregateDoc.values)) {
        aggregateDoc.values = [];
    }
    
    if (aggregateDoc.values.length <= index) {
        const existingValues = cloneDeep(aggregateDoc.values);
        for (let i = aggregateDoc.values.length; i <= index; i++) {
            existingValues.unshift(0);
        }
        aggregateDoc.values = existingValues;
    }

};
