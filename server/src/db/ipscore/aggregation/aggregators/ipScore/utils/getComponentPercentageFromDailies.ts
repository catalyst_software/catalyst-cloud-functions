import { mean } from 'lodash'

import { IpScoreAggregateComponentInterface } from '../../../../models/interfaces/ipscore-component-aggregate'
import { LifestyleEntryType } from '../../../../../../models/enums/lifestyle-entry-type'

export const getComponentPercentageFromDailies = (
    dayIndex: number,
    type: LifestyleEntryType,
    dailies: Array<IpScoreAggregateComponentInterface>,
    startIndex: number
): number => {
    const percentages: Array<number> = []

    const log = false
    if (log) {
        console.log(
            '--------------------->>>>>>>>>>>>>>>>>> getComponentPercentageFromDailies startIndex',
            startIndex
        )

        console.log(
            '--------------------->>>>>>>>>>>>>>>>>> getComponentPercentageFromDailies dayIndex',
            LifestyleEntryType[type]
        )
        console.log('--------------------->>>>>>>>>>>>>>>>>> type ', dayIndex)
    }

    for (let i = startIndex; i <= dayIndex; i++) {
        // 12 and 12
        const dailyAggregate = dailies[i]
        switch (type) {
            case LifestyleEntryType.Mood:
                // console.log('======================>>>>>>>>>>>>>>>>>>>>>>> getComponentPercentageFromDailies dailyAggregate.brain.mood.percentage', dailyAggregate.brain.mood.percentage);
                percentages.push(dailyAggregate.brain.mood.percentage) // 0.08333333333333333
                // console.log('======================>>>>>>>>>>>>>>>>>>>>>>> percentages.push(dailyAggregate.brain.mood.percentage): percentages === ', percentages);

                break
            case LifestyleEntryType.Sleep:
                percentages.push(dailyAggregate.brain.sleep.percentage)
                break
            case LifestyleEntryType.Fatigue:
                percentages.push(dailyAggregate.body.fatigue.percentage)
                break
            case LifestyleEntryType.Pain:
                percentages.push(dailyAggregate.body.pain.percentage)
                break
            case LifestyleEntryType.Weight:
                percentages.push(dailyAggregate.body.weight.percentage)
                break
            case LifestyleEntryType.Steps:
            case LifestyleEntryType.Distance:
            case LifestyleEntryType.HeartRate:
                percentages.push(dailyAggregate.body.wearables.percentage)
                break
            case LifestyleEntryType.Food:
                percentages.push(dailyAggregate.food.components.percentage)
                break
            case LifestyleEntryType.Training:
                percentages.push(dailyAggregate.training.components.percentage)
                break
        }
    }
    const meanValue = mean(percentages)

    //    console.log('======================>>>>>>>>>>>>>>>>>>>>>>> mean(percentages) === ', meanValue);

    return meanValue
}
