import { firestore } from 'firebase-admin';
import { getDayDifference, getIsoWeekDay, getMonthDayIndex } from '../../../../../../shared/utils/moment';
export const getStartIndex = (dayIndex: number, athleteCreationTimestamp: firestore.Timestamp, utcOffset: number, isMonth?: boolean): number => {
    const diffInDays = getDayDifference(athleteCreationTimestamp, firestore.Timestamp.now(), utcOffset);
    if (isMonth) {
        if (diffInDays >= 30) {
            return 0;
        }
        else {
            return getMonthDayIndex(athleteCreationTimestamp, utcOffset);
        }
    } else {
        if (diffInDays >= 7) {
            return 0;
        }
        else {
            return getIsoWeekDay(athleteCreationTimestamp, utcOffset);
        }
    }
};
