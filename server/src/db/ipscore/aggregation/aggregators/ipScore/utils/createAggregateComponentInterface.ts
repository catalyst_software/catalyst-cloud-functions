import { IpScoreAggregateComponentInterface } from '../../../../models/interfaces/ipscore-component-aggregate'
import { IpScoreProgramsConstituent } from '../../../../models/interfaces/ipscore-programs-constituent.interface'
import { IpScoreBrainConstituent } from '../../../../models/interfaces/ipscore-brain-constituent.interface'
import { IpScoreBodyConstituent } from '../../../../models/interfaces/ipscore-body-constituent.interface'
import { IpScoreFoodConstituent } from '../../../../models/interfaces/ipscore-food-constituent.interface'
import { IpScoreTrainingConstituent } from '../../../../models/interfaces/ipscore-training-constituent.interface'
export const createAggregateComponentInterface = (): IpScoreAggregateComponentInterface => {
    return {
        value: 0,
        brain: {
            mood: {
                realized: 0,
                expected: 0,
                percentage: 0,
            },
            sleep: {
                realized: 0,
                expected: 0,
                percentage: 0,
            },
        } as IpScoreBrainConstituent,
        body: {
            fatigue: {
                realized: 0,
                expected: 0,
                percentage: 0,
            },
            pain: {
                realized: 0,
                expected: 0,
                percentage: 0,
            },
            weight: {
                realized: 0,
                expected: 0,
                percentage: 0,
            },
            wearables: {
                realized: 0,
                expected: 0,
                percentage: 0,
            },
        } as IpScoreBodyConstituent,
        food: {
            components: {
                realized: 0,
                expected: 0,
                percentage: 0,
            },
        } as IpScoreFoodConstituent,
        training: {
            components: {
                realized: 0,
                expected: 0,
                percentage: 0,
            },
        } as IpScoreTrainingConstituent,
        programs: {
            videoConsumed: false,
            surveyConsumed: false,
            worksheetConsumed: false,
        } as IpScoreProgramsConstituent,
    } as IpScoreAggregateComponentInterface
}
