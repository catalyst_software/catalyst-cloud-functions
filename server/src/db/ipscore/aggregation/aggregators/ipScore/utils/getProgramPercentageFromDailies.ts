import { mean } from 'lodash';

import { IpScoreAggregateComponentInterface } from '../../../../models/interfaces/ipscore-component-aggregate';
import { calculateIpScoreFromProgramsConstituent } from './calculateIpScoreFromProgramsConstituent';

export const getProgramPercentageFromDailies = (dayIndex: number, dailies: Array<IpScoreAggregateComponentInterface>, startIndex: number): number => {

    const percentages: Array<number> = [];
    for (let i = startIndex; i <= dayIndex; i++) {
        const dailyAggregate = dailies[i];
        percentages.push(calculateIpScoreFromProgramsConstituent(dailyAggregate.programs));
    }
    return mean(percentages);
};
