import { IpScoreProgramsConstituent } from '../../../../models/interfaces/ipscore-programs-constituent.interface';
export const calculateIpScoreFromProgramsConstituent = (programs: IpScoreProgramsConstituent): number => {
    let complete = 0;
    complete += (programs.videoConsumed) ? 1 : 0;
    complete += (programs.surveyConsumed) ? 1 : 0;
    complete += (programs.worksheetConsumed) ? 1 : 0;
    return complete / 3;
};
