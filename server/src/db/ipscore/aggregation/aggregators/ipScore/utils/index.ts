import { mean } from 'lodash'

// import { AthleteBiometricEntry } from '../../../../../../models/well/interfaces/athelete-biometric-entry';
// import { LogInfo } from '../../../../../../shared/logger/logger';
import { IpScoreLifestyleTypeConstituent } from '../../../../models/interfaces/ipscore-lifestyletype-constituent.interface'
import { IpScoreAggregateComponentInterface } from '../../../../models/interfaces/ipscore-component-aggregate'

import { LifestyleEntryType } from '../../../../../../models/enums/lifestyle-entry-type'
import { getComponentPercentageFromDailies } from './getComponentPercentageFromDailies'
import { getProgramPercentageFromDailies } from './getProgramPercentageFromDailies'

export const isNumber = (val: any) => {
    return !isNaN(parseInt(val))
}

export const updateIpScoreConstituent = (
    realized: number,
    expected: number,
    constituent: IpScoreLifestyleTypeConstituent
) => {
    console.log('updateIpScoreConstituent -> realized', realized)
    console.log('updateIpScoreConstituent -> expected', expected)
    console.log('updateIpScoreConstituent -> constituent', constituent)
    constituent.realized = realized
    constituent.expected = expected
    const percent = realized / expected
    constituent.percentage = percent < 1 ? percent : 1
}

export const calculateIpScoreFromDailies = (
    dailies: Array<IpScoreAggregateComponentInterface>,
    dayIndex: number,
    startIndex: number
): number => {
    const percentages: Array<number> = []
    for (let i = startIndex; i <= dayIndex; i++) {
        percentages.push(dailies[i].value)
    }

    return mean(percentages)
}

export const recordHistoryIpScoreComponentPercentagesFromDailies = (
    component: IpScoreAggregateComponentInterface,
    dailies: Array<IpScoreAggregateComponentInterface>,
    dayIndex: number,
    startIndex: number,
    includeWearables: boolean
): void => {
    component.brain.mood.percentage = getComponentPercentageFromDailies(
        dayIndex,
        LifestyleEntryType.Mood,
        dailies,
        startIndex
    ) // 0.08333333333333333
    component.brain.sleep.percentage = getComponentPercentageFromDailies(
        dayIndex,
        LifestyleEntryType.Sleep,
        dailies,
        startIndex
    )
    component.brain.value = mean([
        component.brain.mood.percentage,
        component.brain.sleep.percentage,
    ])
    component.body.fatigue.percentage = getComponentPercentageFromDailies(
        dayIndex,
        LifestyleEntryType.Fatigue,
        dailies,
        startIndex
    )
    component.body.pain.percentage = getComponentPercentageFromDailies(
        dayIndex,
        LifestyleEntryType.Pain,
        dailies,
        startIndex
    )
    component.body.weight.percentage = getComponentPercentageFromDailies(
        dayIndex,
        LifestyleEntryType.Weight,
        dailies,
        startIndex
    )
    if (includeWearables) {
        component.body.wearables.percentage = getComponentPercentageFromDailies(
            dayIndex,
            LifestyleEntryType.HeartRate,
            dailies,
            startIndex
        )
        component.body.value = mean([
            component.body.fatigue.percentage,
            component.body.pain.percentage,
            component.body.weight.percentage,
            component.body.wearables.percentage,
        ])
    } else {
        component.body.value = mean([
            component.body.fatigue.percentage,
            component.body.weight.percentage,
            component.body.pain.percentage,
        ])
    }
    component.food.components.percentage = getComponentPercentageFromDailies(
        dayIndex,
        LifestyleEntryType.Food,
        dailies,
        startIndex
    )
    component.food.value = component.food.components.percentage
    component.training.components.percentage = getComponentPercentageFromDailies(
        dayIndex,
        LifestyleEntryType.Training,
        dailies,
        startIndex
    )
    component.training.value = component.training.components.percentage
    component.programs.value = getProgramPercentageFromDailies(
        dayIndex,
        dailies,
        startIndex
    )
}
