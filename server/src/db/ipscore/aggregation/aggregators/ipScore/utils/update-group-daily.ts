import { mean, sum } from 'lodash';

import { IpScoreAggregateComponentInterface } from '../../../../models/interfaces/ipscore-component-aggregate';

export const updateGroupDaily = (parts: string[], percentages: number[], totalProgramPoints: Array<number> = [], dailyAggregate: IpScoreAggregateComponentInterface, includeWearables: boolean) => {
    switch (parts[1].toLowerCase()) {
        case 'brain':
            switch (parts[2].toLowerCase()) {
                case 'mood':
                    dailyAggregate.brain.mood.percentage = mean(percentages);
                    break;
                case 'sleep':
                    dailyAggregate.brain.sleep.percentage = mean(percentages);
                    break;
            }
            dailyAggregate.brain.value = mean([dailyAggregate.brain.mood.percentage, dailyAggregate.brain.sleep.percentage]);
            break;
        case 'body':
            switch (parts[2].toLowerCase()) {
                case 'fatigue':
                    dailyAggregate.body.fatigue.percentage = mean(percentages);
                    break;
                case 'pain':
                    dailyAggregate.body.pain.percentage = mean(percentages);
                    break;
                case 'wearables':
                    if (includeWearables) {
                        dailyAggregate.body.wearables.percentage = mean(percentages);
                    }
                    break;
            }
            dailyAggregate.body.value = mean([dailyAggregate.body.fatigue.percentage, dailyAggregate.body.pain.percentage]);
            break;
        case 'food':
        case 'simple-food':
                dailyAggregate.food.components.percentage = mean(percentages);
                dailyAggregate.food.value = dailyAggregate.food.components.percentage;
            break;
        case 'training':
            dailyAggregate.training.components.percentage = mean(percentages);
            dailyAggregate.training.value = dailyAggregate.training.components.percentage;
            break;
        case 'programs':
            dailyAggregate.programs.value = sum(totalProgramPoints);
            break;
    }
};
