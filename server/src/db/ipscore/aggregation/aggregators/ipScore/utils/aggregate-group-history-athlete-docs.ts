import { cloneDeep } from 'lodash';

import { createAggregateComponentInterface } from "./createAggregateComponentInterface";

import { Group } from "../../../../../../models/group/group.model";
import { AthleteIpScoreMonthlyHistoryAggregateInterface } from '../../../../models/interfaces/athlete-ipscore-monthly-history-aggregate';
import { getUniqueListBy } from '../../../../../../api/app/v1/callables/utils/getUniqueListBy';
import { AthleteIndicator } from '../../../../../biometricEntries/well/interfaces/athlete-indicator';

export const aggregateGroupHistoryAthleteDocs = (athleteAggregateDocs: AthleteIpScoreMonthlyHistoryAggregateInterface[], dayIndex: number, parts: string[], percentages: number[], 
    totalProgramPoints: Array<number> = [], includeWearables: boolean, group?: Group) => {

    athleteAggregateDocs.forEach((doc: AthleteIpScoreMonthlyHistoryAggregateInterface) => {
        if (doc.trackingCache.days.length <= dayIndex) {
            const existingDays = cloneDeep(doc.trackingCache.days);
            for (let i = doc.trackingCache.days.length; i <= dayIndex; i++) {
                const component = createAggregateComponentInterface();
                existingDays.push(component);
            }
            doc.trackingCache.days = existingDays;
        }
        const athleteDailyAggregate = cloneDeep(doc.trackingCache.days[dayIndex]);
        switch (parts[1].toLowerCase()) {
            case 'brain':
                switch (parts[2].toLowerCase()) {
                    case 'mood':
                        percentages.push(athleteDailyAggregate.brain.mood.percentage || 0);
                        break;
                    case 'sleep':
                        percentages.push(athleteDailyAggregate.brain.sleep.percentage || 0);
                        break;
                }
                break;
            case 'body':
                switch (parts[2].toLowerCase()) {
                    case 'fatigue':
                        percentages.push(athleteDailyAggregate.body.fatigue.percentage || 0);
                        break;
                    case 'pain':
                        percentages.push(athleteDailyAggregate.body.pain.percentage || 0);
                        break;
                    case 'wearables':
                        if (includeWearables) {
                            athleteDailyAggregate.body.wearables.realized++;
                            percentages.push(athleteDailyAggregate.body.wearables.percentage || 0);
                        }
                        break;
                }
                break;
            case 'simple-food':
            case 'food':
                percentages.push(athleteDailyAggregate.food.components.percentage || 0);
                break;
            case 'training':
                percentages.push(athleteDailyAggregate.training.components.percentage || 0);
                break;
            case 'programs':
                const { historicalValues } = athleteDailyAggregate.programs;
                totalProgramPoints.concat(historicalValues || []);
                break;
        }
        if (group) {
            const athleteRecentEntries = cloneDeep(doc.recentEntries);
            let existingAthletes = cloneDeep(group.athletes);
            existingAthletes = getUniqueListBy(existingAthletes, 'uid') as AthleteIndicator[]

            const athleteIndex: number = existingAthletes.findIndex((ath) => ath.uid === doc.entityId);
            if (athleteIndex > -1) {
                const athleteIndicator = cloneDeep(group.athletes[athleteIndex]);
                athleteIndicator.recentEntries = athleteRecentEntries;
                existingAthletes[athleteIndex] = athleteIndicator;
            }
            group.athletes = existingAthletes;
        }

    });
};
