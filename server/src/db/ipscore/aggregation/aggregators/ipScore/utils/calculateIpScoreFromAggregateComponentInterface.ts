import { has, mean } from 'lodash'
import { IpScoreAggregateComponentInterface } from '../../../../models/interfaces/ipscore-component-aggregate'
import { calculateIpScoreFromProgramsConstituent } from './calculateIpScoreFromProgramsConstituent'
export const calculateIpScoreFromAggregateComponentInterface = (
    component: IpScoreAggregateComponentInterface,
    includeWearables?: boolean,
    isPenalized?: boolean,
    maxScorePossible?: number
): number => {
    console.warn(
        '----->>>>>>>> calculateIpScoreFromAggregateComponentInterface component',
        component
    )
    console.log('----->>>>>>>> component.brain', component.brain)
    console.log('----->>>>>>>> component.body', component.body)
    console.log('----->>>>>>>> component.food', component.food)
    console.log('----->>>>>>>> component.training', component.training)
    console.log('----->>>>>>>> component.programs', component.programs)

    const percentages: Array<number> = []
    percentages.push(component.brain.mood.percentage)
    percentages.push(component.brain.sleep.percentage)
    component.brain.value = mean([
        component.brain.mood.percentage,
        component.brain.sleep.percentage,
    ])
    percentages.push(component.body.fatigue.percentage)
    percentages.push(component.body.pain.percentage)
    percentages.push(component.body.weight.percentage)

    if (includeWearables) {
        percentages.push(component.body.wearables.percentage)
        component.body.value = mean([
            component.body.fatigue.percentage,
            component.body.pain.percentage,
            component.body.weight.percentage,
            component.body.wearables.percentage,
        ])
    } else {
        component.body.value = mean([
            component.body.fatigue.percentage,
            component.body.pain.percentage,
            component.body.weight.percentage,
        ])
    }

    percentages.push(component.food.components.percentage)
    component.food.value = component.food.components.percentage
    percentages.push(component.training.components.percentage)

    if (component.programs) {
        if (component.programs.expected) {
            console.log(
                '----->>>>>>>> setting component.programs.value = component.programs.percentage',
                component.programs
            )
            component.programs.value = component.programs.percentage
            console.log(
                '----->>>>>>>> component.programs after set',
                component.programs
            )
            percentages.push(component.programs.percentage)
        } else {
            component.programs.value = calculateIpScoreFromProgramsConstituent(
                component.programs
            )
            console.log(
                '----->>>>>>>> calculateIpScoreFromProgramsConstituent(component.programs) update',
                component.programs
            )
            percentages.push(component.programs.value)
        }
    }

    console.log('----->>>>>>>>  daily percentages =>', percentages)
    let ipScore = mean(percentages)
    console.log('----->>>>>>>>  calculated daily ipScore =>', ipScore)

    if (isPenalized) {
        console.log('Athlete isPenalized', isPenalized)
        console.log(
            'Athlete ipScore > maxScorePossible',
            ipScore > maxScorePossible
        )
        if (ipScore > maxScorePossible) {
            ipScore = maxScorePossible
        }
    }

    return ipScore
}
