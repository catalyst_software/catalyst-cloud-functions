import { cloneDeep } from "lodash";

import { AthleteIpScoreWeeklyHistoryAggregateInterface } from "../../../models/interfaces/athlete-ipscore-weekly-history-aggregate";
import { createAggregateComponentInterface } from "./utils/createAggregateComponentInterface";
import { GroupIpScoreMonthlyHistoryAggregateInterface } from "../../../models/interfaces/group-ipscore-monthly-history-aggregate";
import { IpScoreAggregateComponentInterface } from "../../../models/interfaces/ipscore-component-aggregate";
import { GroupIpScoreWeeklyHistoryAggregateInterface } from "../../../models/interfaces/group-ipscore-weekly-history-aggregate";

export const validateTrackingCacheDays = (aggregateDoc: AthleteIpScoreWeeklyHistoryAggregateInterface, dayIndex: number) => {

    if (aggregateDoc.trackingCache.days.length <= dayIndex) {
        const existingDays = cloneDeep(aggregateDoc.trackingCache.days);
        for (let i = aggregateDoc.trackingCache.days.length; i <= dayIndex; i++) {
            existingDays.push(createAggregateComponentInterface());
            aggregateDoc.trackingCache.dayIndex = i;
        }
        aggregateDoc.trackingCache.days = existingDays;
    }
};


export const validateGroupIpScoreWeeklyHistoryTrackingCacheDays = (aggregateDoc: GroupIpScoreWeeklyHistoryAggregateInterface, dayIndex: number) => {
    if (aggregateDoc.trackingCache.days.length <= dayIndex) {
        const existingDays = cloneDeep(aggregateDoc.trackingCache.days);
        for (let i = aggregateDoc.trackingCache.days.length; i <= dayIndex; i++) {
            existingDays.push(createAggregateComponentInterface());
        }
        aggregateDoc.trackingCache.days = existingDays;
    }
};

export const validateMonthlyTrackingCacheDays = (aggregateDoc: GroupIpScoreMonthlyHistoryAggregateInterface, dayIndex: number,
    existingRecentEntries: IpScoreAggregateComponentInterface[]) => {
    if (aggregateDoc.trackingCache.days.length <= dayIndex) {
        const existingDays = cloneDeep(aggregateDoc.trackingCache.days);
        for (let i = aggregateDoc.trackingCache.days.length; i <= dayIndex; i++) {
            const component = createAggregateComponentInterface();
            existingDays.push(component);
            if (existingRecentEntries.length >= 10) {
                existingRecentEntries.pop();
            }
            aggregateDoc.trackingCache.dayIndex = i;
            existingRecentEntries.unshift(component);
        }
        aggregateDoc.trackingCache.days = existingDays;
    }
};
