import { cloneDeep, isArray } from 'lodash';
import { firestore } from 'firebase-admin';

import { getDayIndex } from './../../../../../shared/utils/moment/index';
import { LogInfo } from '../../../../../shared/logger/logger'
import { calculateIpScoreFromDailies, recordHistoryIpScoreComponentPercentagesFromDailies } from './utils'
import { calculateIpScoreFromAggregateComponentInterface } from "./utils/calculateIpScoreFromAggregateComponentInterface";
import { getWeekNumber } from '../../../../../shared/utils/moment'

import { Athlete } from '../../../../../models/athlete/interfaces/athlete'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { GroupIpScoreWeeklyHistoryAggregateInterface } from '../../../models/interfaces/group-ipscore-weekly-history-aggregate'
import { validateGroupIpScoreWeeklyHistoryTrackingCacheDays } from './validateTrackingCacheDays';
import { Group } from "../../../../../models/group/group.model";
import { AthleteIpScoreWeeklyHistoryAggregateInterface } from '../../../models/interfaces/athlete-ipscore-weekly-history-aggregate';
import { IpScoreAggregateComponentInterface } from '../../../models/interfaces/ipscore-component-aggregate';
import { getStartIndex } from './utils/getStartIndex';
import { validateTrackingCache } from "./validateTrackingCache";
import { validateComponentValues } from './validateComponentValues';
import { validateValuesLength } from './validateValuesLength';
import { updateGroupDaily } from './utils/update-group-daily';
import { aggregateGroupHistoryAthleteDocs } from './utils/aggregate-group-history-athlete-docs';

export const aggregateGroupIpScoreWeeklyHistory = (
    directive: AggregateIpScoreDirective
): GroupIpScoreWeeklyHistoryAggregateInterface => {
    const athlete: Athlete = directive.athlete;
    const group: Group = directive.group;

    debugger;
    const aggregateDoc: GroupIpScoreWeeklyHistoryAggregateInterface = directive.aggregateDocument as GroupIpScoreWeeklyHistoryAggregateInterface;
    const athleteAggregateDocs: Array<AthleteIpScoreWeeklyHistoryAggregateInterface> = directive.athleteDocuments as Array<AthleteIpScoreWeeklyHistoryAggregateInterface>;

    if (athleteAggregateDocs) {
        const dayIndex = getDayIndex(firestore.Timestamp.now(), directive.utcOffset || 0);
        // console.warn('===============>>>>>>>>>>>>> group.creationTimestamp ', group.creationTimestamp.toDate())
        // console.warn('===============>>>>>>>>>>>>> athlete.currentIpScoreTracking.dateTime ', athlete.currentIpScoreTracking.dateTime.toDate())
        const weekNumber = getWeekNumber(group.creationTimestamp, athlete.currentIpScoreTracking.dateTime, directive.utcOffset);

        // console.warn('===============>>>>>>>>>>>>> getWeekNumber(group.creationTimestamp, athlete.currentIpScoreTracking.dateTime)', weekNumber)

        const currentTracking = athlete.currentIpScoreTracking;
        // const execute = currentTracking.directive.execute || false;
        const paths = currentTracking.directive.paths || [];

        LogInfo({
            message: `Processing GroupWeekly History aggregate document in IpScore aggregator.`,
            data: {
                AggregateDocument: aggregateDoc,
                Athlete: athlete,
                WeekNumber: weekNumber,
                DayIndex: dayIndex,
                ExecuteDirective: athlete.currentIpScoreTracking.directive
            }
        });

        // console.warn('aggregateGroupIpScoreWeeklyHistory aggregateDoc', aggregateDoc)
        validateTrackingCache(aggregateDoc, weekNumber);

        // Array of current week IP Score aggregate component data structures
        // On daily basis

        // TODO: Compare Below Methods
        validateGroupIpScoreWeeklyHistoryTrackingCacheDays(aggregateDoc, dayIndex);
        // validateMonthlyTrackingCacheDays(aggregateDoc, dayIndex, existingRecentEntries);

        // Array of week IP Score (numbers)
        validateValuesLength(aggregateDoc, weekNumber);

        // Array of historical week IP Score aggregate component data structures
        // Representing week percentages across all lifestyle entry types
        validateComponentValues(aggregateDoc, weekNumber);

        const dailyAggregate: IpScoreAggregateComponentInterface = aggregateDoc.trackingCache.days[dayIndex];

        const includeWearables = false;
        const totalProgramPoints = [];

        paths.forEach((path: string) => {
            const percentages: Array<number> = [];
            const parts = path.split('.');

            // Aggregate athlete docs given path
            aggregateGroupHistoryAthleteDocs(athleteAggregateDocs, dayIndex, parts, percentages, totalProgramPoints, includeWearables);

            const includedAthletes = group.athletes.filter((ath) => ath.includeGroupAggregation)
            if (athleteAggregateDocs.length < includedAthletes.length) {
                for (let i = athleteAggregateDocs.length; i < includedAthletes.length; i++) {
                    percentages.push(0);
                }
            }

            // Update the group daily from the group weekly doc
            updateGroupDaily(parts, percentages, totalProgramPoints, dailyAggregate, includeWearables);
        });

        dailyAggregate.value = calculateIpScoreFromAggregateComponentInterface(dailyAggregate);
        aggregateDoc.trackingCache.days[dayIndex] = dailyAggregate;

        const updatedExistingValues = cloneDeep(aggregateDoc.values);

        const startIndex = getStartIndex(dayIndex, group.creationTimestamp, directive.utcOffset, false);

        updatedExistingValues[0] = calculateIpScoreFromDailies(aggregateDoc.trackingCache.days, dayIndex, startIndex);
        aggregateDoc.values = updatedExistingValues;

        const existingComponents = cloneDeep(aggregateDoc.componentValues);
        recordHistoryIpScoreComponentPercentagesFromDailies(existingComponents[0], aggregateDoc.trackingCache.days, dayIndex, startIndex, false);

        aggregateDoc.componentValues = existingComponents;

        aggregateDoc.lastAggregateDate = firestore.Timestamp.now();

    } else {
        console.warn('NO ATHLETES FOUND FOR AGGREGATION')
    }


    // console.warn('aggregateGroupIpScoreWeeklyHistory aggregateDoc', aggregateDoc)
    return aggregateDoc;

};

export const validateValues = (aggregateDoc: GroupIpScoreWeeklyHistoryAggregateInterface, weekNumber: number) => {
    if (!isArray(aggregateDoc.values)) {
        aggregateDoc.values = [];
    }
    if (aggregateDoc.values.length <= weekNumber) {
        const existingValues = cloneDeep(aggregateDoc.values);
        for (let i = aggregateDoc.values.length; i <= weekNumber; i++) {
            existingValues.unshift(0);
        }
        aggregateDoc.values = existingValues;
    }
};
