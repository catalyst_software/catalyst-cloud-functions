import { mean, cloneDeep, sum, isArray } from "lodash";

import { calculateIpScoreFromDailies } from "./utils";
import { calculateIpScoreFromAggregateComponentInterface } from "./utils/calculateIpScoreFromAggregateComponentInterface";

import { getDayIndex } from "../../../../../shared/utils/moment";
import { Athlete } from "../../../../../models/athlete/interfaces/athlete";
import { AggregateIpScoreDirective } from "../../../services/interfaces/aggregate-ipscore-directive";
import { GroupIpScoreWeeklyAggregateInterface, AthleteIpScoreWeeklyAggregateInterface, GroupIpScoreDailyAggregate, AthleteIpScoreDailyAggregate } from "../../../models/interfaces";
import { Group } from "../../../../../models/group/group.model";
import { getStartIndex } from "./utils/getStartIndex";
import { firestore } from "firebase-admin";

export const aggregateGroupIpScoreWeekly = (
    directive: AggregateIpScoreDirective
): GroupIpScoreWeeklyAggregateInterface => {
    const athlete: Athlete = directive.athlete;
    const group: Group = directive.group;

    debugger;
    const aggregateDoc: GroupIpScoreWeeklyAggregateInterface = directive.aggregateDocument as GroupIpScoreWeeklyAggregateInterface;
    const athleteAggregateDocs: Array<AthleteIpScoreWeeklyAggregateInterface> = directive.athleteDocuments as Array<AthleteIpScoreWeeklyAggregateInterface>;

    if (athleteAggregateDocs) {


        const dayIndex = getDayIndex(firestore.Timestamp.now(), directive.utcOffset || 0);

        const currentTracking = athlete.currentIpScoreTracking;

        const paths = currentTracking.directive.paths || [];

        aggregateDoc.dailies.forEach((daily) => console.log(daily));
        console.log(`=====================>>>>>>>>>>>>>> Processing GroupWeekly aggregate document in IpScore aggregator.`, {
            AggregateDocument: aggregateDoc,
            Athlete: athlete,
            DayIndex: dayIndex,
            ExecuteDirective: athlete.currentIpScoreTracking.directive
        });


        const originalDailyGroupAggregate: GroupIpScoreDailyAggregate = aggregateDoc.dailies[dayIndex];

        const dailyGroupAggregate: GroupIpScoreDailyAggregate = cloneDeep(originalDailyGroupAggregate);

        const totalProgramPoints = [];

        if (paths && isArray(paths)) {

            paths.forEach((path: string) => {
                const percentages: Array<number> = [];
                const parts = path.split('.');

                // Aggregate athlete docs given path
                athleteAggregateDocs.forEach((athleteAggregateDoc: AthleteIpScoreWeeklyAggregateInterface) => {

                    const dailyAthleteAggregate: AthleteIpScoreDailyAggregate = athleteAggregateDoc.dailies[dayIndex];

                    switch (parts[1].toLowerCase()) {

                        case 'brain':
                            switch (parts[2].toLowerCase()) {
                                case 'mood':
                                    percentages.push(dailyAthleteAggregate.brain.mood.percentage || 0);
                                    break;
                                case 'sleep':
                                    percentages.push(dailyAthleteAggregate.brain.sleep.percentage || 0);
                                    break;
                            }
                            break;
                        case 'body':
                            switch (parts[2].toLowerCase()) {
                                case 'fatigue':
                                    percentages.push(dailyAthleteAggregate.body.fatigue.percentage || 0);
                                    break;
                                case 'pain':
                                    percentages.push(dailyAthleteAggregate.body.pain.percentage || 0);
                                    break;
                                // case 'wearables':
                                //     dailyAthleteAggregate.body.wearables.realized++;
                                //     percentages.push(dailyAthleteAggregate.body.wearables.percentage || 0);
                                //     break;
                            }
                            break;
                        case 'simple-food':
                        case 'food':
                            percentages.push(dailyAthleteAggregate.food.components.percentage || 0);
                            break;
                        case 'training':
                            percentages.push(dailyAthleteAggregate.training.components.percentage || 0);
                            break;
                        case 'programs':
                            dailyGroupAggregate.programs.expected = 1;

                            const { historicalValues } = dailyGroupAggregate.programs;

                            totalProgramPoints.concat(historicalValues || []);
                            break;
                    }
                });

                const includedAthletes = group.athletes.filter((ath) => ath.includeGroupAggregation)
                if (athleteAggregateDocs.length < includedAthletes.length) {
                    for (let i = athleteAggregateDocs.length; i < includedAthletes.length; i++) {
                        percentages.push(0);
                    }
                }

                // console.log('----->>>>>>>> percentages', percentages);

                // Update the group daily from the group weekly doc
                switch (parts[1].toLowerCase()) {
                    case 'brain':
                        switch (parts[2].toLowerCase()) {
                            case 'mood':
                                dailyGroupAggregate.brain.mood.percentage = mean(percentages);
                                break;
                            case 'sleep':
                                dailyGroupAggregate.brain.sleep.percentage = mean(percentages);
                                break;
                        }
                        dailyGroupAggregate.brain.value = mean([dailyGroupAggregate.brain.mood.percentage, dailyGroupAggregate.brain.sleep.percentage]);
                        break;
                    case 'body':
                        switch (parts[2].toLowerCase()) {
                            case 'fatigue':
                                dailyGroupAggregate.body.fatigue.percentage = mean(percentages);
                                break;
                            case 'pain':
                                dailyGroupAggregate.body.pain.percentage = mean(percentages);
                                break;
                            // case 'wearables':
                            //     dailyAggregate.body.wearables.percentage = mean(percentages);
                            //     break;
                        }
                        dailyGroupAggregate.body.value = mean([dailyGroupAggregate.body.fatigue.percentage, dailyGroupAggregate.body.pain.percentage]);
                        break;
                    case 'simple-food':
                    case 'food':
                        dailyGroupAggregate.food.components.percentage = mean(percentages);
                        dailyGroupAggregate.food.value = dailyGroupAggregate.food.components.percentage;
                        break;
                    case 'training':
                        dailyGroupAggregate.training.components.percentage = mean(percentages);
                        dailyGroupAggregate.training.value = dailyGroupAggregate.training.components.percentage;
                        break;
                    case 'programs':
                        if (dailyGroupAggregate.programs.expected) {
                            dailyGroupAggregate.programs.percentage = mean(percentages);
                        }

                        dailyGroupAggregate.programs.value = sum(totalProgramPoints);

                        break;
                }

                dailyGroupAggregate.value = calculateIpScoreFromAggregateComponentInterface(dailyGroupAggregate);
                // console.log('------------------------->>>>>>>>>>>>>>>>>>>>>>>  dailyGroupAggregate.value', dailyGroupAggregate.value);

                aggregateDoc.dailies[dayIndex] = dailyGroupAggregate;
                
                const startIndex = getStartIndex(dayIndex, group.creationTimestamp, directive.utcOffset, false);
                
                aggregateDoc.value = calculateIpScoreFromDailies(aggregateDoc.dailies, dayIndex, startIndex);

            });
        } else {
            console.error('NO PATH SPECIFIED')
        }


    } else {
        console.warn('NO ATHLETES FOUND FOR AGGREGATION')
    }

    return aggregateDoc;

};
