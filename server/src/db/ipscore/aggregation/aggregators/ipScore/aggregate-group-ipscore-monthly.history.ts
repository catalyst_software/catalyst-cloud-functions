import { firestore } from 'firebase-admin';

import { getDayIndex, getMonthNumber } from './../../../../../shared/utils/moment/index';
import { LogInfo } from '../../../../../shared/logger/logger'
import { calculateIpScoreFromDailies, recordHistoryIpScoreComponentPercentagesFromDailies } from './utils'
import { calculateIpScoreFromAggregateComponentInterface } from "./utils/calculateIpScoreFromAggregateComponentInterface";
import { cloneDeep, isArray } from 'lodash'
import { Athlete } from '../../../../../models/athlete/interfaces/athlete'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { GroupIpScoreMonthlyHistoryAggregateInterface } from '../../../models/interfaces/group-ipscore-monthly-history-aggregate';
import { Group } from "../../../../../models/group/group.model";
import { AthleteIpScoreMonthlyHistoryAggregateInterface } from '../../../models/interfaces/athlete-ipscore-monthly-history-aggregate';
import { IpScoreAggregateComponentInterface } from '../../../models/interfaces/ipscore-component-aggregate';
import { getStartIndex } from './utils/getStartIndex';
import { validateTrackingCache } from './validateTrackingCache';
import { updateRecentEntries } from './updateRecentEntries';
import { validateMonthlyTrackingCacheDays } from './validateTrackingCacheDays';
import { validateComponentValuesLength } from './validateComponentValuesLength';
import { validateValuesLength } from './validateValuesLength';
import { updateGroupDaily } from './utils/update-group-daily';
import { aggregateGroupHistoryAthleteDocs } from './utils/aggregate-group-history-athlete-docs';

export const aggregateGroupIpScoreMonthlyHistory = (
    directive: AggregateIpScoreDirective
): GroupIpScoreMonthlyHistoryAggregateInterface => {
    const athlete: Athlete = directive.athlete;
    const group: Group = directive.group;

    debugger;
    const aggregateDoc: GroupIpScoreMonthlyHistoryAggregateInterface = directive.aggregateDocument as GroupIpScoreMonthlyHistoryAggregateInterface;
    const athleteAggregateDocs: Array<AthleteIpScoreMonthlyHistoryAggregateInterface> = directive.athleteDocuments as Array<AthleteIpScoreMonthlyHistoryAggregateInterface>;

    if (athleteAggregateDocs) {

        const dayIndex = getDayIndex(firestore.Timestamp.now(), directive.utcOffset || 0, true);
        const monthNumber = getMonthNumber(group.creationTimestamp, athlete.currentIpScoreTracking.dateTime, directive.utcOffset);

        const currentTracking = athlete.currentIpScoreTracking;

        const paths = currentTracking.directive.paths || [];

        LogInfo({
            message: `Processing Group Monthly History aggregate document in IpScore aggregator.`,
            data: {
                AggregateDocument: aggregateDoc,
                Athlete: athlete,
                MonthNumber: monthNumber,
                DayIndex: dayIndex,
                ExecuteDirective: athlete.currentIpScoreTracking.directive
            }
        });

        validateTrackingCache(aggregateDoc, monthNumber);

        if (!isArray(aggregateDoc.recentEntries)) {
            aggregateDoc.recentEntries = [];
        }
        const existingRecentEntries = cloneDeep(aggregateDoc.recentEntries);

        // Array of current week IP Score aggregate component data structures
        // On daily basis
        validateMonthlyTrackingCacheDays(aggregateDoc, dayIndex, existingRecentEntries);

        // Array of month IP Score (numbers)
        validateValuesLength(aggregateDoc, monthNumber);

        // Array of historical month IP Score aggregate component data structures
        // Representing month percentages across all lifestyle entry types
        validateComponentValuesLength(aggregateDoc, monthNumber);

        const originalDailyAggregate: IpScoreAggregateComponentInterface = aggregateDoc.trackingCache.days[dayIndex];

        const dailyAggregate = cloneDeep(originalDailyAggregate);

        const includeWearables = false;
        const totalProgramPoints = [];

        paths.forEach((path: string) => {
            const percentages: Array<number> = [];
            const parts = path.split('.');

            // Aggregate athlete docs given path
            aggregateGroupHistoryAthleteDocs(athleteAggregateDocs, dayIndex, parts, percentages, totalProgramPoints, includeWearables, group);

            const includedAthletes = group.athletes.filter((ath) => ath.includeGroupAggregation)
            if (athleteAggregateDocs.length < includedAthletes.length) {
                for (let i = athleteAggregateDocs.length; i < includedAthletes.length; i++) {
                    percentages.push(0);
                }
            }

            if (percentages.length) {

                // Update the group daily from the group weekly doc
                updateGroupDaily(parts, percentages, totalProgramPoints, dailyAggregate, includeWearables);

                dailyAggregate.value = calculateIpScoreFromAggregateComponentInterface(dailyAggregate); // 0.011904761904761904
                aggregateDoc.trackingCache.days[dayIndex] = dailyAggregate;

                const existingValues = cloneDeep(aggregateDoc.values);

                const startIndex = getStartIndex(dayIndex, group.creationTimestamp, directive.utcOffset, true);

                existingValues[0] = calculateIpScoreFromDailies(aggregateDoc.trackingCache.days, dayIndex, startIndex);
                aggregateDoc.values = existingValues;

                const existingComponents = cloneDeep(aggregateDoc.componentValues);

                recordHistoryIpScoreComponentPercentagesFromDailies(existingComponents[0], aggregateDoc.trackingCache.days, dayIndex, startIndex, false);

                aggregateDoc.componentValues = existingComponents;

                updateRecentEntries(existingRecentEntries, aggregateDoc, dayIndex, dailyAggregate);

            } else {

                LogInfo(`IpScore Group Monthly History percentages.length === 0 at dayIndex ${dayIndex}.`);
            }
        });

        aggregateDoc.trackingCache.days[dayIndex] = dailyAggregate;
        aggregateDoc.lastAggregateDate = firestore.Timestamp.now();
    } else {
        console.warn('NO ATHLETES FOUND FOR AGGREGATION')
    }

    return aggregateDoc;
};


