import { GroupIpScoreMonthlyHistoryAggregateInterface } from '../../../models/interfaces/group-ipscore-monthly-history-aggregate';
import { IpScoreAggregateComponentInterface } from '../../../models/interfaces/ipscore-component-aggregate';

export const updateRecentEntries = (existingRecentEntries: IpScoreAggregateComponentInterface[], aggregateDoc: GroupIpScoreMonthlyHistoryAggregateInterface, dayIndex: number, dailyAggregate: IpScoreAggregateComponentInterface) => {
    const ln = existingRecentEntries.length;
    if (aggregateDoc.trackingCache.dayIndex !== dayIndex) {
        if (ln > 10) {
            existingRecentEntries.pop();
        }
        aggregateDoc.trackingCache.dayIndex = dayIndex;
        existingRecentEntries.unshift(dailyAggregate);
    }
    else {
        // Reset first position based on daily in tracking cache
        if (ln > 0) {
            existingRecentEntries[0] = dailyAggregate;
        }
        else {
            existingRecentEntries.push(dailyAggregate);
        }
    }
    aggregateDoc.recentEntries = existingRecentEntries;
};
