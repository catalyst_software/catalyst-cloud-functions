import { cloneDeep, isArray, has } from 'lodash'
import { firestore } from 'firebase-admin'

import {
    updateIpScoreConstituent,
    calculateIpScoreFromDailies,
    recordHistoryIpScoreComponentPercentagesFromDailies,
} from './utils/index'
import { calculateIpScoreFromAggregateComponentInterface } from './utils/calculateIpScoreFromAggregateComponentInterface'
import { createAggregateComponentInterface } from './utils/createAggregateComponentInterface'

import { LogInfo } from '../../../../../shared/logger/logger'
import { getDayIndex, getMonthNumber } from '../../../../../shared/utils/moment'
import { Athlete } from '../../../../../models/athlete/interfaces/athlete'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { AthleteIpScoreMonthlyHistoryAggregateInterface } from '../../../models/interfaces/athlete-ipscore-monthly-history-aggregate'
import { getStartIndex } from './utils/getStartIndex'
import { validateTrackingCache } from './validateTrackingCache'
import { validateComponentValuesLength } from './validateComponentValuesLength'
import { IpScoreAggregateComponentInterface } from '../../../models/interfaces/ipscore-component-aggregate'

export const aggregateAthleteIpScoreMonthlyHistory = (
    directive: AggregateIpScoreDirective
): AthleteIpScoreMonthlyHistoryAggregateInterface => {
    debugger
    const athlete: Athlete = directive.athlete
    const aggregateDoc: AthleteIpScoreMonthlyHistoryAggregateInterface = directive.aggregateDocument as AthleteIpScoreMonthlyHistoryAggregateInterface

    const dayIndex = getDayIndex(
        firestore.Timestamp.now(),
        directive.utcOffset || 0,
        true
    )
    const monthNumber = getMonthNumber(
        athlete.metadata.creationTimestamp,
        athlete.currentIpScoreTracking.dateTime,
        directive.utcOffset
    )

    const config = athlete.ipScoreConfiguration
    const currentTracking = athlete.currentIpScoreTracking

    const paths = currentTracking.directive.paths || []

    // console.log('=============>>>>>>>>>>>> Processing Athlete IpScore Monthly History Aggregate', aggregateDoc)

    LogInfo(
        `Processing AthleteIpScoreMonthlyHistoryAggregateInterface aggregate document in IpScore aggregator.`,
        {
            AggregateDocument: aggregateDoc,
            Athlete: athlete,
            MonthNumber: monthNumber,
            DayIndex: dayIndex,
            ExecuteDirective: athlete.currentIpScoreTracking.directive,
        }
    )

    validateTrackingCache(aggregateDoc, monthNumber)

    if (!isArray(aggregateDoc.recentEntries)) {
        aggregateDoc.recentEntries = []
    }
    const existingRecentEntries = cloneDeep(aggregateDoc.recentEntries)

    // Array of current week IP Score aggregate component data structures
    // On daily basis
    validateTrackingCacheDays(aggregateDoc, dayIndex, existingRecentEntries)

    // Array of week IP Score (numbers)
    validateValuesLength(aggregateDoc, monthNumber)

    // Array of historical week IP Score aggregate component data structures
    // Representing week percentages across all lifestyle entry types
    validateComponentValuesLength(aggregateDoc, monthNumber)

    const dailyAggregate = aggregateDoc.trackingCache.days[dayIndex]
    const includeWearables =
        config.quantitativeWellness.body.wearables.include || false

    paths.forEach((path: string) => {
        let realized = 0
        let expected = 0

        const parts = path.split('.')
        switch (parts[1].toLowerCase()) {
            case 'brain':
                switch (parts[2].toLowerCase()) {
                    case 'mood':
                        realized =
                            currentTracking.quantitativeWellness.brain.mood
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.brain.mood
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.brain.mood
                        )
                        break
                    case 'sleep':
                        // console.warn('currentTracking.quantitativeWellness.brain.sleep', currentTracking.quantitativeWellness.brain.sleep)
                        // console.warn('dailyAggregate.brain.sleep', dailyAggregate.brain.sleep)
                        realized =
                            currentTracking.quantitativeWellness.brain.sleep
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.brain.sleep
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.brain.sleep
                        )
                        break
                }
                break
            case 'body':
                switch (parts[2].toLowerCase()) {
                    case 'fatigue':
                        realized =
                            currentTracking.quantitativeWellness.body.fatigue
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.body.fatigue
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.body.fatigue
                        )
                        break
                    case 'pain':
                        realized =
                            currentTracking.quantitativeWellness.body.pain
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.body.pain
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.body.pain
                        )
                        break
                    case 'weight':
                        realized =
                            currentTracking.quantitativeWellness.body.weight
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.body.weight
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.body.weight
                        )
                        break
                    case 'wearables':
                        if (includeWearables) {
                            realized =
                                currentTracking.quantitativeWellness.body
                                    .wearables.recordedDailyEntries
                            expected =
                                config.quantitativeWellness.body.wearables
                                    .expectedDailyEntries
                            updateIpScoreConstituent(
                                realized,
                                expected,
                                dailyAggregate.body.wearables
                            )
                        }
                        break
                }
                break
            case 'simple-food':
            case 'food':
                realized =
                    currentTracking.quantitativeWellness.food.components
                        .recordedDailyEntries
                expected =
                    config.quantitativeWellness.food.components
                        .expectedDailyEntries
                updateIpScoreConstituent(
                    realized,
                    expected,
                    dailyAggregate.food.components
                )
                break
            case 'training':
                realized =
                    currentTracking.quantitativeWellness.training.components
                        .recordedDailyEntries
                expected =
                    config.quantitativeWellness.training.components
                        .expectedDailyEntries
                updateIpScoreConstituent(
                    realized,
                    expected,
                    dailyAggregate.training.components
                )
                break
            case 'programs':
                // dailyAggregate.programs.components.realized++;
                switch (parts[2].toLowerCase()) {
                    case 'video':
                        dailyAggregate.programs.videoConsumed =
                            currentTracking.programs.videoConsumed
                        break
                    case 'survey':
                        dailyAggregate.programs.surveyConsumed =
                            currentTracking.programs.surveyConsumed
                        break
                    case 'worksheet':
                        dailyAggregate.programs.worksheetConsumed =
                            currentTracking.programs.worksheetConsumed
                        break
                }
                if (has(currentTracking.programs, 'expectedDailyEntries')) {
                    realized = currentTracking.programs.recordedDailyEntries
                    expected = currentTracking.programs.expectedDailyEntries
                    updateIpScoreConstituent(
                        realized,
                        expected,
                        dailyAggregate.programs
                    )
                }
                break
        }
    })

    const { isPenalized, maxScorePossible } = currentTracking.directive

    dailyAggregate.value = calculateIpScoreFromAggregateComponentInterface(
        dailyAggregate,
        includeWearables,
        isPenalized,
        maxScorePossible
    )

    const startIndex = getStartIndex(
        dayIndex,
        athlete.metadata.creationTimestamp,
        directive.utcOffset,
        true
    )

    aggregateDoc.values[0] = calculateIpScoreFromDailies(
        aggregateDoc.trackingCache.days,
        dayIndex,
        startIndex
    )
    recordHistoryIpScoreComponentPercentagesFromDailies(
        aggregateDoc.componentValues[0],
        aggregateDoc.trackingCache.days,
        dayIndex,
        startIndex,
        includeWearables
    )

    updateRecentEntries(
        existingRecentEntries,
        aggregateDoc,
        dayIndex,
        dailyAggregate
    )
    aggregateDoc.lastAggregateDate = firestore.Timestamp.now()

    // console.warn('=============>>>>>>>>>>>> Updated  AthleteIpScoreMonthlyHistoryAggregate', aggregateDoc)
    return aggregateDoc
}

function updateRecentEntries(
    existingRecentEntries: IpScoreAggregateComponentInterface[],
    aggregateDoc: AthleteIpScoreMonthlyHistoryAggregateInterface,
    dayIndex: number,
    dailyAggregate: IpScoreAggregateComponentInterface
) {
    const ln = existingRecentEntries.length
    if (aggregateDoc.trackingCache.dayIndex !== dayIndex) {
        if (ln > 10) {
            existingRecentEntries.pop()
        }
        aggregateDoc.trackingCache.dayIndex = dayIndex
        existingRecentEntries.unshift(dailyAggregate)
    } else {
        // Reset first position based on daily in tracking cache
        if (ln > 0) {
            existingRecentEntries[0] = dailyAggregate
        } else {
            existingRecentEntries.push(dailyAggregate)
        }
    }
    aggregateDoc.recentEntries = existingRecentEntries
}
function validateValuesLength(
    aggregateDoc: AthleteIpScoreMonthlyHistoryAggregateInterface,
    monthNumber: number
) {
    if (!isArray(aggregateDoc.values)) {
        aggregateDoc.values = []
    }
    if (aggregateDoc.values.length <= monthNumber) {
        const existingValues = cloneDeep(aggregateDoc.values)
        for (let i = aggregateDoc.values.length; i <= monthNumber; i++) {
            existingValues.unshift(0)
        }
        aggregateDoc.values = existingValues
    }
}

function validateTrackingCacheDays(
    aggregateDoc: AthleteIpScoreMonthlyHistoryAggregateInterface,
    dayIndex: number,
    existingRecentEntries: IpScoreAggregateComponentInterface[]
) {
    if (aggregateDoc.trackingCache.days.length <= dayIndex) {
        const existingDays = cloneDeep(aggregateDoc.trackingCache.days)
        for (
            let i = aggregateDoc.trackingCache.days.length;
            i <= dayIndex;
            i++
        ) {
            const component = createAggregateComponentInterface()
            existingDays.push(component)
            if (existingRecentEntries.length >= 10) {
                existingRecentEntries.pop()
            }
            aggregateDoc.trackingCache.dayIndex = i
            existingRecentEntries.unshift(component)
        }
        aggregateDoc.trackingCache.days = existingDays
    }
}
