import { GroupIpScoreMonthlyHistoryAggregateInterface } from '../../../models/interfaces/group-ipscore-monthly-history-aggregate';
import { IpScoreAggregateTrackingCacheInterface } from '../../../models/interfaces/ipscore-aggregate-tracking-cache';
export const validateTrackingCache = (aggregateDoc: GroupIpScoreMonthlyHistoryAggregateInterface, index: number) => {

    if (!aggregateDoc.trackingCache) {
        // Initialize tracking cache
        aggregateDoc.trackingCache = {
            index: 0,
            days: []
        } as IpScoreAggregateTrackingCacheInterface
    }
    
    if (aggregateDoc.trackingCache.index !== index) {
        aggregateDoc.trackingCache.index = index;
        // TODO: Needed here?
        // aggregateDoc.trackingCache.dayIndex = -1;
        aggregateDoc.trackingCache.days = [];
    }
    
};
