import { cloneDeep, has } from 'lodash'

import { calculateIpScoreFromAggregateComponentInterface } from './utils/calculateIpScoreFromAggregateComponentInterface'
import { LogInfo } from '../../../../../shared/logger/logger'
import { calculateIpScoreFromDailies, isNumber } from './utils'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { AthleteIpScoreWeeklyAggregateInterface } from '../../../models/interfaces'
import { AthleteIpScoreDailyAggregate } from '../../../models/interfaces/athlete-ipscore-daily-aggregate'
import { updateIpScoreConstituent } from './utils/index'
import { getDayIndex } from '../../../../../shared/utils/moment/index'
import { getStartIndex } from './utils/getStartIndex'

export const aggregateAthleteIpScoreWeekly = (
    directive: AggregateIpScoreDirective
): AthleteIpScoreWeeklyAggregateInterface => {
    const { athlete } = directive

    debugger
    const theDateTime = directive.athlete.currentIpScoreTracking.dateTime

    console.warn(theDateTime.toDate())
    let aggregateDoc: AthleteIpScoreWeeklyAggregateInterface = directive.aggregateDocument as AthleteIpScoreWeeklyAggregateInterface
    const dayIndex = getDayIndex(
        athlete.currentIpScoreTracking.dateTime,
        isNumber(directive.utcOffset)
            ? directive.utcOffset === 0
                ? 0
                : directive.utcOffset
            : 10 // Brisbane (Australia - Queensland)
    )

    console.warn(dayIndex)
    const config = athlete.ipScoreConfiguration
    const currentTracking = athlete.currentIpScoreTracking

    const paths = currentTracking.directive.paths || []

    // console.log('aggregateDoc - BEFORE', aggregateDoc)
    console.warn(
        `Processing AthleteIpScoreWeekly aggregate document in IpScore aggregator.`,
        {
            AggregateDocument: {
                ...aggregateDoc,
                endDate: aggregateDoc.endDate
                    ? aggregateDoc.endDate.toDate()
                    : 'UNKNOWN',
                startDate: aggregateDoc.startDate
                    ? aggregateDoc.startDate.toDate()
                    : 'UNKNOWN',
                // endDate: aggregateDoc.endDate.toDate(),
            },
            Dailies: aggregateDoc.dailies,
            Athlete: {
                ...athlete,
                currentIpScoreTracking: {
                    ...athlete.currentIpScoreTracking,
                    dateTime: athlete.currentIpScoreTracking.dateTime.toDate(),
                },
            },
            DayIndex: dayIndex,
            // ExecuteDirective: athlete.currentIpScoreTracking.directive
        }
    )

    console.log(
        `Daily Aggregate Doc Value at index ${dayIndex} BEFORE aggreagete.`,
        aggregateDoc.dailies[dayIndex].value
    )

    // console.log(`Daily Aggregate Doc Value at index ${dayIndex} BEFORE aggreagete.`, aggregateDoc.dailies[dayIndex].value);

    // console.warn(`Processing AthleteIpScoreWeekly aggregate document in IpScore aggregator.`, {
    //     AggregateDocument: aggregateDoc,
    //     Athlete: athlete,
    //     DayIndex: dayIndex,
    //     // ExecuteDirective: athlete.currentIpScoreTracking.directive
    // });

    console.log(`Directive`, athlete.currentIpScoreTracking.directive)
    const includeWearables =
        config.quantitativeWellness.body.wearables.include || false

    const originalDailyAggregate: AthleteIpScoreDailyAggregate =
        aggregateDoc.dailies[dayIndex]

    const dailyAggregate: AthleteIpScoreDailyAggregate = cloneDeep(
        originalDailyAggregate
    )
    console.warn(
        '=====>>>>>>> dailyAggregate INITIAL value',
        dailyAggregate.value
    )
    paths.forEach((path: string) => {
        let realized = 0
        let expected = 0

        const parts = path.split('.')
        switch (parts[1].toLowerCase()) {
            case 'brain':
                switch (parts[2].toLowerCase()) {
                    case 'mood':
                        realized =
                            currentTracking.quantitativeWellness.brain.mood
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.brain.mood
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.brain.mood
                        )
                        break
                    case 'sleep':
                        // console.warn('currentTracking.quantitativeWellness.brain.sleep', currentTracking.quantitativeWellness.brain.sleep)
                        // console.warn('dailyAggregate.brain.sleep', dailyAggregate.brain.sleep)
                        realized =
                            currentTracking.quantitativeWellness.brain.sleep
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.brain.sleep
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.brain.sleep
                        )
                        break
                }
                break
            case 'body':
                switch (parts[2].toLowerCase()) {
                    case 'fatigue':
                        realized =
                            currentTracking.quantitativeWellness.body.fatigue
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.body.fatigue
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.body.fatigue
                        )
                        break
                    case 'pain':
                        realized =
                            currentTracking.quantitativeWellness.body.pain
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.body.pain
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.body.pain
                        )
                        break
                    case 'weight':
                        realized =
                            currentTracking.quantitativeWellness.body.weight
                                .recordedDailyEntries
                        expected =
                            config.quantitativeWellness.body.weight
                                .expectedDailyEntries
                        updateIpScoreConstituent(
                            realized,
                            expected,
                            dailyAggregate.body.weight
                        )
                        break
                    case 'wearables':
                        if (includeWearables) {
                            realized =
                                currentTracking.quantitativeWellness.body
                                    .wearables.recordedDailyEntries
                            expected =
                                config.quantitativeWellness.body.wearables
                                    .expectedDailyEntries
                            updateIpScoreConstituent(
                                realized,
                                expected,
                                dailyAggregate.body.wearables
                            )
                        }
                        break
                }
                break
            case 'food':
                realized =
                    currentTracking.quantitativeWellness.food.components
                        .recordedDailyEntries
                expected =
                    config.quantitativeWellness.food.components
                        .expectedDailyEntries
                updateIpScoreConstituent(
                    realized,
                    expected,
                    dailyAggregate.food.components
                )
                break
            case 'simple-food':
                realized =
                    currentTracking.quantitativeWellness.food.components
                        .recordedDailyEntries
                expected = 3
                updateIpScoreConstituent(
                    realized,
                    expected,
                    dailyAggregate.food.components
                )
                break
            case 'training':
                realized =
                    currentTracking.quantitativeWellness.training.components
                        .recordedDailyEntries
                expected =
                    config.quantitativeWellness.training.components
                        .expectedDailyEntries
                updateIpScoreConstituent(
                    realized,
                    expected,
                    dailyAggregate.training.components
                )
                break
            case 'programs':
                // dailyAggregate.programs.components.realized++;
                switch (parts[2].toLowerCase()) {
                    case 'video':
                        dailyAggregate.programs.videoConsumed =
                            currentTracking.programs.videoConsumed
                        break
                    case 'survey':
                        dailyAggregate.programs.surveyConsumed =
                            currentTracking.programs.surveyConsumed
                        break
                    case 'worksheet':
                        dailyAggregate.programs.worksheetConsumed =
                            currentTracking.programs.worksheetConsumed
                        break
                }
                if (has(currentTracking.programs, 'expectedDailyEntries')) {
                    realized = currentTracking.programs.recordedDailyEntries
                    expected = currentTracking.programs.expectedDailyEntries
                    updateIpScoreConstituent(
                        realized,
                        expected,
                        dailyAggregate.programs
                    )
                }
                break
        }
    })

    const { isPenalized, maxScorePossible } = currentTracking.directive

    const currentIPScoreValue = dailyAggregate.value
    dailyAggregate.value = calculateIpScoreFromAggregateComponentInterface(
        dailyAggregate,
        includeWearables,
        isPenalized,
        maxScorePossible
    )

    const startIndex = getStartIndex(
        dayIndex,
        athlete.metadata.creationTimestamp,
        directive.utcOffset,
        false
    )

    aggregateDoc.dailies[dayIndex] = dailyAggregate
    let aggregateDocVSalue = calculateIpScoreFromDailies(
        aggregateDoc.dailies,
        dayIndex,
        startIndex
    )
    aggregateDoc = (aggregateDoc as any).with({
        value: aggregateDocVSalue,
    })
    athlete.currentIpScoreTracking.ipScore = dailyAggregate.value

    const ipScoreIncrease = dailyAggregate.value - currentIPScoreValue

    if (!isNumber(athlete.runningTotalIpScore)) {
        athlete.runningTotalIpScore = 0

        console.warn(
            'ipScore BEFORE set  NOW A NUMBER (WAS NaN)',
            athlete.runningTotalIpScore
        )
    }

    athlete.runningTotalIpScore += ipScoreIncrease

    console.log(
        `Daily Aggregate Doc Value at index ${dayIndex} AFTER aggreagete.`,
        aggregateDoc.dailies[dayIndex].value
    )

    console.log('aggregateDoc - AFTER', aggregateDoc)

    return aggregateDoc
}
