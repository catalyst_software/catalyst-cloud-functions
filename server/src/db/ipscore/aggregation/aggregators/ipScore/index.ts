
import { aggregateAthleteIpScoreWeekly } from './aggregate-athlete-ipscore-weekly';
import { aggregateAthleteIpScoreWeeklyHistory } from './aggregate-athlete-ipscore-weekly.history';
import { aggregateAthleteIpScoreMonthlyHistory } from './aggregate-athlete-ipscore-monthly.history';
import { aggregateGroupIpScoreWeekly } from './aggregate-group-ipscore-weekly';
// import { aggregateGroupIpScoreWeeklyHistory } from './aggregate-group-ipscore-weekly.history';
import { aggregateGroupIpScoreMonthlyHistory } from './aggregate-group-ipscore-monthly.history';
import { CalculateIpScoreAggregates } from '../../interfaces/calculate-ipscore-aggregates';
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive';
import { AthleteIpScoreWeeklyAggregateInterface, GroupIpScoreWeeklyAggregateInterface } from '../../../models/interfaces';
import { AthleteIpScoreWeeklyHistoryAggregateInterface } from '../../../models/interfaces/athlete-ipscore-weekly-history-aggregate';
import { AthleteIpScoreMonthlyHistoryAggregateInterface } from '../../../models/interfaces/athlete-ipscore-monthly-history-aggregate';
import { GroupIpScoreWeeklyHistoryAggregateInterface } from '../../../models/interfaces/group-ipscore-weekly-history-aggregate';
import { GroupIpScoreMonthlyHistoryAggregateInterface } from '../../../models/interfaces/group-ipscore-monthly-history-aggregate';
import { aggregateGroupIpScoreWeeklyHistory } from './aggregate-group-ipscore-weekly.history';


export class IpScoreAggregator implements CalculateIpScoreAggregates {

    // constructor(private props: {
    //     // entry: AthleteBiometricEntry
    //     // aggregateDoc: AthleteWeeklyAggregate
    // }) { }


    // Athlete Weekly
    public processAthleteIpScoreWeekly(directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyAggregateInterface {
        return aggregateAthleteIpScoreWeekly(directive);
    }

    public processAthleteIpScoreWeeklyHistory(directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyHistoryAggregateInterface {
        return aggregateAthleteIpScoreWeeklyHistory(directive);
    }

    public processAthleteIpScoreMonthlyHistory(directive: AggregateIpScoreDirective
    ): AthleteIpScoreMonthlyHistoryAggregateInterface {
        return aggregateAthleteIpScoreMonthlyHistory(directive);
    }

    // Group Weekly
    public processGroupIpScoreWeekly(directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyAggregateInterface {
        debugger;
        return aggregateGroupIpScoreWeekly(directive);
    }

    public processGroupIpScoreWeeklyHistory(directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyHistoryAggregateInterface {
        debugger;
        return aggregateGroupIpScoreWeeklyHistory(directive);
    }

    public processGroupIpScoreMonthlyHistory(directive: AggregateIpScoreDirective
    ): GroupIpScoreMonthlyHistoryAggregateInterface {
        debugger;
        return aggregateGroupIpScoreMonthlyHistory(directive);
    }
}
