import { AthleteIpScoreWeeklyHistoryAggregateInterface } from '../../../models/interfaces/athlete-ipscore-weekly-history-aggregate';
import { IpScoreAggregateTrackingCacheInterface } from '../../../models/interfaces/ipscore-aggregate-tracking-cache';
export const validateWeeklyTrackingCache = (aggregateDoc: AthleteIpScoreWeeklyHistoryAggregateInterface, index: number) => {
    if (!aggregateDoc.trackingCache) {
        aggregateDoc.trackingCache = {
            index: 0,
            days: []
        } as IpScoreAggregateTrackingCacheInterface;
    }
    if (aggregateDoc.trackingCache.index !== index) {
        aggregateDoc.trackingCache.index = index;
        aggregateDoc.trackingCache.dayIndex = -1;
        aggregateDoc.trackingCache.days = [];
    }
};
