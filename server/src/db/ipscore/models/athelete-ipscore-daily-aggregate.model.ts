import { firestore } from 'firebase-admin'

import { AthleteIpScoreDailyAggregate } from './interfaces/athlete-ipscore-daily-aggregate'

import { HealthComponentType } from '../../biometricEntries/well/enums/health-component-type'
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type'
import { IpScoreAggregateModel } from './ipscore-aggregate.model'
import { IpScoreBrainConstituent } from './interfaces/ipscore-brain-constituent.interface'
import { IpScoreBodyConstituent } from './interfaces/ipscore-body-constituent.interface'
import { IpScoreFoodConstituent } from './interfaces/ipscore-food-constituent.interface'
import { IpScoreTrainingConstituent } from './interfaces/ipscore-training-constituent.interface'
import { IpScoreProgramsConstituent } from './interfaces/ipscore-programs-constituent.interface'

export class AthleteIpScoreDailyAggregateModel extends IpScoreAggregateModel
    implements AthleteIpScoreDailyAggregate {
    userId?: string;
    organizationId: string;
    fullName?: string;
    dateTime?: firestore.Timestamp;
    value?: number;
    brain?: IpScoreBrainConstituent;
    body?: IpScoreBodyConstituent;
    food?: IpScoreFoodConstituent;
    training?: IpScoreTrainingConstituent;
    programs: IpScoreProgramsConstituent;

    constructor(
        uid?: string,
        userId?: string,
        fullName?: string,
        dateTime?: firestore.Timestamp,
        organizationId?: string,
        healthComponentType?: HealthComponentType,
        lifestyleEntryType?: LifestyleEntryType,
        value?: number,
        brain?: IpScoreBrainConstituent,
        body?: IpScoreBodyConstituent,
        food?: IpScoreFoodConstituent,
        training?: IpScoreTrainingConstituent,
        programs?: IpScoreProgramsConstituent
    ) {
        super();

        this.uid = uid;
        this.userId = userId;
        this.fullName = fullName;
        this.dateTime = dateTime;
        this.organizationId = organizationId;
        this.healthComponentType = healthComponentType;
        this.lifestyleEntryType = lifestyleEntryType;

        this.value = value;
        this.brain = brain;
        this.body = body;
        this.food = food;
        this.training = training;
        this.programs = programs
    }
}
