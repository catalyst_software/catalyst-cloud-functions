// tslint:disable-next-line:max-line-length
import { HealthComponentType } from '../../biometricEntries/well/enums/health-component-type';
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type';
import { AggregateDocumentType } from '../../biometricEntries/well/enums/aggregate-document-type';

import { IpScoreHistoryAggregate } from './interfaces/ipscore-history-aggregate';
import { IpScoreAggregateTrackingCacheInterface } from './interfaces/ipscore-aggregate-tracking-cache';
import { IpScoreAggregateComponentInterface } from './interfaces/ipscore-component-aggregate';
import { IpScoreAggregateHistoryCollection } from '../enums/firestore-ipscore-aggregate-history-collection';
import { AggregateEntityType } from '../../biometricEntries/enums/aggregate-entity-type';

export class IpScoreAggregateHistoryModel implements IpScoreHistoryAggregate {
    
    uid?: string;
    guid?: string;

    entityId: string;
    entityName: string;

    collection?: IpScoreAggregateHistoryCollection;
    
    organizationId: string;
    
    aggregateDocumentType?: AggregateDocumentType;
    aggregateEntityType?: AggregateEntityType;

    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    values?: Array<number>;
    componentValues?: Array<IpScoreAggregateComponentInterface>;
    trackingCache?: IpScoreAggregateTrackingCacheInterface;
}
