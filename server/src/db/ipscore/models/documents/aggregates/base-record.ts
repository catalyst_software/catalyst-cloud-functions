import { Map as ImmutableMap } from 'immutable';
import _ = require('lodash');

export abstract class BaseRecord {
    // TODO: private 
    _data: ImmutableMap<any, any> = ImmutableMap<any, any>();

    data() {
        return this._data;
    }

    constructor(initialValues?: any) {

        if (initialValues) {

            if (initialValues.entry && !initialValues.entry.athleteCreationTimestamp) {
                initialValues.entry.athleteCreationTimestamp = initialValues.entry.creationTimestamp
            }

            this._data = this._data.merge(initialValues);
            _.forEach(initialValues, (value, key) => {
                Object.defineProperty(this, key, {
                    enumerable: false,
                    get() {
                        const data = this._data.get(key);
                        return data;
                    },
                    set() {
                        throw new Error('Cannot set on an immutable record.');
                    }
                });
            });
        } else {
            this._data = ImmutableMap<any, any>();
        }
    }
    get entry() {
        return this._data.get('entry');
    }
    toJS(): any {
        const json = this._data.delete('_data').toJSON();
        return json;
    }
    equals(otherRecord: BaseRecord) {
        return typeof this === typeof otherRecord &&
            this._data.equals(otherRecord._data);
    }
    with?(values: any): any {
        // const returnVal = new (this.constructor as any);
        // returnVal._data = this._data.merge(values);

        debugger;
        const returnVal = new (this.constructor as any)({ ...this._data.merge(values).toJS() });

        return returnVal;
    }

}
