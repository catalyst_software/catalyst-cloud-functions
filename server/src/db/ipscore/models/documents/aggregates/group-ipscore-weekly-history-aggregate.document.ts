import { firestore } from 'firebase-admin'

import { IpScoreAggregateHistoryRecord } from './ipscore-aggregate-history-record'
import { IpScoreAggregateHistoryCollection } from '../../../enums/firestore-ipscore-aggregate-history-collection'
import { GroupIpScoreWeeklyHistoryAggregateInterface } from '../../interfaces/group-ipscore-weekly-history-aggregate'

import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { AggregateDocumentType } from '../../../../biometricEntries/well/enums/aggregate-document-type';
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type';
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { IpScoreAggregateComponentInterface } from '../../interfaces/ipscore-component-aggregate';
import { IpScoreAggregateTrackingCacheInterface } from '../../interfaces/ipscore-aggregate-tracking-cache';
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type';

/**
 * Default Values when instanciating a new Group Biometric Weekly Aggregate
 */

const _defaultValues: GroupIpScoreWeeklyHistoryAggregateInterface = {

    aggregateEntityType: AggregateEntityType.Group,
    collection: IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory,

    weekNumber: 0,
    startDate: firestore.Timestamp.now(),
    endDate: firestore.Timestamp.now(),

    entityId: undefined,
    entityName: undefined,

    uid: undefined,

    aggregateDocumentType: AggregateDocumentType.WeeklyHistory,

    healthComponentType: HealthComponentType.IpScore,
    lastAggregateDate: firestore.Timestamp.now(),
    lifestyleEntryType: LifestyleEntryType.IpScore,
    organizationId: undefined,

    componentValues: [],

    trackingCache: {
        index: 0,
        days: []
    } as IpScoreAggregateTrackingCacheInterface,

    values: []
};

export class GroupIpScoreWeeklyHistoryAggregate extends IpScoreAggregateHistoryRecord {

    public readonly uid?: string;
    public readonly guid?: string;

    public readonly entityId: string;
    public readonly entityName: string;

    public readonly collection?: IpScoreAggregateHistoryCollection;

    public readonly historicalValues?: Array<any>;

    groupId?: string
    groupName?: string

    startDate?: firestore.Timestamp
    endDate?: firestore.Timestamp

    lifestyleEntryType?: LifestyleEntryType;
    healthComponentType?: HealthComponentType;
    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;

    organizationId: string;

    weekNumber?: number;

    componentValues?: Array<IpScoreAggregateComponentInterface>;
    trackingCache?: IpScoreAggregateTrackingCacheInterface;

    lastAggregateDate?: firestore.Timestamp;

    values?: Array<number>

    constructor(params?: GroupIpScoreWeeklyHistoryAggregateInterface) {
        debugger;
        if (params) {
            super({
                ..._defaultValues,
                ...params,
            } as GroupIpScoreWeeklyHistoryAggregateInterface)
        } else {
            super(_defaultValues)
        }
    }

    async calculateAggregates(
        directive: AggregateIpScoreDirective
    ): Promise<AggregateIpScoreDirective> {
        debugger;
        // const savedDoc = await saveAggregateDoc(
        //     this.aggregator.processGroupIpScoreWeeklyHistory(directive),
        //     // aggregator.processGroupIpScoreWeeklyHistory(directive),
        //     directive
        // );

        const savedDoc = this.aggregator.processGroupIpScoreWeeklyHistory(directive);
        debugger;
        return {
            ...directive,
            message: `Updated ${directive.collection} Docs.`,
            updatedDocument: savedDoc,
        }
    }
}
