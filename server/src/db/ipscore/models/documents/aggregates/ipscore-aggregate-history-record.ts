import { firestore } from 'firebase-admin';

import { BaseRecord } from '../../../../biometricEntries/well/documents/base-record'
import { IpScoreAggregateHistoryCollection } from '../../../enums/firestore-ipscore-aggregate-history-collection'
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type'
import { AggregateDocumentType } from '../../../../biometricEntries/well/enums/aggregate-document-type'
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type'
import { IpScoreAggregateInterface } from '../../interfaces'
import { IpScoreAggregateComponentInterface } from '../../interfaces/ipscore-component-aggregate';
import { AthleteBiometricEntryInterface } from '../../../../biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { IpScoreAggregateTrackingCacheInterface } from '../../interfaces/ipscore-aggregate-tracking-cache';
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type';
import { DynamicAggregator } from '../../../../biometricEntries/aggregation/dynamic-aggregator';
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive';

// import { BiometricHistoryAggregate as IpScoreAggregateHistoryInterface } from '../../../../../../models/well/aggregates/interfaces/biometric-history-aggregate';

/**
 * Interface which defines the types passed into the constructor.
 */
const _defaultValues: IpScoreAggregateInterface = {
    organizationId: undefined,
    uid: undefined,

    entityId: undefined,
    entityName: undefined,
};
export class IpScoreAggregateHistoryRecord extends BaseRecord
    implements IpScoreAggregateInterface {

    public readonly uid?: string;
    public readonly guid?: string;

    // public readonly entry: AthleteBiometricEntryInterface;

    public readonly entityId: string;
    public readonly entityName: string;

    organizationId: string;

    // TODO:
    // public readonly collection?: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection;
    // collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection;
    collection?: IpScoreAggregateHistoryCollection;

    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;
    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    monthNumber?: number;

    componentValues?: Array<IpScoreAggregateComponentInterface>;
    recentEntries?: Array<IpScoreAggregateComponentInterface>
    // recentEntries: Array<AthleteBiometricEntry>;
    trackingCache?: IpScoreAggregateTrackingCacheInterface;

    values?: Array<number>

    // value?: number;

    lastAggregateDate?: firestore.Timestamp;


    // trackingCache: {
    //     index: number;
    //     valueCache: Array<number>;
    //     componentCache: BiometricAggregateComponentHistoryInterface;
    // };

    aggregator: DynamicAggregator;


    constructor(params?: IpScoreAggregateInterface) {
        if (params) {
            super({
                ..._defaultValues,
                ...params,
            } as AthleteBiometricEntryInterface)
        } else {
            super(_defaultValues)
        }

        debugger;
        // this.aggregator = new DynamicAggregator(LifestyleEntryType.IpScore);
        // if (!this.aggregator) {

        this.aggregator = new DynamicAggregator(LifestyleEntryType[LifestyleEntryType.IpScore]);
        // }

    }

    /**
     * return a new version of the BiometricAggregateModel with updated values as specified in the updatedValues parameter
     * @param updatedValues The new values for various members of the BiometricAggregateModel class.
     * @returns a new instance of a BiometricAggregateModel class with updated values
     */
    // with?(updatedValues: BiometricAggregateInterface): BiometricAggregateInterface {
    //     return super.with(updatedValues) as BiometricAggregateInterface;
    // }

    async calculateAggregates(
        directive: AggregateIpScoreDirective
    ): Promise<AggregateIpScoreDirective> {
        // const savedDoc = await saveAggregateDoc(
        //     this.aggregator.processAthleteIpScoreWeeklyHistory(directive),
        //     directive
        // );
        // debugger;
        return {
            ...directive,
            message: `Updated ${directive.collection} Doc.`,
            aggregateDocument: directive.aggregateDocument,
        }
    }
}
