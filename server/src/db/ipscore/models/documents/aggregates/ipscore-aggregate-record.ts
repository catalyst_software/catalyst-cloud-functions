import { firestore } from 'firebase-admin'

import { BaseRecord } from './base-record'
import { IpScoreAggregateInterface } from '../../interfaces'
import { AggregateDocumentType } from '../../../../biometricEntries/well/enums/aggregate-document-type'
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type'
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type'
import { IpScoreAggregateCollection } from '../../../enums/firestore-ipscore-aggregate-collection'
import { IpScoreAggregateHistoryCollection } from '../../../enums/firestore-ipscore-aggregate-history-collection'
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type'
import { DynamicAggregator } from '../../../../biometricEntries/aggregation/dynamic-aggregator'

/**
 * Interface which defines the types passed into the constructor.
 */
const _defaultValues: IpScoreAggregateInterface = {
    entityId: undefined,
    entityName: undefined,

    uid: '',
    organizationId: undefined,
    value: 0
};

export class IpScoreAggregateRecord extends BaseRecord
    implements IpScoreAggregateInterface {
    organizationId: string;
    public readonly uid: string;

    public readonly guid: string;
    public readonly creationTimestamp: firestore.Timestamp;

    public readonly entityId: string;
    public readonly entityName: string;

    public readonly collection?:
        | IpScoreAggregateCollection
        | IpScoreAggregateHistoryCollection;

    public readonly aggregateDocumentType?: AggregateDocumentType;
    public readonly aggregateEntityType?: AggregateEntityType;

    public readonly healthComponentType: HealthComponentType;
    public readonly lifestyleEntryType: LifestyleEntryType;

    public readonly value?: number;


    aggregator: DynamicAggregator;
    
    // public brain: IpScoreBrainConstituent
    // public body: IpScoreBodyConstituent
    // public food: IpScoreFoodConstituent
    // public training: IpScoreTrainingConstituent
    // public programs: IpScoreProgramsConstituent

    // TODO:
    constructor(params: any) {
        const { entry } = params;
        // TODO: check group??
        if (entry)
            super({
                ..._defaultValues,
                ...params,
                entityId: entry.uid ? entry.uid : entry.userId,
                entityName: entry.fullName,
            });
        else
            super({
                ..._defaultValues,
                ...params,
            })
        // DocumentBuilder

        this.aggregator = new DynamicAggregator(entry ? LifestyleEntryType[entry.lifestyleEntryType] : LifestyleEntryType[LifestyleEntryType.IpScore]);
    }
    // get entry() {
    //     return super.entry;
    // }

    /**
     * return a new version of the BiometricAggregateModel with updated values as specified in the updatedValues parameter
     * @param updatedValues The new values for various members of the BiometricAggregateModel class.
     * @returns a new instance of a BiometricAggregateModel class with updated values
     */
    // with?(updatedValues: BiometricAggregateInterface): BiometricAggregateRecord {
    //     return super.with(updatedValues) as BiometricAggregateRecord;
    // }
}
