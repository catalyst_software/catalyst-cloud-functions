import { firestore } from 'firebase-admin'
import { AthleteIpScoreDailyAggregate } from '../../interfaces'
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type'
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type'
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type'
// import { AthleteDailyAggregate } from '../../../../../../models/well/aggregates/interfaces';

/**
 * Default Values when instanciating a new Athlete Biometric Daily Aggregate
 */
export const AthleteIpScoreDailyDocument: AthleteIpScoreDailyAggregate = {
    uid: '',

    entityId: '',
    entityName: '',
    userId: '',
    fullName: '',

    dateTime: firestore.Timestamp.now(),

    aggregateEntityType: AggregateEntityType.Athlete,
    organizationId: '',
    healthComponentType: HealthComponentType.IpScore,
    lifestyleEntryType: LifestyleEntryType.IpScore,
    value: 0,
    brain: {
        value: 0,
        mood: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
        sleep: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
    },
    body: {
        value: 0,
        fatigue: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
        pain: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
        weight: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
        wearables: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
    },
    food: {
        value: 0,
        components: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
    },
    training: {
        value: 0,
        components: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
    },
    programs: {
        value: 0,
        programDay: 0,
        worksheetConsumed: false,
        videoConsumed: false,
        surveyConsumed: false,
        expected: 0,
        realized: 0,
        percentage: 0,
    },
}
