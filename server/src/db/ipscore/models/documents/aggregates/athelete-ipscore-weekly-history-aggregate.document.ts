import { firestore } from 'firebase-admin'

import { AthleteIpScoreWeeklyHistoryAggregateInterface } from '../../interfaces/athlete-ipscore-weekly-history-aggregate'
import { IpScoreAggregateHistoryCollection } from '../../../enums/firestore-ipscore-aggregate-history-collection'
import { IpScoreAggregateHistoryRecord } from './ipscore-aggregate-history-record'
import { AthleteBiometricEntryInterface } from '../../../../biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type'
import { saveAggregateDoc } from '../../../services/save-aggregates'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type';
import { AggregateDocumentType } from '../../../../biometricEntries/well/enums/aggregate-document-type';
import { IpScoreAggregateComponentInterface } from '../../interfaces/ipscore-component-aggregate';
import { IpScoreAggregateTrackingCacheInterface } from '../../interfaces/ipscore-aggregate-tracking-cache';
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type'

/**
 * Default Values when instanciating a new Athlete Biometric Weekly Aggregate
 */

const _defaultValues: AthleteIpScoreWeeklyHistoryAggregateInterface = {

    uid: undefined,

    entityId: undefined,
    entityName: undefined,

    organizationId: undefined,

    collection: IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory,

    lifestyleEntryType: LifestyleEntryType.IpScore,

    aggregateEntityType: AggregateEntityType.Athlete,
    aggregateDocumentType: AggregateDocumentType.WeeklyHistory,
    healthComponentType: HealthComponentType.IpScore,

    weekNumber: undefined,

    componentValues: [],

    trackingCache: {
        index: 0,
        days: []
    } as IpScoreAggregateTrackingCacheInterface,

    values: []
};
export class AthleteIpScoreWeeklyHistoryAggregate extends IpScoreAggregateHistoryRecord {

    public readonly uid: string;
    public readonly guid: string;

    public readonly entry: AthleteBiometricEntryInterface;

    public readonly entityId: string;
    public readonly entityName: string;

    public readonly lifestyleEntryType: LifestyleEntryType;

    public readonly collection: IpScoreAggregateHistoryCollection;

    organizationId: string;

    healthComponentType: HealthComponentType;
    aggregateEntityType: AggregateEntityType;
    aggregateDocumentType: AggregateDocumentType;

    weekNumber: number;

    componentValues: Array<IpScoreAggregateComponentInterface>;
    trackingCache: IpScoreAggregateTrackingCacheInterface;

    values: Array<number>

    lastAggregateDate: firestore.Timestamp;

    constructor(params?: AthleteIpScoreWeeklyHistoryAggregateInterface) {
        if (params) {
            super({ ..._defaultValues, ...params } as any)
        } else {
            super(_defaultValues)
        }
    }

    async calculateAggregates(
        directive: AggregateIpScoreDirective
    ): Promise<AggregateIpScoreDirective> {
        const savedDoc = await saveAggregateDoc(
            this.aggregator.processAthleteIpScoreWeeklyHistory(directive),
            directive
        );
        debugger;
        return {
            ...directive,
            message: `Updated ${directive.collection} Doc.`,
            updatedDocument: savedDoc, 
        }
    }
}
