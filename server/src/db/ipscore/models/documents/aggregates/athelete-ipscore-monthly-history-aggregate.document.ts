import { firestore } from 'firebase-admin'

import { AthleteIpScoreMonthlyHistoryAggregateInterface } from '../../interfaces/athlete-ipscore-monthly-history-aggregate'
import { IpScoreAggregateHistoryCollection } from '../../../enums/firestore-ipscore-aggregate-history-collection'
import { IpScoreAggregateHistoryRecord } from './ipscore-aggregate-history-record'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { saveAggregateDoc } from '../../../services/save-aggregates'
import { AggregateDocumentType } from '../../../../biometricEntries/well/enums/aggregate-document-type';
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type';
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { IpScoreAggregateComponentInterface } from '../../interfaces/ipscore-component-aggregate';
import { IpScoreAggregateTrackingCacheInterface } from '../../interfaces/ipscore-aggregate-tracking-cache';
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type';

/**
 * Default Values when instanciating a new Athlete Biometric Weekly Aggregate
 */

const _defaultValues: AthleteIpScoreMonthlyHistoryAggregateInterface = {

    uid: undefined,

    entityId: undefined,
    entityName: undefined,

    organizationId: undefined,

    collection: IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory,

    aggregateEntityType: AggregateEntityType.Athlete,
    aggregateDocumentType: AggregateDocumentType.MonthlyHistory,
    healthComponentType: HealthComponentType.IpScore,
    lifestyleEntryType: LifestyleEntryType.IpScore,

    monthNumber: 0,

    recentEntries: [],
    componentValues: [],

    trackingCache: {
        index: 0,
        days: []
    } as IpScoreAggregateTrackingCacheInterface,

    values: [],

    lastAggregateDate: firestore.Timestamp.now()

};

export class AthleteIpScoreMonthlyHistoryAggregate
    extends IpScoreAggregateHistoryRecord
    implements AthleteIpScoreMonthlyHistoryAggregateInterface {

    uid?: string;
    guid?: string;

    entityName: string;
    entityId: string;

    organizationId: string;

    collection?: IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory;

    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;
    healthComponentType?: HealthComponentType;

    monthNumber?: number;

    values?: Array<number>

    componentValues?: Array<IpScoreAggregateComponentInterface>;
    recentEntries?: Array<IpScoreAggregateComponentInterface>
    trackingCache?: IpScoreAggregateTrackingCacheInterface;

    lastAggregateDate?: firestore.Timestamp;

    constructor(params?: AthleteIpScoreMonthlyHistoryAggregateInterface) {
        if (params) {
            super({ ..._defaultValues, ...params } as any)
        } else {
            super(_defaultValues)
        }
    }

    async calculateAggregates(
        directive: AggregateIpScoreDirective
    ): Promise<AggregateIpScoreDirective> {
        const savedDoc = await saveAggregateDoc(
            this.aggregator.processAthleteIpScoreMonthlyHistory(directive),
            directive
        );
        debugger;
        return {
            ...directive,
            message: `Updated ${directive.collection} Doc.`,
            updatedDocument: savedDoc,
        }
    }
}
