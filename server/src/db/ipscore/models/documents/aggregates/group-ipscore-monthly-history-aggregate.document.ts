import { firestore } from 'firebase-admin'

import { IpScoreAggregateHistoryCollection } from '../../../enums/firestore-ipscore-aggregate-history-collection'
import { IpScoreAggregateHistoryRecord } from './ipscore-aggregate-history-record'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'

import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type';
import { AggregateDocumentType } from '../../../../biometricEntries/well/enums/aggregate-document-type';
import { IpScoreAggregateComponentInterface } from '../../interfaces/ipscore-component-aggregate';
import { IpScoreAggregateTrackingCacheInterface } from '../../interfaces/ipscore-aggregate-tracking-cache';
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { AthleteBiometricEntryInterface } from '../../../../biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { GroupIpScoreMonthlyHistoryAggregateInterface } from '../../interfaces/group-ipscore-monthly-history-aggregate';
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type';
import { db } from '../../../../../shared/init/initialise-firebase'
import { FirestoreCollection } from '../../../../biometricEntries/enums/firestore-collections'
import { updateAthleteIndicatorsIPScoreOnGroups } from '../../../../../analytics/triggers/update-athlete-indicators-ipscore'
import { AthleteIndicator } from '../../../../biometricEntries/well/interfaces/athlete-indicator'

/**
 * Default Values when instanciating a new Group Biometric Monthly Aggregate
 */

const _defaultValues: GroupIpScoreMonthlyHistoryAggregateInterface = {

    uid: undefined,

    organizationId: undefined,

    // entry: undefined,

    entityId: undefined,
    entityName: undefined,

    collection: IpScoreAggregateHistoryCollection.GroupIpScoreMonthlyHistory,

    aggregateEntityType: AggregateEntityType.Group,
    aggregateDocumentType: AggregateDocumentType.MonthlyHistory,
    healthComponentType: HealthComponentType.IpScore,
    lifestyleEntryType: LifestyleEntryType.IpScore,

    monthNumber: 0, // TODO: Move to 

    componentValues: [],
    recentEntries: [],

    trackingCache: {
        index: 0,
        days: []
    } as IpScoreAggregateTrackingCacheInterface,

    values: [],

    startDate: firestore.Timestamp.now(),
    endDate: firestore.Timestamp.now(),

    lastAggregateDate: firestore.Timestamp.now()

};

export class GroupIpScoreMonthlyHistoryAggregate extends IpScoreAggregateHistoryRecord {

    public readonly uid: string;
    public readonly guid: string;

    public readonly entry: AthleteBiometricEntryInterface;

    public readonly entityId: string;
    public readonly entityName: string;

    organizationId: string;

    collection: IpScoreAggregateHistoryCollection;

    aggregateEntityType: AggregateEntityType;
    aggregateDocumentType: AggregateDocumentType;
    healthComponentType: HealthComponentType;
    lifestyleEntryType: LifestyleEntryType;

    monthNumber: number;

    componentValues: Array<IpScoreAggregateComponentInterface>;
    recentEntries: Array<IpScoreAggregateComponentInterface>;
    trackingCache: IpScoreAggregateTrackingCacheInterface;

    values: Array<number>;

    lastAggregateDate?: firestore.Timestamp;

    constructor(params?: any) {
        if (params) {
            super({ ..._defaultValues, ...params })
        } else {
            super(_defaultValues)
        }
    }

    async calculateAggregates(
        directive: AggregateIpScoreDirective
    ): Promise<AggregateIpScoreDirective> {
        const savedDoc = this.aggregator.processGroupIpScoreMonthlyHistory(directive);
        // await saveAggregateDoc(
        // this.aggregator.processGroupIpScoreMonthlyHistory(directive),
        // aggregator.processGroupIpScoreMonthlyHistory(directive),
        //     directive
        // )
        //     .then(async (saved) => {
        // debugger;
        const { group, athlete } = directive;

        console.log('GroupIpScoreMonthlyHistoryAggregate => directive', directive)

        const groupCollectionRef: firestore.CollectionReference = db.collection(
            FirestoreCollection.Groups
        );


        const athleteUID = athlete.uid
        let dailyIPScore;

        group.athletes.filter((ath) => ath.uid === athleteUID).forEach((theAthlete) => {
            dailyIPScore=  theAthlete.ipScore
            athlete.currentIpScoreTracking.ipScore = theAthlete.ipScore
            athlete.runningTotalIpScore = theAthlete.runningTotalIpScore

        })

        const theGroupIds = [group.uid || group.groupId]
        console.warn('++++++++++++============>>>>>>> updating group', {
            uid: group.uid,
            group: group.groupName,
            
            athleteIPScores: group.athletes.map((ath) => {
                return {
                    name: ath.firstName + ' ' + ath.lastName,
                    ipScore: ath.ipScore,
                    runninIPScore: ath.runningTotalIpScore
                }
            })
        })
        if (theGroupIds.length) {

            console.warn('running updateAthleteIPScoreValue')
            console.log({ uid: athlete.uid, runningTotalIpScore: athlete.runningTotalIpScore, dailyIPScore })


            const updateResults = await updateAthleteIndicatorsIPScoreOnGroups(athlete.uid, theGroupIds, athlete.runningTotalIpScore, dailyIPScore,
                // TODO: get utcOffset 
                0);

            if (updateResults.length) {

                const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName

                console.log(`IPScore Group Indicators update results for ${name} -  ${athleteUID}`,
                    updateResults
                )
                // return {
                //     message: `IPScore Group Indicators update results for ${name} -  ${athleteUID}`,
                //     updateResults
                // }

            } else {
                console.warn(`No ipscore GroupIndicator update results for ${athlete.profile.firstName} -  ${athleteUID}`);

                // return {
                //     message: `No ipscore GroupIndicator update results for ${athlete.profile.firstName} - ${athleteUID}`
                // }
            }
        } else {
            console.warn(`No Groups found for ${athlete.profile.firstName} - ${athleteUID}`);

            // return {
            //     message: `No ipscore GroupIndicator update results ${athleteUID}`
            // }
        }




        if (athlete) {
            console.log('athlete ipscore details', {
                ipscore: athlete.currentIpScoreTracking.ipScore,
                runningTotalIpScore: athlete.runningTotalIpScore
            })
        }


        await groupCollectionRef
            .doc(group.uid)
            .update(group).then((writeResult) => {
                return writeResult
            })
            .catch((err) => {
                console.log(err);
                return err
            });


        // return saved
        // });

        debugger;
        return {
            ...directive,
            message: `Updated ${directive.collection} Docs.`,
            updatedDocument: savedDoc,
        }
    }
}
