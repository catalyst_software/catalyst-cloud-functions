import { firestore } from 'firebase-admin'

import { GroupIpScoreDailyAggregate } from '../../interfaces'
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type'
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type'
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type'

/**
 * Default Values when instanciating a new Group Biometric Daily Aggregate
 */
export const GroupIpScoreDailyDocument: GroupIpScoreDailyAggregate = {
    uid: '',

    entityId: undefined,
    entityName: undefined,

    aggregateEntityType: AggregateEntityType.Group,
    organizationId: '',
    healthComponentType: HealthComponentType.IpScore,
    lifestyleEntryType: LifestyleEntryType.IpScore,

    dateTime: firestore.Timestamp.now(),

    value: 0,
    brain: {
        value: 0,
        mood: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
        sleep: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
    },
    body: {
        value: 0,
        fatigue: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
        pain: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
        weight: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
        wearables: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
    },
    food: {
        value: 0,
        components: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
    },
    training: {
        value: 0,
        components: {
            expected: 0,
            realized: 0,
            percentage: 0,
        },
    },
    programs: {
        value: 0,
        programDay: 0,
        worksheetConsumed: false,
        videoConsumed: false,
        surveyConsumed: false,
        expected: 0,
        realized: 0,
        percentage: 0,
    },
}
