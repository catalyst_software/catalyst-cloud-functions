import { IpScoreAggregateRecord } from './ipscore-aggregate-record'

import { IpScoreAggregateCollection } from '../../../enums/firestore-ipscore-aggregate-collection'
import {
    AthleteWeeklyAggregate,
} from '../../../../biometricEntries/models/documents/aggregates'
import {
    AthleteIpScoreWeeklyAggregateInterface,
    AthleteIpScoreDailyAggregate,
} from '../../interfaces'
import { firestore } from 'firebase-admin'
import { saveAggregateDoc } from '../../../services/save-aggregates'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { updateAthleteIPScoreValue } from '../../../../../api/app/v1/controllers/utils/update-athlete-ipscore-value';
import { AggregateDocumentType } from '../../../../biometricEntries/well/enums/aggregate-document-type';
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type';
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type'

/**
 * Default Values when instanciating a new Athlete Biometric Weekly Aggregate
 */

// const _defaultValues: AthleteWeeklyAggregateInterface = {
const _defaultValues: AthleteIpScoreWeeklyAggregateInterface = {

    uid: undefined,

    entityId: undefined,
    entityName: undefined,

    organizationId: undefined,

    aggregateEntityType: AggregateEntityType.Athlete,
    aggregateDocumentType: AggregateDocumentType.Weekly,
    collection: IpScoreAggregateCollection.AthleteIpScoreWeekly,

    healthComponentType: HealthComponentType.IpScore,
    lifestyleEntryType: LifestyleEntryType.IpScore,
    dailies: undefined,

    weekNumber: 0,
    startDate: firestore.Timestamp.now(),
    endDate: firestore.Timestamp.now(),

    value: undefined,

    lastAggregateDate: firestore.Timestamp.now(),

};

export class AthleteIpScoreWeeklyAggregate extends IpScoreAggregateRecord {

    public readonly uid: string;
    public readonly guid: string;


    // public readonly userId: string;
    // public readonly fullName: string;
    // public readonly entry: string;

    public readonly entityId: string;
    public readonly entityName: string;

    public readonly collection: IpScoreAggregateCollection;

    lifestyleEntryType: LifestyleEntryType;
    healthComponentType: HealthComponentType;
    aggregateEntityType: AggregateEntityType;
    aggregateDocumentType: AggregateDocumentType;

    dailies: Array<AthleteIpScoreDailyAggregate>;

    weekNumber: number;
    startDate: firestore.Timestamp;
    endDate: firestore.Timestamp;
    // readonly weekNumber?: number;

    value: number;

    lastAggregateDate: firestore.Timestamp;

    constructor(params: AthleteIpScoreWeeklyAggregateInterface) {
        if (params) {
            super({ ..._defaultValues, ...params })
        } else {
            super(_defaultValues)
        }
    }

    // get fullName() {
    //     return super.entry.fulllName
    // }
    with?(values): AthleteWeeklyAggregate {
        // const returnVal = new (this.constructor as any)();
        // returnVal._data = this._data.merge(values);
        debugger;
        const returnVal = new (this.constructor as any)({ ...this._data.merge(values).toJS() });
        return returnVal
    }

    async calculateAggregates(
        directive: AggregateIpScoreDirective
    ) {

        const { athlete } = directive;

        console.warn('athlete IPScore BEFORE aggregation', athlete.runningTotalIpScore)


        const updatedDoc = this.aggregator.processAthleteIpScoreWeekly(directive)
        const savedDoc = await saveAggregateDoc(
            updatedDoc,
            directive
        );

        const dailyIPScore = athlete.currentIpScoreTracking.ipScore

        // console.warn('updatedDoc dailyIpScore AFTER aggregation', dailyIPScore)

        // const dailyIPScoreRounded = Math.round(dailyIPScore)
        // console.warn('updatedDoc dailyIpScore ROUNDED AFTER aggregation', dailyIPScoreRounded)


        console.warn('running updateAthleteIPScoreValue')
        console.log({ uid: athlete.uid, runningTotalIpScore: athlete.runningTotalIpScore, dailyIPScore })
        debugger;
        const ipscoreUpdateResult = await updateAthleteIPScoreValue(athlete, athlete.runningTotalIpScore, dailyIPScore)

        console.warn('athlete IPScore AFTER aggregation', athlete.runningTotalIpScore)

        console.warn('ipscoreUpdateResult', ipscoreUpdateResult)


        return {
            ...directive,
            message: `Updated ${directive.collection} Doc.`,
            updatedDocument: savedDoc,
        }
    }
}
