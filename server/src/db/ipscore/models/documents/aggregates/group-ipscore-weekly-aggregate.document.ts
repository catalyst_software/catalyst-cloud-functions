import { firestore } from 'firebase-admin'

import {
    GroupIpScoreWeeklyAggregateInterface,
    GroupIpScoreDailyAggregate,
} from '../../interfaces'

import { IpScoreAggregateCollection } from '../../../enums/firestore-ipscore-aggregate-collection'
import { IpScoreAggregateRecord } from './ipscore-aggregate-record'
import { GroupIpScoreWeeklyHistoryAggregate } from '.'
import { AggregateIpScoreDirective } from '../../../services/interfaces/aggregate-ipscore-directive'
import { AggregateDocumentType } from '../../../../biometricEntries/well/enums/aggregate-document-type';
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type';
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { AggregateEntityType } from '../../../../biometricEntries/enums/aggregate-entity-type'

/**
 * Default Values when instanciating a new Group Biometric Weekly Aggregate
 */

const _defaultValues: GroupIpScoreWeeklyAggregateInterface = {
    uid: undefined,

    entityId: undefined,
    entityName: undefined,

    organizationId: undefined,

    aggregateEntityType: AggregateEntityType.Group,
    aggregateDocumentType: AggregateDocumentType.Weekly,
    collection: IpScoreAggregateCollection.GroupIpScoreWeekly,

    healthComponentType: HealthComponentType.IpScore,
    lifestyleEntryType: LifestyleEntryType.IpScore,

    dailies: undefined,

    weekNumber: 0,
    startDate: firestore.Timestamp.now(),
    endDate: firestore.Timestamp.now(),

    value: undefined

};

export class GroupIpScoreWeeklyAggregate extends IpScoreAggregateRecord {
    public readonly collection: IpScoreAggregateCollection;
    public readonly historyDoc?: GroupIpScoreWeeklyHistoryAggregate;
    public readonly uid: string;
    public readonly guid: string;

    public readonly entityId: string;
    public readonly entityName: string;

    lifestyleEntryType: LifestyleEntryType;
    healthComponentType: HealthComponentType;
    aggregateEntityType: AggregateEntityType;
    aggregateDocumentType: AggregateDocumentType;

    dailies: Array<GroupIpScoreDailyAggregate>;

    weekNumber: number;
    startDate: firestore.Timestamp;
    endDate: firestore.Timestamp;
    // readonly weekNumber?: number;

    value: number;

    constructor(params: GroupIpScoreWeeklyAggregateInterface) {
        super({ ..._defaultValues, ...params })
    }

    async calculateAggregates(
        directive: AggregateIpScoreDirective
    ): Promise<AggregateIpScoreDirective> {
        // const savedDoc = await saveAggregateDoc(
        //     this.aggregator.processGroupIpScoreWeekly(directive),
        //     // aggregator.processGroupIpScoreWeekly(directive),
        //     directive
        // );
        const savedDoc =  this.aggregator.processGroupIpScoreWeekly(directive)
        debugger;
        return {
            ...directive,
            message: `Updated ${directive.collection} Docs.`,
            updatedDocument: savedDoc,
        }
    }
}
