
import { Guid } from 'guid-typescript';
import { GroupWeeklyAggregateInterface } from '../../../../biometricEntries/well/aggregate/interfaces';
import { HealthComponentType } from '../../../../biometricEntries/well/enums/health-component-type';
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';



/**
 * Default Values when instanciating a new Group Biometric Weekly Aggregate
 */
export const CommonMonthlyDefaultProps: GroupWeeklyAggregateInterface = {
    uid: '',
    organizationId: '',
    entityId: undefined,
    entityName: undefined,
    healthComponentType: HealthComponentType.IpScore,
    lifestyleEntryType: LifestyleEntryType.IpScore
};

