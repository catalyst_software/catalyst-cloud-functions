export interface IpScoreConfiguration {
    quantitativeWellness: IpScoreQuantitativeWellnessConfiguration
    qualitativeWellness: IpScoreQualitativeWellnessConfiguration
    programs: IpScoreProgramConfiguration
    nonEngagementPenalization?: IpScoreNonEngagementPenalizationConfiguration
}

export interface IpScoreQuantitativeWellnessConfiguration {
    include: boolean
    brain: IpScoreQuantitativeWellnessBrainConfiguration
    body: IpScoreQuantitativeWellnessBodyConfiguration
    food: IpScoreQuantitativeWellnessComponentConfiguration
    training: IpScoreQuantitativeWellnessComponentConfiguration
}

export interface IpScoreQualitativeWellnessConfiguration {
    include: boolean
}

export interface IpScoreProgramConfiguration {
    include: boolean
    includeVideoConsumption: boolean
    includeSurveyConsumption: boolean
    includeWorkSheetConsumption: boolean
}

export interface IpScoreQuantitativeWellnessBrainConfiguration {
    mood: IpScoreQuantitativeWellnessEntryConfiguration
    sleep: IpScoreQuantitativeWellnessEntryConfiguration
}

export interface IpScoreQuantitativeWellnessBodyConfiguration {
    fatigue: IpScoreQuantitativeWellnessEntryConfiguration
    pain: IpScoreQuantitativeWellnessEntryConfiguration
    weight: IpScoreQuantitativeWellnessEntryConfiguration
    wearables: IpScoreQuantitativeWellnessEntryConfiguration
}

export interface IpScoreQuantitativeWellnessComponentConfiguration {
    components: IpScoreQuantitativeWellnessEntryConfiguration
}

export interface IpScoreQuantitativeWellnessEntryConfiguration {
    include: boolean
    expectedDailyEntries: number
    allowedSkipDays?: Array<number>
}

export interface IpScoreNonEngagementPenalizationConfiguration {
    programs: IpScoreProgramNonEngagementPenalizationConfiguration
}

export interface IpScoreProgramNonEngagementPenalizationConfiguration {
    include: boolean
    maxScorePossible?: number
}
