import { firestore } from 'firebase-admin';

import { IpScoreAggregateInterface } from './ipscore-aggregate';
import { IpScoreAggregateCollection } from '../../enums/firestore-ipscore-aggregate-collection';

export interface AthleteIpScoreMonthlyAggregateInterface extends IpScoreAggregateInterface {

    uid?: string;
    guid?: string;

    collection?: IpScoreAggregateCollection;
    
    fromDate?: firestore.Timestamp; 
    toDate?: firestore.Timestamp; 
}