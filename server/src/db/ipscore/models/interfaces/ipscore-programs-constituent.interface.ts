import { IpScoreLifestyleTypeConstituent } from "./ipscore-lifestyletype-constituent.interface";

export interface IpScoreProgramsConstituent extends IpScoreLifestyleTypeConstituent {
    value: number;
    programDay?: number;
    videoConsumed?: boolean;
    surveyConsumed?: boolean;
    worksheetConsumed?: boolean;
     // New
    historicalValues?: Array<number>;
}