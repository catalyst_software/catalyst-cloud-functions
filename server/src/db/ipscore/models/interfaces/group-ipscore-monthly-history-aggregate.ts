
import { firestore } from 'firebase-admin';

import { IpScoreHistoryAggregate } from './ipscore-history-aggregate';
import { IpScoreAggregateHistoryCollection } from '../../enums/firestore-ipscore-aggregate-history-collection';
import { IpScoreAggregateComponentInterface } from './ipscore-component-aggregate';
import { HealthComponentType } from '../../../biometricEntries/well/enums/health-component-type';
import { AggregateDocumentType } from '../../../biometricEntries/well/enums/aggregate-document-type';
import { IpScoreAggregateTrackingCacheInterface } from './ipscore-aggregate-tracking-cache';
import { AggregateEntityType } from '../../../biometricEntries/enums/aggregate-entity-type';

export interface GroupIpScoreMonthlyHistoryAggregateInterface extends IpScoreHistoryAggregate {

    guid?: string;
    uid?: string;

    collection?: IpScoreAggregateHistoryCollection;

    startDate?: firestore.Timestamp;
    endDate?: firestore.Timestamp;

    healthComponentType?: HealthComponentType;

    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;

    organizationId: string;
    entityName: string;
    entityId: string;

    monthNumber?: number;

    values?: Array<number>

    componentValues?: Array<IpScoreAggregateComponentInterface>;
    trackingCache?: IpScoreAggregateTrackingCacheInterface;
    recentEntries?: Array<IpScoreAggregateComponentInterface>;

    lastAggregateDate?: firestore.Timestamp;
}