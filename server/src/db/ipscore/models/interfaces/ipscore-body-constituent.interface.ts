import { IpScoreLifestyleTypeConstituent } from './ipscore-lifestyletype-constituent.interface'

export interface IpScoreBodyConstituent {
    value: number
    fatigue: IpScoreLifestyleTypeConstituent
    pain: IpScoreLifestyleTypeConstituent
    weight: IpScoreLifestyleTypeConstituent
    wearables: IpScoreLifestyleTypeConstituent
}
