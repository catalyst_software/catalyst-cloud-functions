import { firestore } from "firebase-admin";

import { IpScoreAggregateInterface, GroupIpScoreDailyAggregate } from ".";

import { IpScoreAggregateCollection } from "../../enums/firestore-ipscore-aggregate-collection";
import { LifestyleEntryType } from "../../../../models/enums/lifestyle-entry-type";
import { HealthComponentType } from "../../../biometricEntries/well/enums/health-component-type";
import { AggregateDocumentType } from "../../../biometricEntries/well/enums/aggregate-document-type";
import { AggregateEntityType } from "../../../biometricEntries/enums/aggregate-entity-type";

export interface GroupIpScoreWeeklyAggregateInterface extends IpScoreAggregateInterface {
    collection?: IpScoreAggregateCollection;

    lifestyleEntryType?: LifestyleEntryType;
    healthComponentType?: HealthComponentType;
    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;

    dailies?: Array<GroupIpScoreDailyAggregate>;

    weekNumber?: number;
    startDate?: firestore.Timestamp;
    endDate?: firestore.Timestamp;
}