import { IpScoreAggregateInterface } from './ipscore-aggregate';
import { AthleteIpScoreDailyAggregate } from '.';
import { firestore } from 'firebase-admin';
import { IpScoreAggregateCollection } from '../../enums/firestore-ipscore-aggregate-collection';
import { AggregateIpScoreDirective } from '../../services/interfaces/aggregate-ipscore-directive';


export interface AthleteIpScoreWeeklyAggregateInterface extends IpScoreAggregateInterface {

    collection?: IpScoreAggregateCollection;
    userId?: string;
    fullName?: string;
    dailies: Array<AthleteIpScoreDailyAggregate>;

    weekNumber?: number;
    startDate?: firestore.Timestamp;
    endDate?: firestore.Timestamp;

    lastAggregateDate?: firestore.Timestamp,

    calculateAggregates?: (
        directive?: AggregateIpScoreDirective
    ) => Promise<any>;

}