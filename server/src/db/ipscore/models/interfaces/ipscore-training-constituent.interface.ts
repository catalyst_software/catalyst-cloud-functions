import { IpScoreLifestyleTypeConstituent } from "./ipscore-lifestyletype-constituent.interface";

export interface IpScoreTrainingConstituent {
    value: number;
    components: IpScoreLifestyleTypeConstituent;
}