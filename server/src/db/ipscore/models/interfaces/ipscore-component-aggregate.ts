

import { IpScoreBrainConstituent } from './ipscore-brain-constituent.interface';
import { IpScoreBodyConstituent } from './ipscore-body-constituent.interface';
import { IpScoreFoodConstituent } from './ipscore-food-constituent.interface';
import { IpScoreTrainingConstituent } from './ipscore-training-constituent.interface';
import { IpScoreProgramsConstituent } from './ipscore-programs-constituent.interface';

export interface IpScoreAggregateComponentInterface {

    value?: number;
    brain?: IpScoreBrainConstituent;
    body?: IpScoreBodyConstituent;
    food?: IpScoreFoodConstituent;
    training?: IpScoreTrainingConstituent;
    programs?: IpScoreProgramsConstituent;

}

