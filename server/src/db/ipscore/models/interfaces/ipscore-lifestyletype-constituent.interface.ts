export interface IpScoreLifestyleTypeConstituent {
    expected: number;
    realized: number;
    percentage: number;
}