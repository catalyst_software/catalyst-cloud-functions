import { IpScoreLifestyleTypeConstituent } from "./ipscore-lifestyletype-constituent.interface";

export interface IpScoreBrainConstituent {
    value: number;
    mood: IpScoreLifestyleTypeConstituent;
    sleep: IpScoreLifestyleTypeConstituent;
}