import { IpScoreAggregateComponentInterface } from "./ipscore-component-aggregate";

export interface IpScoreAggregateTrackingCacheInterface {
    index: number;
    dayIndex?: number;
    days: Array<IpScoreAggregateComponentInterface>;
}