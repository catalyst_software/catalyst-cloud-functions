import { firestore } from 'firebase-admin';

import { IpScoreHistoryAggregate } from './ipscore-history-aggregate'
import { IpScoreAggregateHistoryCollection } from '../../enums/firestore-ipscore-aggregate-history-collection'
import { HealthComponentType } from '../../../biometricEntries/well/enums/health-component-type';
import { AggregateDocumentType } from '../../../biometricEntries/well/enums/aggregate-document-type';
import { IpScoreAggregateComponentInterface } from './ipscore-component-aggregate';
import { IpScoreAggregateTrackingCacheInterface } from './ipscore-aggregate-tracking-cache';

import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { AggregateEntityType } from '../../../biometricEntries/enums/aggregate-entity-type';

export interface AthleteIpScoreWeeklyHistoryAggregateInterface
    extends IpScoreHistoryAggregate {

    collection?: IpScoreAggregateHistoryCollection;

    values?: Array<number>

    lifestyleEntryType?: LifestyleEntryType;
    healthComponentType?: HealthComponentType;
    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;

    organizationId: string;

    entityId: string;
    entityName: string;

    weekNumber?: number;

    componentValues?: Array<IpScoreAggregateComponentInterface>;
    trackingCache?: IpScoreAggregateTrackingCacheInterface;

    lastAggregateDate?: firestore.Timestamp;
}
