import { firestore } from 'firebase-admin';

import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { AggregateDocumentType } from '../../../biometricEntries/well/enums/aggregate-document-type';
import { IpScoreAggregateTrackingCacheInterface } from './ipscore-aggregate-tracking-cache';
import { HealthComponentType } from '../../../biometricEntries/well/enums/health-component-type';
import { IpScoreAggregateComponentInterface } from './ipscore-component-aggregate';
import { IpScoreAggregateHistoryCollection } from '../../enums/firestore-ipscore-aggregate-history-collection';
import { AggregateEntityType } from '../../../biometricEntries/enums/aggregate-entity-type';

export interface IpScoreHistoryAggregate {
    guid?: string;
    uid?: string;
    // documentId: string;
    entityId: string;
    entityName: string;
    collection?: IpScoreAggregateHistoryCollection;
    organizationId?: string;
    lifestyleEntryType?: LifestyleEntryType;
    healthComponentType?: HealthComponentType;
    
    aggregateDocumentType?: AggregateDocumentType;
    aggregateEntityType?: AggregateEntityType;
    

    values?: Array<number>;
    componentValues?: Array<IpScoreAggregateComponentInterface>;
    trackingCache?: IpScoreAggregateTrackingCacheInterface;

    lastAggregateDate?: firestore.Timestamp;
}
