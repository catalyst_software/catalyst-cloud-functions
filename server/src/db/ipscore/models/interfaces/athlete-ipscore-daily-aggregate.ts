import { firestore } from 'firebase-admin'

import { IpScoreAggregateInterface } from './ipscore-aggregate'
import { HealthComponentType } from '../../../biometricEntries/well/enums/health-component-type'
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type'
import { IpScoreBrainConstituent } from './ipscore-brain-constituent.interface'
import { IpScoreBodyConstituent } from './ipscore-body-constituent.interface'
import { IpScoreFoodConstituent } from './ipscore-food-constituent.interface'
import { IpScoreTrainingConstituent } from './ipscore-training-constituent.interface'
import { IpScoreProgramsConstituent } from './ipscore-programs-constituent.interface'

export interface AthleteIpScoreDailyAggregate
    extends IpScoreAggregateInterface {
    uid?: string
    organizationId: string

    userId?: string
    fullName?: string

    dateTime?: firestore.Timestamp

    
    healthComponentType?: HealthComponentType
    lifestyleEntryType?: LifestyleEntryType
    value?: number
    brain?: IpScoreBrainConstituent
    body?: IpScoreBodyConstituent
    food?: IpScoreFoodConstituent
    training?: IpScoreTrainingConstituent
    programs?: IpScoreProgramsConstituent
}
