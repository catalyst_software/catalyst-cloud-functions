import { AggregateDocumentType } from '../../../biometricEntries/well/enums/aggregate-document-type';
import { IpScoreAggregateComponentInterface } from './ipscore-component-aggregate';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { HealthComponentType } from '../../../biometricEntries/well/enums/health-component-type';
import { AggregateIpScoreDirective } from '../../services/interfaces/aggregate-ipscore-directive';
import { AllAggregateCollectionType } from '../../../../models/types/biometric-aggregate-types';
import { AggregateEntityType } from '../../../biometricEntries/enums/aggregate-entity-type';

export interface IpScoreAggregateInterface extends IpScoreAggregateComponentInterface {
    uid?: string;


    entityId: string;
    entityName: string;

    collection?: AllAggregateCollectionType;

    organizationId: string;

    lifestyleEntryType?: LifestyleEntryType;
    healthComponentType?: HealthComponentType;

    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;

    calculateAggregates?: (
        directive?: AggregateIpScoreDirective
    ) => Promise<AggregateIpScoreDirective>;

}