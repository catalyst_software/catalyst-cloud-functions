import { IpScoreLifestyleTypeConstituent } from "./ipscore-lifestyletype-constituent.interface";

export interface IpScoreFoodConstituent {
    value: number;
    components: IpScoreLifestyleTypeConstituent;
}