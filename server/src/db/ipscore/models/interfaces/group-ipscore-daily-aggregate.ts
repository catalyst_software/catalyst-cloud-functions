import { firestore } from 'firebase-admin';

import { IpScoreAggregateInterface } from './ipscore-aggregate';

export interface GroupIpScoreDailyAggregate extends IpScoreAggregateInterface {
    uid?: string;
    guid?: string;

    dateTime?: firestore.Timestamp; 
}

