import { firestore } from 'firebase-admin';

import { IpScoreAggregateInterface } from './ipscore-aggregate';
import { IpScoreAggregateCollection } from '../../enums/firestore-ipscore-aggregate-collection';

import { LifestyleEntryType } from "../../../../models/enums/lifestyle-entry-type";
import { HealthComponentType } from "../../../biometricEntries/well/enums/health-component-type";
import { AggregateDocumentType } from "../../../biometricEntries/well/enums/aggregate-document-type";
import { AggregateEntityType } from '../../../biometricEntries/enums/aggregate-entity-type';

export interface GroupIpScoreMonthlyAggregateInterface extends IpScoreAggregateInterface {

    uid?: string;
    guid?: string;

    collection?: IpScoreAggregateCollection;

    lifestyleEntryType?: LifestyleEntryType;
    healthComponentType?: HealthComponentType;
    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;

    fromDate?: firestore.Timestamp; 
    toDate?: firestore.Timestamp; 
}