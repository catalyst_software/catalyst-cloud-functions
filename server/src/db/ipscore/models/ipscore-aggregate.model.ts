import { IpScoreAggregateCollection } from '../enums/firestore-ipscore-aggregate-collection';
import { IpScoreAggregateInterface } from './interfaces/ipscore-aggregate';
import { HealthComponentType } from '../../biometricEntries/well/enums/health-component-type';
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type';
import { AggregateDocumentType } from '../../biometricEntries/well/enums/aggregate-document-type';
import { IpScoreBrainConstituent } from './interfaces/ipscore-brain-constituent.interface';
import { IpScoreBodyConstituent } from './interfaces/ipscore-body-constituent.interface';
import { IpScoreFoodConstituent } from './interfaces/ipscore-food-constituent.interface';
import { IpScoreTrainingConstituent } from './interfaces/ipscore-training-constituent.interface';
import { IpScoreProgramsConstituent } from './interfaces/ipscore-programs-constituent.interface';
import { AggregateEntityType } from '../../biometricEntries/enums/aggregate-entity-type';

export class IpScoreAggregateModel implements IpScoreAggregateInterface {

    uid?: string;
    organizationId: string;
    entityId: string;
    entityName: string;

    collection?: IpScoreAggregateCollection;
    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;

    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    value?: number;
    brain?: IpScoreBrainConstituent;
    body?: IpScoreBodyConstituent;
    food?: IpScoreFoodConstituent;
    training?: IpScoreTrainingConstituent; 
    programs?: IpScoreProgramsConstituent;

}
