import {
    AthleteIpScoreWeeklyHistoryAggregate,
    GroupIpScoreWeeklyHistoryAggregate,
} from '../models/documents/aggregates'
import {
    AthleteIpScoreWeeklyAggregate,
    GroupIpScoreWeeklyAggregate,
    // GroupIpScoreMonthlyAggregate,
    // AthleteIpScoreMonthlyAggregate,
} from '../../../models/documents/well'
import { GroupIpScoreMonthlyHistoryAggregate } from '../models/documents/aggregates/group-ipscore-monthly-history-aggregate.document'
import { Athlete } from '../../../models/athlete/interfaces/athlete'
import { Group } from '../../../models/group/interfaces/group'
import { GroupIndicator } from '../../biometricEntries/well/interfaces/group-indicator'
import { AthleteIpScoreMonthlyHistoryAggregate } from '../models/documents/aggregates/athelete-ipscore-monthly-history-aggregate.document'
import {
    AthleteIpScoreWeeklyAggregateInterface,
    GroupIpScoreWeeklyAggregateInterface,
} from '../models/interfaces'
import { AthleteIpScoreWeeklyHistoryAggregateInterface } from '../models/interfaces/athlete-ipscore-weekly-history-aggregate'
import { AthleteIpScoreMonthlyHistoryAggregateInterface } from '../models/interfaces/athlete-ipscore-monthly-history-aggregate'
import { GroupIpScoreWeeklyHistoryAggregateInterface } from '../models/interfaces/group-ipscore-weekly-history-aggregate'
import { GroupIpScoreMonthlyHistoryAggregateInterface } from '../models/interfaces/group-ipscore-monthly-history-aggregate'

// export type BiometricAggregateType = AthleteWeeklyAggregateInterface & AthleteMonthlyAggregateInterface | GroupMonthlyAggregateInterface & GroupWeeklyAggregateInterface;
// type HistoryAggregateInterfaceType = AthleteWeeklyHistoryAggregateInterface & AthleteMonthlyHistoryAggregateInterface | GroupMonthlyHistoryAggregateInterface & GroupWeeklyHistoryAggregateInterface;
// export type GeneralAggregateType = HistoryAggregateInterfaceType | BiometricAggregateType;
// export type AllAggregateInterfaceType = BiometricAggregateInterface & GeneralAggregateType;

export type IpScoreAggregateType =
    | AthleteIpScoreWeeklyAggregate
    | GroupIpScoreWeeklyAggregate
    | AthleteIpScoreWeeklyAggregateInterface
    | GroupIpScoreWeeklyAggregateInterface
// AthleteMonthlyAggregate |
// GroupMonthlyAggregate;

export type IpScoreHistoryAggregateType =
    | AthleteIpScoreWeeklyHistoryAggregate
    | AthleteIpScoreMonthlyHistoryAggregate
    | GroupIpScoreWeeklyHistoryAggregate
    | GroupIpScoreMonthlyHistoryAggregate
    | AthleteIpScoreWeeklyHistoryAggregateInterface
    | AthleteIpScoreMonthlyHistoryAggregateInterface
    | GroupIpScoreWeeklyHistoryAggregateInterface
    | GroupIpScoreMonthlyHistoryAggregateInterface

export type AllIpScoreAggregateType =
    | IpScoreAggregateType
    | IpScoreHistoryAggregateType
    | AthleteIpScoreWeeklyAggregateInterface
    | IpScoreAggregateType
    // | AthleteIpScoreMonthlyAggregate
    // | GroupIpScoreMonthlyAggregate
    | IpScoreAggregateType

export type WeeklyIpScoreAggregateType =
    | AthleteIpScoreWeeklyAggregate
    | GroupIpScoreWeeklyAggregate
export type WeeklyIpScoreHistoryAggregateType =
    | AthleteIpScoreWeeklyHistoryAggregate
    | GroupIpScoreWeeklyHistoryAggregate
export type MonthlyIpScoreHistoryAggregateType =
    | AthleteIpScoreMonthlyHistoryAggregate
    | GroupIpScoreMonthlyHistoryAggregate

export type EntityType = Athlete | Group | GroupIndicator

export type AthleteIpScoreDocumentType =
    | AthleteIpScoreWeeklyAggregateInterface
    | AthleteIpScoreWeeklyHistoryAggregateInterface
    | AthleteIpScoreMonthlyHistoryAggregateInterface

export type AllIpScoreAggregateTypeCheck2 =
    | IpScoreAggregateDocumentType
    | IpScoreAggregateHistoryDocumentType

export type IpScoreAggregateDocumentType =
    | AthleteIpScoreWeeklyAggregate
    // | AthleteIpScoreMonthlyAggregate
    | GroupIpScoreWeeklyAggregate
// | GroupIpScoreMonthlyAggregate

export type IpScoreAggregateHistoryDocumentType =
    | AthleteIpScoreWeeklyHistoryAggregate
    | AthleteIpScoreMonthlyHistoryAggregate
    | GroupIpScoreWeeklyHistoryAggregate
    | GroupIpScoreMonthlyHistoryAggregate

export type IpScoreAllAggregateDocumentType =
    | IpScoreAggregateDocumentType
    | IpScoreAggregateHistoryDocumentType
