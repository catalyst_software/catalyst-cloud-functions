import { Athlete } from "../../models/athlete/interfaces/athlete";
import { AggregateDirectiveType } from "../../models/types/biometric-aggregate-types";
import { AggregateIpScoreDirective } from "./services/interfaces/aggregate-ipscore-directive";
import { getAggregateDocumentID } from "../../api/app/v1/callables/get-aggregate-document-Id";
import { executeAggregate } from "../../api/app/v1/callables/execute-aggregate";
import { allIPScoreAggregateCollections } from "./services/ip-score-aggregate-collections";
import { processGroupIPScoreAggregationRequest } from "../../api/app/v1/callables/processAggregationRequest";
import { firestore } from "firebase-admin";
import { AthleteBiometricEntryInterface } from "../biometricEntries/well/aggregate/interfaces";
import { isArray } from "lodash";

export const updateIPScoreAggregates = async (t: FirebaseFirestore.Transaction, athlete: Athlete, loggerService: boolean, utcOffset: number
    // ): Promise<Array<AggregateDirectiveType>> 
) => {
    const updatesResults: Array<AggregateDirectiveType> = []
    debugger;
    // Loop through the Aggregate Collections
    await Promise.all(allIPScoreAggregateCollections.athlete
        .map(async (athletecollectionMeta) => {
            const { collection } = athletecollectionMeta;
            const updateAthleteDirective: AggregateIpScoreDirective = {
                collection,
                message: `Update ${collection} Docs.`,
                athlete,
                athleteWIP: undefined,
                athleteDocuments: undefined,
                utcOffset,
                loggerService,
                t
            };

            const { dateTime } = athlete.currentIpScoreTracking

            const entry: AthleteBiometricEntryInterface = undefined;
            const docID = getAggregateDocumentID(updateAthleteDirective, entry, entry ? entry.dateTime : dateTime || firestore.Timestamp.now()); // TODO: Check datetime here

            // Push updateIpScoreDocument Promise to the athletePromises array
            // athletePromises.push()// Promise.resolve([updateAthleteDirective])

            const theResult = await executeAggregate(updateAthleteDirective, docID, undefined)
                .then(async (fetchAthleteDocsDirective: AggregateIpScoreDirective) => {
                    debugger;
                    // TODO: check fetchAthleteDocsDirective === AggregateIpScoreDirective ie has *athleteDocuments*
                    debugger
                    if (loggerService) {
                        console.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
                    }

                    athlete.includeGroupAggregation = true;

                    if (athlete.includeGroupAggregation && athlete.groups && isArray(athlete.groups) && athlete.groups.length > 0) {

                        const groupIds = athlete.groups.map((group) => group.uid || group.groupId)
                        const groupResults = await processGroupIPScoreAggregationRequest(groupIds, fetchAthleteDocsDirective, loggerService);

                        return groupResults;
                        // const groupPromises = await athlete.groups.map(async group => {

                        //     const groupUID = group.uid ? group.uid : group.groupId;

                        //     const allTheGroupRes = await getAthleteDocs(updateAthleteDirective, groupUID)
                        //         .then(async (groupDirective: AggregateIpScoreDirective) => {

                        //             removeUndefinedProps(groupDirective.athleteDocuments);

                        //             if (groupDirective.athleteDocuments) {

                        //                 const groupDocID = getAggregateDocumentID(groupDirective, undefined);

                        //                 const theGroupRes: AggregateIpScoreDirective = await executeAggregate(groupDirective, groupDocID, undefined)
                        //                     .then(async (groupDocsDirective) => {
                        //                         if (loggerService) {
                        //                             // console.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
                        //                             console.log('Group Aggregation Complete');
                        //                         }
                        //                         // if (loggerService) {
                        //                         //     console.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
                        //                         // }
                        //                         if (updateAthleteDirective !== groupDocsDirective) {
                        //                             debugger;
                        //                         }

                        //                         return groupDirective;
                        //                     });

                        //                 return theGroupRes;
                        //             } else {
                        //                 console.warn('No Athletes returned for Group Aggregation', groupDirective);

                        //                 return groupDirective;
                        //             }
                        //         });

                        //     return allTheGroupRes;


                        //     // allIPScoreAggregateCollections.group.map((groupCollectionMeta) => {
                        //     //     const { collection: theGroupCollection } = groupCollectionMeta;
                        //     //     const updateGroupDirective: AggregateIpScoreDirective = {
                        //     //         collection: theGroupCollection,
                        //     //         message: `Update ${theGroupCollection} Docs.`,
                        //     //         athlete,
                        //     //         athleteWIP: undefined,
                        //     //         athleteDocuments: undefined,
                        //     //         utcOffset: 0,
                        //     //         loggerService
                        //     //     };

                        //     //     // const entry = undefined;
                        //     //     const groupDocID = getAggregateDocumentID(updateGroupDirective, entry);


                        //     //     debugger;

                        //     //     // TODO: ALL NEEDS TESTING

                        //     //     // Push updateIpScoreDocument Promise to the athletePromises array
                        //     //     athletePromises.push(// Promise.resolve([updateAthleteDirective])

                        //     //         executeAggregate(updateGroupDirective, groupDocID, undefined)
                        //     //             .then(async (groiupAggregationDocsDirective) => {
                        //     //                 if (loggerService) {
                        //     //                     console.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
                        //     //                 }
                        //     //                 // if (loggerService) {
                        //     //                 //     console.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
                        //     //                 // }
                        //     //                 if (updateGroupDirective !== groiupAggregationDocsDirective) {
                        //     //                     debugger;
                        //     //                 }

                        //     //                 return updateGroupDirective;
                        //     //             }));
                        //     // });





                        // });

                        // return await Promise.all(groupPromises)
                    } else

                        return [updateAthleteDirective]
                    // if (loggerService) {
                    //     console.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
                    // }
                    // if (updateAthleteDirective !== fetchAthleteDocsDirective) {
                    //     debugger;
                    // }



                    // return updateAthleteDirective;
                });
            updatesResults.push(...theResult);
        }));

    // Await all promises
    debugger;
    // const theRes = await Promise.all(athletePromises);
    // const theRes = updatesResults;
    // const theressss = [].concat(...theRes) as Array<AggregateDirectiveType>;
    debugger;

    return updatesResults;
};
