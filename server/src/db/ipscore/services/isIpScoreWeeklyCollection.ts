
import { IpScoreAggregateHistoryCollection } from '../enums/firestore-ipscore-aggregate-history-collection'
import { IpScoreAggregateCollection } from '../enums/firestore-ipscore-aggregate-collection'

export const isIpScoreWeeklyCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    const result =
        collection === IpScoreAggregateCollection.AthleteIpScoreWeekly ||
        collection === IpScoreAggregateCollection.GroupIpScoreWeekly ||
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory;
    return result
};
