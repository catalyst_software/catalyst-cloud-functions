// import { EntityType } from '../../../models/enums/entity-type';
import { WellAggregateBinaryHistoryCollection } from '../../biometricEntries/enums/firestore-aggregate-binary-history-collection';
import { WellAggregateCollection } from '../../biometricEntries/enums/firestore-aggregate-collection';
import { WellAggregateHistoryCollection } from '../../biometricEntries/enums/firestore-aggregate-history-collection';
// import { isIPScoreAthleteCollection, isIPScoreHistoryCollection, isIPScoreWeeklyCollection, isIPScoreWeeklyHistoryCollection } from '../aliases/collections';
import { IpScoreAggregateCollection } from '../enums/firestore-ipscore-aggregate-collection';
import { IpScoreAggregateHistoryCollection } from '../enums/firestore-ipscore-aggregate-history-collection';
import { IpScoreAggregateDocumentType } from '../types/ip-score-aggregate-types';
import { isAthleteCollection, isWeeklyHistoryCollection } from '../../biometricEntries/aliases/collections';
import { HealthComponentType } from '../../biometricEntries/well/enums/health-component-type';
import { AggregateDocumentType } from '../../biometricEntries/well/enums/aggregate-document-type';
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type';
import { AggregateEntityType } from '../../biometricEntries/enums/aggregate-entity-type';
import { isIPScoreAthleteCollection, isIPScoreWeeklyHistoryCollection, isIPScoreHistoryCollection, isIPScoreWeeklyCollection } from './ipscore-aggregate-collections';
export const getSharedTypes = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection | WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection,
    entryType: number
) => {
    let healthComponentType: HealthComponentType;
    let aggregateEntityType: AggregateEntityType;
    let aggregateDocumentType:
        | AggregateDocumentType
        | IpScoreAggregateDocumentType;

    switch (+entryType) {
        case LifestyleEntryType.Sleep:
        case LifestyleEntryType.Mood:
            healthComponentType = HealthComponentType.Brain;
            break;
        case LifestyleEntryType.Fatigue:
        case LifestyleEntryType.Pain:
            healthComponentType = HealthComponentType.Body;
            break;
        case LifestyleEntryType.Food:
            healthComponentType = HealthComponentType.Food;
            break;

        case LifestyleEntryType.Training:
            healthComponentType = HealthComponentType.Training;
            break;

        case LifestyleEntryType.Distance:
            healthComponentType = HealthComponentType.Training;
            break;

        case LifestyleEntryType.HeartRate:
            healthComponentType = HealthComponentType.Training;
            break;

        case LifestyleEntryType.IpScore:
            healthComponentType = HealthComponentType.IpScore;
            break;

        case LifestyleEntryType.Steps:
            healthComponentType = HealthComponentType.Training;
            break;
    }

    switch (+entryType) {
        case LifestyleEntryType.IpScore:
            // Set AggregateEntityType
            isIPScoreAthleteCollection(collection as IpScoreAggregateCollection)
                ? ((aggregateEntityType) = AggregateEntityType.Athlete)
                : (aggregateEntityType = AggregateEntityType.Group);
            // Set AggregateDocumentType
            isIPScoreWeeklyHistoryCollection(collection as IpScoreAggregateHistoryCollection)
                ? (aggregateDocumentType = AggregateDocumentType.WeeklyHistory)
                : (aggregateDocumentType = AggregateDocumentType.MonthlyHistory);
            break;

        default:
            // Set AggregateEntityType
            isAthleteCollection(collection as WellAggregateCollection)
                ? (aggregateEntityType = AggregateEntityType.Athlete)
                : (aggregateEntityType = AggregateEntityType.Group);
            // Set AggregateDocumentType
            isWeeklyHistoryCollection(collection as WellAggregateHistoryCollection)
                ? (aggregateDocumentType = AggregateDocumentType.WeeklyHistory)
                : (aggregateDocumentType = AggregateDocumentType.MonthlyHistory);
            break;
    }

    !isIPScoreHistoryCollection(collection as IpScoreAggregateHistoryCollection)
        // Not Historic
        ? isIPScoreWeeklyCollection(collection as IpScoreAggregateCollection)
            ? (aggregateDocumentType = AggregateDocumentType.Weekly)
            : (aggregateDocumentType = AggregateDocumentType.Monthly)
        // Historic
        : isIPScoreWeeklyCollection(collection as IpScoreAggregateCollection)
            ? (aggregateDocumentType = AggregateDocumentType.WeeklyHistory)
            : (aggregateDocumentType = AggregateDocumentType.MonthlyHistory);

    if (!aggregateDocumentType) {
        // Daily
        aggregateDocumentType = AggregateDocumentType.Daily;
    }

    return { healthComponentType, aggregateEntityType, aggregateDocumentType };
};
