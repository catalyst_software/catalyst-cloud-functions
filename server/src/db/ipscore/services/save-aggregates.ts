import { isObject } from 'lodash'
import { firestore } from 'firebase-admin'

import { db } from '../../../shared/init/initialise-firebase'
import { Log } from '../../../shared/logger/logger'
import { AggregateLogLevel } from '../../biometricEntries/enums/aggregate-log-level'
import { LogLevel } from '../../biometricEntries/enums/log-level'
import { SocketLogDirective } from '../../../shared/logger/interfaces/socket-log-directive'
import { AllIpScoreAggregateType } from '../types/ip-score-aggregate-types';
import { AllAggregateType } from '../../../models/types/biometric-aggregate-types'

export const saveAggregateDoc = async (
    updatedDoc: AllIpScoreAggregateType | AllAggregateType,
    directive: any //  AggregateIpScoreDirective
) => { //: Promise<AthleteIpScoreWeeklyAggregate | AthleteIpScoreWeeklyHistoryAggregate | AthleteIpScoreMonthlyHistoryAggregate | AthleteIpScoreWeeklyAggregateInterface | AthleteWeeklyAggregate | AthleteWeeklyHistoryAggregate | AthleteMonthlyHistoryAggregate | BinaryBiometricHistoryAggregate> => {
    const aggregateCollectionRef: firestore.CollectionReference = db.collection(
        updatedDoc.collection
    );

    const { t } = directive


    const tranaction: FirebaseFirestore.Transaction = t;

    try {
        const aggregateDoc = updatedDoc;

        const { ...theDocument } = aggregateDoc;

        removeUndefinedProps(theDocument);

        const docRef = aggregateCollectionRef
            .doc(theDocument.uid);

        if (tranaction) {
            return tranaction.get(docRef)
                .then(() => {
                    // Add one person to the city population.
                    // Note: this could be done without a transaction
                    //       by updating the population using FieldValue.increment()
                    // let newPopulation = doc.data().population + 1;
                    tranaction.set(docRef, theDocument);

                    handleNextRequest('writeResult');

                    return updatedDoc
                }).catch((err) => handleError(err, updatedDoc))

        } else {
            return await docRef
                .set(theDocument)
                .then((writeResult: firestore.WriteResult) => {
                    Log({
                        type: AggregateLogLevel.SavedDoc,
                        message: `${updatedDoc.collection} Document Saved!`,
                        data: {
                            collection: updatedDoc.collection,
                            document: updatedDoc,
                            rest: writeResult,
                        },

                        level: LogLevel.Debug,
                    });

                    handleNextRequest(writeResult);

                    return updatedDoc
                }).catch((err) => handleError(err, updatedDoc))
        }

    } catch (err) {
        debugger;
        const ff: SocketLogDirective = {
            message: 'Unable to save document! - catch err => ' + err,
            data: {
                rest: { updatedDoc: JSON.stringify(updatedDoc), error: err },
            },
            level: LogLevel.Error,
        };
        Log(ff);
        handleNextRequest(undefined, err);
        debugger;

        return updatedDoc
    }

    // return '';
};
function handleError(err, updatedDoc) {

    // .catch(err => {
    debugger;
    const ff: SocketLogDirective = {
        message: 'Unable to save document! - err => ' + err,
        data: {
            rest: {
                updatedDoc: JSON.stringify(updatedDoc),
                error: err,
            },
        },
        level: LogLevel.Error,
    };
    Log(ff);

    handleNextRequest(undefined, err);
    return updatedDoc
    // })
}
function handleNextRequest(writeResult?, err?) {
    if (err) {
        //
    }
    // const queueEntry = getQueueDirectives(entry)

    const socketDirective: SocketLogDirective = {
        type: AggregateLogLevel.SavedDoc,
        // entity: updatedDoc.entry,
        message: `Processing next item in the queue. ${
            AggregateLogLevel.SavedDoc
            }`,
        data: {
            // savedValue: entry.value,
            rest: { writeResult: writeResult },
        },
    };

    Log(socketDirective)
}

export function removeUndefinedProps(theDoc: { [key: string]: any }) {
    Object.keys(theDoc).forEach(key => {
        (theDoc[key] === undefined ||
            theDoc[key] === null ||
            theDoc[key] === 'entry' ||
            theDoc[key] === '_data' ||
            theDoc[key] === 'data') &&
            delete theDoc[key];

        if (isObject(theDoc[key])) {
            removeUndefinedProps(theDoc[key])
        }
    })
}

export function setNANtoZero(theDoc: { [key: string]: any }, savedoc) {

    let updateDoc = savedoc
    Object.keys(theDoc).forEach(key => {

        if (!isObject(theDoc[key]) && isNaN(theDoc[key])) {
            updateDoc = true
            console.log(`theDoc[key] ${key}`, theDoc[key]);
            theDoc[key] = 0

            console.log(`theDoc[key] ${key} NOW`, theDoc[key]);
        }

        if (isObject(theDoc[key])) {
            setNANtoZero(theDoc[key], updateDoc)
        }
    })

    return updateDoc
}
