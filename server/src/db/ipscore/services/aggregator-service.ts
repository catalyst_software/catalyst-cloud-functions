// import { Observer, Observable } from 'rxjs'

// import { DocMngrActionTypes } from '../enums/document-manager-action-types'
// // import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type';


// import { LogDirective } from '../../../shared/logger/interfaces/log-directive'

// // import { AthleteBiometricEntryModel } from '../../../models/well/athelete-biometric-ent

// import { LogLevel } from '../enums/log-level'

// // import { DynamicAggregator } from '../aggregation/dynamic-aggregator';
// // import { LogError } from '../../../shared/logger/logger';
// import { AthleteBiometricEntry } from '../../../models/well/interfaces/athelete-biometric-entry'
// import { aggregateDocManagerSubject$ } from '../managers/aggregate-document-manager'
// import { AggregateLogLevel } from '../enums/aggregate-log-level'
// import { Athlete } from '../../../models/athlete/interfaces/athlete'
// import { Group } from '../../../models/group/interfaces/group'
// import { AthleteBiometricEntryModel } from '../../../models/well/athelete-biometric-entry.model'
// import { HistoryAggregateType } from '../../../models/types/biometric-aggregate-types'
// import { LogInfo } from '../../../shared/logger/logger';
// import { addDocumentToQueue, removeDocumentFromQueue } from '../../biometricEntries/services/addDocumentToQueue';
// import { handleAggregate } from '../../biometricEntries/services/utils';
// import { getQueue, getQueueDirectives } from '../../biometricEntries/services/utils/getQueueID';


// export const socket = require('socket.io-client')('http://97.74.4.116:8881')
// export const docManagerService = new AggregateDocumentManagerService()
// export let myObserver: Observable<AggregateDocumentDirective>
// interface IDictionary {
//     [index: string]: AggregateDocumentDirective[]
// }
// export const queue = {} as IDictionary

// interface IDictionary2 {
//     [index: string]: AthleteBiometricEntryModel[]
// }
// export const queue2 = {} as IDictionary2

// export class AggregatorService {
//     // implements AggregatorServiceInterface {


//     init() {
//         LogInfo('aggrsrv process.env', process.env)
//         const directive: LogDirective = {
//             message: 'Number of Observers',
//             data: {
//                 value: aggregateDocManagerSubject$.observers.length,
//             },
//             method: {
//                 name: 'AggregatorService.constructor(biometricEntry)',
//                 line: 43,
//             },
//             level: LogLevel.Info,
//         }
//         // LogInfo(directive)

//         LogInfo(directive.message, directive)

//         if (!aggregateDocManagerSubject$.observers.length) {
//             // Create Subscribtion on Application Init, passing the
//             // entry as theinitial value
//             this.InitializeService()
//         } else {
//             LogInfo('AggregatorService already initialised!!!!!!!!!!!')
//         }
//     }
//     public processAggregateRequest(
//         entity: AthleteBiometricEntry | Athlete | Group,
//         type: DocMngrActionTypes
//     ): Promise<AggregateDocumentDirective> {
//         aggregateDocManagerSubject$.observers.map(
//             (observer: Observer<AggregateDocumentDirective>) => {
//                 observer.next({
//                     actionType: type,
//                     entity,
//                     message: `updateAggregates(entry) - observer.next().${type}`,
//                 })
//             }
//         )

//         // myObserver = 
//         return aggregateDocManagerSubject$.asObservable().toPromise()
//         // return myObserver.toPromise()
//     }
//     private InitializeService() {
//         // LogInfo('Initializing AggregatorService!!!');

//         if (
//             aggregateDocManagerSubject$.observers &&
//             aggregateDocManagerSubject$.observers.length > 1
//         ) {
//             LogInfo(
//                 'InitializeService.subscribe()ed. Number of Observers now?!',
//                 aggregateDocManagerSubject$.observers.length
//             )
//         }

//         return true
//     }

//     saveHistoryAggregate(aggregateHistoryDocument: HistoryAggregateType) {
//         return docManagerService.saveHistoryAggregate(aggregateHistoryDocument)
//     }

//     handleRead(entry: AthleteBiometricEntry) {
//         return docManagerService.updateAggregates(entry).then((data) => {
//             console.log('complete?!.....................')
//             // aggregateDocManagerSubject$.complete();
//         })
//         // return this.handleQueue(entry)
//     }

//     handleAggregate(directive: AggregateDocumentDirective) {
//         // return docManagerService.updateAggregates(entry);
//         const actionType = directive.actionType
//         if (actionType === DocMngrActionTypes.AddToQueue) {
//             addDocumentToQueue(directive)
//         }
//         return handleAggregate(directive).then(data => {
//             LogInfo(data)
//         })
//     }

//     handleReadNext(directive: AggregateDocumentDirective) {
//         return this.removeFromQueue(directive)
//     }

//     handleHistoricCreate(entity: Athlete | Group, uid?, doc?) {
//         LogInfo(`calling handleHistoricCreate -> ${entity}`)
//         docManagerService.createHistoryAggregateDocs(entity)
//     }

//     handleHistoricUpdate(directive: AggregateDocumentDirective) {
//         // handleHistoricUpdate(entity: AthleteBiometricEntryModel, doc?) {
//         // addDocumentToQueue(directive)
//         return docManagerService.updateHistoryAggregates(directive)
//     }

//     handleDefault(directive: AggregateDocumentDirective) {
//         socket.emit(AggregateLogLevel.Info, {
//             type: AggregateLogLevel.Info,
//             data: {
//                 message: ` event received by AggregatorService handleDefault()`,
//                 directive,
//                 docMngrActionType: DocMngrActionTypes.CreateHistoryDocs,
//                 equal: DocMngrActionTypes.CreateHistoryDocs,
//             },
//             level: LogLevel.Debug,
//         })
//         LogInfo(
//             'event received by AggregatorService handleDefault()',
//             directive
//         )
//     }

//     private removeFromQueue(directive: AggregateDocumentDirective) {
//         const { entity: entry } = <{ entity: AthleteBiometricEntryModel }>(
//             directive
//         )

//         // const queueEntry = getQueueDirectives(entry);
//         const fullQueue = getQueue()

//         // var obj = { a: 'test1', b: 'test2' };

//         const itemsInQueue = []

//         Object.keys(fullQueue).forEach(key => {
//             fullQueue[key].forEach(item => {
//                 itemsInQueue.push(item)
//             })
//             fullQueue[key].length > 0
//         })

//         const entriesById = getQueueDirectives(entry)
//         // const currentQueue = queue;
//         const remainingResults = removeDocumentFromQueue(directive)

//         const { completed, remaining} = remainingResults
//         // entriesById.pop()
//         LogInfo('Completed item removed from queue', completed)
//         // aggregateDocManagerSubject$.complete()
//         if (remaining && remaining.length > 0) {
//             // const selectedDirective = getQueueEntriesById(entry).shift();
//             // LogInfo('....... Running next aggregate request', selectedDirective)
//             LogInfo('....... Running next aggregate request')
//             // return selectedDirective;
//             // return docManagerService.updateAggregates(selectedDirective.entity);
//         } else {
//             aggregateDocManagerSubject$.complete()
//         }
//         //    .next(directive)
//         return true
//     }
// }
