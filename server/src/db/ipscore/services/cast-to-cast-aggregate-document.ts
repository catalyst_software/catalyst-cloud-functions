
import { AthleteBiometricEntryInterface } from '../../biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { AggregateDirectiveType } from "../../../models/types/biometric-aggregate-types";
import { Athlete } from "../../../models/athlete/interfaces/athlete";
import { firestore } from "firebase-admin";
import { IpScoreDynamicAggregateDocument } from "./interfaces/dynamic-aggregate-document";
import { getSharedTypes } from "./get-shared-types";
import { LifestyleEntryType } from "../../../models/enums/lifestyle-entry-type";
import { IpScoreAggregateCollection } from "../enums/firestore-ipscore-aggregate-collection";
import { IpScoreAggregateHistoryCollection } from "../enums/firestore-ipscore-aggregate-history-collection";
import { getEntityDetails } from './get-entity-details';
import { setAggregateSharedDocProps } from './set-aggregate-shared-doc-props';

export const castAggregateDocument = (
    directive: AggregateDirectiveType,
    documentId: string,
    athlete: Athlete,
    entry: AthleteBiometricEntryInterface,
    snapshot: firestore.DocumentSnapshot
): IpScoreDynamicAggregateDocument => {

    const { collection, group, utcOffset } = directive;

    const {
        healthComponentType,
        aggregateEntityType,
        aggregateDocumentType
    } = getSharedTypes(collection, LifestyleEntryType.IpScore);

    const lifestyleEntryType = !!entry ? entry.lifestyleEntryType : LifestyleEntryType.IpScore;

    const { entityId, entityName } = getEntityDetails(group || athlete); // TODO: SORT METHOD

    // TODO: FIX TYPING
    let record: any; // IpScoreAggregateInterface;

    // if (loggerService) {
    const message = snapshot.exists
        ? 'Casting Retrieved Document'
        : 'Casting New Document';

    // loggerService.info(message);
    console.info(message);
    // }


    let organizationId

    if (athlete) {
        const { organizations } = athlete;

        organizationId = (organizations && organizations.length) ? organizations[0].organizationId : athlete.organizationId;

    } else {
        organizationId = group.organizationId
    }

    record = {
        uid: documentId,
        organizationId,
        entityId,
        entityName,
        collection,
        healthComponentType,
        aggregateEntityType,
        aggregateDocumentType,
        lifestyleEntryType,
        ...snapshot.exists ? { ...snapshot.data(), uid: snapshot.id } : {},
        entry
    };
    let dateTime, creationTimestamp
    if (athlete) {
        const { metadata, currentIpScoreTracking } = athlete;
        creationTimestamp = metadata.creationTimestamp;
        dateTime = currentIpScoreTracking.dateTime;
    } else {
        creationTimestamp = group.creationTimestamp
        
        dateTime = entry.dateTime;
    }


    const {
        sharedBaseProperties,
        weeklySharedProps
    } = setAggregateSharedDocProps(
        aggregateEntityType,
        entityId,
        entityName,
        lifestyleEntryType,
        organizationId,
        creationTimestamp,
        collection,
        documentId,
        dateTime,
        utcOffset || 0
    );

    const ipScoreAggregateDocument = new IpScoreDynamicAggregateDocument(collection, {
        ...sharedBaseProperties,
        ...weeklySharedProps,
        ...record,
        collection: collection as IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
    });

    return ipScoreAggregateDocument;
};
