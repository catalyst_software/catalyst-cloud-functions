import { IpScoreAggregateCollection } from '../enums/firestore-ipscore-aggregate-collection';
import { IpScoreAggregateHistoryCollection } from '../enums/firestore-ipscore-aggregate-history-collection';
import { WellAggregateCollection } from '../../biometricEntries/enums/firestore-aggregate-collection';
import { WellAggregateHistoryCollection } from '../../biometricEntries/enums/firestore-aggregate-history-collection';
import { WellAggregateBinaryHistoryCollection } from '../../biometricEntries/enums/firestore-aggregate-binary-history-collection';
// const getAthleteDocs2 = (
//     athletes,
//     collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
// ) => {
//     // let documentRef1 = db.doc('col/doc1')
//     // let documentRef2 = db.doc('col/doc2')
//     // let arr = [documentRef1, documentRef2]
//     const athleteAgrretgeDocsRefs = new Array<
//         FirebaseFirestore.DocumentReference
//     >()
//     for (const athlete of athletes) {
//         const ref = db.collection(collection).doc(`${athlete.uid}_${LifestyleEntryType.IpScore}`)
//         athleteAgrretgeDocsRefs.push(ref)
//     }
//     const first = athleteAgrretgeDocsRefs.shift()
//     // return db.getAll(first, ...arr).then(docs => {
//     //     docs.map((doc, index) => {
//     //         console.log(`Document ${index}: ${JSON.stringify(docs[0])}`)
//     //     })
//     //     return docs
//     // })
//     return db.getAll(first, ...athleteAgrretgeDocsRefs).then(docs => {
//         docs.map((doc, index) => {
//             console.log(`Document ${index}: ${JSON.stringify(doc)}`)
//             // console.log(`Document ${index}: ${JSON.stringify(docs[index])}`)
//         })
//         return docs
//             .filter(snap => snap.exists)
//             .map(doc => {
//                 return {
//                     uid: doc.id,
//                     ...(doc.data() as AthleteIpScoreDocumentType),
//                 }
//             })
//     })
// }
export const getGroupCollectionFromAthleteCollection = (collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
    | WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection) => {
    let groupCollection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection;
    switch (collection) {
        case IpScoreAggregateCollection.AthleteIpScoreWeekly:
            groupCollection = IpScoreAggregateCollection.GroupIpScoreWeekly;
            break;
        // case IpScoreAggregateCollection.AthleteIpScoreMonthly:
        //     groupCollection = IpScoreAggregateCollection.GroupIpScoreMonthly
        //     break
        case IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory:
            groupCollection =
                IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory;
            break;
        case IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory:
            groupCollection =
                IpScoreAggregateHistoryCollection.GroupIpScoreMonthlyHistory;
            break;
    }
    return groupCollection;
};
