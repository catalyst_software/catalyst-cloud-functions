import { WellAggregateCollectionType, IPScoreAggregateCollectionType } from "../../../models/types/biometric-aggregate-types";
import { IpScoreAggregateCollection } from "../enums/firestore-ipscore-aggregate-collection";
import { IpScoreAggregateHistoryCollection } from "../enums/firestore-ipscore-aggregate-history-collection";


export const isIPScoreWeeklyCollection = (
    collection: WellAggregateCollectionType | IPScoreAggregateCollectionType
) => {

    const result = (
        collection === IpScoreAggregateCollection.AthleteIpScoreWeekly ||
        collection === IpScoreAggregateCollection.GroupIpScoreWeekly
    );

    // const result =
    //     collection === IpScoreAggregateCollection.AthleteIpScoreWeekly; // ||
    // // collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory;

    return result;
};

export const isIPScoreAthleteCollection = (
    collection: WellAggregateCollectionType | IPScoreAggregateCollectionType
) => {
    return (
        collection === IpScoreAggregateCollection.AthleteIpScoreWeekly ||
        // collection === IpScoreAggregateCollection.AthleteIpScoreMonthly ||
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory
    );
};

export const isIPScoreAthleteHistoryCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    const result = collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory;

    return result;
};

// export const isIPScoreAthleteMonthlyCollection = (
//     collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
// ) => {
//     return collection === IpScoreAggregateCollection.AthleteIpScoreMonthly;
// };

// export const isIPScoreGroupMonthlyCollection = (
//     collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
// ) => {
//     return collection === IpScoreAggregateCollection.GroupIpScoreMonthly;
// };

export const isIPScoreWeeklyHistoryCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    return (
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory
    );
};

export const isIPScoreGroupWeeklyCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    return (
        collection === IpScoreAggregateCollection.GroupIpScoreWeekly
    );
};

export const isIPScoreGroupCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    return (
        collection === IpScoreAggregateCollection.GroupIpScoreWeekly // ||
        // collection === IpScoreAggregateCollection.GroupIpScoreMonthly
    );
};

// export const isWeeklyCollection = (collection: AggregateCollection) => {
//     return collection === AggregateCollection.AthleteWeekly || collection === AggregateCollection.GroupWeekly;
// }

// export const isAthleteCollection = (collection: AggregateCollection) => {
//     return collection === AggregateCollection.AthleteWeekly || collection === AggregateCollection.AthleteMonthly;
// }

export const isIPScoreHistoryCollection = (
    collection: IpScoreAggregateCollection | IpScoreAggregateHistoryCollection
) => {
    const result =
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory ||
        collection === IpScoreAggregateHistoryCollection.GroupIpScoreMonthlyHistory;

    return result;
};

// export const isWeeklyHistoryCollection = (collection: AggregateHistoryCollection) => {
//     return collection === AggregateHistoryCollection.AthleteWeeklyHistory || collection === AggregateHistoryCollection.GroupWeeklyHistory;
// }
