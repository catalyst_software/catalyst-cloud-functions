import { firestore } from "firebase-admin";


import { WellAggregateCollectionType, IPScoreAggregateCollectionType } from "../../../models/types/biometric-aggregate-types";
import { getWeekNumber, getWeekFromDate, getMonthFromDate, getMonthToDate } from "../../../shared/utils/moment";
import { LifestyleEntryType } from "../../../models/enums/lifestyle-entry-type";
import { createIPScoreDailies } from "./create-ipscore-dailies";
import { isWeeklyCollection } from "../../biometricEntries/aliases/interfaces";
import { AggregateEntityType } from "../../biometricEntries/enums/aggregate-entity-type";

export const setAggregateSharedDocProps = (
    aggregateEntityType: AggregateEntityType,
    entityId: string,
    entityName: string,
    lifestyleEntryType: LifestyleEntryType,
    organizationId: string,
    creationTimestamp: firestore.Timestamp,
    collection: WellAggregateCollectionType | IPScoreAggregateCollectionType,
    documentId: string,
    dateTime: firestore.Timestamp,
    utcOffset: number
) => {

    const weekNumber = getWeekNumber(creationTimestamp, dateTime, utcOffset);

    const sharedBaseProperties = {
        uid: documentId,
        collection,
        organizationId,
        entityId,
        entityName,
        lifestyleEntryType
    };

    const isWeekly = isWeeklyCollection(collection)

    const weeklySharedProps = isWeekly //|| isWeeklyWELL || isWeeklyIP
        ? {
            weekNumber,
            dailies: createIPScoreDailies(
                entityId,
                entityName,
                lifestyleEntryType,
                organizationId,
                collection,
                getWeekFromDate(dateTime, utcOffset)
            )
        }
        : { dailies: undefined, weekNumber: undefined };

    const monthlySharedProps = {
        fromDate: getMonthFromDate(dateTime, utcOffset),
        toDate: getMonthToDate(dateTime, utcOffset)
    };

    return { sharedBaseProperties, weeklySharedProps, monthlySharedProps };
};
