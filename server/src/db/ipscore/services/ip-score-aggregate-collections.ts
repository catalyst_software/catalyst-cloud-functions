import { IpScoreAggregateCollection } from '../enums/firestore-ipscore-aggregate-collection'
import { IpScoreAggregateHistoryCollection } from '../enums/firestore-ipscore-aggregate-history-collection'

export const allIPScoreAggregateCollections = {
    athlete: [
        {
            collection: IpScoreAggregateCollection.AthleteIpScoreWeekly,
        },
        {
            collection:
                IpScoreAggregateHistoryCollection.AthleteIpScoreWeeklyHistory,
        },
        {
            collection:
                IpScoreAggregateHistoryCollection.AthleteIpScoreMonthlyHistory,
        },
    ],
    group: [
        {
            collection: IpScoreAggregateCollection.GroupIpScoreWeekly,
        },
        {
            collection:
                IpScoreAggregateHistoryCollection.GroupIpScoreWeeklyHistory,
        },
        {
            collection:
                IpScoreAggregateHistoryCollection.GroupIpScoreMonthlyHistory,
        },
    ],
};
