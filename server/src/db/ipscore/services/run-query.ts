import { firestore } from 'firebase-admin'

import { WellAggregateCollection } from '../../biometricEntries/enums/firestore-aggregate-collection'
import { WellAggregateHistoryCollection } from '../../biometricEntries/enums/firestore-aggregate-history-collection'
import { IpScoreAggregateCollection } from '../enums/firestore-ipscore-aggregate-collection'
import { IpScoreAggregateHistoryCollection } from '../enums/firestore-ipscore-aggregate-history-collection'
import { WellAggregateBinaryHistoryCollection } from '../../biometricEntries/enums/firestore-aggregate-binary-history-collection'
export const runQuery = async (
    docRef: firestore.DocumentReference,
    collection:
        WellAggregateCollection |
        WellAggregateHistoryCollection |
        WellAggregateBinaryHistoryCollection |

        IpScoreAggregateCollection |
        IpScoreAggregateHistoryCollection,

    documentID: string,
    loggerService: boolean,
    // isConnected: boolean
    t: FirebaseFirestore.Transaction

): Promise<firestore.DocumentSnapshot> => {

    if (t) {
        return t.get(docRef).catch(err => {

            return Promise.reject(err)
        })
    } else {
        return docRef.get().catch(err => {
            debugger;
            return Promise.reject(err)
        })
    }
};
