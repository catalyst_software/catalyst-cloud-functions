import { Athlete } from "../../../models/athlete/interfaces/athlete";
import { Group } from "../../../models/group/interfaces/group";

export const getEntityDetails = (entity: Athlete | Group) => {
    const entityId = entity.uid;
    const entityName = (entity as Athlete).profile ? `${(entity as Athlete).profile.firstName} ${(entity as Athlete).profile.lastName}` :  (entity as Group).groupName;

    return { entityId, entityName };
};
