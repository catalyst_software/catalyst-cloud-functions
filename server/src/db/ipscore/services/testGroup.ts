import { firestore } from 'firebase-admin';

import { GroupIndicator } from '../../biometricEntries/well/interfaces/group-indicator';
export const testGroup: GroupIndicator = {
    uid: 'WMvaLikyaTdKIiMc1prA-1',
    groupId: 'WMvaLikyaTdKIiMc1prA-1',
    groupName: 'Ian\'s group',
    organizationId: '',
    organizationName: '',
    creationTimestamp: firestore.Timestamp.now(),
    // athletes: [
    //     // {
    //     //     uid: 'gdzdzyM8nkQ823oqEB3HhCVWyPo2',
    //     //     firstName: 'Ian',
    //     //     lastName: 'Gouws2',
    //     //     ipScore: 0,
    //     //     isIpScoreDecrease: false,
    //     //     isIpScoreIncrease: false,
    //     //     profileImageURL: '',
    //     //     email: 'ian2@server.com',
    //     //     // groupName: 'Group1',
    //     //     creationTimestamp: firestore.Timestamp.now()
    //     // },
    //     // {
    //     //     uid: '6Wqkr2RjXiaASQsQnB9Ra9trjxV2',
    //     //     firstName: 'Piet',
    //     //     lastName: 'Gouws3',
    //     //     ipScore: 0,
    //     //     isIpScoreDecrease: false,
    //     //     isIpScoreIncrease: false,
    //     //     profileImageURL: '',
    //     //     email: 'ian3@server.com',
    //     //     // groupName: 'Group1',
    //     //     creationTimestamp: firestore.Timestamp.now()
    //     // },
    //     {
    //         uid: 'l7iiYF2wDKXtb1Jy83BuHEWtitd2',
    //         firstName: 'Ian',
    //         lastName: 'Gouws',
    //         ipScore: 0,
    //         runningTotalIpScore: 0,
    //         isIpScoreDecrease: false,
    //         isIpScoreIncrease: false,
    //         profileImageURL: '',
    //         email: 'ian@server.com',
    //         // groupName: 'Group1',
    //         subscriptionType: SubscriptionType.Free,
    //         firebaseMessagingIds: [],
    //         creationTimestamp: firestore.Timestamp.now()
    //     },
    // ],
};
