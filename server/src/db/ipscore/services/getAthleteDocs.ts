import { AggregateIpScoreDirective } from './interfaces/aggregate-ipscore-directive';
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type';
import { db } from '../../../shared/init/initialise-firebase';
import { isIPScoreWeeklyCollection } from '../aliases/collections';
import { firestore } from 'firebase-admin';
import { AthleteIpScoreDocumentType } from '../types/ip-score-aggregate-types';
import { getGroupCollectionFromAthleteCollection } from './getGroupCollection';
import { getWeekNumber } from '../../../shared/utils/moment';
import { FirestoreCollection } from '../../biometricEntries/enums/firestore-collections';
import { Group } from '../../../models/group/interfaces/group';
import { LogInfo } from '../../../shared/logger/logger';
import { removeUndefinedProps } from './save-aggregates';
// import { Group } from '../../../models/group/group.model';
/**
 * Retrieves multiple documents from Firestore.
 *
 * @param {...DocumentReference} documents - The document references
 * to receive.
 * @returns {Promise<Array.<DocumentSnapshot>>} A Promise that
 * contains an array with the resulting document snapshots.
 */
export const getAthleteDocs = (aggregateIpScoreDirective: AggregateIpScoreDirective, groupId: string) => {

    const { athlete, athleteWIP,
        // group: gg,
        collection } = aggregateIpScoreDirective;

    const athleteAgrretgeDocsRefs = new Array<FirebaseFirestore.DocumentReference>();

    const { currentIpScoreTracking } = athleteWIP || athlete

    const { dateTime, directive } = currentIpScoreTracking

    // const date = dateTime.toDate()

    try {

        return db
            .collection(FirestoreCollection.Groups)
            .doc(groupId)
            .get()
            .then(async (snapshot: firestore.DocumentSnapshot) => {
                if (snapshot.exists) {
                    const theGroup = snapshot.data() as Group;
                    // console.log('theGroup', theGroup);
                    console.log('theGroup athlete ip scores', theGroup.athletes.map((ath) => {
                        return {
                            uid: ath.uid,
                            name: ath.firstName + ' ' + ath.lastName,
                            runningIPScore: ath.runningTotalIpScore,
                            dailyIPScore: ath.ipScore
                        }
                    }));

                    let documentId = '';

                    // for (const theAthleteIndicator of theGroup.athletes) {
                    theGroup.athletes.forEach((theAthleteIndicator) => {

                        const { uid, isCoach, includeGroupAggregation, creationTimestamp } = theAthleteIndicator;

                        let includeAthlete = true;

                        if (isCoach && !includeGroupAggregation) {
                            includeAthlete = false;
                        }
                        console.log(`includeAthlete with uid ${theAthleteIndicator.uid}? => ${includeAthlete}`);

                        // console.warn('aggregateIpScoreDirective.utcOffset ==================================>>>>>>>>>>>>>>>>>>>>>>>>>>>>', aggregateIpScoreDirective.utcOffset)

                        if (includeAthlete) {
                            if (isIPScoreWeeklyCollection(collection)) {
                                documentId = `${uid}_${LifestyleEntryType.IpScore}_${getWeekNumber(creationTimestamp, dateTime, aggregateIpScoreDirective.utcOffset)}`;

                            }
                            else {
                                documentId = `${uid}_${LifestyleEntryType.IpScore}`;
                            }

                            // LogInfo(`Athlete documentId => ${documentId}`);

                            const ref = db
                                .collection(collection)
                                .doc(documentId);
                            athleteAgrretgeDocsRefs.push(ref);
                        }

                    });

                    if (athleteAgrretgeDocsRefs && athleteAgrretgeDocsRefs.length) {
                        return await db.getAll(...athleteAgrretgeDocsRefs)
                            .then(docs => {

                                LogInfo(`docs.length => ${docs.length}`);
                                const athleteDocuments = docs
                                    // .filter(snap => snap.exists)
                                    .map(doc => {

                                        if (doc.exists) {
                                            const entry = doc.data();

                                            const athleteDoc = {
                                                uid: entry.id,
                                                ...(entry as AthleteIpScoreDocumentType)
                                            };

                                            LogInfo(`retreived athleteDoc => ${athleteDoc.uid}`);
                                            return athleteDoc;
                                        } else {
                                            return undefined
                                        }

                                    }).filter((ath) => !!ath);

                                removeUndefinedProps(athleteDocuments);

                                LogInfo(`retreived athleteDocuments => ${athleteDocuments.length}`);

                                return {
                                    ...aggregateIpScoreDirective,
                                    collection: getGroupCollectionFromAthleteCollection(collection),
                                    message: `Update ${collection} Docs.`,
                                    athleteDocuments,
                                    group: theGroup
                                };
                            });
                    } else {
                        return Promise.resolve({
                            ...aggregateIpScoreDirective,
                            collection: getGroupCollectionFromAthleteCollection(collection),
                            message: `No athletes to aggregate for ${collection} Document with uid ${groupId}.`,
                            athleteDocuments: undefined
                        });
                    }
                }
                else {

                    LogInfo(`Can't find ${collection} Document with uid ${groupId}.`);
                    return Promise.reject({
                        ...aggregateIpScoreDirective,
                        collection: getGroupCollectionFromAthleteCollection(collection),
                        message: `Can't find ${collection} Document with uid ${groupId}.`,
                        athleteDocuments: undefined
                    });
                }
            });
    } catch {
        debugger;
        LogInfo(`An exceptio occured loading athlete documents for ${collection} Document with uid ${groupId}.`);
        return Promise.reject({
            ...aggregateIpScoreDirective,
            collection: getGroupCollectionFromAthleteCollection(collection),
            message: `An exceptio occured loading athlete documents for ${collection} Document with uid ${groupId}.`,
            athleteDocuments: undefined
        });
    }
};
