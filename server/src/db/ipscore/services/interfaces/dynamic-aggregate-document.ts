import { BaseRecord } from "../../../biometricEntries/well/documents/base-record";
import { CalculateIpScoreAggregates } from "../../aggregation/interfaces/calculate-ipscore-aggregates";
import { WellAggregateCollectionType, IPScoreAggregateCollectionType, AggregateDirectiveType } from "../../../../models/types/biometric-aggregate-types";
import { AthleteIpScoreMonthlyHistoryAggregateInterface } from "../../models/interfaces/athlete-ipscore-monthly-history-aggregate";
import { AthleteIpScoreWeeklyAggregateInterface, GroupIpScoreWeeklyAggregateInterface } from "../../models/interfaces";
import { AthleteIpScoreWeeklyHistoryAggregateInterface } from "../../models/interfaces/athlete-ipscore-weekly-history-aggregate";
import { AggregateIpScoreDirective } from "./aggregate-ipscore-directive";


// ATHLETE IPSCORE

import { AthleteIpScoreMonthlyHistoryAggregate as athleteIpScoreMonthlyHistoryAggregates } from "../../models/documents/aggregates/athelete-ipscore-monthly-history-aggregate.document";
import { AthleteIpScoreWeeklyAggregate as athleteIpScoreWeeklyAggregates } from '../../models/documents/aggregates/athelete-ipscore-weekly-aggregate.document';
import { AthleteIpScoreWeeklyHistoryAggregate as athleteIpScoreWeeklyHistoryAggregates } from '../../models/documents/aggregates/athelete-ipscore-weekly-history-aggregate.document';

// ATHLETE WELL
import {
    AthleteWeeklyAggregate as athleteBiometricWeeklyAggregates,
    AthleteWeeklyHistoryAggregate as athleteBiometricWeeklyHistoryAggregates,
    GroupWeeklyAggregate as groupBiometricWeeklyAggregates,
    GroupWeeklyHistoryAggregate as groupBiometricWeeklyHistoryAggregates
} from '../../../biometricEntries/models/documents/aggregates';

import { AthleteMonthlyHistoryAggregate as athleteBiometricMonthlyHistoryAggregates } from '../../../biometricEntries/models/documents/aggregates/athlete/athelete-monthly-history-aggregate.document';

// GROUP IPSCORE

// GROUP WELL
import {
    GroupIpScoreMonthlyHistoryAggregate as groupIpScoreMonthlyHistoryAggregates,
    GroupIpScoreWeeklyAggregate as groupIpScoreWeeklyAggregates
} from "../../../../models/documents/well";
import {
    GroupIpScoreWeeklyHistoryAggregate as groupIpScoreWeeklyHistoryAggregates
} from "../../models/documents/aggregates";
import { GroupMonthlyHistoryAggregate as groupBiometricMonthlyHistoryAggregates } from "../../../biometricEntries/models/documents/aggregates/group/group-monthly-history-aggregate.document";
import { GroupIpScoreMonthlyHistoryAggregateInterface } from "../../models/interfaces/group-ipscore-monthly-history-aggregate";
import { GroupIpScoreWeeklyHistoryAggregateInterface } from "../../models/interfaces/group-ipscore-weekly-history-aggregate";



export const AggregatorDocumentStore = {

    //// Athlete

    // IPSCORE Collections
    athleteIpScoreWeeklyAggregates,
    athleteIpScoreWeeklyHistoryAggregates,
    athleteIpScoreMonthlyHistoryAggregates,

    // WELL Collections
    athleteBiometricWeeklyAggregates,
    athleteBiometricWeeklyHistoryAggregates,
    athleteBiometricMonthlyHistoryAggregates,

    //// Group

    // IPSCORE Collections
    groupIpScoreWeeklyAggregates,
    groupIpScoreWeeklyHistoryAggregates,
    groupIpScoreMonthlyHistoryAggregates,

    // WELL Collections
    groupBiometricWeeklyAggregates,
    groupBiometricWeeklyHistoryAggregates,
    groupBiometricMonthlyHistoryAggregates
};


export class IpScoreDynamicAggregateDocument extends BaseRecord implements CalculateIpScoreAggregates {

    readonly organizationId: string;

    readonly uid: string;
    readonly entityId: string;
    readonly entityName: string;

    constructor(

        collection: WellAggregateCollectionType | IPScoreAggregateCollectionType,
        //  IpScoreAggregateCollection | IpScoreAggregateHistoryCollection | AggregateCollection | AggregateHistoryCollection | AggregateBinaryHistoryCollection | IpScoreAggregateCollection | IpScoreAggregateHistoryCollection | AggregateCollection | AggregateHistoryCollection | AggregateBinaryHistoryCollection,
        aggregateDocument: AthleteIpScoreMonthlyHistoryAggregateInterface
            | AthleteIpScoreWeeklyAggregateInterface
            | AthleteIpScoreWeeklyHistoryAggregateInterface
            | GroupIpScoreMonthlyHistoryAggregateInterface
            | GroupIpScoreWeeklyAggregateInterface
            | GroupIpScoreWeeklyHistoryAggregateInterface

    ) {

        super();

        if (
            AggregatorDocumentStore[collection] === undefined ||
            AggregatorDocumentStore[collection] === null
        ) {
            // Log({
            //     type: AggregateLogLevel.Info,
            //     message: `Aggregator for ${collection} could not be found in the IPScore Aggregator Document Store`,
            //     level: LogLevel.Sockets
            // });

            return undefined;
        }

        return new AggregatorDocumentStore[collection](aggregateDocument);
    }

    processAthleteIpScoreWeekly(
        directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyAggregateInterface {
        throw new Error('Method not implemented.');
    }

    processAthleteIpScoreWeeklyHistory(
        directive: AggregateIpScoreDirective
    ): any {
        throw new Error('Method not implemented.');
    }

    processAthleteIpScoreMonthlyHistory(
        directive: AggregateIpScoreDirective
    ): any {
        throw new Error('Method not implemented.');
    }


    processGroupIpScoreWeekly(
        directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyAggregateInterface {
        throw new Error('Method not implemented.');
    }

    processGroupIpScoreWeeklyHistory(
        directive: AggregateIpScoreDirective
    ): any {
        throw new Error('Method not implemented.');
    }

    processGroupIpScoreMonthlyHistory(
        directive: AggregateIpScoreDirective
    ): any {
        throw new Error('Method not implemented.');
    }

    calculateAggregates(
        directive: AggregateDirectiveType
    ): Promise<AggregateDirectiveType> {
        console.error('Implemented on Aggregator.');
        throw new Error('Implemented on Aggregator.');
    }
}
