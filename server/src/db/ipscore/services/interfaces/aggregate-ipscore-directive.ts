import { firestore } from 'firebase-admin';

// Enums
import { LogLevel } from '../../enums/log-level'

import { Group } from '../../../../models/group/group.model'
import { Athlete } from '../../../../models/athlete/interfaces/athlete';
import { IpScoreAggregateType, IpScoreHistoryAggregateType } from '../../types/ip-score-aggregate-types';
import { IPScoreAggregateCollectionType, WellAggregateCollectionType } from '../../../../models/types/biometric-aggregate-types';
import { IpScoreDynamicAggregateDocument } from './dynamic-aggregate-document';
import { AthleteWeeklyAggregate, GroupWeeklyAggregate } from '../../../../models/documents/well';
import { AthleteWeeklyHistoryAggregate } from '../../../biometricEntries/models/documents/aggregates';
import { AthleteIndicator } from '../../../biometricEntries/well/interfaces/athlete-indicator';


export interface AggregateIpScoreDirective {
    athleteDocuments: any // Array<AthleteIpScoreWeeklyAggregateInterface> |  Array<AthleteIpScoreWeeklyHistoryAggregateInterface>;

    athlete?: Athlete;
    athleteWIP?: {
        organizationId: string;
        includeGroupAggregation: boolean; // TODO: Confirm here
        currentIpScoreTracking: {
            dateTime: firestore.Timestamp,
            directive: { paths: Array<string> }
        }
    }
    group?: Group;
    athleteIndicators?: AthleteIndicator[],

    // actionType?: DocMngrActionTypes
    collection?: WellAggregateCollectionType | IPScoreAggregateCollectionType;

    // aggregateDocument?: IpScoreAllAggregateDocumentType;
    // updatedDocument?: AllIpScoreAggregateType;

    aggregateDocument?: IpScoreDynamicAggregateDocument | IpScoreAggregateType | IpScoreHistoryAggregateType;
    // TODO: any below
    updatedDocument?: AthleteWeeklyAggregate | GroupWeeklyAggregate | IpScoreDynamicAggregateDocument | IpScoreAggregateType | IpScoreHistoryAggregateType | AthleteWeeklyHistoryAggregate;

    message: string;
    // Generic BLOB for now, go wild!
    data?: any;
    uid?: string;
    utcOffset: number;
    // The logger server will be checking this property
    // and handling these events appropriately
    log?: {
        level: LogLevel
        // error: 0
        // warn: 1
        // info: 2
        // verbose: 3
        // debug: 4
        // silly: 5};
    };
    loggerService?: boolean; // LoggerService;
    t?: FirebaseFirestore.Transaction;
}
