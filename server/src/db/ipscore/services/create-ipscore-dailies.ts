import { firestore } from 'firebase-admin'
import moment from 'moment'

import { AllAggregateCollectionType } from '../../../models/types/biometric-aggregate-types'
import { AthleteIpScoreDailyAggregate } from '../models/interfaces'
import { getSharedTypes } from './get-shared-types'
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type'
import { AthleteIpScoreDailyAggregateModel } from '../models/athelete-ipscore-daily-aggregate.model'
import { AthleteDailyDocument } from '../../../models/documents/well'

export const createIPScoreDailies = (
    // athlete: Athlete,
    entityId,
    entityName,
    lifestyleEntryType: LifestyleEntryType,
    organizationId,
    collection: AllAggregateCollectionType,
    fromDate: firestore.Timestamp
): Array<AthleteIpScoreDailyAggregate> => {
    const {
        // healthComponentType,
        aggregateEntityType,
        aggregateDocumentType,
    } = getSharedTypes(collection, lifestyleEntryType)

    const sharedDailyProperties = {
        uid: '',
        organizationId,

        // healthComponentType: entry.healthComponentType,
        lifestyleEntryType,

        value: 0,

        dateTime: fromDate,

        aggregateEntityType,
        // aggregateDocumentType: AggregateDocumentType.Daily,
        aggregateDocumentType,
        // healthComponentType

        brain: {
            value: 0,
            mood: {
                expected: 0,
                realized: 0,
                percentage: 0,
            },
            sleep: {
                expected: 0,
                realized: 0,
                percentage: 0,
            },
        },
        body: {
            value: 0,
            fatigue: {
                expected: 0,
                realized: 0,
                percentage: 0,
            },
            pain: {
                expected: 0,
                realized: 0,
                percentage: 0,
            },
            weight: {
                expected: 0,
                realized: 0,
                percentage: 0,
            },
            wearables: {
                expected: 0,
                realized: 0,
                percentage: 0,
            },
        },
        food: {
            value: 0,
            components: {
                expected: 0,
                realized: 0,
                percentage: 0,
            },
        },
        training: {
            value: 0,
            components: {
                expected: 0,
                realized: 0,
                percentage: 0,
            },
        },
        programs: {
            value: 0,
            programDay: 0,
            worksheetConsumed: false,
            videoConsumed: false,
            surveyConsumed: false,
            expected: 0,
            realized: 0,
            percentage: 0,
        },
    }

    const dailies: Array<AthleteIpScoreDailyAggregate> = []

    ;[0, 1, 2, 3, 4, 5, 6].forEach((offset, i) => {
        sharedDailyProperties.dateTime = firestore.Timestamp.fromDate(
            moment(fromDate.toDate()).add(offset, 'day').toDate()
        )

        sharedDailyProperties.uid = i.toString()

        const athleteDaily: AthleteIpScoreDailyAggregateModel = {
            ...AthleteDailyDocument,
            ...sharedDailyProperties,
            entityName,
            entityId,
            userId: entityId,
            fullName: entityName,
        }
        dailies.push(athleteDaily)
    })

    return dailies.length ? dailies : undefined
}
