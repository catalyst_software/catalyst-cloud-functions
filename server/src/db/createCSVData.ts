import moment = require('moment')

import { HealthComponentType } from './biometricEntries/well/enums/health-component-type'
import { LifestyleEntryType } from '../models/enums/lifestyle-entry-type'
import { SportType } from './biometricEntries/well/enums/sport-type'
import { PainType } from './biometricEntries/well/enums/pain-type'
import { BodyLocationType } from './biometricEntries/well/enums/body-location-type'
import { IntensityLevelType } from './biometricEntries/well/enums/intensity-level-type'
// import { MealTimeType } from '../models/enums/meal-time-type'

// import { PainLevelType } from '../models/enums/pain-level-type'
import { toTitleCase } from '../shared/utils/toTitleCase';

export const createCSVData = (
    uid: string,
    userId: string,
    fullName: string,
    groupName: string,
    momentDate: moment.Moment,
    lifestyleEntryTypeValue: LifestyleEntryType,
    healthComponentTypeValue: HealthComponentType,
    value: number,
    // Meal
    mealType?: string,
    foodType?: string,
    servingCount?: number,

    simpleMealClasificationType?: string,
    // Pain
    painTypeType?: PainType,
    bodyLocationType?: BodyLocationType,
    painLevelValue?: number,
    // Sport
    sportTypeType?: SportType,
    sportName?: string,
    intensityLevelValue?: IntensityLevelType,
    minutes?: number
) => {
    const lifestyleEntryType = LifestyleEntryType[+lifestyleEntryTypeValue]
    // const healthComponentType = LifestyleEntryType[+healthComponentTypeValue];

    const theValue = isNANorNull(value) ? 0 : value

    const painLevel = typeof painLevelValue === 'number'
        ? +painLevelValue
        : '' // "Can't Compute -> " + painLevelValue
    const bodyLocation = typeof bodyLocationType === 'number'
        ? BodyLocationType[+bodyLocationType]
        : '' // "Can't Compute -> " + bodyLocationType
    const painType = typeof painTypeType === 'number' ? PainType[+painTypeType] : '' // "Can't Compute -> " + painTypeType

    const sportType = typeof sportTypeType === 'number' ? SportType[+sportTypeType] : '' // "Can't Compute -> " + sportTypeType
    const intensityLevel = typeof intensityLevelValue === 'number'
        ? IntensityLevelType[+intensityLevelValue]
        : '' // "Can't Compute -> " + intensityLevelValue

    const csvDataFormat = {
        uid,
        userId,
        fullName,
        groupName,
        dateTime: momentDate.toISOString(),
        // dateTime1: momentDate.toISOString().toString(),
        dateTimeFormatted: momentDate.toString(),
        lifestyleEntryType: lifestyleEntryType
            ? lifestyleEntryType
            : "Can't Compute -> " + lifestyleEntryTypeValue,
        // healthComponentType: healthComponentType
        //     ? healthComponentType
        //     : "Can't Compute -> " + healthComponentTypeValue,
        value: typeof theValue === 'number' && theValue >= 0 ? +theValue : '',
        // Meal
        mealType: mealType ? toTitleCase(mealType) : '',
        foodType: foodType ? toTitleCase(foodType) : '',
        servingCount: typeof servingCount === 'number' ? +servingCount : '',
        // Simple Food
        simpleMealClasificationType: simpleMealClasificationType ? toTitleCase(simpleMealClasificationType) : '',
        // foodType: foodType ? toTitleCase(foodType) : '',
        // servingCount: typeof servingCount === 'number' ? +servingCount : '',
        // Pain
        painType,
        bodyLocation,
        painLevel,
        // Sport
        sportType,
        intensityLevel,
        minutes: typeof minutes === 'number' ? +minutes : '',
    }
    console.log('csvData', csvDataFormat)
    return csvDataFormat
}
function isNANorNull(value: number) {
    return value !== undefined && isNaN(value) || value === null;
}

