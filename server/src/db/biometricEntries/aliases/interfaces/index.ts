
import { WellAggregateCollection } from "../../enums/firestore-aggregate-collection";

//
// A bit of reading material
// https://www.typescriptlang.org/docs/handbook/advanced-types.html
//

import { Group } from "../../../../models/group/interfaces/group";
import { Athlete } from "../../../../models/athlete/interfaces/athlete";
import { AllAggregateType } from "../../../../models/types/biometric-aggregate-types";
import { AthleteWeeklyAggregate, AthleteWeeklyHistoryAggregate, GroupWeeklyAggregate, GroupWeeklyHistoryAggregate } from "../../models/documents/aggregates";
import { BiometricAggregateHistoryRecord } from "../../models/documents/aggregates/biometric-aggregate-history-record";
import { BiometricAggregateRecord } from "../../models/documents/aggregates/biometric-aggregate-record";
import { IpScoreAggregateHistoryCollection } from "../../../ipscore/enums/firestore-ipscore-aggregate-history-collection";
import { IpScoreAggregateCollection } from "../../../ipscore/enums/firestore-ipscore-aggregate-collection";
import { WellAggregateHistoryCollection } from "../../enums/firestore-aggregate-history-collection";
import { WellAggregateBinaryHistoryCollection } from "../../enums/firestore-aggregate-binary-history-collection";


export const isAthlete = (entity:
    Athlete | Group
): entity is Athlete => {
    const result = (<Athlete>entity).profile !== undefined;
    console.log('isAthlete', result);
    return result
};

export const isAthleteWeekly = (aggregateDoc: AllAggregateType
): aggregateDoc is AthleteWeeklyAggregate => {
    return aggregateDoc instanceof AthleteWeeklyAggregate;
};

export const isAthleteWeeklyHistory = (aggregateDoc:
    AllAggregateType
): aggregateDoc is AthleteWeeklyHistoryAggregate => {
    return aggregateDoc instanceof AthleteWeeklyHistoryAggregate;
};

export const isHistoryRecord = (aggregateDoc:
    BiometricAggregateHistoryRecord
): aggregateDoc is BiometricAggregateHistoryRecord => {
    return aggregateDoc instanceof BiometricAggregateHistoryRecord;
};


export const isGroupWeekly = (aggregateDoc:
    AthleteWeeklyHistoryAggregate | GroupWeeklyAggregate | GroupWeeklyHistoryAggregate
): aggregateDoc is GroupWeeklyAggregate => {
    return aggregateDoc instanceof GroupWeeklyAggregate;
};

export const isGroupWeeklyHistory = (aggregateDoc:
    GroupWeeklyHistoryAggregate |
    BiometricAggregateRecord |
    AthleteWeeklyAggregate |
    GroupWeeklyAggregate
): aggregateDoc is GroupWeeklyHistoryAggregate => {

    return aggregateDoc instanceof GroupWeeklyHistoryAggregate;
};

export const isHistory = (aggregateDoc:
    AthleteWeeklyHistoryAggregate | GroupWeeklyHistoryAggregate | BiometricAggregateHistoryRecord
): aggregateDoc is BiometricAggregateHistoryRecord => {

    return aggregateDoc instanceof BiometricAggregateHistoryRecord;
};


export const isAthleteDoc = (aggregateDoc:
    AthleteWeeklyAggregate | GroupWeeklyAggregate |
    //  GroupMonthlyAggregate |
    AthleteWeeklyHistoryAggregate
): aggregateDoc is AthleteWeeklyAggregate |
//  AthleteMonthlyAggregate |
AthleteWeeklyHistoryAggregate => {

    if (isAthleteWeekly(aggregateDoc)) {
        return true
    } else return false
};


export const isAthleteHistoryDoc = (aggregateDoc:
    AthleteWeeklyHistoryAggregate
): aggregateDoc is AthleteWeeklyHistoryAggregate => {

    if (isAthleteWeeklyHistory(aggregateDoc)) {
        return true
    } else return false
};

export const isWeeklyCollection = (collection: WellAggregateCollection | IpScoreAggregateCollection | IpScoreAggregateHistoryCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection) => {
    return collection === WellAggregateCollection.AthleteWeekly ||
        collection === WellAggregateCollection.GroupWeekly ||
        collection === IpScoreAggregateCollection.AthleteIpScoreWeekly ||
        collection === IpScoreAggregateCollection.GroupIpScoreWeekly;

};
