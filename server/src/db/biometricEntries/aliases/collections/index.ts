import { WellAggregateCollection } from '../../enums/firestore-aggregate-collection'
import { WellAggregateHistoryCollection } from '../../enums/firestore-aggregate-history-collection'
import { WellAggregateBinaryHistoryCollection } from '../../enums/firestore-aggregate-binary-history-collection';

export const isBinaryCollection = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    return (
        collection === WellAggregateBinaryHistoryCollection.AthleteBinaryHistory ||
        collection === WellAggregateBinaryHistoryCollection.GroupBinaryHistory
    )
};

export const isWeeklyWellCollection = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    return (
        collection === WellAggregateCollection.AthleteWeekly ||
        collection === WellAggregateCollection.GroupWeekly
    )
};

export const isAthleteCollection = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    return (
        collection === WellAggregateCollection.AthleteWeekly ||
        collection === WellAggregateHistoryCollection.AthleteWeeklyHistory ||
        collection === WellAggregateHistoryCollection.AthleteMonthlyHistory ||
        collection === WellAggregateBinaryHistoryCollection.AthleteBinaryHistory
    )
};

export const isAthleteHistoryCollection = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    const result =
        collection === WellAggregateHistoryCollection.AthleteWeeklyHistory ||
        collection === WellAggregateHistoryCollection.AthleteMonthlyHistory;

    return result
};

export const isWeeklyHistoryCollection = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    return (
        collection === WellAggregateHistoryCollection.AthleteWeeklyHistory ||
        collection === WellAggregateHistoryCollection.GroupWeeklyHistory
    )
};

export const isGroupWeeklyCollection = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    return (
        collection === WellAggregateCollection.GroupWeekly ||
        collection === WellAggregateHistoryCollection.GroupWeeklyHistory
    )
};

export const isGroupCollection = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    const result = (
        collection === WellAggregateCollection.GroupWeekly ||
        collection === WellAggregateHistoryCollection.GroupWeeklyHistory ||
        collection === WellAggregateHistoryCollection.GroupMonthlyHistory || 
        collection === WellAggregateBinaryHistoryCollection.GroupBinaryHistory
    )

    return result
};

export const isGroupWeeklyHistoryCollection = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    return collection === WellAggregateHistoryCollection.GroupWeeklyHistory
};

export const isHistoryCollection = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection
) => {
    const result =
        collection === WellAggregateHistoryCollection.AthleteWeeklyHistory ||
        collection === WellAggregateHistoryCollection.AthleteMonthlyHistory ||
        collection === WellAggregateHistoryCollection.GroupWeeklyHistory ||
        collection === WellAggregateHistoryCollection.GroupMonthlyHistory;
    return result
};
