import { AggregateDocumentType } from "../../../../well/enums/aggregate-document-type";
import { WellAggregateHistoryCollection } from "../../../../enums/firestore-aggregate-history-collection";
import { BiometricAggregateHistoryRecord } from "../biometric-aggregate-history-record";
import { LifestyleEntryType } from "../../../../../../models/enums/lifestyle-entry-type";
import { BiometricAggregateComponentHistoryInterface } from "../../../../well/aggregate/interfaces/biometric-aggregate-component-history";
import { BiometricAggregateTrackingCacheInterface } from "../../../../well/aggregate/interfaces/biometric-aggregate-tracking-cache";
import { AggregateDocumentDirective } from "../../../../services/interfaces/aggregate-document-directive";
import { docManagerService } from "../../../../services/persist-service";
import { HealthComponentType } from "../../../../well/enums/health-component-type";
import { AggregateEntityType } from "../../../../enums/aggregate-entity-type";
import { GroupMonthlyHistoryAggregateInterface } from "../../../../well/aggregate/interfaces/group/group-biometric-monthly-history-aggregate";

/**
 * Default Values when instanciating a new Group Biometric Monthly Aggregate
 */

const _defaultGroupMonthlyHistoryValues: any = {
    uid: undefined,
    guid: undefined,

    entry: undefined,

    aggregateEntityType: AggregateEntityType.Group,
    aggregateDocumentType: AggregateDocumentType.MonthlyHistory,
    healthComponentType: undefined,

    lifestyleEntryType: undefined,
    collection: WellAggregateHistoryCollection.GroupMonthlyHistory,

    entityId: undefined,
    entityName: undefined,

    organizationId: undefined,

    recentEntries: [],
    componentValues: [],
    values: [],

    monthNumber: undefined,


    // TODO: Used here?
    painComponentHistory: [],
    mealComponentHistory: [],
    trainingComponentHistory: [],

    trackingCache: {
        index: 0,
        valueCache: [],
        componentCache: {
            index: 0,
            components: []
        }
    }
};

export class GroupMonthlyHistoryAggregate extends BiometricAggregateHistoryRecord {
    public readonly collection: WellAggregateHistoryCollection;

    public readonly uid: string;
    public readonly guid: string;

    public readonly entityId: string;
    public readonly entityName: string;

    public readonly historicalValues: Array<any>;


    lifestyleEntryType: LifestyleEntryType;

    values: Array<number>;

    painComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    mealComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    trainingComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;

    aggregateDocumentType: AggregateDocumentType;
    aggregateEntityType: AggregateEntityType;
    healthComponentType: HealthComponentType;

    trackingCache: BiometricAggregateTrackingCacheInterface;

    constructor(params?: GroupMonthlyHistoryAggregateInterface) {
        if (params) {
            super({ ..._defaultGroupMonthlyHistoryValues, ...params });
        } else {
            super(_defaultGroupMonthlyHistoryValues);
        }
    }

    calculateAggregates(directive: AggregateDocumentDirective) {

        debugger;

        const result = this.aggregator.processGroupMonthlyHistory(super.entry, this, directive);

        return docManagerService.saveAggregate(result, directive);
    }
}
