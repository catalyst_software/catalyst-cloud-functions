import { GroupWeeklyAggregateInterface } from '../../../../well/aggregate/interfaces'
import { AggregateEntityType } from '../../../../enums/aggregate-entity-type'
import { AggregateDocumentType } from '../../../../well/enums/aggregate-document-type'
import { WellAggregateCollection } from '../../../../enums/firestore-aggregate-collection'
import { BiometricAggregateRecord } from '../biometric-aggregate-record'
import { GroupDailyAggregateModel } from '../../../../well/aggregate/group-biometric-daily-aggregate.model'
import { AggregateDocumentDirective } from '../../../../services/interfaces/aggregate-document-directive'
import { docManagerService } from '../../../../services/persist-service'
import { AggregateDirectiveType } from '../../../../../../models/types/biometric-aggregate-types'
import { createDailies } from '../../../../services/utils'
import { isArray } from 'lodash'

/**
 * Default Values when instanciating a new Group Biometric Weekly Aggregate
 */

const _defaultValues: GroupWeeklyAggregateInterface = {
    uid: undefined,
    entry: undefined,

    entityId: undefined,
    entityName: undefined,

    organizationId: undefined,

    aggregateEntityType: AggregateEntityType.Group,
    aggregateDocumentType: AggregateDocumentType.Weekly,

    collection: WellAggregateCollection.GroupWeekly,

    dailies: undefined,

    value: undefined,
}

export class GroupWeeklyAggregate extends BiometricAggregateRecord {
    public readonly collection: WellAggregateCollection
    public readonly uid: string
    public readonly historicalValues?: Array<any>

    public readonly entityId: string
    public readonly entityName: string

    // readonly dailies: Array<GroupDailyAggregateModel>;
    // historyDocument: AthleteWeeklyHistoryAggregate | AthleteMonthlyHistoryAggregate | GroupWeeklyHistoryAggregate | GroupMonthlyHistoryAggregate;
    // readonly weekNumber?: number;

    // readonly groupId: string;
    // readonly groupName: string;
    dailies: GroupDailyAggregateModel[]
    // readonly organizationId: string;
    // readonly value: any;

    // constructor(params: GroupWeeklyAggregateInterface) {
    //     super({ ..._defaultValues, ...params });
    // }

    constructor(
        params: GroupWeeklyAggregateInterface,
        directive: AggregateDirectiveType,
        dateTime: Date,
        weekNumber: number,
        utcOffset: number
    ) {
        const { entry } = params
        if (params) {
            // const {
            //     entityId,
            //     entityName,
            //     organizationId,
            //     lifestyleEntryType
            // } = params;
            super({
                ..._defaultValues,

                ...params,
                dailies:
                    (!!params &&
                        isArray(params.dailies) &&
                        params.dailies.map(dailyAggregate => {
                            if (!isArray(dailyAggregate.historicalValues)) {
                                console.warn(
                                    'Having to set dailyAggregate.historicalValues = [] in GroupWeeklyAggregate()'
                                )
                                dailyAggregate.historicalValues = []
                            }

                            return dailyAggregate
                        })) ||
                    createDailies(
                        entry,
                        _defaultValues.collection,
                        entry.athleteCreationTimestamp
                    ),
            }) //, directive);
        } else {
            super(_defaultValues) //, directive);
        }
    }
    calculateAggregates(directive: AggregateDocumentDirective) {
        const result = this.aggregator.processGroupWeekly(
            this.entry,
            this,
            directive
        )

        return docManagerService.saveAggregate(result, directive)
    }
    //     calculateHistoryAggregates(aggregator: DynamicAggregator, directive: AggregateDocumentDirective) {

    // debugger;
    //         const result = aggregator.processGroupWeeklyHistory(
    //             this.entry, directive.aggregateDocument as any, directive);

    //         return docManagerService.saveAggregate(result, directive);
    //     }
}
