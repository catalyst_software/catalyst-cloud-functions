import { GroupWeeklyHistoryAggregateInterface } from "../../../../well/aggregate/interfaces/group/group-biometric-weekly-history-aggregate";
// import { EntityType } from "../../../../well/enums/entity-type";
import { AggregateDocumentType } from "../../../../well/enums/aggregate-document-type";
import { WellAggregateHistoryCollection } from "../../../../enums/firestore-aggregate-history-collection";
import { BiometricAggregateHistoryRecord } from "../biometric-aggregate-history-record";
import { LifestyleEntryType } from "../../../../../../models/enums/lifestyle-entry-type";
import { BiometricAggregateComponentHistoryInterface } from "../../../../well/aggregate/interfaces/biometric-aggregate-component-history";
// import { HealthComponentType } from '../../enums/health-component-type';
import { BiometricAggregateTrackingCacheInterface } from "../../../../well/aggregate/interfaces/biometric-aggregate-tracking-cache";
// import { DynamicAggregator } from "../../../../aggregation/dynamic-aggregator";
import { AggregateDocumentDirective } from "../../../../services/interfaces/aggregate-document-directive";
import { docManagerService } from "../../../../services/persist-service";
import { HealthComponentType } from "../../../../well/enums/health-component-type";
import { AggregateEntityType } from "../../../../enums/aggregate-entity-type";


/**
 * Default Values when instanciating a new Group Biometric Weekly Aggregate
 */


const _defaultGroupWeeklyHistoryValues: GroupWeeklyHistoryAggregateInterface = {

    uid: undefined,
    entry: undefined,
    guid: undefined,

    entityId: undefined,
    entityName: undefined,

    organizationId: undefined,

    aggregateEntityType: AggregateEntityType.Group,
    aggregateDocumentType: AggregateDocumentType.WeeklyHistory,

    collection: WellAggregateHistoryCollection.GroupWeeklyHistory,

    lifestyleEntryType: undefined,
    value: undefined,

    values: [],

    //TODO: Used here?
    painComponentHistory: [],
    mealComponentHistory: [],
    trainingComponentHistory: [],

    trackingCache: {
        index: 0,
        valueCache: [],
        componentCache: {
            index: 0,
            components: []
        }
    }
};

export class GroupWeeklyHistoryAggregate extends BiometricAggregateHistoryRecord {
    public readonly collection: WellAggregateHistoryCollection;

    public readonly uid: string;
    public readonly guid: string;

    public readonly entityId: string;
    public readonly entityName: string;

    public readonly historicalValues: Array<any>;

    lifestyleEntryType: LifestyleEntryType;

    values: Array<number>;

    painComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    mealComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    trainingComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;

    aggregateDocumentType: AggregateDocumentType;
    aggregateEntityType: AggregateEntityType;
    healthComponentType: HealthComponentType;

    trackingCache: BiometricAggregateTrackingCacheInterface;

    constructor(params?: GroupWeeklyHistoryAggregateInterface) {
        if (params) {
            super({ ..._defaultGroupWeeklyHistoryValues, ...params });
        } else {
            super(_defaultGroupWeeklyHistoryValues);
        }
    }

    calculateAggregates(directive: AggregateDocumentDirective) {

        debugger;
        const result = this.aggregator.processGroupWeeklyHistory(super.entry, this, directive);

        return docManagerService.saveAggregate(result, directive);

    }
}
