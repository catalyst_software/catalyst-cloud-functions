import { firestore } from 'firebase-admin';

import { GroupDailyAggregate } from '../../../../well/aggregate/interfaces';
import { AggregateDocumentType } from '../../../../well/enums/aggregate-document-type';
import { AggregateEntityType } from '../../../../enums/aggregate-entity-type';

/**
 * Default Values when instanciating a new Group Biometric Daily Aggregate
 */
export const GroupDailyDocument: GroupDailyAggregate = {
    uid: '',

    aggregateEntityType: AggregateEntityType.Group,
    aggregateDocumentType: AggregateDocumentType.Daily,
    entityId: undefined,
    entityName: undefined,
    organizationId: undefined,

    groupId: '',
    groupName: '',

    dateTime: firestore.Timestamp.now()
};