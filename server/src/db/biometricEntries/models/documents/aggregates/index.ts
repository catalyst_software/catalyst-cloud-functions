
export * from './athlete/athelete-weekly-aggregate.document';
// export * from './athelete-monthly-aggregate.document';
export * from './athlete/athelete-weekly-history-aggregate.document';

export * from './group/group-weekly-aggregate.document';
// export * from './group-monthly-aggregate.document';
export * from './group/group-weekly-history-aggregate.document';