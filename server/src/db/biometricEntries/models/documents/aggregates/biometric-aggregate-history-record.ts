import { BiometricHistoryAggregate } from "../../../well/aggregate/interfaces/biometric-history-aggregate";
import { BaseRecord } from "../../../well/documents/base-record";
import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { WellAggregateHistoryCollection } from "../../../enums/firestore-aggregate-history-collection";
// import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces";

import { AggregateDocumentType } from "../../../well/enums/aggregate-document-type";
import { HealthComponentType } from "../../../well/enums/health-component-type";
import { IpScoreAggregateComponentInterface } from "../../../../ipscore/models/interfaces/ipscore-component-aggregate";
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { IpScoreAggregateTrackingCacheInterface } from '../../../../ipscore/models/interfaces/ipscore-aggregate-tracking-cache';
import { BiometricAggregateTrackingCacheInterface } from '../../../well/aggregate/interfaces/biometric-aggregate-tracking-cache';
import { AggregateEntityType } from '../../../enums/aggregate-entity-type';
import { DynamicAggregator } from "../../../aggregation/dynamic-aggregator";

/**
 * Interface which defines the types passed into the constructor.
 */
const _defaultValues: BiometricHistoryAggregate = {
    organizationId: undefined,
    uid: undefined,

    entityId: undefined,
    entityName: undefined,



    lifestyleEntryType: undefined,

    // values: [],
    // painComponentHistory: [],
    // mealComponentHistory: [],
    // trainingComponentHistory: [],

    // trackingCache: {
    //     index: 0,
    //     valueCache: [],
    //     componentCache: {
    //         index: 0,
    //         components: []
    //     }
    // }
    // as BiometricAggregateTrackingCacheInterface
};
export class BiometricAggregateHistoryRecord extends BaseRecord implements BiometricHistoryAggregate {

    readonly uid: string;
    readonly entityId: string;
    readonly entityName: string;
    readonly entry: AthleteBiometricEntryInterface;

    readonly collection?: WellAggregateHistoryCollection;

    lifestyleEntryType: LifestyleEntryType;

    aggregateDocumentType?: AggregateDocumentType;
    aggregateEntityType?: AggregateEntityType;
    healthComponentType?: HealthComponentType;

    componentValues?: Array<IpScoreAggregateComponentInterface>;
    trackingCache?: IpScoreAggregateTrackingCacheInterface | BiometricAggregateTrackingCacheInterface;
    recentEntries?: Array<IpScoreAggregateComponentInterface>;

    lastAggregateDate: Date;
    monthNumber?: number;

    aggregator: DynamicAggregator;

    constructor(params?: BiometricHistoryAggregate) {

        const { entry } = params;

        debugger;
        if (params) {
            super({ ..._defaultValues, ...params } as AthleteBiometricEntryInterface);
        }
        else {
            super(_defaultValues);
        }

        if (entry) {
            this.aggregator = new DynamicAggregator(LifestyleEntryType[entry.lifestyleEntryType]);
        }

    }



    organizationId: string;


    // values: Array<number>;

    // lifestyleEntryType: LifestyleEntryType;

    // // painComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    // // mealComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    // // trainingComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;

    // aggregateDocumentType?: AggregateDocumentType;
    // aggregateEntityType?: AggregateEntityType;
    // healthComponentType?: HealthComponentType;


    // componentValues?: Array<IpScoreAggregateComponentInterface>;
    // trackingCache?: IpScoreAggregateTrackingCacheInterface | BiometricAggregateTrackingCacheInterface;
    // recentEntries?: Array<IpScoreAggregateComponentInterface>;

    // lastAggregateDate: firestore.Timestamp;
    // monthNumber?: number;


    /**
     * return a new version of the BiometricAggregateModel with updated values as specified in the updatedValues parameter
     * @param updatedValues The new values for various members of the BiometricAggregateModel class.
     * @returns a new instance of a BiometricAggregateModel class with updated values
     */
    // with?(updatedValues: BiometricAggregateInterface): BiometricAggregateInterface {
    //     return super.with(updatedValues) as BiometricAggregateInterface;
    // }
}
