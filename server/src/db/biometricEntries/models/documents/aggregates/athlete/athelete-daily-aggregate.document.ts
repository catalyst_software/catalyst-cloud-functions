import { firestore } from "firebase-admin";

import { AggregateDocumentType } from "../../../../well/enums/aggregate-document-type";

import { AthleteDailyAggregate } from "../../../../well/aggregate/interfaces";
import { AggregateEntityType } from "../../../../enums/aggregate-entity-type";

/**
 * Default Values when instanciating a new Athlete Biometric Daily Aggregate
 */
export const AthleteDailyDocument: AthleteDailyAggregate = {
    uid: '',
    organizationId: '',

    aggregateEntityType: AggregateEntityType.Athlete,
    aggregateDocumentType: AggregateDocumentType.Daily,
    entityId: '',
    entityName: '',
    userId: '',
    fullName: '',

    dateTime: firestore.Timestamp.now()
};