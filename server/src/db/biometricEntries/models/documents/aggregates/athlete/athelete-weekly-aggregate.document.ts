import { isArray } from "lodash";

import { AthleteWeeklyAggregateInterface } from "../../../../well/aggregate/interfaces";
import { AggregateDocumentType } from "../../../../well/enums/aggregate-document-type";
import { WellAggregateCollection } from "../../../../enums/firestore-aggregate-collection";
import { BiometricAggregateRecord } from "../biometric-aggregate-record";
import { AthleteDailyAggregateModel } from "../../../../well/aggregate/athelete-biometric-daily-aggregate.model";
import { docManagerService } from "../../../../services/persist-service";
import { AggregateEntityType } from "../../../../enums/aggregate-entity-type";
import { AggregateDocumentDirective } from "../../../../services/interfaces/aggregate-document-directive";
import { createDailies } from "../../../../services/utils";


/**
 * Default Values when instanciating a new Athlete Biometric Weekly Aggregate
 */

const _defaultValues: AthleteWeeklyAggregateInterface = {
  uid: undefined,
  guid: undefined,

  entityId: undefined,
  entityName: undefined,

  aggregateEntityType: AggregateEntityType.Athlete,
  aggregateDocumentType: AggregateDocumentType.Weekly,

  collection: WellAggregateCollection.AthleteWeekly,
  // historyDoc: undefined,

  userId: undefined,
  fullName: undefined,

  dailies: undefined,

  organizationId: undefined,
  value: undefined,
  values: undefined
};

export class AthleteWeeklyAggregate extends BiometricAggregateRecord {
  readonly collection: WellAggregateCollection;
  // public readonly historyDoc?: AthleteIpScoreWeeklyHistoryAggregate;
  readonly userId: string;
  readonly uid: string;
  readonly guid: string;
  readonly fullName: string;
  // public readonly entry: string;

  readonly entityId: string;
  readonly entityName: string;

  dailies: Array<AthleteDailyAggregateModel>;

  weekNumber?: number;

  weeklyHistory: {};
  monthlyHistory: {};

  organizationId: string;
  value: any;
  startDate
  endDate


  constructor(params: AthleteWeeklyAggregateInterface) {

    const { entry } = params
    if (params) {
      super({
        ..._defaultValues,

        ...params,
        dailies: !!params && isArray(params.dailies) && params.dailies.map((dailyAggregate) => {
          if (!isArray(dailyAggregate.historicalValues)) {
            console.warn('Having to set dailyAggregate.historicalValues = [] in AthleteWeeklyAggregate()')
            dailyAggregate.historicalValues = [];
          }

          return dailyAggregate;
        }) || createDailies(
          entry,
          _defaultValues.collection,
          entry.athleteCreationTimestamp)
      })//, directive);
    } else {
      super(_defaultValues); //, directive);
    }
  }


  // constructor(params: AthleteWeeklyAggregateInterface) {
  //   if (params) {
  //     super({ ..._defaultValues, ...params });
  //   } else {
  //     super(_defaultValues);
  //   }
  // }

  with?(values): AthleteWeeklyAggregate {
    debugger;
    const returnVal = new (this.constructor as any)({ ...this._data.merge(values).toJS() });

    // returnVal._data = this._data.merge(values);

    return returnVal;
  }

  calculateAggregates(directive: AggregateDocumentDirective) {

    debugger;
    const result = this.aggregator.processAthleteWeekly(this.entry, this, directive);

    return docManagerService.saveAggregate(result, directive);
  }
}
