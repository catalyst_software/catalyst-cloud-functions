import { cloneDeep, isArray } from "lodash";

import { AthleteMonthlyHistoryAggregateInterface } from "../../../../well/aggregate/interfaces/athlete/athlete-biometric-monthly-history-aggregate";
import { AggregateDocumentType } from "../../../../well/enums/aggregate-document-type";
import { WellAggregateHistoryCollection } from "../../../../enums/firestore-aggregate-history-collection";
import { BiometricAggregateHistoryRecord } from "../biometric-aggregate-history-record";
import { AthleteBiometricEntryInterface } from "../../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { LifestyleEntryType } from "../../../../../../models/enums/lifestyle-entry-type";
import { BiometricAggregateComponentHistoryInterface } from "../../../../well/aggregate/interfaces/biometric-aggregate-component-history";
import { HealthComponentType } from "../../../../well/enums/health-component-type";
import { AggregateDocumentDirective } from "../../../../services/interfaces/aggregate-document-directive";

import { docManagerService } from "../../../../services/persist-service";
import { BiometricAggregateTrackingCacheInterface } from "../../../../well/aggregate/interfaces/biometric-aggregate-tracking-cache";
import { AggregateEntityType } from "../../../../enums/aggregate-entity-type";

/**
 * Default Values when instanciating a new Athlete Biometric Weekly Aggregate
 */


const _defaultValues: AthleteMonthlyHistoryAggregateInterface = {
    uid: undefined,
    guid: undefined,

    // entry: undefined,

    entityId: undefined,
    entityName: undefined,

    aggregateEntityType: AggregateEntityType.Athlete,
    aggregateDocumentType: AggregateDocumentType.MonthlyHistory,

    collection: WellAggregateHistoryCollection.AthleteMonthlyHistory,

    organizationId: undefined,
    value: undefined,

    values: [],
    painComponentHistory: [],
    mealComponentHistory: [],
    trainingComponentHistory: [],

    trackingCache: {
        index: 0,
        valueCache: [],
        componentCache: {
            index: 0,
            components: []
        }
    }
};

export class AthleteMonthlyHistoryAggregate extends BiometricAggregateHistoryRecord {
    readonly collection: WellAggregateHistoryCollection;
    readonly uid: string;

    readonly guid: string;
    readonly entityId: string;
    readonly entityName: string;
    readonly userId: string;
    readonly fullName: string;
    readonly historicalValues: Array<any>;
    recentEntries?: Array<AthleteBiometricEntryInterface>;

    lifestyleEntryType: LifestyleEntryType;

    values: Array<number>;

    painComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    mealComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    trainingComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;

    aggregateDocumentType: AggregateDocumentType;
    aggregateEntityType: AggregateEntityType;
    healthComponentType: HealthComponentType;

    trackingCache: BiometricAggregateTrackingCacheInterface;

    constructor(params?: AthleteMonthlyHistoryAggregateInterface) {
        if (params) {
            super({ ..._defaultValues, ...params });
        } else {
            super(_defaultValues);
        }
    }

    calculateAggregates(
        directive: AggregateDocumentDirective
    ) {
        debugger;
        const updatedDoc = this.aggregator.processAthleteMonthlyHistory(this.entry, this as any, directive);

        const existing = isArray(updatedDoc.recentEntries) ? cloneDeep(updatedDoc.recentEntries) : [];
        if (existing.length === 10) {
            existing.pop();
        }

        existing.unshift(this.entry);

        return docManagerService.saveAggregate(
            updatedDoc
                .with({
                    recentEntries: existing
                }),
            directive
        );
    }
}
