
import { AthleteWeeklyHistoryAggregateInterface } from "../../../../well/aggregate/interfaces/athlete/athlete-biometric-weekly-history-aggregate";
import { AggregateDocumentType } from "../../../../well/enums/aggregate-document-type";
import { WellAggregateHistoryCollection } from "../../../../enums/firestore-aggregate-history-collection";

import { BiometricAggregateHistoryRecord } from "../biometric-aggregate-history-record";
import { AthleteBiometricEntryInterface } from "../../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { LifestyleEntryType } from "../../../../../../models/enums/lifestyle-entry-type";
import { BiometricAggregateComponentHistoryInterface } from "../../../../well/aggregate/interfaces/biometric-aggregate-component-history";
import { HealthComponentType } from "../../../../well/enums/health-component-type";
import { docManagerService } from "../../../../services/persist-service";
import { AggregateDocumentDirective } from "../../../../services/interfaces/aggregate-document-directive";
import { BiometricAggregateTrackingCacheInterface } from "../../../../well/aggregate/interfaces/biometric-aggregate-tracking-cache";
import { AggregateEntityType } from "../../../../enums/aggregate-entity-type";



/**
 * Default Values when instanciating a new Athlete Biometric Weekly Aggregate
 */

const _defaultValues: AthleteWeeklyHistoryAggregateInterface = {

    //  entityId: undefined,
    entry: undefined,

    entityId: undefined,
    entityName: undefined,

    aggregateEntityType: AggregateEntityType.Athlete,
    aggregateDocumentType: AggregateDocumentType.WeeklyHistory,

    collection: WellAggregateHistoryCollection.AthleteWeeklyHistory,

    uid: undefined,
    organizationId: undefined,


    painComponentHistory: [],
    mealComponentHistory: [],
    trainingComponentHistory: [],

    trackingCache: {
        index: 0,
        valueCache: [],
        componentCache: {
            index: 0,
            components: []
        }
    },
    values: []

    // entityId: undefined,
    // documentId: undefined,
};
export class AthleteWeeklyHistoryAggregate extends BiometricAggregateHistoryRecord {

    public readonly collection: WellAggregateHistoryCollection;
    public readonly uid: string;
    public readonly guid: string;

    public readonly entry: AthleteBiometricEntryInterface;
    public readonly entityId: string;
    public readonly entityName: string;

    public readonly lifestyleEntryType: LifestyleEntryType;
    // public readonly documentId: string;
    // public readonly userId: string;
    // public readonly fullName: string;

    historicalValues: any[];

    values: Array<number>;

    painComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    mealComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    trainingComponentHistory: Array<BiometricAggregateComponentHistoryInterface>;
    aggregateDocumentType: AggregateDocumentType;
    aggregateEntityType: AggregateEntityType;
    healthComponentType: HealthComponentType;

    trackingCache: BiometricAggregateTrackingCacheInterface;

    constructor(params?: AthleteWeeklyHistoryAggregateInterface) {
        if (params) {
            super({ ..._defaultValues, ...params });
        } else {
            super(_defaultValues);
        }
    }

    calculateAggregates( directive: AggregateDocumentDirective) {

        debugger;
        const result = this.aggregator.processAthleteWeeklyHistory(super.entry, this, directive);

        return docManagerService.saveAggregate(result, directive);
        
        // return docManagerService.saveAggregate(result, directive);
    }


}
