import { firestore } from 'firebase-admin';

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces';
import { BaseRecord } from '../../../well/documents/base-record';
import { WellAggregateCollection } from '../../../enums/firestore-aggregate-collection';
import { AggregateDocumentType } from '../../../well/enums/aggregate-document-type';
import { HealthComponentType } from '../../../well/enums/health-component-type';
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { MealComponent } from '../../../well/interfaces/meal-component';
import { PainComponent } from '../../../well/interfaces/pain-component';
import { TrainingComponentInterface } from '../../../well/interfaces/training-component';
import { AllAggregateInterfaceType } from '../../../../../models/types/biometric-aggregate-types';
import { BinaryBiometricHistoryAggregateInterface } from '../../../well/aggregate/interfaces/athlete/athlete-binary-biometric-aggregate';
import { AggregateEntityType } from '../../../enums/aggregate-entity-type';
import { DynamicAggregator } from '../../../aggregation/dynamic-aggregator';

/**
 * Interface which defines the types passed into the constructor.
 */
const _defaultValues: AthleteBiometricEntryInterface = {

    // healthComponentType: HealthComponentType.Brain,
    // lifestyleEntryType: LifestyleEntryType.Fatigue,

    entityId: undefined,
    entityName: undefined,

    uid: '',
    organizationId: undefined,
    // organizationId: '',

    value: 0,
    historicalValues: [],
    points: 0,

    mealComponents: [],
    painComponents: [],
    trainingComponents: []
};
export class BiometricAggregateRecord extends BaseRecord implements AthleteBiometricEntryInterface {
    public readonly uid: string;


    public readonly guid: string;
    public readonly athleteCreationTimestamp: firestore.Timestamp;

    public readonly entityId: string;
    public readonly entityName: string;

    public readonly collection?: WellAggregateCollection;

    public readonly aggregateDocumentType?: AggregateDocumentType;
    public readonly aggregateEntityType?: AggregateEntityType;


    public readonly healthComponentType: HealthComponentType;
    public readonly lifestyleEntryType: LifestyleEntryType;

    public readonly value: any;
    public readonly historicalValues?: Array<any>;
    public readonly points?: any;

    public readonly mealComponents?: Array<MealComponent>;
    public readonly painComponents?: Array<PainComponent>;
    public readonly trainingComponents?: Array<TrainingComponentInterface>;


    aggregator: DynamicAggregator;

    constructor(params: AllAggregateInterfaceType | BinaryBiometricHistoryAggregateInterface) {
        debugger;
        const { entry } = params;
        // TODO: check group??
        if (entry)
            super({
                ..._defaultValues,
                entityId: entry.uid ? entry.uid : entry.userId,
                entityName: entry.fullName,
                ...params,
            });
        else
            super({
                ..._defaultValues,
                ...params
            });

        if (this.entry) {
            this.aggregator = new DynamicAggregator(LifestyleEntryType[this.entry.lifestyleEntryType]);
        } else {
            // console.error('ARGH ENTITY UNDEFINED')
        }
  

    }
    // get entry() {
    //     return super.entry;
    // }

    /**
     * return a new version of the BiometricAggregateModel with updated values as specified in the updatedValues parameter
     * @param updatedValues The new values for various members of the BiometricAggregateModel class.
     * @returns a new instance of a BiometricAggregateModel class with updated values
     */
    // with?(updatedValues: BiometricAggregateInterface): BiometricAggregateRecord {
    //     return super.with(updatedValues) as BiometricAggregateRecord;
    // }
}
