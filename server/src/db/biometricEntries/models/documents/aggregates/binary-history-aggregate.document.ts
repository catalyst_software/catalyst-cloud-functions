import { last } from 'lodash';

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces";
import { BinaryBiometricHistoryAggregateInterface, BinaryBiometric } from "../../../well/aggregate/interfaces/athlete/athlete-binary-biometric-aggregate";
import { EntityType } from "../../../well/enums/entity-type";
import { AggregateDocumentType } from "../../../well/enums/aggregate-document-type";
import { DynamicAggregator } from "../../../aggregation/dynamic-aggregator";
import { GroupWeeklyAggregate, AthleteWeeklyHistoryAggregate, GroupWeeklyHistoryAggregate } from ".";
import { docManagerService } from "../../../services/persist-service";
import { WellAggregateBinaryHistoryCollection } from "../../../enums/firestore-aggregate-binary-history-collection";
import { BaseRecord } from "../../../well/documents/base-record";
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive';
import { getAthleteDocFromUID } from '../../../../refs';
import { updateAthleteIndicatorsSickPain } from '../../../../../analytics/triggers/update-athlete-indicators-sick-pain';
import { BiometricAggregate } from '../../../well/aggregate/interfaces/dailies';
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { HealthComponentType } from '../../../well/enums/health-component-type';
import { AggregateEntityType } from '../../../enums/aggregate-entity-type';

/**
 * Default Values when instanciating a new Binary Biometric History Aggregate
 */

const _defaultValues: BinaryBiometricHistoryAggregateInterface = {
  uid: undefined,

  entityId: undefined,
  entityName: undefined,

  aggregateEntityType: undefined,
  aggregateDocumentType: AggregateDocumentType.BinaryHistory,
  // TODO: HealthComponentType???
  healthComponentType: HealthComponentType.Body,
  lifestyleEntryType: undefined,

  collection: WellAggregateBinaryHistoryCollection.AthleteBinaryHistory,

  organizationId: undefined,
  values: [],

  utcOffset: undefined
};

export class BinaryBiometricHistoryAggregate extends BaseRecord implements AthleteBiometricEntryInterface {
  public readonly collection: WellAggregateBinaryHistoryCollection;

  public readonly uid: string;

  public readonly entityId: string;
  public readonly entityName: string;

  constructor(params: BinaryBiometricHistoryAggregateInterface) {
    debugger;

    const { entry } = params;
    if (params) {
      super({
        ..._defaultValues, ...params,
        collection: params.group ? WellAggregateBinaryHistoryCollection.GroupBinaryHistory : WellAggregateBinaryHistoryCollection.AthleteBinaryHistory,
        organizationId: params.entry ? params.entry.organizationId : params.organizationId,
        entityId: params.group ? params.group.uid || params.group.groupId : params.entry.uid,
        entityName: params.group ? params.group.groupName : params.entry.fullName,
        aggregateEntityType: params.group ? EntityType.Group : EntityType.Athlete
      });
    } else {
      super(_defaultValues);
    }

    debugger;
    this.aggregator = new DynamicAggregator(LifestyleEntryType[entry.lifestyleEntryType]);
  }

  public readonly entry: BiometricAggregate; // TODO: Requered because of interfaces TS

  public readonly lifestyleEntryType: LifestyleEntryType;

  public readonly aggregateDocumentType: AggregateDocumentType;
  public readonly aggregateEntityType: AggregateEntityType;

  utcOffset?: number;
  public readonly organizationId?: string;
  values: Array<BinaryBiometric>;

  aggregator: DynamicAggregator;

  with?(values): BinaryBiometricHistoryAggregate {
    debugger;
    const returnVal = new (this.constructor as any)({ ...this._data.merge(values).toJS() });
   
    // const returnVal = new (this.constructor as any);
    // returnVal._data = this._data.merge(values);
    return returnVal;
  }

  async calculateAggregates(
    directive: AggregateDocumentDirective
  )
    : Promise<
      | BinaryBiometricHistoryAggregate
    // // | AthleteMonthlyAggregate
    // | GroupWeeklyAggregate
    // | AthleteWeeklyHistoryAggregate
    // | GroupWeeklyHistoryAggregate
    // any
    > {

    debugger
    let updatedDoc: BinaryBiometricHistoryAggregate
    debugger;
    if (directive.group) {
      updatedDoc = this.aggregator.processGroupBinaryHistory(this.entry, this, directive);
    } else {
      updatedDoc = this.aggregator.processAthleteBinaryHistory(this.entry, this, directive);
    }


    // Check if values updated and save doc, otherwise skip
    if (JSON.stringify(this.toJS().values) !== JSON.stringify(updatedDoc.toJS().values)) {

      const savedDoc = await docManagerService.saveAggregate(updatedDoc, directive) // as BinaryBiometricHistoryAggregate
      debugger;

      if (this.aggregateEntityType === AggregateEntityType.Group)
        debugger;
      await this.updatedAthleteIndicatorsBinaryUpdateOnGroups({ ...directive, aggregateDocument: savedDoc });

      // TODO: any here
      return savedDoc
    } else {
      return updatedDoc
    }
  }

  private async updatedAthleteIndicatorsBinaryUpdateOnGroups(directive: AggregateDocumentDirective) {
    debugger;
    const { values } = directive.aggregateDocument as BinaryBiometricHistoryAggregateInterface

    const athleteUID = this.entry.uid
    const theAthlete = await getAthleteDocFromUID(athleteUID)

    const theGroupIds = theAthlete.groups.map((athGroup) => athGroup.uid || athGroup.groupId);

    if (theGroupIds.length) {
      console.warn('running updateAthleteIndicatorsSickPain');
      console.log({ uid: theAthlete.uid })

      const el = last(values);
      const updateResults = await updateAthleteIndicatorsSickPain(theAthlete.uid, theGroupIds, theAthlete, this.entry.lifestyleEntryType, el);
      if (updateResults.length) {
        const name = (updateResults[0].athlete).firstName + ' ' + (updateResults[0].athlete).lastName
        console.log(`IPScore Group Indicators update results for ${name} -  ${athleteUID}`,
          updateResults
        )
      } else {
        console.warn(`No ipscore GroupIndicator update results for ${theAthlete.profile.firstName} -  ${athleteUID}`);
      }
    }
    else {
      console.warn(`No Groups found for ${theAthlete.profile.firstName} - ${athleteUID}`);

    }

  }

  calculateHistoryAggregates(aggregator: DynamicAggregator, directive) {
    debugger;
    const result = aggregator.processAthleteBinaryHistory(this.entry, this, directive);

    return docManagerService.saveAggregate(result, directive);
  }
}
