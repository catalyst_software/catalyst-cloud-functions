import { firestore } from 'firebase-admin';

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces';
import { BaseRecord } from '../../../well/documents/base-record';
import { WellAggregateCollection } from '../../../enums/firestore-aggregate-collection';
import { AggregateDocumentType } from '../../../well/enums/aggregate-document-type';
import { HealthComponentType } from '../../../well/enums/health-component-type';
import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { MealComponent } from '../../../well/interfaces/meal-component';
import { PainComponent } from '../../../well/interfaces/pain-component';
import { TrainingComponentInterface } from '../../../well/interfaces/training-component';
import { AllAggregateInterfaceType } from '../../../../../models/types/biometric-aggregate-types';
import { AggregateEntityType } from '../../../enums/aggregate-entity-type';


/**
 * Interface which defines the types passed into the constructor.
 */
const _defaultValues: AthleteBiometricEntryInterface = {

    // healthComponentType: HealthComponentType.Brain,
    // lifestyleEntryType: LifestyleEntryType.Fatigue,

    entityId: undefined,
    entityName: undefined,

    uid: '',
    // organizationId: '',

    value: 0,
    historicalValues: [],
    points: 0,

    mealComponents: [],
    painComponents: [],
    trainingComponents: []
};
export class BiometricAggregateRecord extends BaseRecord implements AthleteBiometricEntryInterface { // TODO: NOT USED
    public readonly uid: string;


    public readonly guid: string;
    public readonly athleteCreationTimestamp: firestore.Timestamp;

    public readonly entityId: string;
    public readonly entityName: string;

    public readonly collection?: WellAggregateCollection;

    public readonly aggregateDocumentType?: AggregateDocumentType;
    public readonly aggregateEntityType?: AggregateEntityType;


    public readonly healthComponentType: HealthComponentType;
    public readonly lifestyleEntryType: LifestyleEntryType;

    public readonly value: any;
    public readonly historicalValues?: Array<any>;
    public readonly points?: any;

    public readonly mealComponents?: Array<MealComponent>;
    public readonly painComponents?: Array<PainComponent>;
    public readonly trainingComponents?: Array<TrainingComponentInterface>;

    constructor(params: AllAggregateInterfaceType) {
        const { entry } = params;
        // TODO: check group??
        if (entry)
            super({
                ..._defaultValues,
                entityId: entry.uid ? entry.uid : entry.userId,
                entityName: entry.fullName,
                ...params,
            });
        else
            super({
                ..._defaultValues,
                ...params
            });
        // DocumentBuilder

    }
    // get entry() {
    //     return super.entry;
    // }

    /**
     * return a new version of the BiometricAggregateModel with updated values as specified in the updatedValues parameter
     * @param updatedValues The new values for various members of the BiometricAggregateModel class.
     * @returns a new instance of a BiometricAggregateModel class with updated values
     */
    // with?(updatedValues: BiometricAggregateInterface): BiometricAggregateRecord {
    //     return super.with(updatedValues) as BiometricAggregateRecord;
    // }
}
