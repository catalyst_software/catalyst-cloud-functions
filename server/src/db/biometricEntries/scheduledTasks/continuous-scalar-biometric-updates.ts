import { pubsub } from 'firebase-functions'
import moment from 'moment'
import { Athlete } from '../../../models/athlete/interfaces/athlete'
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type'
import { initialiseFirebase } from '../../../shared/init/initialise-firebase'
import { ScheduledTaskWorkerType } from '../../taskQueue/enums/scheduled-task.enums'
import { BiometricTask } from '../../taskQueue/interfaces/scheduled-task-payload.interface'
import { ScheduledTask } from '../../taskQueue/interfaces/scheduled-task.interface'
import { FirestoreCollection } from '../enums/firestore-collections'

const formatDate = (date, seperator): string => {
    const d = new Date(date)
    const year = d.getFullYear()
    let month = '' + (d.getMonth() + 1)
    let day = '' + d.getDate()

    if (month.length < 2) {
        month = '0' + month
    }
    if (day.length < 2) {
        day = '0' + day
    }

    return [year, month, day].join(seperator)
}

const getYesterdayDate = (): Date => {
    let theDate = new Date()

    theDate.setDate(theDate.getDate() - 1)

    return moment
        .tz(`${formatDate(theDate, '-')} 23:55:00`, 'Europe/Rome')
        .toDate()
}

const getUtcDateTimeNow = (): number => {
    const now = new Date()

    return Date.parse(now.toISOString())
}

const enqueueTask = (
    db: FirebaseFirestore.Firestore,
    task: ScheduledTask
): Promise<void> => {
    return db
        .collection(FirestoreCollection.TaskQueue)
        .add(task)
        .then(() => {
            console.log(`Task ${task.payload} successfully queued`)
            return
        })
}

const createTask = (
    athleteId: string,
    segmentId: string,
    type: LifestyleEntryType,
    value: number
): ScheduledTask => {
    return {
        athleteId,
        segmentId,
        bundleId: 'com.catalystapp.app',
        creationTimestamp: getYesterdayDate(),
        performAtUtc: getUtcDateTimeNow(),
        worker: ScheduledTaskWorkerType.Biometric,
        payload: {
            worker: ScheduledTaskWorkerType.Biometric,
            globalSessionID: '',
            screenSessionID: '',
            programId: '',
            evolutionInstanceId: '',
            healthComponentType: 'Body',
            lifestyleEntryType: LifestyleEntryType[type],
            biometricEntryId: '',
            value,
        } as BiometricTask,
        device_type: '',
        device_manufacturer: '',
        device_model: '',
        device_os: '',
        device_os_version: '',
        device_sdk_version: '',
        device_uuid: '',
    } as ScheduledTask
}

export const scheduledWeightBmiMigration = pubsub
    .schedule('0 2 * * *')
    .timeZone('Europe/Rome')
    .onRun(async context => {
        console.log('Running weight / bmi ETL for data warehouse')
        const db = initialiseFirebase('API').db
        console.log('Firestore initialized')

        const athletes = await db
            .collection(FirestoreCollection.Athletes)
            .get()
            .then(docs => {
                if (docs.size) {
                    return docs.docs.map(doc => {
                        return doc.data() as Athlete
                    })
                } else {
                    return []
                }
            })
            .catch(err => {
                console.error(
                    'An error occurred when retrieving athlete documents from firestore.',
                    { err }
                )

                return []
            })

        console.log(`Retrieved ${athletes.length} athlete documents`)
        if (athletes.length) {
            for (let a of athletes) {
                const bc = a.profile.bodyComposition
                if (bc) {
                    const weight = createTask(
                        a.uid,
                        a.organizationId,
                        LifestyleEntryType.Weight,
                        bc.weight.kilos
                    )
                    await enqueueTask(db, weight)
                    console.log(
                        `Successfully queued weight task with value ${bc.weight.kilos} for athlete ${a.uid}`
                    )

                    const bmi = createTask(
                        a.uid,
                        a.organizationId,
                        LifestyleEntryType.BodyMassIndex,
                        bc.bmi
                    )
                    await enqueueTask(db, bmi)
                    console.log(
                        `Successfully queued bmi task with value ${bc.bmi} for athlete ${a.uid}`
                    )

                    const bodyFat = createTask(
                        a.uid,
                        a.organizationId,
                        LifestyleEntryType.BodyFatPercentage,
                        bc.bodyFatPercentage
                    )
                    await enqueueTask(db, bodyFat)
                    console.log(
                        `Successfully queued bodyFatPercentage task with value ${bc.bodyFatPercentage} for athlete ${a.uid}`
                    )
                }
            }
        }

        return null
    })
