
import { saveAggregateDoc } from '../../ipscore/services/save-aggregates';
import { BinaryBiometricHistoryAggregate } from '../models/documents/aggregates/binary-history-aggregate.document';
import { AllAggregateType, HistoryAggregateType } from '../../../models/types/biometric-aggregate-types';

export class PersistService {

    saveAggregate(updatedDoc: AllAggregateType, directive) {
        return saveAggregateDoc(updatedDoc.toJS(), directive);
    }
}

export const docManagerService = new PersistService();
