// Enums
import { DocMngrActionTypes } from '../../enums/document-manager-action-types'
import { LogLevel } from '../../enums/log-level'

import {
    AggregateType,
    HistoryAggregateType,
    EntityType,
    BinaryHistoryAggregateType,
    AllAggregateCollectionType,
} from '../../../../models/types/biometric-aggregate-types'
import { Athlete } from '../../../../models/athlete/interfaces/athlete'
import { GroupIndicator } from '../../well/interfaces/group-indicator'
import { AthleteBiometricEntryInterface } from '../../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { IpScoreDynamicAggregateDocument } from '../../../ipscore/services/interfaces/dynamic-aggregate-document'
import {
    IpScoreAggregateType,
    IpScoreHistoryAggregateType,
} from '../../../ipscore/types/ip-score-aggregate-types'
import { AthleteIndicator } from '../../well/interfaces/athlete-indicator'

export interface AggregateDocumentDirective {
    entity?: EntityType
    entry?: AthleteBiometricEntryInterface
    group?: GroupIndicator
    athleteIndicators?: AthleteIndicator[]
    athlete?: Athlete
    actionType?: DocMngrActionTypes
    collection?: AllAggregateCollectionType

    aggregateDocument?:
        | AggregateType
        | HistoryAggregateType
        | BinaryHistoryAggregateType
        | IpScoreDynamicAggregateDocument
        | IpScoreAggregateType
        | IpScoreHistoryAggregateType
    // aggregateHistoryDocument?: AggregateType | HistoryAggregateType | BinaryHistoryAggregateType;

    message: string
    // Generic BLOB for now, go wild!
    data?: any
    utcOffset: number

    // The logger server will be checking this property
    // and handling these events appropriately
    log?: {
        level: LogLevel
        // error: 0
        // warn: 1
        // info: 2
        // verbose: 3
        // debug: 4
        // silly: 5};
    }
    isConnected?: boolean
    loggerService?: boolean // LoggerService;
    t?: FirebaseFirestore.Transaction
    groupAthletes?: Array<Athlete>
}
