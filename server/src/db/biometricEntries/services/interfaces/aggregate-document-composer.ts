
import { WellAggregateCollection } from '../../enums/firestore-aggregate-collection';
import { AggregateDocumentDirective } from './aggregate-document-directive';
import { AthleteBiometricEntryInterface } from '../../well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { WellAggregateHistoryCollection } from '../../enums/firestore-aggregate-history-collection';
import { GroupIndicator } from '../../well/interfaces/group-indicator';
import { AthleteWeeklyHistoryAggregate } from '../../models/documents/aggregates';

export interface AggregateDocumentComposer {
    composeAggregateDocument(
        entity: AthleteBiometricEntryInterface | AthleteWeeklyHistoryAggregate,
        collection: WellAggregateCollection | WellAggregateHistoryCollection,
        documentId?: string,
        group?: GroupIndicator): AggregateDocumentDirective,
        // TODO: Remove this
        completedDocDirective?: AggregateDocumentDirective
}