import { AthleteBiometricEntryInterface } from "../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

export interface AggregateDocumentComposerService {

    readAthleteWeeklies(entry: AthleteBiometricEntryInterface): void;

    readAthleteMonthlyAggregateDocument(entry: AthleteBiometricEntryInterface): void;

}