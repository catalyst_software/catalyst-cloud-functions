import { AthleteBiometricEntryInterface } from "../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

export interface AggregateDocumentManagerService {
    updateAggregates(
        t: FirebaseFirestore.Transaction, entry: AthleteBiometricEntryInterface, athleteUID: string, skipAthletes: boolean): void;
}