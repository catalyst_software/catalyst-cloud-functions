import { LogLevel } from '../../enums/log-level'
import {
    HistoryAggregateType,
    AggregateType,
    AllAggregateType,
} from '../../../../models/types/biometric-aggregate-types'
import { Log } from '../../../../shared/logger/logger'
import { SocketLogDirective } from '../../../../shared/logger/interfaces/socket-log-directive'

import { BinaryBiometricHistoryAggregate } from '../../models/documents/aggregates/binary-history-aggregate.document';
import { removeUnusedProps } from './save-aggregates/utils/remove-unused-props';
import { removeUndefinedProps } from './save-aggregates/utils/remove-undefined-props';


// const saveUpdates = async (docRef: Promise<firestore.WriteResult>, updatedDoc: AthleteWeeklyAggregate | GroupWeeklyAggregate | GroupWeeklyHistoryAggregate | GroupMonthlyHistoryAggregate | BinaryBiometricHistoryAggregate) => {
//     return await docRef
//         .then((writeResult: firestore.WriteResult) => {
//             return handleSuccess(updatedDoc, writeResult);
//         })
//         .catch(err => {
//             return handleError(err, updatedDoc);
//         });
// }

export const saveAggregateDoc = async (
    updatedDoc: AggregateType | HistoryAggregateType | AllAggregateType | BinaryBiometricHistoryAggregate
) => {

    try {
        const aggregateDoc = updatedDoc;

        const { entry, ...theDocument } = aggregateDoc.toJS();

        removeUnusedProps(theDocument)

        removeUndefinedProps(theDocument);

        return Promise.resolve(theDocument)
        // return await saveUpdates(docRef, updatedDoc)

    } catch (err) {
        debugger;
        const ff: SocketLogDirective = {
            message: 'Unable to save document! - catch err => ' + err,
            data: {
                rest: { updatedDoc: JSON.stringify(updatedDoc), error: err },
            },
            level: LogLevel.Error,
        };
        Log(ff);

        debugger;

        return updatedDoc
    }

};
