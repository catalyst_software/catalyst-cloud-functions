import { isObject } from 'util';
export const removeUndefinedProps = (theDoc: {
    [key: string]: any;
}) => {
    Object.keys(theDoc).forEach(key => {
        (theDoc[key] === undefined ||
            theDoc[key] === '_data' ||
            theDoc[key] === 'data') &&
            delete theDoc[key];
        if (isObject(theDoc[key])) {
            removeUndefinedProps(theDoc[key]);
        }
    });
};
