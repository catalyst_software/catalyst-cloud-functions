import { AllAggregateType } from '../../../../../../models/types/biometric-aggregate-types';
import { isArray } from 'lodash';
// import { BinaryBiometricHistoryAggregate } from '../../../../models/documents/aggregates/binary-history-aggregate.document';

export const removeUnusedProps = (theDoc: AllAggregateType //|
    //  BinaryBiometricHistoryAggregate
) => {

    Object.keys(theDoc).forEach(key => {

        (isArray(theDoc[key]) && !theDoc[key].length) &&
            delete theDoc[key];
        // if (isObject(theDoc[key])) {
        //     removeUnusedProps(theDoc[key]);
        // }
    });
};
