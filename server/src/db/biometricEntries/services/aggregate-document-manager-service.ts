import { firestore } from 'firebase-admin';
import { isString, isDate, isArray } from 'lodash';
import moment from 'moment';

// Interfaces
import { AggregateDocumentManagerService as AggregateDocumentServiceInterface } from './interfaces/aggregate-document-manager-service'
import { AggregateDocumentDirective } from './interfaces/aggregate-document-directive';
import { Athlete } from '../../../models/athlete/interfaces/athlete';
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type';
import { binaryAggregateCollections, wellAggregateCollections } from '../../aggregate-collections';
import { LogInfo } from '../../../shared/logger/logger';
import { AthleteBiometricEntryInterface } from '../well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { WellAggregateCollection } from '../enums/firestore-aggregate-collection';
import { WellAggregateHistoryCollection } from '../enums/firestore-aggregate-history-collection';
import { WellAggregateBinaryHistoryCollection } from '../enums/firestore-aggregate-binary-history-collection';
import { AggregateDirectiveType } from '../../../models/types/biometric-aggregate-types';
import { getAggregateDocumentID } from '../../../api/app/v1/callables/get-aggregate-document-Id';
import { executeAggregate } from '../../../api/app/v1/callables/execute-aggregate';
import { getAthleteDocFromUID, getGroupDocFromSnapshot } from '../../refs';
import { db } from '../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../enums/firestore-collections';
// import { query } from 'express';
import { AthleteIndicator } from '../well/interfaces/athlete-indicator';

export class AggregateDocumentManagerService
    implements AggregateDocumentServiceInterface {

    async updateAggregates(
        t: FirebaseFirestore.Transaction,
        entry: AthleteBiometricEntryInterface, athleteUID: string) { // | Athlete | Group) {

        // const athleteDocUpdates: Array<AggregateIpScoreDirective | AggregateDocumentDirective> = [];

        entry.utcOffset = !!entry.utcOffset ? +entry.utcOffset : 0

        if (typeof (<any>entry).creationTimestamp === 'string') {
            console.warn('===... entry.creationTimestamp is string, running convertDateStringsToTimestamp(entry)')
            this.convertDateStringsToTimestamp(entry)

        } else if (isDate((<any>entry).dateTime)) {
            console.warn('===... entry.dateTime is a Date, running timestampFromDate((<any>entry).dateTime)')
            entry.dateTime = this.timestampFromDate((<any>entry).dateTime)


            console.warn('... entry.dateTime is a Date, running timestampFromDate(entry.creationTimestamp)');

            (<any>entry).creationTimestamp = this.timestampFromDate(
                (<any>entry).creationTimestamp as Date
            );

            if (entry.groups) {
                entry.groups.map(group => {
                    console.warn('... entry.dateTime is a Date, running timestampFromDate(group.creationTimestamp)');
                    group.creationTimestamp = this.timestampFromDate(
                        (<any>group).creationTimestamp as Date
                    )
                })
            }
        }
        if (!entry.athleteCreationTimestamp) {
            console.warn('----- Setting entry.athleteCreationTimestamp to entry.creationTimestamp', entry.creationTimestamp)
            entry.athleteCreationTimestamp = entry.creationTimestamp
        }
        const athlete = await getAthleteDocFromUID(athleteUID)
        const { utcOffset, skipAggregate } = entry

        console.warn(`Setting utcOffset ${entry.utcOffset} for athlete having UID ${athleteUID}`);
        // console.warn(`THE ENTRY JSON`, JSON.stringify({
        //     ...entry,
        //     creationTimestamp: entry.creationTimestamp.toDate(),
        //     dateTime: entry.dateTime.toDate(),
        //     athleteCreationTimestamp: entry.athleteCreationTimestamp.toDate(),
        // }))

        let collectionsToAggregate: {
            athlete: Array<({
                collection: WellAggregateCollection;
            } | {
                collection: WellAggregateHistoryCollection;
            } | {
                collection: WellAggregateBinaryHistoryCollection;
            })>,
            group: Array<({
                collection: WellAggregateCollection;
            } | {
                collection: WellAggregateHistoryCollection;
            } | {
                collection: WellAggregateBinaryHistoryCollection;
            })>;
        };
        switch (entry.lifestyleEntryType) {
            case LifestyleEntryType.Period:
            case LifestyleEntryType.Sick:
                collectionsToAggregate = binaryAggregateCollections;
                break;
            default:
                collectionsToAggregate = wellAggregateCollections;
                break;
        }

        debugger;
        const updateAthleteDirectives: Array<AggregateDirectiveType> = []

        if (!skipAggregate) {
            await Promise.all(collectionsToAggregate.athlete.map(async (athleteCollectionMeta) => {

                const updateAthleteDirective: AggregateDirectiveType = await handleAthleteAggregation(undefined, athleteCollectionMeta, athlete, entry, utcOffset);

                updateAthleteDirectives.push(updateAthleteDirective)

            }));
        }

        console.log('theEntry.includeGroupAggregation ', entry.includeGroupAggregation);

        let groupDocUpdates: AggregateDocumentDirective[]

        let includeGroups = false

        if (includeGroups || (entry.includeGroupAggregation && entry.groups && isArray(entry.groups))) {

            console.log('running handleGroupAggregation');
            groupDocUpdates = await handleGroupAggregation(undefined, entry, collectionsToAggregate, athlete, utcOffset);

        } else {
            console.warn('NOT running handleGroupAggregation', {
                includeGroups,
                entryGroups: entry.groups
            });

        }

        debugger;
        return { updateAthleteDirectives, groupDocUpdates };
        // return allGroupPromises;

        // return Promise.all([athletePromises, allGroupPromises])
        // return true
    }

    convertDateStringsToTimestamp(entry: AthleteBiometricEntryInterface) {
        entry.dateTime = this.timestampFromDate(new Date(entry.dateTime as any));
        entry.athleteCreationTimestamp = this.timestampFromDate(
            new Date((entry.athleteCreationTimestamp as any)
            ));

        if (entry.groups) {
            entry.groups.map(group => {
                group.creationTimestamp = this.timestampFromDate(
                    (group.creationTimestamp as any)
                )
            })
        }
    }

    private timestampFromDate(date: Date) {
        console.log('evt-timestamp-conversion', {
            type: 'timestampFromDate ',
            creationTimestamp: date,
        });
        return date
            ? this.toTimestamp(moment(date).toDate())
            : this.toTimestamp(moment('10/12/2015').toDate())
    }

    private toTimestamp(date: Date): firestore.Timestamp {
        LogInfo('firestore.Timestamp.fromDate(date);', date);
        return firestore.Timestamp.fromDate(date)

    }


}
async function handleAthleteAggregation(t: FirebaseFirestore.Transaction, athleteCollectionMeta: { collection: WellAggregateCollection; } | { collection: WellAggregateHistoryCollection; } | { collection: WellAggregateBinaryHistoryCollection; }, athlete: Athlete, entry: AthleteBiometricEntryInterface, utcOffset: number
): Promise<AggregateDocumentDirective> {

    const { collection } = athleteCollectionMeta;

    const updateAthleteDirective: AggregateDirectiveType = {
        collection: collection as WellAggregateCollection,
        message: `Update ${collection} Docs.`,
        athlete,
        entry,
        utcOffset,
        // isConnected,
        loggerService: true,
        t
    };

    debugger;

    const docID = getAggregateDocumentID(updateAthleteDirective, entry, entry.dateTime); // TODO: Check dateTime here

    const theUpdateResult: AggregateDocumentDirective = await executeAggregate(updateAthleteDirective, docID, entry)
        .then(async (fetchAthleteDocsDirective) => {
            // if (loggerService) {
            //     loggerService.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
            // }
            if (updateAthleteDirective !== fetchAthleteDocsDirective) {
                debugger;
            }
            return updateAthleteDirective;
        }).catch((err) => { console.error(err); return err; });

    return theUpdateResult;
}

async function handleGroupAggregation(
    t: FirebaseFirestore.Transaction,
    entry: AthleteBiometricEntryInterface, collectionsToAggregate: {
        athlete: ({ collection: WellAggregateCollection; } | { collection: WellAggregateHistoryCollection; } | { collection: WellAggregateBinaryHistoryCollection; })[];
        group: ({ collection: WellAggregateCollection; } | { collection: WellAggregateHistoryCollection; } | { collection: WellAggregateBinaryHistoryCollection; })[];
    }, athlete: Athlete, utcOffset: number
): Promise<AggregateDocumentDirective[]> {

    const groupDocUpdates: AggregateDocumentDirective[][] = await Promise.all(entry.groups.map(async (groupIndicator) => {

        const allTheResults: Array<AggregateDocumentDirective> = await Promise.all(collectionsToAggregate.group.map(async (groupCollectionMeta) => {

            const group = await getGroupDocFromSnapshot(groupIndicator.groupId)

            const { collection } = groupCollectionMeta;

            // let gdsdsdsd: { uid: string, athleteIndex: any} = {
            //     "uid": "7E06TpZCunMDDUBJFdTP-ONE",
            //     "athleteIndex": {
            //         "RBeAYcB3ElSO0aHx9I4eWsdlne23": true,
            //         "UquHta32oBfmCMcKogWoh6IcAbN2": true,
            //         "ovUnBhVzEFOKvcyvz8WEu4PHCed2": true,
            //         "20HPPBLgViQ3CgJnSJjxLq3uAVZ2": true,

            //     },
            // }

            // const fff = gdsdsdsd


            let athleteIndicators: AthleteIndicator[] = []

            if (group.athleteIndex) {
                athleteIndicators = await db.collection(FirestoreCollection.Groups).doc(groupIndicator.groupId).collection('athletes').get().then((querySnap) => {
                    if (querySnap.size) {
                        return querySnap.docs.map((queryDocSnap) => {
                            return queryDocSnap.data() as AthleteIndicator
                        })
                    } else {
                        return []
                    }
                })
            } else {
                athleteIndicators = group.athletes
            }

            const updateAthleteDirective: AggregateDirectiveType = {
                collection: collection as WellAggregateCollection,
                message: `Update ${collection} Docs.`,
                athlete,
                entry,
                utcOffset,
                group,
                athleteIndicators,
                // isConnected,
                loggerService: true,
                t
            };


            const docID = getAggregateDocumentID(updateAthleteDirective, entry, entry.dateTime); // TODO: check dateTime here

            debugger;

            const theUpdateResult = await executeAggregate(updateAthleteDirective, docID, entry)
                .then(async (fetchAthleteDocsDirective) => {
                    // if (loggerService) {
                    //     loggerService.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
                    // }
                    if (updateAthleteDirective !== fetchAthleteDocsDirective) {
                        debugger;
                    }

                    return fetchAthleteDocsDirective;
                });

            return theUpdateResult;
        }));

        return allTheResults;
    }));

    const concatenatedGroups: Array<AggregateDocumentDirective> = [].concat(...groupDocUpdates);

    debugger;

    // const theRes = await Promise.all(athleteDocUpdates);
    // const theressss = [].concat(...theRes) as Array<AggregateDirectiveType>;

    debugger;

    return concatenatedGroups;
}

