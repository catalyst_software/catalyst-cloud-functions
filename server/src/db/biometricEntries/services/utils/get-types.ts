import { WellAggregateCollection } from '../../enums/firestore-aggregate-collection'
import { WellAggregateHistoryCollection } from '../../enums/firestore-aggregate-history-collection'
import { WellAggregateBinaryHistoryCollection } from '../../enums/firestore-aggregate-binary-history-collection'
import {
    isAthleteCollection,
    isHistoryCollection,
    isWeeklyWellCollection,
} from '../../aliases/collections'

import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type'

import { HealthComponentType } from '../../well/enums/health-component-type'
import { AggregateDocumentType } from '../../well/enums/aggregate-document-type'


import { AggregateEntityType } from '../../enums/aggregate-entity-type'

export const getTypes = (
    collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection,
    entryType: number
) => {
    let healthComponentType: HealthComponentType;
    let aggregateEntityType: AggregateEntityType;
    let aggregateDocumentType: AggregateDocumentType;

    switch (+entryType) {
        case LifestyleEntryType.Sick:
        case LifestyleEntryType.Period:
            //TODO: HealthComponentType??
            healthComponentType = HealthComponentType.Body;
            break;
        case LifestyleEntryType.Sleep:
        case LifestyleEntryType.Mood:
            healthComponentType = HealthComponentType.Brain;
            break;
        case LifestyleEntryType.Fatigue:
        case LifestyleEntryType.Pain:
            healthComponentType = HealthComponentType.Body;
            break;
        case LifestyleEntryType.Food:
            healthComponentType = HealthComponentType.Food;
            break;

        case LifestyleEntryType.Training:
            healthComponentType = HealthComponentType.Training;
            break;

        case LifestyleEntryType.Distance:
            healthComponentType = HealthComponentType.Body;
            break;

        case LifestyleEntryType.HeartRate:
            healthComponentType = HealthComponentType.Body;
            break;

        case LifestyleEntryType.IpScore:
            healthComponentType = HealthComponentType.IpScore;
            break;

        case LifestyleEntryType.Steps:
            healthComponentType = HealthComponentType.Body;
            break
    }

    // Set AggregateEntityType
    isAthleteCollection(collection)
        ? (aggregateEntityType = AggregateEntityType.Athlete)
        : (aggregateEntityType = AggregateEntityType.Group);

    // Set AggregateDocumentType
    !isHistoryCollection(collection)
        // Not Historic
        ? isWeeklyWellCollection(collection)
            ? (aggregateDocumentType = AggregateDocumentType.Weekly)
            : (aggregateDocumentType = AggregateDocumentType.Monthly)
        // Historic
        : isWeeklyWellCollection(collection)
            ? (aggregateDocumentType = AggregateDocumentType.WeeklyHistory)
            : (aggregateDocumentType = AggregateDocumentType.MonthlyHistory);

    if (!aggregateDocumentType) {
        // Daily
        aggregateDocumentType = AggregateDocumentType.Daily
    }
    return { healthComponentType, aggregateEntityType, aggregateDocumentType }
};
