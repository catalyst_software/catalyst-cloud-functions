
import { WellAggregateCollection } from '../../enums/firestore-aggregate-collection';

import { getHistoryDocId } from '.';
import { AthleteWeeklyHistoryAggregate } from '../../models/documents/aggregates';


export const processHistoryDocument = async (
    collection: WellAggregateCollection,
    // TODO: AthleteWeeklyHistoryAggregate ???
    entry: AthleteWeeklyHistoryAggregate,
    aggregateDocumentComposer): Promise<string> => {

    const documentIs = getHistoryDocId(
        entry.entityId,
        entry.lifestyleEntryType);

    if (documentIs !== '') {
        return await aggregateDocumentComposer.createAggregateDocument(entry, collection, undefined, documentIs)
    } else {
        return Promise.reject('Could not create entityId')
    }
};
