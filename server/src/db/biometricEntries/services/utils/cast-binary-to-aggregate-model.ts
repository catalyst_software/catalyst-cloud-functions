
import { GroupIndicator } from '../../well/interfaces/group-indicator';
import { firestore } from 'firebase-admin'
import * as _ from 'lodash'

// AthleteBiometricEntry
import { AthleteBiometricEntryInterface } from '../../well/aggregate/interfaces/athlete/athelete-biometric-entry'


import { LogInfo } from '../../../../shared/logger/logger'
import { BinaryBiometricHistoryAggregate } from '../../models/documents/aggregates/binary-history-aggregate.document';
import { WellAggregateBinaryHistoryCollection } from '../../enums/firestore-aggregate-binary-history-collection';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';

export const castToBinaryAggregateModel = (
    entry: AthleteBiometricEntryInterface,
    collection: WellAggregateBinaryHistoryCollection,
    aggregateDocumentSnapshot: firestore.DocumentSnapshot,
    documentId?: string,
    group?: GroupIndicator
): BinaryBiometricHistoryAggregate => {

    const docData = aggregateDocumentSnapshot.data();

    const aggregateDocument: BinaryBiometricHistoryAggregate = new BinaryBiometricHistoryAggregate({
        entry,
        group,
        collection,
        lifestyleEntryType: LifestyleEntryType[entry.lifestyleEntryType],
        uid: documentId,
        ...(docData as BinaryBiometricHistoryAggregate),
    });

    LogInfo('casted docType', {
        collection,
        aggregateDoc: aggregateDocument,
    });

    return aggregateDocument
};
