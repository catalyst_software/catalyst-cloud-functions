import { firestore } from 'firebase-admin';
import moment from 'moment';

// Interfaces
import { AthleteBiometricEntryInterface } from '../../well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { BiometricAggregate } from '../../well/aggregate/interfaces/dailies';
import { GroupIndicator } from '../../well/interfaces/group-indicator';
//ENUMS
import { WellAggregateCollection } from '../../enums/firestore-aggregate-collection';
import { WellAggregateHistoryCollection } from '../../enums/firestore-aggregate-history-collection';
// Models
import { AthleteDailyAggregateModel } from '../../well/aggregate/athelete-biometric-daily-aggregate.model';
import { GroupDailyAggregateModel } from '../../well/aggregate/group-biometric-daily-aggregate.model';
// Documents
import { GroupDailyDocument } from '../../models/documents/aggregates/group/group-daily-aggregate.document';
import { AthleteDailyDocument } from '../../models/documents/aggregates/athlete/athelete-daily-aggregate.document';
// Utils
import { getTypes } from './get-types';
import { isWeeklyWellCollection } from '../../aliases/collections';


export const createDailies = (
    entry: AthleteBiometricEntryInterface,
    collection: WellAggregateCollection | WellAggregateHistoryCollection,
    fromDate: firestore.Timestamp,
    group?: GroupIndicator
): Array<BiometricAggregate> => {

    const {
        // healthComponentType,
        aggregateEntityType,
        aggregateDocumentType
    } = getTypes(collection, +entry.lifestyleEntryType);

    const sharedDailyProperties = {
        uid: '',
 
        organizationId: entry.organizationId,


        healthComponentType: entry.healthComponentType,
        lifestyleEntryType: entry.lifestyleEntryType,

        historicalValues: [],
        value: 0,

        dateTime: fromDate,

        painComponents: [],
        trainingComponents: [],
        mealComponents: [],
        simpleMealComponents: [],

        aggregateEntityType,
        // aggregateDocumentType: AggregateDocumentType.Daily,
        aggregateDocumentType,
        // healthComponentType
        // value: number,
        brain: undefined,
        body: undefined,
        food: undefined,
        training: undefined

    };

    if (+entry.lifestyleEntryType === 15) {
        sharedDailyProperties.mealComponents = undefined
        sharedDailyProperties.painComponents = undefined
        sharedDailyProperties.trainingComponents = undefined
    } else {
        sharedDailyProperties.simpleMealComponents = undefined
    }

    const dailies: Array<BiometricAggregate> = [];
    
    if (isWeeklyWellCollection(collection)) {
        [0, 1, 2, 3, 4, 5, 6].forEach((offset, i) => {
            sharedDailyProperties.dateTime = firestore.Timestamp.fromDate(moment(fromDate.toDate()).add(offset, 'day').toDate());
            sharedDailyProperties.uid = i.toString();

            if (group) {
                const groupDaily: GroupDailyAggregateModel = {
                    ...GroupDailyDocument,
                    ...sharedDailyProperties,

                    groupId: group.groupId,
                    groupName: group.groupName

                };
                dailies.push(groupDaily);
            } else {
                const athleteDaily: AthleteDailyAggregateModel = {
                    ...AthleteDailyDocument,
                    ...sharedDailyProperties,

                    userId: entry.userId,
                    fullName: entry.fullName

                };
                dailies.push(athleteDaily);
            }
        });
    }

    return dailies;
};
