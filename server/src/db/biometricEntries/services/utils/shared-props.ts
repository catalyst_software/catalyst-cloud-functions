
import { createDailies } from '.';
import { GroupIndicator } from '../../well/interfaces/group-indicator';

import { AthleteBiometricEntryInterface } from '../../well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { getTypes } from './get-types';
import { WellAggregateCollection } from '../../enums/firestore-aggregate-collection';
import { WellAggregateHistoryCollection } from '../../enums/firestore-aggregate-history-collection';
import { EntityType } from '../../well/enums/entity-type';
import { AggregateDocumentType } from '../../well/enums/aggregate-document-type';
import { getWeekNumber, getWeekFromDate } from '../../../../shared/utils/moment';
import { WellAggregateBinaryHistoryCollection } from '../../enums/firestore-aggregate-binary-history-collection';
import { isBinaryCollection } from '../../aliases/collections';

export const setSharedProps = (
    entry: AthleteBiometricEntryInterface, group: GroupIndicator, collection: WellAggregateCollection | WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection,
    documentId: string, dateTime: FirebaseFirestore.Timestamp,
    utcOffset: number
) => {
    debugger;

    const { organizationId,
        lifestyleEntryType,
        healthComponentType } = entry;

    const entityId = group ? group.groupId : entry.uid ? entry.uid : entry.userId;
    const entityName = group ? group.groupName : entry.fullName;
    const creationTimestamp = group ? group.creationTimestamp : entry.athleteCreationTimestamp;

    const {
        //  healthComponentType,
        aggregateEntityType, aggregateDocumentType
    } = getTypes(
        collection,
        +lifestyleEntryType
    );

    const sharedBaseProperties
        // : AthleteWeeklyAggregate | GroupWeeklyAggregate |
        // AthleteMonthlyAggregate | GroupMonthlyAggregate
        = {

        uid: documentId,
        organizationId,
        // creationTimestamp: firestore.Timestamp.now(),
        entry,

        entityId,
        entityName,

        lifestyleEntryType,

        healthComponentType,
        aggregateEntityType: EntityType[aggregateEntityType],
        aggregateDocumentType: AggregateDocumentType[aggregateDocumentType]
    };


    let dailies = []

    if (!isBinaryCollection(collection)) {
        dailies = createDailies(entry, collection as any, getWeekFromDate(dateTime, utcOffset), group)
    }
    const weeklySharedProps = {
        weekNumber: getWeekNumber(creationTimestamp, dateTime, utcOffset),
        dailies
    };
    // const monthlySharedProps
    //     // : AthleteMonthlyAggregate | GroupMonthlyAggregate
    //     = {
    //     fromDate: getMonthFromDate(dateTime, utcOffset),
    //     toDate: getMonthToDate(dateTime, utcOffset)
    // };
    return { sharedBaseProperties, weeklySharedProps };

};


