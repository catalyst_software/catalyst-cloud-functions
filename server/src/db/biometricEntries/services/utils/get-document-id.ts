import { firestore } from 'firebase-admin';
import moment from 'moment';

import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { getWeekNumber, getMonthStart } from '../../../../shared/utils/moment';



export const getWeeklyDocId = (
    entityId: string,
    creationTimestamp: firestore.Timestamp,
    dateTime: firestore.Timestamp,
    lifestyleEntryType: LifestyleEntryType,
    utcOffset: number
): string => {
    // ID_%_WN / Weekly
    // ID_2_1 / ID_3
    const docId = `${entityId}_${lifestyleEntryType}_${getWeekNumber(creationTimestamp, dateTime, utcOffset)}`;

    return docId;
};

export const getMonthlyDocId = (
    entityId: string,
    dateTime: firestore.Timestamp,
    lifestyleEntryType: LifestyleEntryType,
    utcOffset: number
): string => {
    // ID_%_MMYYYYY / Monthly
    // ID_1_012019 / ID_4_122018
    const docId = `${entityId}_${lifestyleEntryType}_${moment(getMonthStart(dateTime, utcOffset)).format('MMYYYY')}`;
    return docId;

};

export const getHistoryDocId = (
    entityId: string,
    lifestyleEntryType: number
): string => {
    // ID_%_WN / Weekly
    // ID_2_1 / ID_3

    const docId = `${entityId}_${lifestyleEntryType}`;
    return docId;
};
