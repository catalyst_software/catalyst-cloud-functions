


export enum WellAggregateHistoryCollection {
    AthleteWeeklyHistory = 'athleteBiometricWeeklyHistoryAggregates',
    AthleteMonthlyHistory = 'athleteBiometricMonthlyHistoryAggregates',
    GroupWeeklyHistory = 'groupBiometricWeeklyHistoryAggregates',
    GroupMonthlyHistory = 'groupBinaryBiometricHistoryAggregates',
}