export enum AggregateEntityType {
    Athlete = "Athlete",
    Group = "Group"
}