export enum DocMngrActionTypes {
    Read = 'read',
    // Update = 'update',
    Aggregate = 'aggregate',
    Error = "error",
    Write = "write",
    CreateHistoryDocs = "createHistoryDocs",
    HistoryDocRetrieved = "HistoryDocRetrieved",
    Save = "Save",
    HandleNext = "HandleNext",
    ReadNext = "ReadNext",
    UpdateHistoryDocs = "UpdateHistoryDocs",
    Complete = "Complete",
    AddToQueue = "AddToQueue",
    RemoveFromQueue = "RemoveFromQueue",
    ContinueAggregattion = "ContinueAggregattion"
}