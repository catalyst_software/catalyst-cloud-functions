export enum WellAggregateCollection {
    AthleteWeekly = 'athleteBiometricWeeklyAggregates',
    // AthleteMonthly = 'athleteBiometricMonthlyAggregates',
    GroupWeekly = 'groupBiometricWeeklyAggregates',
    // GroupMonthly = 'groupBiometricMonthlyAggregates'
}