export enum SheduledTaskTypes {
    IPScoreNonEnagagement = "ipScoreNonEnagagement",
    ScoreSurveys = "scoreSurveys"
}