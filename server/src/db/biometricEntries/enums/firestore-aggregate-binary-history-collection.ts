


export enum WellAggregateBinaryHistoryCollection {
    AthleteBinaryHistory = 'BinaryBiometricHistoryAggregates',
    GroupBinaryHistory = 'groupBinaryBiometricHistoryAggregates'
}