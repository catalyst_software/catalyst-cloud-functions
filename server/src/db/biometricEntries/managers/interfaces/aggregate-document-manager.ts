import { Subject } from 'rxjs';

import { AthleteBiometricEntryModel } from '../../well/athelete-biometric-entry.model';
import { AggregateDocumentDirective } from '../../services/interfaces/aggregate-document-directive';
import { DocMngrActionTypes } from '../../enums/document-manager-action-types';

export interface AggregateDocumentManager {
    aggregateDocs: Subject<AggregateDocumentDirective>

    doRead(
        entity: AthleteBiometricEntryModel,
        athleteUID: string,
        type: DocMngrActionTypes,
        from: string,
        logLevel: string,
        t: FirebaseFirestore.Transaction): void;
    // doWrite(entry: AthleteBiometricEntry, aggregateDoc: BiometricAggregateModel): void
}