import { Subject } from 'rxjs'

import { AggregateDocumentManager as AggregateDocumentManagerInterface } from './interfaces/aggregate-document-manager'
// Interfaces
import { AggregateDocumentDirective } from '../services/interfaces/aggregate-document-directive'
// Services

import { initialiseLoggers } from '../../../shared/logger/service/logger-service'
// AthleteBiometricEntry
import { AthleteBiometricEntryModel } from '../well/athelete-biometric-entry.model'

import { initialiseFirebase } from '../../../shared/init/initialise-firebase'

import { setLoggerLevel, LogInfo } from '../../../shared/logger/logger'

import { LogLevel } from '../enums/log-level'
import { DocMngrActionTypes } from '../enums/document-manager-action-types'

import { AthleteBiometricEntryInterface } from '../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { AggregateDocumentManagerService } from '../services/aggregate-document-manager-service'

//
// Our document store. We export it here we we can grab hold of it
// anywhere in the application to .next() & .subscribe()
// and, if you are really keen on an observable you can do that as well... aggregateDocs.asObservable
//



// import {  } from './aggregate-document-manager-service'


export const docManagerService = new AggregateDocumentManagerService();


export let selectedAggregator;

export class AggregateDocumentManager
    implements AggregateDocumentManagerInterface {
    entity: AthleteBiometricEntryModel;
    aggregateDocs: Subject<AggregateDocumentDirective>;

    initialised = false;
    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     */
    public bootstrap(): boolean {
        initialiseFirebase('bootstrap');
        initialiseLoggers(LogLevel.Debug, 'bootstrap()');

        LogInfo('bootstrap() called!!!!!!!!!!!!');

        return true
    }

    constructor() {
        //configure application
        this.initialise();
        // this.setupSubscriptions()
    }

    /**
     * Initialise application
     *
     * Initilise Logger, Firebase DocManager and Aggregator related services
     *
     * @private
     * @memberof AggregateDocumentManager
     */
    private initialise() {
        this.bootstrap();
        this.initialised = true
    }

    /**
     * Reads the BiometricAggregateModel documets
     *
     * Creates a new Observable with the aggregateDocs Subject as the source.
     * You can do this to create customize Observer-side logic of the Subject
     * and conceal it from code that uses the Observable.
     *
     * @param {AthleteBiometricEntryInterface} entity
     * @returns {Observable<AggregateDocumentDirective>}
     * @memberof AggregateDocumentManager
     */
    async doRead(
        entity: AthleteBiometricEntryInterface, // | Athlete | Group,
        athleteUID: string,
        type: DocMngrActionTypes,
        from: string,
        logLevel: string,
        t
    ) {

        setLoggerLevel(logLevel);

        LogInfo(
            'doRead().aggserv.processAggregateRequest(entry) called from ',
            from
        );

        const result = await docManagerService.updateAggregates(t, entity, athleteUID).then((data) => {
            console.log('complete?!.....................', { documents: data })
            return data
        })

        return result
    }

    // async doGroupRead(
    //     entity: AthleteBiometricEntryInterface, // | Athlete | Group,
    //     athleteUID: string,
    //     type: DocMngrActionTypes,
    //     from: string,
    //     logLevel: string,
    //     t: FirebaseFirestore.Transaction
    // ) {

    //     setLoggerLevel(logLevel);

    //     LogInfo(
    //         'doRead().aggserv.processAggregateRequest(entry) called from ',
    //         from
    //     );

    //     const result = await docManagerService.updateAggregates(undefined, entity, athleteUID).then((data) => {
    //         console.log('complete?!.....................', { documents: data })
    //         return data
    //     })

    //     return result
    // }
}
