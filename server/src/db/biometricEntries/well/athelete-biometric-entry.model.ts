import { firestore } from 'firebase-admin';

//Interfaces
import { AthleteBiometricEntryInterface } from './aggregate/interfaces/athlete/athelete-biometric-entry';
import { GeoPointInterface } from '../../../models/geopoint.model';
import { TrainingComponentInterface } from './interfaces/training-component';
import { PainComponent } from './interfaces/pain-component';
import { MealComponent } from './interfaces/meal-component';
//Enums
import { HealthComponentType } from './enums/health-component-type';
import { LifestyleEntryType } from '../../../models/enums/lifestyle-entry-type';
import { Group } from '../../../models/group/group.model';
import { AggregateEntityType } from '../enums/aggregate-entity-type';
import { AggregateDocumentType } from './enums/aggregate-document-type';

export class AthleteBiometricEntryModel implements AthleteBiometricEntryInterface {
    entityId: string;
    entityName: string;
    entry?: AthleteBiometricEntryInterface;
    collection?: any;
    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;
    values?: any[];
    simpleMealComponents?: MealComponent[];
    firebaseLogsLevel?: any;
    isToday?: boolean;
    isIncrease?: boolean;
    isDecrease?: boolean;
    classification?: string;
    tag?: string;
    analyticsHistory?: boolean;
    unixTimestamp?: number;
    // entityName?: any;
    // dateTimeOld?: Date;
    // firebaseLogsLevel?: any;
    // isToday?: boolean;
    // isIncrease?: boolean;
    // isDecrease?: boolean;
    // classification?: string;
    // tag?: string;
    // analyticsHistory?: boolean;
    // unixTimestamp?: number;

    uid: string;
    userId?: string;
    firstName?: string;
    lastName?: string;

    guid?: string;

    organizationId?: string;

    fullName?: string;
    athleteCreationTimestamp?: firestore.Timestamp;
    creationTimestamp?: firestore.Timestamp; // For backward compatability

    dateTime?: firestore.Timestamp;
    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;
    
    includeGroupAggregation?: boolean;

    value?: number;
    historicalValues?: Array<any>;
    points?: number;

    groups?: Array<Group>;
    geoPoint?: GeoPointInterface;

    painComponents?: Array<PainComponent>;
    mealComponents?: Array<MealComponent>;
    trainingComponents?: Array<TrainingComponentInterface>;
    queueGuid?: any;
    utcOffset?: number;
    skipAggregate?: boolean;
    
    constructor(props: AthleteBiometricEntryModel) {

        debugger;
        this.uid = props.userId
        // super(props);
    }
}
