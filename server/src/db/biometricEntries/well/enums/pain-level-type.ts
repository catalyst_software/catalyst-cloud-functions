export enum PainLevelType {
    NoPain = 0,
    VeryLittle,
    Little,
    Painful,
    VeryPainful
}