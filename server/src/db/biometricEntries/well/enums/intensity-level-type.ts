export enum IntensityLevelType {
    Light = 0,
    Moderate,
    Vigorous
}