export enum AggregateDocumentType {
    Daily = 0,
    Weekly, // 1
    Monthly, // 2
    WeeklyHistory, // 3
    MonthlyHistory, // 4
    BinaryHistory
}