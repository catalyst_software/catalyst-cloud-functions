export enum BodyLocationType {
    Neck = 0,
    Back = 1,
    UpperBack = 2,
    LowerBack = 3,
    LeftShoulder = 4,
    RightShoulder = 5,
    LeftElbow = 6,
    RightElbow = 7,
    LeftWrist = 8,
    RightWrist = 9,
    LeftHip = 10,
    RightHip = 11,
    LeftKnee = 12,
    RightKnee = 13,
    LeftAnkle = 14,
    RightAnkle = 15
}