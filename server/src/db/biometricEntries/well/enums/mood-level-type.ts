export enum MoodLevelType {
    VeryPleasant = 0,
    Pleasant,
    Average,
    Unpleasant,
    VeryUnpleasant
}