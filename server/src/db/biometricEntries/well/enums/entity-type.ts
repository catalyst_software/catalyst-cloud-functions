export enum EntityType {
    Athlete = 'Athlete',
    Group = 'Group',
    Organization = 'Organization'
}