export enum MealTimeType {
    Breakfast = 0,
    MorningSnack,
    Lunch,
    AfternoonSnack,
    Dinner
}