export enum FatigueLevelType {
    NotFatigued = 0,
    ModeratelyFatigued,
    Fatigued,
    VeryFatigued,
    ExtremelyFatigued
}