export enum HealthComponentType {
    Brain = 0,
    Body,
    Food,
    Training,
    IpScore,
    Wearables,
    Programs,
    Binary
}