import { HealthComponentType } from '../enums/health-component-type';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { MealComponent } from '../interfaces/meal-component';
import { PainComponent } from '../interfaces/pain-component';
import { TrainingComponentInterface } from '../interfaces/training-component';
import { BiometricAggregateModel } from './biometric-aggregate.model';
import { GroupMonthlyAggregateInterface } from './interfaces/group/group-biometric-monthly-aggregate';
import { WellAggregateCollection } from '../../enums/firestore-aggregate-collection';
import { AthleteBiometricEntryInterface } from './interfaces/athlete/athelete-biometric-entry';

export class GroupMonthlyAggregateModel extends BiometricAggregateModel 
implements GroupMonthlyAggregateInterface {
  fromDate?: FirebaseFirestore.Timestamp;
  toDate?: FirebaseFirestore.Timestamp;

  documentId?: string;
  entry?: AthleteBiometricEntryInterface;
  collection: WellAggregateCollection;
  groupId?: string;
  groupName?: string;

  guid: string;

  organizationId?: string;
  
  healthComponentType?: HealthComponentType;
  lifestyleEntryType?: LifestyleEntryType;

  value?: any;
  historicalValues?: Array<any>;
  points?: number;

  painComponents?: Array<PainComponent>; 
  mealComponents?: Array<MealComponent>;
  trainingComponents?: Array<TrainingComponentInterface>;
 
}
