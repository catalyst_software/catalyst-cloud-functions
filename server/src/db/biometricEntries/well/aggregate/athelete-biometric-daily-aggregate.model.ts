import { firestore } from 'firebase-admin';

import { AthleteDailyAggregate } from './interfaces/athlete/athlete-biometric-daily-aggregate';

import { HealthComponentType } from '../enums/health-component-type';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { MealComponent } from '../interfaces/meal-component';
import { PainComponent } from '../interfaces/pain-component';
import { TrainingComponentInterface } from '../interfaces/training-component';
import { BiometricAggregateModel } from './biometric-aggregate.model';
import { AthleteBiometricEntryInterface } from './interfaces/athlete/athelete-biometric-entry';

export class AthleteDailyAggregateModel extends BiometricAggregateModel
    implements AthleteDailyAggregate {
    entry?: AthleteBiometricEntryInterface;
    guid?: string;
    uid?: string;
    userId?: string;
    fullName?: string;

    entityId: string;
    entityName: string;

    dateTime?: firestore.Timestamp;

    organizationId?: string;

    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    value?: number;
    historicalValues?: Array<any>;
    points?: number;

    painComponents?: Array<PainComponent>;
    mealComponents?: Array<MealComponent>;
    simpleMealComponents?: Array<MealComponent>;
    trainingComponents?: Array<TrainingComponentInterface>;

    constructor(
        uid?: string,
        entry?: AthleteBiometricEntryInterface,
        fullName?: string,

        dateTime?: firestore.Timestamp,

        guid?: string,

        organizationId?: string,

        healthComponentType?: HealthComponentType,
        lifestyleEntryType?: LifestyleEntryType,

        value?: number,
        historicalValues?: Array<any>,
        points?: number,

        painComponents?: Array<PainComponent>,
        mealComponents?: Array<MealComponent>,
        simpleMealComponents?: Array<MealComponent>,
        trainingComponents?: Array<TrainingComponentInterface>,
    ) {
        super();


        this.uid = uid;
        this.entry = entry;
        this.fullName = fullName;
        this.dateTime = dateTime;
        this.guid = guid;
        this.organizationId = organizationId;
        this.healthComponentType = healthComponentType;
        this.lifestyleEntryType = lifestyleEntryType;

        this.value = value;
        this.historicalValues = historicalValues;
        this.points = points;
        this.painComponents = painComponents;
        this.mealComponents = mealComponents;
        this.simpleMealComponents = simpleMealComponents;
        this.trainingComponents = trainingComponents;
    }
}
