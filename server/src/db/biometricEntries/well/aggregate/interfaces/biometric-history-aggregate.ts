import { LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { AggregateDocumentType } from '../../enums/aggregate-document-type';
import { WellAggregateHistoryCollection } from '../../../enums/firestore-aggregate-history-collection';
import { AthleteBiometricEntryInterface } from './athlete/athelete-biometric-entry';
import { HealthComponentType } from '../../enums/health-component-type';
import { IpScoreAggregateComponentInterface } from '../../../../ipscore/models/interfaces/ipscore-component-aggregate';
import { IpScoreAggregateTrackingCacheInterface } from '../../../../ipscore/models/interfaces/ipscore-aggregate-tracking-cache';
import { BiometricAggregateTrackingCacheInterface } from './biometric-aggregate-tracking-cache';
import { AggregateEntityType } from '../../../enums/aggregate-entity-type';

export interface BiometricHistoryAggregate {
    guid?: string;
    uid?: string;
    // documentId: string;
    entityId: string;
    entityName: string;
    collection?: WellAggregateHistoryCollection;

    entry?: AthleteBiometricEntryInterface;

    lifestyleEntryType?: LifestyleEntryType;

    aggregateDocumentType?: AggregateDocumentType;
    aggregateEntityType?: AggregateEntityType;
    healthComponentType?: HealthComponentType;
    utcOffset?: number;


    organizationId?: string;


    componentValues?: Array<IpScoreAggregateComponentInterface>;
    trackingCache?: IpScoreAggregateTrackingCacheInterface | BiometricAggregateTrackingCacheInterface;
    recentEntries?: Array<IpScoreAggregateComponentInterface>;

    values?: Array<any>;


}


export interface BinaryBiometric {
    on: Date
    off?: Date
    uid?: string
}