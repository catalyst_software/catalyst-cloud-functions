export interface BiometricAggregateTrackingCacheInterface {
    index: number;
    valueCache: Array<any>
    componentCache: Array<Array<any>>;
}