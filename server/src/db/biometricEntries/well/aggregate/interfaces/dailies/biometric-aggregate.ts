import { WellAggregateCollection } from "../../../../enums/firestore-aggregate-collection";
import { WellAggregateHistoryCollection } from "../../../../enums/firestore-aggregate-history-collection";
import { HealthComponentType } from "../../../enums/health-component-type";
import { LifestyleEntryType } from "../../../../../../models/enums/lifestyle-entry-type";
import { PainComponent } from "../../../interfaces/pain-component";
import { MealComponent } from "../../../interfaces/meal-component";
import { TrainingComponentInterface } from "../../../interfaces/training-component";


export interface BiometricAggregate {
    collection?: WellAggregateCollection | WellAggregateHistoryCollection,

    uid?: string;
    documentId?: string;
    guid?: string;

    entityId: string;
    entityName: string;

    organizationId?: string;

    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    value?: any;
    historicalValues?: Array<any>;
    points?: number;

    painComponents?: Array<PainComponent>;
    mealComponents?: Array<MealComponent>;
    trainingComponents?: Array<TrainingComponentInterface>;
}
