// import { BiometricAggregate } from './biometric-aggregate';
import { firestore } from 'firebase-admin';
import { BiometricAggregate } from '.';
export interface GroupDailyAggregate extends BiometricAggregate {
    groupId?: string;
    groupName?: string;

    dateTime?: firestore.Timestamp; 
}