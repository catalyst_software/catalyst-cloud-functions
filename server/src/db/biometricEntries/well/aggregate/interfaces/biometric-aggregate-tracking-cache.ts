import { BiometricAggregateComponentHistoryInterface } from "./biometric-aggregate-component-history";

export interface BiometricAggregateTrackingCacheInterface {
    index: number;
    valueCache: Array<number>;
    componentCache: BiometricAggregateComponentHistoryInterface;
}