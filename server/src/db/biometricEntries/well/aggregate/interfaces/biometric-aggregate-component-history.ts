import { PainComponent } from "../../interfaces/pain-component";
import { MealComponent } from "../../interfaces/meal-component";
import { TrainingComponentInterface } from "../../interfaces/training-component";

export interface BiometricAggregateComponentHistoryInterface {
	index: number; //Week / Month
    components: Array<PainComponent | MealComponent | TrainingComponentInterface>;
    // Simple Food 
    value?: number;
}