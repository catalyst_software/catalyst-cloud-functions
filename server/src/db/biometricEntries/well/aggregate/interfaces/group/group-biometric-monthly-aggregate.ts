import { firestore } from 'firebase-admin';

import { WellAggregateCollection } from '../../../../enums/firestore-aggregate-collection';
import { AthleteBiometricEntryInterface } from '../athlete/athelete-biometric-entry';

export interface GroupMonthlyAggregateInterface extends AthleteBiometricEntryInterface {
    entry?: AthleteBiometricEntryInterface;
    collection?: WellAggregateCollection;
    guid?: string;
    uid?: string;

    fromDate?: firestore.Timestamp;
    toDate?: firestore.Timestamp;

    recentEntries?: Array<AthleteBiometricEntryInterface>;
}