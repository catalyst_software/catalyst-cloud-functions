import { firestore } from 'firebase-admin';

import { AthleteBiometricEntryInterface } from '../athlete/athelete-biometric-entry';

export interface GroupDailyAggregate extends AthleteBiometricEntryInterface {
    uid?: string;
    guid?: string;

    groupId?: string;
    groupName?: string;

    dateTime?: firestore.Timestamp; 
}

