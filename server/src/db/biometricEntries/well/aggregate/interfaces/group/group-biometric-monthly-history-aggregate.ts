
import { WellAggregateHistoryCollection } from '../../../../enums/firestore-aggregate-history-collection';
import { AthleteBiometricEntryInterface } from '../athlete/athelete-biometric-entry';
import { BiometricAggregateComponentHistoryInterface } from '../biometric-aggregate-component-history';
import { BiometricAggregateTrackingCacheInterface } from '../biometric-aggregate-tracking-cache';

export interface GroupMonthlyHistoryAggregateInterface extends AthleteBiometricEntryInterface {
    entry?: AthleteBiometricEntryInterface;
    collection?: WellAggregateHistoryCollection;
    guid?: string;
    uid?: string;

    
    values?: Array<any>;
    historicalValues?: any[]
    painComponentHistory?: BiometricAggregateComponentHistoryInterface[];
    mealComponentHistory?: BiometricAggregateComponentHistoryInterface[];
    trainingComponentHistory?: BiometricAggregateComponentHistoryInterface[];

    trackingCache?: BiometricAggregateTrackingCacheInterface;

    recentEntries?: Array<AthleteBiometricEntryInterface>;
}