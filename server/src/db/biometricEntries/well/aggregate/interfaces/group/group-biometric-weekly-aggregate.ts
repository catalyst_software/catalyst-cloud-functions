import { WellAggregateCollection } from '../../../../enums/firestore-aggregate-collection';
import { GroupDailyAggregateModel } from '../../group-biometric-daily-aggregate.model';
import { AthleteBiometricEntryInterface } from '../athlete/athelete-biometric-entry';
import { GroupWeeklyHistoryAggregate } from '../../../../models/documents/aggregates';

export interface GroupWeeklyAggregateInterface extends AthleteBiometricEntryInterface {
    entry?: AthleteBiometricEntryInterface;
    guid?: string;
    uid?: string;
    collection?: WellAggregateCollection;
    historyDoc?: GroupWeeklyHistoryAggregate;

    entityId: string;
    entityName: string;

    dailies?: Array<GroupDailyAggregateModel>;

    weekNumber?: number;
}