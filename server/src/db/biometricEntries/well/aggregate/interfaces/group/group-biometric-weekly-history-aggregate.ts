
import { AthleteBiometricEntryInterface } from '../athlete/athelete-biometric-entry';
import { WellAggregateHistoryCollection } from '../../../../enums/firestore-aggregate-history-collection';
import { BiometricAggregateComponentHistoryInterface } from '../biometric-aggregate-component-history';
import { BiometricAggregateTrackingCacheInterface } from '../biometric-aggregate-tracking-cache';

export interface GroupWeeklyHistoryAggregateInterface extends AthleteBiometricEntryInterface {
    
    collection?: WellAggregateHistoryCollection;
    
    entry?: AthleteBiometricEntryInterface;
    uid?: string;
    guid?: string;

    entityId: string;
    entityName: string;

    
    values?: Array<any>;
    historicalValues?: any[]
    painComponentHistory?: BiometricAggregateComponentHistoryInterface[];
    mealComponentHistory?: BiometricAggregateComponentHistoryInterface[];
    trainingComponentHistory?: BiometricAggregateComponentHistoryInterface[];

    trackingCache?: BiometricAggregateTrackingCacheInterface;

    recentEntries?: Array<AthleteBiometricEntryInterface>;
}