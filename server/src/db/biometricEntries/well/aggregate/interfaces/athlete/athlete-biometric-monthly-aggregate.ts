import { firestore } from 'firebase-admin';

import { AthleteBiometricEntryInterface } from './athelete-biometric-entry';
import { WellAggregateCollection } from '../../../../enums/firestore-aggregate-collection';
import { PainComponent } from '../../../interfaces/pain-component';
import { MealComponent } from '../../../interfaces/meal-component';
import { TrainingComponentInterface } from '../../../interfaces/training-component';

export interface AthleteMonthlyAggregateInterface extends AthleteBiometricEntryInterface {
    guid: string;
    collection?: WellAggregateCollection;
    
    userId?: string;
    fullName?: string;

    fromDate?: firestore.Timestamp; 
    toDate?: firestore.Timestamp; 
    
    recentEntries?: []
    painComponents?: Array<PainComponent>;
    mealComponents?: Array<MealComponent>;
    trainingComponents?: Array<TrainingComponentInterface>;
}