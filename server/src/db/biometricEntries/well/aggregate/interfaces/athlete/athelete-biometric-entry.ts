import { firestore } from 'firebase-admin';

import { HealthComponentType } from '../../../enums/health-component-type';
import { LifestyleEntryType } from '../../../../../../models/enums/lifestyle-entry-type';

import { PainComponent } from '../../../interfaces/pain-component';
import { MealComponent } from '../../../interfaces/meal-component';
import { TrainingComponentInterface } from '../../../interfaces/training-component';
// import { AthleteBiometricEntryInterface } from './athelete-biometric-entry';
import { AggregateDocumentType } from '../../../enums/aggregate-document-type';
import { AggregateEntityType } from '../../../../enums/aggregate-entity-type';
import { GroupIndicator } from '../../../interfaces/group-indicator';
import { GeoPointInterface } from '../../../../../../models/geopoint.model';

// import { GroupDirective } from '../../interfaces/group-indicator';
// import { GeoPointInterface } from '../../../geopoint.model';

export interface AthleteBiometricEntryInterface {
    uid?: string;

    creationTimestamp?: firestore.Timestamp; // For backward compatability
    athleteCreationTimestamp?: firestore.Timestamp;

    userId?: string;
    entityId: string;
    entityName: string;

    entry?: AthleteBiometricEntryInterface;

    // TODO:
    collection?: any;

    organizationId?: string;

    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;

    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    value?: any;
    values?: Array<any>;
    historicalValues?: Array<any>;
    points?: number;

    painComponents?: Array<PainComponent>;
    mealComponents?: Array<MealComponent>;
    trainingComponents?: Array<TrainingComponentInterface>;



    // userId?: string;
    firstName?: string;
    lastName?: string;
    fullName?: string;

    dateTime?: firestore.Timestamp;
    // dateTimeOld?: Date;



    includeGroupAggregation?: boolean;


    // historicalValues?: Array<any>;
    // points?: number;

    groups?: Array<GroupIndicator>;
    geoPoint?: GeoPointInterface;


    simpleMealComponents?: Array<MealComponent>;
    // TODO:
    firebaseLogsLevel?: any;

    isToday?: boolean;
    isIncrease?: boolean;
    isDecrease?: boolean;
    classification?: string;
    tag?: string;
    analyticsHistory?: boolean;
    unixTimestamp?: number;
    utcOffset?: number;

    skipAggregate?: boolean;

}

