import { firestore } from 'firebase-admin';

import { AthleteDailyAggregateModel } from '../../athelete-biometric-daily-aggregate.model';
import { AthleteBiometricEntryInterface } from './athelete-biometric-entry';
import { WellAggregateCollection } from '../../../../enums/firestore-aggregate-collection';

export interface AthleteWeeklyAggregateInterface extends AthleteBiometricEntryInterface {
    entry?: AthleteBiometricEntryInterface;
    guid: string;
    collection?: WellAggregateCollection;

    userId?: string;
    fullName?: string;

    dailies: Array<AthleteDailyAggregateModel>;

    weekNumber?: number;
    // fromDate?: firestore.Timestamp;
    // toDate?: firestore.Timestamp;

    
    lastAggregateDate?: firestore.Timestamp,

}