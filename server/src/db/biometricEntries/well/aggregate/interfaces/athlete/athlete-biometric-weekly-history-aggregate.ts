
import { WellAggregateHistoryCollection } from '../../../../enums/firestore-aggregate-history-collection';
import { BiometricHistoryAggregate } from '../biometric-history-aggregate';
import { AthleteBiometricEntryInterface } from './athelete-biometric-entry';
import { BiometricAggregateComponentHistoryInterface } from '../biometric-aggregate-component-history';
import { BiometricAggregateTrackingCacheInterface } from '../biometric-aggregate-tracking-cache';

export interface AthleteWeeklyHistoryAggregateInterface extends BiometricHistoryAggregate {
    collection?: WellAggregateHistoryCollection;
    
    entry?: AthleteBiometricEntryInterface;
    guid?: string;

    entityId: string;
    entityName: string;

    
    values?: Array<any>;
    historicalValues?: any[]
    painComponentHistory?: BiometricAggregateComponentHistoryInterface[];
    mealComponentHistory?: BiometricAggregateComponentHistoryInterface[];
    trainingComponentHistory?: BiometricAggregateComponentHistoryInterface[];

    trackingCache?: BiometricAggregateTrackingCacheInterface;

    recentEntries?: Array<AthleteBiometricEntryInterface>;
}