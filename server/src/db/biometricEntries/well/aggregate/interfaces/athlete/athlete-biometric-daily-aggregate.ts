import { firestore } from 'firebase-admin';

import { AthleteBiometricEntryInterface } from './athelete-biometric-entry';

export interface AthleteDailyAggregate extends AthleteBiometricEntryInterface {
    guid?: string;
    userId?: string;
    fullName?: string;

    dateTime?: firestore.Timestamp; 

}
