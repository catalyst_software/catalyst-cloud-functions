
import { firestore } from "firebase-admin";

import { GroupIndicator } from '../../../interfaces/group-indicator';
import { AthleteBiometricEntryInterface } from './athelete-biometric-entry';
import { WellAggregateHistoryCollection } from "../../../../enums/firestore-aggregate-history-collection";
import { AggregateDocumentType } from "../../../enums/aggregate-document-type";
import { WellAggregateBinaryHistoryCollection } from "../../../../enums/firestore-aggregate-binary-history-collection";
import { LifestyleEntryType } from '../../../../../../models/enums/lifestyle-entry-type';
import { HealthComponentType } from '../../../enums/health-component-type';
import { AggregateEntityType } from "../../../../enums/aggregate-entity-type";

export interface BinaryBiometricHistoryAggregateInterface {
    uid?: string;
    entry?: AthleteBiometricEntryInterface; // TODO: Requered because of interfaces TS
    group?: GroupIndicator,
	entityId: string;
    entityName: string;
    collection?: WellAggregateHistoryCollection | WellAggregateBinaryHistoryCollection;

    lifestyleEntryType?: LifestyleEntryType;

    aggregateDocumentType?: AggregateDocumentType;
    aggregateEntityType?: AggregateEntityType;
    healthComponentType?: HealthComponentType;
	
    utcOffset?: number;
    organizationId?: string;
    values: Array<BinaryBiometric>;
}

export interface BinaryBiometric {
    on: firestore.Timestamp;
    off?: firestore.Timestamp;
    uid?: string;
}