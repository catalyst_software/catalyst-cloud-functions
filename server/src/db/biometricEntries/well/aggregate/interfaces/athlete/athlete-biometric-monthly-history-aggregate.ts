
import { firestore } from 'firebase-admin';

import { AthleteBiometricEntryInterface } from './athelete-biometric-entry';
import { WellAggregateHistoryCollection } from '../../../../enums/firestore-aggregate-history-collection';
import { AggregateDocumentType } from '../../../enums/aggregate-document-type';
import { BiometricAggregateTrackingCacheInterface } from '../biometric-aggregate-tracking-cache';
import { BiometricAggregateComponentHistoryInterface } from '../biometric-aggregate-component-history';
import { LifestyleEntryType } from '../../../../../../models/enums/lifestyle-entry-type';
import { AggregateEntityType } from '../../../../enums/aggregate-entity-type';

export interface AthleteMonthlyHistoryAggregateInterface extends AthleteBiometricEntryInterface {
    entry?: AthleteBiometricEntryInterface;
    collection?: WellAggregateHistoryCollection;
    uid?: string;
    guid?: string;

    entityId: string;
    entityName: string;

    lifestyleEntryType?: LifestyleEntryType;

    // documentId: string;
    // athlete: Athlete;
    athleteCreationTimestamp?: firestore.Timestamp;
    aggregateDocumentType?: AggregateDocumentType;
    aggregateEntityType?: AggregateEntityType;

    values?: Array<any>;
    historicalValues?: Array<any>;
    painComponentHistory?: Array<BiometricAggregateComponentHistoryInterface>;
    mealComponentHistory?: Array<BiometricAggregateComponentHistoryInterface>;
    trainingComponentHistory?: Array<BiometricAggregateComponentHistoryInterface>;

    trackingCache?: BiometricAggregateTrackingCacheInterface;

    recentEntries?: Array<AthleteBiometricEntryInterface>;
}