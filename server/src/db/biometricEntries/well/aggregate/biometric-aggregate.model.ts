import { AthleteBiometricEntryInterface } from './interfaces/athlete/athelete-biometric-entry';
import { MealComponent } from '../interfaces/meal-component';
import { PainComponent } from '../interfaces/pain-component';
import { TrainingComponentInterface} from '../interfaces/training-component';

import { HealthComponentType } from '../enums/health-component-type';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { AggregateDocumentType } from '../enums/aggregate-document-type';
import { AggregateEntityType } from '../../enums/aggregate-entity-type';

export class BiometricAggregateModel implements AthleteBiometricEntryInterface {
    id?: string;
    guid?: string;

    organizationId?: string;
    aggregateDocumentType?: AggregateDocumentType;
    aggregateEntityType?: AggregateEntityType;

    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    value?: any;
    historicalValues?: Array<any>;
    points?: number;

    painComponents?: Array<PainComponent>;
    mealComponents?: Array<MealComponent>;
    trainingComponents?: Array<TrainingComponentInterface>;

    entry?: AthleteBiometricEntryInterface;

    uid?: string;

    entityId: string;
    entityName: string;
}
