import { GroupWeeklyAggregateInterface } from "./interfaces/group/group-biometric-weekly-aggregate";
import { MealComponent } from "../interfaces/meal-component";
import { TrainingComponentInterface } from "../interfaces/training-component";

import { HealthComponentType } from "../enums/health-component-type";
import { LifestyleEntryType } from "../../../../models/enums/lifestyle-entry-type";
import { GroupDailyAggregateModel } from "./group-biometric-daily-aggregate.model";
import { PainComponent } from "../interfaces/pain-component";
import { BiometricAggregateModel } from "./biometric-aggregate.model";
import { WellAggregateCollection } from "../../enums/firestore-aggregate-collection";
import { AthleteBiometricEntryInterface } from "./interfaces/athlete/athelete-biometric-entry";
import { GroupWeeklyHistoryAggregate } from "../../models/documents/aggregates";


export class GroupWeeklyAggregateModel extends BiometricAggregateModel
  implements GroupWeeklyAggregateInterface {
  weekNumber?: number;
  userId: string;
  documentId?: string;
  entry?: AthleteBiometricEntryInterface;
  collection: WellAggregateCollection;
  historyDoc?: GroupWeeklyHistoryAggregate;

  guid: string;

  groupId?: string;
  groupName?: string;

  organizationId?: string;

  healthComponentType?: HealthComponentType;
  lifestyleEntryType?: LifestyleEntryType;

  value?: any;
  historicalValues?: Array<any>;
  points?: number;

  dailies?: Array<GroupDailyAggregateModel>;

  painComponents?: Array<PainComponent>;
  mealComponents?: Array<MealComponent>;
  trainingComponents?: Array<TrainingComponentInterface>;
}
