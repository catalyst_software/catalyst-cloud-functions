import { AthleteWeeklyAggregateInterface } from './interfaces/athlete/athlete-biometric-weekly-aggregate';
import { MealComponent } from '../interfaces/meal-component';
import { PainComponent } from '../interfaces/pain-component';
import { TrainingComponentInterface} from '../interfaces/training-component';

import { HealthComponentType } from '../enums/health-component-type';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { AthleteDailyAggregateModel } from './athelete-biometric-daily-aggregate.model';
import { BiometricAggregateModel } from './biometric-aggregate.model';
import { WellAggregateCollection } from '../../enums/firestore-aggregate-collection';
import { AthleteBiometricEntryInterface } from './interfaces/athlete/athelete-biometric-entry';

import { AggregateDocumentType } from '../enums/aggregate-document-type';
import { AthleteWeeklyHistoryAggregate } from '../../models/documents/aggregates';
import { AggregateEntityType } from '../../enums/aggregate-entity-type';


export class AthleteWeeklyAggregateModel extends BiometricAggregateModel 
implements AthleteWeeklyAggregateInterface {
    aggregateEntityType?: AggregateEntityType;
    aggregateDocumentType?: AggregateDocumentType;
    uid: string;
    weekNumber?: number;
    entry?: AthleteBiometricEntryInterface;
    documentId?: string;
    historyDoc?: AthleteWeeklyHistoryAggregate;
    collection?: WellAggregateCollection;
    userId?: string;
    fullName?: string;
    
    entityId: string;
    entityName: string;
    guid: string;

    organizationId?: string;
    
    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    value?: any;
    historicalValues?: Array<any>;
    points?: number;

    dailies: Array<AthleteDailyAggregateModel>;

    painComponents?: Array<PainComponent>; 
    mealComponents?: Array<MealComponent>;
    trainingComponents?: Array<TrainingComponentInterface>;
}
