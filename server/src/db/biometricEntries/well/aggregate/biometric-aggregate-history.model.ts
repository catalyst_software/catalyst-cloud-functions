// tslint:disable-next-line:max-line-length
import { HealthComponentType } from '../enums/health-component-type';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { AggregateDocumentType } from '../enums/aggregate-document-type';
import { EntityType } from '../enums/entity-type';

import { PainComponent } from '../interfaces/pain-component';
import { MealComponent } from '../interfaces/meal-component';
import { TrainingComponentInterface } from '../interfaces/training-component';
import { WellAggregateHistoryCollection } from '../../enums/firestore-aggregate-history-collection';
import { AthleteBiometricEntryInterface } from './interfaces/athlete/athelete-biometric-entry';
import { BiometricHistoryAggregate } from './interfaces/biometric-history-aggregate';
import { AggregateEntityType } from '../../enums/aggregate-entity-type';

export class BiometricAggregateHistoryModel implements BiometricHistoryAggregate {
    entry?: AthleteBiometricEntryInterface;
    collection: WellAggregateHistoryCollection;
    uid: string;

    guid: string;
    entityId: string;
    entityName: string;
    organizationId?: string;
    aggregateDocumentType?: AggregateDocumentType;
    aggregateEntityType?: AggregateEntityType;;

    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    values?: Array<any>;

    painComponents?: Array<PainComponent>;
    mealComponents?: Array<MealComponent>;
    trainingComponents?: Array<TrainingComponentInterface>;
}