import { firestore } from 'firebase-admin';

import { PainComponent } from '../interfaces/pain-component';
// import { GroupBiometricDailyAggregateInterface } from './interfaces/group-biometric-daily-aggregate';
import { MealComponent } from '../interfaces/meal-component';
import { TrainingComponentInterface } from '../interfaces/training-component';

import { HealthComponentType } from '../enums/health-component-type';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { BiometricAggregateModel } from './biometric-aggregate.model';
import { GroupDailyAggregate } from './interfaces';

export class GroupDailyAggregateModel extends BiometricAggregateModel 
implements GroupDailyAggregate {
    groupId?: string;
    groupName?: string;

    dateTime?: firestore.Timestamp; 

    guid?: string;

    organizationId?: string;
    
    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    value?: any;
    historicalValues?: Array<any>;
    points?: number;

    painComponents?: Array<PainComponent>; 
    mealComponents?: Array<MealComponent>;
    trainingComponents?: Array<TrainingComponentInterface>;

    constructor(
        uid?: string,
        groupId?: string,
        groupName?: string,

        dateTime?: firestore.Timestamp,

        guid?: string,

        organizationId?: string,

        healthComponentType?: HealthComponentType,
        lifestyleEntryType?: LifestyleEntryType,

        value?: number,
        historicalValues?: Array<any>,
        points?: number,

        painComponents?: Array<PainComponent>,
        mealComponents?: Array<MealComponent>,
        trainingComponents?: Array<TrainingComponentInterface>,
    ) {
        super();

  
        this.uid = uid;
        this.groupId = groupId;
        this.groupName = groupName;
        this.dateTime = dateTime;
        this.guid = guid;
        this.organizationId = organizationId;
        this.healthComponentType = healthComponentType;
        this.lifestyleEntryType = lifestyleEntryType;

        this.value = value;
        this.historicalValues = historicalValues;
        this.points = points;
        this.painComponents = painComponents;
        this.mealComponents = mealComponents;
        this.trainingComponents = trainingComponents;
    }

}
