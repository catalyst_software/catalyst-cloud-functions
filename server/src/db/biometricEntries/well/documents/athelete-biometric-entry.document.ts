import { firestore } from "firebase-admin";

import { AthleteBiometricEntryInterface } from "../aggregate/interfaces/athlete/athelete-biometric-entry";
import { LifestyleEntryType } from "../../../../models/enums/lifestyle-entry-type";
import { HealthComponentType } from "../enums/health-component-type";

export const AthleteBiometricEntryDocument: AthleteBiometricEntryInterface = {
    uid: '',

    athleteCreationTimestamp: firestore.Timestamp.now(),

    // userId: '',
    fullName: '',

    entityId: '', 
    entityName: '',

    organizationId: '',

    dateTime: firestore.Timestamp.now(),
    lifestyleEntryType: LifestyleEntryType.Sleep,

    healthComponentType: HealthComponentType.Brain,

    value: 7,
    // historicalValues: [],
    // points: 0,

    // groups: [
    //     { groupId: '1', groupName: 'G1', creationTimestamp: firestore.Timestamp.now() }
    // ],

    geoPoint: null,

    painComponents: [],
    mealComponents: [],
    trainingComponents: [],

    utcOffset: 0
};