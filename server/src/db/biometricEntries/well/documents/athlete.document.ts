import { AthleteProfile } from '../../../../models/athlete/interfaces/athlete-profile';
// import { Athlete } from '../athlete/interfaces/athlete';

export const athleteDocument = { //}: Athlete = {
    // unique key for example acme.inspire.org
    uid: undefined,

    firstName: undefined,
    lastName: undefined,
    userName: undefined,
    mobileNumber: undefined,
    bio: undefined,
    dob: undefined,
    sex: undefined,
    sport: undefined,
    location: undefined,
    secret: undefined,
    metadata: undefined,
    profile: new AthleteProfile(),
    groupIds: Array<string>(),
    organizationId: undefined,
    isCoach: false,
    ipScore: 0,
    currentIpScoreTracking: undefined,
    ipScoreConfiguration: undefined
};