import { Map as ImmutableMap } from 'immutable';
import _ = require('lodash');

import { removeUndefinedProps } from '../../../ipscore/services/save-aggregates';

export abstract class BaseRecord {
    // TODO: private 
    _data: ImmutableMap<any, any> = ImmutableMap<any, any>();

    changes: Array<any>

    data() {
        return this._data;
    }

    constructor(initialValues?: any) {

        if (initialValues) {
            if (initialValues.entry && !initialValues.entry.athleteCreationTimestamp) {
                initialValues.entry.athleteCreationTimestamp = initialValues.entry.creationTimestamp
            }

            this._data = this._data.merge(initialValues);
            _.forEach(initialValues, (value, key) => {
                Object.defineProperty(this, key, {
                    enumerable: false,
                    get() {
                        const data = this._data.get(key);
                        return data;
                    },
                    set() {
                        throw new Error('Cannot set on an immutable record.');
                    }
                });
            });
        } else {
            this._data = ImmutableMap<any, any>();
        }

        this.changes = []

    }
    get entry() {
        return this._data.get('entry');
    }
    toJS(): any {
        const jsonWithoutData = this._data.delete('_data') // .toJSON();
        const json = jsonWithoutData.delete('aggregator').toJSON();
        removeUndefinedProps(json)
        return json;
    }
    equals(otherRecord: BaseRecord) {
        return typeof this === typeof otherRecord &&
            this._data.equals(otherRecord._data);
    }
    with?(values: any): any {

        // logChanges(this.changes, this.toJS(), values)


        // const returnVal = new (this.constructor as any);
        // returnVal._data = this._data.merge(values);
        debugger;
        const returnVal = new (this.constructor as any)({ ...this._data.merge(values).toJS() });

        return returnVal;
    }

}

// export const logChanges = (changes: Array<any>, oldValue, newValue) => {
//     Object.keys(oldValue).forEach((key, index, theArray) => {

//         changes.push(oldValue)
//         const theOldValue = oldValue[key];
//         const theNewValue = oldValue[key];

//         // if (theOldValue !== theNewValue) {
//         //     console.log(key + ' Changed! === ', { theOldValue, theNewValue })
//         // }

//         const isArrayTest = Array.isArray(oldValue[key]) && !oldValue[key].length;
//         // (isArrayTest)
//         if (isObject(oldValue[key])) {
//             logChanges(changes, oldValue[key], newValue[key]);
//         }
//     });
// }

/**
 * Default Values when instanciating a new Athlete Biometric Monthly Aggregate
 */
// export const BiometricAggregateRecord = (defaultValues) => class extends Record
//     ({
//         uid: '',
//         guid: Guid.raw(),
//         organizationId: '',
//         value: 0,
//         historicalValues: [],
//         points: 0,
//         mealComponents: [],
//         painComponents: [],
//         trainingComponents: [],
//         ...defaultValues
//     }) {
//     with(values: Iterable<[string, any]> | Partial<BiometricAggregateInterface>) {
//         return this.merge(values) as this;
//     }
// };
