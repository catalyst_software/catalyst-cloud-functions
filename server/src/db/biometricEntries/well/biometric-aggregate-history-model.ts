import { firestore } from "firebase-admin";

import { AggregateDocumentType } from "./enums/aggregate-document-type";
import { LifestyleEntryType } from "../../../models/enums/lifestyle-entry-type";
import { PainComponent } from "./interfaces/pain-component";
import { MealComponent } from "./interfaces/meal-component";
import { TrainingComponentInterface } from "./interfaces/training-component";
import { BiometricAggregateTrackingCacheInterface } from "./aggregate/interfaces/biometric-aggregate-tracking-cache";
import { HealthComponentType } from "./enums/health-component-type";
import { AggregateEntityType } from "../enums/aggregate-entity-type";

// import { AggregateDocumentType } from "../../db/biometricEntries/well/enums/aggregate-document-type";
// import { AggregateEntityType } from "../../db/biometricEntries/enums/aggregate-entity-type";
// import { HealthComponentType } from "../../db/biometricEntries/well/enums/health-component-type";
// import { LifestyleEntryType } from "../enums/lifestyle-entry-type";
// import { PainComponent } from "../../db/biometricEntries/well/interfaces/pain-component";
// import { MealComponent } from "../../db/biometricEntries/well/interfaces/meal-component";
// import { TrainingComponent } from "../../db/biometricEntries/well/interfaces/training-component";
// import { firestore } from "firebase-admin";
// // import { BiometricAggregateTrackingCacheInterface } from "../../db/biometricEntries/well/aggregate/interfaces/biometric-aggregate-tracking-cache";

export class BiometricAggregateHistoryModel {
    guid: string;

    entityId?: string;
    organizationId?: string;

    aggregateDocumentType?: AggregateDocumentType;
    aggregateEntityType?: AggregateEntityType;

    healthComponentType?: HealthComponentType;
    lifestyleEntryType?: LifestyleEntryType;

    values?: Array<any>;

    painComponentHistory?: Array<Array<PainComponent>>;
    mealComponentHistory?: Array<Array<MealComponent>>;
    trainingComponentHistory?: Array<Array<TrainingComponentInterface>>;

    documentId?: string;

    creationTimestamp?: firestore.Timestamp;

    trackingCache?: BiometricAggregateTrackingCacheInterface;
}
