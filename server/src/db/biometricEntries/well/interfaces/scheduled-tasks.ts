import { SchedulingType } from "../../../../models/enums/scheduling-type";
import { EntityType } from "../enums/entity-type";

export interface ScheduledTasks {
    entityType: EntityType;
    schedulingType: SchedulingType.Hourly | SchedulingType.Quarterly | SchedulingType.Minute;
    entityId: string;
    taskName: string;
    startTime: number;
    endTime: number;
}

