import { MealComponent } from './meal-component';
import { PainComponent } from './pain-component';
import { TrainingComponentInterface } from './training-component';
export interface AggregateComponentResponse {
    component: MealComponent | PainComponent | TrainingComponentInterface;
    componentArray: Array<MealComponent | PainComponent | TrainingComponentInterface>;
    value?: number;
}