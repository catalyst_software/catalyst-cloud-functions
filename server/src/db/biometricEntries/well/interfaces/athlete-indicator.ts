import { firestore } from "firebase-admin";
import { IpScoreAggregateComponentInterface } from "../../../ipscore/models/interfaces/ipscore-component-aggregate";
import { GroupNotificationConfig } from "../../../../api/app/v1/controllers/athletes";
import { SubscriptionType } from "../../../../models/enums/enums.model";
import { OrganizationIndicator } from "../../../../models/athlete/interfaces/athlete";
import { OrganizationTool } from "../../../../models/onboarding-configuration";
import { GroupIndicator } from "./group-indicator";

export interface AthleteIndicator {

  organizationId?: string;
  // organizationName: string;
  organizations?: Array<OrganizationIndicator>;

  uid: string;
  firstName: string;
  lastName: string;
  email: string;

  profileImageURL?: string;

  isCoach?: boolean;
  includeGroupAggregation?: boolean;

  ipScore?: number;
  isIpScoreIncrease?: boolean;
  isIpScoreDecrease?: boolean;
  runningTotalIpScore: number;

  recentEntries?: Array<IpScoreAggregateComponentInterface>;

  subscriptionType: SubscriptionType;

  subType?: string;
  period?: boolean;
  sick?: boolean;

  // // TODO: IG - Needed here?
  firebaseMessagingIds?: Array<string>;
  commsConfig?: GroupNotificationConfig;

  creationTimestamp: firestore.Timestamp;

  excludedNotificationTypes?: number[];
  orgTools: Array<OrganizationTool>;
  bio: string;
  sport?: string;



  groups?: Array<GroupIndicator>;
  groupId?: string;
  groupName?: string;


}
