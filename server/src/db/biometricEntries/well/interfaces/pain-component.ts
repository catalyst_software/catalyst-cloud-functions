import { BodyLocationType } from '../enums/body-location-type';
import { PainType } from '../enums/pain-type';

export interface PainComponent {
    painType: PainType;
    bodyLocation: BodyLocationType;
    painLevel: number;
    historicalPainLevels: Array<number>;
}