export interface MealComponent {
    mealType: string
    foodType?: string
    foodItemType?: string
    servingCount?: number
    calorieCount?: number
    points?: number
    historicalServingsCount?: Array<number>
    respose?: any
    componentArray?: any
    component?: any
    values?: Array<number>
    value?: number
    classificationType?: string
    // Wellness X
    entityId?: string
    dayIndex?: number
}
