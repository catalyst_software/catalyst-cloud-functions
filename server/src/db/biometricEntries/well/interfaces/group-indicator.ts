import { firestore } from "firebase-admin";
import { AthleteIndicator } from "./athlete-indicator";

export interface GroupIndicator {
    organizationId: string;
    organizationName: string;
    uid?: string;
    entityName?: string;

    groupId: string;
    groupName?: string;

    creationTimestamp: firestore.Timestamp;
    logoUrl?: string;
    athletes?: Array<AthleteIndicator>;
}
