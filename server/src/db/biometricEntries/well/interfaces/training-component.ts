import { IntensityLevelType } from '../enums/intensity-level-type';
import { SportType } from '../enums/sport-type';

export interface TrainingComponentInterface {
    sportType: SportType;
    sportName: string;
    icon: string;
    intensityLevel: IntensityLevelType;
    // Total Time In Minutes
    minutes: number;
    color: string;
    persisted: boolean;
    historicalMinutes: Array<number>;
    // TODO: Check implementation
    numberOfSessions?: number;
    historicalSessions?: Array<number>;

}
