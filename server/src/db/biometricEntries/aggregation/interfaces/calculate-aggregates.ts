import { AthleteWeeklyAggregate, GroupWeeklyAggregate } from '../../../../models/documents/well';

import { AthleteBiometricEntryInterface } from '../../well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { AggregateDocumentDirective } from '../../services/interfaces/aggregate-document-directive';
import { AthleteWeeklyHistoryAggregate, GroupWeeklyHistoryAggregate } from '../../models/documents/aggregates';
import { BinaryBiometricHistoryAggregate } from '../../models/documents/aggregates/binary-history-aggregate.document';

export interface CalculateAggregates {
    // Athlete Binary
    // processAthleteBinaryHistory?(entry: AthleteBiometricEntry, aggregateDoc: BinaryBiometricHistoryAggregate, directive: AggregateDocumentDirective
    // ): AthleteWeeklyAggregate
    // Athlete Weekly
    processAthleteWeekly?(entry: AthleteBiometricEntryInterface, aggregateDoc: AthleteWeeklyAggregate, directive: AggregateDocumentDirective
    ): AthleteWeeklyAggregate
    processAthleteWeeklyHistory?(entry: AthleteBiometricEntryInterface, aggregateDoc: AthleteWeeklyHistoryAggregate, directive: AggregateDocumentDirective
    ): AthleteWeeklyHistoryAggregate


    // Group Binary
    // processGroupBinaryHistory?(entry: AthleteBiometricEntry, aggregateDoc: BinaryBiometricHistoryAggregate, directive: AggregateDocumentDirective
    // ): AthleteWeeklyAggregate
    // Group Weekly
    processGroupWeekly?(entry: AthleteBiometricEntryInterface, aggregateDoc: GroupWeeklyAggregate, directive: AggregateDocumentDirective
    ): GroupWeeklyAggregate
    processGroupWeeklyHistory?(entry: AthleteBiometricEntryInterface, aggregateDoc: GroupWeeklyHistoryAggregate, directive: AggregateDocumentDirective
    ): GroupWeeklyHistoryAggregate

    // Binaries
    processAthleteBinaryHistory?(entry: AthleteBiometricEntryInterface, aggregateDoc: BinaryBiometricHistoryAggregate): BinaryBiometricHistoryAggregate;
    processGroupBinaryHistory?(entry: AthleteBiometricEntryInterface, aggregateDoc: BinaryBiometricHistoryAggregate): BinaryBiometricHistoryAggregate;
}