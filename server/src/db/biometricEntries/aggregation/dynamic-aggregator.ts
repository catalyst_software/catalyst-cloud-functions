import { LogLevel } from './../enums/log-level'
import { AggregateLogLevel } from '../enums/aggregate-log-level'
import { CalculateIpScoreAggregates } from '../../ipscore/aggregation/interfaces/calculate-ipscore-aggregates'
import { AggregatorStore } from './aggregators'
import { AggregateIpScoreDirective } from '../../ipscore/services/interfaces/aggregate-ipscore-directive'
import {
    AthleteIpScoreWeeklyAggregateInterface,
    GroupIpScoreWeeklyAggregateInterface,
} from '../../ipscore/models/interfaces'
import { AthleteBiometricEntryInterface } from '../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import {
    AthleteWeeklyAggregate,
    GroupWeeklyAggregate,
} from '../../../models/documents/well'
import { AggregateDocumentDirective } from '../services/interfaces/aggregate-document-directive'
import {
    AthleteWeeklyHistoryAggregate,
    GroupWeeklyHistoryAggregate,
} from '../models/documents/aggregates'
import { BinaryBiometricHistoryAggregate } from '../models/documents/aggregates/binary-history-aggregate.document'
import { GroupMonthlyHistoryAggregate } from '../models/documents/aggregates/group/group-monthly-history-aggregate.document'
import { GroupIpScoreWeeklyHistoryAggregateInterface } from '../../ipscore/models/interfaces/group-ipscore-weekly-history-aggregate'
import { GroupIpScoreMonthlyHistoryAggregateInterface } from '../../ipscore/models/interfaces/group-ipscore-monthly-history-aggregate'

export class DynamicAggregator implements CalculateIpScoreAggregates {
    processGroupIpScoreWeekly(
        directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyAggregateInterface {
        throw new Error('Method not implemented.')
    }
    processGroupIpScoreWeeklyHistory(
        directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyHistoryAggregateInterface {
        throw new Error('Method not implemented.')
    }
    processGroupIpScoreMonthlyHistory(
        directive: AggregateIpScoreDirective
    ): GroupIpScoreMonthlyHistoryAggregateInterface {
        throw new Error('Method not implemented.')
    }

    constructor(className) {
        if (
            AggregatorStore[className] === undefined ||
            AggregatorStore[className] === null
        ) {
            console.error({
                type: AggregateLogLevel.Info,
                className,
                message: `Aggregator for ${className} could not be found in the Dynamic Aggregator store `,
                level: LogLevel.Sockets,
            })

            return undefined
        }

        const theAggregator = new AggregatorStore[className]()

        return theAggregator
    }
    processAthleteIpScoreWeekly(
        directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyAggregateInterface {
        throw new Error('Method not implemented.')
    }

    processAthleteIpScoreWeeklyHistory(
        directive: AggregateIpScoreDirective
    ): any {
        throw new Error('Method not implemented.')
    }

    processAthleteIpScoreMonthlyHistory(
        directive: AggregateIpScoreDirective
    ): any {
        throw new Error('Method not implemented.')
    }

    // ADDED TO REFACTOR

    /// WELL

    // ATHLETE
    processAthleteWeekly?(
        entry: AthleteBiometricEntryInterface,
        aggregateDoc: AthleteWeeklyAggregate,
        directive: AggregateDocumentDirective
    ): AthleteWeeklyAggregate
    processAthleteWeeklyHistory?(
        entry: AthleteBiometricEntryInterface,
        aggregateDoc: AthleteWeeklyHistoryAggregate,
        directive: AggregateDocumentDirective
    ): AthleteWeeklyHistoryAggregate
    processAthleteMonthlyHistory?(
        entry: AthleteBiometricEntryInterface,
        aggregateDoc: AthleteWeeklyHistoryAggregate,
        directive: AggregateDocumentDirective
    ): AthleteWeeklyHistoryAggregate

    // GROUP
    processGroupWeekly?(
        entry: AthleteBiometricEntryInterface,
        aggregateDoc: GroupWeeklyAggregate,
        directive: AggregateDocumentDirective
    ): GroupWeeklyAggregate

    processGroupMonthlyHistory?(
        entry: AthleteBiometricEntryInterface,
        aggregateDoc: GroupMonthlyHistoryAggregate,
        directive: AggregateDocumentDirective
    ): GroupMonthlyHistoryAggregate

    processGroupWeeklyHistory?(
        entry: AthleteBiometricEntryInterface,
        aggregateDoc: GroupWeeklyHistoryAggregate,
        directive: AggregateDocumentDirective
    ): GroupWeeklyHistoryAggregate

    // Binaries
    processAthleteBinaryHistory?(
        entry: AthleteBiometricEntryInterface,
        aggregateDoc: BinaryBiometricHistoryAggregate,
        directive: AggregateDocumentDirective
    ): BinaryBiometricHistoryAggregate
    processGroupBinaryHistory?(
        entry: AthleteBiometricEntryInterface,
        aggregateDoc: BinaryBiometricHistoryAggregate,
        directive: AggregateDocumentDirective
    ): BinaryBiometricHistoryAggregate

    calculateAggregates(): AggregateIpScoreDirective {
        console.error('Implemented on Aggregator.')
        throw new Error('Implemented on Aggregator.')
    }
}
