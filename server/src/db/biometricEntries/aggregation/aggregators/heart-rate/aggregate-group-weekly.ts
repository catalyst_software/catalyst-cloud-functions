import { cloneDeep, mean, filter } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";


import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber, logDayIndex } from "./utils";
import { GroupDailyAggregate } from "../../../well/aggregate/interfaces";
import { getIsoWeekDay, getWeekFromDate } from "../../../../../shared/utils/moment";
import { validateFieldsInitialized } from "../utils";
import { createDailies } from "../../../services/utils";
import { GroupIndicator } from "../../../well/interfaces/group-indicator";
import { GroupWeeklyAggregate } from "../../../models/documents/aggregates";

export const aggregateGroupWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyAggregate,
    group: GroupIndicator
): GroupWeeklyAggregate => {

    LogInfo(`Processing GroupWeekly aggregate document in Heart Rate aggregator.`, {
        AggregateDocument: aggregateDoc,
        BiometricEntity: entry
    });

    let docUpdates: any = aggregateDoc;

    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);
    logDayIndex(entry, dayIndex);
    if (!docUpdates.dailies) {
        const dailies = createDailies(entry, aggregateDoc.collection, getWeekFromDate(entry.dateTime, entry.utcOffset), 
        group);

        docUpdates = docUpdates.with({dailies: dailies})
    }
    const dailyAggregate: GroupDailyAggregate = docUpdates.dailies[dayIndex];
    docUpdates = validateFieldsInitialized(docUpdates, dailyAggregate);

    if (isNumber(entry.value)) {

        LogInfo(`Group Weekly Daily aggregate before update:`, dailyAggregate);

        const val = +entry.value;
        if (dailyAggregate) {
            const historicalValues = cloneDeep(dailyAggregate.historicalValues);
            historicalValues.push(val);

            dailyAggregate.historicalValues = historicalValues;
            dailyAggregate.value = mean(dailyAggregate.historicalValues);
        }

        const dailyValues = filter(docUpdates.dailies, (d) => {
            return d.value > 0;
        }).map((d) => {
            return d.value;
        });
        if (dailyValues && dailyValues.length) {
            docUpdates = docUpdates.with({
                value: mean(dailyValues)
            });
        }
        LogInfo(`Group weekly aggregate after update:`, dailyAggregate)

    } else {
        LogError(`Invalid value in Group Weekly Heart Rate aggregator. 
        Value must be a numerical value: `, { value: entry.value })
    }
    LogInfo(`Group weekly aggregate after update:`, dailyAggregate);

    return docUpdates;

};
