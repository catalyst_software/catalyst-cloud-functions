import { isNumber } from "./utils"
import { mean } from "lodash"

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry"
import { LogError, LogInfo } from "../../../../../shared/logger/logger"
import { getWeekNumber } from "../../../../../shared/utils/moment"
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive"
import { AthleteWeeklyHistoryAggregate } from "../../../models/documents/aggregates"
import { handleScalarHistoryUpdates } from "../utils/handle-scalar-history-updates"

export const aggregateAthleteWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): AthleteWeeklyHistoryAggregate => {

    LogInfo({
        message: `Processing AthleteWeekly History aggregate document in Heart Rate aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    const weekNumber = getWeekNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset)
    LogInfo({
        message: `Calculated week number for AthleteWeekly History ${weekNumber}.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    let updates = aggregateDoc
    if (isNumber(entry.value)) {
        updates = handleScalarHistoryUpdates(entry, aggregateDoc, weekNumber) as AthleteWeeklyHistoryAggregate
        updates.values[0] = mean(updates.trackingCache.valueCache)
    } else {
        LogError(`Invalid value in Athlete Weekly Heart Rate aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return updates
}

