import { cloneDeep } from 'lodash'

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'

import { LogError, LogInfo } from '../../../../../shared/logger/logger'
import { isNumber } from '../utils'
import { manageHistoricalMealComponent } from './manageHistoricalMealComponent'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive'
import { getMonthNumber } from '../../../../../shared/utils/moment'
import { GroupMonthlyHistoryAggregate } from '../../../models/documents/aggregates/group/group-monthly-history-aggregate.document'
import { MealComponent } from '../../../well/interfaces/meal-component'

export const aggregateGroupMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupMonthlyHistoryAggregate => {
    LogInfo({
        message: `Processing GroupMonthly History aggregate document in SimpleFood aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry,
        },
    })

    let updates = aggregateDoc

    const monthNumber = getMonthNumber(
        entry.athleteCreationTimestamp,
        entry.dateTime,
        entry.utcOffset
    )
    LogInfo({
        message: `Calculated month number for GroupMonthly History ${monthNumber}.`,
        data: {
            AggregateDocument: updates,
            BiometricEntity: entry,
        },
    })

    if (!updates.trackingCache) {
        updates.trackingCache = {
            index: 0,
            valueCache: [],
            componentCache: {
                index: 0,
                value: 0,
                components: [],
            },
        }
    }

    if (updates.trackingCache.index !== monthNumber) {
        updates.trackingCache.index = monthNumber
        updates.trackingCache.componentCache = {
            index: monthNumber,
            value: 0,
            components: [],
        }
    }

    if (!updates.mealComponentHistory) {
        updates = updates.with({ mealComponentHistory: [] })
    }

    if (updates.mealComponentHistory.length <= monthNumber) {
        const existing = cloneDeep(updates.mealComponentHistory)
        for (
            let i = updates.mealComponentHistory.length;
            i <= monthNumber;
            i++
        ) {
            existing.unshift({ index: i, value: 0, components: [] })
        }
        updates = updates.with({ mealComponentHistory: existing })
    }

    entry.simpleMealComponents.forEach((c: MealComponent) => {
        if (isNumber(c.calorieCount)) {
            let response = manageHistoricalMealComponent(
                entry,
                c,
                updates.trackingCache.componentCache.components,
                true,
                directive.group
            )
            updates.trackingCache.componentCache.components =
                response.componentArray
            updates.trackingCache.componentCache.value = response.value
            let cacheComponent = response.component

            LogInfo(`Processed component cache in SimpleFood aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.trackingCache.componentCache,
                BiometricEntity: entry,
            })

            response = manageHistoricalMealComponent(
                entry,
                c,
                updates.mealComponentHistory[0].components,
                true,
                directive.group
            )
            updates.mealComponentHistory[0].components = response.componentArray
            updates.mealComponentHistory[0].value = response.value
            cacheComponent = response.component

            LogInfo(
                `Processed meal component history in SimpleFood aggregator.`,
                {
                    CacheComponent: cacheComponent,
                    ComponentCacheArray: updates.mealComponentHistory[0],
                    BiometricEntity: entry,
                }
            )
        } else {
            LogError(
                `Invalid value in Monthly SimpleFood aggregator. 
            calorieCount must be a numerical value:`,
                { value: c.calorieCount }
            )
        }
    })

    return updates
}
