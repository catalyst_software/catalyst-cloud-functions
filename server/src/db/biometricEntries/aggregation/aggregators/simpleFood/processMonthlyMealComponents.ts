import { isArray, cloneDeep, find, mean } from 'lodash'

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'

import { LogError, LogInfo } from '../../../../../shared/logger/logger'
import { isNumber } from '../utils'
import { MealComponent } from '../../../well/interfaces/meal-component'

export const processMonthlyMealComponents = (
    entry: AthleteBiometricEntryInterface,
    monthlyDocs: any
) => {
    debugger
    let docUpdates = monthlyDocs
    if (
        isArray(entry.simpleMealComponents) &&
        entry.simpleMealComponents.length > 0
    ) {
        if (!isArray(docUpdates.historicalValues)) {
            docUpdates = docUpdates.with({ historicalValues: [] })
        }
        if (!isArray(docUpdates.mealComponents)) {
            docUpdates = docUpdates.with({ mealComponents: [] })
        }

        entry.simpleMealComponents.forEach((m: MealComponent) => {
            if (isNumber(m.calorieCount)) {
                m.calorieCount = +m.calorieCount
                // Track the meal component itself
                const historicalValues = cloneDeep(docUpdates.historicalValues)
                historicalValues.push(m)
                docUpdates = docUpdates.with({
                    historicalValues: historicalValues,
                })

                // Find the Monthly Meal Component object by type and aggregagte below
                let monthlyAggregateComponent: MealComponent = undefined
                monthlyAggregateComponent = find(
                    docUpdates.mealComponents,
                    mc => {
                        return (
                            m.foodType === mc.foodType &&
                            m.foodItemType === mc.foodItemType
                        )
                    }
                )

                if (!monthlyAggregateComponent) {
                    LogInfo(
                        `Aggregate monthly simple food component for food type ${m.foodType} not found.  Creating new!`
                    )
                    monthlyAggregateComponent = {
                        foodType: m.foodType,
                        foodItemType: m.foodItemType,
                        value: 0,
                        values: [],
                    } as MealComponent
                    const mealComponents = cloneDeep(docUpdates.mealComponents)
                    mealComponents.push(monthlyAggregateComponent)

                    docUpdates = docUpdates.with({ mealComponents })
                } else {
                    LogInfo(
                        `Aggregate monthly simple food component for meal type ${m.mealType} exists.`
                    )
                }
                if (!isArray(monthlyAggregateComponent.values)) {
                    monthlyAggregateComponent.values = []
                }
                const counts = cloneDeep(monthlyAggregateComponent.values)
                counts.push(+m.calorieCount)
                monthlyAggregateComponent.values = counts
                monthlyAggregateComponent.value = mean(
                    monthlyAggregateComponent.values
                )
            } else {
                LogError(
                    `Invalid value in Athlete Weekly SimpleFood aggregator. 
                Points must be a numerical value:`,
                    { value: m.calorieCount }
                )
            }
        })
    } else {
        LogError(`Invalid values in Athlete Monthly SimpleFood aggregator. 
        Meal components array is empty or null.`)
    }

    return docUpdates
}
