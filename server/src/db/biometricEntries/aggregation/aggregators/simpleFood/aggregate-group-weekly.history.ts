import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'

import { LogError, LogInfo } from '../../../../../shared/logger/logger'
import { isNumber } from '../utils'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive'
import { manageHistoricalMealComponent } from './manageHistoricalMealComponent'
import { cloneDeep } from 'lodash'
import { getWeekNumber } from '../../../../../shared/utils/moment'
import { GroupWeeklyHistoryAggregate } from '../../../models/documents/aggregates'
import { MealComponent } from '../../../well/interfaces/meal-component'

export const aggregateGroupWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyHistoryAggregate => {
    LogInfo({
        message: `Processing GroupWeekly History aggregate document in SimpleFood aggregator.`,
        data: {
            AggregateDocument: aggregateDoc.toJS(),
            BiometricEntity: entry,
        },
    })
    let updates = aggregateDoc
    const weekNumber = getWeekNumber(
        entry.athleteCreationTimestamp,
        entry.dateTime,
        entry.utcOffset
    )
    LogInfo({
        message: `Calculated week number for GroupWeekly History ${weekNumber}.`,
        data: {
            AggregateDocument: updates,
            BiometricEntity: entry,
        },
    })

    if (!updates.trackingCache) {
        updates.trackingCache = {
            index: 0,
            valueCache: [],
            componentCache: {
                index: 0,
                value: 0,
                components: [],
            },
        }
    }

    if (updates.trackingCache.index !== weekNumber) {
        updates.trackingCache.index = weekNumber
        updates.trackingCache.componentCache = {
            index: 0,
            value: 0,
            components: [],
        }
    }

    if (!updates.mealComponentHistory) {
        updates = updates.with({ mealComponentHistory: [] })
    }

    if (updates.mealComponentHistory.length <= weekNumber) {
        const existing = cloneDeep(updates.mealComponentHistory)
        for (
            let i = updates.mealComponentHistory.length;
            i <= weekNumber;
            i++
        ) {
            existing.unshift({ index: i, value: 0, components: [] })
        }
        updates = updates.with({ mealComponentHistory: existing })
    }

    entry.simpleMealComponents.forEach((c: MealComponent) => {
        if (isNumber(c.calorieCount)) {
            let response = manageHistoricalMealComponent(
                entry,
                c,
                updates.trackingCache.componentCache.components,
                true,
                directive.group
            )
            updates.trackingCache.componentCache.components =
                response.componentArray
            updates.trackingCache.componentCache.value = response.value
            let cacheComponent = response.component as MealComponent

            LogInfo(`Processed component cache in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.trackingCache.componentCache,
                BiometricEntity: entry,
            })

            response = manageHistoricalMealComponent(
                entry,
                c,
                updates.mealComponentHistory[0].components,
                true,
                directive.group
            )
            updates.mealComponentHistory[0].components = response.componentArray
            updates.mealComponentHistory[0].value = response.value
            cacheComponent = response.component as MealComponent

            LogInfo(`Processed meal component history in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.mealComponentHistory[0],
                BiometricEntity: entry,
            })
        } else {
            LogError(
                `Invalid value in Weekly SimpleFood aggregator. 
            CalorieCount must be a numerical value:`,
                { value: c.calorieCount }
            )
        }
    })

    return updates
}
