import { cloneDeep, find } from 'lodash'

import { MealComponent } from '../../../well/interfaces/meal-component'
import { PainComponent } from '../../../well/interfaces/pain-component'
import { TrainingComponentInterface } from '../../../well/interfaces/training-component'
import { AggregateComponentResponse } from '../../../well/interfaces/aggregate-component-response'

import { Group } from '../../../../../models/group/group.model'
import { getGroupAggregateForSimpleFood } from '../utils'
import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { getIsoWeekDay } from '../../../../../shared/utils/moment'

export const manageHistoricalMealComponent = (
    entry: AthleteBiometricEntryInterface,
    entryMealComponent: MealComponent,
    componentArray: Array<
        MealComponent | PainComponent | TrainingComponentInterface
    >,
    isGroup: boolean,
    group?: Group
): AggregateComponentResponse => {
    const newArray = cloneDeep(componentArray)

    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset)
    let component: MealComponent | PainComponent | TrainingComponentInterface =
        find(newArray, (c: MealComponent) => {
            if (isGroup) {
                return (
                    c.foodType === entryMealComponent.foodType &&
                    c.foodItemType === entryMealComponent.foodItemType &&
                    c.dayIndex === dayIndex &&
                    c.entityId === entry.userId
                )
            } else {
                return (
                    c.foodType === entryMealComponent.foodType &&
                    c.foodItemType === entryMealComponent.foodItemType &&
                    c.dayIndex === dayIndex
                )
            }
        })

    if (!component) {
        component = {
            foodType: entryMealComponent.foodType,
            foodItemType: entryMealComponent.foodItemType,
            calorieCount: 0,
            servingCount: 0,
            value: 0,
            points: 0,
            entityId: entry.userId,
            dayIndex,
        } as MealComponent
        newArray.push(component)
    }

    ;(component as MealComponent).calorieCount +=
        entryMealComponent.calorieCount
    ;(component as MealComponent).servingCount +=
        entryMealComponent.servingCount

    // const existing = cloneDeep((component as MealComponent).values);
    // existing.push(+entryMealComponent.points);
    // (component as MealComponent).values = existing;

    let value = 0
    if (isGroup) {
        value = getGroupAggregateForSimpleFood(
            group,
            newArray as MealComponent[],
            false
        )
    } else {
        value = newArray
            .map((c: MealComponent) => c.calorieCount || 0)
            .reduce((a, b) => a + b, 0)
    }

    return {
        component,
        componentArray: newArray,
        value,
    }
}
