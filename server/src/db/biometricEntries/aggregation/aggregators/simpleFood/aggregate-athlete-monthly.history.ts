import { cloneDeep } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { AthleteMonthlyHistoryAggregate } from "../../../models/documents/aggregates/athlete/athelete-monthly-history-aggregate.document";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";

import { manageHistoricalMealComponent } from "./manageHistoricalMealComponent";

import { getMonthNumber } from "../../../../../shared/utils/moment";
import { MealComponent } from '../../../well/interfaces/meal-component';

export const aggregateAthleteMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteMonthlyHistoryAggregate,
): AthleteMonthlyHistoryAggregate => {

    LogInfo({
        message: `Processing Athlete Monthly History aggregate document in SimpleFood aggregator.`,
        data: { historyDocument: aggregateDoc, biometricEntryId: entry.uid }
    });

    const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);

    LogInfo({
        message: `Calculated month number for Athlete Monthly History ${monthNumber}.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    });
    
    let updates = aggregateDoc as any;

    if (!updates.trackingCache) {
        updates = updates.trackingCache = {
            index: 0,
            valueCache: [],
            componentCache: {
                index: 0,
                components: []
            }
        };
    }

    if (updates.trackingCache.index !== monthNumber) {
        updates.trackingCache.index = monthNumber;
        updates.trackingCache.componentCache = {
            index: monthNumber,
            components: []
        }
    }

    if (!updates.mealComponentHistory) {
        updates = updates.with({ mealComponentHistory: [] });
    }

    if (updates.mealComponentHistory.length <= monthNumber) {
        const existing = cloneDeep(updates.mealComponentHistory);
        for (let i = updates.mealComponentHistory.length; i <= monthNumber; i++) {
            existing.unshift({index: i, components: []});
        }

        updates = updates.with({mealComponentHistory: existing});
    }

    entry.simpleMealComponents.forEach((c: MealComponent) => {
        if (isNumber(c.points) && c.mealType.toLowerCase() !== 'snack') {
            let response = manageHistoricalMealComponent(entry, c, updates.trackingCache.componentCache.components, false);
            updates.trackingCache.componentCache.components = response.componentArray;
            updates.trackingCache.value = response.value;
            let cacheComponent = response.component as MealComponent;

            LogInfo(`Processed component cache in SimpleFood aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.trackingCache.componentCache,
                BiometricEntity: entry
            });

            response = manageHistoricalMealComponent(entry, c, updates.mealComponentHistory[0].components, false);
            updates.mealComponentHistory[0].components = response.componentArray;
            updates.mealComponentHistory[0].value = response.value;
            cacheComponent = response.component as MealComponent;

            LogInfo(`Processed meal component history in SimpleFood aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.mealComponentHistory[0],
                BiometricEntity: entry
            });

        } else {
            LogError(`Invalid value in Monthly History SimpleFood aggregator. 
            Points must be a numerical value:`, { value: c.points });
        }
    });

    return updates;
};


