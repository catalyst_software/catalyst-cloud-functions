import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'

import { LogInfo } from '../../../../../shared/logger/logger'
import { GroupDailyAggregate } from '../../../well/aggregate/interfaces'
import { processWeeklyMealComponents } from './processWeeklyMealComponents'
import { createDailies } from '../../../services/utils'
import {
    getIsoWeekDay,
    getWeekFromDate,
} from '../../../../../shared/utils/moment'
import { GroupWeeklyAggregate } from '../../../models/documents/aggregates'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive';

export const aggregateGroupWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyAggregate,
    groupDirective: AggregateDocumentDirective
): GroupWeeklyAggregate => {
    LogInfo({
        message: `Processing Group Weekly aggregate document in SimpleFood aggregator.`,
        data: {
            historyDocument: { uid: aggregateDoc.uid },
            biometricEntryId: entry.uid,
        },
    });

    let docUpdates = aggregateDoc;
    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);
    LogInfo({
        message: `Entry date: ${entry.dateTime.toDate()}.  Day index: ${dayIndex}.`,
        data: {
            dayIndex,
            document: aggregateDoc,
            biometricEntryId: entry.uid,
        },
    });
    if (!docUpdates.dailies) {
        const group = aggregateDoc.entry.groups[0];
        const dailies = createDailies(
            entry,
            aggregateDoc.collection,
            getWeekFromDate(entry.dateTime, entry.utcOffset),
            {
                ...group,
                uid: group.uid || group.groupId
            }
        );

        docUpdates = docUpdates.with({ dailies: dailies })
    }

    const dailyAggregate: GroupDailyAggregate = aggregateDoc.dailies[dayIndex];
    
    return processWeeklyMealComponents(
        entry,
        aggregateDoc,
        dailyAggregate,
        true,
        groupDirective.group
    ) as GroupWeeklyAggregate
};
