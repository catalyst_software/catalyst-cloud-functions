import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogInfo } from "../../../../../shared/logger/logger";
import { logDayIndex } from "../utils";
import { processWeeklyMealComponents } from "./processWeeklyMealComponents";
import { AthleteDailyAggregate } from '../../../well/aggregate/interfaces';
import { createDailies } from "../../../services/utils";
import { getIsoWeekDay, getWeekFromDate } from "../../../../../shared/utils/moment";
import { AthleteWeeklyAggregate } from "../../../models/documents/aggregates";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";

export const aggregateAthleteWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyAggregate,
    directive: AggregateDocumentDirective
): AthleteWeeklyAggregate => {

    LogInfo({
        message: `Processing Athlete Weekly aggregate document in SimpleFood aggregator.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    });

    let docUpdates = aggregateDoc;
    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);
    logDayIndex(entry, dayIndex);
    if (!docUpdates.dailies) {
        const dailies = createDailies(entry, aggregateDoc.collection, getWeekFromDate(entry.dateTime, entry.utcOffset), 
       null);

        docUpdates = docUpdates.with({dailies: dailies})
    }
    const dailyAggregate: AthleteDailyAggregate = aggregateDoc.dailies[dayIndex];

    const theUpdatedDoc =  processWeeklyMealComponents(entry, aggregateDoc, dailyAggregate, false, undefined) as AthleteWeeklyAggregate;

    return theUpdatedDoc
};
