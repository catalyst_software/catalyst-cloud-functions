import { isArray, cloneDeep, find } from 'lodash'

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces'
import { getGroupAggregateForSimpleFood, isNumber } from '../utils'
import { LogInfo, LogError } from '../../../../../shared/logger/logger'
import { MealComponent } from '../../../well/interfaces/meal-component'
import {
    AthleteWeeklyAggregate,
    GroupWeeklyAggregate,
} from '../../../models/documents/aggregates'
import { Group } from '../../../../../models/group/group.model'
import { getIsoWeekDay } from '../../../../../shared/utils/moment'
import { AthleteDailyAggregateModel } from '../../../well/aggregate/athelete-biometric-daily-aggregate.model'

export const processWeeklyMealComponents = (
    entry: AthleteBiometricEntryInterface,
    weeklyDoc: AthleteWeeklyAggregate | GroupWeeklyAggregate,
    dailyDoc: AthleteBiometricEntryInterface,
    isGroup: boolean,
    group: Group
): AthleteBiometricEntryInterface => {
    let docUpdates: AthleteWeeklyAggregate | GroupWeeklyAggregate = weeklyDoc

    if (
        isArray(entry.simpleMealComponents) &&
        entry.simpleMealComponents.length > 0
    ) {
        if (!isArray(weeklyDoc.historicalValues)) {
            docUpdates = docUpdates.with({ historicalValues: [] })
        }
        if (!isArray(weeklyDoc.mealComponents)) {
            docUpdates = docUpdates.with({ mealComponents: [] }) // Check if needed
        }

        if (!isArray(dailyDoc.historicalValues)) {
            dailyDoc.historicalValues = [] // Check if needed
        }
        if (!isArray(dailyDoc.mealComponents)) {
            dailyDoc.mealComponents = []
        }

        const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset)
        entry.simpleMealComponents.forEach((m: MealComponent) => {
            if (isNumber(m.calorieCount)) {
                m.calorieCount = +m.calorieCount
                // Track the meal component itself
                const historicalValues = cloneDeep(docUpdates.historicalValues)
                historicalValues.push(m)
                docUpdates = docUpdates.with({
                    historicalValues: historicalValues,
                })

                // Find the Daily Meal Component object by type and aggregagte below
                let dailyAggregateComponent = find(
                    dailyDoc.mealComponents,
                    mc => {
                        if (isGroup) {
                            return (
                                m.foodType === mc.foodType &&
                                m.foodItemType === mc.foodItemType &&
                                mc.entityId === entry.userId
                            )
                        } else {
                            return (
                                m.foodType === mc.foodType &&
                                m.foodItemType === mc.foodItemType
                            )
                        }
                    }
                )

                if (!dailyAggregateComponent) {
                    LogInfo(
                        `Aggregate daily food component for meal type ${m.mealType} not found.  Creating new!`
                    )
                    dailyAggregateComponent = {
                        foodType: m.foodType,
                        foodItemType: m.foodItemType,
                        servingCount: 0,
                        calorieCount: 0,
                        value: 0,
                        points: 0,
                        entityId: entry.userId,
                        dayIndex,
                    } as MealComponent

                    const mealComponents = cloneDeep(dailyDoc.mealComponents)
                    mealComponents.push(dailyAggregateComponent)

                    dailyDoc.mealComponents = mealComponents
                } else {
                    LogInfo(
                        `Aggregate daily food component for meal type ${m.mealType} exists.`
                    )
                }

                dailyAggregateComponent.calorieCount += m.calorieCount
                dailyAggregateComponent.servingCount += m.servingCount

                if (isGroup) {
                    dailyDoc.value =
                        getGroupAggregateForSimpleFood(
                            group,
                            dailyDoc.mealComponents,
                            false
                        ) || 0
                    LogInfo(
                        `Calculated daily simpleFood value for group ${group.groupId}: ${dailyDoc.value}`
                    )
                } else {
                    // Loop through all mealComponents on the daily (max 3)
                    // and calculate average for the day
                    dailyDoc.value = dailyDoc.mealComponents
                        .map(c => c.calorieCount || 0)
                        .reduce((a, b) => a + b, 0)
                    LogInfo(
                        `Calculated daily simpleFood value for athlete ${entry.userId}: ${dailyDoc.value}`
                    )
                }

                // Loop through all dailies for the week (up to and including the current day index)
                // and calculate average for the week based on the day value property

                docUpdates.dailies[dayIndex] = dailyDoc
                const days = docUpdates.dailies.slice(
                    0,
                    dayIndex + 1
                ) as AthleteDailyAggregateModel[]
                const weeklyValue = days
                    .map(d => d.value || 0)
                    .reduce((a, b) => a + b, 0)

                LogInfo(
                    `Calculated weekly simpleFood value for athlete ${entry.userId}: ${weeklyValue}`
                )
                docUpdates = docUpdates.with({ value: weeklyValue })
            } else {
                LogError(
                    `Invalid value in Athlete Weekly SimpleFood aggregator. 
                    MealType is snack or points must be a numerical value:`,
                    { value: m.points }
                )
            }
        })
    } else {
        LogError(`Invalid values in Athlete Weekly SimpleFood aggregator. 
            Meal components array is empty or null.`)
    }

    return docUpdates
}
