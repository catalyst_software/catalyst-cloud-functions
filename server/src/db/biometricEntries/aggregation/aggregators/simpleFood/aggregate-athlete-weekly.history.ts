import { cloneDeep } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";

import { manageHistoricalMealComponent } from "./manageHistoricalMealComponent";
import { getWeekNumber } from "../../../../../shared/utils/moment";
import { AthleteWeeklyHistoryAggregate } from "../../../models/documents/aggregates";
import { MealComponent } from '../../../well/interfaces/meal-component';

export const aggregateAthleteWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyHistoryAggregate,
): AthleteWeeklyHistoryAggregate => {

    LogInfo({
        message: `Processing Athlete Weekly History aggregate document in SimpleFood aggregator.`,
        data: { historyDocument: aggregateDoc, biometricEntryId: entry.uid }
    });

    let update = aggregateDoc as any;
    if (!update.trackingCache) {
        update = update.trackingCache = {
            index: 0,
            valueCache: [],
            componentCache: {
                index: 0,
                value: 0,
                components: []
            }
        };
    }

    const weekNumber = getWeekNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);
    
    if (update.trackingCache.index !== weekNumber) {
        update.trackingCache.index = weekNumber;
        update.trackingCache.componentCache = {
            index: weekNumber,
            value: 0,
            components: []
        }
    }

    if (!update.mealComponentHistory) {
        update = update.with({ mealComponentHistory: [] });
    }

    if (update.mealComponentHistory.length <= weekNumber) {
        const existing = cloneDeep(update.mealComponentHistory);
        for (let i = update.mealComponentHistory.length; i <= weekNumber; i++) {
            existing.unshift({index: i, value: 0, components: []});
        }

        update = update.with({mealComponentHistory:  existing});
    }

    entry.simpleMealComponents.forEach((c: MealComponent) => {
        if (isNumber(c.points) && c.mealType.toLowerCase() !== 'snack') {
            let response = manageHistoricalMealComponent(entry, c, update.trackingCache.componentCache.components, false);
            update.trackingCache.componentCache.components = response.componentArray;
            update.trackingCache.value = response.value;
            let cacheComponent = response.component as MealComponent;

            LogInfo(`Processed component cache in SimpleFood aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: update.trackingCache.componentCache,
                BiometricEntity: entry
            });

            response = manageHistoricalMealComponent(entry, c, update.mealComponentHistory[0].components, false);
            update.mealComponentHistory[0].components = response.componentArray;
            update.mealComponentHistory[0].value = response.value;
            cacheComponent = response.component as MealComponent;

            LogInfo(`Processed meal component history in SimpleFood aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: update.mealComponentHistory[0],
                BiometricEntity: entry
            });

        } else {
            LogError(`Invalid value in Weekly SimpleFood aggregator. 
            Value must be a numerical value:`, { value: c.value });
        }
    });

    return update;
};
