import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { LogInfo, LogError } from "../../../../../shared/logger/logger";
import { MonthlyHistoryAggregateType } from "../../../../../models/types/biometric-aggregate-types";
import { isArray, cloneDeep } from "lodash";
import { getMonthNumber } from "../../../../../shared/utils/moment";
import { PainComponent } from "../../../well/interfaces/pain-component";
import { isNumber } from "util";
import { manageHistoricalPainComponent } from "./manageHistoricalPainComponent";

export const processMonthlyHistoryPainComponents = (entry: AthleteBiometricEntryInterface, aggregateDoc: MonthlyHistoryAggregateType): MonthlyHistoryAggregateType => {
    let updates = aggregateDoc as any;
    if (isArray(entry.painComponents) && entry.painComponents.length > 0) {
       
   
        if (!isArray(updates.painComponentHistory)) {
            updates.painComponentHistory = [];
        }
        const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);
        LogInfo({
            message: `Calculated month number for AthleteWeekly History ${monthNumber}.`,
            data: {
                AggregateDocument: updates,
                BiometricEntity: entry
            }
        });
        
        if (!updates.trackingCache) {
            updates.trackingCache = {
                index: 0,
                valueCache: [],
                componentCache: {
                    index: 0,
                    components: []
                }
            };
        }

        if (updates.trackingCache.index !== monthNumber) {
            updates.trackingCache.index = monthNumber;
            updates.trackingCache.componentCache = {
                index: monthNumber,
                components: []
            }
        }

        if (!updates.painComponentHistory) {
            updates = updates.with({ painComponentHistory: [] });
        }
        
        if (updates.painComponentHistory.length <= monthNumber) {
            const existing = cloneDeep(updates.painComponentHistory);
            for (let i = updates.painComponentHistory.length; i <= monthNumber; i++) {
                existing.unshift({ index: i, components: [] });
            }

            updates = updates.with({painComponentHistory: existing});
        }
        entry.painComponents.forEach((c) => {
            if (isNumber(c.painLevel)) {
                let response = manageHistoricalPainComponent(c, updates.trackingCache.componentCache.components);
                updates.trackingCache.componentCache.components = response.componentArray;

                let cacheComponent = response.component as PainComponent;

                LogInfo(`Processed component cache in Pain aggregator.`, {
                    CacheComponent: cacheComponent,
                    ComponentCacheArray: updates.trackingCache.componentCache,
                    BiometricEntity: entry
                });
                
                response = manageHistoricalPainComponent(c, updates.painComponentHistory[0].components);
                updates.painComponentHistory[0].components = response.componentArray;
                cacheComponent = response.component as PainComponent;

                LogInfo(`Processed pain component history in Pain aggregator.`, {
                    CacheComponent: cacheComponent,
                    ComponentCacheArray: updates.painComponentHistory[0],
                    BiometricEntity: entry
                });
            }
            else {
                LogError(`Invalid value in Monthly Pain aggregator. 
                    Pain level must be a numerical value:`, { painLevel: c.painLevel });
            }
        });
    }
    else {
        LogInfo(`Invalid values in Monthly Pain aggregator. 
            Pain components array is empty or null.`);
    }
    return updates;
};