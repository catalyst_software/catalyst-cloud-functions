import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { AthleteWeeklyAggregate } from "../../../models/documents/aggregates";

import { LogInfo } from "../../../../../shared/logger/logger";
import { processWeeklyPainComponents } from "./processWeeklyPainComponents";

import { logDayIndex } from "../utils";
import { getIsoWeekDay } from "../../../../../shared/utils/moment";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";

export const aggregateAthleteWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyAggregate,
    directive: AggregateDocumentDirective
): AthleteWeeklyAggregate => {

    LogInfo({
        message: `Processing AthleteMonthly aggregate document in Pain aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    });

    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);
    logDayIndex(entry, dayIndex);
    const dailyAggregate = aggregateDoc.dailies[dayIndex];

    return processWeeklyPainComponents(entry, aggregateDoc, dailyAggregate, dayIndex) as AthleteWeeklyAggregate;
};
