import { isArray, cloneDeep, mean, find } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";
import { PainComponent } from "../../../well/interfaces/pain-component";
import { WeeklyAggregateType } from "../../../../../models/types/biometric-aggregate-types";
import { AthleteWeeklyAggregate, GroupWeeklyAggregate } from "../../../models/documents/aggregates";

export const processWeeklyPainComponents = (entry: AthleteBiometricEntryInterface,
    weeklyDoc: WeeklyAggregateType, dailyDoc: AthleteBiometricEntryInterface, dayIndex: number
): WeeklyAggregateType => {
    let docUpdates: AthleteWeeklyAggregate | GroupWeeklyAggregate = weeklyDoc;

    if (isArray(entry.painComponents) && entry.painComponents.length > 0) {

        if (!isArray(docUpdates.historicalValues)) {
            docUpdates = docUpdates.with({ historicalValues: [] });
        }
        if (!isArray(docUpdates.painComponents)) {
            docUpdates = docUpdates.with({ painComponents: [] });
        }

        if (!isArray(dailyDoc.historicalValues)) {
            dailyDoc.historicalValues = [];
        }
        if (!isArray(dailyDoc.painComponents)) {
            dailyDoc.painComponents = [];
        }

        entry.painComponents.forEach((c) => {
            if (isNumber(c.painLevel)) {
                // Track the pain component itself
                const historicalValues = cloneDeep(docUpdates.historicalValues);
                historicalValues.push(c);
                docUpdates = docUpdates.with({ historicalValues: historicalValues });

                // Find the Daily Pain Component object by type and aggregagte below
                let dailyAggregateComponent = find(dailyDoc.painComponents, (pc) => {
                    return c.painType === pc.painType && c.bodyLocation === pc.bodyLocation;
                });

                if (!dailyAggregateComponent) {
                    LogInfo(`Aggregate daily training component for pain type ${c.painType} and body location type ${c.bodyLocation} not found.  Creating new!`);
                    dailyAggregateComponent = {
                        painType: c.painType,
                        bodyLocation: c.bodyLocation,
                        historicalPainLevels: []
                    } as PainComponent;

                    const painComponents = cloneDeep(dailyDoc.painComponents);
                    painComponents.push(dailyAggregateComponent);

                    dailyDoc.painComponents = painComponents;
                } else {
                    LogInfo(`Aggregate daily training component for pain type ${c.painType} and body location type ${c.bodyLocation} exists.`);
                }

                if (!isArray(dailyAggregateComponent.historicalPainLevels)) {
                    dailyAggregateComponent.historicalPainLevels = [];
                }

                let counts = cloneDeep(dailyAggregateComponent.historicalPainLevels);
                counts.push(c.painLevel);
                dailyAggregateComponent.historicalPainLevels = counts;
                dailyAggregateComponent.painLevel = mean(dailyAggregateComponent.historicalPainLevels);

                let weeklyAggregateComponent = find(docUpdates.painComponents, (pc) => {
                    return c.painType === pc.painType && c.bodyLocation === pc.bodyLocation;
                });

                if (!weeklyAggregateComponent) {
                    LogInfo(`Aggregate weekly pain component for pain type ${c.painType} and body location ${c.bodyLocation} not found.  Creating new!`);
                    weeklyAggregateComponent = {
                        painType: c.painType,
                        bodyLocation: c.bodyLocation,
                        historicalPainLevels: []
                    } as PainComponent;

                    const weeklyPainComponents = cloneDeep(docUpdates.painComponents);
                    weeklyPainComponents.push(weeklyAggregateComponent);
                    docUpdates = docUpdates.with({ painComponents: weeklyPainComponents });
                } else {
                    LogInfo(`Aggregate weekly pain component for food type ${c.painType} and body location ${c.bodyLocation} exists.`);
                }

                counts = cloneDeep(weeklyAggregateComponent.historicalPainLevels);
                counts.push(c.painLevel);
                weeklyAggregateComponent.historicalPainLevels = counts;
                weeklyAggregateComponent.painLevel = mean(weeklyAggregateComponent.historicalPainLevels);

            } else {
                LogError(`Invalid value in Athlete Weekly Pain aggregator. 
                Pain level must be a numerical value:`, { painLevel: c.painLevel })
            }
        });

        docUpdates.dailies[dayIndex] = dailyDoc
    } else {
        LogInfo(`Invalid values in Athlete Weekly Pain aggregator. 
        Pain components array is empty or null.`);
    }

    return docUpdates;
};
