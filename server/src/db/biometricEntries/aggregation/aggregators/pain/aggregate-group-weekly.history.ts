import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { GroupWeeklyHistoryAggregate } from "../../../models/documents/aggregates";

import { LogInfo } from "../../../../../shared/logger/logger";
import { processWeeklyHistoryPainComponents } from "./processWeeklyHistoryPainComponents";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";

export const aggregateGroupWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyHistoryAggregate => {

    LogInfo({
        message: 'Processing GroupWeekly history aggregate document in Pain aggregator',
        data: {
            historyDocument: aggregateDoc,
            // BiometricEntity: entry
        }
    });

    return processWeeklyHistoryPainComponents(entry, aggregateDoc);
};
