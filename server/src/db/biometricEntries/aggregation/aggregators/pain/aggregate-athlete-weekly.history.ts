import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogInfo } from "../../../../../shared/logger/logger";
import { processWeeklyHistoryPainComponents } from "./processWeeklyHistoryPainComponents";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";
import { AthleteWeeklyHistoryAggregate } from "../../../models/documents/aggregates";

export const aggregateAthleteWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): AthleteWeeklyHistoryAggregate => {

    LogInfo({
        message: 'Processing AthleteWeekly history aggregate document in Pain aggregator',
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    });

    return processWeeklyHistoryPainComponents(entry, aggregateDoc);
};
