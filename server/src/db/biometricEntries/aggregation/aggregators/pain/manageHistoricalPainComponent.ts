import { cloneDeep, find, mean } from "lodash";
import { PainComponent } from "../../../well/interfaces/pain-component";
import { AggregateComponentResponse } from "../../../well/interfaces/aggregate-component-response";
import { TrainingComponentInterface } from "../../../well/interfaces/training-component";
import { MealComponent } from "../../../well/interfaces/meal-component";

export const manageHistoricalPainComponent = (entryPainComponent: PainComponent, componentArray: Array<MealComponent | PainComponent | TrainingComponentInterface>): AggregateComponentResponse => {

    const newArray = cloneDeep(componentArray);

    let component: MealComponent | PainComponent | TrainingComponentInterface = find(newArray, (c: PainComponent) => {
        return c.painType === entryPainComponent.painType && c.bodyLocation === entryPainComponent.bodyLocation;
    });

    if (component === undefined) {
        component = {
            painType: entryPainComponent.painType,
            bodyLocation: entryPainComponent.bodyLocation,
            historicalPainLevels: []
        } as PainComponent;
        newArray.push(component);
    }
    
    const existing = cloneDeep((component as PainComponent).historicalPainLevels);
    existing.push(entryPainComponent.painLevel);
    (component as PainComponent).historicalPainLevels = existing;

    (component as PainComponent).painLevel = mean((component as PainComponent).historicalPainLevels);
    
    return {
        component,
        componentArray: newArray
    };
};