import { AggregateDocumentDirective } from './../../../services/interfaces/aggregate-document-directive';
import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";


import { GroupDailyAggregate } from "../../../well/aggregate/interfaces";
import { processWeeklyPainComponents } from "./processWeeklyPainComponents";
import { LogInfo } from "../../../../../shared/logger/logger";
import { getIsoWeekDay } from '../../../../../shared/utils/moment';
import { GroupWeeklyAggregate } from "../../../models/documents/aggregates";


export const aggregateGroupWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyAggregate => {

    LogInfo({
        message: `Processing Group Weekly aggregate document in Pain aggregator.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    });

    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);
    LogInfo({ message: `Entry date: ${entry.dateTime.toDate()}.  Day index: ${dayIndex}.` });

    const dailyAggregate: GroupDailyAggregate = aggregateDoc.dailies[dayIndex];
    return processWeeklyPainComponents(entry, aggregateDoc, dailyAggregate, dayIndex) as GroupWeeklyAggregate;


};

