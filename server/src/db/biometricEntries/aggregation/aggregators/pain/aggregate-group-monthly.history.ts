import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { GroupMonthlyHistoryAggregate } from "../../../models/documents/aggregates/group/group-monthly-history-aggregate.document";

import { LogInfo } from "../../../../../shared/logger/logger";
import { processMonthlyHistoryPainComponents } from "./processMonthlyHistoryPainComponents";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";

export const aggregateGroupMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupMonthlyHistoryAggregate => {

    LogInfo({
        message: 'Processing Group Monthly History aggregate document in Pain aggregator',
        data: {
            historyDocument: aggregateDoc,
            // BiometricEntity: entry
        }
    });
    debugger;
    return processMonthlyHistoryPainComponents(entry, aggregateDoc);
};
