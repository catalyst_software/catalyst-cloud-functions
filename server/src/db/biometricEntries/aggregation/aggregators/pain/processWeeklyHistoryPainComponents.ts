import { isArray, cloneDeep } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";
import { getWeekNumber } from "../../../../../shared/utils/moment";
import { PainComponent } from "../../../well/interfaces/pain-component";
import { manageHistoricalPainComponent } from "./manageHistoricalPainComponent";
import { WeeklyHistoryAggregateType } from "../../../../../models/types/biometric-aggregate-types";
import { AthleteWeeklyHistoryAggregate } from "../../../models/documents/aggregates";

export const processWeeklyHistoryPainComponents = (entry: AthleteBiometricEntryInterface, aggregateDoc: WeeklyHistoryAggregateType): AthleteWeeklyHistoryAggregate => {
    let docUpdates = aggregateDoc;
    if (isArray(entry.painComponents) && entry.painComponents.length > 0) {
        if (!isArray(docUpdates.painComponentHistory)) {
            docUpdates.painComponentHistory = [];
        }
        if (!docUpdates.trackingCache) {
            docUpdates.trackingCache = {
                index: 0,
                valueCache: [],
                componentCache: {
                    index: 0,
                    components: []
                }
            };
        }

        const weekNumber = getWeekNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);
        LogInfo({
            message: `Calculated week number for AthleteWeekly History ${weekNumber}.`,
            data: {
                AggregateDocument: docUpdates,
                BiometricEntity: entry
            }
        });
        
        if (docUpdates.trackingCache.index !== weekNumber) {
            docUpdates.trackingCache.index = weekNumber;
            docUpdates.trackingCache.componentCache = {
                index: weekNumber,
                components: []
            }
        }

        if (!docUpdates.painComponentHistory) {
            docUpdates = docUpdates.with({ painComponentHistory: [] });
        }

        if (docUpdates.painComponentHistory.length <= weekNumber) {
            const existing = cloneDeep(docUpdates.painComponentHistory);
            for (let i = docUpdates.painComponentHistory.length; i <= weekNumber; i++) {
                existing.unshift({ index: i, components: [] });
            }

            docUpdates = docUpdates.with({painComponentHistory: existing});
        }

        entry.painComponents.forEach((c) => {
            if (isNumber(c.painLevel)) {
                let response = manageHistoricalPainComponent(c, docUpdates.trackingCache.componentCache.components);
                docUpdates.trackingCache.componentCache.components = response.componentArray;

                let cacheComponent = response.component as PainComponent;

                LogInfo(`Processed component cache in Pain aggregator.`, {
                    CacheComponent: cacheComponent,
                    ComponentCacheArray: docUpdates.trackingCache.componentCache,
                    BiometricEntity: entry
                });

                response = manageHistoricalPainComponent(c, docUpdates.painComponentHistory[0].components);
                docUpdates.painComponentHistory[0].components = response.componentArray;
                cacheComponent = response.component as PainComponent;

                LogInfo(`Processed pain component history in Pain aggregator.`, {
                    CacheComponent: cacheComponent,
                    ComponentCacheArray: docUpdates.painComponentHistory[0],
                    BiometricEntity: entry
                });
            }
            else {
                LogError(`Invalid value in Weekly Pain aggregator. 
                        Pain level must be a numerical value:`, { painLevel: c.painLevel });
            }
        });
    }
    else {
        LogInfo(`Invalid values in Weekly Pain aggregator. 
                Pain components array is empty or null.`);
    }
    
    return docUpdates as AthleteWeeklyHistoryAggregate;
};
