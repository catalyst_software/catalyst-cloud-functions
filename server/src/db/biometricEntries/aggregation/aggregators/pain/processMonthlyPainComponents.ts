import { isArray, cloneDeep, find, mean , isNumber} from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogInfo, LogError } from "../../../../../shared/logger/logger";
import { PainComponent } from "../../../well/interfaces/pain-component";

export const processMonthlyPainComponents = (entry: AthleteBiometricEntryInterface, monthlyDoc: any) => {
    debugger;
    let docUpdates = monthlyDoc;
    debugger;
    if (isArray(entry.painComponents) && entry.painComponents.length > 0) {
        if (!isArray(docUpdates.historicalValues)) {
            docUpdates = docUpdates.with({ historicalValues: [] });
        }
        if (!isArray(docUpdates.painComponents)) {
            docUpdates = docUpdates.with({ painComponents: [] });
        }
        entry.painComponents.forEach((c) => {
            if (isNumber(c.painLevel)) {
                // Track the meal component itself
                const historicalValues = cloneDeep(docUpdates.historicalValues);
                historicalValues.push(c);
                docUpdates = docUpdates.with({ historicalValues: historicalValues });

                // Find the Daily Training Component object by type and aggregagte below
                let aggregateComponent = find(docUpdates.painComponents, (pc) => {
                    return c.painType === pc.painType && c.bodyLocation === pc.bodyLocation;
                });
                if (!aggregateComponent) {
                    LogInfo(`Aggregate monthly training component for pain type ${c.painType} and body location type ${c.bodyLocation} not found.  Creating new!`);
                    aggregateComponent = {
                        painType: c.painType,
                        bodyLocation: c.bodyLocation,
                        historicalPainLevels: []
                    } as PainComponent;
                    const painComponents = cloneDeep(docUpdates.painComponents);
                    painComponents.push(aggregateComponent);

                    docUpdates = docUpdates.with({ painComponents });
                }
                else {
                    LogInfo(`Aggregate monthly training component for pain type ${c.painType} and body location type ${c.bodyLocation} exists.`);
                }

                if (!isArray(aggregateComponent.historicalPainLevels)) {
                    aggregateComponent.historicalPainLevels = [];
                }
                const counts = cloneDeep(aggregateComponent.historicalPainLevels);
                counts.push(c.painLevel);
                aggregateComponent.historicalPainLevels = counts;
                aggregateComponent.painLevel = mean(aggregateComponent.historicalPainLevels);
            }
            else {
                LogError(`Invalid value in Athlete Monthly Pain aggregator. 
                    Pain level must be a numerical value:`, { painLevel: c.painLevel });
            }
        });
    }
    else {
        LogInfo(`Invalid values in Athlete Monthly Pain aggregator. 
            Pain components array is empty or null.`);
    }
    return docUpdates;
};
