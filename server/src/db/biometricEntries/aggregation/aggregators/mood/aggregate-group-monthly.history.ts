import { isNumber } from "./utils"
import { mean } from "lodash"

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry"
import { GroupMonthlyHistoryAggregate } from "../../../models/documents/aggregates/group/group-monthly-history-aggregate.document"
import { LogError, LogInfo } from "../../../../../shared/logger/logger"
import { getMonthNumber } from "../../../../../shared/utils/moment"
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive"
import { handleScalarHistoryUpdates } from "../utils/handle-scalar-history-updates"

export const aggregateGroupMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupMonthlyHistoryAggregate => {

    LogInfo({
        message: `Processing GroupMonthly History aggregate document in Mood aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    let updates = aggregateDoc
    const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset)
    LogInfo({
        message: `Calculated month number for GroupMonthly History ${monthNumber}.`,
        data: {
            AggregateDocument: updates,
            BiometricEntity: entry
        }
    })

    if (isNumber(entry.value)) {
        debugger;
        updates = handleScalarHistoryUpdates(entry, aggregateDoc, monthNumber)
        updates.values[0] = mean(updates.trackingCache.valueCache)
    } else {
        LogError(`Invalid value in Group Monthly Mood aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return updates
}

