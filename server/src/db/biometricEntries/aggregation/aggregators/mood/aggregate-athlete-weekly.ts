import moment from "moment";
import { isArray, cloneDeep, mean, filter } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { AthleteWeeklyAggregate } from "../../../models/documents/aggregates";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber, logDayIndex } from "./utils";
import { GroupDailyAggregate } from "../../../well/aggregate/interfaces";


export const aggregateAthleteWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyAggregate
): AthleteWeeklyAggregate => {

    LogInfo(`Processing AthleteWeekly aggregate document in Mood aggregator.`, {
        AggregateDocument: aggregateDoc,
        BiometricEntity: entry
    });

    const dayIndex = moment(entry.dateTime.toDate()).isoWeekday() - 1;
    logDayIndex(entry, dayIndex);

    const dailyAggregate: GroupDailyAggregate = aggregateDoc.dailies[dayIndex];
    if (!isArray(dailyAggregate.historicalValues)) {
        dailyAggregate.historicalValues = [];
    }

    if (isNumber(entry.value)) {

        LogInfo(`Athlete Weekly Daily aggregate before update:`, dailyAggregate);

        const val = +entry.value;

        if (dailyAggregate) {
            const historicalValues = cloneDeep(dailyAggregate.historicalValues);
            historicalValues.push(val);

            dailyAggregate.historicalValues = historicalValues;
            dailyAggregate.value = mean(dailyAggregate.historicalValues);
        }

        LogInfo(`Athlete weekly Daily aggregate after update:`, dailyAggregate)

        const dailyValues = filter(aggregateDoc.dailies, (d) => {
            return d.value > 0;
        }).map((d) => {
            return d.value;
        });
        if (dailyValues && dailyValues.length) {

            const updatedDoc = aggregateDoc.with({
                value: mean(dailyValues)
            });

            LogInfo(`Athlete weekly aggregate after update:`, updatedDoc.toJS())
            return updatedDoc
        }


    } else {
        LogError(`Invalid value in Mood aggregator.  Value must be a numerica value: ${entry.value}.`)
    }
    
    LogInfo(`Athlete weekly aggregate NOT updated:`, aggregateDoc.toJS())

    return aggregateDoc;
};

