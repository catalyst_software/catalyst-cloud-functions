import { isNumber } from "./utils"
import { mean } from "lodash"

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry"
import { GroupWeeklyHistoryAggregate } from "../../../models/documents/aggregates"
import { LogError, LogInfo } from "../../../../../shared/logger/logger"
import { getWeekNumber } from "../../../../../shared/utils/moment"
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive"
import { handleScalarHistoryUpdates } from "../utils/handle-scalar-history-updates"

export const aggregateGroupWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyHistoryAggregate => {
    debugger;
    LogInfo({
        message: `Processing GroupWeekly History aggregate document in Sleep aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    const weekNumber = getWeekNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset)
    LogInfo({
        message: `Calculated week number for GroupWeekly History ${weekNumber}.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    let updates = aggregateDoc
    if (isNumber(entry.value)) {
        debugger;
        updates = handleScalarHistoryUpdates(entry, aggregateDoc, weekNumber)
        updates.values[0] = mean(updates.trackingCache.valueCache)
    } else {
        LogError(`Invalid value in Group Weekly History Mood aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return updates
}

