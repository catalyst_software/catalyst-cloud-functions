import { IpScoreAggregator as IpScore } from './ip-score-aggregator'
import { PainAggregator as Pain } from './pain'
import { SleepAggregator as Sleep } from './sleep'
import { FatigueAggregator as Fatigue } from './fatigue'
import { MoodAggregator as Mood } from './mood'
import { FoodAggregator as Food } from './food'
import { TrainingAggregator as Training } from './training'
import { StepsAggregator as Steps } from './steps'
import { HeartRateAggregator as HeartRate } from './heart-rate'
import { DistanceAggregator as Distance } from './distance'
import { PeriodAggregator as Period } from './period'
import { SicknessAggregator as Sick } from './sickness'
import { SimpleFoodAggregator as SimpleFood } from './simpleFood'
import { WeightAggregator as Weight } from './weight/weight-aggregator'
import { BodyMassIndexAggregator as BodyMassIndex } from './bmi/bmi-aggregator'

export const AggregatorStore: any = {
    Fatigue,
    IpScore,
    Mood,
    Pain,
    Sleep,
    Training,
    Steps,
    HeartRate,
    Distance,
    Period,
    Sick,
    SimpleFood,
    Food,
    Weight,
    BodyMassIndex,
}
