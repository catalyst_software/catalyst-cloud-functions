import { cloneDeep } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { AthleteMonthlyHistoryAggregate } from "../../../models/documents/aggregates/athlete/athelete-monthly-history-aggregate.document";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";

import { MealComponent } from "../../../well/interfaces/meal-component";
import { manageHistoricalMealComponent } from "./manageHistoricalMealComponent";

import { getMonthNumber } from "../../../../../shared/utils/moment";

export const aggregateAthleteMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteMonthlyHistoryAggregate,
): AthleteMonthlyHistoryAggregate => {

    LogInfo({
        message: `Processing Athlete Monthly History aggregate document in Food  aggregator.`,
        data: { historyDocument: aggregateDoc, biometricEntryId: entry.uid }
    });

    const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);

    LogInfo({
        message: `Calculated month number for Athlete Monthly History ${monthNumber}.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    });

    let updates = aggregateDoc as any;

    if (!updates.trackingCache) {
        updates = updates.trackingCache = {
            index: 0,
            valueCache: [],
            componentCache: {
                index: 0,
                components: []
            }
        };
    }

    if (updates.trackingCache.index !== monthNumber) {
        updates.trackingCache.index = monthNumber;
        updates.trackingCache.componentCache = {
            index: monthNumber,
            components: []
        }
    }

    if (!updates.mealComponentHistory) {
        updates = updates.with({ mealComponentHistory: [] });
    }

    if (updates.mealComponentHistory.length <= monthNumber) {
        const existing = cloneDeep(updates.mealComponentHistory);
        for (let i = updates.mealComponentHistory.length; i <= monthNumber; i++) {
            existing.unshift({ index: i, components: [] });
        }

        updates = updates.with({ mealComponentHistory: existing });
    }

    entry.mealComponents.forEach((c) => {
        if (isNumber(c.servingCount)) {

            let response = manageHistoricalMealComponent(c, updates.trackingCache.componentCache.components, false);
            updates.trackingCache.componentCache.components = response.componentArray;
            let cacheComponent = response.component as MealComponent;

            LogInfo(`Processed component cache in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.trackingCache.componentCache,
                BiometricEntity: entry
            });

            response = manageHistoricalMealComponent(c, updates.mealComponentHistory[0].components, false);
            updates.mealComponentHistory[0].components = response.componentArray;
            cacheComponent = response.component as MealComponent;

            LogInfo(`Processed meal component history in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.mealComponentHistory[0],
                BiometricEntity: entry
            });

            // TODO: Remove tracking Cache?
            // // let response = manageHistoricalMealComponent(c, updates.trackingCache.componentCache.components, false);
            // // updates.trackingCache.componentCache.components = response.componentArray;
            // // let cacheComponent = response.component as MealComponent;

            // // LogInfo(`Processed component cache in Meal aggregator.`, {
            // //     CacheComponent: cacheComponent,
            // //     ComponentCacheArray: updates.trackingCache.componentCache,
            // //     BiometricEntity: entry
            // // });

            // const response = manageHistoricalMealComponent(c, updates.mealComponentHistory[0].components, false);
            // updates.mealComponentHistory[0].components = response.componentArray;
            // const cacheComponent = response.component as MealComponent;

            // LogInfo(`Processed meal component history in Meal aggregator.`, {
            //     CacheComponent: cacheComponent,
            //     ComponentCacheArray: updates.mealComponentHistory[0],
            //     BiometricEntity: entry
            // });

        } else {
            LogError(`Invalid value in Weekly Meal aggregator. 
            Serving Count must be a numerical value:`, { servingCount: c.servingCount });
        }
    });

    return updates;
};


