import { cloneDeep, sum, find } from "lodash";

import { MealComponent } from "../../../well/interfaces/meal-component";
import { PainComponent } from '../../../well/interfaces/pain-component';
import { TrainingComponentInterface } from '../../../well/interfaces/training-component';
import { AggregateComponentResponse } from '../../../well/interfaces/aggregate-component-response';


import { Group } from "../../../../../models/group/group.model";
import { getGroupAggregate } from "../utils";

export const manageHistoricalMealComponent = (entryMealComponent: MealComponent, componentArray: Array<MealComponent | PainComponent | TrainingComponentInterface>, 
    isGroup: boolean, group?: Group): AggregateComponentResponse => {

    const newArray = cloneDeep(componentArray);

    let component: MealComponent | PainComponent | TrainingComponentInterface = find(newArray, (c: MealComponent) => {
        return c.foodType === entryMealComponent.foodType && c.mealType === entryMealComponent.mealType;
    });

    if (!component) {
        component = {
            foodType: entryMealComponent.foodType,
            mealType: entryMealComponent.mealType,
            historicalServingsCount: []
        } as MealComponent;
        newArray.push(component);
    }

    const existing = cloneDeep((component as MealComponent).historicalServingsCount);
    existing.push(+entryMealComponent.servingCount);
    (component as MealComponent).historicalServingsCount = existing;

    if (isGroup) {
        (component as MealComponent).servingCount = getGroupAggregate(group, (component as MealComponent).historicalServingsCount, true);
    } else {
        (component as MealComponent).servingCount = sum((component as MealComponent).historicalServingsCount);
    }

    return {
        component,
        componentArray: newArray
    };
};
