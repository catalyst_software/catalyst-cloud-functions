
import { isArray, cloneDeep, find, sum } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces";
import { getGroupAggregate, isNumber } from "../utils";
import { LogInfo, LogError } from "../../../../../shared/logger/logger";
import { MealComponent } from "../../../well/interfaces/meal-component";
import { AthleteWeeklyAggregate, GroupWeeklyAggregate } from "../../../models/documents/aggregates";
import { Group } from "../../../../../models/group/group.model";

export const processWeeklyMealComponents = (entry: AthleteBiometricEntryInterface, weeklyDoc: AthleteWeeklyAggregate | GroupWeeklyAggregate,
    dailyDoc: AthleteBiometricEntryInterface, isGroup: boolean, group: Group): AthleteBiometricEntryInterface => {
    let docUpdates: AthleteWeeklyAggregate | GroupWeeklyAggregate = weeklyDoc;
    if (isArray(entry.mealComponents) && entry.mealComponents.length > 0) {

        if (!isArray(weeklyDoc.historicalValues)) {
            docUpdates = docUpdates.with({ historicalValues: [] });
        }
        if (!isArray(weeklyDoc.mealComponents)) {
            docUpdates = docUpdates.with({ mealComponents: [] });
        }

        if (!isArray(dailyDoc.historicalValues)) {
            dailyDoc.historicalValues = [];
        }
        if (!isArray(dailyDoc.mealComponents)) {
            dailyDoc.mealComponents = [];
        }

        entry.mealComponents.forEach((m) => {
            if (isNumber(m.servingCount)) {
                m.servingCount = +m.servingCount;
                // Track the meal component itself
                const historicalValues = cloneDeep(docUpdates.historicalValues);
                historicalValues.push(m);
                docUpdates = docUpdates.with({ historicalValues: historicalValues });

                // Find the Daily Meal Component object by type and aggregagte below
                let dailyAggregateComponent = find(dailyDoc.mealComponents, (mc) => {
                    return m.foodType === mc.foodType && m.mealType === mc.mealType;
                });

                if (!dailyAggregateComponent) {
                    LogInfo(`Aggregate daily food component for food type ${m.foodType} and meal type ${m.mealType} not found.  Creating new!`);
                    dailyAggregateComponent = {
                        foodType: m.foodType,
                        mealType: m.mealType,
                        historicalServingsCount: []
                    } as MealComponent;

                    const mealComponents = cloneDeep(dailyDoc.mealComponents);
                    mealComponents.push(dailyAggregateComponent);

                    dailyDoc.mealComponents = mealComponents;
                } else {
                    LogInfo(`Aggregate daily food component for food type ${m.foodType} and meal type ${m.mealType} exists.`);
                }

                if (!isArray(dailyAggregateComponent.historicalServingsCount)) {
                    dailyAggregateComponent.historicalServingsCount = [];
                }

                let counts = cloneDeep(dailyAggregateComponent.historicalServingsCount);
                counts.push(+m.servingCount);
                dailyAggregateComponent.historicalServingsCount = counts;

                if (isGroup)
                    dailyAggregateComponent.servingCount = getGroupAggregate(group, dailyAggregateComponent.historicalServingsCount, true);
                else
                    dailyAggregateComponent.servingCount = sum(dailyAggregateComponent.historicalServingsCount);

                let weeklyAggregateComponent = find(weeklyDoc.mealComponents, (mc) => {
                    return m.foodType === mc.foodType && m.mealType === mc.mealType;
                });

                if (!weeklyAggregateComponent) {
                    LogInfo(`Aggregate weekly food component for food type ${m.foodType} and meal type ${m.mealType} not found.  Creating new!`);
                    weeklyAggregateComponent = {
                        foodType: m.foodType,
                        mealType: m.mealType,
                        historicalServingsCount: []
                    } as MealComponent;

                    const weeklyMealComponents = cloneDeep(docUpdates.mealComponents);
                    weeklyMealComponents.push(weeklyAggregateComponent);
                    docUpdates = docUpdates.with({ mealComponents: weeklyMealComponents });

                } else {
                    LogInfo(`Aggregate weekly food component for food type ${m.foodType} and meal type ${m.mealType} exists.`);
                }

                counts = cloneDeep(weeklyAggregateComponent.historicalServingsCount);
                counts.push(+m.servingCount);
                weeklyAggregateComponent.historicalServingsCount = counts;
                if (isGroup) {
                    weeklyAggregateComponent.servingCount = getGroupAggregate(group, dailyAggregateComponent.historicalServingsCount, true);
                } else
                    weeklyAggregateComponent.servingCount = sum(weeklyAggregateComponent.historicalServingsCount);
            } else {
                LogError(`Invalid value in Athlete Weekly Food aggregator. 
                    Serving count must be a numerical value:`, { servingCount: m.servingCount })
            }
        });
    } else {
        LogError(`Invalid values in Athlete Weekly Food aggregator. 
            Meal components array is empty or null.`);
    }

    return docUpdates;
};
