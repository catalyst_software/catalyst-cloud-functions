// import { isArray, cloneDeep, find, mean } from "lodash";

// import { AthleteBiometricEntryInterface } from "../../../well/interfaces/athelete-biometric-entry";

// import { LogError, LogInfo } from "../../../../../shared/logger/logger";
// import { isNumber } from "../utils";
// import { MealComponent } from "../../../well/interfaces/meal-component";
// import { AthleteMonthlyAggregate, GroupMonthlyAggregate } from "../../../models/documents/aggregates";

// export const processMonthlyMealComponents = (
//     entry: AthleteBiometricEntryInterface, monthlyDocs: AthleteMonthlyAggregate | GroupMonthlyAggregate): AthleteMonthlyAggregate | GroupMonthlyAggregate => {
//     let docUpdates: AthleteMonthlyAggregate | GroupMonthlyAggregate = monthlyDocs;
//     if (isArray(entry.mealComponents) && entry.mealComponents.length > 0) {

//         if (!isArray(docUpdates.historicalValues)) {
//             docUpdates = docUpdates.with({ historicalValues: [] });
//         }
//         if (!isArray(docUpdates.mealComponents)) {
//             docUpdates = docUpdates.with({ mealComponents: [] });
//         }

//         entry.mealComponents.forEach((m) => {
//             if (isNumber(m.servingCount)) {
//                 m.servingCount = +m.servingCount;
//                 // Track the meal component itself
//                 const historicalValues = cloneDeep(docUpdates.historicalValues);
//                 historicalValues.push(m);
//                 docUpdates = docUpdates.with({ historicalValues: historicalValues });

//                 // Find the Monthly Meal Component object by type and aggregagte below 
//                 let monthlyAggregateComponent = find(docUpdates.mealComponents, (mc) => {
//                     return m.foodType === mc.foodType && m.mealType === mc.mealType;
//                 });

//                 if (!monthlyAggregateComponent) {
//                     LogInfo(`Aggregate monthly food component for food type ${m.foodType} and meal type ${m.mealType} not found.  Creating new!`);
//                     monthlyAggregateComponent = {
//                         foodType: m.foodType,
//                         mealType: m.mealType,
//                         historicalServingsCount: []
//                     } as MealComponent;
//                     const mealComponents = cloneDeep(docUpdates.mealComponents);
//                     mealComponents.push(monthlyAggregateComponent);

//                     docUpdates = docUpdates.with({ mealComponents });
//                 } else {
//                     LogInfo(`Aggregate monthly food component for food type ${m.foodType} and meal type ${m.mealType} exists.`);
//                 }
//                 if (!isArray(monthlyAggregateComponent.historicalServingsCount)) {
//                     monthlyAggregateComponent.historicalServingsCount = [];
//                 }
//                 const counts = cloneDeep(monthlyAggregateComponent.historicalServingsCount);
//                 counts.push(+m.servingCount);
//                 monthlyAggregateComponent.historicalServingsCount = counts;
//                 monthlyAggregateComponent.servingCount = mean(monthlyAggregateComponent.historicalServingsCount);
//             } else {
//                 LogError(`Invalid value in Athlete Weekly Food aggregator. 
//                 Serving count must be a numerical value:`, { servingCount: m.servingCount })
//             }
//         });
//     } else {
//         LogError(`Invalid values in Athlete Weekly Food aggregator. 
//         Meal components array is empty or null.`);
//     }

//     return docUpdates;
// };
