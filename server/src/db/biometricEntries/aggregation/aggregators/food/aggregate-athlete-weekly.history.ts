import { cloneDeep } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";

import { MealComponent } from "../../../well/interfaces/meal-component";
import { manageHistoricalMealComponent } from "./manageHistoricalMealComponent";
import { getWeekNumber } from "../../../../../shared/utils/moment";
import { AthleteWeeklyHistoryAggregate } from "../../../models/documents/aggregates";

export const aggregateAthleteWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyHistoryAggregate,
): AthleteWeeklyHistoryAggregate => {

    LogInfo({
        message: `Processing Athlete Weekly History aggregate document in Food aggregator.`,
        data: { historyDocument: aggregateDoc, biometricEntryId: entry.uid }
    });

    let update = aggregateDoc as any;
    if (!update.trackingCache) {
        update = update.trackingCache = {
            index: 0,
            valueCache: [],
            componentCache: {
                index: 0,
                components: []
            }
        };
    }

    const weekNumber = getWeekNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);

    if (update.trackingCache.index !== weekNumber) {
        update.trackingCache.index = weekNumber;
        update.trackingCache.componentCache = {
            index: weekNumber,
            components: []
        }
    }

    if (!update.mealComponentHistory) {
        update = update.with({ mealComponentHistory: [] });
    }

    if (update.mealComponentHistory.length <= weekNumber) {
        const existing = cloneDeep(update.mealComponentHistory);
        for (let i = update.mealComponentHistory.length; i <= weekNumber; i++) {
            existing.unshift({ index: i, components: [] });
        }

        update = update.with({ mealComponentHistory: existing });
    }

    entry.mealComponents.forEach((c) => {
        if (isNumber(c.servingCount)) {
            let response = manageHistoricalMealComponent(c, update.trackingCache.componentCache.components, false);
            update.trackingCache.componentCache.components = response.componentArray;
            let cacheComponent = response.component as MealComponent;

            LogInfo(`Processed component cache in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: update.trackingCache.componentCache,
                BiometricEntity: entry
            });

            response = manageHistoricalMealComponent(c, update.mealComponentHistory[0].components, false);
            update.mealComponentHistory[0].components = response.componentArray;
            cacheComponent = response.component as MealComponent;
            
            // TODO: Remove trackingCache?!?
            // // let response = manageHistoricalMealComponent(c, update.trackingCache.componentCache.components, false);
            // // update.trackingCache.componentCache.components = response.componentArray;
            // // let cacheComponent = response.component as MealComponent;

            // // LogInfo(`Processed component cache in Meal aggregator.`, {
            // //     CacheComponent: cacheComponent,
            // //     ComponentCacheArray: update.trackingCache.componentCache,
            // //     BiometricEntity: entry
            // // });

            // const response = manageHistoricalMealComponent(c, update.mealComponentHistory[0].components, false);
            // update.mealComponentHistory[0].components = response.componentArray;
            // const cacheComponent = response.component as MealComponent;

            LogInfo(`Processed meal component history in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: update.mealComponentHistory[0],
                BiometricEntity: entry
            });

        } else {
            LogError(`Invalid value in Weekly Meal aggregator. 
            Serving Count must be a numerical value:`, { servingCount: c.servingCount });
        }
    });

    return update;
};
