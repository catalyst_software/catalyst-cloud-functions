import { cloneDeep } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";
import { manageHistoricalMealComponent } from "./manageHistoricalMealComponent";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";
import { getMonthNumber } from "../../../../../shared/utils/moment";
import { GroupMonthlyHistoryAggregate } from "../../../models/documents/aggregates/group/group-monthly-history-aggregate.document";

export const aggregateGroupMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupMonthlyHistoryAggregate => {

    LogInfo({
        message: `Processing GroupMonthly History aggregate document in Sleep aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    });

    let updates = aggregateDoc;

    const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);
    LogInfo({
        message: `Calculated month number for GroupMonthly History ${monthNumber}.`,
        data: {
            AggregateDocument: updates,
            BiometricEntity: entry
        }
    });

    if (!updates.trackingCache) {
        updates.trackingCache = {
            index: 0,
            valueCache: [],
            componentCache: {
                index: 0,
                components: []
            }
        };
    }

    if (updates.trackingCache.index !== monthNumber) {
        updates.trackingCache.index = monthNumber;
        updates.trackingCache.componentCache = {
            index: monthNumber,
            components: []
        };
    }

    if (!updates.mealComponentHistory) {
        updates = updates.with({ mealComponentHistory: [] });
    }

    if (updates.mealComponentHistory.length <= monthNumber) {
        const existing = cloneDeep(updates.mealComponentHistory);
        for (let i = updates.mealComponentHistory.length; i <= monthNumber; i++) {
            existing.unshift({ index: i, components: [] });
        }
        updates = updates.with({ mealComponentHistory: existing });
    }

    entry.mealComponents.forEach((c) => {
        if (isNumber(c.servingCount)) {
            let response = manageHistoricalMealComponent(c, updates.trackingCache.componentCache.components, true, directive.group);
            updates.trackingCache.componentCache.components = response.componentArray;
            let cacheComponent = response.component;

            LogInfo(`Processed component cache in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.trackingCache.componentCache,
                BiometricEntity: entry
            });

            response = manageHistoricalMealComponent(c, updates.mealComponentHistory[0].components, true, directive.group);
            updates.mealComponentHistory[0].components = response.componentArray;
            cacheComponent = response.component;

            LogInfo(`Processed meal component history in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.mealComponentHistory[0],
                BiometricEntity: entry
            });

        } else {
            LogError(`Invalid value in Weekly Meal aggregator. 
            Serving Count must be a numerical value:`, { servingCount: c.servingCount });
        }
    });

    return updates;
};
