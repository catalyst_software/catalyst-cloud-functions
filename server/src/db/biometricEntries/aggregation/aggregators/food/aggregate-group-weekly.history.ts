import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";
import { MealComponent } from "../../../well/interfaces/meal-component";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";
import { manageHistoricalMealComponent } from './manageHistoricalMealComponent';
import { cloneDeep } from 'lodash';
import { getWeekNumber } from "../../../../../shared/utils/moment";
import { GroupWeeklyHistoryAggregate } from "../../../models/documents/aggregates";

export const aggregateGroupWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyHistoryAggregate => {

    LogInfo({
        message: `Processing GroupWeekly History aggregate document in Food aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    });
    let updates = aggregateDoc as any;
    const weekNumber = getWeekNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);
    LogInfo({
        message: `Calculated week number for GroupWeekly History ${weekNumber}.`,
        data: {
            AggregateDocument: updates,
            BiometricEntity: entry
        }
    });

    if (!updates.trackingCache) {
        updates.trackingCache = {
            index: 0,
            valueCache: [],
            componentCache: {
                index: 0,
                components: []
            }
        };
    }

    if (updates.trackingCache.index !== weekNumber) {
        updates.trackingCache.index = weekNumber;
        updates.trackingCache.componentCache = {
            index: 0,
            components: []
        };
    }

    if (!updates.mealComponentHistory) {
        updates = updates.with({ mealComponentHistory: [] });
    }

    if (updates.mealComponentHistory.length <= weekNumber) {
        const existing = cloneDeep(updates.mealComponentHistory);
        for (let i = updates.mealComponentHistory.length; i <= weekNumber; i++) {
            existing.unshift({index: i, components: []});
        }
        updates = updates.with({mealComponentHistory: existing});
    }

    entry.mealComponents.forEach((c) => {
        if (isNumber(c.servingCount)) {
            let response = manageHistoricalMealComponent(c, updates.trackingCache.componentCache.components, true, directive.group);
            updates.trackingCache.componentCache.components = response.componentArray;
            let cacheComponent = response.component as MealComponent;

            LogInfo(`Processed component cache in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.trackingCache.componentCache,
                BiometricEntity: entry
            });

            response = manageHistoricalMealComponent(c, updates.mealComponentHistory[0].components, true, directive.group);
            updates.mealComponentHistory[0].components = response.componentArray;
            cacheComponent = response.component as MealComponent;

            LogInfo(`Processed meal component history in Meal aggregator.`, {
                CacheComponent: cacheComponent,
                ComponentCacheArray: updates.mealComponentHistory[0],
                BiometricEntity: entry
            });

        } else {
            LogError(`Invalid value in Weekly Meal aggregator. 
            Serving Count must be a numerical value:`, { servingCount: c.servingCount });
        }
    });

    return updates;
};
