
import { CalculateAggregates } from "../../interfaces/calculate-aggregates";

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry';

import { aggregateAthleteHistory } from "./aggregate-athlete-history";
import { aggregateGroupHistory } from "./aggregate-group-history";
import { BinaryBiometricHistoryAggregate } from "../../../models/documents/aggregates/binary-history-aggregate.document";

/**
* Still working on this // function(current Document, collection of aggregates)
* // function(current Document, collection of aggregates)
* @param biometricEntry: AthleteBiometricEntry
* @param aggregateDoc: BiometricAggregate
*/
export class PeriodAggregator implements CalculateAggregates {

    // BINARY
    processAthleteBinaryHistory?(entry: AthleteBiometricEntryInterface, aggregateDoc: BinaryBiometricHistoryAggregate): BinaryBiometricHistoryAggregate {
        const athleteRes = aggregateDoc.with(aggregateAthleteHistory(entry, aggregateDoc.toJS()));
        return athleteRes
    }
    processGroupBinaryHistory?(entry: AthleteBiometricEntryInterface, aggregateDoc: BinaryBiometricHistoryAggregate): BinaryBiometricHistoryAggregate {
        const groupRes = aggregateDoc.with(aggregateGroupHistory(entry, aggregateDoc.toJS()));

        return groupRes
    }
}
