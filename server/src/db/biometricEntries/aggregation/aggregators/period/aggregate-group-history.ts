import { isNumber } from "./utils"
import { cloneDeep, findLast } from "lodash"

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry"
import { LogError, LogInfo } from "../../../../../shared/logger/logger"
import { BinaryBiometricHistoryAggregateInterface, BinaryBiometric } from '../../../well/aggregate/interfaces/athlete/athlete-binary-biometric-aggregate';

export const aggregateGroupHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: BinaryBiometricHistoryAggregateInterface
): BinaryBiometricHistoryAggregateInterface => {

    LogInfo({
        message: `Processing Group Binary History aggregate document in Period aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    const val = +entry.value;
    if (isNumber(val)) {

        const values = cloneDeep(aggregateDoc.values);
        const el = findLast(values, (e: BinaryBiometric) => {
            return e.uid === entry.userId;
        });

        if (val === 0) {

            if (el && !el.off)
                el.off = entry.dateTime;

        } else {
            if (!el || (el && el.off))
                values.push({
                    on: entry.dateTime,
                    uid: entry.userId
                } as BinaryBiometric);
        }

        aggregateDoc.values = values;
    } else {
        LogError(`Invalid value in Group Binary Period aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return aggregateDoc;
}


