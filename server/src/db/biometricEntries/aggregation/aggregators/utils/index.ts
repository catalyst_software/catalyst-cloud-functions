import { isArray, sum } from 'lodash'
import { firestore } from 'firebase-admin'

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { Group } from '../../../../../models/group/group.model'
import { MealComponent } from '../../../well/interfaces/meal-component'

export const logDayIndex = (
    entry: AthleteBiometricEntryInterface,
    dayIndex: number,
    message?: string
) => {
    // TODO: sockets

    const toTimestamp = new firestore.Timestamp(
        (<any>entry).dateTime._seconds,
        (<any>entry).dateTime._nanoseconds
    )
    console.log(message, {
        EntryDate: toTimestamp.toDate(),
        DayIndex: dayIndex,
    })
}

export const isNumber = (val: any) => {
    return !isNaN(parseInt(val))
}

export const getGroupAggregate = (
    group: Group,
    values: Array<number>,
    doSum?: boolean
) => {
    if (values.length) {
        const includedAthletes = group.athletes.filter(
            ath => ath.includeGroupAggregation
        )

        const denominator = group
            ? isArray(includedAthletes)
                ? includedAthletes.length
                : 1
            : 1

        if (doSum) {
            return sum(values)
        } else {
            return sum(values) / denominator
        }
    } else {
        return 0
    }
}

export const getGroupAggregateForSimpleFood = (
    group: Group,
    mealComponents: Array<MealComponent>,
    doSum?: boolean
) => {
    if (mealComponents.length) {
        if (!group.athletes) {
            group.athletes = []
        }
        const includedAthletes = group.athletes.filter(
            ath => ath.includeGroupAggregation
        )

        const denominator = group
            ? isArray(includedAthletes)
                ? includedAthletes.length
                : 1
            : 1

        const val = mealComponents
            .map(mc => mc.calorieCount)
            .reduce((a, b) => a + b, 0)
        if (doSum) {
            return val
        } else {
            return denominator ? val / denominator : val
        }
    } else {
        return 0
    }
}

export * from './update-weekly-scalar-values'
