import { isArray, cloneDeep, mean, filter } from 'lodash';
import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry';

import { AthleteDailyAggregateModel } from '../../../well/aggregate/athelete-biometric-daily-aggregate.model';
import { AllAggregateType, AggregateType } from '../../../../../models/types/biometric-aggregate-types';

import { logDayIndex } from '.';
import { LogInfo } from '../../../../../shared/logger/logger';
import { getIsoWeekDay } from '../../../../../shared/utils/moment';
import { firestore } from 'firebase-admin';
import { AthleteWeeklyAggregate } from '../../../models/documents/aggregates';
import { isAthleteWeekly } from '../../../aliases/interfaces';

export const validateFieldsInitialized = (aggregateDoc: AllAggregateType, dailyAggregate?: AthleteDailyAggregateModel): AllAggregateType => {
    if (dailyAggregate && !isArray(dailyAggregate.historicalValues)) {
        dailyAggregate.historicalValues = [];
    }
    if ((<AggregateType>aggregateDoc).historicalValues && !isArray((<AggregateType>aggregateDoc).historicalValues)) {
        return aggregateDoc.with({ historicalValues: [] });
    }
    else
        return aggregateDoc;
};

export const updateWeeklyScalarValues = (entry: AthleteBiometricEntryInterface,
    aggregateDoc: AllAggregateType,
    aggregator: string, entity) => {

    LogInfo(`Processing ${entity} Weekly aggregate document in ${aggregator} aggregator.`, {
        AggregateDocument: aggregateDoc,
        entityId: entry.uid
    });

    const val = +entry.value;
    let docUpdates = aggregateDoc;
    if (isAthleteWeekly(docUpdates)) {

debugger;
        const toTimestamp = new firestore.Timestamp((<any>entry).dateTime._seconds, (<any>entry).dateTime._nanoseconds);
 

        const dayIndex = getIsoWeekDay(toTimestamp, entry.utcOffset);
        logDayIndex(entry, dayIndex, `${entity} Weekly Entry Day Index data:`);

        const dailyAggregate: AthleteDailyAggregateModel = docUpdates.dailies[dayIndex];

         docUpdates = validateFieldsInitialized(docUpdates, dailyAggregate);

        if (dailyAggregate) {

            LogInfo(`${entity} Weekly Daily aggregate before update:`, dailyAggregate);

            const historicalValues = cloneDeep(dailyAggregate.historicalValues);
            historicalValues.push(val);

            dailyAggregate.historicalValues = historicalValues;
            dailyAggregate.value = mean(dailyAggregate.historicalValues);

            LogInfo(`${entity} Weekly aggregate after update:`, dailyAggregate)
        }

        const dailyValues = filter((<AthleteWeeklyAggregate>docUpdates).dailies,
            (d) => { return d.value > 0; }).map((d) => { return d.value; });
            LogInfo(`${entity} Mean aggregate before update:`, dailyAggregate);

        if (dailyValues && dailyValues.length) {
            docUpdates = docUpdates.with({
                value: mean(dailyValues)
            });
        }  
         LogInfo(`${entity} Mean aggregate before update:`, dailyAggregate)

    }
    return docUpdates;
};
