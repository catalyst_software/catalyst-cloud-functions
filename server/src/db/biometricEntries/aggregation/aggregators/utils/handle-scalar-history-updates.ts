import { cloneDeep } from "lodash";
import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { AthleteMonthlyHistoryAggregate } from "../../../models/documents/aggregates/athlete/athelete-monthly-history-aggregate.document";
import { AthleteWeeklyHistoryAggregate } from "../../../models/documents/aggregates";
import { GroupMonthlyHistoryAggregate } from "../../../models/documents/aggregates/group/group-monthly-history-aggregate.document";

export const handleScalarHistoryUpdates = (entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteMonthlyHistoryAggregate | AthleteWeeklyHistoryAggregate | GroupMonthlyHistoryAggregate,
    theNumber: number) => {
    let updates = aggregateDoc;
    const val = +entry.value;
    if (!updates.trackingCache) {
        updates.trackingCache = {
            index: 0,
            valueCache: [],
            componentCache: {
                index: 0,
                components: []
            }
        };
    }

    // if (!updates.trackingCache) {
    //     updates = updates.trackingCache = {
    //         index: 0,
    //         valueCache: [],
    //         componentCache: {
    //             index: 0,
    //             components: []
    //         }
    //     };
    // }
    if (updates.trackingCache.index !== theNumber) {
        updates.trackingCache.index = theNumber;
        updates.trackingCache.valueCache = [];
    }
    if (updates.values.length <= theNumber) {
        const existing = cloneDeep(updates.values);
        for (let i = updates.values.length; i <= theNumber; i++) {
            existing.unshift(0);
        }
        updates = updates.with({ values: existing });
    }
    const newVals = cloneDeep(updates.trackingCache.valueCache);
    newVals.push(val);
    updates.trackingCache.valueCache = newVals;

    return updates;
};
