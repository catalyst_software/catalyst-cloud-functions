import { isNumber } from './utils'
import { mean } from 'lodash'

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { LogInfo, LogError } from '../../../../../shared/logger/logger'
import { getWeekNumber } from '../../../../../shared/utils/moment'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive'
import { GroupWeeklyHistoryAggregate } from '../../../models/documents/aggregates'
import { handleScalarHistoryUpdates } from '../utils/handle-scalar-history-updates'

export const aggregateGroupWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyHistoryAggregate => {
    LogInfo({
        message: `Processing GroupWeekly History aggregate document in Weight aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry,
        },
    })
    let updates = aggregateDoc as any
    const weekNumber = getWeekNumber(
        entry.athleteCreationTimestamp,
        entry.dateTime,
        entry.utcOffset
    )
    LogInfo({
        message: `Calculated week number for GroupWeekly History ${weekNumber}.`,
        data: {
            AggregateDocument: updates,
            BiometricEntity: entry,
        },
    })

    if (isNumber(entry.value)) {
        const val = +entry.value
        let totalWeight = val
        const athletes = directive.groupAthletes
        athletes.forEach(a => {
            if (a.uid !== entry.userId) {
                if (a.profile.bodyComposition) {
                    totalWeight += a.profile.bodyComposition.weight.kilos || 0
                }
            }
        })

        updates = handleScalarHistoryUpdates(entry, aggregateDoc, weekNumber)
        updates.values[0] = totalWeight / athletes.length // mean(updates.trackingCache.valueCache)
    } else {
        LogError(
            `Invalid value in Group Weekly Weight aggregator. 
        Value must be a numerical value:`,
            { value: entry.value }
        )
    }

    return updates
}
