import { cloneDeep, filter, mean } from 'lodash'

import {
    AggregateType,
    AllAggregateType,
} from '../../../../../models/types/biometric-aggregate-types'
import { AthleteDailyAggregateModel } from '../../../well/aggregate/athelete-biometric-daily-aggregate.model'
import { isAthleteWeekly } from '../../../aliases/interfaces'
import { AthleteBiometricEntryInterface as AthleteBiometricEntry } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'

export const updateValues = (
    entry: AthleteBiometricEntry,
    aggregateDoc: AllAggregateType,
    dailyAggregate?: AthleteDailyAggregateModel
) => {
    const val = +entry.value

    if (isAthleteWeekly(aggregateDoc)) {
        if (dailyAggregate) {
            const historicalValues = cloneDeep(dailyAggregate.historicalValues)
            historicalValues.push(val)

            dailyAggregate.historicalValues = historicalValues
            dailyAggregate.value = mean(dailyAggregate.historicalValues)
        }

        const dailyValues = filter(aggregateDoc.dailies, d => {
            return d.value > 0
        }).map(d => {
            return d.value
        })
        if (dailyValues && dailyValues.length) {
            return aggregateDoc.with({
                value: mean(dailyValues),
            })
        }
    }

    return aggregateDoc
}
export const validateFieldsInitialized = (
    aggregateDoc: AllAggregateType,
    dailyAggregate?: AthleteDailyAggregateModel
): AllAggregateType => {
    if (dailyAggregate && !Array.isArray(dailyAggregate.historicalValues)) {
        dailyAggregate.historicalValues = []
    }
    if (
        (<AggregateType>aggregateDoc).historicalValues &&
        !Array.isArray((<AggregateType>aggregateDoc).historicalValues)
    ) {
        return aggregateDoc.with({ historicalValues: [] })
    } else {
        return aggregateDoc
    }
}
