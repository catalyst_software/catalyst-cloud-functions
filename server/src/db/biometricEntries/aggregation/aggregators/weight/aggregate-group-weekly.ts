import { isArray, cloneDeep, mean, filter } from 'lodash'

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'

import { LogInfo, LogError } from '../../../../../shared/logger/logger'
import { isNumber } from './utils'
import { GroupDailyAggregate } from '../../../well/aggregate/interfaces'
import { getIsoWeekDay } from '../../../../../shared/utils/moment'
import { GroupWeeklyAggregate } from '../../../models/documents/aggregates'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive'

export const aggregateGroupWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyAggregate => {
    LogInfo(`Processing GroupWeekly aggregate document in Weight aggregator.`, {
        AggregateDocument: aggregateDoc,
        BiometricEntity: entry,
    })

    let updates = aggregateDoc
    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset)

    const dailyAggregate: GroupDailyAggregate = updates.dailies[dayIndex]
    if (!isArray(dailyAggregate.historicalValues)) {
        dailyAggregate.historicalValues = []
    }

    if (isNumber(entry.value)) {
        const val = +entry.value
        let totalWeight = val
        const athletes = directive.groupAthletes
        athletes.forEach(a => {
            if (a.uid !== entry.userId) {
                if (a.profile.bodyComposition) {
                    totalWeight += a.profile.bodyComposition.weight.kilos || 0
                }
            }
        })

        if (isArray(athletes) && athletes.length) {
            totalWeight /= athletes.length
            if (dailyAggregate) {
                const historicalValues = cloneDeep(
                    dailyAggregate.historicalValues
                )
                historicalValues.push(val)

                dailyAggregate.historicalValues = historicalValues
                // dailyAggregate.value = mean(dailyAggregate.historicalValues)
                dailyAggregate.value = totalWeight
            }

            const dailyValues = filter(updates.dailies, d => {
                return d.value > 0
            }).map(d => {
                return d.value
            })
            if (dailyValues && dailyValues.length) {
                updates = updates.with({
                    value: totalWeight, // mean(dailyValues),
                })
            }
        } else {
            LogError(
                `No athlete documents returned from group ${aggregateDoc.entityId}`
            )
        }
    } else {
        LogError(
            `Invalid value in Group Weekly Weight aggregator. 
        Value must be a numerical value: `,
            { value: entry.value }
        )
    }

    return updates
}
