import { isNumber } from './utils'
import { mean } from 'lodash'

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { LogError, LogInfo } from '../../../../../shared/logger/logger'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive'
import { getMonthNumber } from '../../../../../shared/utils/moment'
import { GroupMonthlyHistoryAggregate } from '../../../models/documents/aggregates/group/group-monthly-history-aggregate.document'
import { handleScalarHistoryUpdates } from '../utils/handle-scalar-history-updates'

export const aggregateGroupMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupMonthlyHistoryAggregate => {

    debugger;
    LogInfo({
        message: `Processing Group Monthly History document in Fatigue aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry,
        },
    })
    let updates = aggregateDoc
    const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset)
    LogInfo({
        message: `Calculated month number for GroupMonthly History ${monthNumber}.`,
        data: {
            AggregateDocument: updates,
            BiometricEntity: entry,
        },
    })

    if (isNumber(entry.value)) {
        debugger
        updates = handleScalarHistoryUpdates(entry, aggregateDoc, monthNumber)
        updates.values[0] = mean(updates.trackingCache.valueCache)
    } else {
        LogError(
            `Invalid value in Group Monthly Fatigue aggregator. 
        Value must be a numerical value:`,
            { value: entry.value }
        )
    }

    return updates
}
