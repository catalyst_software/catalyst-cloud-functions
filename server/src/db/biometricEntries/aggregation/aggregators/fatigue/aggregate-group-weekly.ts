import { isArray, cloneDeep, mean, filter } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogInfo, LogError } from "../../../../../shared/logger/logger";
import { isNumber } from "./utils";
import { GroupDailyAggregate } from "../../../well/aggregate/interfaces";
import { getIsoWeekDay } from "../../../../../shared/utils/moment";
import { GroupWeeklyAggregate } from "../../../models/documents/aggregates";


export const aggregateGroupWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyAggregate
): GroupWeeklyAggregate => {

    LogInfo(`Processing GroupWeekly aggregate document in Fatigue aggregator.`, {
        AggregateDocument: aggregateDoc,
        BiometricEntity: entry
    });

    let updates = aggregateDoc;
    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);

    const dailyAggregate: GroupDailyAggregate = updates.dailies[dayIndex];
    if (!isArray(dailyAggregate.historicalValues)) {
        dailyAggregate.historicalValues = [];
    }

    if (isNumber(entry.value)) {
        const val = +entry.value;

        if (dailyAggregate) {
            const historicalValues = cloneDeep(dailyAggregate.historicalValues);
            historicalValues.push(val);

            dailyAggregate.historicalValues = historicalValues;
            dailyAggregate.value = mean(dailyAggregate.historicalValues);
        }

        const dailyValues = filter(updates.dailies, (d) => {
            return d.value > 0;
        }).map((d) => {
            return d.value;
        });
        if (dailyValues && dailyValues.length) {
            updates = updates.with({
                value: mean(dailyValues)
            });
        }
    } else {
        LogError(`Invalid value in Group Weekly Sleep aggregator. 
        Value must be a numerical value: `, { value: entry.value })
    }

    return updates;

};
