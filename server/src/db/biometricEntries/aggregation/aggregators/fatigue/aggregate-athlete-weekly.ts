import { cloneDeep, mean, filter } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { isNumber } from "./utils";
import { GroupDailyAggregate } from "../../../well/aggregate/interfaces";
import { logDayIndex, validateFieldsInitialized } from "../utils";

import { LogError } from '../../../../../shared/logger/logger';
import { createDailies } from "../../../services/utils";
import { getIsoWeekDay, getWeekFromDate } from "../../../../../shared/utils/moment";
import { AthleteWeeklyAggregate } from "../../../models/documents/aggregates";


export const aggregateAthleteWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyAggregate
): AthleteWeeklyAggregate => {

    let docUpdates: any = aggregateDoc;

    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);
    logDayIndex(entry, dayIndex, `Athlete Weekly Entry Day Indexdata:`);
   

    if (!docUpdates.dailies) {
        const dailies = createDailies(entry, aggregateDoc.collection, getWeekFromDate(entry.dateTime, entry.utcOffset), 
        null);

        docUpdates = docUpdates.with({dailies: dailies})
    }
    const dailyAggregate: GroupDailyAggregate = docUpdates.dailies[dayIndex];
    docUpdates = validateFieldsInitialized(docUpdates, dailyAggregate);

    if (isNumber(entry.value)) {
        // LogInfo(`Athlete Weekly Daily aggregate before update:`, dailyAggregate)

        const val = +entry.value;

        if (dailyAggregate) {
            const historicalValues = cloneDeep(dailyAggregate.historicalValues);
            historicalValues.push(val);

            dailyAggregate.historicalValues = historicalValues;
            dailyAggregate.value = mean(dailyAggregate.historicalValues);
        }

        const dailyValues = filter(docUpdates.dailies, (d) => {
            return d.value > 0;
        }).map((d) => {
            return d.value;
        });
        if (dailyValues && dailyValues.length) {
            docUpdates = docUpdates.with({
                value: mean(dailyValues)
            });
        }
    } else {
        LogError(`Invalid value in Athlete Weekly Fatigue aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return docUpdates;
};

