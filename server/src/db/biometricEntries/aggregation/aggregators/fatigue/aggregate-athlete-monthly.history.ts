import { isNumber } from "./utils"
import { mean } from "lodash"

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry"
import { LogError, LogInfo } from "../../../../../shared/logger/logger"
import { getMonthNumber } from "../../../../../shared/utils/moment"
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive"
import { AthleteMonthlyHistoryAggregate } from "../../../models/documents/aggregates/athlete/athelete-monthly-history-aggregate.document"
import { handleScalarHistoryUpdates } from "../utils/handle-scalar-history-updates"

export const aggregateAthleteMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): AthleteMonthlyHistoryAggregate => {

    LogInfo({
        message: `Processing Athlete Monthly aggregate document in Fatigue aggregator.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    })


    const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset)
    console.log({
        message: `Calculated month number for AthleteMonthly History ${monthNumber}.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    let updates = aggregateDoc
    if (isNumber(entry.value)) {
        updates = handleScalarHistoryUpdates(entry, aggregateDoc, monthNumber) as AthleteMonthlyHistoryAggregate
        updates.values[0] = updates.trackingCache ? mean(updates.trackingCache.valueCache) : 0
    } else {
        LogError(`Invalid value in Athlete Monthly Fatigue aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return updates
}

