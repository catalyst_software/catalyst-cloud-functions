import { CalculateIpScoreAggregates } from "../../../ipscore/aggregation/interfaces/calculate-ipscore-aggregates";
import { AggregateIpScoreDirective } from "../../../ipscore/services/interfaces/aggregate-ipscore-directive";
import { AthleteIpScoreWeeklyAggregateInterface, GroupIpScoreWeeklyAggregateInterface } from "../../../ipscore/models/interfaces";
import { aggregateAthleteIpScoreWeekly } from "../../../ipscore/aggregation/aggregators/ipscore/aggregate-athlete-ipscore-weekly";
import { AthleteIpScoreWeeklyHistoryAggregateInterface } from "../../../ipscore/models/interfaces/athlete-ipscore-weekly-history-aggregate";
import { aggregateAthleteIpScoreWeeklyHistory } from "../../../ipscore/aggregation/aggregators/ipscore/aggregate-athlete-ipscore-weekly.history";
import { AthleteIpScoreMonthlyHistoryAggregateInterface } from "../../../ipscore/models/interfaces/athlete-ipscore-monthly-history-aggregate";
import { aggregateAthleteIpScoreMonthlyHistory } from "../../../ipscore/aggregation/aggregators/ipscore/aggregate-athlete-ipscore-monthly.history";
import { aggregateGroupIpScoreWeekly } from "../../../ipscore/aggregation/aggregators/ipscore/aggregate-group-ipscore-weekly";
import { GroupIpScoreWeeklyHistoryAggregateInterface } from "../../../ipscore/models/interfaces/group-ipscore-weekly-history-aggregate";
import { aggregateGroupIpScoreWeeklyHistory } from "../../../ipscore/aggregation/aggregators/ipscore/aggregate-group-ipscore-weekly.history";
import { GroupIpScoreMonthlyHistoryAggregateInterface } from "../../../ipscore/models/interfaces/group-ipscore-monthly-history-aggregate";
import { aggregateGroupIpScoreMonthlyHistory } from "../../../ipscore/aggregation/aggregators/ipscore/aggregate-group-ipscore-monthly.history";


export class IpScoreAggregator implements CalculateIpScoreAggregates {

    // constructor(private props: {
    //     // entry: AthleteBiometricEntry
    //     // aggregateDoc: AthleteWeeklyAggregate
    // }) { }

    // Athlete Weekly
    processAthleteIpScoreWeekly(directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyAggregateInterface {
        return aggregateAthleteIpScoreWeekly(directive);
    }

    processAthleteIpScoreWeeklyHistory(directive: AggregateIpScoreDirective
    ): AthleteIpScoreWeeklyHistoryAggregateInterface {
        return aggregateAthleteIpScoreWeeklyHistory(directive);
    }

    processAthleteIpScoreMonthlyHistory(directive: AggregateIpScoreDirective
    ): AthleteIpScoreMonthlyHistoryAggregateInterface {
        return aggregateAthleteIpScoreMonthlyHistory(directive);
    }

    processGroupIpScoreWeekly(directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyAggregateInterface {
        debugger;
        return aggregateGroupIpScoreWeekly(directive);
    }

    processGroupIpScoreWeeklyHistory(directive: AggregateIpScoreDirective
    ): GroupIpScoreWeeklyHistoryAggregateInterface {
        debugger;
        return aggregateGroupIpScoreWeeklyHistory(directive);
    }

    processGroupIpScoreMonthlyHistory(directive: AggregateIpScoreDirective
    ): GroupIpScoreMonthlyHistoryAggregateInterface {
        debugger;
        return aggregateGroupIpScoreMonthlyHistory(directive);
    }

}
