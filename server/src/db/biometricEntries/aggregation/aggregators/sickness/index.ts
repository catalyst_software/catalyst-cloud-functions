
import { CalculateAggregates } from "../../interfaces/calculate-aggregates";

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry';

import { aggregateAthleteHistory } from "./aggregate-athlete-history";
import { aggregateGroupHistory } from "./aggregate-group-history";
import { BinaryBiometricHistoryAggregate } from "../../../models/documents/aggregates/binary-history-aggregate.document";

/**
* Still working on this // function(current Document, collection of aggregates)
* // function(current Document, collection of aggregates)
* @param biometricEntry: AthleteBiometricEntry
* @param aggregateDoc: BiometricAggregate
*/
export class SicknessAggregator implements CalculateAggregates {

    // BINARY
    processAthleteBinaryHistory?(entry: AthleteBiometricEntryInterface, aggregateDoc: BinaryBiometricHistoryAggregate): BinaryBiometricHistoryAggregate {

        const update = aggregateAthleteHistory(entry, aggregateDoc.toJS())

        const updatedAthleteAggregateDoc = aggregateDoc.with(update);

        return updatedAthleteAggregateDoc
    }
    processGroupBinaryHistory?(entry: AthleteBiometricEntryInterface, aggregateDoc: BinaryBiometricHistoryAggregate): BinaryBiometricHistoryAggregate {
        const updatedGroupAggregateDoc = aggregateDoc.with(aggregateGroupHistory(entry, aggregateDoc.toJS()));

        return updatedGroupAggregateDoc
    }
}
