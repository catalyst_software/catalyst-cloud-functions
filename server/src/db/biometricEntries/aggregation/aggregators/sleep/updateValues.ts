import _ = require('lodash');
import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { AthleteDailyAggregateModel } from '../../../well/aggregate/athelete-biometric-daily-aggregate.model';
import { AllAggregateType, AggregateType } from '../../../../../models/types/biometric-aggregate-types';
import { isAthleteWeekly } from '../../../aliases/interfaces';

export const updateValues = (entry: AthleteBiometricEntryInterface, aggregateDoc: AllAggregateType, dailyAggregate?: AthleteDailyAggregateModel) => {

    const val = +entry.value;

    if (isAthleteWeekly(aggregateDoc)) {
        if (dailyAggregate) {
            const historicalValues = _.cloneDeep(dailyAggregate.historicalValues);
            historicalValues.push(val);

            dailyAggregate.historicalValues = historicalValues;
            dailyAggregate.value = _.mean(dailyAggregate.historicalValues);
        }

        const dailyValues = _.filter(aggregateDoc.dailies, (d) => {
            return d.value > 0;
        }).map((d) => {
            return d.value;
        });
        if (dailyValues && dailyValues.length) {
            return aggregateDoc.with({
                value: _.mean(dailyValues)
            });
        }
    }
    return aggregateDoc;
};
export const validateFieldsInitialized = (aggregateDoc: AllAggregateType, dailyAggregate?: AthleteDailyAggregateModel): AllAggregateType => {
    if (dailyAggregate && !_.isArray(dailyAggregate.historicalValues)) {
        dailyAggregate.historicalValues = [];
    }
    if ((<AggregateType>aggregateDoc).historicalValues && !_.isArray((<AggregateType>aggregateDoc).historicalValues)) {
        return aggregateDoc.with({ historicalValues: [] });
    }
    else
        return aggregateDoc;
};
