import { isNumber } from '../utils'
import { mean } from 'lodash'

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { LogError, LogInfo } from '../../../../../shared/logger/logger'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive'
import { getMonthNumber } from '../../../../../shared/utils/moment'
import { GroupMonthlyHistoryAggregate } from '../../../models/documents/aggregates/group/group-monthly-history-aggregate.document'
import { handleScalarHistoryUpdates } from '../utils/handle-scalar-history-updates'

export const aggregateGroupMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupMonthlyHistoryAggregate => {

    LogInfo({
        message: `Processing Group Monthly History aggregate document in Sleep aggregator.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    })

    let updates = aggregateDoc
    const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset)

    LogInfo({
        message: `Calculated month number for Group Monthly History ${monthNumber}.`,
        data: { historyDocument: { uid: updates.uid }, biometricEntryId: entry.uid }
    })

    if (isNumber(entry.value)) {
        debugger;
        updates = handleScalarHistoryUpdates(entry, aggregateDoc, monthNumber)as any
        updates.values[0] = mean(updates.trackingCache.valueCache)

    } else {
        LogError(`Invalid value in Group Monthly Sleep aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return updates
}

