import { isNumber } from "../utils"
import { mean } from "lodash"

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { LogError, LogInfo } from '../../../../../shared/logger/logger'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive'
import { getWeekNumber } from '../../../../../shared/utils/moment'
import { GroupWeeklyHistoryAggregate } from '../../../models/documents/aggregates'
import { handleScalarHistoryUpdates } from "../utils/handle-scalar-history-updates"


export const aggregateGroupWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyHistoryAggregate => {
debugger;
    LogInfo({
        message: `Processing Group Weekly HIstory aggregate document in Sleep aggregator.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    })

    const weekNumber =getWeekNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset)

    LogInfo({
        message: `Calculated week number for Group Weekly History ${weekNumber}.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    })

    let updates = aggregateDoc
    if (isNumber(entry.value)) {
        debugger;
        updates = handleScalarHistoryUpdates(entry, aggregateDoc, weekNumber)
        updates.values[0] = mean(updates.trackingCache.valueCache)
    } else {
        LogError(`Invalid value in Group Weekly Sleep aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return updates
}

