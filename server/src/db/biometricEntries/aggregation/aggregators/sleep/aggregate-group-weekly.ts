import { cloneDeep, mean, filter } from 'lodash';

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry';


import { LogError, LogInfo } from '../../../../../shared/logger/logger';
import { isNumber, validateFieldsInitialized } from '../utils';
import { GroupDailyAggregateModel } from '../../../well/aggregate/group-biometric-daily-aggregate.model';
import { getIsoWeekDay, getWeekFromDate } from "../../../../../shared/utils/moment";
import { createDailies } from '../../../services/utils';
import { GroupIndicator } from '../../../well/interfaces/group-indicator';
import { GroupWeeklyAggregate } from '../../../models/documents/aggregates';

export const aggregateGroupWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyAggregate,
    group: GroupIndicator
): GroupWeeklyAggregate => {

    LogInfo({
        message: `Processing Group Weekly aggregate document in Sleep aggregator.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    });

    let docUpdates: any = aggregateDoc;

if (!docUpdates.dailies) {
    const dailies = createDailies(entry, aggregateDoc.collection, getWeekFromDate(entry.dateTime, entry.utcOffset), 
    group);

    docUpdates = docUpdates.with({dailies: dailies})
}

    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);
    const dailyAggregate: GroupDailyAggregateModel = docUpdates.dailies[dayIndex];
    
    docUpdates = validateFieldsInitialized(docUpdates, dailyAggregate);

    if (isNumber(entry.value)) {
        const val = +entry.value;
        const historicalValues = cloneDeep(dailyAggregate.historicalValues);
        historicalValues.push(val);
        dailyAggregate.historicalValues = historicalValues;
        dailyAggregate.value = mean(dailyAggregate.historicalValues);
        
        LogInfo({
            message: `Value after mean`,
            data: {
                historyDocument: { uid: docUpdates.uid }, biometricEntryId: entry.uid,
            }
        });

        const dailyValues = filter(docUpdates.dailies, (d) => {
            return d.value > 0;
        }).map((d) => {
            return d.value;
        });

        if (dailyValues && dailyValues.length) {
            docUpdates = docUpdates.with({
                value: mean(dailyValues)
            });
        }
        LogInfo(`Group Weekly Extracted daily aggregates:`, dailyValues);
    } else {
        LogError(`Invalid value in Group Weekly Sleep aggregator. 
    Value must be a numerical value: `, { value: entry.value })
    }

    return docUpdates;

};
