import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry';

import { LogError, LogInfo } from '../../../../../shared/logger/logger';
import { isNumber, updateWeeklyScalarValues} from '../utils';
import { AthleteWeeklyAggregate } from '../../../models/documents/aggregates';

export const aggregateAthleteWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyAggregate
): AthleteWeeklyAggregate => {

    LogInfo({
        message: `Processing Athlete Weekly aggregate document in Sleep aggregator.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    });
    let docUpdates = aggregateDoc;
    if (isNumber(entry.value)) {
        docUpdates = updateWeeklyScalarValues(entry, docUpdates, 'Sleep', 'Athlete') as AthleteWeeklyAggregate;
    }
    else {
        LogError(`Invalid value in Athlete Weekly Sleep aggregator. Value must be a numerical value:`, { value: entry.value });
    }

    return docUpdates;
};


