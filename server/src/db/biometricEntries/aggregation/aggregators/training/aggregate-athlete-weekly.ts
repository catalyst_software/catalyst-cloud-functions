import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { AthleteWeeklyAggregate } from "../../../models/documents/aggregates";
import { LogInfo } from "../../../../../shared/logger/logger";
import { logDayIndex } from "../utils";
import { processWeeklyTrainingComponents } from "./processWeeklyTrainingComponents";
import { getIsoWeekDay, getWeekFromDate } from "../../../../../shared/utils/moment";
import { createDailies } from "../../../services/utils";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";

export const aggregateAthleteWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyAggregate,
    directive: AggregateDocumentDirective
): AthleteWeeklyAggregate => {

    LogInfo({
        message: `Processing Athlete Weekly aggregate document in Training aggregator.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    });

    let docUpdates = aggregateDoc; 
    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);
    logDayIndex(entry, dayIndex);

    if (!docUpdates.dailies) {
        const dailies = createDailies(entry, aggregateDoc.collection, getWeekFromDate(entry.dateTime, entry.utcOffset), null);

        docUpdates = docUpdates.with({dailies: dailies})
    }

    return processWeeklyTrainingComponents(entry, docUpdates, false, directive, dayIndex) as AthleteWeeklyAggregate;
};
