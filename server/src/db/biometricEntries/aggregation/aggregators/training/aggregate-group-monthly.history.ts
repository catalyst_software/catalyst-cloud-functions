import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { GroupMonthlyHistoryAggregate } from "../../../models/documents/aggregates/group/group-monthly-history-aggregate.document";
import { processMonthlyHistoryTrainingComponents } from "./processMonthlyHistoryTrainingComponents";
import { LogInfo } from "../../../../../shared/logger/logger";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";

export const aggregateGroupMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupMonthlyHistoryAggregate => {

    LogInfo({
        message: `Processing Group Monthly History aggregate document in Training aggregator.`
    });
    debugger;
    return processMonthlyHistoryTrainingComponents(entry, aggregateDoc, true, directive.group);
};
