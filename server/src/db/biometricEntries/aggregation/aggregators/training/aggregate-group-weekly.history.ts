import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { GroupWeeklyHistoryAggregate } from "../../../models/documents/aggregates";
import { LogInfo } from "../../../../../shared/logger/logger";
import { processWeeklyHistoryTrainingComponents } from "./processWeeklyHistoryTrainingComponents";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";

export const aggregateGroupWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyHistoryAggregate => {

    LogInfo({
        message: 'Processing Group Weekly History aggregate document in Training aggregator',
        data: {
            historyDocument: aggregateDoc,
            biometricEntry: entry
        }
    });

    return processWeeklyHistoryTrainingComponents(entry, aggregateDoc, true, directive.group);

};
