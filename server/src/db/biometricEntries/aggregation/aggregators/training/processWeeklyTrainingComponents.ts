import { isArray, cloneDeep, find, sum } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { getGroupAggregate, isNumber } from "../utils";
import { TrainingComponentInterface } from "../../../well/interfaces/training-component";
import { WeeklyAggregateType } from "../../../../../models/types/biometric-aggregate-types";
import { AthleteWeeklyAggregate, GroupWeeklyAggregate } from "../../../models/documents/aggregates";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";
import { AthleteDailyAggregate } from "../../../well/aggregate/interfaces";


export const processWeeklyTrainingComponents = (entry: AthleteBiometricEntryInterface, weeklyDoc: WeeklyAggregateType,
    isGroup: boolean,
    directive: AggregateDocumentDirective,
    dayIndex): WeeklyAggregateType => {

    let docUpdates: AthleteWeeklyAggregate | GroupWeeklyAggregate = weeklyDoc;

    const dailyDoc: AthleteDailyAggregate = weeklyDoc.dailies[dayIndex];

    if (isArray(entry.trainingComponents) && entry.trainingComponents.length > 0) {
        if (!isArray(docUpdates.historicalValues)) {
            docUpdates.with({ historicalValues: [] });
        }
        if (!isArray(docUpdates.painComponents)) {
            docUpdates.with({ painComponents: [] });
        }
        if (!isArray(dailyDoc.historicalValues)) {
            dailyDoc.historicalValues = [];
        }
        if (!isArray(dailyDoc.trainingComponents)) {
            dailyDoc.trainingComponents = [];
        }
        entry.trainingComponents.forEach((c) => {
            if (isNumber(c.minutes)) {
                // Track the pain component itself
                const historicalValues = cloneDeep(docUpdates.historicalValues);
                historicalValues.push(c);
                docUpdates = docUpdates.with({ historicalValues: historicalValues });

                // Find the Daily Pain Component object by type and aggregagte below
                let dailyAggregateComponent = find(dailyDoc.trainingComponents, (tc) => {
                    return c.sportType === tc.sportType && c.intensityLevel === tc.intensityLevel;
                });
                if (!dailyAggregateComponent) {
                    LogInfo({
                        message: `Aggregate daily training component for sport type ${c.sportType} and intensity level type ${c.intensityLevel} not found.  Creating new!`,
                        data: {
                            biometricEntry: entry
                        }
                    });
                    dailyAggregateComponent = {
                        sportType: c.sportType,
                        intensityLevel: c.intensityLevel,
                        historicalMinutes: [],
                        historicalSessions: [],
                        numberOfSessions: 0
                    } as TrainingComponentInterface;

                    const trainingComponents = cloneDeep(dailyDoc.trainingComponents);
                    trainingComponents.push(dailyAggregateComponent);

                    dailyDoc.trainingComponents = trainingComponents;

                    debugger;
                    docUpdates.dailies[dayIndex] = dailyDoc;
                }
                else {

                    LogInfo({
                        message: `Aggregate daily training component for sport type ${c.sportType} and intensity level type ${c.intensityLevel} exists.`,
                        data: {
                            biometricEntry: entry
                        }
                    });
                }

                if (!isArray(dailyAggregateComponent.historicalMinutes)) {
                    dailyAggregateComponent.historicalMinutes = [];
                }

                if (!isArray(dailyAggregateComponent.historicalSessions)) {
                    dailyAggregateComponent.historicalSessions = [];
                }

                let minutes = cloneDeep(dailyAggregateComponent.historicalMinutes);
                minutes.push(c.minutes);
                dailyAggregateComponent.historicalMinutes = minutes;

                const sessions = cloneDeep(dailyAggregateComponent.historicalSessions);
                sessions.push(c.numberOfSessions || 1);
                dailyAggregateComponent.historicalSessions = sessions;

                if (isGroup) {
                    dailyAggregateComponent.minutes = getGroupAggregate(directive.group, dailyAggregateComponent.historicalMinutes, true);
                    dailyAggregateComponent.numberOfSessions = getGroupAggregate(directive.group, dailyAggregateComponent.historicalSessions, true);
                }
                else {
                    dailyAggregateComponent.minutes = sum(dailyAggregateComponent.historicalMinutes);
                    dailyAggregateComponent.numberOfSessions = sum(dailyAggregateComponent.historicalSessions);
                }

                let weeklyAggregateComponent = find(docUpdates.trainingComponents, (tc) => {
                    return c.sportType === tc.sportType && c.intensityLevel === tc.intensityLevel;
                });

                if (!weeklyAggregateComponent) {
                    LogInfo({
                        message: `Aggregate weekly training component for sport type ${c.sportType} and intensity level type ${c.intensityLevel} not found.  Creating new!`,
                        data: {
                            // historyDocument: aggregateDoc,
                            biometricEntry: entry,
                            dailyDoc,
                            docUpdates
                        }
                    });
                    weeklyAggregateComponent = {
                        sportType: c.sportType,
                        intensityLevel: c.intensityLevel,
                        historicalMinutes: []
                    } as TrainingComponentInterface;

                    const weeklyTrainingComponents = cloneDeep(docUpdates.trainingComponents);
                    weeklyTrainingComponents.push(weeklyAggregateComponent);
                }
                else {
                    LogInfo({
                        message: `Aggregate weekly training component for sport type ${c.sportType} and intensity level type ${c.intensityLevel} exists.`,
                        data: {
                            biometricEntry: entry
                        }
                    });
                }

                minutes = cloneDeep(weeklyAggregateComponent.historicalMinutes);
                minutes.push(c.minutes);
                weeklyAggregateComponent.historicalMinutes = minutes;
                if (isGroup)
                    weeklyAggregateComponent.minutes = getGroupAggregate(directive.group, dailyAggregateComponent.historicalMinutes, true);
                else
                    weeklyAggregateComponent.minutes = sum(weeklyAggregateComponent.historicalMinutes);

            }
            else {
                LogError(`Invalid value in Athlete Weekly Training aggregator. 
                    Minutes must be a numerical value:`, { minutes: c.minutes });
            }
        });
    }
    else {
        LogError(`Invalid values in Athlete Weekly Training aggregator. 
            Training components array is empty or null.`);
    }
    return docUpdates;
};