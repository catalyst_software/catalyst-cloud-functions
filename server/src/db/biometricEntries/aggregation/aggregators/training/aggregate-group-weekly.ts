import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogInfo } from "../../../../../shared/logger/logger";
import { processWeeklyTrainingComponents } from "./processWeeklyTrainingComponents";
import { getIsoWeekDay } from "../../../../../shared/utils/moment";
import { GroupWeeklyAggregate } from "../../../models/documents/aggregates";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";

export const aggregateGroupWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyAggregate => {
    LogInfo({
        message: `Processing Group Weekly aggregate document in Training aggregator.`,
        data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    });
    const dayIndex = getIsoWeekDay(entry.dateTime, entry.utcOffset);

    LogInfo({
        message: `Entry date: ${entry.dateTime.toDate()}.  Day index: ${dayIndex}.`,
        data: {
            dayIndex,
            document: aggregateDoc,
            biometricEntryId: entry.uid
        }
    });
    debugger;

    return processWeeklyTrainingComponents(entry, aggregateDoc, true, directive, dayIndex) as GroupWeeklyAggregate;
};


