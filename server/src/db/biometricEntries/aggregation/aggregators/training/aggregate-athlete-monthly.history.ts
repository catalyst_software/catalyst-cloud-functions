
import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { AthleteMonthlyHistoryAggregate } from "../../../models/documents/aggregates/athlete/athelete-monthly-history-aggregate.document";

import { LogInfo } from "../../../../../shared/logger/logger";
import { processMonthlyHistoryTrainingComponents } from "./processMonthlyHistoryTrainingComponents";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";


export const aggregateAthleteMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): AthleteMonthlyHistoryAggregate => {

    LogInfo({
        message: `Processing Athlete Monthly History aggregate document in Training aggregator.`,
        data: { historyDocument: aggregateDoc, biometricEntryId: entry.uid }
    });

    return processMonthlyHistoryTrainingComponents(entry, aggregateDoc, false) as AthleteMonthlyHistoryAggregate;
};
