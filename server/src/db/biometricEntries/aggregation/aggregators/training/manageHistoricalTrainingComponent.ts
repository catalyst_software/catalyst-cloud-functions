import { cloneDeep, find, sum, isArray } from "lodash";

import { TrainingComponentInterface } from '../../../well/interfaces/training-component';

import { AggregateComponentResponse } from '../../../well/interfaces/aggregate-component-response';
import { Group } from '../../../../../models/group/group.model';
import { getGroupAggregate } from '../utils';

export const manageHistoricalTrainingComponent = (entryTrainingComponent: TrainingComponentInterface, componentArray: any, 
        isGroup: boolean, group?: Group): AggregateComponentResponse => {

    const newArray = cloneDeep(componentArray);

    let component: TrainingComponentInterface = find(newArray, (c: TrainingComponentInterface) => {
        return entryTrainingComponent.sportType === c.sportType && entryTrainingComponent.intensityLevel === c.intensityLevel;
    });
    if (!component) {
        component = {
            sportType: entryTrainingComponent.sportType,
            intensityLevel: entryTrainingComponent.intensityLevel,
            historicalMinutes: [],
            historicalSessions: [],
            numberOfSessions: 0
        } as TrainingComponentInterface;
        newArray.push(component);
    } 

    if (!isArray(component.historicalMinutes)) {
        component.historicalMinutes = [];
    }

    if (!isArray(component.historicalSessions)) {
        component.historicalSessions = [];
    }
    
    const minutes = cloneDeep(component.historicalMinutes);
    minutes.push(+entryTrainingComponent.minutes);
    component.historicalMinutes = minutes;

    const sessions = cloneDeep(component.historicalSessions);
    sessions.push(+entryTrainingComponent.numberOfSessions || 1);
    component.historicalSessions = sessions;

    if (isGroup) {
        component.minutes = getGroupAggregate(group, component.historicalMinutes, true);
        component.numberOfSessions = getGroupAggregate(group, component.historicalSessions, true);
    } else {
        component.minutes = sum(component.historicalMinutes);
        component.numberOfSessions = sum(component.historicalSessions);
    }

    return {
        component,
        componentArray: newArray
    };
};