import { isArray, cloneDeep } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";
import { getWeekNumber } from "../../../../../shared/utils/moment";
import { TrainingComponentInterface } from "../../../well/interfaces/training-component";
import { manageHistoricalTrainingComponent } from "./manageHistoricalTrainingComponent";
import { GroupWeeklyHistoryAggregate } from "../../../models/documents/aggregates";
import { Group } from "../../../../../models/group/group.model";


export const processWeeklyHistoryTrainingComponents = (entry: AthleteBiometricEntryInterface, 
        aggregateDoc: GroupWeeklyHistoryAggregate,
        isGroup: boolean,
        group?: Group): GroupWeeklyHistoryAggregate => {

    let docUpdates = aggregateDoc as any;
    if (isArray(entry.trainingComponents) && entry.trainingComponents.length > 0) {
        if (!isArray(docUpdates.trainingComponentHistory)) {
            docUpdates.trainingComponentHistory = [];
        }
        if (!docUpdates.trackingCache) {
            docUpdates.trackingCache = {
                index: 0,
                valueCache: [],
                componentCache: {
                    index: 0,
                    components: []
                }
            };
        }
        const weekNumber = getWeekNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);
        LogInfo({
            message: `Calculated week number for AthleteWeekly History ${weekNumber}.`,
            data: {
                historyDocument: docUpdates,
                biometricEntry: entry
            }
        });
        if (docUpdates.trackingCache.index !== weekNumber) {
            docUpdates.trackingCache.index = weekNumber;
            docUpdates.trackingCache.componentCache = {
                index: weekNumber,
                components: []
            };
        }

        if (!docUpdates.trainingComponentHistory) {
            docUpdates = docUpdates.with({ trainingComponentHistory: [] });
        }

        if (docUpdates.trainingComponentHistory.length <= weekNumber) {
            const existing = cloneDeep(docUpdates.trainingComponentHistory);
            for (let i = docUpdates.trainingComponentHistory.length; i <= weekNumber; i++) {
                existing.unshift({ index: i, components: [] });
            }

            docUpdates = docUpdates.with({trainingComponentHistory: existing});
        }

        entry.trainingComponents.forEach((c) => {
            if (isNumber(c.minutes)) {

                let response;
                if (isGroup) {
                    response = manageHistoricalTrainingComponent(c, docUpdates.trackingCache.componentCache.components, true, group);
                } else {
                    response = manageHistoricalTrainingComponent(c, docUpdates.trackingCache.componentCache.components, false);
                }
                docUpdates.trackingCache.componentCache.components = response.componentArray;

                let cacheComponent = response.component as TrainingComponentInterface;

                LogInfo({
                    message: `Processed component cache in Training aggregator1.`,
                    data: {
                        // historyDocument: docUpdates,
                        biometricEntry: JSON.stringify(entry),
                        cacheComponent,
                        componentCacheArray: docUpdates.trackingCache.componentCache,
                    }
                });
                if (isGroup) {
                    response = manageHistoricalTrainingComponent(c, docUpdates.trainingComponentHistory[0].components, true, group);
                } else {
                    response = manageHistoricalTrainingComponent(c, docUpdates.trainingComponentHistory[0].components, false);
                }
                
                docUpdates.trainingComponentHistory[0].components = response.componentArray;
                cacheComponent = response.component as TrainingComponentInterface;

                LogInfo({
                    message: `Processed pain component history in Training aggregator.`,
                    data: {
                        // historyDocument: docUpdates,
                        biometricEntry: entry,
                        cacheComponent,
                        componentCacheArray: docUpdates.trainingComponentHistory[0],
                    }
                });
            }
            else {
                LogError(`Invalid value in Weekly Training aggregator. 
                Minutes must be a numerical value:`, { minutes: c.minutes });
            }
        });
    }
    else {
        LogError(`Invalid values in Weekly Training aggregator. 
        Training components array is empty or null.`);
    }
    
    return docUpdates;
};