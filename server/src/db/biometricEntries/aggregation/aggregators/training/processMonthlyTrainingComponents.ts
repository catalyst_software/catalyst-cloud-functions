import { isArray, find, mean } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";
import { TrainingComponentInterface } from "../../../well/interfaces/training-component";

export const processMonthlyTrainingComponents = (entry: AthleteBiometricEntryInterface, monthlyDoc: any) => {
    debugger;
    const docUpdates = monthlyDoc;
    debugger;
    if (isArray(entry.trainingComponents) && entry.trainingComponents.length > 0) {
        if (!isArray(docUpdates.historicalValues)) {
            docUpdates.with({ historicalValues: [] });
        }
        if (!isArray(docUpdates.trainingComponents)) {
            docUpdates.with({ trainingComponents: [] });
        }
        entry.trainingComponents.forEach((c) => {
            if (isNumber(c.minutes)) {
                // Track the pain component itself
                docUpdates.historicalValues.push(c);
                // Find the Daily Training Component object by type and aggregagte below
                let aggregateComponent = find(docUpdates.trainingComponents, (tc) => {
                    return c.sportType === tc.sportType && c.intensityLevel === tc.intensityLevel;
                });
                if (!aggregateComponent) {
                    LogInfo({
                        message: `Aggregate monthly training component for sport type ${c.sportType} and intensity level type ${c.intensityLevel} not found.  Creating new!`,
                        data: {
                            biometricEntry: entry,
                        }
                    });
                    aggregateComponent = {
                        sportType: c.sportType,
                        intensityLevel: c.intensityLevel,
                        historicalMinutes: []
                    } as TrainingComponentInterface;
                    docUpdates.trainingComponents.push(aggregateComponent);
                }
                else {
                    LogInfo({
                        message: `Aggregate monthly training component for sport type ${c.sportType} and intensity level type ${c.intensityLevel} exists.`,
                        data: {
                            biometricEntry: entry
                        }
                    });
                }
                aggregateComponent.historicalMinutes.push(+c.minutes);
                aggregateComponent.minutes = mean(aggregateComponent.historicalMinutes);
            }
            else {
                LogError(`Invalid value in Athlete Monthly Training aggregator. 
                Minutes must be a numerical value:`, { minutes: +c.minutes });
            }
        });
    }
    else {
        LogError(`Invalid values in Athlete Monthly Training aggregator. 
        Training components array is empty or null.`);
    }
    return docUpdates;
};
