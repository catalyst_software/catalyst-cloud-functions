import { isArray, cloneDeep } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { AthleteMonthlyHistoryAggregate } from "../../../models/documents/aggregates/athlete/athelete-monthly-history-aggregate.document";
import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber } from "../utils";
import { getMonthNumber } from "../../../../../shared/utils/moment";
import { TrainingComponentInterface } from '../../../well/interfaces/training-component';
import { manageHistoricalTrainingComponent } from "./manageHistoricalTrainingComponent";
import { GroupMonthlyHistoryAggregate } from "../../../models/documents/aggregates/group/group-monthly-history-aggregate.document";
import { Group } from "../../../../../models/group/group.model";

export const processMonthlyHistoryTrainingComponents = (entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteMonthlyHistoryAggregate | GroupMonthlyHistoryAggregate,
    isGroup: boolean,
    group?: Group) => {

    let docUpdates = aggregateDoc;
    if (isArray(entry.trainingComponents) && entry.trainingComponents.length > 0) {
        if (!isArray(docUpdates.trainingComponentHistory)) {
            docUpdates.trainingComponentHistory = [];
        }
        if (!docUpdates.trackingCache) {
            docUpdates.trackingCache = {
                index: 0,
                valueCache: [],
                componentCache: {
                    index: 0,
                    components: []
                }
            };
        }
        const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset);
        LogInfo({
            message: `Calculated month number for Athlete Monthly History ${monthNumber}.`
        });
        if (docUpdates.trackingCache.index !== monthNumber) {
            docUpdates.trackingCache.index = monthNumber;
            docUpdates.trackingCache.componentCache = {
                index: monthNumber,
                components: []
            };
        }

        if (!docUpdates.trainingComponentHistory) {
            docUpdates = docUpdates.with({ trainingComponentHistory: [] });
        }
        
        if (docUpdates.trainingComponentHistory.length <= monthNumber) {
            const existing = cloneDeep(docUpdates.trainingComponentHistory);
            for (let i = docUpdates.trainingComponentHistory.length; i <= monthNumber; i++) {
                existing.unshift({ index: i, components: [] });
            }

            docUpdates = docUpdates.with({ trainingComponentHistory: existing });
        }

        entry.trainingComponents.forEach((c) => {
            if (isNumber(+c.minutes)) {
                let response;
                if (isGroup) {
                    response = manageHistoricalTrainingComponent(c, docUpdates.trackingCache.componentCache.components, true, group);
                } else {
                    response = manageHistoricalTrainingComponent(c, docUpdates.trackingCache.componentCache.components, false);
                }
                docUpdates.trackingCache.componentCache.components = response.componentArray;

                let cacheComponent = response.component as TrainingComponentInterface;

                LogInfo({
                    message: `Processed component cache in Training aggregator2.`,
                    data: {
                        biometricEntry: JSON.stringify(entry),
                        cacheComponent,
                        componentCacheArray: docUpdates.trackingCache.componentCache,
                    }
                });
                if (isGroup) {
                    response = manageHistoricalTrainingComponent(c, docUpdates.trainingComponentHistory[0].components, true, group);
                } else {
                    response = manageHistoricalTrainingComponent(c, docUpdates.trainingComponentHistory[0].components, false);
                }

                docUpdates.trainingComponentHistory[0].components = response.componentArray;
                cacheComponent = response.component as TrainingComponentInterface;

                LogInfo({
                    message: `Processed pain component history in Training aggregator.`,
                    data: {
                        biometricEntry: entry,
                        cacheComponent,
                        componentCacheArray: docUpdates.trainingComponentHistory[0],
                    }
                });
            }
            else {
                LogError(`Invalid value in Monthly Training aggregator. 
                Minutes must be a numerical value:`, { minutes: c.minutes });
            }
        });
    }
    else {
        LogError(`Invalid values in Monthly Training aggregator. 
        Training components array is empty or null.`);
    }

    return docUpdates;
};