
import { CalculateAggregates } from "../../interfaces/calculate-aggregates";

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry';

import { AthleteMonthlyHistoryAggregate } from "../../../models/documents/aggregates/athlete/athelete-monthly-history-aggregate.document";
import { aggregateAthleteWeekly } from "./aggregate-athlete-weekly";
import { aggregateAthleteWeeklyHistory } from "./aggregate-athlete-weekly.history";
import { aggregateAthleteMonthlyHistory } from "./aggregate-athlete-monthly.history";
import { aggregateGroupWeekly } from "./aggregate-group-weekly";
import { aggregateGroupWeeklyHistory } from "./aggregate-group-weekly.history";
import { aggregateGroupMonthlyHistory } from "./aggregate-group-monthly.history";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";
import { AthleteWeeklyAggregate, AthleteWeeklyHistoryAggregate, GroupWeeklyAggregate, GroupWeeklyHistoryAggregate } from "../../../models/documents/aggregates";
import { GroupMonthlyHistoryAggregate } from "../../../models/documents/aggregates/group/group-monthly-history-aggregate.document";

/**
* Still working on this // function(current Document, collection of aggregates)
* // function(current Document, collection of aggregates)
* @param biometricEntry: AthleteBiometricEntry
* @param aggregateDoc: BiometricAggregate
*/
export class TrainingAggregator implements CalculateAggregates {

    // Athlete Weekly
    public processAthleteWeekly(entry: AthleteBiometricEntryInterface, aggregateDoc: AthleteWeeklyAggregate, directive: AggregateDocumentDirective
    ): AthleteWeeklyAggregate {
        return aggregateAthleteWeekly(entry, aggregateDoc, directive);
    }

    public processAthleteWeeklyHistory(entry: AthleteBiometricEntryInterface, aggregateDoc: AthleteWeeklyHistoryAggregate, directive: AggregateDocumentDirective
    ): AthleteWeeklyHistoryAggregate {
        return aggregateAthleteWeeklyHistory(entry, aggregateDoc, directive);
    }



    public processAthleteMonthlyHistory(entry: AthleteBiometricEntryInterface, aggregateDoc: AthleteMonthlyHistoryAggregate, directive: AggregateDocumentDirective
    ): AthleteMonthlyHistoryAggregate {
        return aggregateAthleteMonthlyHistory(entry, aggregateDoc, directive);
    }

    // Group Weekly
    public processGroupWeekly(entry: AthleteBiometricEntryInterface, aggregateDoc: GroupWeeklyAggregate, directive: AggregateDocumentDirective
    ): GroupWeeklyAggregate {
        return aggregateGroupWeekly(entry, aggregateDoc, directive);
    }

    public processGroupWeeklyHistory(entry: AthleteBiometricEntryInterface, aggregateDoc: GroupWeeklyHistoryAggregate, directive: AggregateDocumentDirective
    ): GroupWeeklyHistoryAggregate {
        return aggregateGroupWeeklyHistory(entry, aggregateDoc, directive);
    }


    public processGroupMonthlyHistory(entry: AthleteBiometricEntryInterface, aggregateDoc: GroupMonthlyHistoryAggregate, directive: AggregateDocumentDirective
    ): GroupMonthlyHistoryAggregate {
        return aggregateGroupMonthlyHistory(entry, aggregateDoc, directive);
    }
}
