import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry"

import { LogInfo } from "../../../../../shared/logger/logger";
import { processWeeklyHistoryTrainingComponents } from "./processWeeklyHistoryTrainingComponents";
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive";
import { AthleteWeeklyHistoryAggregate } from "../../../models/documents/aggregates";

export const aggregateAthleteWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): AthleteWeeklyHistoryAggregate => {

    LogInfo({
        message: 'Processing Athlete Weekly history aggregate document in Training aggregator',
        data: {
            historyDocument: aggregateDoc,
            biometricEntry: entry
        }
    });

    return processWeeklyHistoryTrainingComponents(entry, aggregateDoc, false) as AthleteWeeklyHistoryAggregate;
};
