import { isNumber } from "./utils"
import { sum } from "lodash"

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry"
import { LogError, LogInfo } from "../../../../../shared/logger/logger"
import { getMonthNumber } from "../../../../../shared/utils/moment"
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive"
import { GroupMonthlyHistoryAggregate } from "../../../models/documents/aggregates/group/group-monthly-history-aggregate.document"
import { handleScalarHistoryUpdates } from "../utils/handle-scalar-history-updates"

export const aggregateGroupMonthlyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupMonthlyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupMonthlyHistoryAggregate => {

    debugger;
    
    LogInfo({
        message: `Processing Group Monthly History document in Distance aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    const monthNumber = getMonthNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset)
    LogInfo({
        message: `Calculated month number for GroupMonthly History ${monthNumber}.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    let updates = aggregateDoc
    if (isNumber(entry.value)) {
        debugger;
        updates = handleScalarHistoryUpdates(entry, aggregateDoc, monthNumber)
        updates.values[0] = updates.trackingCache ? sum(updates.trackingCache.valueCache) : 0
    } else {
        LogError(`Invalid value in Group Monthly Distance aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return updates
}

