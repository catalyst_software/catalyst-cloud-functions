import { isNumber } from "./utils"
import { sum } from "lodash"

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry"
import { LogError, LogInfo } from "../../../../../shared/logger/logger"
import { AggregateDocumentDirective } from "../../../services/interfaces/aggregate-document-directive"
import { getWeekNumber } from "../../../../../shared/utils/moment"
import { GroupWeeklyHistoryAggregate } from "../../../models/documents/aggregates"
import { handleScalarHistoryUpdates } from "../utils/handle-scalar-history-updates"

export const aggregateGroupWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: GroupWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): GroupWeeklyHistoryAggregate => {

    LogInfo({
        message: `Processing GroupWeekly History aggregate document in Sleep aggregator.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })

    const weekNumber = getWeekNumber(entry.athleteCreationTimestamp, entry.dateTime, entry.utcOffset)
    LogInfo({
        message: `Calculated week number for GroupWeekly History ${weekNumber}.`,
        data: {
            AggregateDocument: aggregateDoc,
            BiometricEntity: entry
        }
    })
    let updates: any = aggregateDoc
    if (isNumber(entry.value)) {
        updates = handleScalarHistoryUpdates(entry, aggregateDoc, weekNumber)
        updates.values[0] = updates.trackingCache ? sum(updates.trackingCache.valueCache) : 0
    } else {
        LogError(`Invalid value in Group Weekly History Distance aggregator. 
        Value must be a numerical value:`, { value: entry.value })
    }

    return updates
}

