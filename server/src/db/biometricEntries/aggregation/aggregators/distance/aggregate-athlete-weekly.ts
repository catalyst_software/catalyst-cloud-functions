import moment from "moment";
import { cloneDeep, sum, filter } from "lodash";

import { AthleteBiometricEntryInterface } from "../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";

import { LogError, LogInfo } from "../../../../../shared/logger/logger";
import { isNumber, logDayIndex } from "./utils";
import { GroupDailyAggregate } from "../../../well/aggregate/interfaces";
import { createDailies } from "../../../services/utils";
import { getWeekFromDate } from "../../../../../shared/utils/moment";
import { AthleteWeeklyAggregate } from "../../../models/documents/aggregates";


export const aggregateAthleteWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyAggregate
): AthleteWeeklyAggregate => {

    LogInfo(`Processing AthleteWeekly aggregate document in Distance aggregator.`, {
        AggregateDocument: aggregateDoc,
        BiometricEntity: entry
    });

    const dayIndex = moment(entry.dateTime.toDate()).isoWeekday() - 1;
    logDayIndex(entry, dayIndex);
    
    let docUpdates = aggregateDoc;

    if (!docUpdates.dailies) {
        const dailies = createDailies(entry, aggregateDoc.collection, getWeekFromDate(entry.dateTime, entry.utcOffset), 
        null);

        docUpdates = docUpdates.with({dailies: dailies})
    }
    const dailyAggregate: GroupDailyAggregate = docUpdates.dailies[dayIndex];
 

    if (isNumber(entry.value)) {

        LogInfo(`Athlete Weekly Daily aggregate before update:`, dailyAggregate);

        const val = +entry.value;

        if (dailyAggregate) {
            const historicalValues = cloneDeep(dailyAggregate.historicalValues);
            historicalValues.push(val);

            dailyAggregate.historicalValues = historicalValues;
            dailyAggregate.value = sum(dailyAggregate.historicalValues);
        }

        const dailyValues = filter(docUpdates.dailies, (d) => {
            return d.value > 0;
        }).map((d) => {
            return d.value;
        });
        if (dailyValues && dailyValues.length) {
            docUpdates =  docUpdates.with({
                value: sum(dailyValues)
            });
        }

        LogInfo(`Athlete weekly aggregate after update:`, dailyAggregate)

    } else {
        LogError(`Invalid value in Distance aggregator.  Value must be a numerica value: ${entry.value}.`)
    }

    return docUpdates;
};

