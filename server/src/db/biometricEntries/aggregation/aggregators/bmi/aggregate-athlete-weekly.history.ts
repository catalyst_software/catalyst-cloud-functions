import { mean } from 'lodash'

import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { getWeekNumber } from '../../../../../shared/utils/moment'
import { AthleteWeeklyHistoryAggregate } from '../../../models/documents/aggregates'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive'
import { isNumber } from '../utils'
// import { AthleteBiometricEntry } from '../../../well/interfaces/athelete-biometric-entry';
import { handleScalarHistoryUpdates } from '../utils/handle-scalar-history-updates'

export const aggregateAthleteWeeklyHistory = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyHistoryAggregate,
    directive: AggregateDocumentDirective
): AthleteWeeklyHistoryAggregate => {
    const weekNumber = getWeekNumber(
        entry.athleteCreationTimestamp,
        entry.dateTime,
        entry.utcOffset
    )

    //  LogInfo({
    //     message: `Processing Athlete Weekly History aggregate document in Sleep aggregator.`,
    //     data: { historyDocument: aggregateDoc, biometricEntryId: entry.uid }
    // })

    let updates = aggregateDoc
    if (isNumber(entry.value)) {
        updates = handleScalarHistoryUpdates(
            entry,
            aggregateDoc,
            weekNumber
        ) as AthleteWeeklyHistoryAggregate
        updates.values[0] = mean(updates.trackingCache.valueCache)
    } else {
        // LogError(`Invalid value in Athlete Weekly Sleep aggregator.
        // Value must be a numerical value:`, { value: entry.value })
    }

    return updates
}
