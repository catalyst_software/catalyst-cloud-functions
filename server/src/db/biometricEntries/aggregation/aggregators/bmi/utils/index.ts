import { AthleteBiometricEntryInterface } from "../../../../well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { LogInfo } from '../../../../../../shared/logger/logger';

export const logDayIndex = (entry: AthleteBiometricEntryInterface, dayIndex: number, message?: string) => {
    LogInfo(message, {
        EntryDate: entry.dateTime.toDate(),
        DayIndex: dayIndex
    });
};

export const isNumber = (val: any) => {
    return !isNaN(parseInt(val));
};
