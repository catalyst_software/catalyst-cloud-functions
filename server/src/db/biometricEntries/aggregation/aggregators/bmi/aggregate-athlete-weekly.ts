import { AthleteBiometricEntryInterface } from '../../../well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { AthleteWeeklyAggregate } from '../../../models/documents/aggregates'
import { AggregateDocumentDirective } from '../../../services/interfaces/aggregate-document-directive'
// import { AthleteBiometricEntry } from '../../../well/interfaces/athelete-biometric-entry';
import { isNumber, updateWeeklyScalarValues } from '../utils'

export const aggregateAthleteWeekly = (
    entry: AthleteBiometricEntryInterface,
    aggregateDoc: AthleteWeeklyAggregate,
    directive: AggregateDocumentDirective
): AthleteWeeklyAggregate => {
    // LogInfo({
    //     message: `Processing Athlete Weekly aggregate document in Sleep aggregator.`,
    //     data: { historyDocument: { uid: aggregateDoc.uid }, biometricEntryId: entry.uid }
    // });
    let docUpdates = aggregateDoc
    if (isNumber(entry.value)) {
        docUpdates = updateWeeklyScalarValues(
            entry,
            docUpdates,
            'BodyMassIndex',
            'Athlete'
        ) as AthleteWeeklyAggregate
    } else {
        // LogError(`Invalid value in Athlete Weekly Sleep aggregator. Value must be a numerical value:`, { value: entry.value });
    }

    return docUpdates
}
