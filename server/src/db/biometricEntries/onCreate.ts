import moment from 'moment';
import momentTZ from 'moment-timezone';
import { isNumber } from 'lodash';

// import { Observable } from 'rxjs';
import * as functions from 'firebase-functions';
// import firestore = require('@google-cloud/firestore')
import { firestore } from 'firebase-admin';
import { DocumentSnapshot } from 'firebase-functions/lib/providers/firestore';
// const momentTZ = require('moment-timezone');
// Interfaces
// import { AggregateDocumentDirective } from './services/interfaces/aggregate-document-directive';
// Enums
import { FirestoreCollection } from './enums/firestore-collections';

import { AthleteBiometricEntryModel } from './well/athelete-biometric-entry.model';

import {
    // aggregateDocumentManager,
    AggregateDocumentManager
} from './managers/aggregate-document-manager';
import { DocMngrActionTypes } from './enums/document-manager-action-types';
import { LogInfo } from '../../shared/logger/logger';
import { getUserByID } from "../../api/app/v1/controllers/get-user-by-id";
import { db } from '../../shared/init/initialise-firebase';
import { Config } from './../../shared/init/config/config';


exports.updateBiometricEntries = functions.firestore
    ._documentWithOptions(`${FirestoreCollection.Athletes}/{userId}/${FirestoreCollection.AthleteBiometricEntries}/{biometricEntryId}`, {regions: Config.regions})
    .onCreate(async (
        aggregateSnapshot: DocumentSnapshot, context) => {

        try {
            const biometricEntry = <AthleteBiometricEntryModel>aggregateSnapshot.data();

            const { userId, uid } = biometricEntry;
            !uid ? biometricEntry.uid = userId : uid;

            if (isNumber(biometricEntry.value)) {
                biometricEntry.value = +biometricEntry.value;
            }



            if (!biometricEntry.athleteCreationTimestamp && !biometricEntry.creationTimestamp) {
                const user = await getUserByID(biometricEntry.userId)
                biometricEntry.athleteCreationTimestamp =
                    firestore.Timestamp.fromDate(new Date(user.metadata.creationTime))
            } else {
                biometricEntry.athleteCreationTimestamp = biometricEntry.creationTimestamp
            }

            if (!biometricEntry.utcOffset) {
                biometricEntry.utcOffset = 0
            }

            const timezone = momentTZ.tz.guess();
            console.warn('Server timezone ======================>', timezone);

            // testTZ(biometricEntry);

            if (biometricEntry.skipAggregate) {
                LogInfo('Skipping Well Aggregation as skipAggregate is set', 'onCreate() executed');

                // const aggregateDocumentManager = new AggregateDocumentManager();
                // const res = await aggregateDocumentManager.doReadNew(biometricEntry, DocMngrActionTypes.Read, 'onCreate', logLevel)

                return true
            } else if (biometricEntry.athleteCreationTimestamp) {
                const { apiHandlesDoRead } = aggregateSnapshot.data();
                if (apiHandlesDoRead) {
                    LogInfo('Aggregate Update handled by apiDev !!', 'onCreate() executed');
                    return Promise.resolve('updateBiometricEntries.onCreate().doRead(biometricEntry) executed WOOT Promise!');
                } else {
                    // dev -- set dynamic logging level :)
                    const { logLevel } = aggregateSnapshot.data();
                    LogInfo('Aggregate Update handled by onCreate !!', 'onCreate() executed');
                    const aggregateDocumentManager = new AggregateDocumentManager();

                    debugger;
                    // TODO: CHEKC TRANSACTION

                    let transactionResult = await db.runTransaction(async (t: FirebaseFirestore.Transaction) => {

                        let transaction: FirebaseFirestore.Transaction
                        let useTRansaction = false

                        if (useTRansaction) {
                            transaction = t
                        }

                        const allResults = await aggregateDocumentManager.doRead(biometricEntry, biometricEntry.userId, DocMngrActionTypes.Read, 'onCreate', logLevel, transaction)

                            .then((docDirective) => {
                                LogInfo('OnCreate Promise Received, aggregation all done!!!!!!!!', docDirective);
                                return Promise.resolve(docDirective);
                            })
                            .catch((err) => {
                                debugger;

                                LogInfo(`updateBiometricEntries.onCreate() execution failed! - err -> ${err}`);
                                return Promise.reject(
                                    `updateBiometricEntries.onCreate() execution failed! - err -> ${err}`
                                )

                            });

                        return allResults


                    }).then((results) => {
                        console.log('Transaction success!', results);
                        return true;
                    }).catch(err => {
                        console.log('Transaction failure:', err);
                        return false
                    });

                    return transactionResult


                }
            } else {
                console.error(`updateBiometricEntries.onCreate() execution failed! - err -> biometricEntry.creationTimestamp === undefined`);
                return Promise.reject(
                    `updateBiometricEntries.onCreate() execution failed! - err -> biometricEntry.creationTimestamp === undefined`
                )
            }


        } catch (err) {

            LogInfo(`updateBiometricEntries.onCreate() execution failed! - TRY/CATCH err -> ${err}`);

            return Promise.reject(
                `updateBiometricEntries.onCreate() execution failed! - TRY/CATCH err -> ${err}`
            )
        }
    });



function testTZ(biometricEntry: AthleteBiometricEntryModel) {
    momentTZ.tz(biometricEntry.dateTime, "America/Los_Angeles").format(); // 2014-06-22T09:21:08-07:00
    console.log('biometricEntry.dateTime', biometricEntry.dateTime.toDate());
    console.warn('biometricEntry.dateTime.toDate().getTimezoneOffset()', biometricEntry.dateTime.toDate().getTimezoneOffset());
    console.log('biometricEntry.dateTime.toDate().getUTCDate()', biometricEntry.dateTime.toDate().getUTCDate());
    console.log(' moment(biometricEntry.dateTime.toDate())', moment(biometricEntry.dateTime.toDate()));
    const date = biometricEntry.dateTime.toDate(); //.format('YYYY-MM-DD HH:mm:ss'))
    const dateOffset = moment(biometricEntry.dateTime.toDate())
        .utcOffset(biometricEntry.utcOffset)
        .toDate(); //.format('YYYY-MM-DD HH:mm:ss'))
    // console.log('dateTime', moment(date))
    console.log(`dateTime with utcOffset of ${biometricEntry.utcOffset}`, dateOffset);
    const dateTimestampDate = firestore.Timestamp.fromDate(date).toDate();
    const dateOffsetTimestampDate = firestore.Timestamp.fromDate(dateOffset).toDate();
    console.warn('dateTimestampDate', dateTimestampDate);
    console.warn(`dateOffsetTimestampDate`, dateOffsetTimestampDate);
}
// exports.createAthleteHistoryAggregates = functions.firestore.document(`${FirestoreCollection.Athlete}/{athleteId}/`)

//     .onCreate((
//         aggregateSnapshot: DocumentSnapshot,
//         // context: functions.EventContext
//     ) => {
//         // process.on('warning', e => e)
//         // process.setMaxListeners(100)
//         console.log('createHistoryAggregates!!!!!!!!!!!!!!!!!!!!!!!--------------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.onCreate().process.listeners.length', process.listeners.length)
//         try {
//             const athlete: Athlete = <Athlete>aggregateSnapshot.data()

//             athlete.uid = aggregateSnapshot.id;

//             // const userId = context.params.userId;

//             console.log(athlete);
//             const aggregateDocumentManager = new AggregateDocumentManager();
//             return aggregateDocumentManager.doRead(athlete, DocMngrActionTypes.CreateHistoryDocs, 'createHistoryAggregates.onCreate()', 'info').then((docDirective) => {
//                 console.log('OnCreate Promise Received', docDirective)
//                 return docDirective;
//             }).catch((err) => {
//                 debugger
//             });;

//             console.log('greate Athlete HistoryAggregates.onCreate().doRead(biometricEntry) executed');


//             // return Promise.resolve('greate Athlete HistoryAggregates.onCreate().doRead(biometricEntry) executed WOOT Promise!');
//         } catch (err) {

//             return Promise.reject('onCreate().doRead(biometricEntry) execution failed ARGH Promise!');
//         }
//     });


