

export interface Report {
    uid?: string;
    strategyGuid?: string;
    docGuid?: string;
    fileGuid?: string;
    title?: string;
    version?: number;
    header?: string;
    footer?: string;
}
