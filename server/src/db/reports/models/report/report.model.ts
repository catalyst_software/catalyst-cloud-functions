
import { Report as ReportInterface } from '../interfaces/report';


export class Report implements ReportInterface {
    uid?: string;
    strategyGuid?: string;
    docGuid?: string;
    fileGuid?: string;
    title?: string;
    version?: number;
    
    header?: string;
    footer?: string;
    
    constructor(props: ReportInterface) {
        // super(props);
    }
}