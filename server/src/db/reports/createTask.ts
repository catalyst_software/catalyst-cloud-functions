'use strict';

import { removeUndefinedProps } from "../ipscore/services/save-aggregates";
import cloudTasks from '@google-cloud/tasks'
/**
 * Create a task for a given queue with an arbitrary payload.
 */
export async function createTask(project, location, queue, options) {

  // const cloudTasks = require('@google-cloud/tasks');

  console.log('cloudTasks required');

  // Instantiates a client.
  const client = new cloudTasks.CloudTasksClient();

  // Construct the fully qualified queue name.
  const parent = client.queuePath(project, location, queue);

  console.log('options.payload', options.payload);

  const task = {
    appEngineHttpRequest: {
      httpMethod: 'POST',
      relativeUri: '/video_convert_request',
      appEngineRouting: {
        // service: 'video-convert'
        service: 'convert-video-queue'
      },
      body: ''
    },
    scheduleTime: {
      seconds: Date.now() / 1000
    }
  };

  if (options.payload !== undefined) {
    task.appEngineHttpRequest.body = Buffer.from(JSON.stringify(options.payload)).toString(
      'base64'
    );
    console.log('task.appEngineHttpRequest.body', task.appEngineHttpRequest.body)

  }

  if (options.inSeconds !== undefined) {
    task.scheduleTime = {
      ...task.scheduleTime,
      seconds: options.inSeconds + task.scheduleTime.seconds,
    };
  }

  removeUndefinedProps(task)

  const request = {
    parent: parent,
    task: task as any,
  };

  console.log('Sending task %j', task);
  // Send create task request.
  const [response] = await client.createTask({...request,
  task:{
    ...request.task,
    name: 'test'
  }});
  const name = response.name;
  console.log(`Created task ${name}`);

  return response

}

const cli = require(`yargs`)
  .options({
    location: {
      alias: 'l',
      description: 'Location of the queue to add the task to.',
      type: 'string',
      requiresArg: true,
      required: true,
    },
    queue: {
      alias: 'q',
      description: 'ID (short name) of the queue to add the task to.',
      type: 'string',
      requiresArg: true,
      required: true,
    },
    project: {
      alias: 'p',
      description: 'Project of the queue to add the task to.',
      default: process.env.GCLOUD_PROJECT,
      type: 'string',
      requiresArg: true,
      required: true,
    },
    payload: {
      alias: 'd',
      description: '(Optional) Payload to attach to the push queue.',
      type: 'string',
      requiresArg: true,
    },
    inSeconds: {
      alias: 's',
      description:
        '(Optional) The number of seconds from now to schedule task attempt.',
      type: 'number',
      requiresArg: true,
    },
  })
  .example(`node $0 --project my-project-id`)
  .wrap(120)
  .recommendCommands()
  .epilogue(`For more information, see https://cloud.google.com/cloud-tasks`)
  .strict();

if (module === require.main) {
  console.log('module === require.main');
  const opts = cli.help().parse(process.argv.slice(2));
  process.env.GCLOUD_PROJECT = opts.project;
  createTask(opts.project, opts.location, opts.queue, opts).catch(
    console.error
  );
}

exports.createTask = createTask;
