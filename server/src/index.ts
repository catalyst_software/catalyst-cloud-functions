import { schedulePushNotification } from './api/app/v1/notifications/schedule-push-notification'
//// tslint:disable-next-line:no-reference
//// <reference path='./_all.d.ts' />
// import * as glob from 'glob';
// import * as camelCase from 'camelcase';

import * as biometricEntriesOnCreate from './db/biometricEntries/onCreate'
import * as athleteBiometricHistoryOnCreate from './db/biometricHistory/athletes/onCreate'
import * as updateAthleteIndicatorBIO from './db/biometricHistory/athletes/onUpdate'
import * as groupBiometricHistoryOnCreate from './db/biometricHistory/groups/onCreate'

// import { updateIpScore } from './db/ipscore/onUpdate';
import * as updateIpScore from './db/ipscore/onUpdate'
// import * as addUser from './db/athletes/onCreate';
import { api1 } from './api/app/v1/app'

import { addCoachClaim } from './db/addCoachClaim'
import { getBiomentricEntriesById } from './db/getBiomentricEntriesById'
import { getSurveysById } from './db/getSurveysById'

// import helloFirestore from './analytics/triggers';
import {
    //     athleteOnboarded,
    //     organizationCodeValidated,
    //     biometricStepsCreated,
    //     biometricMoodCreated,
    //     biometricTrainingCreated,
    //     // biometricsMoodDistraught,
    //     biometricsMoodOK,
    //     biometricsMoodSad,
    //     biometricsFatigueExtreme,
    transactionValidationError,
    iapInitialBuyFailed,
    //     biometricInjuryPainAbove2,
    //     biometricTrainingPainAbove2
} from './analytics/triggers/triggers'

import { notificationGooglePlay } from './pubsub/subscriptions/notification-google-play'
import { bqExport } from './pubsub/subscriptions/bq-export'
import { ipScoreNonEngagement } from './pubsub/ip-score-non-engagement'

import * as eventListeners from './analytics/triggers/events'

import { scheduleEssentialComms } from './api/app/v1/callables/schedule-essential-comms'
import { updateCoachMessaginIDsOnConfig } from './api/app/v1/callables/update-coach-messaginig-ids-on-config'
import { validateOrganizationCode } from './api/app/v1/callables/validate-organization-code'

import {
    setUserEmailVerified,
    getUserEmailVerified,
} from './api/app/v1/callables/email-verification'
import { setUserEmail } from './api/app/v1/callables/set-email'
import { validateTransactionReceipt } from './api/app/v1/callables/validate-transaction-receipt'
import { updateAthleteSubscription } from './api/app/v1/callables/update-athlete-subscription'
import { deleteUserAccount } from './api/app/v1/callables/delete-user-account'

import { scheduleReport } from './api/app/v1/callables/schedule-report'
import { bigQSqlQuery } from './api/app/v1/callables/bigQSqlQuery'

import { runGroupIpScoreAggregation } from './api/app/v1/callables/run-group-ip-score-aggregation'
import { runGroupWellnessDataAggregation } from './api/app/v1/callables/run-group-wellness-data-aggregation'
import { getOembedData } from './api/app/v1/callables/get-oembed-data'
import { updateNotificationCardMetadata } from './api/app/v1/callables/update-notification-card-metadata'
import { updateCalendarEventInvitationStatus } from './api/app/v1/callables/calendar-event-invitation-status'
import { getBQData } from './api/app/v1/callables/get-bq-data'
import { scheduleSendNotifications } from './api/app/v1/callables/schedule-send-notifications'
import { moveAthleteToGroup } from './api/app/v1/callables/moveAthleteToGroup'
import { addAthleteToGroup } from './api/app/v1/callables/addAthleteToGroup'
import { removeAthleteFromGroup } from './api/app/v1/callables/removeAthleteFromGroup'
import { getDynamicGroupId } from './api/app/v1/callables/utils/onboarding/get-dynamic-group-id'
import { setRedFlagResloveState } from './api/app/v1/callables/set-red-flag-reslove-state '
import { createVideoConversionTask } from './api/app/v1/callables/createVideoConversionTask'

/* TRAVIS IMPORTS */
import * as manageTaskQueue from './db/taskQueue/onCreate'
import * as manageAthleteCreation from './db/athletes/onCreate'
import * as manageAsyncJobQueueOnCreate from './db/asyncJobQueue/onCreate'
import * as manageAsyncJobQueueOnUpdate from './db/asyncJobQueue/onUpdate'

// pubsub
import { appEngagmentBigQueryWorker } from './pubsub/subscriptions/taskWorkers/app-engagement-worker'
import { appSessionUsageMetricBigQueryWorker } from './pubsub/subscriptions/taskWorkers/app-session-usage-metric-worker'
import { updateAthleteIpScoreForGroups } from './api/app/v1/callables/update-athlete-ipscore-for-groups'
import { biometricBigQueryWorker } from './pubsub/subscriptions/taskWorkers/biometric-worker'
import { programContentEngagmentBigQueryWorker } from './pubsub/subscriptions/taskWorkers/program-content-engagement-worker'
import { ipScoreAchievementBigQueryWorker } from './pubsub/subscriptions/taskWorkers/ipscore-achievement-worker'
import { notificationCardEngagmentBigQueryWorker } from './pubsub/subscriptions/taskWorkers/notification-card-engagement-worker'
import { cardRenderedBigQueryWorker } from './pubsub/subscriptions/taskWorkers/card-rendered-worker'
import { redFlagBigQueryWorker } from './pubsub/subscriptions/taskWorkers/red-flag-worker'
import { selfHealingEveryTwoHourWorker } from './pubsub/subscriptions/taskWorkers/self-healing-every-two-hour-worker'
import { bigQueryDataManipulationReattemptWorker } from './pubsub/subscriptions/taskWorkers/big-query-data-manipulation-reattempt-worker'
import { cloudTaskUsageMetricBigQueryWorker } from './pubsub/subscriptions/taskWorkers/cloud-task-usage-metric-worker'
import { getCustomAuthToken } from './api/app/v1/callables/get-custom-auth-token'
import { onboardingInvitationWorker } from './pubsub/subscriptions/taskWorkers/onboarding-invitation-worker'
import { manageSegmentDeletionWorker } from './pubsub/subscriptions/taskWorkers/manage-segment-deletion-worker'
import { manageAthleteSegmentAdditionWorker } from './pubsub/subscriptions/taskWorkers/manage-athlete-segment-addition-worker'
import { manageNewProgramAdditionWorker } from './pubsub/subscriptions/taskWorkers/manage-new-program-addition-worker'
import { scheduleNotificationCardDeliveryWorker } from './pubsub/subscriptions/taskWorkers/schedule_notification-card-delivery-worker'
import { scheduleNotificationCardDeliveryHttpHandler } from './httpTriggerFunctions/schedule-notfication-card-delivery.http-handler'
import { scheduledWeightBmiMigration } from './db/biometricEntries/scheduledTasks/continuous-scalar-biometric-updates'

/** EXPORT ALL FUNCTIONS
 *
 *   Loads all `.f.js` files
 *   Exports a cloud function matching the file name
 *   Author: David King
 *   Edited: Tarik Huber
 *   Based on this thread:
 *     https://github.com/firebase/functions-samples/issues/170
 */

// const files = glob.sync('./db/**/*.f.js', { cwd: __dirname, ignore: './node_modules/**' });
// for (let f = 0, fl = files.length; f < fl; f++) {
//     const file = files[f];
//     const functionName = camelCase(file.slice(0, -5).split('/').join('_')); // Strip off '.f.js'
//     if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === functionName) {
//         exports[functionName] = require(file);
//     }
// }

/**
 * We export all our functions here. The compiler will include any functions included in this export
 */

// if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'updateAggregates-updateBiometricEntries') {
//     export const updateAggregates = require('./db/biometricEntries/onCreate');
// }

// if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'apiDevTest') {
//     export const apiDevTest = require('./api/app/v1/app');
// }
// if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'api') {
//     export const api1 = require('./api/app/v1/app');
// }
// if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === 'updateAggregates-updateBiometricEntries') {
//     export const updateAggregates = require('./api/app/v1/app');
// }
export {
    api1 as api,
    updateIpScore,
    addCoachClaim,
    getBiomentricEntriesById,
    getSurveysById,
    biometricEntriesOnCreate as updateAggregates,
    athleteBiometricHistoryOnCreate,
    groupBiometricHistoryOnCreate,
    notificationGooglePlay,
    transactionValidationError as transactionValidationErrorTrigger,
    iapInitialBuyFailed as iapInitialBuyFailedTrigger,
    eventListeners,
    scheduleEssentialComms,
    scheduleSendNotifications,
    schedulePushNotification,
    updateAthleteIpScoreForGroups,
    // updateGroupIpScores,
    updateCoachMessaginIDsOnConfig,
    ipScoreNonEngagement,
    validateOrganizationCode,
    setUserEmailVerified,
    getUserEmailVerified,
    setUserEmail,
    validateTransactionReceipt,
    updateAthleteSubscription,
    deleteUserAccount,
    bqExport,
    getBQData,
    runGroupIpScoreAggregation,
    runGroupIpScoreAggregation as runGroupTestIpScoreAggregation,
    runGroupWellnessDataAggregation,
    updateNotificationCardMetadata,
    scheduleReport,
    bigQSqlQuery,
    getOembedData,
    updateCalendarEventInvitationStatus,
    updateAthleteIndicatorBIO,
    moveAthleteToGroup,
    addAthleteToGroup,
    removeAthleteFromGroup,
    getDynamicGroupId,
    setRedFlagResloveState,
    createVideoConversionTask,
    // Travis
    manageTaskQueue as processTasks,
    manageAthleteCreation as processAthletes,
    manageAsyncJobQueueOnCreate,
    manageAsyncJobQueueOnUpdate,
    appEngagmentBigQueryWorker,
    appSessionUsageMetricBigQueryWorker,
    biometricBigQueryWorker,
    redFlagBigQueryWorker,
    programContentEngagmentBigQueryWorker,
    notificationCardEngagmentBigQueryWorker,
    ipScoreAchievementBigQueryWorker,
    cardRenderedBigQueryWorker,
    selfHealingEveryTwoHourWorker,
    bigQueryDataManipulationReattemptWorker,
    cloudTaskUsageMetricBigQueryWorker,
    onboardingInvitationWorker,
    manageSegmentDeletionWorker,
    manageAthleteSegmentAdditionWorker,
    manageNewProgramAdditionWorker,
    scheduleNotificationCardDeliveryWorker,
    getCustomAuthToken,
    scheduleNotificationCardDeliveryHttpHandler,
    scheduledWeightBmiMigration,
}
