// import * as firebaseHelper from 'firebase-functions-helper';

// var Firebase = require('firebase');
import express from 'express'
import cors from 'cors'
// import * as bodyParser from 'body-parser'
// Import the Firebase SDK for Google Cloud Functions.
import * as functions from 'firebase-functions'

// import { initialiseLoggers } from '../../../shared/logger/service/logger-service';
import setRoutes from './routes'
import { isProduction } from './is-production'
import { Config } from './../../../shared/init/config/config'
// import { FirestoreCollection } from '../../../db/biometricEntries/enums/firestore-collections';

/**
 * System wide logger
 */
// const logger = initialiseLoggers('info', 'warn', 'logger-service')

/**
 * Our express app
 */
// Create HTTP Server
const app = express()

//
// START: Inject Middleware.  Middleware intercepts the request and the response
//

/**
 * CORS is a node.js package for providing a Connect/Express middleware that can be used
 * to enable CORS with various options.
 */

// app.options('/products/:id', cors()) // enable pre-flight request for DELETE request
// app.del('/products/:id', cors(), function (req, res, next) {
//     res.json({ msg: 'This is CORS-enabled for all origins!' })
// })

const corsOptions = {
    origin: true,
    // origin: 'http://example.com',
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions))

app.use((req, res, next) => {
    // Set CORS headers

    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888')

    res.header('Access-Control-Allow-Origin', '*')
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )

    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Request-Method', '*')
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET')
    res.setHeader('Access-Control-Allow-Headers', '*')
    // res.setHeader('Access-Control-Allow-Headers', req.headers.origin);

    // res.setHeader('Access-Control-Allow-Headers', req.headers.origin);
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )
    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if (req.method === 'OPTIONS') {
        res.writeHead(200)
        res.end()
        return
    }
    // res.header("Access-Control-Allow-Origin", "*");
    next()
})
// const server = require('http').createServer((req, res, next) => {
//     // Set CORS headers
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Request-Method', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
//     // res.setHeader('Access-Control-Allow-Headers', '*');
//     res.setHeader('Access-Control-Allow-Headers', req.headers.origin);
//     if (req.method === 'OPTIONS') {
//         res.writeHead(200);
//         res.end();
//         return;
//     }
//     //     next();

// });

/**
 * bodyParser Parse incoming request bodies in a middleware before your handlers,
 * available under the req.body property.
 */
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

/**
 * Set API Routes!!!
 */
setRoutes(app)

if (isProduction()) {
    // console.log('app process.env', JSON.stringify(process.env))
    // console.log('=====........ isProduction => app process.env.FUNCTION_NAME?!?', process.env.FUNCTION_NAME)
    // console.log("=====>>> isProduction === true =>  ", { service: process.env.K_SERVICE })
    // console.log("=====>>> FIREBASE_CONFIG: ", { FIREBASE_CONFIG: process.env.FIREBASE_CONFIG })
    // console.log("=====>>> storageBucket: ", { FIREBASE_CONFIG: JSON.parse(process.env.FIREBASE_CONFIG).storageBucket })
}
console.log('production?', isProduction() || false)
//
// END: Inject Middleware.
//

console.log('Config: ' + JSON.stringify(Config))

if (Config.isDebugging || true) {
    // Running Locally
    console.log('creating local server')
    const server = require('http').createServer(app)

    // Indicate port 3000 as host
    const port = process.env.PORT || 3000

    // Create a new firebase reference
    // const firebaseRef = new Firebase(
    //     `https://inspire-219716.firebaseio.com/athleteBiometricWeeklyAggregates/FKORArPPLiRSjACLvqWQIaZWFJzo2_0_0/`
    // );

    // TB - Node10 in GCF sets this up for us, no need to call listen with port
    // Make the server listens on port 3000
    server.listen(port, function () {
        // console.log("Server listening on port %d", port);
        console.log('Your server available at http://localhost:%d', port)
    })
} else {
    console.log('server initialized')
}

/**
 * Inspire HttpsFunction!
 * @returns functions.HttpsFunction
 */
export const api1: functions.HttpsFunction =
    functions.https._onRequestWithOptions(app, { regions: Config.regions })
