import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections';
import { Response, Request } from 'express'
import * as functions from 'firebase-functions'
// import * as sgMail from '@sendgrid/mail'
const sgMail = require('@sendgrid/mail');

import BaseCtrl from '../../controllers/base';
import { isProductionProject } from '../../is-production';
import { db } from '../../../../../shared/init/initialise-firebase';
import { KeyValuePair } from '../../../../../models/notifications/interfaces/schedule-notification-payload';
import { StackDriverTemplateIDs } from '../../../../../models/enums/lifestyle-entry-type';

export interface EmailMessage {

    // firstName: string;
    // lastName: string;

    // email: string;

    personalizations: [{
        to: {
            name: string;
            email: string;
        }[]
    }],
    from?: {
        name: string;
        email: string;
    },
    // subject: string;
    // text?: string;
    // html: string;
    dynamic_template_data?: {

        isActive?: boolean;
        name: string;

        subject?: string;
        message?: string;

        title?: string;

        programsHeading?: string;
        programsSubHeading?: string;

        programs?: Array<Program>;
        program1?: Program;
        program2?: Program;

        showDownloadButton?: boolean;
        barType?: {
            danger?: boolean;
            warn?: boolean;
            success?: boolean;
        };


    }
    templateId: string;
    emailTemplateName: string
}
export interface EmailMessageTemplate {

    to: {
        name: string;
        email: string;
    }[]

    from?: {
        name: string;
        email: string;
    },

    emailTemplateData: {

        isActive?: boolean;
        name: string;

        subject?: string;
        message?: string;

        title?: string;

        programsHeading?: string;
        programsSubHeading?: string;

        programs?: Array<Program>;
        program1?: Program;
        program2?: Program;

        showDownloadButton?: boolean;
        barType: {
            danger?: boolean;
            warn?: boolean;
            success?: boolean;
        };


    }
    templateId: string;
    emailTemplateName: string
}
export interface EmailMessageParams {

    USER_ID?: string;
    ORGANIZATION_ID?: string;
    GROUP_IDS: Array<string>;

    USER_FULL_NAME: string;
}

// const emailMessageTemplate: EmailMessageTemplate = {
//     to: [{
//         name: "Ian Kaapisoft",
//         email: "ian.gouws@kaapisoft.com"
//     },
//     {
//         name: "Ian iNSPIRE",
//         email: "ian@inspiresportonline.com"
//     }],
//     from: {
//         name: "iNSPIRE Dev",
//         email: "athleteservices@inspiresportonline.com"
//     },

//     templateId: "d-93756f51e55b4734a5b93d23e4310750",
//     emailTemplateName: "badMood",

//     emailTemplateData: {

//         name: "Ian Gouws",

//         subject: "Custom Email Subject",

//         message: "<h1>Custom messtage</h1>",

//         showDownloadButton: false,
//         barType: {
//             danger: true
//         }
//     }
// }
export enum NotificationBarType {
    Danger = 'danger',
    Warn = 'warn',
    Success = 'success'
}
export enum HeadingImageType {
    Mood = 'mood',
    Body = 'body',
    Training = 'training',
    Food = 'food'
}
export interface TemplateData {
    heading: string;
    showDangerBar: boolean;
    lifestyleEntryType: HeadingImageType;
    // dynamic_Template_data: {
    isActive?: boolean;
    name: string;
    programs?: Array<Program>,
    message: string;
    programsHeading: string;
    programsSubHeading: string;
    subject: string;
    title: string;

    showDownloadButton?: boolean;
    barType: {
        danger?: boolean;
        warn?: boolean;
        success?: boolean;
    };
    // }


}

export interface Program {
    description: string;
    image: string;
    link: string;
    title: string;
}

export const replaceParts = (
    emailTemplate: TemplateData,
    keyValuePairs: KeyValuePair[]
) => {
    let heading = emailTemplate.heading
    if (heading)
        keyValuePairs.forEach(pair => {
            heading = heading.replace(pair.key, pair.value)
        })

    let message = emailTemplate.message
    if (message)
        keyValuePairs.forEach(pair => {
            message = message.replace(pair.key, pair.value)
        })

    let subject = emailTemplate.subject
    if (subject)
        keyValuePairs.forEach(pair => {
            subject = subject.replace(pair.key, pair.value)
        })

    let title = emailTemplate.title
    if (title)
        keyValuePairs.forEach(pair => {
            title = title.replace(pair.key, pair.value)
        })

    let programsHeading = emailTemplate.programsHeading
    if (programsHeading)
        keyValuePairs.forEach(pair => {
            programsHeading = programsHeading.replace(pair.key, pair.value)
        })

    let programsSubHeading = emailTemplate.programsSubHeading
    if (programsSubHeading)
        keyValuePairs.forEach(pair => {
            programsSubHeading = programsSubHeading.replace(pair.key, pair.value)
        })

    const res: TemplateData = {
        ...emailTemplate,
        heading,
        message,
        subject,
        title,
        programsHeading,
        programsSubHeading

    }
    return res
}


/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class SendGridCtrl
 * @extends {BaseCtrl}
 */
export default class SendGridCtrl extends BaseCtrl {
    res: Response

    // logger: Logger;
    model = undefined // SendGrid

    collection = undefined //FirestoreCollection.SendGrid

    // sendGridHistoryDocumentManager: SendGridHistoryDocumentManager

    constructor() {
        super()
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof SendGridCtrl
     */

    sendEmail = async (req: Request, res: Response) => {





        const { message } = <{ message: EmailMessage }>req.body

        const config = functions.config()



        const { id: key } = req.params

        // const message: EmailMessage = {
        //     personalizations: [
        //         {
        //             to: [
        //                 {
        //                     name: "Ian Kaapisoft",
        //                     email: "ian.gouws@kaapisoft.com"
        //                 },
        //                 {
        //                     name: "Ian iNSPIRE",
        //                     email: "ian@inspiresportonline.com"
        //                 }
        //             ]
        //         }
        //     ],
        //     from: {
        //         name: "iNSPIRE Dev",
        //         email: "athleteservices@inspiresportonline.com"
        //     },
        //     templateId: "d-93756f51e55b4734a5b93d23e4310750",
        //     emailTemplate: "badMood",
        //     dynamic_template_data: {

        //         name: "Ian Gouws!!",


        //         barType: {
        //             danger: true
        //         }
        //     }
        // }

        // return sendEmail(message, key)

        if (isProductionProject() && config.sendgrid && config.sendgrid.key) {

            const keyFromConfig = config.sendgrid.key
            sgMail.setApiKey(keyFromConfig);

        } else if (req.params.id) {
            if (key && key !== '') {
                sgMail.setApiKey(key);
            } else {
                return res.status(501).json({
                    message: 'sendEmail',
                    responseCode: 501,
                    data: {},
                    error: 'SendGrid API Key undefined',
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            }
        }


        // const template = {
        //     "uid": "badMood",
        //     "message": "<b> {{{name}}}<\/b> logged 3 consequitive \"very sad\" or \"sad\" mood entries and may need your help",
        //     "programsHeading": "Perhaps these programs can help???? :P",
        //     "programsSubHeading": "We have a wide range of wellness type programs that may be useful?!",
        //     "title": "Mood Entries!!!",
        //     "programs": [
        //         {
        //             "title": "Some Program",
        //             "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png",
        //             "description": "Some details here.",
        //             "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png"
        //         },
        //         {
        //             "description": "Some Other details here.",
        //             "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png",
        //             "title": "Some Other Program",
        //             "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png"
        //         }
        //     ],
        //     "subject": "An urgent message about {{{ name }}}!!!!"
        // }

        // sendEmail(message, keyFromConfig || key )
        const emailTemplate = message.emailTemplateName || 'badMood'

        const dbTemplateData = await db.collection(FirestoreCollection.EmailTemplates).doc(emailTemplate)
            .get().then((docSnap) => {

                return {
                    uid: docSnap.id,
                    ...docSnap.data() as TemplateData,
                }
            })
        // await db.collection(FirestoreCollection.EmailTemplates)
        // .doc('distraughtMood')
        // .set({
        //   ...dbTemplateData,
        //   message: '<b>{{{name}}}</b> logged a <b>DISTRAUGHT</b> mood entry and may need your help'
        // })

        const { programs: dbPrograms, ...restDBTemplateData } = dbTemplateData

        const { programs: customPrograms, ...restCustomTemplateData } = message.dynamic_template_data

        console.warn('Adding key: $NAME')

        const keyValuePairs: KeyValuePair[] = [
          {
            key: '{{{name}}}',
            value: restCustomTemplateData.name
          },
          {
            key: '$NAME',
            value: restCustomTemplateData.name
          }
        ];

        const parsedMessage = replaceParts({ ...restDBTemplateData, ...restCustomTemplateData }, keyValuePairs)


        const program1 = customPrograms && customPrograms[0]
            ? customPrograms[0] : dbPrograms[0]

        const program2 = customPrograms && customPrograms[1]
            ? customPrograms[1] : dbPrograms[1]


        message.templateId = message.templateId || StackDriverTemplateIDs.DoubleImageLayout;

        message.dynamic_template_data = { ...parsedMessage, program1, program2 }
        const from = {
            name: "iNSPIRE",
            email: "athleteservices@inspiresportonline.com"
        }

        message.from = from

        return await sgMail.send({
            ...message,
            // subject: unescape(message.subject),
            // text: unescape(message.text),
            // title: unescape(message.title),
            // html: unescape(message.html),
            // personalizations: [{
            //     to: {
            //         name: unescape(message.personalizations[0].to[0].name),
            //         email: unescape(message.personalizations[0].to[0].email)
            //     }
            // }]
            // ,
            // from: {
            //     name: unescape(message.from.name),
            //     email: unescape(message.from.email)
            // }
        }, (resCB) => {
            if (resCB) {
                console.log('resCB', resCB)
            }
        })
            .then((response) => {
                // POST succeeded...
                // console.log('parsedBody', parsedBody)
                console.log('sendEmail statusCode', response[0].statusCode)
                return res.status(response[0].statusCode || 202).json({
                    message: 'sendEmail',
                    responseCode: response[0].statusCode || 202,
                    data: {
                        response,
                    },
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            })
            .catch((err) => {
                // POST failed...
                console.log('err', err)
                return res.status(500).json({
                    message: 'sendEmail',
                    responseCode: 500,
                    data: {},
                    error: err,
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            });


    }

    addEmailTemplateData = async (req: Request, res: Response) => {





        // const { message } = <{ message: EmailMessage }>req.body

        // const config = functions.config()



        // const { id: key } = req.params

        // const message: EmailMessage = {
        //     personalizations: [
        //         {
        //             to: [
        //                 {
        //                     name: "Ian Kaapisoft",
        //                     email: "ian.gouws@kaapisoft.com"
        //                 },
        //                 {
        //                     name: "Ian iNSPIRE",
        //                     email: "ian@inspiresportonline.com"
        //                 }
        //             ]
        //         }
        //     ],
        //     from: {
        //         name: "iNSPIRE Dev",
        //         email: "athleteservices@inspiresportonline.com"
        //     },
        //     templateId: "d-93756f51e55b4734a5b93d23e4310750",
        //     emailTemplate: "badMood",
        //     dynamic_template_data: {

        //         name: "Ian Gouws!!",


        //         barType: {
        //             danger: true
        //         }
        //     }
        // }

        // return sendEmail(message, key)
        const dbTemplateData = { "uid": "badMood", "programsSubHeading": "We have a wide range of wellness type programs that may be useful", "message": "<b>{{{name}}}</b> logged 3 consequitive <b>\"very sad\"</b> or <b>\"sad\"</b> mood entries and may need your help", "programsHeading": "Perhaps these programs can help???", "showDownloadButton": false, "heading": "An urgent message about  <br>{{{name}}}", "barType": { "danger": true }, "isActive": true, "lifestyleEntryType": { "body": false, "training": false, "food": false, "mood": true }, "title": "Mood Entries", "programs": [{ "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png", "title": "Some Program", "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png", "description": "Some details here." }, { "description": "Other details here.", "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png", "title": "Other Program", "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png" }], "subject": "An urgent message about {{{name}}}" }
        await db.collection(FirestoreCollection.EmailTemplates)
            .doc('distraughtMood')
            .set({
                ...dbTemplateData,
                message: '<b>{{{name}}}</b> logged a <b>DISTRAUGHT</b> mood entry and may need your help',
                uid: 'distraughtMood'
            })

        await db.collection(FirestoreCollection.EmailTemplates)
            .doc('badMood')
            .set(dbTemplateData)

    }
}
