import * as functions from 'firebase-functions'

import { sendNotification } from '../controllers/utils/send-message';
import { ScheduleNotificationPayload } from '../../../../models/notifications/interfaces/schedule-notification-payload';
import { Config } from './../../../../shared/init/config/config';

export const schedulePushNotification = functions.https._onCallWithOptions(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            try {
                const { type, token, keyValuePairs, appIdentifier } = <ScheduleNotificationPayload>data

                console.log('data', data)
                const notificationPath = ''
                if (token) {
                    // TODO: 
                    const isRedFlags = true;
                    return sendNotification(isRedFlags, type, notificationPath, keyValuePairs, token, undefined, false, undefined, undefined, appIdentifier).then(
                        ({ message, response }) => {
                            if (response) {
                                return {
                                    data: {
                                        message,
                                        response,
                                    },
                                }
                            } else {
                                return {
                                    data: {
                                        message,
                                    },
                                }
                            }
                        }
                    )
                } else {
                    console.error('Token is empty.... Message not sent')
                    return {
                        data: {
                            message: 'Token is empty.... Message not sent',
                        }
                    }
                }
            } catch (error) {
                console.log('Error sending message:', error)
                throw error
            }



        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )
            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)
