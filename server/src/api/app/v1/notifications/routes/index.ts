import * as express from 'express'

import setSendGridRoutes from '../../routes/send-grid'

/**
 * Configure routes
 * @param app: Express
 * // TODO: 
 * @param logger: winston.Logger
 */
export default function setRoutes(app) {
    const router = express.Router();

    setSendGridRoutes(router);

    app.use('/', router)
}
