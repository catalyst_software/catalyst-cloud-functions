import * as express from 'express';
import SendGridCtrl from '../sendGrid/conrollers/email';

/**
* Configure SendGrid routes
* @param router: express.Router
*/
export default function setSendGridRoutes(router: express.Router) {

    const sendGridCtrl = new SendGridCtrl();
    
    router.route('/sendgridEmail/:id').post(sendGridCtrl.sendEmail);
    router.route('/sendgrid/addTemplate').post(sendGridCtrl.addEmailTemplateData);
}
