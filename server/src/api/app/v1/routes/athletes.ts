import * as express from 'express';

import AthletesCtrl from '../controllers/athletes';

/**
* Configure Athlete routes
* @param router: express.Router
*/
export default function setAthletesRoutes(router: express.Router) {

    const athletesCtrl = new AthletesCtrl();
 
    router.route('/athletes/history').post(athletesCtrl.createAthleteHistoryDocuments);
    router.route('/athletes').get(athletesCtrl.getAthletes);

    router.route('/updateAthlete').post(athletesCtrl.updateAthlete);
    router.route('/athletes/purge').post(athletesCtrl.purgeAthletes);
    router.route('/athletes/setTimestamp').post(athletesCtrl.setAthleteCreationTimestamp);
    router.route('/athletes/updateAthleteIndicatorIpscore').post(athletesCtrl.updateAthleteIndicatorIpscore);
    router.route('/athletes/updateMessagingIDInConfig').post(athletesCtrl.updateMessagingIDInConfig);
    router.route('/athletes/setOrgCode').post(athletesCtrl.setOrgCode);
    router.route('/athletes/clone').post(athletesCtrl.cloneAthlete);
    router.route('/athletes/removeStalePrograms').post(athletesCtrl.removeStalePrograms);
    router.route('/athletes/removeEmailFromGroupInd').post(athletesCtrl.removeEmailFromGroupInd);

    router.route('/athletes/splitIntoMultipleGroups').post(athletesCtrl.splitIntoMultipleGroups);
    router.route('/athletes/updateGroupsInOrgArray').post(athletesCtrl.updateGroupsInOrgArray);
    router.route('/athletes/clearAthleteDocs').post(athletesCtrl.clearAthleteDocs);
    router.route('/athletes/fixAthleteGroups').post(athletesCtrl.fixAthleteGroups);
}
