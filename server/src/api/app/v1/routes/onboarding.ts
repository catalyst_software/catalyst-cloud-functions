import * as express from 'express';

import OnboardingCtrl from '../controllers/onboarding';

/**
* Configure Maintenance routes
* @param router: express.Router
*/

export default function setOnboardingRoutes(router: express.Router) {

    const onboardingCtrl = new OnboardingCtrl();

    router.route('/onboarding/invite').post(onboardingCtrl.processInvitation);
    router.route('/onboarding/move').post(onboardingCtrl.performMaintenanceMove);
}
