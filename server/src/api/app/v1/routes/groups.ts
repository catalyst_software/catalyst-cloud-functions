import * as express from 'express';

import GroupsCtrl from '../controllers/groups';

/**
* Configure Group routes
* @param router: express.Router
*/
export default function setGroupsRoutes(router: express.Router) {

    const groupsCtrl = new GroupsCtrl();
 
    // router.route('/groups').post(groupsCtrl.addGroup);
    router.route('/groups/add').post(groupsCtrl.addGroup);
    router.route('/groups/remove').post(groupsCtrl.removeGroup);
    router.route('/groups/history').post(groupsCtrl.createGroupHistoryDocuments);
    router.route('/groups/addAthlete').post(groupsCtrl.addAthlete);
    router.route('/groups/moveAthlete').post(groupsCtrl.moveAthlete);
    router.route('/groups/moveAthleteToInspire').post(groupsCtrl.moveAthleteToInspire);
    router.route('/groups/removeAthlete').post(groupsCtrl.removeAthlete);
    router.route('/groups/updateIP').post(groupsCtrl.updateIP);
    router.route('/groups/updateIPTest').post(groupsCtrl.resetAthleteIndicatorsDailyIPScore);
    router.route('/groups/clearIPScore').post(groupsCtrl.clearIPScore);
    router.route('/groups/updateSubscription').post(groupsCtrl.updateSubscription);
    router.route('/groups/runGroupIPScoreAggregation').post(groupsCtrl.runGroupIPScoreAggregation);
    router.route('/groups/runGroupWellAggregation').post(groupsCtrl.runGroupWellAggregation);
    
    // router.route('/group').delete(groupsCtrl.deleteGroup);

}
