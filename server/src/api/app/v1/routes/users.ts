import * as express from 'express';

import UsersCtrl from '../controllers/users';

/**
* Configure User routes
* @param router: express.Router
*/
export default function setUsersRoutes(router: express.Router) {

    const usersCtrl = new UsersCtrl();

    // router.route('/users').get(usersCtrl.getAllUsers);
    // router.route('/users').post(usersCtrl.addUser);
    // router.route('/users/:id').delete(usersCtrl.deleteUser);
    router.route('/users/setEmailVerified').post(usersCtrl.setEmailVerified);
    router.route('/users/setEmail').post(usersCtrl.setEmail);


}
