import * as express from 'express'

import MaintenanceCtrl from '../controllers/maintenance'

/**
 * Configure Maintenance routes
 * @param router: express.Router
 */

export default function setMaintenanceRoutes(router: express.Router) {
    const maintenanceCtrl = new MaintenanceCtrl()

    router
        .route('/maintenance/fixIPScoreNAN')
        .post(maintenanceCtrl.fixIPScoreNAN)
    router
        .route('/maintenance/copyNotificationTemplate')
        .post(maintenanceCtrl.copyNotificationTemplate)
    router.route('/maintenance/copyTheme').post(maintenanceCtrl.copyTheme)
    router
        .route('/maintenance/replaceAthletesTheme')
        .post(maintenanceCtrl.replaceAthletesTheme)
    router
        .route('/maintenance/copyEmailTemplate')
        .post(maintenanceCtrl.copyEmailTemplate)
    router
        .route('/maintenance/copyRootCollection')
        .post(maintenanceCtrl.copyRootCollection)
    router
        .route('/maintenance/reRunAggregation')
        .post(maintenanceCtrl.reRunAggregation)
    router
        .route('/maintenance/updateAthleteSponsoredSubscription')
        .post(maintenanceCtrl.updateAthleteSponsoredSubscription)
    router.route('/maintenance/billingTest').post(maintenanceCtrl.billingTest)
    router
        .route('/maintenance/migrateGroupAthleteIndicators')
        .post(maintenanceCtrl.migrateGroupAthleteIndicators)
    router
        .route('/maintenance/createAthleteIndexForGroups')
        .post(maintenanceCtrl.createAthleteIndexForGroups)
    router
        .route('/maintenance/revertAthleteSubcollectionOnGroups')
        .post(maintenanceCtrl.revertAthleteSubcollectionOnGroups)
    router
        .route('/maintenance/migrateAthleteLookups')
        .post(maintenanceCtrl.migrateAthleteLookupValues)
    router
        .route('/maintenance/migrateProgramDimensions')
        .post(maintenanceCtrl.migrateProgramDimensionValues)
    router.route('/maintenance/migrateOrg').post(maintenanceCtrl.migrateOrg)
    router
        .route('/maintenance/migrateOrgBiometrics')
        .post(maintenanceCtrl.migrateOrgBiometrics)
    router
        .route('/maintenance/extractHcpEventReport')
        .post(maintenanceCtrl.extractHcpEventReport)
    router
        .route('/maintenance/removeAllOrgUsers')
        .post(maintenanceCtrl.removeAllOrgUsers)
    router
        .route('/maintenance/getAllUsersInOrg')
        .post(maintenanceCtrl.getAllUsersInOrg)
    router
        .route('/maintenance/copyNotificationTemplates')
        .post(maintenanceCtrl.copyNotificationTemplates)
    router
        .route('/maintenance/fixIpScoreConfig')
        .post(maintenanceCtrl.fixIpScoreConfig)
    router
        .route('/maintenance/updateFoodLexicon')
        .post(maintenanceCtrl.updateFoodLexicon)
}
