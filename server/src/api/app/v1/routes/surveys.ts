import * as express from 'express';

import SurveysCtrl from '../controllers/surveys';

/**
* Configure Survey routes
* @param router: express.Router
*/
export default function setSurveysRoutes(router: express.Router) {

    const surveysCtrl = new SurveysCtrl();
 
    router.route('/surveys').get(surveysCtrl.getAllRecords);
    router.route('/survey/:id').get(surveysCtrl.getSurvey);
    router.route('/surveys/add').post(surveysCtrl.addSurvey);
    router.route('/surveys/results').get(surveysCtrl.getAllAthleteSurveys);

}
