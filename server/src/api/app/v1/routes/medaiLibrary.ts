import * as express from 'express';

import MediaLibraryCtrl from '../controllers/mediaLibrary';

/**
* Configure Program routes
* @param router: express.Router
*/
export default function setMediaLibraryRoutes(router: express.Router) {

    const mediaLibraryCtrl = new MediaLibraryCtrl();
 
    router.route('/mediaLibrary/add').post(mediaLibraryCtrl.addDummyMedia);
    // router.route('/mediaLibrary/update').post(mediaLibraryCtrl.updateDummyMedia);
    router.route('/mediaLibrary/addVideo').post(mediaLibraryCtrl.addVideoCard);
    router.route('/mediaLibrary/addLibraryToIndex').post(mediaLibraryCtrl.addLibraryToIndex);
    // router.route('/mediaLibrary/updateDummyMeta').post(mediaLibraryCtrl.updateDummyMeta);

}
