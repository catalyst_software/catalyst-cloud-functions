import * as express from 'express';

import ReportsCtrl from '../controllers/reports';

/**
* Configure Report routes
* @param router: express.Router
*/
export default function setReportsRoutes(router: express.Router) {

    const reportsCtrl = new ReportsCtrl();
 
    router.route('/reports').post(reportsCtrl.addReport);
    router.route('/reports/generate').post(reportsCtrl.generateReports);
    router.route('/reports/getBiometricsDeprecated').post(reportsCtrl.getBiometricsDeprecated);
    router.route('/reports/getInspireOrgCount').post(reportsCtrl.getInspireOrgCount);
    router.route('/reports/getInspireSubs').post(reportsCtrl.getInspireOrgSubs);
    router.route('/reports/getInspireSubs').get(reportsCtrl.getInspireOrgSubs);

}
