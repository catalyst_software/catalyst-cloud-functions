import * as express from 'express';

import TaskHandlerCtrl from '../controllers/taskHandlers';

/**
* Configure Athlete routes
* @param router: express.Router
*/
export default function setTaskHandlerRoutes(router: express.Router) {

    const taskCtrl = new TaskHandlerCtrl();
 
    router.route('/tasksHandler/scheduledNotificationCardHttpHandler').post(taskCtrl.scheduledNotiticationCardTaskHandler);
}
