import * as express from 'express';

import OrganizationsCtrl from '../controllers/organizations';

/**
* Configure Organization routes
* @param router: express.Router
*/
export default function setOrganizationsRoutes(router: express.Router) {

    const organizationsCtrl = new OrganizationsCtrl();
 
    // router.route('/organizations/add').post(organizationsCtrl.insert);
    router.route('/organizations/add').post(organizationsCtrl.addOrganization);
    router.route('/organizations/bulkadd').post(organizationsCtrl.addOrganizations);
    router.route('/organizations/code').post(organizationsCtrl.validateOrganizationCode);
    // router.route('/organizations/program').post(organizationsCtrl.addProgramToOrganization);

    // router.route('/organizations/program/update').post(organizationsCtrl.updateOrgOnboardingProgram);
    router.route('/organizations/config/update').post(organizationsCtrl.updateOrgNonEngagementConfiguration);
    // populate Routes - Create a couple of organizations for testing purposes
    // router.route('/populate/organizations').post(organizationsCtrl.populateOrganizations);

    router.route('/organizations').get(organizationsCtrl.getAllRecords);
    // router.route('/organizations/count').get(organizationsCtrl.count);
    // router.route('/organization').post(organizationsCtrl.insert);
    router.route('/organization/:id').get(organizationsCtrl.get);
    // router.route('/organization/:id').put(organizationsCtrl.update);
    // router.route('/organization/:id').delete(organizationsCtrl.delete);

}
