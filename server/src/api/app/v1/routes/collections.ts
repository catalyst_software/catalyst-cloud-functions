import * as express from 'express';

import UtilsCtrl from '../controllers/utils';

/**
* Configure Collection routes
* @param router: express.Router
*/
export default function setCollectionsRoutes(router: express.Router) {

    const utilsCtrl = new UtilsCtrl();
 
    router.route('/maintenance/resetIPScores').post(utilsCtrl.resetIPScores);
    router.route('/maintenance/getAllAthletes').post(utilsCtrl.getAllAthletes);
    router.route('/maintenance/cloneOrg').post(utilsCtrl.cloneOrg);
    router.route('/maintenance/cloneOrgs').post(utilsCtrl.cloneOrgs);
    
}
