import * as express from 'express';

import WordpressBridgeCtrl from '../controllers/wp-bridge';

/**
* Configure Athlete routes
* @param router: express.Router
*/
export default function setWpBridgeRoutes(router: express.Router) {

    const wpBridgeCtrl = new WordpressBridgeCtrl();
 
    router.route('/wp-bridge/users').post(wpBridgeCtrl.getTotalNumberOfUsers);
    router.route('/wp-bridge/distance').post(wpBridgeCtrl.getTotalDistance);

    router.route('/wp-bridge/leaderboard').post(wpBridgeCtrl.getDistanceLeaderboard);
}
