import * as express from 'express';

import TestsCtrl from '../controllers/tests';
import AndroidTestsCtrl from '../controllers/android-tests';
import IPScoreNonEngagementCtrl from '../controllers/tests/ipscore-non-engagement';
import CopyEmailTemplateCtrl from '../controllers/tests/copy-email-template';

/**
* Configure Test routes
* @param router: express.Router
*/
export default function setTestsRoutes(router: express.Router) {

    const testsCtrl = new TestsCtrl();
    const ipScoreNonEngagementCtrl = new IPScoreNonEngagementCtrl();
    const copyEmailTemplateCtrl = new CopyEmailTemplateCtrl();
    const androidTestsCtrl = new AndroidTestsCtrl();
 
    router.route('/test/token').post(testsCtrl.testGoogleToken);
    router.route('/test/programs').post(testsCtrl.importPrograms);
    router.route('/test/programs/add').post(testsCtrl.addProgram);
    router.route('/test/surveys/update').post(testsCtrl.updateSurveys);
    router.route('/test/callback').post(testsCtrl.handleNotificationEvent);
    router.route('/test/android/callback').post(androidTestsCtrl.handleNotificationEvent);
    
    router.route('/test/android/notification').post(testsCtrl.getAndroidNotifications);
    router.route('/test/ios/notification').post(testsCtrl.getIOSNotifications);
    router.route('/test/users').post(testsCtrl.getAllUsers);
    router.route('/test/fixEntityNames').post(testsCtrl.fixGroupWeeklyEntityNames);
    router.route('/test/fixIsCoach').post(testsCtrl.fixIsCoach);
    router.route('/test/addMessagingIdToAthleteIndicator').post(testsCtrl.addMessagingIdToAthleteIndicator);
    router.route('/test/athleteGroups').post(testsCtrl.getAthleteGroups);
    router.route('/test/emailCallable').post(testsCtrl.emailCallable);
    router.route('/test/ipScoreNonEngagement').post(ipScoreNonEngagementCtrl.ipScoreNonEngagement);
    router.route('/test/copyEmailTemplate').post(copyEmailTemplateCtrl.copyTemplate);
    router.route('/test/addToMainingList').post(copyEmailTemplateCtrl.addToMainingList);
    router.route('/test/downloadFromURL').post(testsCtrl.downloadFromURL);
    router.route('/test/biometrics').post(testsCtrl.getScalarBiometrics);
    router.route('/test/setStorageCors').post(testsCtrl.setStorageCors);
    router.route('/test/essentialComms').post(testsCtrl.scheduleEssentialCommsTest);

}
