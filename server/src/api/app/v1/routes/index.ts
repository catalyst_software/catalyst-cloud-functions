import * as express from 'express'

import setCollectionsRoutes from './collections'
import setBiometricsRoutes from './biometrics'
import setAthletesRoutes from './athletes'
import setGroupsRoutes from './groups'
import setIPScoreRoutes from './ip-score'
import setOrganizationsRoutes from './organizations'
import setProgramsRoutes from './programs'
import setSurveysRoutes from './surveys'
import setUsersRoutes from './users'
import setConsumerModelsRoutes from './consumer-model';
import setReportsRoutes from './reports';
import setNotificationsRoutes from './notifications';
import setPurchasesRoutes from './purchases';
import setTestsRoutes from './tests';
import setSendGridRoutes from './send-grid';
import setMaintenanceRoutes from './maintenance';
import setWhiteLabelRoutes from './whiteLabels';
import setVideosRoutes from './videos'
import setMediaLibraryRoutes from './medaiLibrary'
import setTasksRoutes from './tasks'
import setWarehouseRoutes from './warehouse';
import setAsyncJobQueueRoutes from './asyncJobQueue';
import setOnboardingRoutes from './onboarding';
import setTaskHandlerRoutes from './taskHandlers';


/**
 * Configure routes
 * @param app: Express
 * // TODO: 
 * @param logger: winston.Logger
 */
export default function setRoutes(app) {
    const router = express.Router();

    setCollectionsRoutes(router);
    setAthletesRoutes(router);
    setBiometricsRoutes(router);
    setConsumerModelsRoutes(router);
    setGroupsRoutes(router);
    setIPScoreRoutes(router);
    setOrganizationsRoutes(router);
    setProgramsRoutes(router);
    setReportsRoutes(router);
    setSurveysRoutes(router);
    setUsersRoutes(router);
    setNotificationsRoutes(router);
    setPurchasesRoutes(router);
    setTestsRoutes(router);
    setMaintenanceRoutes(router);
    setSendGridRoutes(router);
    setWhiteLabelRoutes(router);
    setVideosRoutes(router);
    setMediaLibraryRoutes(router);
    setTasksRoutes(router);
    
    setWarehouseRoutes(router);
    setAsyncJobQueueRoutes(router);
    setOnboardingRoutes(router);
    setTaskHandlerRoutes(router);

    app.use('/app/v1', router)
}
