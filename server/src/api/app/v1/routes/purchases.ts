import * as express from 'express';

import GoogleStoreNotificationsCtrl from '../controllers/notificationsGoogleStore';
import AppleStoreNotificationsCtrl from '../controllers/notifications-apple-store';
import PurchasesCtrl from '../controllers/purchases';

/**
* Configure Purchase routes
* @param router: express.Router
*/
export default function setPurchasesRoutes(router: express.Router) {

    const purchasesCtrl = new PurchasesCtrl();
    const googleStoreNotificationsCtrl = new GoogleStoreNotificationsCtrl();
    const appleStoreNotificationsCtrl = new AppleStoreNotificationsCtrl();

    router.route('/purchases/addIOSReceiptValidationSecret').post(purchasesCtrl.addIOSReceiptValidationSecret);
    router.route('/purchases/validate').post(purchasesCtrl.validateReceipt);
    router.route('/purchases/addIOSReceiptValidationSecret').post(purchasesCtrl.addIOSReceiptValidationSecret);
    router.route('/purchases/getReceipt').post(purchasesCtrl.getReceipt);
    // router.route('/purchases/validateTest').post(purchasesCtrl.validateReceiptTesting);
    router.route('/purchases/notifications/google').post(googleStoreNotificationsCtrl.handleNotificationEvent);
    router.route('/purchases/notifications/apple').post(appleStoreNotificationsCtrl.handleNotificationEvent);
}


