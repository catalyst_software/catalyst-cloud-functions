import * as express from 'express';

import WarehouseCtrl from '../controllers/warehouse';

/**
* Configure Athlete routes
* @param router: express.Router
*/
export default function setWarehouseRoutes(router: express.Router) {

    const warehouseCtrl = new WarehouseCtrl();
 
    router.route('/warehouse/dimension/athlete/migrate').post(warehouseCtrl.migrateAthleteDimension);
    router.route('/warehouse/dimension/athlete/reset').post(warehouseCtrl.resetAthleteDimensionTable);

    router.route('/warehouse/dimension/segment/migrate').post(warehouseCtrl.migrateSegmentDimension);
    router.route('/warehouse/dimension/segment/reset').post(warehouseCtrl.resetSegmentDimensionTables);

    router.route('/warehouse/dimension/program/migrate').post(warehouseCtrl.migrateProgramDimension);
    router.route('/warehouse/test/olap/query').post(warehouseCtrl.testOlapQuery);
}
