import * as express from 'express';

import ProgramsCtrl from '../controllers/programs';

/**
* Configure Program routes
* @param router: express.Router
*/
export default function setProgramsRoutes(router: express.Router) {

    const programsCtrl = new ProgramsCtrl();
 
    router.route('/programs/add').post(programsCtrl.addProgram);
    router.route('/programs').get(programsCtrl.getAllPrograms);
    router.route('/programs/:id').get(programsCtrl.getProgram);

    // Registrations
    router.route('/programs/register').post(programsCtrl.resgisterProgramToAthlete);

    router.route('/programs/register/athlete').post(programsCtrl.registerAthlete);
    router.route('/programs/register/group').post(programsCtrl.registerGroup);
    router.route('/programs/register/organization').post(programsCtrl.registerOrganization);
    router.route('/programs/add/content').post(programsCtrl.addContentToAthleteProgram);
    router.route('/programs/copyProgramsToDev').post(programsCtrl.copyProgramsToDev);

}
