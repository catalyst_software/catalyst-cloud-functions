import * as express from 'express';

import BiometricsCtrl from '../controllers/biometrics';

/**
* Configure Biometrics routes
* @param router: express.Router
*/
export default function setBiometricsRoutes(router: express.Router) {

    const biometricsCtrl = new BiometricsCtrl();

    // dev -- set dynamic logging level :)
    // router.route('/biometrics/:lifestyleEntryType/:value/:logLevel?').get(biometricsCtrl.addBiomentricEntry);

    router.route('/biometrics/:lifestyleEntryType/:value/:count?/:logLevel?').get(biometricsCtrl.addBiomentricEntryMultiple);
    router.route('/biometrics').post(biometricsCtrl.addBiomentricEntryMultiple);

}
