import * as express from 'express';
import VideosCtrl from '../controllers/videos';



/**
* Configure Video routes
* @param router: express.Router
*/
export default function setVideosRoutes(router: express.Router) {

    const videosCtrl = new VideosCtrl();
 
    router.route('/videos').post(videosCtrl.addVideo);

}
