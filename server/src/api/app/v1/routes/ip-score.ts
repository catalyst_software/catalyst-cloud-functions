import * as express from 'express';

import IPScoresCtrl from '../controllers/ip-scores';

/**
* Configure IP Score routes
* @param router: express.Router
*/
export default function setIPScoreRoutes(router: express.Router) {

    const ipScoreCtrl = new IPScoresCtrl();
 
    router.route('/ipscore').post(ipScoreCtrl.aggregateIpScore);
    router.route('/ipscore/nonaggregate').post(ipScoreCtrl.runNonAggregateIpScore);
    router.route('/ipscore/resetAthletesIpScore').post(ipScoreCtrl.resetAthletesIpScore);

}
