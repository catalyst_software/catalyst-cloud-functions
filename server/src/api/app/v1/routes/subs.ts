import * as express from 'express';

import SubsCtrl from '../controllers/subs';

/**
* Configure Survey routes
* @param router: express.Router
*/
export default function setSurveysRoutes(router: express.Router) {

    const surbsCtrl = new SubsCtrl();
 
    router.route('/subs').get(surbsCtrl.getAllRecords);
    router.route('/subs/:id').get(surbsCtrl.getReport);


}
