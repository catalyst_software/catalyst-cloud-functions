import * as express from 'express';

import NotificationsCtrl from '../controllers/notifications';

import CopyEmailTemplateCtrl from '../controllers/tests/copy-email-template';

/**
* Configure Notification routes
* @param router: express.Router
*/
export default function setNotificationsRoutes(router: express.Router) {

    const notificationsCtrl = new NotificationsCtrl();
    const copyEmailTemplateCtrl = new CopyEmailTemplateCtrl();

    router.route('/notifications/sendDevice').post(notificationsCtrl.sendMessage);
    router.route('/notifications/templates/add').post(notificationsCtrl.addTemplate);
    router.route('/notifications/templates/msCalendarNotifications').post(notificationsCtrl.msCalendarNotifications);

    router.route('/notifications/copyEmailTemplatesToDev').post(copyEmailTemplateCtrl.copyEmailTemplatesToDev);
    router.route('/notifications/copyNotificationTemplatesToDev').post(copyEmailTemplateCtrl.copyNotificationTemplatesToDev);
}
