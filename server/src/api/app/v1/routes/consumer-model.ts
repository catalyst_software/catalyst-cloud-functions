import * as express from 'express';

import ConsumerModelCtrl from '../controllers/consumer-model';

/**
* Configure Consumer Model routes
* @param router: express.Router
*/
export default function setConsumerModelsRoutes(router: express.Router) {

    const consumerModelCtrl = new ConsumerModelCtrl();
 
    router.route('/consumermodel').post(consumerModelCtrl.saveConsumerModel);
    router.route('/consumermodel/program').post(consumerModelCtrl.updateProgram);
    router.route('/consumermodel').get(consumerModelCtrl.getConsumerModel);

}
