import * as express from 'express';

import WarehouseCtrl from '../controllers/warehouse';
import AsyncJobQueueCtrl from '../controllers/asyncJobQueue';

/**
* Configure Athlete routes
* @param router: express.Router
*/
export default function setAsyncJobQueueRoutes(router: express.Router) {

    const asyncJobCtrl = new AsyncJobQueueCtrl();
 
    router.route('/asyncJobs/test').post(asyncJobCtrl.addTestJob);
    router.route('/asyncJobs/test').get(asyncJobCtrl.getTestJob);
}
