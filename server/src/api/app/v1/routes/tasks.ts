import * as express from 'express';

import TasksCtrl from '../controllers/tasks';



/**
* Configure Video routes
* @param router: express.Router
*/
export default function setTasksRoutes(router: express.Router) {

    const tasksCtrl = new TasksCtrl();
 
    router.route('/tasks/list').post(tasksCtrl.getAllTasks);

    router.route('/tasks/updateProvisioning').post(tasksCtrl.updateProvisioning);

}
