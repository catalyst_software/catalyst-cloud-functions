import * as express from 'express';

import WhiteLabelsCtrl from '../controllers/whiteLabels';

/**
* Configure Group routes
* @param router: express.Router
*/
export default function setGroupsRoutes(router: express.Router) {

    const whiteLabelsCtrl = new WhiteLabelsCtrl();
 
    // router.route('/groups').post(groupsCtrl.addGroup);
    router.route('/whiteLabels/templates').get(whiteLabelsCtrl.getTemplates);
    
}
