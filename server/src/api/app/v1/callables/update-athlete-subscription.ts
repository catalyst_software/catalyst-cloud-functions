import * as functions from 'firebase-functions'


import { updateAthleteSubscription as updateAthleteSubscriptions } from './utils/update-athlete-subscription';
import { AthleteUpdateSubscriptionRequest } from '../../../../models/athlete/interfaces/athlete-subscription';
import { isArray } from 'lodash';
import { Config } from './../../../../shared/init/config/config';

export const updateAthleteSubscription = functions.https._onCallWithOptions(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('data => ', data)


            const { uid: athleteUID } = context.auth.token;


            const {
                // uid: athleteUID,
                subscription,
                includeAthlete,
                includeGroups,
                groupIds }
                // = <{
                //     uid: string, ipScore: string, runningTotalIpScore: string,
                //     subscription: {
                //         status: number,
                //         type: number
                //     }
                //     groupIds: string | Array<string>
                // }>data
                = <AthleteUpdateSubscriptionRequest>data


            const theGroupIds = isArray(groupIds)
                ? () => {
                    console.log('isArray(groupIds) => ', isArray(groupIds));
                    return groupIds
                }
                : () => {
                    console.log('isArray(groupIds) => ', isArray(groupIds));
                    return JSON.parse(groupIds) as Array<string>
                }

            const updateResults = await updateAthleteSubscriptions(
                athleteUID, theGroupIds(),
                subscription,
                // { 
                //     ...subscription, 
                //     expirationDate: timestampFromDateString(subscription.expirationDate as any),
                //     commencementDate: timestampFromDateString(subscription.commencementDate as any)
                // },
                includeAthlete,
                includeGroups
            );

            if (updateResults.length) {

                // const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName
                const name = athleteUID
                console.warn(`Athlete Subscription Group Indicators update results for ${name}`);

                return {
                    message: `Athlete Subscription Group Indicators update results for ${name}`,
                    updateResults
                }

            }
            else {
                console.warn(`No Athlete Subscription GroupIndicator update results ${athleteUID}`);

                return {
                    message: `No Athlete Subscription GroupIndicator update results ${athleteUID}`
                }
            }


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)
