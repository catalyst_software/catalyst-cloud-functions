import * as functions from 'firebase-functions'

import { resetAthleteIndicatorsDailyIPScore } from '../../../../analytics/triggers/reset-athlete-indicators-daily-ipscore';
import { isArray } from 'lodash';

export const crossProjectTest = functions.https.onCall(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('data => ', data)

            console.log('context => ', context)

                return {
                    message: `Function Called`,
                }

            // const {
            //     // uid: athleteUID,
            //     ipScore, runningTotalIpScore, groupIds, utcOffset }
            //     = <{ uid: string, ipScore: string, runningTotalIpScore: string, groupIds: string | Array<string>, utcOffset: number }>data
            // console.log({
            //     // uid: athleteUID,
            //     ipScore, runningTotalIpScore, groupIds, utcOffset
            // })

            // const theGroupIds = isArray(groupIds)
            //     ? () => {
            //         console.log('isArray(groupIds) => ', isArray(groupIds));
            //         return groupIds
            //     }
            //     : () => {
            //         console.log('isArray(groupIds) => ', isArray(groupIds));
            //         return JSON.parse(groupIds) as Array<string>
            //     }

            // const theDailyIPScore = (ipScore !== undefined) ? +ipScore : 0
            // const therunningTotalIpScore = (runningTotalIpScore !== undefined) ? +runningTotalIpScore : undefined
            // debugger;

            // console.log('=====>>>>> theDailyIPScore', theDailyIPScore)
            // console.log('=====>>>>> therunningTotalIpScore', therunningTotalIpScore)
            // const updateResults = await resetAthleteIndicatorsDailyIPScore(theGroupIds(), therunningTotalIpScore, theDailyIPScore, utcOffset || 0);

            // if (updateResults.length) {

            //     // const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName

            //     return {
            //         message: `IPScore Group Indicators update results for ${updateResults[0].groupUID}`,
            //         updateResults
            //     }

            // }
            // else {
            //     console.warn(`No ipscore GroupIndicator update results ${groupIds}`);

            //     return {
            //         message: `No ipscore GroupIndicator update results ${groupIds}`
            //     }
            // }


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return error
        }
    }
)
