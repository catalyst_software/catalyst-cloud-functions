import { firestore } from "firebase-admin";
import { cloneDeep, isArray } from "lodash";

import { ManageAthleteGroupRequest } from "../../../../models/report-model";
import { Group } from "../../../../models/group/interfaces/group";
import { getGroupDocRef, getAthleteDocRef, getOrgDocRef } from "../../../../db/refs";
import { Athlete } from "../../../../models/athlete/athlete.model";

import { Organization } from "../../../../models/organization.model";
import { AthleteIndicator } from "../../../../db/biometricEntries/well/interfaces/athlete-indicator";
import { getUserByEmail } from "../controllers/get-user-by-email";
import { OrganizationIndicator } from "../../../../models/athlete/interfaces/athlete";

import { updateAthlete } from "./moveAthleteToGroup";
import { removeUndefinedProps } from "../../../../db/ipscore/services/save-aggregates";
import { getUniqueListBy } from "./utils/getUniqueListBy";
import { Config } from './../../../../shared/init/config/config';


const functions = require('firebase-functions');

export const addAthleteToGroup = functions.https._onCallWithOptions(
  async (data: ManageAthleteGroupRequest, context) => {

    try {

      if (!context.auth.token.uid) {
        console.error(
          'Request not authorized. User must logged in to fulfill this request'
        )
        return {
          error:
            'Request not authorized. User must logged in to fulfill this request ' +
            data,
        }
      }

      // const { uid, reportType, requestDateTime: requestDateTimeString, requestProcessed, requestSuccess, requestProcessedDateTime, requestingUser, entity, errorLogs } = data;

      const { groupId, userId, coachFlag, orgUID: theOrgID, currentOrgName: orgName, firebaseMessagingIds } = <{
        groupId: string, userId: string, coachFlag?: boolean, moveUserOrg?: boolean,
        orgUID: string, currentOrgName: string, organizationCode: string,
        firebaseMessagingIds: string[]
      }>data;
      console.log(data);

      // const group = {
      //     athletes: [],
      //     creationTimestamp: new Date(),
      //     groupId: "8aRllwKnpOw01lvhB2j8-QLD1",
      //     groupIdentifier: "QLD1",
      //     groupName: "Diving Australia: Queensland",
      //     logoUrl: "https://s3-ap-southeast-2.amazonaws.com/piano.revolutionise.com.au/logos/yuojmbb9cnx1akfn.png",
      //     organizationId: "8aRllwKnpOw01lvhB2j8",
      //     uid: "8aRllwKnpOw01lvhB2j8-QLD1"
      // }

      let group = {} as Group;
      let pass = true;
      let oldAthlete: Athlete;
      if (pass && groupId && userId) {
        console.log('running add to group')
        return getGroupDocRef(groupId).get()
          .then(async (groupSnap) => {
            if (groupSnap.exists) {
              console.log('group found')

              group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
              if (!group.athletes) {
                group.athletes = []
              }
              const atheleteRef = getAthleteDocRef(userId);// getAthleteDocRef('5FqVZGNUqVV9rPpo48CtqPB8xYE3').get()
              await atheleteRef.get()
                .then(async (athleteSnap) => {
                  if (athleteSnap.exists) {

                    const orgUID = theOrgID || group.organizationId;
                    const theAthlete = { ...athleteSnap.data(), uid: athleteSnap.id } as Athlete;
                    oldAthlete = cloneDeep(theAthlete);
                    console.log('athlete found', { theAthlete })
                    console.log('payload', { payload: data })

                    // TODO: Test All this


                    if (!theAthlete.organizations || theAthlete.organizations.find((org) => org.organizationId === group.organizationId) === undefined) {

                      if (!theAthlete.organizations) {
                        theAthlete.organizations = [{
                          organizationId: orgUID || group.organizationId,
                          organizationName: orgName || group.organizationName,
                          organizationCode: 'UNKOWN',
                          active: true,
                          joinDate: firestore.Timestamp.now()
                        }]
                      } else {
                        debugger;


                        const currOrg = theAthlete.organizations.find((org) => org.organizationId === theAthlete.organizationId);

                        if (currOrg && !currOrg.joinDate) {

                          currOrg.joinDate = theAthlete.metadata.creationTimestamp;

                        }
                        const currNewOrg = theAthlete.organizations.find((org) => org.organizationId === orgUID);

                        if (!currNewOrg) {
                          theAthlete.organizations.unshift({
                            organizationId: group.organizationId,
                            organizationName: group.organizationName,
                            organizationCode: 'UNKOWN',
                            active: true,
                            joinDate: firestore.Timestamp.now()
                          })
                        } else {
                          currNewOrg.active = true;
                        }

                      }
                    }


                    if (!theAthlete.groups) {
                      theAthlete.groups = []
                    }
                    if (!theAthlete.metadata.creationTimestamp) {
                      const user = await getUserByEmail(theAthlete.profile.email || theAthlete.email)
                      theAthlete.metadata.creationTimestamp =
                        firestore.Timestamp.fromDate(new Date(user.metadata.creationTime))
                      theAthlete.metadata.lastSignInTimestamp =
                        firestore.Timestamp.fromDate(new Date(user.metadata.lastSignInTime))
                    }
                    try {
                      if (!theAthlete.metadata.creationTimestamp && theAthlete.profile.onboardingCode.toUpperCase() === 'INSPIRE') {
                        theAthlete.metadata.creationTimestamp = theAthlete.groups[0].creationTimestamp
                      }
                    } catch (error) {
                      console.error('Athlete Creation Timestamp Undefinded, setting to now', firestore.Timestamp.now().toDate())
                      theAthlete.metadata.creationTimestamp = firestore.Timestamp.now()
                    }
                    // 96WTqSOTgxSBMYDMBsrs
                    const newAthleteOrg = theAthlete.organizations[0]
                    await getOrgDocRef(newAthleteOrg.organizationId).get()
                      .then(async (orgSnapshot: firestore.DocumentSnapshot) => {
                        if (orgSnapshot.exists) {
                          const newOrg = { ...orgSnapshot.data(), uid: orgSnapshot.id } as Organization
                          // const orgUID = newOrg.uid;
                          // const newOrg = await getOrgDocFromSnapshot(orgUID)
                          console.log('org found')

                          let isCoach = coachFlag ? coachFlag : false;

                          const theEmail = theAthlete.email || theAthlete.profile.email
                          const isCoachEmail = newOrg.coaches.find((email) => email.toLowerCase() === theEmail.toLowerCase())
                          if (isCoachEmail && isCoachEmail.length !== 0) {
                            console.warn('isCoach !== isCoachEmail, setting to ${isCoachEmail}')
                            isCoach = true
                          }

                          // if (moveUserOrg) {
                          //     newAthleteOrg.active = false;
                          //     theAthlete.organizations.unshift({
                          //         active: true,
                          //         organizationId: currentOrg.uid,
                          //         organizationName: currentOrg.name,
                          //         organizationCode: currentOrg.organizationCode,
                          //         joinDate: firestore.Timestamp.now()
                          //     })
                          // } else {
                          const currOrg: OrganizationIndicator = theAthlete.organizations.find((org) => org.organizationId === newOrg.uid);

                          currOrg.active = true;
                          currOrg.organizationId = newOrg.uid;
                          currOrg.organizationName = newOrg.name;
                          currOrg.organizationCode = newOrg.organizationCode;
                          // }


                          const athlete: AthleteIndicator = {
                            uid: theAthlete.uid,
                            // TODO:
                            // organizations: moveUserOrg ? {organizationId: newOrg.uid} : theAthlete.organizations,
                            // organizations: theAthlete.organizations,
                            // organizationId: moveUserOrg ? orgUID || group.organizationId : newAthleteOrg.organizationId,
                            // organizationId: newAthleteOrg.organizationId,
                            // organizationName: moveUserOrg ? group.organizationName : currentAthleteOrg.organizationName,

                            firstName: theAthlete.profile.firstName,
                            lastName: theAthlete.profile.lastName,
                            email: theAthlete.profile.email || theAthlete.email,
                            profileImageURL: theAthlete.profile.imageUrl,

                            isCoach,

                            subscriptionType: theAthlete.profile.subscription.type,
                            includeGroupAggregation: !isCoach,
                            ipScore: theAthlete.currentIpScoreTracking.ipScore || 0,
                            runningTotalIpScore: theAthlete.runningTotalIpScore || 0,
                            isIpScoreDecrease: false,
                            isIpScoreIncrease: false,
                            orgTools: theAthlete.orgTools,
                            bio: theAthlete.profile.bio || '',
                            firebaseMessagingIds: firebaseMessagingIds || theAthlete.firebaseMessagingIds || ['NOT-SET'],
                            creationTimestamp: theAthlete.metadata.creationTimestamp,
                            organizations: theAthlete.organizations
                          };
                          console.warn('------------------------>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', athlete.firebaseMessagingIds);


                          if (athlete.firebaseMessagingIds[0] === 'NOT-SET') {
                            // TODO: Sort Getting ID!!
                            console.error(`firebaseMessagingId NOT SET FOR USER ${athlete.firstName}, ${athlete.uid}`)

                          }
                          // if (!athlete.firebaseMessagingIds) {
                          //   console.warn('firebaseMessagingIds not set for ' + athlete.firstName, athlete.uid)
                          //   athlete.firebaseMessagingIds = firebaseMessagingIds || []
                          // }
                          const clonedAthletes = cloneDeep(group.athletes);
                          const groupAthletes = getUniqueListBy(clonedAthletes, 'uid') as AthleteIndicator[]
                          
                          if (groupAthletes.length !== clonedAthletes.length) {
                            console.warn('dup athletes removed', {
                              groupAthletesWithDups: clonedAthletes.map((a) => {
                                return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                              }),
                              groupAthletesCleaned: groupAthletes.map((a) => {
                                return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                              })
                            })
                          }
  


                          const currentAthlete = groupAthletes.find(ath => ath.uid === theAthlete.uid);
                          if (!currentAthlete)
                            groupAthletes.push(athlete);
                          groupAthletes.forEach((a, i) => {

                            console.log('group athlete ' + i)
                            console.log('firebaseMessagingIds ' + a.firebaseMessagingIds)
                            if (!isArray(a.firebaseMessagingIds)) {

                              console.error('clearing firebaseMessagingIds for ', a)
                              // a.firebaseMessagingIds = []
                            }

                          })
                          console.log('updating group')

                          console.warn({ athlete })
                          console.warn({ group })

                          const theUpdate = {
                            ...group,
                            athletes: groupAthletes,
                            organizationName: newOrg.name,
                            sdsd: newOrg.organizationCode
                          };
                          console.warn({ theUpdate })


                          console.warn({
                            theUpdate: theUpdate.athletes.map((a) => {
                              return {
                                uid: a.uid,
                                name: a.firstName + ' ' + a.lastName,
                                firebaseMessagingIds: a.firebaseMessagingIds
                              }
                            })
                          })

                          removeUndefinedProps(theUpdate)
                          
                          theUpdate.athletes.forEach((a, i) => {

                            console.log('group athlete ' + i)
                            if (!!a.firebaseMessagingIds) {
                              console.error('clearing firebaseMessagingIds for ', a)
                              a.firebaseMessagingIds = []
                            }

                          })

                          try {

                            const groupUpdate = await groupSnap.ref.update(theUpdate)
                              .catch((e) => {
                                console.error('updating group failed', e)
                                throw e
                              });



                            console.log('group updated', groupUpdate)

                          } catch (error) {

                            console.error('group update failed', error)
                          }

                          // TODO: Check This
                          let onboardingCode = theAthlete.profile.onboardingCode
                          // if (moveUserOrg || onboardingCode.toUpperCase() === 'INSPIRE') {
                          //     onboardingCode = newOrg.organizationCode
                          // }

                          // if (onboardingCode.toUpperCase() === 'INSPIRE') {
                          // onboardingCode = `${newOrg.organizationCode}-${group.groupIdentifier}`
                          // }

                          const updatedAthlete: Athlete = {
                            ...theAthlete,
                            isCoach,
                            includeGroupAggregation: athlete.includeGroupAggregation,
                            // organizationId: athlete.organizationId,
                            organizations: theAthlete.organizations,
                            uid: athlete.uid,
                            profile: {
                              ...theAthlete.profile,
                              onboardingCode: onboardingCode
                            }
                          }

                          // TODO: Check orgID passed here
                          console.log('updating athlete')

                          return updateAthlete({
                            ...group, athletes: groupAthletes,
                            organizationName: group.organizationName || newOrg.name
                          },
                            updatedAthlete, oldAthlete, atheleteRef, coachFlag, true,
                            newAthleteOrg.organizationId)
                            .then(() => {
                              console.log('athlete updated')
                              return {
                                message: 'Athlete Group Updated',
                                responseCode: 201,
                                // theDdata,
                                data: {
                                  group,
                                },
                                method: {
                                  name: 'UpdateAthlete',
                                  line: 58,
                                },
                              }
                            })
                            .catch((e) => {
                              console.log(e)
                              console.error({ message: 'Unable to update athlete group details', e })
                              return { message: 'Unable to update athlete group details', e }
                            })



                        } else {
                          console.log('Group Not Found')
                          throw new Error('Group Not Found')
                        }

                      })
                      .then((writeResult) => {
                        console.log(writeResult);
                        return writeResult
                      })
                    // })

                    //includeGroupAggregation: true


                  }

                })

            }
          }).catch((err) => {
            return { error: err }
          })

      } else {
        console.error({ message: 'No ID supplied' })
        return { message: 'No ID supplied' }
      }
    }
    catch (e) {
      console.log('e', e)
      throw e

    }
  }, {regions: Config.regions})
