import { ReportRequest } from "../../../../models/report-model";
import * as pdf from 'pdf-thumbnail';
import { theMainApp } from "../../../../shared/init/initialise-firebase";
import * as storage from '@google-cloud/storage';

const functions = require('firebase-functions');


export const getPPDFImage = functions.https.onCall(
  async (data: ReportRequest, context) => {

    try {

      if (!context.auth.token.uid) {
        console.error(
          'Request not authorized. User must logged in to fulfill this request'
        )
        return {
          error:
            'Request not authorized. User must logged in to fulfill this request ' +
            data,
        }
      }

      const { filePath } = data as any;

      console.log({ filePath });
      // const getReadStream = (theFilePath) => {




      // Your Google Cloud Platform project ID
      const projectId = 'inspire-1540046634666';
      console.log({ projectId });
      // Creates a client
      const thestorage = new storage.Storage({
        projectId,
        // clientEmail:
        //     'firebase-adminsdk-yabfi@inspire-1540046634666.iam.gserviceaccount.com',
        // privateKey:
        //     '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDEa8dBKl8pFlrH\nEtlOFu5Pajeg6dhmCOKwISfdO2WYEaWtM80kzGxrxfkw+ePJO4+WzigOm9FcSwhk\nMTlJovU4S1G1vP/tF3uAeNstqgQiENnDuQtbFb2FzhUFMXptkcTfBuoe2vmyn4pk\neBTn63HOUmaD6doU8HtikdQsb5uaJJCpH5VKaPCPAYtLelV/h/8eAu+1dQcoShEY\noY808KXJzzwtWnLHUBnb/s7iUQUVfWH1JP5OTujkxdM7B11KRB3JX9PsexS/+tVJ\niZVbxPIr5CzmM+fjKD/Gico/07IqyloPYzBZ0CSaTRJrEtpiebWXBMHPODsHsI9p\na1KZdkwbAgMBAAECggEAC6WQZQ7MIbWPoGo/tF+rtc3IiqQTDsnMF0GACaAx//hb\n6I8/xMTSUPmmMv8+QHploz2KJoOawGw6jSZWDUW6YKImsC4KxtYznoSMCoMgR5zU\nLMTWJYp+eGal3G74oXKZR9gzHa1CTRMks5xjMPVHBELijUPaiI9R4aBgAlqD0Xnq\n1buKaF9Mvtha3jvHsrWk0mJeyoR5SsQ5o718RVJ5k/xuS4/nc5uOz33SG/xxKyBa\nMuFomisEP92QpTbksut87/FlsQd7XgnPkp+r7UL9olGd8gByII3islpsmEhgrOPT\nmnGpCBjD9pzifTUMQLcZKCmnymfRFRevv3CS5Gk+cQKBgQD4rY8XU/IMoFPiE6FD\nbvHv8+2b0P54uka7GN49Yx7riw73EG9SmgZCTT6V2jhlxXv8kV023085RTXQdPCr\ng0s2kvIJxU4quiZeJBquG1Scji2DSlMKv9tOZNswb4K2x+ZSDZ9GTvfXj/09NDnk\n2jJMRp8K826bI7RwHp03F90+ZwKBgQDKNFN68CjRWGe3zd/LaW6yAwFa8t9yJwdk\nwgcKfKgIm+a5UkQgE8sQ7wWvGMrxSuYJFH1fHIuVFRmpQsJgfydKVHJs16ZdvDun\nvDXy6psYhvggm9pEf+xqct4702oVhaQY/Ee0K6pQgcH966VYwmz4USJj+woWJYxW\nr9cLf76MLQKBgQDP6QPOhC/F4LHhPXpBj9uVO8L32Dc5prwGJ1d/yYSLd/ruE36P\neBkti7l8vjMS25a65qohe3iYMEY639pr+1yB5z+Xba/Zx0LWyKbJ1C3cqn5g214s\niZWIqIgdqc2GlgD5r0vwE4vhXRBkAGs67DbLUOwd0sMx0BtG9kGJU1l1lwKBgQDE\nrCCGcxFAjbxUCuqh7uq8OjAXRiQP4+ZNGmu+x4Co3vqLRnj8ukPJNLNSm8rI5xDX\nxBYtbJZXay6Kc2ScdxDAO2MQerBWe7+KZoYSwB4avSyaivzBo6tP3mpJxlholpQF\nuVwE4nPF2m/Vil5I9tMGs+O/W210HRFjP6TqilXMAQKBgGfbZaGFD3xqSYt+3dA+\nyF7w2/4Bj8xzBmKDQQj2NPan/5V1cWXxGfXNqLDgmxqzsJ+cD6yhPd6t55l2yIcV\nuEWg8XhOq3l0jU+Fh8hD0lHqeFH7yxpp2ymzMjCg3nbq90h9fZqNvHzZSI2cUoCJ\n8UAojg1vDxolRkag4gmGVa7s\n-----END PRIVATE KEY-----\n',
      });


      console.log({ thestorage });
      // const dataID = 'image.params.dataID'
      // const childDataID = 'image.params.childDataID'
      // const fileName = 'image.data.val()'

      //get your project storage bucket id
      // const storageBucket = functions.config().firebase.storageBucket
      // console.log({ storageBucket });
      //path to image
      // const imagePath = `${dataID}/${childDataID}`
      //open bucket
      console.log('setting bucket');
      let bucket
      try {
        bucket = thestorage.bucket('INSPIRE/worksheet')
      } catch (error) {
        console.error('EEERRROOORRR', error)
        throw error
      }
      // const bucket = thestorage.bucket('') //gcs.bucket(storageBucket)
      console.log({ bucket: 'wot' });


      return bucket.file(filePath).download((err, files) => {
        if (!err) {

          console.log('files', files)

          if (files.length) {
            console.log('files found')

            return files[0]

          } else {
            console.log('NO files found')

            return 'not found'
          }
        } else {

          console.error('EEERRROOORRR', err)
          throw err

        }
      })
        // .then((files) => {

        // })
        // .catch((e) => {

        // })
      // //location of the image in the bucket
      // const object = bucket.file(theFilePath)

      // const fileBucket = object.bucket; // The Storage bucket that contains the file.
      // const filePath2 = object.name; // File path in the bucket.
      // console.log('fileBucket', fileBucket)
      // console.log('filePath2', filePath2)
      // console.log('theFilePath', theFilePath)
      // // const metadata = {
      // //   contentType: 'image/jpeg'
      // // };

      // // We add a 'thumb_' prefix to thumbnails file name. That's where we'll upload the thumbnail.
      // // const thumbFileName = `thumb_${fileName}`;
      // // const thumbFilePath = path.join(path.dirname(filePath), thumbFileName);
      // // Create write stream for uploading thumbnail
      // // const thumbnailUploadStream = bucket.file(thumbFilePath).createWriteStream({ metadata });

      // // Create Sharp pipeline for resizing the image and use pipe to read from bucket read stream
      // // const pipeline = sharp();
      // // pipeline
      // //   .resize(400, 400)
      // //   .max()
      // //   .pipe(thumbnailUploadStream);


      // // bucket.file(filePath).createReadStream()//.pipe(pipeline);

      // // const streamAsPromise = new Promise((resolve, reject) =>
      // //   thumbnailUploadStream.on('finish', resolve).on('error', reject));

      // // return streamAsPromise.then(() => {
      // //   console.log('Thumbnail created successfully');

      // //   var today = new Date();
      // //   var dd = today.getDate();
      // //   var mm = today.getMonth() + 1; //January is 0!
      // //   const yyyy = today.getFullYear() + 5; // add a few years

      // //   if (dd < 10) {
      // //     dd = '0' + dd
      // //   }

      // //   if (mm < 10) {
      // //     mm = '0' + mm
      // //   }

      // //   today = mm + '-' + dd + '-' + yyyy;

      // //   bucket.file(filePath).getSignedUrl({
      // //     action: 'read',
      // //     expires: today
      // //   }, function (err, url) {
      // //     if (err) {
      // //       console.error(err);
      // //       return;
      // //     }
      // //     //add thumb image url to message (in database)
      // //     return admin.database().ref(`/${dataID}/childData/${childDataID}`).child('image').update({
      // //       thumb: url
      // //     })
      // //   });
      // // });


















      // // let file;
      // try {
      //   // file = admin
      //   //   .storage()
      //   //   .bucket(theFilePath);

      //   const options = {
      //     compress: {
      //       type: 'JPEG',  //default
      //       quality: 70    //default
      //     }
      //   }


      //   //       return admin .storage().bucket.file(filePath).download()
      //   // .then(data => {
      //   //     const contents = data[0];
      //   //     // Do something with the contents constant, e.g. derive the value you want to write to Firestore

      //   //     return db.collection('myCollection').doc().set({
      //   //        value: ......
      //   //     });

      //   // });


      //   // const ret = new Promise((resolve, reject) => {





      //   return bucket.file(filePath).download()
      //   // .then((response: storage.DownloadResponse) /*Stream of the image*/ => {
      //   //   // ...
      //   //   console.log('WE HAVE THE PDF')
      //   //   console.log('response', response)

      //   //   // try {


      //   //   //   pdf(
      //   //   //     response[0], //pdfBuffer, /*Buffer or stream of the pdf*/
      //   //   //     options
      //   //   //   )
      //   //   //     .then(thePDFImageData /*Stream of the image*/ => {
      //   //   //       // ...
      //   //   //       console.log('WE HAVE THE PDF IMAGE')
      //   //   //       // return thePDFImageData
      //   //   //       resolve(thePDFImageData)

      //   //   //     })
      //   //   //     .catch(err => {
      //   //   //       console.error('WE DON\'T HAVE THE PDF IMAGE', err);
      //   //   //       reject(err)
      //   //   //       // throw err;
      //   //   //     })

      //   //   // } catch (err) {
      //   //   //   console.log('PDF READ CATCH ERROR', err);
      //   //   //   reject(err);
      //   //   //   // throw err;
      //   //   // }

      //   // })
      //   // .catch(err => {
      //   //   console.error('WE DON\'T HAVE THE PDF IMAGE', err);
      //   //   // throw err;

      //   //   reject(err)
      //   // })

      //   // bucket.file(filePath).createReadStream()
      //   //   .on('success', (theeee) => {
      //   //     console.log('READ SUCCESS', theeee)



      //   //   })
      //   //   .on('error', (err) => {
      //   //     console.error('READ ERROR', err)
      //   //     reject(err);
      //   //   });

      //   // // console.log('readStream', readStream);
      //   // })

      //   // return ret;

      // } catch (err) {
      //   console.log('PDF CATCH ERROR', err);
      //   throw err;
      // }

      // };
      // return getReadStream(filePath)

    } catch (error) {

      // Otherwise throw an error
      // errorCors(req, res, error);
      return error;
    }
  }
)
export const getPPDFImage2 = functions.https.onCall(
  async (data: ReportRequest, context) => {

    try {

      if (!context.auth.token.uid) {
        console.error(
          'Request not authorized. User must logged in to fulfill this request'
        )
        return {
          error:
            'Request not authorized. User must logged in to fulfill this request ' +
            data,
        }
      }

      const { filePath } = data as any;

      console.log({ filePath });
      const getReadStream = (theFilePath) => {
        let file;
        try {
          file = theMainApp
            .storage()
            .bucket(theFilePath);

          const options = {
            compress: {
              type: 'JPEG',  //default
              quality: 70    //default
            }
          }


          //       return admin .storage().bucket.file(filePath).download()
          // .then(data => {
          //     const contents = data[0];
          //     // Do something with the contents constant, e.g. derive the value you want to write to Firestore

          //     return db.collection('myCollection').doc().set({
          //        value: ......
          //     });

          // });
          const readStream = file.createReadStream()
            .on('error', (err) => {
              throw err;
            });
          console.log(readStream);

          return pdf(
            readStream, //pdfBuffer, /*Buffer or stream of the pdf*/
            options
          )
            .then(thePDFImageData /*Stream of the image*/ => {
              // ...
              console.log('WE HAVE THE PDF IMAGE')
              return thePDFImageData
            })
            .catch(err => console.error('WE DON\'T HAVE THE PDF IMAGE', err))

        } catch (err) {
          console.log('PDF CATCH ERROR', err);
          throw err;
        }

      };
      return getReadStream(filePath)

    } catch (error) {

      // Otherwise throw an error
      // errorCors(req, res, error);
      return error;
    }
  }
)
