import * as functions from 'firebase-functions'

import { db } from '../../../../shared/init/initialise-firebase';

import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { ReportRequest } from '../../../../models/report-model';
import { isProductionProject } from '../is-production';
import { createTask } from '../../../../db/reports/createTask';
import { Config } from './../../../../shared/init/config/config';


export const getEnumNameString = (lifestyleEntryType: LifestyleEntryType) => {
    return LifestyleEntryType[lifestyleEntryType]
}

const generateTaskCode = () => {
    const code = Math.random().toString(36).substring(6, 12);
    return Promise.resolve(code)
    // return this.db.collection(this.collection)
    //   .where('organizationCode', '==', code)
    //   .get()
    //   .then((snapshot) => {

    //     if (snapshot.empty) {
    //       return code
    //     }

    //     else {
    //       return undefined
    //     }
    //   })
}


export const createVideoConversionTask = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            // console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            // console.log('context.auth.token => ', context.auth.token)

            // let {
            //     ...redFlagDataItem
            // } = <SetRedFlagResloveStateRequest>data
            console.log('validateReceipt - data', data)

            const videoConversionRequest = data;

            console.log(data);

            return generateTaskCode().then((taskId) => {

                videoConversionRequest.taskId = taskId;
                videoConversionRequest.creationTimestamp = new Date();
                videoConversionRequest.status = 'conversionQueued'
                videoConversionRequest.message = 'Video convertion Queued'

                console.log('taskId', taskId);

                return db
                    .collection(FirestoreCollection.VideoConversions)
                    .add(videoConversionRequest)
                    .then(() => {

                        const isProd = false;
                        let project = '';
                        let queue = '';
                        let location = '';
                        let options: { payload: ReportRequest; };
                        // // PRODUCTION
                        if (isProductionProject() || isProd) {
                            project = 'inspire-219716';
                            queue = 'create-video-converion-queue';
                            location = 'europe-west3';
                            options = { payload: videoConversionRequest };
                        } else {
                            project = 'inspire-1540046634666';
                            queue = 'create-video-converion-queue';
                            location = 'us-central1';
                            options = { payload: videoConversionRequest };
                        }

                        console.log('options', options);

                        return createTask(project, location, queue, options)
                            .then((theTaskData) => {
                                console.log('Video Conversion Task Created', { taskId, data: theTaskData })
                                return {
                                    message: 'Video Conversion Task Created',
                                    responseCode: 201,
                                    data: { taskId, data: theTaskData, videoConversion: videoConversionRequest },
                                    error: undefined,
                                    method: {
                                        name: 'addVideo',
                                        line: 58,
                                    }
                                }
                            })

                    })
                    .catch(err => {
                        return {
                            message: 'Error adding Video Conversion Task',
                            responseCode: 501,
                            data: {
                                videoConversionRequest
                            },
                            error: err,
                            method: {
                                name: 'getReport',
                                line: 58,
                            },
                        }
                    })
            })


        } catch (error) {

            console.error('.........CATCH ERROR - something went wrong...', error)
            return {
                message: 'setRedFlagResloveState',
                responseCode: 510,
                // data: response,
                error,
                method: {
                    name: 'setRedFlagResloveState',
                    line: 42,
                },
            }

        }
    }, {regions: Config.regions}
)
