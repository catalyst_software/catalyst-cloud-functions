import { UpdateNotificationCardRequestInterface, NotificationCardPackageBase } from "../../../../models/report-model";
import { FirestoreCollection } from "../../../../db/biometricEntries/enums/firestore-collections";
import { db } from "../../../../shared/init/initialise-firebase";

const functions = require('firebase-functions');

export const updateNotificationCardMetadata = functions.https.onCall(
  async (data: UpdateNotificationCardRequestInterface, context) => {

    try {

      if (!context.auth.token.uid) {
        console.error(
          'Request not authorized. User must logged in to fulfill this request'
        )
        return {
          error:
            'Request not authorized. User must logged in to fulfill this request ' +
            data,
        }
      }

      console.log('The Request:', { data })

      const { athleteUID, cardUID, acknowledged, creationTimestamp } = data

      const updateResponse = await db.collection(FirestoreCollection.Athletes)
        .doc(athleteUID)
        .collection(FirestoreCollection.CurrentPrograms)
        .doc(cardUID)

        .get().then((snap) => {
          const card = { ...snap.data(), uid: snap.id } as NotificationCardPackageBase
          const res = card.recipients.find((r) => r.uid === athleteUID)

          res.acknowledgeTimestamp = creationTimestamp
          res.state = acknowledged

          return snap.ref.update({
            recipients: card.recipients
          }).then(() => true).catch((e) => {
            console.error('Unable to update recipient status', e)
            return false
          })
        })

      // const { uid, reportType, requestDateTime: requestDateTimeString, requestProcessed, requestSuccess, requestProcessedDateTime, requestingUser, entity, errorLogs } = data;

      // const requestDateTime = requestDateTimeString as any as string

      // const toDate = moment().subtract(1, 'day').format("YYYYMMDD")// '20180226';

      // console.log('Start Date - inclusive = (date) => ', requestDateTime);
      // console.log('To Date = (date) => ', toDate);

      // const { biometricEntriesMulti, startDate } = await getDataFromBQ(requestDateTime, toDate, entity);
      // let result = { biometricEntriesMulti, startDate, toDate }; //await executeQuery(query);#

      // return result;

      return updateResponse

    } catch (error) {

      // Otherwise throw an error
      // errorCors(req, res, error);
      return error;
    }
  }
)

