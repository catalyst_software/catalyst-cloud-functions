import { firestore } from 'firebase-admin';
import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections';
import { ConsumerModelConfiguration } from '../../interfaces/consumer-model-configuration';
import { getCollectionDocs } from '../../../../../db/refs';

export const getConsumerModelConfig = () => {
    return getCollectionDocs(FirestoreCollection.ConsumerModelConfiguration)
        .then(async (docQuerySnap: firestore.QuerySnapshot) => {
            if (!docQuerySnap.empty) {
                const consumerModelConfigSnap = docQuerySnap.docs[0];

                return consumerModelConfigSnap.data() as ConsumerModelConfiguration;
            }
            else {
                return undefined;
            }
        });
};
