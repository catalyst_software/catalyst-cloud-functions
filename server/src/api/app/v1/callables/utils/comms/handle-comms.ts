import { composeEmailV3 } from './../../../controllers/composeEmail';
import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { EmailMessageTemplate } from "../../../sendGrid/templates/email-message-template";
import { PushNotificationType } from '../../../../../../models/enums/push-notification-type';
import { sendEmail } from '../../../../../../analytics/triggers/send-email';
import { ComposeMessageResult } from '../../../sendGrid/interfaces/compose-message-result';
import { composeEmail, composeEmailV2 } from '../../../controllers/composeEmail';
import { NotificationConfig } from '../../../controllers/athletes';
import { sgKey } from '../../schedule-essential-comms';
import { sendPushNotifications } from "./send-push-notifications";
import { IPScoreNonEngagementTemplate, ReportMessagingTemplate } from '../../../sendGrid/templates/ipscore-non-engagement';
import { KeyValuePair } from '../../../../../../models/notifications/interfaces/schedule-notification-payload';
import { sendCoachDashPushNotification } from '../../../controllers/utils/comms/send-coach-dash-push-notification';
import { NotificationCardType } from '../../../../../../models/enums/UpdateNotificationCardMetadataDirective';
import { PushNotificationsType } from '../../../../../../models/enums/enums.model';
import { db } from '../../../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';
import { LogInfo } from '../../../../../../shared/logger/logger';
import { removeUndefinedProps } from '../../../../../../db/ipscore/services/save-aggregates';
import { Athlete } from '../../../../../../models/athlete/athlete.model';
import { RedFlagNotificationUser } from '../essentialComms/types';


export const handleComms = async (
    emailMessageTemplate: EmailMessageTemplate,
    templateReplacements: {
        email: KeyValuePair[];
        notifications: KeyValuePair[];
    },
    userFullName: string,
    recipient: AthleteIndicator | RedFlagNotificationUser,
    athleteCommsSettings: NotificationConfig,
    includePushNotification: boolean,
    athleteUID: string,
    notificationPath: string,
    userFirstName: string,
    isRedFlags = true,
    appIdentifier?: string) => {

    const { emailTemplateName } = emailMessageTemplate;
    const { message }: ComposeMessageResult = { ...composeEmail(emailMessageTemplate, userFullName) };
    const notificationsResults: {
        email: any;
        push: any;
        sms: any;
    } = {
        email: undefined,
        push: undefined,
        sms: undefined
    };
    if (isRedFlags) {
        const toList = [{
            name: recipient.firstName,
            email: recipient.email
        }];
        console.log('Emailing List', toList);
        const sendMailResult = await sendEmail(toList, message, sgKey);
        notificationsResults.email = sendMailResult;
    } else {
        const toList = [{
            name: recipient.firstName,
            email: recipient.email
        }];
        console.log('Emailing List', toList);
        const sendMailResult = await sendEmail(toList, message, sgKey);
        notificationsResults.email = sendMailResult;
    }


    // Send Push Notification
    if ((athleteCommsSettings.sendPushNotification && includePushNotification)) {
        if (recipient.firebaseMessagingIds && recipient.firebaseMessagingIds.length) {
            if (includePushNotification) {
                const sendPushNotificationResult = await sendPushNotifications(
                    isRedFlags,
                    [recipient],
                    athleteUID,
                    message,
                    emailMessageTemplate.notificationTemplateType,
                    notificationPath,
                    userFirstName,
                    emailTemplateName,
                    templateReplacements.notifications,
                    appIdentifier

                );
                notificationsResults.push = await Promise.all(sendPushNotificationResult);
            }
            else {
                const nonReqMessage = `Not sending Push Notification as it's not requested`;
                console.log(nonReqMessage);
                notificationsResults.push = nonReqMessage;
            }
        } else {
            const nonReqMessage = `Not sending Push Notification as no MessagindID's found for user`;
            console.error(nonReqMessage);
            notificationsResults.push = nonReqMessage;
        }

    }

    // Send SMS
    if (athleteCommsSettings.sendSMS) {
        notificationsResults.sms = 'SMS To Be Implemented!!!';
    }
    return notificationsResults;
};

export const handleCommsV2 = async (emailMessageTemplate: IPScoreNonEngagementTemplate, coach: AthleteIndicator, athleteCommsSettings: NotificationConfig, includePushNotification: boolean) => {

    // const { emailTemplateName } = emailMessageTemplate;
    const { message }: ComposeMessageResult = { ...composeEmailV2(emailMessageTemplate) };
    const notificationsResults: {
        email: any;
        push: any;
        sms: any;
    } = {
        email: undefined,
        push: undefined,
        sms: undefined
    };

    // Send Email
    if (athleteCommsSettings.sendEmail) {
        const toList = [{
            name: coach.firstName,
            email: coach.email
        }];
        console.log('Emailing List', toList);
        const sendMailResult = await sendEmail(toList, message, sgKey);
        notificationsResults.email = sendMailResult;
    }

    // Send Push Notification
    if (athleteCommsSettings.sendPushNotification && includePushNotification) {
        if (includePushNotification) {
            // const sendPushNotificationResult = await sendPushNotifications([coach], athleteUID, message, notificationType, notificationPath, userFirstName);
            // notificationsResults.push = await Promise.all(sendPushNotificationResult);
        }
        else {
            const nonReqMessage = `Not sending Push Notification as it's not requested`;
            console.log(nonReqMessage);
            notificationsResults.push = nonReqMessage;
        }
    }

    // Send SMS
    if (athleteCommsSettings.sendSMS) {
        notificationsResults.sms = 'SMS Sent!!!';
    }
    return notificationsResults;
};

export const handleCommsV3 = async (emailMessageTemplate: ReportMessagingTemplate, coach: AthleteIndicator, athleteCommsSettings: NotificationConfig, includePushNotification: boolean, athleteUID?: string, notificationPath?: string, userFirstName?: string) => {

    const { emailTemplateName } = emailMessageTemplate;
    const { message }: ComposeMessageResult = { ...composeEmailV3(emailMessageTemplate) };
    const notificationsResults: {
        email: any;
        push: any;
        sms: any;
    } = {
        email: undefined,
        push: undefined,
        sms: undefined
    };

    // Send Email
    if (athleteCommsSettings.sendEmail) {
        const toList = [{
            name: coach.firstName,
            email: coach.email
        }];
        console.log('Emailing List', toList);
        const sendMailResult = await sendEmail(toList, message, sgKey);
        notificationsResults.email = sendMailResult;
    }

    // Send Push Notification
    if (athleteCommsSettings.sendPushNotification && includePushNotification) {
        if (includePushNotification) {
            const notificationType = emailTemplateName === 'badMood' ? PushNotificationType.BiometricsMoodBad : PushNotificationType.BiometricsMoodDistraught;
            // TODO: 
            const isRedFlags = true; 3
            const sendPushNotificationResult = await sendPushNotifications(isRedFlags, [coach], athleteUID, message, notificationType, notificationPath, userFirstName);
            notificationsResults.push = await Promise.all(sendPushNotificationResult);
        }
        else {
            const nonReqMessage = `Not sending Push Notification as it's not requested`;
            console.log(nonReqMessage);
            notificationsResults.push = nonReqMessage;
        }
    }

    // Send SMS
    if (athleteCommsSettings.sendSMS) {
        notificationsResults.sms = 'SMS Sent!!!';
    }
    return notificationsResults;
};

export const getPushNoticiationTypeAsString = (type: PushNotificationType): string => {
    return PushNotificationType[type];
}

export const getNotificationCardTypeAsString = (type: NotificationCardType): string => {
    return NotificationCardType[type];
}

export const getAthleteDocs = async (athIndicators: AthleteIndicator[]) => {


    const athleteAgrretgeDocsRefs = new Array<FirebaseFirestore.DocumentReference>();


    try {

        athIndicators.forEach((theAthleteIndicator) => {

            const { uid } = theAthleteIndicator;

            // LogInfo(`Athlete documentId => ${documentId}`);

            const ref = db
                .collection(FirestoreCollection.Athletes)
                .doc(uid);
            athleteAgrretgeDocsRefs.push(ref);


        });

        if (athleteAgrretgeDocsRefs && athleteAgrretgeDocsRefs.length) {
            return await db.getAll(...athleteAgrretgeDocsRefs)
                .then(docs => {

                    LogInfo(`docs.length => ${docs.length}`);
                    const athleteDocuments = docs
                        // .filter(snap => snap.exists)
                        .map(doc => {

                            if (doc.exists) {
                                const entry = doc.data();

                                const athleteDoc = {
                                    uid: entry.id,
                                    ...(entry as Athlete)
                                };

                                LogInfo(`retreived athleteDoc => ${athleteDoc.uid}`);
                                return athleteDoc;
                            } else {
                                return undefined
                            }

                        }).filter((ath) => !!ath);

                    removeUndefinedProps(athleteDocuments);

                    LogInfo(`retreived athleteDocuments => ${athleteDocuments.length}`);

                    return athleteDocuments
                        // collection: getGroupCollectionFromAthleteCollection(collection),
                        // message: `Update ${collection} Docs.`,

                        ;
                });
        } else {
            return Promise.resolve(
                // collection: getGroupCollectionFromAthleteCollection(collection),
                // message: `No athletes to aggregate for ${collection} Document with uid ${groupId}.`,
                undefined
            );
        }


    } catch {
        debugger;
        // LogInfo(`An exceptio occured loading athlete documents for ${collection} Document with uid ${groupId}.`);
        return Promise.reject({
            // collection: getGroupCollectionFromAthleteCollection(collection),
            // message: `An exceptio occured loading athlete documents for ${collection} Document with uid ${groupId}.`,
            athleteDocuments: undefined
        });
    }
};


export const handleInAppNotifications = async (data: {
    senderName: string;
    recipients: AthleteIndicator[];
    cardType: NotificationCardType;
    cardTitle: string,
    notificationType: PushNotificationType;
    notificationPath: string;
    icon: PushNotificationsType;
    sendSMS: boolean;
    useTopic: boolean;
    topicIds?: string[],
    appIdentidier?: string
}) => {

    const notificationsResults: {
        push: any;
        sms: any;
    } = {
        push: undefined,
        sms: undefined
    };
    const { senderName, recipients, cardType, cardTitle, notificationType = PushNotificationType.NewContentFeedItem, notificationPath, icon, sendSMS, appIdentidier } = data;

    const keyValuePairs: KeyValuePair[] = [
        {
            key: '$SENDER_NAME',
            value: senderName
        },
        {
            key: '$CARD_TYPE',
            value: cardType
        },
        {
            key: '$CARD_TITLE',
            value: cardTitle
        },
    ];

    if (!data.useTopic) {

        const athDocs = await getAthleteDocs(recipients) as Array<Athlete>;
        recipients.forEach((r) => {
            if (r) {
                const theAth = athDocs.find((ath) => ath.uid === r.uid);
                r.firebaseMessagingIds = (theAth) ? theAth.firebaseMessagingIds || [] : [];

                if (!r.firebaseMessagingIds.length) {
                    console.error('no firebaseMessagingIds for user', {
                        uid: theAth.uid,
                        name: theAth.profile.firstName + ' ' + theAth.profile.lastName
                    });
                }
            }
        })
    }
    const sendPushNotificationResult = await sendCoachDashPushNotification(
        recipients,
        keyValuePairs,
        notificationType,
        notificationPath,
        icon,
        data.useTopic,
        data.topicIds,
        appIdentidier);

    notificationsResults.push = await Promise.all(sendPushNotificationResult);

    // // Send SMS
    if (sendSMS) {
        notificationsResults.sms = 'SMS Send not implemented!!!';
    }
    return notificationsResults;
};

