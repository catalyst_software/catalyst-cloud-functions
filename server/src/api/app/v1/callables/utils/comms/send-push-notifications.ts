import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator'
import { EmailMessage } from '../../../sendGrid/conrollers/email'
import { PushNotificationType } from '../../../../../../models/enums/push-notification-type'
import { sendPushNotification } from '../../../controllers/utils/comms/send-push-notification'
import { KeyValuePair } from '../../../../../../models/notifications/interfaces/schedule-notification-payload'
import { RedFlagNotificationUser } from '../essentialComms/types'

export const sendPushNotifications = async (
    isRedFlags = true,
    usersToNotify: any[],
    athleteUID: string,
    message: EmailMessage,
    notificationType: PushNotificationType,
    path?: string,
    userFirstName?: string,
    emailTemplateName?: string,
    additionalValuePairs: KeyValuePair[] = [],
    appIdentifier?: string
) => {
    if (!message.notificationTemplateType) {
        console.log(
            '----------------->>>>>>>>>>>>>>>>>>>>>>  notificationType',
            notificationType
        )
        console.log(
            '----------------->>>>>>>>>>>>>>>>>>>>>>  message.notificationTemplateType is',
            message.notificationTemplateType
        )
        console.log(
            '----------------->>>>>>>>>>>>>>>>>>>>>>  message.notificationTemplateType || notificationType',
            `${message.notificationTemplateType || notificationType} to be used`
        )
    }
    return usersToNotify.map(async user => {
        const keyValuePairs: KeyValuePair[] = [
            {
                key: '$ATHLETE_UID',
                value: athleteUID,
            },
            ...additionalValuePairs,
        ]

        const sendResult = await sendPushNotification(
            isRedFlags,
            user,
            message.notificationTemplateType || notificationType,
            emailTemplateName,
            path,
            message.dynamic_template_data.name,
            userFirstName,
            '',
            keyValuePairs,
            appIdentifier
        )

        return sendResult
    })
}
