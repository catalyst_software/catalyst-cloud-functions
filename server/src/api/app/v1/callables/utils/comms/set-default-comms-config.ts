import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { NotificationConfig } from '../../../controllers/athletes';
export const setDefaultCommsConfig = (coach: AthleteIndicator) => {

    const commsSettings: NotificationConfig = {
        sendEmail: true,
        sendPushNotification: !coach.firebaseMessagingIds ? false : true,
        sendSMS: false
    };
    
    return commsSettings;
};
