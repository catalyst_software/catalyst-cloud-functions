import { EmailMessage } from '../../../sendGrid/conrollers/email';
export const emailMessageDefault: EmailMessage = {
    personalizations: [
        {
            to: [
                {
                    name: "Ian Kaapisoft",
                    email: "ian.gouws@kaapisoft.com"
                },
                {
                    name: "Ian iNSPIRE",
                    email: "ian@inspiresportonline.com"
                }
            ]
        }
    ],
    from: {
        name: "iNSPIRE",
        email: "athleteservices@inspiresportonline.com"
    },
    templateId: "d-93756f51e55b4734a5b93d23e4310750",
    emailTemplateName: "badMood",
};
