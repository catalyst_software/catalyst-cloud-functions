import { Group } from '../../../../../../models/group/interfaces/group';
import { AthleteNotificationConfig, NotificationsConfig } from '../../../controllers/athletes';

export const setCurrentGroupConfig = (configRef: FirebaseFirestore.DocumentReference, currentConfig: NotificationsConfig, theGroup: Group, athletes?: AthleteNotificationConfig[]) => {

    let groupConfig = currentConfig ? currentConfig.groups : undefined;
    if (groupConfig) {
        // Group Config already exists for this Coach
        const currentGroupConfig = groupConfig.find(config => config.groupId === theGroup.uid);
        if (!currentGroupConfig) {
            // Group Config Not set, Add it now
            const config = {
                groupId: theGroup.uid,
                // transmitRedFlagEmails: true,
                // transmitRedFlagNotifications: true,
                // transmitSMSotifications: false,
                danger: {
                    sendEmail: true,
                    sendPushNotification: !currentConfig.firebaseMessagingIDs ? false : true,
                    sendSMS: false
                },
                warn: {
                    sendEmail: true,
                    sendPushNotification: !currentConfig.firebaseMessagingIDs ? false : true,
                    sendSMS: false
                },
                success: {
                    sendEmail: true,
                    sendPushNotification: !currentConfig.firebaseMessagingIDs ? false : true,
                    sendSMS: false
                }
            };

            if (athletes)
                groupConfig.push({ ...config, athletes })
            else
                groupConfig.push(config)
        }
        else {
            // TODO: Check current config setting for existing athletes
            // reset Athletes
            if (athletes) {
                currentGroupConfig.athletes = athletes;
            }

        }
    }
    else {
        // Group Config does NOT exist for this Coach, create it now 
        const config = {
            groupId: theGroup.uid,
            // transmitRedFlagEmails: true,
            // transmitRedFlagNotifications: true,
            // transmitSMSotifications: false,
            danger: {
                sendEmail: true,
                sendPushNotification: !currentConfig.firebaseMessagingIDs ? false : true,
                sendSMS: false
            },
            warn: {
                sendEmail: true,
                sendPushNotification: !currentConfig.firebaseMessagingIDs ? false : true,
                sendSMS: false
            },
            success: {
                sendEmail: true,
                sendPushNotification: !currentConfig.firebaseMessagingIDs ? false : true,
                sendSMS: false
            }
        };

        if (athletes)
            groupConfig = [{ ...config, athletes }];
        else
            groupConfig = [config];
    }
    return groupConfig;
};

