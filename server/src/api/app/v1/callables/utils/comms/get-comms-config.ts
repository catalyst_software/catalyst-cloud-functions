import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { NotificationSeverity } from '../../../sendGrid/conrollers/email';
import { AthleteNotificationConfig, NotificationConfig } from '../../../controllers/athletes';
import { setDefaultConfig } from '../../../../../../analytics/triggers/utils/get-athlete-coaches';

export const getCommsConfig = (coach: AthleteIndicator, notificationSeverity: NotificationSeverity, athleteConf: AthleteNotificationConfig): NotificationConfig => {

    switch (notificationSeverity) {
        case NotificationSeverity.Success:
            if (athleteConf) {
              return { ...coach.commsConfig.success, ...athleteConf.success };
            }
            else {
               return coach.commsConfig.success || setDefaultConfig(coach).success;
            }
        case NotificationSeverity.Warn:
            if (athleteConf) {
               return { ...coach.commsConfig.warn, ...athleteConf.warn };
            }
            else {
               return coach.commsConfig.warn || setDefaultConfig(coach).warn;
            }
        case NotificationSeverity.Danger:
            if (athleteConf) {
               return { ...coach.commsConfig.danger, ...athleteConf.danger };
            }
            else {
               return coach.commsConfig.danger || setDefaultConfig(coach).danger;
            }
        default:
           return coach.commsConfig.success || setDefaultConfig(coach).success;
    }
};
