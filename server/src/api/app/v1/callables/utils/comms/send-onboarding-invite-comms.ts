import { AthleteIndicator } from "../../../../../../db/biometricEntries/well/interfaces/athlete-indicator";
import { KeyValuePair } from "../../../../../../models/notifications/interfaces/schedule-notification-payload";
import { NotificationConfig } from "../../../controllers/athletes";
import { OnboardingInviteEmailPayload } from "../../../sendGrid/conrollers/email";
import { EmailMessageTemplate } from "../../../sendGrid/templates/email-message-template";
import { getCommsConfig } from "./get-comms-config";
import { handleComms } from "./handle-comms";
import { setDefaultCommsConfig } from "./set-default-comms-config";

//IAN TODO
export const sendOnboardingInviteComms = async (
    payload: OnboardingInviteEmailPayload,
    emailMessageTemplate: EmailMessageTemplate,
    templateReplacements: {
        email: KeyValuePair[];
        notifications: KeyValuePair[];
    },
    athleteToEmail: AthleteIndicator,
    athleteUID: string,
    userFullName: string,
    userFirstName: string
) => {

    const { includePushNotification, notificationPath } = emailMessageTemplate;

    console.log('athleteToEmail.organizationId', athleteToEmail.organizationId)

    const athleteCommsSettings: NotificationConfig = setDefaultCommsConfig(athleteToEmail);

    emailMessageTemplate.emailTemplateData = {
        ...payload,
        name: payload.userFirstName
    }
    athleteCommsSettings.sendPushNotification = includePushNotification;

    const isRedFlags = false;
    const sendResult = await handleComms(
        emailMessageTemplate,
        templateReplacements,
        userFullName,
        athleteToEmail,
        athleteCommsSettings,
        includePushNotification,
        athleteUID,
        notificationPath,
        userFirstName,
        isRedFlags
    );

    return sendResult;

};
