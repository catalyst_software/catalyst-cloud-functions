// import { FieldValue } from '@google-cloud/firestore';
import { firestore } from "firebase-admin";

import { FirestoreCollection } from "../../../../../../db/biometricEntries/enums/firestore-collections";
import { getAthleteGroupsViaOrg } from '../../../../../../analytics/triggers/utils/get-athlete-groups-via-org';
import { getCoachConfigDocName } from '../get-coach-config-doc-name';
import { setDefaulCommsConfig } from '../../../controllers/utils/comms/set-default-comms-config';
import { getOrgDocRef } from '../../../../../../db/refs';


export const updateMessagingIDInConfig = async (
  athleteUID: string,
  organizationId: string,
  firstName: string,
  lastName: string,
  notificationIds: Array<string>):
  Promise<any> => {

  const orgDocSnapRef = getOrgDocRef(organizationId)

  // const org = getDocumentFromSnapshot(await orgDocSnapRef.get()) as Organization

  const coachGroups = await getAthleteGroupsViaOrg(athleteUID, organizationId)

  // const updatePromises = []

  const updated = await coachGroups.map(async (group) => {

    const isCoach = group.athletes.find((athlete) => athlete.uid === athleteUID).isCoach

    if (isCoach) {
      const coachConfigDocName = getCoachConfigDocName(firstName, lastName, athleteUID)
      const coachConfigRef = await orgDocSnapRef
        .collection(FirestoreCollection.CoachEssentialCommsConfiguration)
        .doc(coachConfigDocName)


      const configUpdates = await coachConfigRef.get()
        .then(async (athleteDocSnap) => {
          if (athleteDocSnap.exists) {
            const configUpdateResult = await coachConfigRef.update({
              firebaseMessagingIDs: firestore.FieldValue.arrayUnion(...notificationIds)
            })
            // .catch((err) => {
            //   console.warn('Coach Comms Config does not exist', err)
            //   return err
            // });



            return configUpdateResult
          } else {

            // const config =  getDocumentFromSnapshot(athleteDocSnap) as NotificationsConfig
            const configSetResults = await setDefaulCommsConfig(organizationId, notificationIds, athleteUID)
            return configSetResults // .map(u => true)

          }
        })

      return configUpdates
    } else
      return [] // false

  })


  // .get()
  // .then((athleteDocSnap) => {
  //   return getDocumentFromSnapshot(athleteDocSnap) as NotificationsConfig
  // })

  // if (config) {

  // } else {

  // }
  // const result = await coachConfig.update({
  //   firebaseMessagingIDs: FieldValue.arrayUnion(...notificationIds)
  // }).catch((err) => {
  //   console.warn('Coach Comms Config does not exist', err)
  // });

  return await Promise.all(updated)
};

