import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { EmailMessageTemplate } from "../../../sendGrid/templates/email-message-template";
import { NotificationConfig } from '../../../controllers/athletes';
import { getCommsConfig } from './get-comms-config';
import { handleComms, handleCommsV2, handleCommsV3 } from './handle-comms';
import { setDefaultCommsConfig } from './set-default-comms-config';
import { IPScoreNonEngagementTemplate, ReportMessagingTemplate } from '../../../sendGrid/templates/ipscore-non-engagement';
import { NonEngagementConfiguration } from '../../../../../../models/organization.model';
import { KeyValuePair } from '../../../../../../models/notifications/interfaces/schedule-notification-payload';

export const notifyCoaches = async (
    emailMessageTemplate: EmailMessageTemplate,
    additionalValuePairs: KeyValuePair[],
    coachesToEmail: AthleteIndicator[],
    athleteUID: string,
    userFullName: string,
    userFirstName: string,
    orgConfigs: Array<{
        orgID: string;
        nonEngagementConfiguration: NonEngagementConfiguration;
    }>,
    appIdentifier?: string) => {
    const { includePushNotification, notificationPath, notificationSeverity, emailTemplateData } = emailMessageTemplate;

    const respose = await coachesToEmail.map(async (coach) => {

        const { notificationTemplateType } = emailMessageTemplate
        let include = true;
        if (coach.excludedNotificationTypes) {
            //TB - TODO: MAKE THIS REFER TO ORG LEVEL NOTIFICATION CONFIG
            include = coach.excludedNotificationTypes.find((v) => v === notificationTemplateType) === undefined;
        }
        console.warn('orgConfigs', orgConfigs)
        console.log('coach.organizationId', coach.organizationId)

        const orgConfig = orgConfigs.find((org) => org.orgID === coach.organizationId)

        if (emailTemplateData) {
            emailTemplateData.dayCountNonEngagement = (orgConfig && orgConfig.nonEngagementConfiguration && orgConfig.nonEngagementConfiguration.dayCountNonEngagement)
                ? orgConfig.nonEngagementConfiguration.dayCountNonEngagement
                : 3
        }

        let athleteCommsSettings: NotificationConfig;
        if (include && coach.commsConfig !== undefined) {
            //TB - TODO: WE WONT USE THIS.  WILL BE GLOBAL ORG LEVELS SETTINGS
            if (coach.commsConfig.athletes) {
                const athleteConf = coach.commsConfig.athletes.find((ath) => ath.uid === athleteUID);
                athleteCommsSettings = getCommsConfig(coach, notificationSeverity, athleteConf);
            }
            else {
                athleteCommsSettings = coach.commsConfig.success || setDefaultCommsConfig(coach);
            }
            if (athleteCommsSettings) {
                const sendResult = await handleComms(emailMessageTemplate, { email: [], notifications: additionalValuePairs }, userFullName, coach, athleteCommsSettings, includePushNotification, athleteUID, notificationPath, userFirstName, true, appIdentifier);
                return sendResult;
            }
            else {
                console.warn('athleteCommsSettings', athleteCommsSettings);
            }
            return false;
        }
        else {
            return false;
        }
    });

    return await Promise.all(respose).then((docs) => {
        console.log('theResult', docs);

        return docs;
    });
};

export const notifyCoachesIPScoreNonEngagement = async (emailMessageTemplate: IPScoreNonEngagementTemplate,
    coachesToEmail: AthleteIndicator[]) => {

    const { includePushNotification,
        // notificationPath, notificationSeverity 
    } = emailMessageTemplate;

    const respose = await coachesToEmail.map(async (coach) => {

        let athleteCommsSettings: NotificationConfig;
        if (coach.commsConfig !== undefined) {
            // if (coach.commsConfig.athletes) {
            //     const athleteConf = coach.commsConfig.athletes.find((ath) => ath.uid === athleteUID);
            //     athleteCommsSettings = getCommsConfig(coach, notificationSeverity, athleteConf);
            // }
            // else {
            athleteCommsSettings = coach.commsConfig.success || setDefaultCommsConfig(coach);
            // }
            if (athleteCommsSettings) {
                const sendResult = await handleCommsV2(emailMessageTemplate, coach, athleteCommsSettings, includePushNotification) //, 'athleteUID', 'notificationPath', 'userFirstName');
                return sendResult;
            }
            else {
                console.warn('athleteCommsSettings', athleteCommsSettings);
            }
            return false;
        }
        else {
            return false;
        }
    });

    return await Promise.all(respose).then((docs) => {
        console.log('theResult', docs);

        return docs;
    });
};

export const notifyCoachesReportsReady = async (emailMessageTemplate: ReportMessagingTemplate,
    coachesToEmail: AthleteIndicator[]) => {

    const { includePushNotification,
        // notificationPath, notificationSeverity
    } = emailMessageTemplate;

    const respose = await coachesToEmail.map(async (coach) => {

        let athleteCommsSettings: NotificationConfig;





        // if (coach.commsConfig.athletes) {
        //     const athleteConf = coach.commsConfig.athletes.find((ath) => ath.uid === athleteUID);
        //     athleteCommsSettings = getCommsConfig(coach, notificationSeverity, athleteConf);
        // }
        // else {
        athleteCommsSettings = setDefaultCommsConfig(coach);
        // }
        if (athleteCommsSettings) {
            const sendResult = await handleCommsV3(emailMessageTemplate, coach, athleteCommsSettings, includePushNotification, 'athleteUID', 'notificationPath', 'userFirstName');
            return sendResult;
        }
        else {
            console.warn('athleteCommsSettings', athleteCommsSettings);
        }
        return false;

    });

    return await Promise.all(respose).then((docs) => {
        console.log('theResult', docs);

        return docs;
    });
};