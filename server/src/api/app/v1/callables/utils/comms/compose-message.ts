import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { EmailMessage } from '../../../sendGrid/conrollers/email';
import { EmailMessageTemplate } from "../../../sendGrid/templates/email-message-template";
import { emailMessageDefault } from "./email-message-default";

export const composeMessage = (emailMessageTemplate: EmailMessageTemplate, userFullName: string, coachesToEmail: AthleteIndicator[]) => {
    
    const message: EmailMessage = {
        ...emailMessageDefault,
        templateId: emailMessageTemplate.templateId || "d-93756f51e55b4734a5b93d23e4310750",
        emailTemplateName: emailMessageTemplate.emailTemplateName || "badMood",
        notificationTemplateType: emailMessageTemplate.notificationTemplateType,
        includePushNotification: emailMessageTemplate.includePushNotification,
        dynamic_template_data: {
            ...emailMessageTemplate.emailTemplateData,
            name: userFullName
        }
    };

    const toList = coachesToEmail
        .map((coach: AthleteIndicator) => {
            return {
                name: coach.firstName,
                email: coach.email
            };
        });

    message.personalizations[0].to = toList;

    return { toList, message };
};
