import { IpScoreTracking } from '../../../../../../db/ipscore/models/documents/tracking/ip-score-tracking.interface';
import { AggregateIpScoreDirective } from '../../../../../../db/ipscore/services/interfaces/aggregate-ipscore-directive';
import { aggregateAthleteIpScoreWeekly } from '../../../../../../db/ipscore/aggregation/aggregators/ipscore/aggregate-athlete-ipscore-weekly';

export const saveIpScoreTracking = async (
    directive: AggregateIpScoreDirective,
    currentIpScoreTracking: IpScoreTracking
    // previous?: IpScoreTracking,
    // runningTotalIpScore?: number
) => {
    // TODO: What to do here?
    // const program = (this.programExecutionService) ? this.programExecutionService.getRunningProgram() : null;
    // const programDay = (this.programExecutionService) ? this.programExecutionService.getRunningProgramDayContent() : null;
    // const program = null;
    // const programDay = null;
    // if ((program && program.isPlaceHolder) || (programDay && programDay.entries.length === 0)) {
    console.log('saveIpScoreTracking: Program is placeholder or no program entries for program day.');
    const programConfig = currentIpScoreTracking.programs;
    programConfig.recordedDailyEntries = 1;
    programConfig.expectedDailyEntries = 1;
    programConfig.videoConsumed = true;
    currentIpScoreTracking.directive.paths.push('quantitative.programs.video');
    // }
    // else {
    //     console.log('saveIpScoreTracking: Program is NOT placeholder.');
    // }
    // TODO: non false
    // currentIpScoreTracking.directive.utcOffset = currentIpScoreTracking.directive.utcOffset || moment().utcOffset();

    // directive.aggregateDocument =
    aggregateAthleteIpScoreWeekly(directive);

    // if (!isNull(runningTotalIpScore) && !isUndefined(runningTotalIpScore)) {
    //     return db.collection('athletes')
    //         .doc(athlete.uid)
    //         .update({
    //             currentIpScoreTracking,
    //             runningTotalIpScore
    //         })
    //         .then(() => {
    //             console.log('Athlete document successfully updated!');
    //             if (previous) {
    //                 return db.collection(`athletes/${athlete.uid}/ipScoreTrackingHistory`)
    //                     .add(previous);
    //             } else {
    //                 return undefined;
    //             }
    //         })
    //         .then(() => {
    //             console.log('Previous IpScoreTracking document saved!');
    //         })
    //         .catch((err) => {
    //             // The document probably doesn't exist.
    //             console.error;
    //         });
    // } else {
    //     return db.collection('athletes')
    //         .doc(athlete.uid)
    //         .update({
    //             currentIpScoreTracking
    //         })
    //         .then(() => {
    //             console.log('Athlete document successfully updated!');
    //             if (previous) {
    //                 return db.collection(`athletes/${athlete.uid}/ipScoreTrackingHistory`)
    //                     .add(previous);
    //             } else {
    //                 return Promise.resolve(undefined);
    //             }
    //         })
    //         .then(() => {
    //             console.log('Previous IpScoreTracking document saved!');
    //         })
    //         .catch((err) => {
    //             // The document probably doesn't exist.
    //             console.error;
    //         });
    // }
    return true
};
