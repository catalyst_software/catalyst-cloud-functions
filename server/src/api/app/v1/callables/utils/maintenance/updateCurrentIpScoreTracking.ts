import { includes, isArray , isNumber} from 'lodash';

import { LifestyleEntryType } from '../../../../../../models/enums/lifestyle-entry-type';
import { AthleteBiometricEntryInterface } from '../../../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { MealComponent } from '../../../../../../db/biometricEntries/well/interfaces/meal-component';
import { AggregateIpScoreDirective } from '../../../../../../db/ipscore/services/interfaces/aggregate-ipscore-directive';
import { saveIpScoreTracking } from "./saveIpScoreTracking";

export const updateCurrentIpScoreTracking = async (directive: AggregateIpScoreDirective, entry: AthleteBiometricEntryInterface, utcOffset: number): Promise<void> => {

    const { athlete } = directive

    let propogate = false;
    let isWearables = false;
    let path = '';

    const config = athlete.ipScoreConfiguration.quantitativeWellness;
    const current = athlete.currentIpScoreTracking.quantitativeWellness;

    switch (entry.lifestyleEntryType) {
        case LifestyleEntryType.Mood:
            if (current.brain.mood.recordedDailyEntries < config.brain.mood.expectedDailyEntries) {
                current.brain.mood.recordedDailyEntries += 1;
                path = 'quantitative.brain.mood';
                propogate = true;
            }
            break;
        case LifestyleEntryType.Sleep:
            if (current.brain.sleep.recordedDailyEntries < config.brain.sleep.expectedDailyEntries) {
                current.brain.sleep.recordedDailyEntries += 1;
                path = 'quantitative.brain.sleep';
                propogate = true;
            }
            break;
        case LifestyleEntryType.Fatigue:
            if (current.body.fatigue.recordedDailyEntries < config.body.fatigue.expectedDailyEntries) {
                current.body.fatigue.recordedDailyEntries += 1;
                path = 'quantitative.body.fatigue';
                propogate = true;
            }
            break;
        case LifestyleEntryType.Pain:
            if (current.body.pain.recordedDailyEntries < config.body.pain.expectedDailyEntries) {
                current.body.pain.recordedDailyEntries += 1;
                path = 'quantitative.body.pain';
                propogate = true;
            }
            break;
        case LifestyleEntryType.Steps:
        case LifestyleEntryType.Distance:
        case LifestyleEntryType.HeartRate:
            current.body.wearables.lastUpdate = new Date();
            if (current.body.wearables.recordedDailyEntries < config.body.wearables.expectedDailyEntries) {
                current.body.wearables.recordedDailyEntries += 1;
                path = 'quantitative.body.wearables';
                propogate = true;
            }
            else {
                isWearables = true;
            }
            current.body.wearables.lastUpdate = new Date();
            break;
        case LifestyleEntryType.Food:
            const components = current.food.components;
            if (components.recordedDailyEntries < config.food.components.expectedDailyEntries) {
                if (!isArray(components.tracking)) {
                    components.tracking = [];
                }
                entry.mealComponents.forEach((c: MealComponent) => {
                    if (!includes(components.tracking, c.mealType)) {
                        components.tracking.push(c.mealType);
                        components.recordedDailyEntries += 1;
                        propogate = true;
                    }
                });
                path = 'quantitative.food';
            }
            break;
        case LifestyleEntryType.Training:
            if (current.training.components.recordedDailyEntries < config.training.components.expectedDailyEntries) {
                current.training.components.recordedDailyEntries += entry.trainingComponents.length;
                propogate = true;
                path = 'quantitative.training';
            }
            break;
    }

    if (propogate) {

        let includeGroup = true;
        if (athlete.isCoach) {
            console.log('User is coach');
            if (athlete.includeGroupAggregation) {
                console.log('Coach is flagged for group aggregation.');
            }
            else {
                console.log('Coach is NOT flagged for group aggregation.');
                includeGroup = false;
            }
        }
        athlete.currentIpScoreTracking.directive = {
            execute: true,
            includeGroupAggregation: includeGroup,
            paths: [path],
            utcOffset: isNumber(utcOffset)
                ? (utcOffset === 0 ? 0 : utcOffset)
                : 10 // Brisbane (Australia - Queensland)
        };

        await saveIpScoreTracking(directive, athlete.currentIpScoreTracking);
    } else if (isWearables) {
        athlete.currentIpScoreTracking.directive = {
            execute: false,
            includeGroupAggregation: false,
            paths: [],
            utcOffset: isNumber(utcOffset)
                ? (utcOffset === 0 ? 0 : utcOffset)
                : 10 // Brisbane (Australia - Queensland)
        };

        await saveIpScoreTracking(directive, athlete.currentIpScoreTracking);
    }

};
