import { calculateIpScoreFromDailies } from '../../../../../../db/ipscore/aggregation/aggregators/ipscore/utils/index';
import { firestore } from 'firebase-admin';
import { db } from '../../../../../../shared/init/initialise-firebase';
import { getDocumentFromSnapshot } from '../../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { getAthleteDocRef } from '../../../../../../db/refs';
import { Athlete } from '../../../../../../models/athlete/interfaces/athlete';
// import { AthleteIpScoreWeeklyAggregateModel } from '../../../../../../db/ipscore/models/athelete-ipscore-weekly-aggregate.model';
import { LifestyleEntryType } from '../../../../../../models/enums/lifestyle-entry-type';
import { IpScoreAggregateCollection } from '../../../../../../db/ipscore/enums/firestore-ipscore-aggregate-collection';
import { HealthComponentType } from '../../../../../../db/biometricEntries/well/enums/health-component-type';
import { groupByMap } from "../../../../../../shared/utils/groupByMap";
import { updateAthleteIPScoreValue } from '../../../controllers/utils/update-athlete-ipscore-value';
import { getWeekNumber, getDayIndex } from '../../../../../../shared/utils/moment';
import { getStartIndex } from '../../../../../../db/ipscore/aggregation/aggregators/ipscore/utils/getStartIndex';
import { AthleteIpScoreWeeklyAggregateInterface } from '../../../../../../db/ipscore/models/interfaces';
import { QueryDocumentSnapshot } from '@google-cloud/firestore';

export const GetValuesByIPScoreWeeklyAggregateValue = async (athlete: Athlete, utcOffset: number, currentRunningIPScore, startDate?: Date, endDate?: Date) => {

    const athleteUID = !!athlete ? athlete.uid : undefined
    const curr = currentRunningIPScore
    let athleteWeeklyIpScoreDocs: Array<AthleteIpScoreWeeklyAggregateInterface>;
    if (athleteUID) {


        // const dayIndex = getDayIndex(firestore.Timestamp.fromDate(startDate), utcOffset || 0);
        const weekStartNr = getWeekNumber(athlete.metadata.creationTimestamp, startDate ? firestore.Timestamp.fromDate(startDate) : athlete.metadata.creationTimestamp, utcOffset || 0);
        const weekEndNr = getWeekNumber(athlete.metadata.creationTimestamp, endDate ? firestore.Timestamp.fromDate(endDate) : firestore.Timestamp.now(), utcOffset || 0);

        athleteWeeklyIpScoreDocs = await db
            .collection(IpScoreAggregateCollection.AthleteIpScoreWeekly)
            // .doc(athleteUID)
            .where('entityId', '==', athleteUID)
            .where('weekNumber', '>=', weekStartNr)
            .where('weekNumber', '<=', weekEndNr)
            .get()
            .then((queryDocSnap) => {
                return queryDocSnap.size
                    ? [].concat(...queryDocSnap.docs.map((docSnap) => [getDocumentFromSnapshot(docSnap) as AthleteIpScoreWeeklyAggregateInterface]))
                    : [];
            }).catch((err) => {
                throw err
            });
    }
    else {
        athleteWeeklyIpScoreDocs = await db
            .collection(IpScoreAggregateCollection.AthleteIpScoreWeekly)
            .get()
            .then((querySnap: firestore.QuerySnapshot) => {
                return !querySnap.empty
                    ? querySnap.docs.map((queryDoscSnap) => {
                        return getDocumentFromSnapshot(queryDoscSnap) as AthleteIpScoreWeeklyAggregateInterface;
                    })
                    : [];
            });
    }
    const theEntries = [];

    // const athletesToUpdate = []

    let updateFirestore = true

    if (athleteWeeklyIpScoreDocs.length) {
        const groupedAthleteWeeklyIpScoreDocs = groupByMap(athleteWeeklyIpScoreDocs, (entry: AthleteIpScoreWeeklyAggregateInterface) => entry.entityId) as Map<string, Array<AthleteIpScoreWeeklyAggregateInterface>>;
        groupedAthleteWeeklyIpScoreDocs.forEach((athleteScores, atleteUID) => {
            const ath = athlete // await getAthleteDocRef(atleteUID).get().then(docSnap => { return getDocumentFromSnapshot(docSnap) as Athlete; });
            const weekNr = getWeekNumber(ath.metadata.creationTimestamp, firestore.Timestamp.now(), utcOffset || 0);
            const dayIndex = getDayIndex(firestore.Timestamp.now(), utcOffset || 0);
            const startIndex = getStartIndex(dayIndex, ath.metadata.creationTimestamp, utcOffset, false);

            const reducer = (accumulator: number, currentValue: number) => accumulator + currentValue;

            const athleteData = athleteScores.map((doc) => {
                const theResult = {
                    ipScore: +doc.value,
                    startDate: doc.startDate,
                    weekNumber: doc.weekNumber,
                    // dailies: doc.weekNumber === weekNr ? doc.dailies : undefined,
                    dailies: startDate ? (doc.dailies || []).filter((daily) => daily.dateTime.toDate() >= startDate) : doc.dailies || [],
                    healthComponentType: HealthComponentType[doc.healthComponentType],
                    lifestyleEntryType: LifestyleEntryType[doc.healthComponentType],
                };

                const theUpdatedWeeklyIpScore = theResult.dailies.map((day) => day.value).reduce(reducer);
                theResult.ipScore = theUpdatedWeeklyIpScore

                return theResult
            });
            const dailyIPScoreDocs = athleteData.find((data) => data.weekNumber === weekNr);
            const dailyIPScoreDailyDocs = dailyIPScoreDocs ? dailyIPScoreDocs.dailies : undefined;
            // reset Dailies
            const dailyIPScore = !!dailyIPScoreDailyDocs ? calculateIpScoreFromDailies(dailyIPScoreDailyDocs, dayIndex, startIndex) : 0;
            const runningIPScore = athleteData.map((data) => data.ipScore).reduce(reducer);
            const runningIpScoreRounded = Math.round(runningIPScore * 100);
            // athletesToUpdate.push({ath, runningIPScore, dailyIPScore})
            // const ipScoreUpdateRes = await updateAthleteIPScoreValue(ath, runningIPScore, dailyIPScore);
            const theRes = {
                athlete: ath,
                docUId: athleteScores[0].uid,
                athleteId: athleteScores[0].entityId,
                name: athleteScores[0].entityName,
                ipScoreCount: athleteData.length,
                // ipScoreUpdateRes,
                athleteData,
                runningIpScoreRounded,
                runningIPScore,
                currentRunningIPScore

            };
            theEntries.push(theRes);
            console.log('theRes', theRes);
        });

        !!updateFirestore && await Promise.all(theEntries.map(async (a) => {
            const ipScoreUpdateRes = await updateAthleteIPScoreValue(a.athlete, a.runningIPScore, (a.athlete as Athlete).currentIpScoreTracking.ipScore) // a.dailyIPScore);

            return ipScoreUpdateRes
        }))

        return {
            message: `IPScores Updated`,
            responseCode: 200,
            data: theEntries,
            error: undefined,
        };
    }
    else {

        if (!!updateFirestore) {
            const ipScoreUpdateRes = await updateAthleteIPScoreValue(athlete, 0, athlete.currentIpScoreTracking.ipScore) // a.dailyIPScore);

            // return ipScoreUpdateRes
        }

        // TODO:
        return {
            message: `No IPScore docs found`,
            responseCode: 200,
            data: theEntries,
            error: undefined
        };
    }
};
