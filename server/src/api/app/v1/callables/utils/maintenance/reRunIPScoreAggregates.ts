import { firestore } from 'firebase-admin';
import moment from 'moment';

import { getDocumentFromSnapshot } from '../../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { getAthleteDocRef } from '../../../../../../db/refs';
import { Athlete } from '../../../../../../models/athlete/interfaces/athlete';
import { groupByMap } from "../../../../../../shared/utils/groupByMap";
import { AthleteBiometricEntryInterface } from '../../../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { AggregateIpScoreDirective } from '../../../../../../db/ipscore/services/interfaces/aggregate-ipscore-directive';
import { FirestoreCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';
import { getUserByID } from '../../../controllers/get-user-by-id';
import { getUserByEmail } from '../../../controllers/get-user-by-email';
import { resetIPTracking } from './resetIPTracking';
import { updateCurrentIpScoreTracking } from './updateCurrentIpScoreTracking';
import { castAggregateDocument } from '../../../../../../db/ipscore/services/cast-to-cast-aggregate-document';
import { getAggregateDocumentID } from '../../get-aggregate-document-Id';
import { IpScoreAggregateCollection } from '../../../../../../db/ipscore/enums/firestore-ipscore-aggregate-collection';

export const reRunIPScoreAggregates = async (tr: FirebaseFirestore.Transaction, athlete: Athlete, utcOffset: number, startDate?: Date): Promise<AggregateIpScoreDirective> => {

    let athleteUID = athlete.uid

    if (!athleteUID) {
        athleteUID = await getUserByEmail(athlete.profile.email).then((record) => record.uid)
        if (!athleteUID) {
            return undefined
        }
        athlete.uid = athleteUID
    }

    // const currentIpScoreTrackingIPScore = athlete.currentIpScoreTracking.ipScore || 0
    athlete.runningTotalIpScore = 0;
    athlete.currentIpScoreTracking.ipScore = 0;

    await validateAthleteCreationTimestamp(athlete);

    const athleteBiometricEntries = await getAthleteDocRef(athleteUID).collection(FirestoreCollection.AthleteBiometricEntries)
        .get().then((querySnap) => {
            return querySnap.docs.length
                ? querySnap.docs.map((queryDocSnap) => getDocumentFromSnapshot(queryDocSnap) as AthleteBiometricEntryInterface)
                : [];
        });

    // Group Biometric Entries by day using the 'dateTime' property in the format 'DDMMYY'
    const groupedAthleteBiometricEntryDocs = groupByMap(athleteBiometricEntries, (entry: AthleteBiometricEntryInterface) =>
        moment(entry.dateTime.toDate()).format('DDMMYY')) as Map<string, Array<AthleteBiometricEntryInterface>>;

    let directive: AggregateIpScoreDirective = {
        athlete,
        collection: IpScoreAggregateCollection.AthleteIpScoreWeekly,
        athleteDocuments: undefined,
        message: '',
        utcOffset
    }


    // Loop through each day
    groupedAthleteBiometricEntryDocs.forEach(async (entries: Array<AthleteBiometricEntryInterface>, date) => {

        const { dateTime } = entries[0]
        // const { dateTime } = directive.athlete.currentIpScoreTracking
        const docID = getAggregateDocumentID(directive, undefined, dateTime);// TODO: Check dateTime here

        directive.aggregateDocument = castAggregateDocument(directive, docID, athlete, entries[0], { exists: false } as any);
        resetIPTracking(athlete, dateTime);


        console.log('athlete.currentIpScoreTracking.dateTime', athlete.currentIpScoreTracking.dateTime.toDate())

        entries.forEach(async (entry) => {
        //   let res = await
        //    updateCurrentIpScoreTracking(directive, entry, utcOffset)
debugger;
        // Todo:
        })


    });


    if (!athlete.currentIpScoreTracking.directive) {
        resetIPTracking(athlete, athlete.currentIpScoreTracking.dateTime)
    }

    console.log('Athlete Running Total IpScore', athlete.runningTotalIpScore)


    // athlete.currentIpScoreTracking.ipScore = (directive && directive.aggregateDocuments) ? directive.aggregateDocuments[0].dailies[nowDayIndex].value : 0
    // if (getDayIndex(firestore.Timestamp.now(), utcOffset) > getDayIndex(firestore.Timestamp.now(), utcOffset)) {

    // }

    const { ipScore, name, firstName, lastName, metaData, email, phoneNumber, isAnonymous, ...athleteRest } = athlete as any

    if (!directive) {
        debugger;
        directive = {
            athleteDocuments: undefined,
            athlete: athleteRest as Athlete,
            athleteWIP: undefined,
            message: 'Directive Null',
            utcOffset,
            // tr
            // skipSaveDocs: true
        }
    } else {
        directive.athlete = athleteRest as Athlete
    }


    return directive;
};

async function validateAthleteCreationTimestamp(athlete: Athlete) {
    if (!athlete.metadata.creationTimestamp) {
        const theUser = await getUserByID(athlete.uid);
        console.error('-------------------------- athlete.metadata.creationTimestamp undefined', athlete.metadata);
        athlete.metadata.creationTimestamp = firestore.Timestamp.fromDate(new Date(theUser.metadata.creationTime));
    }
}
