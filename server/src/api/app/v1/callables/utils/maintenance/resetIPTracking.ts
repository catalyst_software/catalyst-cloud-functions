import { firestore } from 'firebase-admin'
import moment from 'moment'

import { Athlete } from '../../../../../../models/athlete/interfaces/athlete'
import { IpScoreConfiguration } from '../../../../../../db/ipscore/models/documents/tracking/ip-score-configuration.interface'
import {
    IpScoreTracking,
    IpScoreProgramTracking,
    IpScoreQualitativeWellnessTracking,
    IpScoreQuantitativeWellnessEntryTracking,
    IpScoreQuantitativeWellnessBrainTracking,
    IpScoreQuantitativeWellnessBodyTracking,
    IpScoreQuantitativeWellnessComponentTracking,
    IpScoreQuantitativeWellnessTracking,
} from '../../../../../../db/ipscore/models/documents/tracking/ip-score-tracking.interface'

export const resetIPTracking = (
    athlete: Athlete,
    dateTime: firestore.Timestamp
) => {
    athlete.currentIpScoreTracking = {
        dateTime,
        ipScore: 0,
        programs: {
            videoConsumed: false,
            worksheetConsumed: false,
            surveyConsumed: false,
        },
        quantitativeWellness: {
            include: true,
            training: {
                components: {
                    recordedDailyEntries: 0,
                    tracking: [],
                },
            },
            body: {
                fatigue: {
                    recordedDailyEntries: 0,
                    tracking: [],
                },
                pain: {
                    recordedDailyEntries: 0,
                    tracking: [],
                },
                weight: {
                    recordedDailyEntries: 0,
                    tracking: [],
                },
                wearables: {
                    recordedDailyEntries: 0,
                    tracking: [],
                },
            },
            brain: {
                sleep: {
                    recordedDailyEntries: 0,
                    tracking: [],
                },
                mood: {
                    recordedDailyEntries: 0,
                    tracking: [],
                },
            },
            food: {
                components: {
                    recordedDailyEntries: 0,
                    tracking: [],
                },
            },
        },
        qualitativeWellness: {
            include: false,
        },
        directive: {
            execute: true,
            includeGroupAggregation: true,
            paths: [],
        },
    }
}

export const createNewIpScoreTracking = (
    ipScoreConfiguration?: IpScoreConfiguration,
    utcOffset?: number
): IpScoreTracking => {
    const theDate = firestore.Timestamp.fromDate(
        moment()
            .add(utcOffset || 10 * 60, 'minutes')
            .startOf('day')
            .toDate()
    )
    const include = ipScoreConfiguration
        ? ipScoreConfiguration.quantitativeWellness.include
        : true

    return {
        // dateTime: ,
        ...ipScoreConfiguration,
        dateTime: theDate,
        ipScore: 0,
        directive: {
            execute: false,
            includeGroupAggregation: false,
            paths: [],
        },
        programs: ({
            videoConsumed: false,
            surveyConsumed: false,
            worksheetConsumed: false,
            recordedDailyEntries: 0,
        } as any) as IpScoreProgramTracking,
        qualitativeWellness: {
            include: false,
        } as IpScoreQualitativeWellnessTracking,
        quantitativeWellness: {
            include,
            brain: {
                mood: {
                    recordedDailyEntries: 0,
                    tracking: [],
                } as IpScoreQuantitativeWellnessEntryTracking,
                sleep: {
                    recordedDailyEntries: 0,
                    tracking: [],
                } as IpScoreQuantitativeWellnessEntryTracking,
            } as IpScoreQuantitativeWellnessBrainTracking,
            body: {
                fatigue: {
                    recordedDailyEntries: 0,
                    tracking: [],
                } as IpScoreQuantitativeWellnessEntryTracking,
                pain: {
                    recordedDailyEntries: 0,
                    tracking: [],
                } as IpScoreQuantitativeWellnessEntryTracking,
                weight: {
                    recordedDailyEntries: 0,
                    tracking: [],
                } as IpScoreQuantitativeWellnessEntryTracking,
                wearables: {
                    recordedDailyEntries: 0,
                    tracking: [],
                    // lastUpdate: undefined
                } as IpScoreQuantitativeWellnessEntryTracking,
            } as IpScoreQuantitativeWellnessBodyTracking,
            food: {
                components: {
                    recordedDailyEntries: 0,
                    tracking: [],
                } as IpScoreQuantitativeWellnessEntryTracking,
            } as IpScoreQuantitativeWellnessComponentTracking,
            training: {
                components: {
                    recordedDailyEntries: 0,
                    tracking: [],
                } as IpScoreQuantitativeWellnessEntryTracking,
            } as IpScoreQuantitativeWellnessComponentTracking,
        } as IpScoreQuantitativeWellnessTracking,
    } as IpScoreTracking
}
