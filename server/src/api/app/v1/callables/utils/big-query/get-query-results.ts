import { bigqueryClient } from './initBQ';
import * as uuid from 'uuid';
import { QueryRowsResponse } from '@google-cloud/bigquery';
import { ScheduledTask } from '../../../../../../db/taskQueue/interfaces/scheduled-task.interface';
import { logTaskQueueMessage } from '../../../../../../shared/utils/taskQueueLogging/logTaskQueueMessage';
import { DeviceDimension } from '../warehouse/device-dimension.interface';

export const getTaskQueryResults = async <T>(task: ScheduledTask, query: string): Promise<Array<T>> => {
    let msg = `Running BigQuery query: ${query}`;
    logTaskQueueMessage(task, msg);
    try {
        const data: QueryRowsResponse  = await bigqueryClient.query(query);
        msg = `Data: ${JSON.stringify(data)}`;
        logTaskQueueMessage(task, msg);

        const rows = data[0] as Array<T>;
        msg = `BigQuery query successful.  Returned ${rows.length} rows.`;
        logTaskQueueMessage(task, msg);
        return rows;
    } catch (e) {
        msg = `An error occurred when running BigQuery query ${query}.  Message: ${e.message}`;
        logTaskQueueMessage(task, msg);

        return [] as Array<T>;
    }
}

export const getQueryResults = async <T>(query: string): Promise<Array<T>> => {
    let msg = `Running BigQuery query: ${query}`;
    console.log(msg);
    try {
        const data: QueryRowsResponse  = await bigqueryClient.query(query);
        msg = `Data: ${JSON.stringify(data)}`;
        console.log(msg);

        const rows = data[0] as Array<T>;
        msg = `BigQuery query successful.  Returned ${rows.length} rows.`;
        console.log(msg);
        return rows;
    } catch (e) {
        msg = `An error occurred when running BigQuery query ${query}.  Message: ${e.message}`;
        console.error(msg);

        return [] as Array<T>;
    }
}

export const manageDeviceDimension = async (task: ScheduledTask, deviceDim: DeviceDimension): Promise<string> => {
    let deviceId: string;
    let msg: string;
    const datasetId = 'catalystDW';
    const tableId = 'deviceDim';
    
    const query: string = `select device_id from ${datasetId}.${tableId} where device_os = '${task.deviceOs}' and device_os_version = '${task.deviceOsVersion}' and device_sdk_version = '${task.deviceSdkVersion}'`;
    logTaskQueueMessage(task, `Searching for existing device dimension with sql ${query}`);
    const rows = await getQueryResults<DeviceDimension>(query);
    if (rows.length > 0) {
        deviceId = rows[0].device_id;
        deviceDim.device_id = deviceId;
        msg = `Existing device found in dimension table: ${deviceId}`;
        logTaskQueueMessage(task, msg);
        return Promise.resolve(deviceId);
    } else {
        const newRows: Array<DeviceDimension> = [];
        newRows.push(deviceDim);

        msg = `Existing device NOT FOUND in dimension table.  Adding new device dimension: ${JSON.stringify(newRows[0])}`;
        logTaskQueueMessage(task, msg);
        deviceId = deviceDim.device_id;

        const theRes = await bigqueryClient
                    .dataset(datasetId)
                    .table(tableId)
                    .insert(newRows)
                    .then((rowCount) => {
                        return { 
                            rowCount, 
                            errors: undefined }
                    })
                    .catch((e) => {
                        console.log(e.errors);
                        return {
                            rowCount: undefined,
                            errors: e.errors
                        }
                    });

        if (theRes.rowCount !== undefined) {
            msg = `Successfully added new device dimension ${deviceId}`;
            logTaskQueueMessage(task, msg);
        } else {
            deviceId = null;
            msg = `${theRes.errors.length} error(s) occurred when adding new DeviceDimension to BigQuery`;
            logTaskQueueMessage(task, msg);
            theRes.errors.forEach((e: any) => {
                msg = `Error: ${JSON.stringify(e)}`;
                logTaskQueueMessage(task, msg);
            });
        }

        return Promise.resolve(deviceId);
    }
}