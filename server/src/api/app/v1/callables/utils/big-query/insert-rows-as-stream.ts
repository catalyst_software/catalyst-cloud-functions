import { bigqueryClient } from './initBQ';

export const insertRowsAsStream = async (
  datasetId,
  tableId,
  rows = [{
    athleteUID: 'Tom',
    orgUID: '30',
    groupUID: ''
  }, {
    athleteUID: 'Jane',
    orgUID: '32',
    groupUID: ''
  }]
) => {
  // Inserts the JSON objects into my_dataset:my_table.

  /**
   * TODO(developer): Uncomment the following lines before running the sample.
   */
  // const datasetId = "my_dataset";
  // const tableId = "my_table";

  // Create a client
  // const bigqueryClient = new BigQuery();

  // Insert data into a table
  await bigqueryClient
    .dataset(datasetId)
    .table(tableId)
    .insert(rows);
  console.log(`Inserted ${rows.length} rows`);

  return rows
}