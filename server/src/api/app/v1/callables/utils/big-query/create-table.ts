import { bigqueryClient } from './initBQ';

export const createTable = async (
    datasetId = "ip_score_non_engagement",
    tableId = "my_new_table",
    schema? //= "Name:string, Age:integer, Weight:float, IsMagic:boolean"

) => {
    // Creates a new dataset named 'datasetId'.



    // For all options, see https://cloud.google.com/bigquery/docs/reference/v2/tables#resource
    const options = {
        schema: schema,
        // location: 'US',
    };

    // // const dataset = bigqueryClient.dataset('my-dataset');
    // const dataset = bigqueryClient.dataset(datasetId);

    // // const tableData = dataset.table('tableId');


    // // const myTable = dataset.table('my-table');
    // const myTable = dataset.createTable(tableId, options);


    const [table] = await bigqueryClient
        .dataset(datasetId)
        .createTable(tableId, options);

    console.log(`Table ${table.id} created.`);
    // const theTable = await myTable.create().then((s) => {
    //     return s
    // });

    // Create a new table in the dataset
    // const [table] = await bigqueryClient
    //     .dataset(datasetId)
    //     .createTable(tables);

    return table
}