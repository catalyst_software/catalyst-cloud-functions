import { bigqueryClient } from './initBQ';

export const listTables = async (datasetId: string) => {
    
        // List all tables in the dataset
        const [tables] = await bigqueryClient.dataset(datasetId).getTables();
    
        console.log('Tables:');
        tables.forEach(table => console.log(table.id));

        return tables
}
