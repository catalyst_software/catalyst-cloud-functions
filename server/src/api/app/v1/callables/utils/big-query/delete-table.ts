import { bigqueryClient } from "./initBQ";

export const deleteTable = async (datasetId: string, tableId: string) => {

    // Delete the table
    await bigqueryClient
        .dataset(datasetId)
        .table(tableId)
        .delete()
        .then((resp) => {

            console.log(`Table ${tableId} deleted.`);

            return true;
        })
        .catch((err) => {
            console.error(err);

            return false;
        });


}
