import { bigqueryClient } from './initBQ';

export const createDataset = async (datasetId) => {
    // Creates a new dataset named 'datasetId'.

    // const datasetId = "ip_score_non_engagement";

    // Create a client
    // const bigqueryClient = new BigQuery();

    // Specify the geographic location where the dataset should reside
    const options = {
        location: 'europe-west6' // Zürich
    };

    // Create a new dataset
    const [dataset] = await bigqueryClient.createDataset(datasetId, options);
    console.log(`Dataset ${dataset.id} created.`);

    return dataset
}