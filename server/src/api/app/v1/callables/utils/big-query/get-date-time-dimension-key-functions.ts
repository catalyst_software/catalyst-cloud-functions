import moment from 'moment';

export const getLocalDateTime = (date: Date, timezoneOffset: number): Date => {
    const d = new Date(date);

    if (!!timezoneOffset) {
        return moment(d).add((timezoneOffset * -1), 'm').toDate();
    } else {
        return d;
    }
}

export const getDateDimensionKey = (date: Date): number => {
    const d = new Date(date);
    const year = d.getFullYear();
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();

    if (month.length < 2) {
        month = '0' + month;
    }
    if (day.length < 2) {
        day = '0' + day;
    }

    return parseInt([year, month, day].join(''));
}

export const getTimeDimensionKey = (date: Date): number => {
    const d = new Date(date);
    let hours = '' + d.getHours();
    let minutes = '' + d.getMinutes();
    let seconds = '' + d.getSeconds();

    if (hours.length < 2) {
        hours = '0' + hours;
    }
    if (minutes.length < 2) {
        minutes = '0' + minutes;
    }
    if (seconds.length < 2) {
        seconds = '0' + seconds;
    }

    return parseInt([hours, minutes, seconds].join(''));
}

