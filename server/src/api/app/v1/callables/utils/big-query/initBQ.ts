import { BigQuery } from '@google-cloud/bigquery';
import { isProductionProject } from '../../../is-production';

export const bigqueryClient = new BigQuery({
            projectId: 'catalyst-31efe',
            // keyFilename: '/path/to/keyfile.json'
            credentials: {
                private_key: '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDg41ka48I5P8/C\nWFietJ3y2NdYuTbeBndw0M/8jwXPA/bQ1inOoOAm0Oqsmr6ySp+9uWjHW6F4bnAi\n4qLrdfieCL6nDOYLM1O10GCUT/f1sN8G/zXLvK+RHe3zPQ/ZO1vHFvdRO7XwxrLF\nEnxlM0+nDlhK3k0cEX3PhivPpLkD+HwZ6JobfXvR4Lz2BAvF7bE3BF1MDAjuwCBM\nrfZBftCN52nHGgDox2d4MGCWh3w7Oq4fJbJTFCP/GLzpBt1Vqct1mfue/lIv+VB8\nA+hF/BzIP7re5wFDh0kJBULGAdrFW3mzeCvE94+ygvSdxs0lohIwYUjjLhD6lW+N\nlIhXMLJdAgMBAAECggEAH6qbvi/ShkWwP1U521UjBZJXlnB0I6Tk9i+NDerkvj74\nZvU1K1mXXpfd56KdlhTwc+Jy+Nlz/fWNGDhyCPZSZQxlrjegf/TBBZG7O78b8w5u\nBTn0DQV7clN00WKog+X6GSlod1mRV79e5mWhOw3T2jnooFg3outqnAlwS1uOStrT\npqEXgJB7gkVGtwpyY+Z/AvYYYJaC9Ps9buuPFTYsEAz4bA4YnmJNsbl6Yrp3Gs9E\ncpfuxYw4z/glC/QqpLEUsVaXchV/a57dYXMj5XmzRyAbV9l9MAot5kb7Fjd9s8qj\nkbJYFGslrVJkYHM0i2MKo6NVLe/hirAqJkuNImRkqQKBgQD0yc32BYciaVeTqm2K\n9At38EfkZ7HRonu0NX++mSXf4sSg4EqTEZAH4VEaHMwEXF9MVm5vEZmm1474ysDJ\nqMP0+xpG1x4EdFv1Ykm+5vhXvKp6CObYHe82uef8zy0d5HJedwr83f9ePz7mRMUS\nn4w2LsxZqREii7yKpNsAp56xSQKBgQDrMDWLxgNw4IcxLcMOmA+Yr3ZMla6qPhfY\nT3B2bYJCb4GBwJ4Su7FK4UaCkEMVk67CsZ4yE+hDdkEQTIomfNZgHgRD4JoSC4c/\nD0Eh3veK9euqxAsFL9kVgeP1GdS4IISXUZhgBGOt+R0hSstQolnaBIl2NO5Dy/mD\n2gmLLshMdQKBgGv3nppGseXhpLkC0LIbFfvexTbe7bVS/1Dqz99XWTc4pBV5GCtp\n8acoIEduMCY0P+xBqw/eKXFHXk4pz4Kip1ukbWCVfS3lzLGcmn5TDXJVSrPsvTTR\nbzMqKEJ/dpzap/82bmg92mbNaobSCkI+kO9NEODzCxW7oHteXyIYzIvRAoGBAL2L\nYvw9H/TO3+YArZ2La7ZwYvUiBVzrIiiRDWBEraCBYWyDImKSpjZlDICZZ5dyKHx8\nzbQqWxxTp+hM52JRZiycxG84CrPUWrG1x5ZWrkZuH/rGeOM7RseQoXmd0dR7UKDr\nf9SSdQt3I8T7FJTt42BUG70Kt6QoNfXPubVUhNUpAoGBAJxFuZ4Rtf/NwF5kci+u\n0hOqSZ4V135nhaXdl0WIGY41gAxVqNgPp0IfC7miE1g8q8y3PYaEaLPlDVYKhnLS\neNh6S0Om9F7Ku/RE7oqwGHhgekSCZVuRPGA0HKjp/vgaFr+2jzlws8Gi5nLvMI7i\nWXy6b1FCH7ZEZ8KV8bPKnbkJ\n-----END PRIVATE KEY-----\n',
                client_email: 'catalyst-31efe@appspot.gserviceaccount.com',
            }
        });
