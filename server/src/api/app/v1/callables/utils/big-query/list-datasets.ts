import { bigqueryClient } from './initBQ';

export const listDatasets = async () => {

    // Lists all datasets in current GCP project.
    const [datasets] = await bigqueryClient.getDatasets();

    console.log('Datasets:');
    datasets.forEach(dataset => console.log(dataset.id));

    // Return All Datasets
    return datasets
}

export const getDataset = async (name: string) => {

    // Lists all datasets in current GCP project.
    const [datasets] = await bigqueryClient.getDatasets();

    console.log('Datasets:');
    datasets.forEach(dataset => console.log(dataset.id));

    // Return Selected Dataset
    return datasets.find((dataset) => dataset.id === name)
}