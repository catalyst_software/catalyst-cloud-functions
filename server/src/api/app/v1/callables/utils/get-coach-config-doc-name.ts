export const getCoachConfigDocName = (firstName: string, lastName: string, athleteUID: string) => {
  return `${firstName}_${lastName}_${athleteUID.slice(0, 5)}`.replace(/\'/g, '_').replace(/\ /g, '_');
};
