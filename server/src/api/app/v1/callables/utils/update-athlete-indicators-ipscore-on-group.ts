import { Athlete } from './../../../../../models/athlete/athlete.model';
import { db } from "../../../../../shared/init/initialise-firebase";
import { FirestoreCollection } from "../../../../../db/biometricEntries/enums/firestore-collections";
import moment from "moment";

import { LogInfo } from "../../../../../shared/logger/logger";

import { Group } from "../../../../../models/group/interfaces/group";

import { getDocumentFromSnapshot } from "../../../../../analytics/triggers/utils/get-document-from-snapshot";
import { isNumber } from '../../../../../db/ipscore/aggregation/aggregators/ipscore/utils';
import { getAthleteCollectionRef } from '../../../../../db/refs';
import { isArray } from 'lodash';


export const updateAthleteIndicatorsIPScoreOnGroup = async (
  groupUID: string,
  utcOffset: number,
  athleteUID?: string):
  Promise<Array<any>> => {

  const groupRef = db
    .collection(FirestoreCollection.Groups)
    .doc(groupUID)

  const group = await groupRef.get().then((groupDocSnap) => {
    return getDocumentFromSnapshot(groupDocSnap) as Group
  });


  if (!group.athleteIndex && !group.athletes) {
    console.warn('Group has no athletes')
    group.athletes = []
  }

  let athleteUIDs: Array<string> = []

  if (athleteUID) {
    athleteUIDs = [athleteUID]
  } else {

    if (group.athleteIndex) {
      athleteUIDs = Object.keys(group.athleteIndex)
    } else {
      athleteUIDs = group.athletes.map((athlete) => {
        return athlete.uid
      })
    }

  }

  console.log('======>>>>> updateAthleteIndicatorsIPScoreOnGroup groupUID', groupUID)
  console.log('======>>>>> athleteUIDs', athleteUIDs)

  const startOfDay = moment.utc().add(isNumber(utcOffset)
    ? utcOffset
    : 10,
    'minute').startOf('day').toDate()

  if (athleteUIDs && isArray(athleteUIDs) && athleteUIDs.length) {

    console.log('latestReportDate to check (1 days) = (date) => ', startOfDay)

    // Set up all the promises for the the query to grab the athletes by uid

    const promises = await Promise.all(athleteUIDs.map(async (currentAthleteUID) => {

      const query = getAthleteCollectionRef()
        .where('uid', '==', currentAthleteUID)
        // .where('currentIpScoreTracking.dateTime', '<', startOfDay)
        .orderBy("currentIpScoreTracking.dateTime", 'desc')

      const update = await query.get()
        .then(async (athleteDocs: FirebaseFirestore.QuerySnapshot) => {

          LogInfo(`athletes.size => ${athleteDocs.size}`);

          const updates = await Promise.all(athleteDocs.docs
            .map((athletesQueryDocSnap: FirebaseFirestore.QueryDocumentSnapshot) => {
              if (!athletesQueryDocSnap.exists) {
                console.warn('Athlete Document Does not Exist in FireStore', athletesQueryDocSnap.id)
              }
              return athletesQueryDocSnap
            })
            .filter(snap => snap.exists)
            .map(async (athleteSnap: FirebaseFirestore.DocumentSnapshot) => {

              const theAthlete = getDocumentFromSnapshot(athleteSnap) as Athlete

              console.warn(`Resetting IPScore on Group Indicator for ${theAthlete.profile.firstName} having UID`, theAthlete.uid)

              const theAthleteUID = theAthlete.uid
              const theAthleteIndicator = group.athletes.find((ath) => ath.uid === theAthleteUID)
              console.log('theAthleteIndicator', theAthleteIndicator)
              console.log('current IPScore on Indicator', theAthleteIndicator.ipScore)
              console.log('updateAthleteIndicatorsIPScoreOnGroup = current Running IPScore on Indicator', theAthleteIndicator.runningTotalIpScore)

              console.log('current IPScore on Athlete', theAthlete.currentIpScoreTracking.ipScore)
              console.log('current Running IPScore on Athlete', theAthlete.runningTotalIpScore)
              const payload = {
                message: '',
                athlete: undefined,
                athleteUID: theAthleteUID,
                groupUID
              }

              if (theAthleteIndicator) {
                // Update The Indicator and Group
                theAthleteIndicator.ipScore = theAthlete.currentIpScoreTracking.ipScore
                theAthleteIndicator.runningTotalIpScore = isNumber(theAthlete.runningTotalIpScore) ? +theAthlete.runningTotalIpScore : 0

                payload.message = 'updateAthleteIndicatorsIPScoreOnGroup: Athlete Inidicator UPDATED in group'

                const { uid, recentEntries, ...athleteIndicatorRest } = theAthleteIndicator
                payload.athlete = athleteIndicatorRest

                console.log('updated IPScore on Indicator', group.athletes.find((athlete) => athlete.uid === theAthleteUID).ipScore)
                console.log('updated Running IPScore on Indicator', group.athletes.find((athlete) => athlete.uid === theAthleteUID).runningTotalIpScore)
                console.log(payload)
                return payload

              } else {

                payload.message = 'Athlete Inidicator NOT FOUND in group'
                console.error(payload)
                return payload
              }

            }));
          // merge all grouped promises (per athlete)
          const athleteGroups = [].concat(...updates) as [
            {
              message: string,
              athleteUID: string,
              groupUID: string
            }]
          const updateStatus = athleteGroups
          return updateStatus
        })
        .then((updateStatus: Array<{
          message: string;
          athleteUID: string;
          groupUID: string;
        }>) => {

          console.log(updateStatus)

          return updateStatus;
        });

      return update
    }))

    const theRes = [].concat(promises)
    console.warn('=====> UPDATING GROUP')
    if (theRes.length) {
      await groupRef.update({
        athletes: group.athletes,
        lastIpScoreUpdate: startOfDay
      })
      return group.athletes //theRes
    } else {
      return [{
        error: 'No Athlete Found'
      }]
    }


  } else {
    return [{
      error: 'No Athletes Found'
    }]
  }
};

export const updateAthleteIndicatorsIPScoreOnGroups = async (
  groupUIDs: Array<string>,
  utcOffset: number,
  athleteUID: string):
  Promise<Array<any>> => {
  debugger;

  const theUpdateResult = Promise.all(groupUIDs.map((groupUID) => {
    return updateAthleteIndicatorsIPScoreOnGroup(groupUID, utcOffset, athleteUID)
  }))

  return theUpdateResult
}
