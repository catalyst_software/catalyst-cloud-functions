// import { FieldValue } from '@google-cloud/firestore';
import { firestore } from "firebase-admin";
import moment from 'moment';

import { SaveEventDirective } from '../../../../../../models/purchase/interfaces/save-event-directive';
import { AthleteSubscription } from "../../../../../../models/athlete/interfaces/athlete-subscription";
import { isArray, includes, isDate, isString } from "lodash";
import { SubscriptionType, SubscriptionStatus, SubscriptionPaymentType } from "../../../../../../models/enums/enums.model";
import { Athlete } from "../../../../../../models/athlete/athlete.model";
import { getDocFromRef } from "../../../../../../db/refs";
import { updateExistingAthleteForPurchasing } from "./updateExistingAthleteForPurchasing";
import { updateAthleteForPurchasing } from "./updateAthleteForPurchasing";
import {
    IpScoreTracking, IpScoreProgramTracking, IpScoreQualitativeWellnessTracking,
    IpScoreQuantitativeWellnessEntryTracking, IpScoreQuantitativeWellnessBrainTracking, IpScoreQuantitativeWellnessBodyTracking,
    IpScoreQuantitativeWellnessComponentTracking, IpScoreQuantitativeWellnessTracking
} from "./IpScoreTracking";
import { IpScoreConfiguration } from "../../../../../../db/ipscore/models/documents/tracking/ip-score-configuration.interface";


const getPaymentTypeFromProductIdentifier = (productIdentifier: string): SubscriptionPaymentType => {
    let type = SubscriptionPaymentType.Monthly;
    const parts = productIdentifier.split('.');
    if (includes(parts, 'quarter')) {
        type = SubscriptionPaymentType.Quarterly;
    } else if (includes(parts, 'annual')) {
        type = SubscriptionPaymentType.Annual;
    }

    return type;
}

const getAccountTypeFromProductIdentifier = (productIdentifier: string): SubscriptionType => {
    let type = SubscriptionType.Basic;

    return type;
}


// const createNewIpScoreTracking = (ipScoreConfiguration?: IpScoreConfiguration): IpScoreTracking => {
//     const theDate = new Date();
//     const include = (ipScoreConfiguration) ? ipScoreConfiguration.quantitativeWellness.include : true;

//     return {
//         dateTime: new Date(theDate.getFullYear(), theDate.getMonth(), theDate.getDate()),
//         ipScore: 0,
//         directive: {
//             execute: false,
//             updateAthleteIndicatorsOnGroups: false,
//             includeGroupAggregation: false,
//             paths: []
//         },
//         programs: {
//             videoConsumed: false,
//             surveyConsumed: false,
//             worksheetConsumed: false,
//             recordedDailyEntries: 0
//         } as any as IpScoreProgramTracking,
//         qualitativeWellness: {
//             include: false
//         } as IpScoreQualitativeWellnessTracking,
//         quantitativeWellness: {
//             include,
//             brain: {
//                 mood: {
//                     recordedDailyEntries: 0,
//                     tracking: []
//                 } as IpScoreQuantitativeWellnessEntryTracking,
//                 sleep: {
//                     recordedDailyEntries: 0,
//                     tracking: []
//                 } as IpScoreQuantitativeWellnessEntryTracking
//             } as IpScoreQuantitativeWellnessBrainTracking,
//             body: {
//                 fatigue: {
//                     recordedDailyEntries: 0,
//                     tracking: []
//                 } as IpScoreQuantitativeWellnessEntryTracking,
//                 pain: {
//                     recordedDailyEntries: 0,
//                     tracking: []
//                 } as IpScoreQuantitativeWellnessEntryTracking,
//                 wearables: {
//                     recordedDailyEntries: 0,
//                     tracking: [],
//                     lastUpdate: undefined
//                 } as IpScoreQuantitativeWellnessEntryTracking
//             } as IpScoreQuantitativeWellnessBodyTracking,
//             food: {
//                 components: {
//                     recordedDailyEntries: 0,
//                     tracking: []
//                 } as IpScoreQuantitativeWellnessEntryTracking
//             } as IpScoreQuantitativeWellnessComponentTracking,
//             training: {
//                 components: {
//                     recordedDailyEntries: 0,
//                     tracking: []
//                 } as IpScoreQuantitativeWellnessEntryTracking
//             } as IpScoreQuantitativeWellnessComponentTracking
//         } as IpScoreQuantitativeWellnessTracking
//     } as IpScoreTracking;
// }


export const updateAtleteSubscriptionExpiration = async (validationResponse: SaveEventDirective, transactionId: string,
    athleteRef: FirebaseFirestore.DocumentReference, savedFrom) => {
    const expirationDate = validationResponse.expirationDate;
    const startDate = validationResponse.startDate;
    const expireDateProp = 'profile.subscription.expirationDate';
    const transactionIdsProp = 'transactionIds';
    const commencementDateProp = 'profile.subscription.commencementDate';
    const values = {};


    console.warn('values[expireDateProp] = moment(expirationDate).toDate();', moment(expirationDate).toDate())

    values[commencementDateProp] = startDate //moment(startDate).toDate();
    values[expireDateProp] = expirationDate //moment(expirationDate).toDate();
    values[transactionIdsProp] = firestore.FieldValue.arrayUnion(transactionId);

    debugger;


    let appIdentifier = ''
    let productIdentifier = ''
    let transactionIdentifier = ''
    let originalTransactionIdentifier

    if (validationResponse.platform === 'Android') {

        const receipt = validationResponse.androidReceipt
        appIdentifier = receipt.packageName
        productIdentifier = receipt.productId
        debugger;
        transactionIdentifier = transactionId
        originalTransactionIdentifier = receipt.originalTransactionId

    } else {
        const receipt = validationResponse.iosReceipt
        appIdentifier = receipt.bundleId
        productIdentifier = receipt.productId
        debugger;
        transactionIdentifier = transactionId
        originalTransactionIdentifier = receipt.originalTransactionId
    }
    let saveUpdate = false

    const athlete = await getDocFromRef(athleteRef) as Athlete
    let sub: AthleteSubscription;
    let force = false;
    const prof = athlete.profile;
    if (!prof.subscription.appIdentifier) {
        sub = prof.subscription;
    } else {
        if (prof.subscription.appIdentifier.toLowerCase() === appIdentifier.toLowerCase()) {
            sub = prof.subscription;
        } else {
            if (isArray(prof.appSubscriptions)) {
                sub = prof.appSubscriptions.find((s: AthleteSubscription) => {
                    return s.appIdentifier.toLowerCase() === appIdentifier.toLowerCase();
                });

                if (!sub) {
                    force = true;
                }
            } else {
                force = true;
            }
        }
    }
    if (force || (sub && sub.type !== SubscriptionType.Basic)) {

        const paymentType = getPaymentTypeFromProductIdentifier(productIdentifier);
        const subscriptionType = getAccountTypeFromProductIdentifier(productIdentifier);

        // if (expDate === now) {
        //     switch (paymentType) {
        //         case SubscriptionPaymentType.Monthly:
        //             // now.setDate(now.getDate() + 30);
        //             expDate = this.addDays(now, 30);
        //             break;
        //         case SubscriptionPaymentType.Quarterly:
        //             // now.setDate(now.getDate() + 90);
        //             expDate = this.addDays(now, 90);
        //             break;
        //         case SubscriptionPaymentType.Annual:
        //             // now.setDate(now.getDate() + 365);
        //             expDate = this.addDays(now, 365);
        //             break;
        //     }
        // }

        updateExistingAthleteForPurchasing(
            athlete,
            subscriptionType,
            SubscriptionStatus.ValidInitial,
            paymentType,
            startDate,
            expirationDate,
            appIdentifier,
            productIdentifier,
            savedFrom
        );

        return updateAthleteForPurchasing(athlete, originalTransactionIdentifier) //transactionId)
            .then(() => {
                return true
            })
            .catch(err => {
                return err
            })
        // return athleteRef
        //     .update(values)
        //     .then(() => {
        //         console.log('Update Atlete Subscription Expiration and add transactionId to array succeeded - values => ', values);

        //         return true;
        //     })
        //     .catch(err => {
        //         console.log('Update Atlete Subscription Expiration and add transactionId to array failed', err);
        //         return false;
        //     });

    } else {
        return true
    }

};
