// import { error } from 'util';

import { TransactionReceiptValidationRequest } from '../../../../../../models/purchase/interfaces/transaction-receipt-validation-request';
import { getAndroidRecieptFromToken } from '../../../controllers/getAndroidRecieptFromToken';
import { SaveEventDirective } from '../../../../../../models/purchase/interfaces/save-event-directive';
import { persistAthleteReceipt } from './persist-athlete-receipt';
import { updateAtleteSubscriptionExpiration } from './update-atlete-subscription-expiration';
import { extractAndroidPersistData } from './extract-android-persist-data';
import { generateReceiptObject } from './generate-receipt-object';
import { isProductionProject } from '../../../is-production';

export const validateAndroid = (data: TransactionReceiptValidationRequest, packageName: string, productIdentifier: string, athleteUid: string) => {

    const getPayload = () => {

        const thePackageName = packageName ? packageName : (isProductionProject() ? 'com.inspiresportonline.main' : 'com.inspiresportonline.dev');

        // const liveAuthBroke = {
        //     private_key: '-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCWcy+zrokUIXy0\nJ9n9nUNoKGqfGfWLq1DS7GDoKuFRDaAAORuqlFcCpJn3sqXxuMHFzXWL6SVJymEs\nphDiBEnVTnIGDBrWweajrBq3RcjP16gmwD9n6LchHBKUe0Tq0t2oh72Yj45ZkOkD\nuhzFVor7QLzCoAc/Fyr+55gQPxLo3hHIk5spmuNehAQe0rijFDVBlqiHLjXox6rH\nwvByHgZDPfU5a5SfQmtuCrMA6LqDVz/lwgwH2xm0WH4FhtTj5PndQ4u7kj8up3MD\n0ZUjHrlnx/eREHFY64vP7YYV50O/jqs6EdTKSY0nFV/55UAneJ2Asm+2V/3inWEx\niTJhhZwJAgMBAAECgf8+VwYeRpZe6G2cYCIHVw75t09jE8ENbWVe7QmJUXZaMzdn\n8/MgWPShI7qorUoVkD3OQlQ3kWPOVw12wYBkxE5xO9TvwDuthg1aOqdNcRM6leaN\nO7lY8orvQO2tDvDOXJGjpf3CYM1Mdvo93+Ba0PeKrm/XcHfe+1gkR1xYmhjZUK/f\n/NnTqYSeKeElSY/NNIJsQHGI6hmUZxyHL+GRtTqOMb7UrJ9HS7LXWQmml14XVXT1\nu9nFmhYv0xoDypqf0SyqCa667anmsSSiFSDJtldsJh5PEUMUx1r6bRrPS3N+HEUb\nqAZXBJprPTG+rlfM7/0PUxB40NJ6d42PiN+TtS0CgYEAxK4ucAVexXOPkiWX2/Zx\nBzLOo3rAO86IVmWWzUWRQZ7A7jLjXfB4WzQQpSgAOpIcF4dRE3MFCqG7RDlHaevC\nkttsqpXprVnZRNydrrCdeWR0N/MVtejUx692D9vYd3jgC6FOS9DU0n9LId+7uCpk\nWKbQ0Cj8i4c5pFZT5vB+HkcCgYEAw9OFwrOcj5jtTfSOvFf9lFDjobA64PZRtgMl\nZRu4UyMA2XuWV9dFkgftWfbDg07Fzxvkhq0tR9UCcm9QTZAWQ7AEFPGwh6k/3zaf\nl32oSzKYnadetv6/wqvalHlTaagWmVCKmAU739fhc/opUbudCgJWpitxMxYBFhVJ\nyBvnCy8CgYBZm85WhWvXZD4+ZNhahF6c2/4fp5ab6Q5e5qwdXBvBHPZj0FWdACml\nzAPla2MnTFh2M9Wxmc7rOCKdA8fQr3SX+lb5JZIww+XkpOtGovssSqNUtnDmz2za\nicutkAmA7VaOlfCZRE0ilpKBnXDmiJHtfhtXK8VcQMD3acmSR3H+kwKBgQCQa5Yx\ngp9LNACHRn5iHw6P7JwEaeDyehbHAkj46VmmTqFYw5GcVNIlIZ2FhLyqQVUXKj3+\nqQdE+65jUP7LRSF2aACKpbiIRGUrrLBEXFLaqoTteDsdAIHSA0Yfy96dhG6uQT0F\nYo7GxgKqsur4MUJe2FypDk7c2zSP4cFZSGK71wKBgBvPeQ3KloRPYFC5NeUL9/je\nnmr1HB6gURsdUwBCXo1QupCPUc2K4xc1IHiiFG9UV7XaxIutHx+ip2Ircri5aPi+\nsKEPmyeaTprMDZfRXlkLtXJD56jkIE+N6zRl45VhC66OpjYCGdud/ZF5UzZ0Ju7d\nNkqGorbotypQupidq94T\n-----END PRIVATE KEY-----\n',
        //     client_email: 'inspire-219716@appspot.gserviceaccount.com',
        // };
        // const liveAuth = {
        //     "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCW9Jy0muEe4DXm\n0AFgnxKCxsQta73JyUR14A/r7PDh6vGwmEp333Aq0iScuUr9naf5PFr4KMdI8fmI\nyjnSOqccdGogJgFIvViesuzGH+WTqG1BApLIWF4yzlMcOJbv1WRiT71BT9QbNCJg\nnUNLlU6mExg4WUampyQ2BfEbwX6f8BnV3XKNJwSEcARPp+0/sXY2kswCTAjnqFve\nrOntpz1A14aG7KKTLhdgcnqltSyVUiUvl2hdacq6isr9WNUFegZT9nsmBs7vGPLD\nwC6xTuUmtZAmpsmfribHpcbeb/KSUW7Knr9Jkow4Z+nJohE5PsEVQhFsO3lEz0/7\nNKJ2dE9jAgMBAAECggEAAWYU2rY7WYSgErQLpZ83/FuzRperL0Sc5h4vDfv+I2nE\n5/Oq5x1RgtL/y/u049kfABF8xPiQj6ICGOvJc+f49aNFtpryeypmhcBVN1OyEeRC\n5/hwmb+UI5st9Qkv3nSmEpvvegDaGBfFHC2J8Acn8vFV88jH/Yu7ml1Y7iC13iCU\nPYBML54T3pNiUyqD4V/OEmGRfCa2EHWIx3etMIPnA5AeEkv/0v3Jm8Pk2jEbpYPv\nXvu4s2iGXl0P9H6EV+dTWQ+p0itVgwwfhAbDsFBSY0dUrKI17Lumzc9S3QdamBFy\n4H2Y1Z/0cxUvMvhDVATz/G8Nuinap/cXyllTrjkWGQKBgQDR/lMlG5hHGyEnbaFv\n3s+meD/8OIqYvqmFcUmjNMhD5N2YKb0wYhRCyV3UdR66OskRNIAHRvEEWI2hkjG/\nj2kDjURtU+kfBtXIgYmYyAETNIjpLBa7GvJAYX4DjMq2KXzzpu1jpt5Xttrq6vTl\nP6jPgWqy0ZjjrLMWHhWfPU96fwKBgQC4Bxcmx7Kl+mo4zeLOXWOpN/gXiytiSgvj\nzdh5bOUhb62V/65s6YPPm4i9XhrbXmS7LaPAckbmYldannfsZHjrAXTWN5cz+eWf\nRvUGxb91UJUM9JGet1TQDvqY0UlwQJEjgiXEvE+scDlvjvJ2latpsykc1VMDceHQ\n/WQxQC8RHQKBgAYOUZOGYjPawnACp180i8R/P0kk0EprZTQl+jussAHVPda/jWcx\nzOfIKt6Pqh0wyM+oNfD/yL+ZVixMrQabOioSDuEgRF7SsaOgqLkxO2a0YyZmURmA\nQ3A734h2rPnn5GSaccon682XxmRz9jAOT+38C3mwtboLJvcCUxlOiVY9AoGAINNS\n893nJegXDm7bKAhMgKC1Ji3y3SNsH4RWOsKL1H0BL6Jd0JuOoXeS2oRwsfSSbzji\n3q8DqpCcGFPv/ECnuPOXjaZ42BKZPOIBTuXzEs/+5kKxvufgvwQM+zlJqjY6LWVH\nq2ITrCSQei+7XB+yZbZJfRUoK5Mivo6UUpyqSJkCgYAkplMbmeNo+oD9eCsx0/y0\nR3dXnnX5xCkllfcsAN3bWq4hGstxONRj4sIg+rPHcuyl2B2ST2H9dkWkibm6FdBF\naMNlBs5VMGdOgUFUl2BYpN53kXOvXeyw9ByFZWmhxOnKSGCa33fvMxYSw1WzB7DD\n/H+xvCzdnAC2GEKkurFI3A==\n-----END PRIVATE KEY-----\n",
        //     "client_email": "google-play-subscription-verif@inspire-219716.iam.gserviceaccount.com",
        // };
        const devAuth = {
            private_key: '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCkr9ZRjXhZImyJ\nExcfXVkFqNlnvfoSdfW4bTGYWDmcOad8iJvv7T3v1W14zTCQjNcKRibMeNBoDrYW\neekUsbfYOeA834ltUE/WE1w0BucRJbojZarwe0O6+1C5QrOL43Hio8Iq74PlFLKk\n9O5rOKXUHrVHwTFjS8/lY2aPUimSZYXtDBU/OBpM+2HzBeP82LacUnaecJbtlrym\nf1nifbG6nR/UWjHgMPf+lrR0Zn2a9lIUn02Acm3G/1yA+hD/u/3bSEX+TMQtmQOv\nzgvfb66LPfUVWe8q96vsATl0VWwvMagjfqh6LoGff1OjAL1uj4i5o6nPrqzEHDfa\n0kEosWBBAgMBAAECggEAC/knww6exxY4X1/msLZT9FQiGEeI4KI4XvP7ZNjXOWs8\nqNJyyecM93ykJKIAa6X9tWbDx95pwoL9TJWI82L3W45bpflXj73EzCrkq3isAIRm\na8/mtWy00CmY5Rs7HArJeyGOSppW08clNNaE5gE8lzczVVfvrAk1QHdxW66szJKc\nlD9jUF2QjHQBURy9cvEq4oO5gAbmcYsmLumjQnxEo0i6lmK/Tc3HJoypgRYHAtPD\nS7aFQyptpHur/hBg/Xiv7BTg0Mj4DiQW8Qaci/6A07ya4IK2M357Xq64dNZEezQO\neU7wf83WVHwr4883UQGt24YojcqHoUW/jHuDRwVkRQKBgQDcB+KsmSHDc99r2jgH\naf0VAg73UABtVnBEwwipAtPcFBNrKneXwLjfmlxoob/Q+jYiQ2Z+8EXfFcpJJOFC\nzHCPlTCBeTOwxf4dU8zbxtZsfmyHkj3B30DCzJGk4Ax9MSCy+ewtw/xoR6KdOhlr\nqPCKV2TMGVCXSLeWk9hv1X1rXQKBgQC/m9oQFAH0QaW7ZJIjx7u9V+CAuugckH8X\n/o1XZY1FhcxEnxFgG4QGe4dhIEXTF2vCndq+fRBEOuiRg3yCE3OLWl9Wt9sD5fce\njXVUi3daFJXdQo0dcsqtJ1Q5tXaZPoml2pVZxt+iBenh8Z85JxH9+7tc45amc1O3\nHmGQ0qZeNQKBgQCWHuMu84OvwN0MzuQvWscLkE35uqGv96u9nnvIJF+75g6hrWXP\nKfR4yu6FjOY8hJpuoiHKNdDWNh2/7eOrGaUqsZVYoQL9dvi7tbMtt+oQN+mATezI\n27NptP0hyqN6vwwaUJ4tU2xhEY8HSt6RL8B+AsaI4jS0Iy7vE4w2MSjTGQKBgHoJ\nfLjS1W/JxBH3ezC4zPVKnB3BbYaL7bbNlR49+t1122U1Xu60d8FdOht9X5uUBjld\nKu46X3rlfiz37vw2AViXRbPIxADWni9ib4FalrjT9aOH+LLx4u6n5vgegJwX/bmZ\n35ffl53tYEpdB0lyff4jL/F4rwHy4DX4brG7yOSlAoGBAIIPm2UQZPHTxazyJKVr\n1GwQQpiKuhRdR03VwqieEDr1bQi/tVzyhEZLp+Mb8NXfJNnIGRyA77iU9OUVAiN3\nKe4ewkquXavMEIlXUli5Jtljqz/3ldXHUxaMlnciGAopnBI2XoYZOyHUMfTkuROg\nUhKwZFKHM8UJ5hMxPu4NWbO2\n-----END PRIVATE KEY-----\n',
            client_email: 'gooleplayapi@inspire-1540046634666.iam.gserviceaccount.com'
        };

        const config = isProductionProject() ? devAuth : devAuth;
        const theKeys: {
            private_key: string;
            client_email: string;
        } = config// isProductionProject() ? liveAuth : devAuth;

        console.log('config', config)
        console.log('process.env.GCP_PROJECT', process.env.GCP_PROJECT)
        console.log('theKeys', theKeys)
        const { productIdentifier: productId, transactionReceipt: purchaseToken, } = data;
        const theReceipt = {
            packageName: thePackageName,
            productId,
            purchaseToken,
        };
        return { keys: theKeys, receipt: theReceipt };
    };

    const { keys, receipt }: {
        keys: {
            private_key: string;
            client_email: string;
        };
        receipt: {
            packageName: string;
            productId: string;
            purchaseToken: string;
        };
    } = getPayload();

    const theAndroidReceipt = getAndroidRecieptFromToken(keys, receipt)
        .then(async (purchaseReceipt) => {
            if (purchaseReceipt.isSuccessful) {
                // const { errorMessage, payload } = data
                console.log('data.isSuccessful');
                const androidReceipt = extractAndroidPersistData(purchaseReceipt);
                // console.log('data', data)

                const receiptDetails: SaveEventDirective = generateReceiptObject(purchaseReceipt, androidReceipt);
                
                const { data: returnedData, theOriginalTransactionId, athleteRef, } = await persistAthleteReceipt(athleteUid, receiptDetails);
                
                if (returnedData) {
                    await updateAtleteSubscriptionExpiration(returnedData, theOriginalTransactionId, athleteRef, 'serverValidateReceipt');
                    const successMessage = 'validateReceipt';
                    const successResponse = {
                        // ...theRest,
                        transactionId: theOriginalTransactionId,
                        transactionIdentifier: theOriginalTransactionId,
                        productIdentifier,
                        isValid: true,
                        message: successMessage,
                        startDate: receiptDetails.startDate.toJSON(),
                        expirationDate: receiptDetails.expirationDate.toJSON(),
                    };
                    return {
                        responseCode: 201,
                        // data: { validatedData, purchaseData },
                        data: successResponse,
                    };
                }
                else {
                    console.log('NO returnedData');
                    return {
                        responseCode: 501,
                        // data: { validatedData, purchaseData },
                        data: { message: 'Unable To Save Receipt' },
                    };
                }
            }
            else {
                // const { errorMessage, payload } = data
                console.log('NOT!!!!!!!! data.isSuccessful');
                // TODO:
                // return persistAthleteReceipt(data.developerOptions, {
                //     ...theResponse,
                //     validatedData,
                //     purchaseData: data.payload,
                // }).then(theRes => {
                //     const message = 'validateReceipt'
                //     const theResponse = {
                //         // ...theRest,
                //         isValid: true,
                //         message,
                //     }
                //     return res.json({
                //         responseCode: 201,
                //         // data: { validatedData, purchaseData },
                //         data: theResponse,
                //     })
                //     // return res.status(500).json(response) //(validateResponse)
                // })
                return {
                    message: 'An unanticipated exception occurred in validateTransactionReceipt',
                    responseCode: 500,
                    data: purchaseReceipt,
                    error: {
                        message: 'An unanticipated exception occurred in validateTransactionReceipt',
                        // exception: error,
                    },
                    method: {
                        name: 'validateTransactionReceipt'
                    }
                };
            }
        })
        .catch(err => {
            console.log('error getting receipt form token', err);
            throw err;
        });
    return theAndroidReceipt;
};
