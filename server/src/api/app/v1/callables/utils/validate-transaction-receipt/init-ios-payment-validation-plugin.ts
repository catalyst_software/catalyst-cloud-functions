import { isProductionProject } from '../../../is-production';
export const initIOSPaymentValidationPlugin = (iap, applePassword: any) => {
    if (isProductionProject()) {
        iap.config({
            /* Configurations for Apple */
            // appleExcludeOldTransactions: true, // if you want to exclude old transaction, set this to true. Default is false
            // the App-Specific Shared Secret: https://appstoreconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/app/1453032120/addons
            applePassword: applePassword
                ? applePassword
                : '0e5b544e5da74ab48ffc00dc6600bd29',
            /* Configurations all platforms */
            test: false,
            verbose: false,
        });
    }
    else {
        iap.config({
            /* Configurations for Apple */
            // appleExcludeOldTransactions: false,
            // the App-Specific Shared Secret: https://appstoreconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/app/1453032120/addons
            applePassword: applePassword
                ? applePassword
                : 'bbfdc220bd9445bbae3543f6974940fc',
            /* Configurations all platforms */
            test: true,
            verbose: true,
        });
    }
};

// SoF Shared Secret 518c8944b04444a483444dd71c39267f