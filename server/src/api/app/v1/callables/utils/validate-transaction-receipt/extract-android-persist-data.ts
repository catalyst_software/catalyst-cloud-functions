import { AndroidPurchaseReceipt } from '../../../../../../models/purchase/purchase.model';
import { AndroidCancelReason, AndroidPurchaseType, AndroidPaymentState } from '../../../../../../models/enums/android-notification-type';

export const extractAndroidPersistData = (purchaseReceipt: AndroidPurchaseReceipt) => {
    const { payload } = purchaseReceipt;
    const { acknowledgementStateString, autoRenewing, cancelReason, countryCode, expiryTimeMillis, kind, orderId, paymentState, paymentStateString, priceAmountMicros, priceCurrencyCode,
        purchaseType, startTimeMillis, } = payload;

    let receipt;

    receipt = {
        ...payload,
        acknowledgementState: payload.acknowledgementStateString,
        autoRenewing,
        cancelReason: AndroidCancelReason[cancelReason] || cancelReason,
        countryCode,

        startTimeMillis: +startTimeMillis,
        startDate: new Date(+startTimeMillis),

        expiryTimeMillis: +expiryTimeMillis,
        expirationDate: new Date(+expiryTimeMillis),
        isExpired: new Date(+expiryTimeMillis) < new Date(new Date()),

        kind,
        orderId,
        // originalOrderId: orderId.slice(0, 24),
        priceAmountMicros: +priceAmountMicros,
        priceCurrencyCode,
        purchaseType: purchaseType !== undefined ? AndroidPurchaseType[purchaseType] : '',


    };
    
    if (paymentState !== undefined) {
        receipt.paymentState = paymentState //AndroidPaymentState[paymentState] // paymentStateString;
    }

    return receipt as {
        acknowledgementState: string;
        autoRenewing: boolean;
        cancelReason: string | number;
        countryCode: string;
        startTimeMillis: number;
        startDate: Date;
        expiryTimeMillis: number;
        expirationDate: Date;
        isExpired: boolean;
        kind: string;
        orderId: string;
        priceAmountMicros: number;
        priceCurrencyCode: string;
        purchaseType: string;
        paymentState?: string
    };
};
