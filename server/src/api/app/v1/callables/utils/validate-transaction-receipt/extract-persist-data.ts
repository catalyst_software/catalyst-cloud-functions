import { AndroidPurchaseReceipt } from '../../../../../../models/purchase/purchase.model';
import { AndroidCancelReason, AndroidPurchaseType } from '../../../../../../models/enums/android-notification-type';
import { AndroidReceipt } from '../../../../../../models/purchase/interfaces/android-receipt';


export interface NotificationReceipt {



    productId: string;
    packageName: string; // appIdentifier
    uid?: string;
    autoRenewing: boolean;
    cancelReason: AndroidCancelReason;
    countryCode: string;
    expiryTimeMillis: number;
    expiryTime: Date;
    kind: string;
    orderId: string;
    originalOrderId: string;
    priceAmountMicros: number;
    priceCurrencyCode: string;
    purchaseType: number;
    startTimeMillis: number;
    startTime: Date;

    // CHECK THESE
    transactionId: string;
    originalTransactionId: string

    isExpired: boolean;
    paymentState?: number;
}

export const extractPersistData = (purchaseReceipt: AndroidPurchaseReceipt) => {
    const { payload } = purchaseReceipt;
    const { autoRenewing, cancelReason, countryCode, expiryTimeMillis, kind, productId, orderId, paymentState, priceAmountMicros, priceCurrencyCode, purchaseType, startTimeMillis, } = payload;

    const receipt: AndroidReceipt = {
        autoRenewing,
        cancelReason: AndroidCancelReason[cancelReason] || cancelReason as any, // TODO
        countryCode,
        expiryTimeMillis: +expiryTimeMillis,
        expiryTime: new Date(+expiryTimeMillis),
        kind,
        orderId,
        productId,
        originalOrderId: orderId.slice(0, 24),
        transactionId: orderId,
        originalTransactionId: orderId.slice(0, 24),
        priceAmountMicros: +priceAmountMicros,
        priceCurrencyCode,
        purchaseType: purchaseType !== undefined ? AndroidPurchaseType[purchaseType] : '' as any, // TODO
        startTimeMillis: +startTimeMillis,
        startTime: new Date(+startTimeMillis),
        isExpired: new Date(+expiryTimeMillis) < new Date(new Date()),
    };
    if (paymentState) {
        receipt.paymentState = paymentState;
    }
    return receipt;
};
