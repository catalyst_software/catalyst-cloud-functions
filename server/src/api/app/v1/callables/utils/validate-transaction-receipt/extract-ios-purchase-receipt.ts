import { IOSPurchaseReceipt } from '../../../../../../models/purchase/purchase.model';
import { IOSSubscriptionExpirationIntent, IOSSubscriptionAutoRenewStatus, IOSSubscriptionRetryFlag, IOSNotificationStatusType } from '../../../../../../models/enums/ios-notification-type';

export const extractIOSPurchaseReceipt = (purchaseReceipt: IOSPurchaseReceipt) => {
    const { auto_renew_product_id, auto_renew_status, expiration_intent: expirationIntent, is_in_billing_retry_period, latest_expired_receipt_info, receipt, sandbox,
        // service,
        status, } = purchaseReceipt;
    const autoRenewing = IOSSubscriptionAutoRenewStatus[auto_renew_status] === 'Active'
        ? true
        : false;
    const isInBillingRetryPeriod = IOSSubscriptionRetryFlag[is_in_billing_retry_period] === 'Active'
        ? true
        : false;
    const result: any = {
        autoRenewing,
        isInBillingRetryPeriod,
        autoRenewProductId: auto_renew_product_id,
        latestExpiredReceiptInfo: latest_expired_receipt_info,
        receipt,
        sandbox,
        // service,
        status: IOSNotificationStatusType[status],
    };
    if (expirationIntent)
        result.expirationIntent =
            IOSSubscriptionExpirationIntent[expirationIntent];
    return result;
};
