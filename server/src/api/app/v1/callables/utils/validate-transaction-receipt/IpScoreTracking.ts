export interface IpScoreTracking {
    dateTime: Date;
    ipScore: number;
    directive: IpScoreTrackingDirective;
    quantitativeWellness: IpScoreQuantitativeWellnessTracking;
    qualitativeWellness: IpScoreQualitativeWellnessTracking;
    programs?: IpScoreProgramTracking;
}
export interface IpScoreTrackingDirective {
    execute: boolean;
    updateAthleteIndicatorsOnGroups?: boolean;
    includeGroupAggregation: boolean;
    paths: Array<string>;
    isPenalized?: boolean;
    maxScorePossible?: number;
    utcOffset?: number;
}
export interface IpScoreQuantitativeWellnessTracking {
    include: boolean;
    brain: IpScoreQuantitativeWellnessBrainTracking;
    body: IpScoreQuantitativeWellnessBodyTracking;
    food: IpScoreQuantitativeWellnessComponentTracking;
    training: IpScoreQuantitativeWellnessComponentTracking;
}
export interface IpScoreQualitativeWellnessTracking {
    include: boolean;
}
export interface IpScoreProgramTracking {
    videoConsumed: boolean;
    surveyConsumed: boolean;
    worksheetConsumed: boolean;
    expectedDailyEntries?: number;
    recordedDailyEntries?: number;
    lastUpdate?: Date;
    staticValue?: number;
}
export interface IpScoreQuantitativeWellnessBrainTracking {
    mood: IpScoreQuantitativeWellnessEntryTracking;
    sleep: IpScoreQuantitativeWellnessEntryTracking;
}
export interface IpScoreQuantitativeWellnessBodyTracking {
    fatigue: IpScoreQuantitativeWellnessEntryTracking;
    pain: IpScoreQuantitativeWellnessEntryTracking;
    wearables: IpScoreQuantitativeWellnessEntryTracking;
}
export interface IpScoreQuantitativeWellnessComponentTracking {
    components: IpScoreQuantitativeWellnessEntryTracking;
}
export interface IpScoreQuantitativeWellnessEntryTracking {
    recordedDailyEntries: number;
    tracking?: Array<any>;
    lastUpdate?: Date;
}
