import { AndroidPurchaseReceipt } from '../../../../../../models/purchase/purchase.model';
import { SaveEventDirective } from '../../../../../../models/purchase/interfaces/save-event-directive';
import { AndroidNotificationType } from '../../../../../../models/enums/android-notification-type';
import { PlatformType } from '../../../../../../models/purchase/enums/platfom_type';

export const generateReceiptObject = (data: AndroidPurchaseReceipt, androidReceipt: {
    acknowledgementState: string;
    autoRenewing: boolean;
    cancelReason: string | number;
    countryCode: string;
    startTimeMillis: number;
    startDate: Date;
    expiryTimeMillis: number;
    expirationDate: Date;
    isExpired: boolean;
    kind: string;
    orderId: string;
    priceAmountMicros: number;
    priceCurrencyCode: string;
    purchaseType: string;
    paymentState?: string
}): SaveEventDirective => {

    const { startDate, expirationDate } = androidReceipt

    return {
        platform: PlatformType.Android,
        transactionId: data.payload.orderId,
        originalTransactionId: data.payload.orderId.slice(0, 24),
        startDate,
        expirationDate,
        notificationType: <any>(AndroidNotificationType[AndroidNotificationType
            .ValidatingInitialBuy]),
        androidReceipt: androidReceipt as any,
    };
};
