import { IOSPurchaseData } from '../../../../../../models/purchase/purchase.model';
import { PlatformType } from '../../../../../../models/purchase/enums/platfom_type';
import { IOSReceipt } from '../../../../../../models/purchase/interfaces/ios-receipt';
import { extractReceipt } from "./extract-receipt";
import { extractExpiredReceipt } from "./extract-expired-receipt";
export const extractIOSPersistData = (theNotificationData: any, // IOSPurchaseReceipt,
    purchaseReceipt: Array<IOSPurchaseData>): IOSReceipt => {
    const { receipt, latestExpiredReceiptInfo, ...notificationRest } = theNotificationData;
    // const {
    //     auto_renew_product_id,
    //     auto_renew_status,
    //     expiration_intent: expirationIntent,
    //     is_in_billing_retry_period,
    //     latest_expired_receipt_info,
    //     receipt,
    //     sandbox,
    //     service,
    //     status,
    // } = purchaseReceipt // as IOSPurchaseReceipt
    // const theNotification = {
    //     autoRenewing: false,
    //     isInBillingRetryPeriod: false,
    //     expirationIntent: 'CanceledByUser',
    //     autoRenewProductId: 'com.inspiresportonline.dev.subscriptions',
    //     latestExpiredReceiptInfo: {
    //         original_purchase_date_pst:
    //             '2019-02-18 08:04:12 America/Los_Angeles',
    //         unique_identifier: '90518714aea0d8d20c8eba0724ffaaf0b345990b',
    //         original_transaction_id: '1000000503436015',
    //         expires_date: '1552069587000',
    //         transaction_id: '1000000508890435',
    //         quantity: '1',
    //         product_id: 'com.inspiresportonline.dev.subscriptions',
    //         bvrs: '1.9',
    //         bid: 'com.inspiresportonline.dev',
    //         unique_vendor_identifier: '40B09BFC-DF7E-4B4D-B48A-AEEFDF0A6527',
    //         web_order_line_item_id: '1000000043133750',
    //         original_purchase_date_ms: '1550505852000',
    //         expires_date_formatted: '2019-03-08 18:26:27 Etc/GMT',
    //         purchase_date: '2019-03-08 18:21:27 Etc/GMT',
    //         is_in_intro_offer_period: 'false',
    //         purchase_date_ms: '1552069287000',
    //         expires_date_formatted_pst:
    //             '2019-03-08 10:26:27 America/Los_Angeles',
    //         is_trial_period: 'false',
    //         purchase_date_pst: '2019-03-08 10:21:27 America/Los_Angeles',
    //         original_purchase_date: '2019-02-18 16:04:12 Etc/GMT',
    //         item_id: '1453012404',
    //     },
    //     receipt: {
    //         original_purchase_date_pst:
    //             '2019-02-18 08:04:12 America/Los_Angeles',
    //         quantity: '1',
    //         unique_vendor_identifier: '0D439F5E-6D71-4BC8-8297-CB048C7092FE',
    //         bvrs: '1.9',
    //         expires_date_formatted: '2019-02-19 17:53:45 Etc/GMT',
    //         is_in_intro_offer_period: 'false',
    //         purchase_date_ms: '1550598525000',
    //         expires_date_formatted_pst:
    //             '2019-02-19 09:53:45 America/Los_Angeles',
    //         is_trial_period: 'false',
    //         item_id: '1453012404',
    //         unique_identifier: '90518714aea0d8d20c8eba0724ffaaf0b345990b',
    //         original_transaction_id: '1000000503436015',
    //         expires_date: '1550598825000',
    //         transaction_id: '1000000503807136',
    //         web_order_line_item_id: '1000000042817425',
    //         version_external_identifier: '0',
    //         bid: 'com.inspiresportonline.dev',
    //         product_id: 'com.inspiresportonline.dev.subscriptions',
    //         purchase_date: '2019-02-19 17:48:45 Etc/GMT',
    //         original_purchase_date: '2019-02-18 16:04:12 Etc/GMT',
    //         purchase_date_pst: '2019-02-19 09:48:45 America/Los_Angeles',
    //         original_purchase_date_ms: '1550505852000',
    //     },
    //     sandbox: true,
    //     service: 'apple',
    //     status: 'success',
    // }
    // const {
    //     unique_vendor_identifier, //: '0D439F5E-6D71-4BC8-8297-CB048C7092FE',
    //     bvrs, //: '1.9',
    //     expires_date_formatted, //: '2019-02-19 17:53:45 Etc/GMT',
    //     is_in_intro_offer_period: isInIntroOfferPeriod, //: 'false',
    //     purchase_date_ms, //: '1550598525000',
    //     expires_date_formatted_pst, //:        '2019-02-19 09:53:45 America/Los_Angeles',
    //     is_trial_period: isTrialPeriod, //: 'false',
    //     item_id, //: '1453012404',
    //     unique_identifier: uniqueIdentifier, //: '90518714aea0d8d20c8eba0724ffaaf0b345990b',
    //     original_transaction_id, //: '1000000503436015',
    //     expires_date, //: '1550598825000',
    //     transaction_id, //: '1000000503807136',
    //     web_order_line_item_id: webOrderLineItemId, //: '1000000042817425',
    //     version_external_identifier, //: '0',
    //     bid, //: 'com.inspiresportonline.dev',
    //     product_id, //: 'com.inspiresportonline.dev.subscriptions',
    //     purchase_date, //: '2019-02-19 17:48:45 Etc/GMT',
    //     original_purchase_date, //: '2019-02-18 16:04:12 Etc/GMT',
    //     original_purchase_date_pst,
    //     purchase_date_pst, //: '2019-02-19 09:48:45 America/Los_Angeles',
    //     original_purchase_date_ms, //: '1550505852000',
    //     ...expiredreceiptRest
    // } = latestExpiredReceiptInfo
    const receiptParsed = extractReceipt(receipt);
    const { transactionId, originalTransactionId, expirationDate, appItemId, bundleId, cancellationDate, isTrial, originalPurchaseDate, productId, purchaseDate, quantity, ...purchaseReceiptRest } = purchaseReceipt[0];
    const parsedData: IOSReceipt = {
        platform: PlatformType.IOS,
        // TODO: Check this
        itemId: appItemId,
        transactionId,
        originalTransactionId,
        expirationDate: new Date(+expirationDate),
        // appItemId: +appItemId,
        bundleId,
        cancellationDate: cancellationDate
            ? new Date(+originalPurchaseDate)
            : cancellationDate,
        isTrial,
        originalPurchaseDate: new Date(+originalPurchaseDate),
        productId,
        purchaseDate: new Date(+purchaseDate),
        quantity,
        ...notificationRest,
        ...receiptParsed,
        ...purchaseReceiptRest,
    };
    if (latestExpiredReceiptInfo) {
        parsedData.expiredReceipt = extractExpiredReceipt(latestExpiredReceiptInfo);
    }
    return parsedData;
};
