import moment from 'moment';
import { db } from '../../../../../../shared/init/initialise-firebase';
import { SaveEventDirective } from '../../../../../../models/purchase/interfaces/save-event-directive';
import { FirestoreCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';
import { AndroidNotificationType, AndroidPaymentState } from '../../../../../../models/enums/android-notification-type';
import { PlatformType } from '../../../../../../models/purchase/enums/platfom_type';
import { removeUndefinedProps } from '../../../../../../db/ipscore/services/save-aggregates';

export const persistAthleteReceipt = async (athleteUid: string, receiptDetails: SaveEventDirective, save = true): Promise<{
    data: SaveEventDirective;
    theOriginalTransactionId: string;
    athleteRef: FirebaseFirestore.DocumentReference;
}> => {

    const athleteRef = db
        .collection(FirestoreCollection.Athletes)
        .doc(athleteUid);

    const { platform, transactionId, originalTransactionId, notificationType, expirationDate, startDate } = receiptDetails;

    const theOriginalTransactionId = platform === 'Android'
        ? transactionId.slice(0, 24)
        : // : receipt.original_transaction_id
        originalTransactionId;

    const transactionDocSnap = await athleteRef
        .collection(FirestoreCollection.TransactionAuditRecord)
        .doc(theOriginalTransactionId)
        .get();

    const receipt = platform === PlatformType.Android
        ? receiptDetails.androidReceipt
        : receiptDetails.iosReceipt;

    let notification;

    if (receipt) {
        // ANDROID
        if (platform === PlatformType.Android) {
            const { androidReceipt, notificationType: ntype, ...rest } = receiptDetails;
            const { acknowledgementStateString, expiryTimeMillis, startTimeMillis, expiryTime, paymentState,
                orderId,
                startTime, ...receiptRest } = receipt as any; // AndroidReceipt
            notification = {
                ...receiptRest,
                ...rest,
                startDate,
                expirationDate,
                creationTimestamp: moment().toDate(),
                // notificationType: AndroidNotificationType[notificationType],
                notificationType,
                // transactionId: orderId
            };
            if (paymentState) {
                notification.paymentState = AndroidPaymentState[paymentState];
            }
        }

        // iOS
        else {

            // autoRenewing:false
            // autoRenewProductId:"com.inspiresportonline.dev.subscriptions.monthly.basic"
            // bundleId:"com.inspiresportonline.dev"
            // cancellationDate:0
            // creationTimestamp:Tue May 26 2020 22:56:41 GMT+0100 (British Summer Time) {}
            // expirationDate:Sat May 18 2019 08:04:07 GMT+0100 (British Summer Time) {}
            // expirationIntent:"CanceledByUser"
            // expiredReceipt:Object {isInIntroOfferPeriod: false, isTrialPeriod: false, expiresDate: Tue May 26 2020 13:44:39 GMT+0100 (British Summer …, …}
            // isInBillingRetryPeriod:false
            // isInIntroOfferPeriod:false
            // isTrial:false
            // isTrialPeriod:false
            // itemId:1461667182
            // notificationType:"VALIDATING_INITIAL_BUY"
            // originalPurchaseDate:Sat May 18 2019 07:04:08 GMT+0100 (British Summer Time) {}
            // originalTransactionId:1000000528918110
            // platform:"iOS"
            // productId:"com.inspiresportonline.dev.subscriptions.annual.basic"
            notification = {
                ...receipt,
                notificationType,
                transactionId: +transactionId,
                expirationDate,
                startDate,
                creationTimestamp: moment().toDate(),
                itemId: +(<any>receipt).itemId,
                isTrialPeriod: (<any>receipt).isTrialPeriod === 'false' ? false : true,
                isInIntroOfferPeriod: (<any>receipt).isInIntroOfferPeriod === 'false'
                    ? false
                    : true,
                webOrderLineItemId: +(<any>receipt).webOrderLineItemId,
                originalTransactionId: +(<any>receipt).originalTransactionId,
            };
        }
    }
    else {
        const { androidReceipt: androidReceiptRemoved, iosReceipt: iosReceiptRemoved, notificationType: notificationTypeRemoved, linkedPurchaseToken, webOrderLineItemId, ...directiveRest } = receiptDetails;
        // const {
        //     platform,
        //     transactionId,
        //     originalTransactionId,
        //     notificationType,
        //     expirationDate,
        // } = directive
        notification = directiveRest;
        if (linkedPurchaseToken)
            notification.linkedPurchaseToken = linkedPurchaseToken;
        if (webOrderLineItemId)
            notification.webOrderLineItemId = webOrderLineItemId;
    }

    removeUndefinedProps(notification);

    if (save) {
        if (transactionDocSnap.exists) {
            if (transactionId) {
                const doc = transactionDocSnap.data();
                if (!doc.notifications) {
                    doc.notifications = [notification];
                }
                else {
                    doc.notifications.push(notification);
                }
                const ress = await transactionDocSnap.ref.update(doc).then(() => {
                    console.log('notification saved')
                    return true
                }).catch((e) => {
                    debugger; console.error(e)
                    return false
                });
                return { data: receiptDetails, theOriginalTransactionId, athleteRef };
            }
            else {
                console.error('unable to save notification as transactionId is undefined')
                return undefined;

            }
        }
        else {
            await transactionDocSnap.ref.set({
                purchaseReceipt: notification,
            });
            return { data: receiptDetails, theOriginalTransactionId, athleteRef };
        }
    } else {
        return { data: receiptDetails, theOriginalTransactionId, athleteRef };
    }

};
