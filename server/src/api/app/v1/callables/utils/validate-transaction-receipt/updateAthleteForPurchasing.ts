import { isArray } from "lodash";

import { Athlete } from "../../../../../../models/athlete/athlete.model";
import { db } from "../../../../../../shared/init/initialise-firebase";

export const updateAthleteForPurchasing = (athlete: Athlete, transactionIdentifier: string): Promise<void> => {
    debugger;
 
    const payload = {
        profile: {
            subscription: athlete.profile.subscription,
            appSubscriptions: athlete.profile.appSubscriptions
        },
        currentIpScoreTracking: athlete.currentIpScoreTracking
    } as any;
    
    if (transactionIdentifier) {
        if (!isArray(athlete.transactionIds)) {
            athlete.transactionIds = [transactionIdentifier];
        }
        else if (athlete.transactionIds.findIndex((t) => t === transactionIdentifier) < 0) {
            athlete.transactionIds.push(transactionIdentifier);
        }
        payload.transactionIds = athlete.transactionIds;
    }
    
    return db.collection('athletes')
        .doc(athlete.uid)
        .set(payload, { merge: true })
        .then(() => {
            console.log('Document successfully updated! - updateAthleteForPurchasing');
        })
        .catch((err) => {
            // The document probably doesn't exist.
            console.error('Error updating document: ', err);
        });
};
