import * as iap from 'in-app-purchase'

import { TransactionReceiptValidationResponse, IOSPurchaseData } from '../../../../../../models/purchase/purchase.model';
import { SaveEventDirective } from '../../../../../../models/purchase/interfaces/save-event-directive';
import { IOSNotificationType } from '../../../../../../models/enums/ios-notification-type';
import { PlatformType } from '../../../../../../models/purchase/enums/platfom_type';
import { persistAthleteReceipt } from './persist-athlete-receipt';
import { extractIOSPurchaseReceipt } from './extract-ios-purchase-receipt';
import { extractIOSPersistData } from './extract-ios-persist-data';
import { updateAtleteSubscriptionExpiration } from './update-atlete-subscription-expiration';


// const crypto = require('crypto');

// export const encryptFunc = (text) => {
//     let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
//     let encrypted = cipher.update(text);
//     encrypted = Buffer.concat([encrypted, cipher.final()]);
//     return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
// }

// export const decryptFunc = (text) => {
//     let theiv = Buffer.from(text.iv, 'hex');
//     let encryptedText = Buffer.from(text.encryptedData, 'hex');
//     let decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), theiv);
//     let decrypted = decipher.update(encryptedText);
//     decrypted = Buffer.concat([decrypted, decipher.final()]);
//     return decrypted.toString();
// }


let theBody: TransactionReceiptValidationResponse = undefined
let theAthleteUid = ''
let thePayload = undefined

const onSuccess = async (validatedData) => {

    let response: TransactionReceiptValidationResponse = {
        ...theBody,
    }

    if (iap.isValidated(validatedData)) {
        const theNotificationData = extractIOSPurchaseReceipt(
            validatedData
        )
        // const {
        //     auto_renew_product_id,
        //     auto_renew_status,
        //     expiration_intent: expirationIntent,
        //     is_in_billing_retry_period,
        //     latest_expired_receipt_info,
        //     receipt,
        //     sandbox,
        //     service,
        //     status,
        // } = validatedData
        // const theExpirationIntent =
        //     IOSSubscriptionExpirationIntent[
        //         theNotificationData.expirationIntent
        //     ]
        // validatedData: the actual content of the validated receipt
        // validatedData also contains the original receipt
        const options = {
            ignoreCanceled: true,
            ignoreExpired: true,
        }
        // validatedData contains sandbox: true/false for Apple and Amazon
        const purchaseData = iap.getPurchaseData(
            validatedData,
            options
        ) as Array<IOSPurchaseData>
        response = {
            ...response,
            expirationDate: new Date(
                +validatedData.receipt.expires_date
            ),
        }
        const receipt = extractIOSPersistData(
            theNotificationData,
            purchaseData
        )
        // const {
        //     transactionId,
        //     originalTransactionId,
        //     expirationDate,
        //     appItemId,
        //     bundleId,
        //     cancellationDate,
        //     isTrial,
        //     originalPurchaseDate,
        //     productId,
        //     purchaseDate,
        //     quantity,
        // } = purchaseData as IOSPurchaseData
        const receiptDetails: SaveEventDirective = {
            // uid: string
            platform: PlatformType.IOS,
            transactionId: receipt.transactionId.toString(),
            originalTransactionId: receipt.originalTransactionId.toString(),
            expirationDate: receipt.expirationDate,
            startDate: receipt.purchaseDate ,
            // webOrderLineItemId:
            //     validatedData.receipt
            //         .web_order_line_item_id,
            notificationType:
                IOSNotificationType.ValidatingInitialBuy,
            iosReceipt: receipt,
        }
        return await persistAthleteReceipt(
            theAthleteUid,
            receiptDetails
            //     {
            //     ...response,
            //     validatedData,
            //     purchaseData,
            // }
        )
            .then(
                async ({
                    data,
                    theOriginalTransactionId,
                    athleteRef,
                }) => {
                    await updateAtleteSubscriptionExpiration(
                        data,
                        theOriginalTransactionId,
                        athleteRef,
                        'serverValidateReceipt'
                    )
                    const message = 'validateReceipt'
                    const theResponse = {
                        // ...thePayload,
                        transactionId: theOriginalTransactionId,

                        isValid: true,
                        // ? true
                        // : false,
                        message,
                    }
                    return {
                        responseCode: 201,
                        // data: { validatedData, purchaseData },
                        data: theResponse,
                    }
                    // return res.status(500).json(response) //(validateResponse)
                }
            )
            .catch(err222 => {
                console.error(
                    'WTF!)!)!)!)!)!)!)!)!)!!))!!!!!!!!?!?!?!?!?',
                    err222
                )
            })
    } else {
        response = {
            isValid: false,
            message: 'Receipt Not Valid',
        }
        // const {transactionId,originalTransactionId, expirationDate } = purchaseData
        // TODO: Purchases
        const receiptDetails: SaveEventDirective = {
            // uid: string
            platform: PlatformType.IOS,
            transactionId: '',
            originalTransactionId: '',
            expirationDate: null,
            startDate: null,
            linkedPurchaseToken: '',
            notificationType:
                IOSNotificationType.ValidatingInitialBuy,
            iosReceipt: undefined,
        }
        return await persistAthleteReceipt(
            theAthleteUid,
            // response
            receiptDetails
        ).then(
            async ({
                data,
                theOriginalTransactionId,
                athleteRef,
            }) => {
                await updateAtleteSubscriptionExpiration(
                    data,
                    theOriginalTransactionId,
                    athleteRef,
                    'serverValidateReceipt'
                )
                response = {
                    // ...thePayload,
                    isValid: false,
                    message: 'Receipt Not Valid',
                }
                console.log(response)
                return {
                    message: 'validateReceipt',
                    responseCode: 501,
                    data:
                    {
                        ...response,
                        expirationDate: response.expirationDate.toJSON(),
                    },
                    // error: undefined,
                    method: {
                        name: 'validateReceipt',
                        line: 42,
                    },
                }
            }
        )
    }

}

const onError = async (err: {
    message: string
}) => {
    // failed to validate the receipt...
    // Failed to validate
    console.error(err)

    // TODO: Purchases
    const receiptDetails: SaveEventDirective = {
        // uid: string
        platform: PlatformType.IOS,
        transactionId: '',
        originalTransactionId: '',
        expirationDate: null,
        startDate: null,
        linkedPurchaseToken: '',
        notificationType:
            IOSNotificationType.ValidatingInitialBuy,
        iosReceipt: undefined,
    }
    console.log('persisting receipt')
    await persistAthleteReceipt(
        theAthleteUid,
        receiptDetails
    ).then(
        async ({
            data,
            theOriginalTransactionId,
            athleteRef,
        }) => {
            console.log(
                'persisting athlete subcriptions expiry and adding transactionId to array list'
            )
            await updateAtleteSubscriptionExpiration(
                data,
                theOriginalTransactionId,
                athleteRef,
                'serverValidateReceipt'
            )
            const message = err.message

            console.log('validateReceipt - req.body', theBody)
            const response = {
                ...thePayload,
                isValid: false,
                message,
            }
            console.log(response)
            // TODO: Check this
            // const data = validateResponse.data();
            // validateResponse.send(data)
            return {
                message: 'validateReceipt',
                responseCode: 500,
                data: response,
                // error: undefined,
                method: {
                    name: 'validateReceipt',
                    line: 42,
                },
            }
            // return res.status(500).json(response) //(validateResponse)
        }
    )
}

export const validateIOS = async (transactionReceipt: string, body: TransactionReceiptValidationResponse, athleteUid: string,
    payload: {
        productIdentifier: string;
        transactionDate: Date;
        transactionIdentifier: string;
    }) => {


    theAthleteUid = athleteUid
    theBody = body
    thePayload = payload

    const theResult = await iap.setup()
        .then(() => {
            // iap.validate(...) automatically detects what type of receipt you are trying to validate

            const theReceipt = transactionReceipt
            return iap.validate(theReceipt).then(onSuccess).catch(onError);
        })
        .catch((error) => {
            // error.
            console.error('something went wrong...', error)
            return {
                message: 'validateReceipt',
                responseCode: 510,
                // data: response,
                error,
                method: {
                    name: 'validateReceipt',
                    line: 42,
                },
            }
        });



    return theResult
};
