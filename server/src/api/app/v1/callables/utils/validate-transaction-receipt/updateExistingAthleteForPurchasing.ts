import { isArray } from "lodash";

import { AthleteSubscription } from "../../../../../../models/athlete/interfaces/athlete-subscription";
import { SubscriptionType, SubscriptionStatus, SubscriptionPaymentType } from "../../../../../../models/enums/enums.model";
import { Athlete } from "../../../../../../models/athlete/athlete.model";
import { updateAthleteForPurchasing } from "./updateAthleteForPurchasing";
import { createNewIpScoreTracking } from "../maintenance/resetIPTracking";
import { removeUndefinedProps } from "../../../../../../db/ipscore/services/save-aggregates";

export const updateExistingAthleteForPurchasing = (
    athlete: Athlete,
    type: SubscriptionType,
    status: SubscriptionStatus,
    paymentType: SubscriptionPaymentType,
    commencementDate: Date,
    expirationDate: Date,

    appIdentifier: string,
    productIdentifier: string,
    savedFrom: string
): Athlete => {

    // console.log(`Updating athlete  ${athlete.uid} for purchasing.`);
    // // const days = getDayDifference(athlete.metadata.creationTimestamp);
    // // console.log(`Days since onboarding date ${days}.`);
    debugger;
    const sub = {
        appIdentifier,
        type,
        status,
        paymentType,
        commencementDate: commencementDate as any, // TODO
        expirationDate: expirationDate as any, // TODO
        productIdentifier: productIdentifier || '',
        savedFrom
    } as AthleteSubscription;

    if (athlete.profile.subscription.appIdentifier === undefined || (athlete.profile.subscription.appIdentifier || '').toLowerCase() === sub.appIdentifier.toLowerCase()) {
        athlete.profile.subscription = { ...sub, appIdentifier: athlete.profile.subscription.appIdentifier };
    }

    if (isArray(athlete.profile.appSubscriptions)) {

        const subIndex = athlete.profile.appSubscriptions.findIndex((s: AthleteSubscription) =>
            s.appIdentifier.toLowerCase() === sub.appIdentifier.toLowerCase());
            
        subIndex >= 0
            ? athlete.profile.appSubscriptions[subIndex] = sub
            : athlete.profile.appSubscriptions.push(sub);

    }
    else {
        athlete.profile.appSubscriptions = [sub];
    }

    console.log(`New athlete subscription: ${JSON.stringify(athlete.profile.subscription)}.  Athlete uid: ${athlete.uid}`);

    if (!athlete.currentIpScoreTracking) {
        athlete.currentIpScoreTracking = createNewIpScoreTracking();
    }
    if (athlete.currentIpScoreTracking.directive) {
        athlete.currentIpScoreTracking.directive.execute = false;
    }
    else {
        athlete.currentIpScoreTracking.directive = {
            execute: false,
            includeGroupAggregation: false,
            paths: []
        };
    }
    removeUndefinedProps(athlete)

    return athlete
};
