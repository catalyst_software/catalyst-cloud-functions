export const extractReceipt = (receipt: any) => {
    const { unique_vendor_identifier, //: '0D439F5E-6D71-4BC8-8297-CB048C7092FE',
        bvrs, //: '1.9',
        expires_date_formatted, //: '2019-02-19 17:53:45 Etc/GMT',
        is_in_intro_offer_period: isInIntroOfferPeriod, //: 'false',
        purchase_date_ms, //: '1550598525000',
        expires_date_formatted_pst, //:        '2019-02-19 09:53:45 America/Los_Angeles',
        is_trial_period: isTrialPeriod, //: 'false',
        item_id, //: '1453012404',
        unique_identifier: uniqueIdentifier, //: '90518714aea0d8d20c8eba0724ffaaf0b345990b',
        original_transaction_id, //: '1000000503436015',
        expires_date, //: '1550598825000',
        transaction_id, //: '1000000503807136',
        web_order_line_item_id: webOrderLineItemId, //: '1000000042817425',
        version_external_identifier, //: '0',
        bid, //: 'com.inspiresportonline.dev',
        product_id, //: 'com.inspiresportonline.dev.subscriptions',
        purchase_date, //: '2019-02-19 17:48:45 Etc/GMT',
        original_purchase_date, //: '2019-02-18 16:04:12 Etc/GMT',
        original_purchase_date_pst, purchase_date_pst, //: '2019-02-19 09:48:45 America/Los_Angeles',
        original_purchase_date_ms, //: '1550505852000',
        quantity, ...receiptRest } = receipt;
    const receiptParsed = {
        isInIntroOfferPeriod,
        isTrialPeriod,
        itemId: item_id,
        uniqueIdentifier,
        webOrderLineItemId,
        ...receiptRest,
    };
    return receiptParsed;
};
