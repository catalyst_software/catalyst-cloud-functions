import { db } from "../../../../../shared/init/initialise-firebase";
import { FirestoreCollection } from "../../../../../db/biometricEntries/enums/firestore-collections";
import { LogInfo } from "../../../../../shared/logger/logger";
import { getDocumentFromSnapshot } from "../../../../../analytics/triggers/utils/get-document-from-snapshot";
import { Group } from "../../../../../models/group/interfaces/group";
import { AthleteSubscription } from "../../../../../models/athlete/interfaces/athlete-subscription";
import { SubscriptionType } from "../../../../../models/enums/enums.model";
import { getUniqueListBy } from "./getUniqueListBy";
import { AthleteIndicator } from "../../../../../db/biometricEntries/well/interfaces/athlete-indicator";

export interface AthleteUpdateSubscriptionRequest {
  athleteUid: string;
  groupIds: Array<string>;
  subscription: AthleteSubscription;
  includeAthlete: boolean;
  includeGroups: boolean;
}

export const updateAthleteSubscription = async (
  athleteUid: string, groupIds: Array<string>,
  subscription: AthleteSubscription,
  includeAthlete: boolean,
  includeGroups: boolean):
  Promise<Array<any>> => {

  // const payload = {
  //   "athleteUid": "hMSy9wDZdJc5NgVftp5esKCAuCl2",
  //   "includeAthlete": false,
  //   "includeGroups": true,
  //   "groupIds": ["5memaFk3DSSMF71qty2R-G2"],
  //   "subscription": { "type": 1, "status": 1 }
  // }

  console.log('======>>>>> athleteUID', athleteUid)
  console.log('======>>>>> groupIds', groupIds)

  const theRes: Array<{
    message: string;
    athleteUID: string;
    groupUID?: string;

  }>
    = []
  const groupDocsRefs = new Array<FirebaseFirestore.DocumentReference>();

  if (includeAthlete) {
    const athleteRef = db
      .collection(FirestoreCollection.Athletes)
      .doc(athleteUid);
    console.log(`Setting Subscription on for athleteUID ${athleteUid}`, subscription)

    // const athleteResult =
    await athleteRef.update({ 'profile.subscription': subscription })

    const payloads = {
      message: `updateAthleteSubscription: Athlete UPDATED`,
      athlete: undefined,
      athleteUID: athleteUid,
    }

    theRes.push(payloads)
  }

  if (includeGroups) {
    // Set up all the doc references for the the query to grab the groups by uid
    groupIds.forEach((groupUID) => {
      const ref = db
        .collection(FirestoreCollection.Groups)
        .doc(groupUID);
      groupDocsRefs.push(ref);
    })

    const theGroupsResults = await db.getAll(...groupDocsRefs)
      .then(async (groupDocs: FirebaseFirestore.DocumentSnapshot[]) => {

        LogInfo(`groups.length => ${groupDocs.length}`);

        const updates = await groupDocs
          .map((groupsDocSnap) => {
            if (!groupsDocSnap.exists) {
              console.warn('Group Document Does not Exist in FireStore', groupsDocSnap.id)
            }
            return groupsDocSnap
          })
          .filter(snap => snap.exists)
          .map(async (groupSnap: FirebaseFirestore.DocumentSnapshot) => {

            const group = getDocumentFromSnapshot(groupSnap) as Group;


            const allGroupAthletes = group.athletes
            const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

            if (allGroupAthletes.length !== groupAthletes.length) {
              console.warn('dup athletes removed', {
                groupAthletesWithDups: allGroupAthletes.map((a) => {
                  return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                }),
                groupAthletesCleaned: groupAthletes.map((a) => {
                  return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                })
              })
            }

            const theAthleteIndicator = groupAthletes.find((athlete) => athlete.uid === athleteUid)
            const theAthleteIndicatorIndex = groupAthletes.findIndex((athlete) => athlete.uid === athleteUid)

            const thePayload = {
              message: '',
              athlete: undefined,
              athleteUID: athleteUid,
              groupUID: group.uid
            }

            if (theAthleteIndicatorIndex >= 0) {
              // Update The Indicator and Group

              console.log(`Setting Subscription on ${group.groupName}: ${group.uid}`, SubscriptionType[theAthleteIndicator.subscriptionType])

              theAthleteIndicator.subscriptionType = subscription.type

              console.warn('updated Subscription Type on Group Indicator', groupAthletes.find((athlete) => athlete.uid === athleteUid).subscriptionType)

              groupAthletes[theAthleteIndicatorIndex] = theAthleteIndicator

              await groupSnap.ref.update({ athletes: groupAthletes })

              thePayload.message = `updateAthleteSubscription: Athlete Inidicator UPDATED in group ${group.groupName}: ${group.uid}`

              const { uid, recentEntries, firebaseMessagingIds, ...athleteIndicatorRest } = theAthleteIndicator
              thePayload.athlete = athleteIndicatorRest

              console.log(thePayload)
              return thePayload

            } else {

              thePayload.message = 'Athlete Inidicator NOT FOUND in group'
              console.error(thePayload)
              return thePayload
            }

          });
        // merge all grouped promises (per group)
        const athleteGroups = [].concat(...updates) as [
          {
            message: string,
            athleteUID: string,
            groupUID: string
          }]
        const res = await Promise.all(athleteGroups)
        return res
      })
      .then(async (updateStatus: Array<{
        message: string;
        athleteUID: string;
        groupUID: string;
      }>) => {
        // Get The Coach Documents form the Athletes Store

        console.log(updateStatus)

        return updateStatus;
      });

    theRes.concat(theGroupsResults)
  }

  return theRes
};
