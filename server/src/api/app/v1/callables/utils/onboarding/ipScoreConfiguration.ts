export const ipScoreConfiguration = {
    programs: {
        include: true,
        includeVideoConsumption: true,
        includeWorkSheetConsumption: true,
        includeSurveyConsumption: true,
    },
    quantitativeWellness: {
        include: true,
        training: {
            components: {
                include: true,
                allowedSkipDays: [5, 6],
                expectedDailyEntries: 1,
            },
        },
        body: {
            fatigue: {
                include: true,
                allowedSkipDays: [],
                expectedDailyEntries: 1,
            },
            pain: {
                include: true,
                allowedSkipDays: [],
                expectedDailyEntries: 1,
            },
            wearables: {
                include: true,
                allowedSkipDays: [],
                expectedDailyEntries: 1,
            },
        },
        brain: {
            sleep: {
                include: true,
                allowedSkipDays: [],
                expectedDailyEntries: 1,
            },
            mood: {
                include: true,
                allowedSkipDays: [],
                expectedDailyEntries: 4,
            },
        },
        food: {
            components: {
                include: true,
                allowedSkipDays: [],
                expectedDailyEntries: 4,
            },
        },
    },
    qualitativeWellness: {
        include: false,
    },
};
