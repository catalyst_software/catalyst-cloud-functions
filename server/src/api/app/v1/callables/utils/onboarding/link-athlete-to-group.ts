import { firestore } from 'firebase-admin';

import { OrganizationInterface } from '../../../../../../models/organization.model';
import { Group } from '../../../../../../models/group/group.model';
import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { createAthleteIndicator } from './create-athlete-indicator';
import { getGroupDocRef } from '../../../../../../db/refs';
import { getDocumentFromSnapshot } from '../../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { IpScoreAggregateComponentInterface } from '../../../../../../db/ipscore/models/interfaces/ipscore-component-aggregate';
import { GroupNotificationConfig } from '../../../controllers/athletes';

import { GroupIndicator } from '../../../../../../db/biometricEntries/well/interfaces/group-indicator';
import { OnboardingValidationResponse } from '../../../interfaces/onboarding-validation-response';
import moment from 'moment';
import { FirestoreCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';

export const getGroupIndicator = (group: Group) => {
    return {
        groupId: group.groupId,
        groupName: group.groupName,
        organizationId: group.organizationId,
        organizationName: group.organizationName,
        logoUrl: group.logoUrl,
        creationTimestamp: group.creationTimestamp.toDate().toJSON()
    };
}

export const linkAthleteToGroup = async (
    org: OrganizationInterface,
    groupSnap: firestore.DocumentSnapshot,
    theGroup: Group,
    groupIdentifier: string,
    athleteIndicator: AthleteIndicator,
    code: string): Promise<OnboardingValidationResponse> => {
    // 5) create and link new group to new org (groupIdentifier = 1)
    // 6) add athleteIndicator to new group
    // 7) set isCoach to true in response



    const response = await createAthleteIndicator(athleteIndicator, org);

    if (groupSnap) {

        if (groupSnap.exists) {

            const existingGroup = groupSnap.data() as Group;
            existingGroup.athleteIndex[`${athleteIndicator.uid}`] = true;
            await groupSnap.ref
                    .set(existingGroup, {merge: true})
                    .then(() => {
                        console.log(`Athlete index for group ${existingGroup.groupId} successfully updated with athlete uid ${athleteIndicator.uid}`);
                    });

            return await groupSnap.ref
                .collection(FirestoreCollection.Athletes)
                .doc(response.athleteIndicator.uid)
                // .update({ athletes: firestore.FieldValue.arrayUnion(response.athleteIndicator) })
                .set(response.athleteIndicator)
                .then(() => {

                    const group = theGroup || getDocumentFromSnapshot(groupSnap) as Group;

                    const groupIndicator: GroupIndicator = {
                        groupId: group.groupId,
                        groupName: group.groupName,
                        organizationId: org.uid,
                        organizationName: org.name,
                        logoUrl: group.logoUrl,
                        creationTimestamp: group.creationTimestamp
                    };

                    return {
                        message: 'Group Updated',
                        responseCode: 200,
                        data: {
                            ...response,
                            groups: [groupIndicator]
                        },
                        method: {
                            name: 'linkToGroup',
                            line: 254,
                        }
                    } as OnboardingValidationResponse
                })
                .catch(() => {
                    return {
                        message: 'Unable to update Group',
                        responseCode: 202,
                        data: {
                            ...response,
                        },
                        method: {
                            name: 'linkToGroup',
                            line: 254,
                        }
                    } as OnboardingValidationResponse
                });
        }
        else {
            if (org.onboardingConfiguration.autoCreateGroups) {
                const group = {
                    uid: `${org.uid}-${groupIdentifier}`,
                    groupId: `${org.uid}-${groupIdentifier}`,
                    groupName: `${code}-${groupIdentifier}`,
                    organizationId: org.uid,
                    organizationName: org.name,
                    groupIdentifier,
                    logoUrl: '',
                    creationTimestamp: firestore.Timestamp.now(),
                    athleteIndex: {
                    }
                };
                group.athleteIndex[`${athleteIndicator.uid}`] = true;
    
                const groupDocRef = await getGroupDocRef(group.groupId)
                    .set(group)
                    .then((doc) => {
                        console.log(`Group ${group.groupId} successfully created`);
                        return doc;
                    })
                    .catch((err) => {
                        console.error(`An error occurred when creating group ${group.groupId}.  Error: ${err}`);
                    });

                return await getGroupDocRef(group.groupId)
                    .collection(FirestoreCollection.Athletes)
                    .doc(athleteIndicator.uid)
                    .set(athleteIndicator)
                    .then((doc) => {

                        const groupIndicator = getGroupIndicator(group);

                        // TODO: Check creationTimestamp on GroupIndicator
                        return {
                            message: 'Group Created',
                            responseCode: 200,
                            data: {
                                ...response,
                                groups: [{ ...groupIndicator, creationTimestamp: firestore.Timestamp.fromDate(moment(groupIndicator.creationTimestamp).toDate()) }],
                            },
                            method: {
                                name: 'linkToGroup',
                                line: 254,
                            }
                        } as OnboardingValidationResponse;
                    })
            }
            else {
                return {
                    message: 'Group Not Found',
                    responseCode: 404,
                    data: {
                        ...response,
                    },
                    method: {
                        name: 'linkToGroup',
                        line: 254,
                    }
                } as OnboardingValidationResponse
            }
        }

    }
    else {
        const groupIdentity = groupIdentifier ? groupIdentifier : 'G1';
        const group = {
            uid: `${org.uid}-${groupIdentity}`,
            groupId: `${org.uid}-${groupIdentity}`,
            groupName: `${athleteIndicator.firstName}'s Team`,
            organizationId: org.uid,
            organizationName: org.name,
            groupIdentifier: groupIdentity,
            logoUrl: '',
            creationTimestamp: firestore.Timestamp.now(),
            athleteIndex: {
            }
        };
        group.athleteIndex[`${athleteIndicator.uid}`] = true;
    
        return await getGroupDocRef(group.groupId)
            .set(group)
            .then(() => {
                const groupIndicator = getGroupIndicator(group);
                // TODO: Check creationTimestamp on GroupIndicator
                return {
                    message: 'Group Created',
                    responseCode: 200,
                    data: {
                        ...response,
                        groups: [{ ...groupIndicator, creationTimestamp: firestore.Timestamp.fromDate(moment(groupIndicator.creationTimestamp).toDate()) }],
                    },
                    method: {
                        name: 'linkToGroup',
                        line: 254,
                    }
                } as OnboardingValidationResponse
            })
    }
};
