import { Group } from './../../../../../../models/group/group.model';
import { firestore } from 'firebase-admin';
export function getGroup(groupSnap: firestore.DocumentSnapshot) {
    return {
        uid: groupSnap.id,
        ...groupSnap.data() as Group
    };
}
