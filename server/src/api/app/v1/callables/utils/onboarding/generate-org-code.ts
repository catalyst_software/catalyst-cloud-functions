import { getOrgCollectionRef } from '../../../../../../db/refs';

export const generateOrgCode = () => {
    
    const code = Math.random()
        .toString(36)
        .substring(6, 12)
        .toUpperCase();
        
    return getOrgCollectionRef()
        .where('organizationCode', '==', code)
        .get()
        .then(snapshot => {
            if (snapshot.empty) {
                return code;
            }
            else {
                return undefined;
            }
        });
};
