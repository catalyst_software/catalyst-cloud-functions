import { Organization } from '../../../../../../models/organization.model';
import { getGroupDocRef } from './../../../../../../db/refs/index';
export async function getGroupSnap(org: Organization, groupIdentifier: any) {
    return await getGroupDocRef(`${org.uid}-${groupIdentifier}`)
        .get()

}
