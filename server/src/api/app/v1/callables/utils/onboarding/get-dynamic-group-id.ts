
import * as functions from 'firebase-functions'
import { getOrgCollectionRef } from '../../../../../../db/refs';
import { getDocumentFromSnapshot } from '../../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { Organization } from '../../../../../../models/organization.model';
import { OnboardingValidationResponse } from '../../../interfaces/onboarding-validation-response';
import { Config } from './../../../../../../shared/init/config/config';

export const chunkBy = (n) => number => {
    const chunks = new Array(Math.ceil(number / n)).fill(n);

    console.log('CHUNKS  now = ', chunks);
    return chunks;
};


export const getDynamicGroupId = functions.https._onCallWithOptions(
    async (data, context) => {
        try {


            const { organizationCode } = data

            console.log('theCode ->', organizationCode);

            let orgCode = organizationCode;

            // TODO: Not using UTCOffset?!?

            const responsePromise = await getOrgCollectionRef()
                .where('organizationCode', '==', orgCode)
                .get()
                .then(async querySnapshot => {

                    if (querySnapshot.size === 1) {
                        const orgDocSnap = querySnapshot.docs.pop();
                        const org = getDocumentFromSnapshot(orgDocSnap) as Organization

                        const chunkByBucketSize = chunkBy(org.config ? org.config.bucketSize || 50 : 50);

                        const cunks = chunkByBucketSize(org.numberUsersOnboarded || 0);


                        console.log({ cunks })

                        const chunkSize = cunks.length + 1


                        // await orgDocSnap.ref.update({
                        //     numberUsersOnboarded: (org.numberUsersOnboarded || 0) + 1

                        // })


                        return {
                            message: '',
                            responseCode: 200,
                            data: `${org.config ? org.config.groupPrefix || 'GROUP' : 'GROUP'}${chunkSize}`,
                            error: undefined
                        }
                    }
                    else {


                        return undefined
                    }
                })
                .catch(exception => {
                    console.log('exception!!!! => ', exception);
                    const theExceptionResponse = {
                        message: 'An error exception in getDynamicGroupId: err ->' +
                            { ...exception },
                        responseCode: 500,
                        data: undefined,
                        error: {
                            message: 'An exception occurred in getDynamicGroupId/:code: err ->' +
                                { ...exception },
                            exception,
                        }
                    } as OnboardingValidationResponse

                    return theExceptionResponse
                });

            return responsePromise

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)
