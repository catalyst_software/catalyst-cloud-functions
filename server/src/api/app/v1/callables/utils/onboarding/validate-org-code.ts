import { firestore } from 'firebase-admin';
import moment from 'moment';

import { linkAthleteToGroup } from "./link-athlete-to-group";
import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { Organization, WellnessGeoCaptureConfigurationInterface, OrganizationConfigurationInterface } from '../../../../../../models/organization.model';
import { OnboardingValidationResponseData, OnboardingValidationResponse, ValidationCodeType } from '../../../interfaces/onboarding-validation-response';
import { getOrgCollectionRef, getCollectionRef } from './../../../../../../db/refs/index';
import { SubscriptionType } from '../../../../../../models/enums/enums.model';
import { getDocumentFromSnapshot } from '../../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { db } from '../../../../../../shared/init/initialise-firebase';
import { getGroupSnap } from './getGroupSnap';
import { getGroup } from './getGroup';
import { WhiteLabelThemeType, WhiteLabelTheme } from '../../../interfaces/WhiteLabelTheme';
import { FirestoreCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';

/**
 * Validates Onboarding Code
 * Create Org And Group if code is iNSPIRE
 * Joins athlete to Org Group, Updating the Group indicator accordingly
 */

export const validateOrgCode = async (onboardingCode: string, athleteIndicator: AthleteIndicator, utcOffset: number) => {
    let code = onboardingCode

    console.log('theCode ->', code);

    // code = await validateAlias(code);

    let orgCode = code;
    let groupIdentifier = 'G1';

    const split = code.split('-');

    if (split.length > 1) {
        orgCode = split[0];
        groupIdentifier = split[1];
    }

    // TODO: Not using UTCOffset?!?

    // if (orgCode.toUpperCase() === 'GIRLWARRIOR') {
    //     groupIdentifier = ''
    // }

    const responsePromise = await getOrgCollectionRef()
        .where('organizationCode', '==', orgCode)
        .get()
        .then(async querySnapshot => {

            if (querySnapshot.size === 1) {
                const orgDocSnap = querySnapshot.docs.pop();
                const org = getDocumentFromSnapshot(orgDocSnap) as Organization

                const publishedMediaBundle = !!org.config && !!org.config.publishedMediaBundles && org.config.publishedMediaBundles.find((mb) => mb.code === onboardingCode) || undefined


                let isOrgRep
                let prePaidGroup
                let isGroupPrePaid = false
                let isCoach
                let subscriptionLengthInMonths

                const { isPrePaid: isOrgPrePaid, subscriptionType, prePaidNumberOfMonths: orgPrePaidNumberOfMonths } = org.onboardingConfiguration;
                let provisioningKey

                if (publishedMediaBundle) {
                    provisioningKey = publishedMediaBundle.provisioningKey

                    const response: any = { //} OnboardingValidationResponseData = {
                        message: 'Valid Media Key',
                        responseCode: 200,
                        data: {
                            validationCodeType: !!provisioningKey ? ValidationCodeType.MediaBundle : ValidationCodeType.Onboarding,
                            contentProvisionKey: provisioningKey
                        },

                    };


                    return response
                } else {

                    let isCoachEmail = []
                    if (!org.coaches) {
                        org.coaches = isCoachEmail = [];
                    }
                    else {
                        isCoachEmail = org.coaches.filter((email: string) => email === athleteIndicator.email);
                    }
                    isCoach = isCoachEmail.length ? true : false;

                    let isOrgRepEmail;
                    if (!org.orgReps) {
                        org.orgReps = isOrgRepEmail = [];
                    }
                    else {
                        isOrgRepEmail = org.orgReps.filter((email: string) => email === athleteIndicator.email);
                    }
                    isOrgRep = isOrgRepEmail.length ? true : false;


                    prePaidGroup =
                        org.onboardingConfiguration &&
                        org.onboardingConfiguration.paidGroups &&
                        org.onboardingConfiguration.paidGroups.find((paidGroup) =>
                            paidGroup.groupName === `${org.organizationCode}-${groupIdentifier}`)


                    const groupSnap = await getGroupSnap(org, groupIdentifier)

                    const group = getGroup(groupSnap)
                    console.log('the group ->', group);
                    subscriptionLengthInMonths =
                        group && group.onboardingConfiguration && group.onboardingConfiguration.prePaidNumberOfMonths ||
                        prePaidGroup && prePaidGroup.prePaidNumberOfMonths ||
                        orgPrePaidNumberOfMonths ||
                        0


                    isGroupPrePaid = prePaidGroup && prePaidGroup.isPrePaid || group && group.onboardingConfiguration && group.onboardingConfiguration.isPrePaid || isGroupPrePaid;


                    console.log('isOrgPrePaid', isOrgPrePaid)
                    console.log('isGroupPrePaid', isGroupPrePaid)


                    if (!isOrgPrePaid || isGroupPrePaid || (isOrgPrePaid && isCoach || org.licensesConsumed < org.licensesPurchased)) {

                        const orgId = org.uid

                        const forcePrepaid = isGroupPrePaid || (isOrgPrePaid || isCoach) || false

                        const nowMoment = moment();
                        const subscriptionEndDate = nowMoment.clone().add(subscriptionLengthInMonths, 'months');

                        console.log('creating response');
                        console.log('the org code', orgCode)


                        const data: OnboardingValidationResponseData = {
                            athlete: undefined,
                            organizationId: orgId,
                            orgTools: org.onboardingConfiguration.tools,
                            organizations: [{ organizationId: org.uid, organizationName: org.name, organizationCode: org.organizationCode, active: true, joinDate: firestore.Timestamp.now() }],
                            isCoach,
                            isOrgRep,
                            ipScoreConfiguration: org.ipScoreConfiguration,
                            includeGroupAggregation: !isCoach,
                            groups: [],
                            subscription: {
                                isPrePaid: forcePrepaid
                            },
                            validationCodeType: !!provisioningKey ? ValidationCodeType.MediaBundle : ValidationCodeType.Onboarding,
                            contentProvisionKey: provisioningKey
                        };

                        if (forcePrepaid) {
                            console.warn('forcePrepaid =>', forcePrepaid)
                            const groupSubType = prePaidGroup ? prePaidGroup.subscriptionType : undefined

                            data.subscription.subscriptionType = subscriptionType || groupSubType || SubscriptionType.Basic;
                            data.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
                            data.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());
                        }
                        // TODO: isPrePaid || forcePrepaid??

                        let updateOrg = false
                        let licensesConsumed = org.licensesConsumed
                        if ((isOrgPrePaid || isGroupPrePaid) && !isCoach) {

                            licensesConsumed = org.licensesConsumed + 1
                            updateOrg = true
                        }

                        let numberUsersOnboarded = (org.numberUsersOnboarded || 0)
                        if (!isOrgPrePaid && !isGroupPrePaid) {
                            numberUsersOnboarded = (org.numberUsersOnboarded || 0) + 1
                            updateOrg = true

                        }


                        if (!org.config) {
                            org.config = {
                                wellnessGeoCapture: {
                                    mood: false,
                                    sleep: false,
                                    fatigue: false,
                                    pain: false,
                                    food: false,
                                    training: false,
                                    period: false,
                                    sick: true
                                } as WellnessGeoCaptureConfigurationInterface,
                                sickSymptomTracking: [],
                                validConsumedOnboardingCodes: ['*'],
                                validConsumedMediaBundleCodes: [],
                                validConsumedNotificationStreamCodes: [],
                                publishedMediaBundles: [],
                                bucketSize: 50,
                                groupPrefix: 'TEAM',
                                storageRoot: 'CATALYST',
                                showLeaderboard: true,
                                showCommunityHub: true,
                                forceAutocompleteValueRefresh: false,
                                validThemes: [],
                                allowInvalidThemeChanges: false,
                                termsAndConditionsUrl: 'https://catalyst.com/app-terms-and-conditions/',
                                privacyStatementUrl: 'https://catalyst.com/privacy-policy/',
                                appChannelId: '',
                                restrictVideoFastForwardToFirstCompletion: true,
                                videoSkipTimeInSeconds: 5,
                                processProgramCardRenderTasks: false,
                                processNotificationCardRenderTasks: true
                            } as OrganizationConfigurationInterface;
                            updateOrg = true
                        }

                        const theStorageRoot = org.config.storageRoot || 'CATALYST'

                        data.storageRoot = theStorageRoot

                        !!updateOrg && await orgDocSnap.ref
                            .update({
                                ...org,
                                config: {
                                    ...org.config,
                                    storageRoot: theStorageRoot
                                },
                                uid: orgDocSnap.id,
                                licensesConsumed,
                                numberUsersOnboarded
                            })

                        let themeUid: WhiteLabelThemeType = org.theme || 'catalyst'

                        const theTheme = await getCollectionRef(FirestoreCollection.WhiteLabelThemes).doc(themeUid)
                            .get().then((docSnap) => {
                                if (docSnap.exists) {
                                    return getDocumentFromSnapshot(docSnap);
                                } else {
                                    themeUid = 'catalyst'

                                    return getCollectionRef(FirestoreCollection.WhiteLabelThemes).doc(themeUid)
                                        .get().then((defaultDocSnap) => {
                                            return getDocumentFromSnapshot(defaultDocSnap);
                                        })
                                }

                            }) as WhiteLabelTheme

                        data.theme = theTheme

                        console.log('groupIdentifier', groupIdentifier);

                        const linkToGroupResponse = await linkAthleteToGroup({ ...org, uid: orgDocSnap.id }, groupSnap, group,
                            groupIdentifier, athleteIndicator, orgCode)

                        const theResp: OnboardingValidationResponse = {
                            ...linkToGroupResponse,
                            data: {
                                ...data,
                                ...linkToGroupResponse.data,
                                subscription: {
                                    ...data.subscription
                                }
                            },
                            message: `Valid Key, ${linkToGroupResponse.message}`
                        }
                        console.warn('theResp', theResp)

                        return theResp
                    }
                    else {
                        const theConsumedResponse = {
                            message: 'All licences consumed',
                            responseCode: 400,
                            data: undefined,
                            method: {
                                name: 'validateOrganizationCode',
                                line: 70,
                            }
                        } as OnboardingValidationResponse

                        return theConsumedResponse
                    }

                }




            }
            else {
                const theInvalidKeyResponse = {
                    message: 'Invalid Key',
                    responseCode: 404,
                    data: undefined,
                    error: { message: 'Invalid Key' },
                    method: {
                        name: 'validateOrganizationCode',
                        line: 82,
                    }
                } as OnboardingValidationResponse

                return theInvalidKeyResponse
            }
        })
        .catch(exception => {
            console.log('exception!!!! => ', exception);
            const theExceptionResponse = {
                message: 'An error exception in /organizations/code/:code: err ->' +
                    { ...exception },
                responseCode: 500,
                data: undefined,
                error: {
                    message: 'An exception occurred in /organizations/code/:code: err ->' +
                        { ...exception },
                    exception,
                }
            } as OnboardingValidationResponse

            return theExceptionResponse
        });

    return responsePromise
};

async function validateAlias(onboardingCode: string) {

    let theCode = onboardingCode.toUpperCase()

    const aliases = await db.collection('systemAliases').doc('aliases').get().then((docSnap) => {
        return {
            uid: docSnap.id,
            ...docSnap.data() as { orgCodes: Array<{ alias: string, code: string }> }
        }
    })

    const codeAlias = aliases.orgCodes.find((aliasGroup) => aliasGroup.alias.toUpperCase() === theCode)

    if (codeAlias) {
        theCode = codeAlias.code.toUpperCase()
    }


    return theCode;
}

