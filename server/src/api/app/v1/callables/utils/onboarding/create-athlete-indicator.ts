
import { OrganizationInterface } from '../../../../../../models/organization.model';
import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { OnboardingValidationResponseData } from '../../../interfaces/onboarding-validation-response';
import { removeUndefinedProps } from '../../../controllers/utils/remove-undefined-props';
import { testForLetter } from '../../../controllers/athletes';

export const createAthleteIndicator = async (athleteIndicator: AthleteIndicator, org: OrganizationInterface) => {
    console.log('athleteIndicator BEFORE', athleteIndicator);
    let isCoachEmail;
    if (!org.coaches) {
        org.coaches = isCoachEmail = [];
    }
    else {
        isCoachEmail = org.coaches.filter((email: string) => email === athleteIndicator.email);
    }
    const isCoach = isCoachEmail.length ? true : false;

    // TODO: Needed on the Indicator?
    // athleteIndicator.organizationName = org.name;
    athleteIndicator.organizationId = org.uid;


    athleteIndicator.isCoach = isCoach;
    athleteIndicator.includeGroupAggregation = !isCoach;
    
    removeUndefinedProps(athleteIndicator)

    if (org.organizationCode.toUpperCase() === 'NETBALLQ') {
        athleteIndicator.bio = athleteIndicator.bio !== undefined && athleteIndicator.bio.trim() !== '' ? athleteIndicator.bio : 'Athlete'
        try {

            const lastName = athleteIndicator.lastName.charAt(0).toUpperCase()
            if (testForLetter(lastName)) {
                athleteIndicator.lastName = lastName || ''
            } else {
                console.error('Last name not a string', athleteIndicator.lastName)
            }

        } catch (error) {
            console.error('Unable to set NEBALLQ athleteIndicator.lastName = athleteIndicator.lastName.charAt(0).toUpperCase()', error)
        }
    }

    console.log('athleteIndicator AFTER', athleteIndicator);

    let isOrgRepEmail;
    if (!org.orgReps) {
        org.orgReps = isOrgRepEmail = [];
    }
    else {
        isOrgRepEmail = org.orgReps.filter((email: string) => email === athleteIndicator.email);
    }
    const isOrgRep = isOrgRepEmail.length ? true : false;

    const response: OnboardingValidationResponseData = {
        athlete: undefined,
        organizationId: org.uid,
        orgTools: org.onboardingConfiguration.tools,
        organizations: [{
            organizationId: org.uid, organizationName: org.name, organizationCode: org.organizationCode, active: true,
            joinDate: athleteIndicator.creationTimestamp
        }],
        isCoach,
        isOrgRep,
        ipScoreConfiguration: org.ipScoreConfiguration,
        includeGroupAggregation: !isCoach,
        groups: [],
        subscription: {
            isPrePaid: false
        },
        athleteIndicator
    };


    return response;
};
