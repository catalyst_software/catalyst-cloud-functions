import { firestore } from 'firebase-admin';
import { getAthleteDocRef } from './../../../../../../db/refs/index';
import { Athlete } from '../../../../../../models/athlete/interfaces/athlete';
import { getUserFullName } from '../../../../../utils/getUserFullName';
/**
 * Validates Onboarding Code
 * Create Org And Group if code is iNSPIRE
 * Joins athlete to Org Group, Updating the Group indicator accordingly
 */
export const createAthleteProfile = async (athlete: Athlete, firebaseMessagingId: string): Promise<Athlete> => {
    try {
        const firebaseMessagingIds = athlete.firebaseMessagingIds || athlete.metadata.firebaseMessagingIds || [];
        if (firebaseMessagingIds.indexOf(firebaseMessagingId) < 0) {
            firebaseMessagingIds.unshift(firebaseMessagingId);
        }
        const theAthlete: Athlete = {
            ...athlete,
            profile: {
                ...athlete.profile,
                fullName: getUserFullName(athlete)
            },
            metadata: {
                ...athlete.metadata,
                firebaseMessagingIds
            }
        };
        return await getAthleteDocRef(athlete.uid)
            .set(theAthlete).then(() => {

                console.warn('!!!!!!!!!!!!!!!!!!!theAthlete==================>>>>>>>>>>>>>>>>>>', theAthlete)

                try {
                    const theAthleteResponse = {
                        ...theAthlete,
                        currentIpScoreTracking: {
                            ...theAthlete.currentIpScoreTracking,
                            dateTime: theAthlete.currentIpScoreTracking.dateTime.toDate().toJSON() as any
                        },
                        currentPrograms: theAthlete.currentPrograms.map((program) => {
                            return {
                                ...program,
                                activeDate: program.activeDate.toDate().toJSON() as any,
                                startDate: program.startDate.toDate().toJSON() as any,
                            };
                        }),
                        groups: theAthlete.groups.map((group) => {
                            return {
                                ...group,
                                creationTimestamp: group.creationTimestamp.toDate().toJSON() as any
                            };
                        }),
                        metadata: {
                            ...athlete.metadata,
                            creationTimestamp: athlete.metadata.creationTimestamp.toDate().toJSON() as any,
                            lastSignInTimestamp: athlete.metadata.lastSignInTimestamp.toDate().toJSON() as any
                        },
                        profile: {
                            ...theAthlete.profile,
                            subscription: {
                                ...theAthlete.profile.subscription,
                                commencementDate: theAthlete.profile.subscription.commencementDate
                                    ? theAthlete.profile.subscription.commencementDate.toDate().toJSON() as any
                                    : null,
                                expirationDate: theAthlete.profile.subscription.expirationDate
                                    ? theAthlete.profile.subscription.expirationDate.toDate().toJSON() as any
                                    : null
                            }
                        }
                    };
                    console.warn('!!!!!!!!!!!!!!!!!!!theAthleteResponse==================>>>>>>>>>>>>>>>>>>', theAthleteResponse)

                    return theAthleteResponse

                } catch (error) {
                    console.warn('!!!!!!!!!!!!!!!!!!!theAthleteResponse   error ==================>>>>>>>>>>>>>>>>>>', error)
                    return theAthlete
                }

            }).catch((err) => {
                console.log(err);
                throw err;
            });
    }
    catch (err) {
        throw err;
    }
};
