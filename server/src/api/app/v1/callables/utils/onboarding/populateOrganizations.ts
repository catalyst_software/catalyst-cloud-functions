// import { db } from './../../../../../shared/init/initialise-firebase';
// import { Organization } from '../../../../../models/organization.model';

// /**
//  * Populate db.collection(this.collection) with two dummy accounts
//  * Currently using it so that I can test the onCreate events! ;P
//  */
// export const populateOrganizations = (req: Request, res: Response): any => {
//     try {
//         const organizationRef = db.collection(this.collection);
//         const Organization1 = new Organization({
//             uid: '',
//             name: 'Organization1',
//             contactPerson: 'Blake Simmonds',
//             contactNumber: '07953 234 776',
//             organizationCode: '12345',
//             licensesConsumed: 10,
//             licensesPurchased: 20,
//             creationTimestamp: 
//         });
//         const Organization2 = new Organization({
//             name: 'Organization1',
//             contactPerson: 'Lisa Wells',
//             contactNumber: '07205 204 209',
//             organizationCode: '67890',
//             licensesConsumed: 10,
//             licensesPurchased: 10,
//         })
//         // TODO: quick fix to sort 'Promises must be handled appropriately' error when running
//         // 'firebase deploy --only functions'
//         // Just for testing purposes, will be deleted most likely in the end though!
//         return organizationRef.doc('Organization1').set(Organization1).then(() => {
//             return organizationRef.doc('Organization2').set(Organization2).then(() => {
//                 this.handleResponse({
//                     message: 'Organisazions saved. Organization1 included with this response',
//                     responseCode: 200,
//                     data: Organization1,
//                     method: {
//                         name: 'populateOrganizations',
//                         line: 76
//                     }
//                     // res
//                 });
//             });
//         });
//     }
//     catch (exception) {
//         this.handleResponse({
//             message: 'An unanticipated exception occurred in api/organizations/code/:code',
//             responseCode: 500,
//             data: undefined,
//             // res,
//             error: {
//                 message: 'An unanticipated exception occurred in api/organizations/code/:code',
//                 exception
//             },
//             method: {
//                 name: 'populateOrganizations',
//                 line: 76
//             }
//         });
//     }
// }