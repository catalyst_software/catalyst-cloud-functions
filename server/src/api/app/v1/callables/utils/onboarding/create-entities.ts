import { OrganizationInterface, Organization, WellnessGeoCaptureConfigurationInterface } from './../../../../../../models/organization.model';

import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { persistNewOrg } from "./create-new-org";
import { generateOrgCode } from './generate-org-code';
import { linkAthleteToGroup } from "./link-athlete-to-group";
import { getConsumerModelConfig } from '../getConsumerModelConfig';
import { SubscriptionType } from '../../../../../../models/enums/enums.model';
import { OnboardingValidationResponse } from '../../../interfaces/onboarding-validation-response';
import { getGroupSnap } from './getGroupSnap';
import { getGroup } from './getGroup';
import { WhiteLabelThemeType, WhiteLabelTheme } from '../../../interfaces/WhiteLabelTheme';
import { getCollectionRef } from '../../../../../../db/refs';
import { FirestoreCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';
import { getDocumentFromSnapshot } from '../../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { response } from 'express';
import { OrganizationConfigurationInterface } from '../../../../../../models/organization.model';

export const createEntities = (athleteIndicator: AthleteIndicator, code: string): Promise<OnboardingValidationResponse> => {

    return generateOrgCode().then(async (organizationCode) => {

        if (organizationCode) {

            const consumerConfig = await getConsumerModelConfig();

            if (consumerConfig) {

                let newOrg: Organization = new Organization({
                    contactNumber: '',
                    contactPerson: athleteIndicator.email,
                    organizationCode,
                    name: `${athleteIndicator.firstName}'s Organization`,
                    uid: '',
                    coaches: [athleteIndicator.email],
                    ...consumerConfig,
                    onboardingConfiguration: {
                        // ...consumerConfig.onboardingConfiguration,
                        autoCreateGroups: true,
                        prePaidNumberOfMonths: 0,
                        isPrePaid: false,
                        subscriptionType: SubscriptionType.Free,
                        paidGroups: []
                    },
                    nonEngagementConfiguration: {
                        dayCountNonEngagement: 3,
                        // ...consumerConfig.nonEngagementConfiguration,
                        // TODO
                        // utcRunHour: 12
                    },
                    licensesConsumed: 1,
                    licensesPurchased: 1,
                    creationTimestamp: athleteIndicator.creationTimestamp,
                    theme: 'catalyst',
                    config: {
                        wellnessGeoCapture: {
                            mood: false,
                            sleep: false,
                            fatigue: false,
                            pain: false,
                            food: false,
                            training: false,
                            period: false,
                            sick: true
                        } as WellnessGeoCaptureConfigurationInterface,
                        sickSymptomTracking: [],
                        validConsumedOnboardingCodes: ['*'],
                        validConsumedMediaBundleCodes: [],
                        validConsumedNotificationStreamCodes: [],
                        publishedMediaBundles: [],
                        bucketSize: 50,
                        groupPrefix: 'TEAM',
                        storageRoot: 'CATALYST',
                        showLeaderboard: true,
                        showCommunityHub: true,
                        forceAutocompleteValueRefresh: false,
                        validThemes: [],
                        allowInvalidThemeChanges: false,
                        termsAndConditionsUrl: 'https://catalyst.com/app-terms-and-conditions/',
                        privacyStatementUrl: 'https://catalyst.com/privacy-policy/',
                        appChannelId: '',
                        restrictVideoFastForwardToFirstCompletion: true,
                        videoSkipTimeInSeconds: 5,
                        processProgramCardRenderTasks: false,
                        processNotificationCardRenderTasks: true
                    } as OrganizationConfigurationInterface
                });

                newOrg = new Organization(await persistNewOrg(newOrg.toJS()).catch(exception => {
                    console.log('exception!!!! => ', exception);
                    const theExceptionResponse = {
                        message: 'An exception occured Creating a new Organization ex ->' +
                            { ...exception },
                        responseCode: 500,
                        data: undefined,
                        error: {
                            message: 'An exception occurred Creating a new Organization: ex ->' +
                                { ...exception },
                            exception,
                        },
                        method: {
                            name: 'createEntities',
                            line: 150,
                        }
                    };
                    throw theExceptionResponse;
                }));

                const groupIdentifier = 'G1';

                const groupSnap = await getGroupSnap(newOrg, groupIdentifier)

                const group = groupSnap.exists ? getGroup(groupSnap) : undefined

                const onboardingValidationResponse = await linkAthleteToGroup(newOrg, groupSnap, group, groupIdentifier, athleteIndicator, code)


                let themeUid: WhiteLabelThemeType = 'catalyst'

                const theTheme = await getCollectionRef(FirestoreCollection.WhiteLabelThemes).doc(themeUid)
                    .get().then((docSnap) => {
                        if (docSnap.exists) {
                            return getDocumentFromSnapshot(docSnap);
                        } else {
                            themeUid = 'catalyst'

                            return getCollectionRef(FirestoreCollection.WhiteLabelThemes).doc(themeUid)
                                .get().then((defaultDocSnap) => {
                                    return getDocumentFromSnapshot(defaultDocSnap);
                                })
                        }

                    }) as WhiteLabelTheme

                onboardingValidationResponse.data.theme = theTheme

                return onboardingValidationResponse;
            }
            else {
                console.log('No consumner Configuration Found')
                return {
                    message: 'No consumner Configuration Found',
                    responseCode: 404,
                    data: undefined,
                    error: {
                        message: 'No consumner Configuration Found',
                    },
                    method: {
                        name: 'createEntities',
                        line: 150,
                    }
                } as OnboardingValidationResponse
            }
        }
        else {
            return createEntities(athleteIndicator, code)
        }
    });
};
