// import { firestore } from 'firebase-admin';

// import { ProgramCatalogue } from '../../../../../../db/programs/models/interfaces/program-catalogue';
// import { FirestoreCollection, FirestoreProgramsCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';
// import { ProgramIndicator } from '../../../../../../db/programs/models/interfaces/program-indicator';
// import { getCollectionRef } from '../../../../../../db/refs';

// export const getOnboardingProgram = async (): Promise<ProgramIndicator> => {

//     let progCatRef: firestore.DocumentReference;

//     return await getCollectionRef(FirestoreCollection.ProgramCatalogue)
//         .doc(FirestoreProgramsCollection.General)
//         .get()
//         .then(async (progCatSnap: firestore.DocumentSnapshot) => {
//             if (progCatSnap.exists) {
//                 progCatRef = progCatSnap.ref;
//                 const progCat = progCatSnap.data() as ProgramCatalogue;
//                 const onboardingProgram = progCat.programs.filter(program => program.isPlaceHolder).shift();
//                 console.log(progCat.programs.length)
//                 console.log(onboardingProgram)
//                 return onboardingProgram;
//             }
//             else
//                 return undefined;
//         })
//     // add the Content
//     .then(async (programIndicator) => {
//         if (programIndicator) {

//             // console.warn('Loading content for Program with GUID', programIndicator.programGuid)
//             await progCatRef
//                 .collection('content')
//                 .doc(programIndicator.programGuid)
//                 .get()
//                 .then(contentSnap => {
//                     if (contentSnap.exists) {
//                         const programContent = contentSnap.data();
//                         if (programContent) {
//                             programIndicator.content =
//                                 programContent.content;
//                         }
//                     }
//                 })
//                 .catch(err => {
//                     console.log(err);
//                 });
//         }
//         // TODO: Handle programIndicator === undefined

//         // console.warn('programIndicator', programIndicator)
        
//         return programIndicator;
//     });
// };
