export const sanitizeOnboardingCode = (input: string) => {
    let code = '';
    if (input) {
        const parts = input.split('-');
        code = parts[0].trim().toUpperCase();
        if (parts.length > 1) {
            code += `-${parts[1].trim().toUpperCase()}`;
        }
    }
    return code;
};
