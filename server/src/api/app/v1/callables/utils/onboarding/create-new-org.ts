import { OrganizationInterface } from '../../../../../../models/organization.model';
import { getOrgCollectionRef } from '../../../../../../db/refs';

export const persistNewOrg = async (org: OrganizationInterface): Promise<OrganizationInterface> => {
    
    return await getOrgCollectionRef()
        .add(org)
        .then((docRef: FirebaseFirestore.DocumentReference) => {
            return docRef.update({ uid: docRef.id }).then(async () => {
                const newOrg: OrganizationInterface = {
                    ...org,
                    uid: docRef.id
                };
                
                return newOrg;
            });
        });
};
