import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections'
import { Athlete } from '../../../../../models/athlete/interfaces/athlete'
import { Group } from '../../../../../models/group/interfaces/group'
import { db } from '../../../../../shared/init/initialise-firebase'

export const getGroupAthletes = async (
    groupId: string
): Promise<Array<Athlete>> => {
    let athletes: Array<Athlete> = []
    console.log(`Querying from group ${groupId}`)
    const group = await db
        .collection(FirestoreCollection.Groups)
        .doc(groupId)
        .get()
        .then(doc => {
            if (doc.exists) {
                return doc.data() as Group
            } else {
                return undefined
            }
        })

    if (group) {
        if (group.athleteIndex) {
            console.log(`Group ${groupId} found.`)
            const keys = Object.keys(group.athleteIndex)
            console.log(`Searching for athletes ${JSON.stringify(keys)}.`)
            for (let key of keys) {
                const athlete = await db
                    .collection(FirestoreCollection.Athletes)
                    .doc(key)
                    .get()
                    .then(doc => {
                        if (doc.exists) {
                            console.log(`Athlete ${key} found.`)
                            return doc.data() as Athlete
                        } else {
                            console.log(`Athlete ${key} NOT found.`)
                            return undefined
                        }
                    })

                if (athlete) {
                    athletes.push(athlete)
                }
            }

            console.log(`Found ${athletes.length} athlete documents`)
        } else {
            console.error(`Group ${groupId} has NO athleteIndex map.`)
        }
    } else {
        console.error(`Group ${groupId} NOT found.`)
    }

    return athletes
}
