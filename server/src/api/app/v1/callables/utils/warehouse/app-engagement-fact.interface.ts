import { AppEngagementTaskType } from '../../../../../../db/taskQueue/enums/scheduled-task.enums';
export interface AppEngagementFact {
    app_engagement_type: AppEngagementTaskType;
    global_session_id: string;
    screen_session_id: string;
    bundle_id: string;
    segment_id: string;
    athlete_id: string;
    program_id?: string;
    evolution_instance_id?: string;
    created_at?: Date;
    device_type?: string;
    device_manufacturer?: string;
    device_model?: string;
    device_os?: string;
    device_os_version?: string;
    device_sdk_version?: string;
    device_uuid?: string;
}