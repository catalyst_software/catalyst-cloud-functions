import { message } from './../../../controllers/utils/reports/message';
import { Athlete } from './../../../../../../models/athlete/athlete.model';
import { initialiseFirebase } from '../../../../../../shared/init/initialise-firebase';
import { AthleteSegmentResponse } from './athlete-segment-response.interface';
import { OrganizationInterface } from '../../../../../../models/organization.model';
import { SegmentDimension } from './segment-dimension.interface';
import { SegmentAthleteDimension } from './segment-athlete-dimension.interface';
import { FirestoreCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';
import { SegmentType } from './segment-type.enum';
import { getDocumentFromSnapshot } from '../../../../../../analytics/triggers/utils/get-document-from-snapshot';
const existingOrgs: Array<OrganizationInterface> = [];

export const getOrganizationDocument = (orgId: string, database?: FirebaseFirestore.Firestore): Promise<OrganizationInterface> => {
    
    const existing = existingOrgs.find((org: OrganizationInterface) => {
        return !!org && org.uid === orgId;
    });

    if (existing) {
        return Promise.resolve(existing);
    } else {
        const db = (!!database) ? database : initialiseFirebase('API').db;
        return db.collection(FirestoreCollection.Organizations)
            .doc(orgId)
            .get()
            .then((doc) => {
                const newOrg = getDocumentFromSnapshot(doc) as OrganizationInterface;
                existingOrgs.push(newOrg);

                return newOrg;
            })
            .catch((e) => {
                console.log(`Error raised retrieving Organization document.  Message: ${e.message}`);
                return undefined;
            });
    }
}

export const getParentOrganizationId = async (childOrgId: string): Promise<string> => {
    const org = await getOrganizationDocument(childOrgId);

    return Promise.resolve((!!org && org.parentOrganizationId || '') || '');
}

const loadAthleteOrgSegments = (orgId: string, athleteUid: string, segments: Array<SegmentDimension>, segmentAthletes: Array<SegmentAthleteDimension>, db: FirebaseFirestore.Firestore): Promise<void> => {
    return new Promise((resolve) => {
        // tslint:disable-next-line: no-floating-promises
        getOrganizationDocument(orgId, db)
            .then(async (org: OrganizationInterface) => {
                if (org) {
                    segments.push({
                        segment_id: org.uid,
                        segment_name: org.name,
                        segment_type: SegmentType.Organization,
                        parent_segment_id: org.parentOrganizationId
                    } as SegmentDimension);
    
                    segmentAthletes.push({
                        segment_id: org.uid,
                        athlete_id: athleteUid
                    } as SegmentAthleteDimension);

                    if (org.parentOrganizationId) {
                        await loadAthleteOrgSegments(org.parentOrganizationId, athleteUid, segments, segmentAthletes, db)
                            .then(() => {
                                resolve();
                            })
                            .catch((e) => {
                                console.error(`Error in recursive loadAthleteOrgSegments call.  Message: ${e.message}`);
                                resolve();
                            });
                    } else {
                        resolve();
                    }
                } else {
                    resolve();
                }
            })
            .catch((e) => {
                console.error(`Error retrieving organization ${orgId}.  Message: ${e.message}`);
                resolve();
            });
    });
}


export const getAthleteOrgSegments = (athlete: Athlete): Promise<AthleteSegmentResponse> => {
    return new Promise(async (resolve) => { 
        const db: FirebaseFirestore.Firestore = initialiseFirebase('API').db;

        const segments: Array<SegmentDimension> = [];
        const segmentAthletes: Array<SegmentAthleteDimension> = [];

        const orgId = athlete.organizationId;

        if (orgId) {
            await loadAthleteOrgSegments(orgId, athlete.uid, segments, segmentAthletes, db)
                .then(() => {
                    resolve({
                        segments,
                        segmentAthletes
                    });
                })
                .catch((e) => {
                    console.error(`Error in loadAthleteOrgSegments call.  Message: ${e.message}`);
                    resolve({
                        segments,
                        segmentAthletes
                    });
                });
        } else {
            resolve({
                segments,
                segmentAthletes
            });
        }
        
    });
}

