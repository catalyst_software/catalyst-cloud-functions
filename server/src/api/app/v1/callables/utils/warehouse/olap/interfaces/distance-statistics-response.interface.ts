import { OlapSecurityProfile } from "./olap-security-profile.interface";

export interface DistanceStatisticsResponse {
    startDate: string;
    endDate: string;
    athleteDistance: number;
    totalDistance: number;
    numberOfParticipants: number;
    numberOfCountries: number;
    requestorSecurityProfile: OlapSecurityProfile;
}
