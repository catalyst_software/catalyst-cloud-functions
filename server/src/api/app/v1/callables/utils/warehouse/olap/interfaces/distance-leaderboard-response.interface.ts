import { OlapSecurityProfile } from "./olap-security-profile.interface";

export interface DistanceLeaderboardResponse {
    topTen: Array<DistanceLeaderboardRecord>;
    startDate: string;
    endDate: string;
    distance: number;
    totalDistance: number;
    rank: string;
    requestorSecurityProfile: OlapSecurityProfile;
}

export interface DistanceLeaderboardRecord {
    'ScalarBiometricFact.athleteId': string;
    'AthleteDim.fullName': string;
    'AthleteDim.imageUrl': string;
    'ScalarBiometricFact.created.day': string;
    'ScalarBiometricFact.created': string;
    'ScalarBiometricFact.totalValue': number;
}

