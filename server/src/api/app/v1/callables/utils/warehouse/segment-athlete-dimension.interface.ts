export interface SegmentAthleteDimension {
    segment_id: string;
    athlete_id: string;
}