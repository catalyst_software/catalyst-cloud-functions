export interface OlapAthleteLeaderboardEntry {
    uid: string;
    firstName: string;
    lastName: string;
    profileImageUrl?: string;
    bio?: string;
    sport?: string;
    currentDayIpScore?: OlapAthleteLeaderboardIpScoreEntry;
    sevenDayIpScore?: OlapAthleteLeaderboardIpScoreEntry;
    fourteenDayIpScore?: OlapAthleteLeaderboardIpScoreEntry;
    twentyEightDayIpScore?: OlapAthleteLeaderboardIpScoreEntry;
}

export interface OlapAthleteLeaderboardIpScoreEntry {
    position: number;
    score: number;
    dateRange: Array<string>;
}