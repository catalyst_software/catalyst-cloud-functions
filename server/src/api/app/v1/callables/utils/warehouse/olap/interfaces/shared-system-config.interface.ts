export interface SharedSystemConfiguration {
    growthStudio: { endpoint: string };
    redis: { host: string; port: number };
    olap: { apiSecret: string; apiUrl: string; };
    postgres: { username: string; password: string; host: string; database: string; port: number; };
    event: { startDate: string; endDate: string; };
    rest: { password: string };
    onboardingWithInviteUrl: string;
    jwtSecret: string;
    scheduledTasks?: Array<ScheduledTaskConfiguration>;
    secondaryProjectIdentifiers?: Array<any>;
}

export interface ScheduledTaskConfiguration {
    httpTrigger: string;
    location: string;
    projectId: string;
    queue: string;
    oidcEmail: string;
}
