import { AppEngagementTaskType } from '../../../../../../db/taskQueue/enums/scheduled-task.enums';
export interface BiometricFact {
    health_component_type: string;
    lifestyle_entry_type: string;
    lifestyle_entry_sub_type_1?: string;
    lifestyle_entry_sub_type_2?: string;
    biometric_entry_id?: string;
    global_session_id: string;
    screen_session_id: string;
    bundle_id: string;
    segment_id: string;
    athlete_id: string;
    evolution_instance_id?: string;
    created_at: any;
    value: number;
    device_type?: string;
    device_manufacturer?: string;
    device_model?: string;
    device_os?: string;
    device_os_version?: string;
    device_sdk_version?: string;
    device_uuid?: string;
}