export interface CloudTaskUsageMetricFact {
    uuid?: string;
    cloud_task_type: string;
    segment_id: string;
    athlete_id: string;
    created_at?: Date;
    input_as_string?: string;
    output_as_string?: string;
    input_size_kb?: number;
    output_size_kb?: number;
    total_milliseconds?: number;
}