export interface AthleteSegmentCacheItem {
    athleteId: string;
    segments: Array<string>;
}