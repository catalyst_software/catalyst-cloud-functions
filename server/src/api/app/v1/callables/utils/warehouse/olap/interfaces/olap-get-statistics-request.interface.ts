import { OlapSecurityProfile } from './olap-security-profile.interface';
import { LifestyleEntryType } from '../../../../../../../../models/enums/lifestyle-entry-type';
import { OlapCubeEntityType } from '../../../../../../../../models/enums/olap-entity-type';
export interface OlapGetStatisticsRequest {
    requestorSecurityProfile: OlapSecurityProfile;
    startDate: string;
    endDate: string;
    granularity?: string;
    entityIds?: Array<string>;
    entityType?: string;
    lifestyleEntryType?: LifestyleEntryType;
    lifestyleEntrySubType1?: string;
    lifestyleEntrySubType2?: string;
    measure?: string;
}

export interface OlapGetLifestyleEntryTypeRequest {
    requestorSecurityProfile: OlapSecurityProfile;
    entityId: string;
    entityType: OlapCubeEntityType;
    lifestyleEntryType: LifestyleEntryType;
    lifestyleEntrySubType1?: string;
    lifestyleEntrySubType2?: string;
    startDate: string;
    endDate: string;
    factTable?: string;
    measure?: string;
    granularity?: string;
}
