import { AthleteSegmentResponse } from './athlete-segment-response.interface';

export interface AthleteSegmentCompoundResponse {
    orgSegmentResponse: AthleteSegmentResponse;
    groupSegmentResponse: AthleteSegmentResponse;
}
