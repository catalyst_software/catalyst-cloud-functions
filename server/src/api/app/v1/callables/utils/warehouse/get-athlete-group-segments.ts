import { Athlete } from './../../../../../../models/athlete/athlete.model';
import { AthleteSegmentResponse } from './athlete-segment-response.interface';
import { Group } from '../../../../../../models/group/interfaces/group';
import { SegmentDimension } from './segment-dimension.interface';
import { SegmentType } from './segment-type.enum';
import { SegmentAthleteDimension } from './segment-athlete-dimension.interface';

export const getAthleteGroupSegments = (athlete: Athlete): AthleteSegmentResponse => {

    let segments: Array<SegmentDimension> = [];
    let segmentAthletes: Array<SegmentAthleteDimension> = [];
    if (athlete.groups) {
        segments = athlete.groups.map((g: Group) => {
            return {
                segment_id: g.groupId,
                segment_name: g.groupName,
                parent_segment_id: athlete.organizationId,
                segment_type: SegmentType.Group
            } as SegmentDimension;
        });

        segmentAthletes = segments.map((s: SegmentDimension) => {
            return {
                segment_id: s.segment_id,
                athlete_id: athlete.uid
            } as SegmentAthleteDimension;
        });

    }

    return {
        segments,
        segmentAthletes
    };
}
