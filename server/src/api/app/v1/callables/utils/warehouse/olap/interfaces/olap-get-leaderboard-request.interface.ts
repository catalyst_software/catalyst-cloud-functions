import { OlapSecurityProfile } from './olap-security-profile.interface';
export interface OlapGetLeaderboardRequest {
    requestorSecurityProfile: OlapSecurityProfile;
    queryGroupId: string;
    queryDaySets: Array<number>;
    startDate: string;
    endDate: string;
    granularity: string;
    excludeAthleteData?: boolean;
}