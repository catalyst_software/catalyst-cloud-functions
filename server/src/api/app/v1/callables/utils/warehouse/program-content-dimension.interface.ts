export interface ProgramContentDimension {
    program_id: string;
    content_entry_id: string;
    program_name: string;
    content_entry_name: string;
    content_entry_type: string;
    program_category?: string;
}