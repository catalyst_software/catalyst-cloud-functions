export interface AppSessionUsageMetricFact {
    app_engagement_type: string;
    global_session_id: string;
    screen_session_id: string;
    bundle_id: string;
    segment_id: string;
    athlete_id: string;
    program_id?: string;
    created_at?: Date;
    total_milliseconds: number;
    device_type?: string;
    device_manufacturer?: string;
    device_model?: string;
    device_os?: string;
    device_os_version?: string;
    device_sdk_version?: string;
    device_uuid?: string;
}