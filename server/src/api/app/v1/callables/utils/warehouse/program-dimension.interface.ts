export interface ProgramDimension {
    program_id: string;
    program_name: string;
    program_category?: string;
}
