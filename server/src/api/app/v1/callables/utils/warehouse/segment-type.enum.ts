export enum SegmentType {
    Organization = 'organization',
    Group = 'group'
}
