export interface ProgramContentTagDimension {
    program_id: string;
    content_entry_id: string;
    tag_name: string;
}