import { LifestyleEntryType } from '../../../../../../../../models/enums/lifestyle-entry-type';

export interface BiometricStatisticsResponse {
    datasets: Array<LifestyleEntryStatisticsResponse>
}

export interface LifestyleEntryStatisticsResponse {
    data: Array<number>;
    metadata: LifestyleEntryStatisticsMetadataResponse;
}

export interface LifestyleEntryStatisticsMetadataResponse {
    entityId: string;
    entityType: string;
    lifestyleEntryType: LifestyleEntryType;
    lifestyleEntrySubType1?: string;
    lifestyleEntrySubType2?: string;
    startDate: string;
    endDate: string;
    measure: string;
}

