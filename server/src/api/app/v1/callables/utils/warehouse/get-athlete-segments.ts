import { Athlete } from './../../../../../../models/athlete/athlete.model';
import { getAthleteOrgSegments } from './get-athlete-org-segments';
import { AthleteSegmentResponse } from './athlete-segment-response.interface';
import { getAthleteGroupSegments } from './get-athlete-group-segments';
import { AthleteSegmentCompoundResponse } from './athlete-segment-compound-response.interface';

export const getAthleteSegments = async (athlete: Athlete): Promise<AthleteSegmentCompoundResponse> => {
    const orgSegmentResponse: AthleteSegmentResponse = await getAthleteOrgSegments(athlete);
    const groupSegmentResponse: AthleteSegmentResponse = getAthleteGroupSegments(athlete);
    
    return {
        orgSegmentResponse,
        groupSegmentResponse
    } as AthleteSegmentCompoundResponse;
}
