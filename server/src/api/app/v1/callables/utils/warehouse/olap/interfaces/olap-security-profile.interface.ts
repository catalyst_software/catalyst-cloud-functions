export interface OlapSecurityProfile {
    athleteId: string;
    groupIds: Array<string>;
    organizationId: string;
}