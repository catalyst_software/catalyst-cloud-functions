export interface ProgramContentEngagementFact {
    content_engagement_type: string;
    content_type: string;
    program_id: string;
    content_entry_id: string;
    global_session_id: string;
    screen_session_id: string;
    bundle_id: string;
    segment_id: string;
    athlete_id: string;
    evolution_instance_id?: string;
    created_at?: any;
    device_type?: string;
    device_manufacturer?: string;
    device_model?: string;
    device_os?: string;
    device_os_version?: string;
    device_sdk_version?: string;
    device_uuid?: string;
}
