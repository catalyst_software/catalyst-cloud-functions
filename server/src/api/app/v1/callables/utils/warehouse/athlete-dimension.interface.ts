export interface AthleteDimension {
    athlete_id: string;
    first_name: string;
    last_name: string;
    full_name: string;
    local_birth_date: number;
    sex: string;
    location: string;
    imageUrl: string;
    sport: string;
    bio: string;
}