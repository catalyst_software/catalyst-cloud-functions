import { OlapSecurityProfile } from "./interfaces/olap-security-profile.interface";
import { OlapSecurityJwtPackage } from "./interfaces/olap-security-jwt-package.interface";
import { initialiseFirebase } from '../../../../../../../shared/init/initialise-firebase';
import { SharedSystemConfiguration } from './interfaces/shared-system-config.interface';
import { FirestoreCollection } from "../../../../../../../db/biometricEntries/enums/firestore-collections";
import { getDocumentFromSnapshot } from "../../../../../../../analytics/triggers/utils/get-document-from-snapshot";

const jwt = require('jsonwebtoken');
const redis = require("redis");
let cachedConfig: SharedSystemConfiguration = undefined;
let client: any;

export const getSharedSystemConfiguration = (): Promise<SharedSystemConfiguration> => {
    // if (!!cachedConfig) {
    //     return Promise.resolve(cachedConfig);
    // } else {
        const db = initialiseFirebase('API').db; 
        return db.collection(FirestoreCollection.SystemConfiguration)
                .doc('shared')
                .get()
                .then((doc) => {
                    cachedConfig = getDocumentFromSnapshot(doc) as unknown as SharedSystemConfiguration;
                    return cachedConfig;
                })
                .catch((e) => {
                    console.log(`Error raised retrieving Shared SystemConfiguration document.  Message: ${e.message}`);
                    return undefined;
                });
    // }
}

export const getRedisValue = (redisClient: any, key: string): Promise<any> => {
    console.log(`Attempting to retrieve value for key ${key} from Redis`);

    return new Promise((resolve) => {
        try {
            redisClient.get(key, (err, reply) => {
                if (err) {
                    console.error(err);
                    resolve(undefined);
                } else {
                    if (!!reply) {
                        console.log(`Value successfully retrieved: ${reply.toString()}`);
                        resolve(JSON.parse(reply.toString()));
                    } else {
                        console.log(`Key ${key} not found.`);
                        resolve(undefined);
                    }
                }
            });
        } catch (err) {
            console.error(err);
            resolve(undefined);
        }

        resolve(undefined);
    });
}

export const setRedisValue = (redisClient: any, key: string, value: string, expiresSeconds: number): Promise<any> => {
    console.log(`Attempting to set Redis key ${key} with value ${value} with expires seconds ${expiresSeconds}`);

    return new Promise((resolve) => {
        redisClient.set(key, value, 'EX', expiresSeconds, (err, reply) => {
            if (err) {
                console.error(err);
                resolve(undefined);
            } else {
                if (!!reply) {
                    console.log(`Redis value with key ${key} successfully set: ${reply.toString()}`);
                    resolve(JSON.parse(reply.toString()));
                } else {
                    console.log(`Key ${key} not set.`);
                    resolve(undefined);
                }
            }
        });
    });
}

export const getOlapJwtPackage = async (securityProfile: OlapSecurityProfile, config: SharedSystemConfiguration): Promise<OlapSecurityJwtPackage> => {
    // if (!client) {
    //     console.log(`Attempting to connect to Redis server at ${config.redis.host} on port ${config.redis.port}`)
    //     try {
    //         client = redis.createClient({
    //             host: config.redis.host,
    //             port: config.redis.port 
    //         });
    //     } catch (e) {
    //         console.error(`An error occurred when attempting to connect to redis.  Message: ${e.message}`);
    //         client = null;
    //     }
    // }

    let tokenPackage: OlapSecurityJwtPackage = undefined;
    // if (!!client) {
    if (false) {
        const tokenKey = `${securityProfile.athleteId}-jwt-package`;
        tokenPackage = await getRedisValue(client, tokenKey) as OlapSecurityJwtPackage;
        if (!tokenPackage) {
            console.log(`Token package not found in Redis.  Proceeding to create olap jwt token for athlete ${securityProfile.athleteId}`);
            const newToken = jwt.sign({u: securityProfile}, config.olap.apiSecret, { expiresIn: '30d' });
            tokenPackage = {
                athleteId: securityProfile.athleteId,
                token: newToken
            } as OlapSecurityJwtPackage;

            const days29 = (60 * 60 * 24 * 29);
            console.log(`Timing Redis key ${tokenKey} out in ${days29} seconds ( 29 days)`);
            await setRedisValue(client, tokenKey, JSON.stringify(tokenPackage), days29);
        }
    } else {

        console.log(`Unable to connect to Redis.  Creating 1 hour jwt token.`);
        const newToken = jwt.sign({u: securityProfile}, config.olap.apiSecret, { expiresIn: '1h' });
        tokenPackage = {
            athleteId: securityProfile.athleteId,
            token: newToken
        } as OlapSecurityJwtPackage;
    }

    return Promise.resolve(tokenPackage);
}