export interface DeviceDimension {
    device_id?: string; 
    device_type?: string; 
    device_manufacturer?: string;
    device_model?: string;
    device_os?: string;
    device_os_version?: string;
    device_sdk_version?: string;
}