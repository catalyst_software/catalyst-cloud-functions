export interface SegmentDimension {
    parent_segment_id: string;
    segment_id: string;
    segment_name: string;
    segment_type: string;
}
