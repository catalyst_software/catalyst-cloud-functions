export interface OlapSecurityJwtPackage {
    athleteId: string;
    token: string;
}