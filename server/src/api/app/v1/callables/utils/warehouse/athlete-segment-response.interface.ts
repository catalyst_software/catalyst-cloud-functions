import { SegmentDimension } from './segment-dimension.interface';
import { SegmentAthleteDimension } from './segment-athlete-dimension.interface';

export interface AthleteSegmentResponse {
    segments: Array<SegmentDimension>;
    segmentAthletes: Array<SegmentAthleteDimension>;
}
