import { GetValuesByIPScoreWeeklyAggregateValue } from './maintenance/GetValuesByIPScoreWeeklyAggregateValue';
import { getAthleteCollectionRef, getDocFromRef, getAthleteDocRef } from '../../../../../db/refs';
import { updateAthleteIPScoreValue } from '../../controllers/utils/update-athlete-ipscore-value';
import { Athlete } from '../../../../../models/athlete/interfaces/athlete';
import { getDocumentFromSnapshot } from '../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { reRunIPScoreAggregates } from './maintenance/reRunIPScoreAggregates';
import { db } from '../../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections';
import { WellAggregateCollection } from '../../../../../db/biometricEntries/enums/firestore-aggregate-collection';
import { getDayIndex, getWeekNumber } from '../../../../../shared/utils/moment';
import { firestore } from 'firebase-admin';








export async function getAthleteAggregateDocs(ath: Athlete, startDate: Date, endDate?: Date, utcOffset?: number) {


    // removeUndefinedProps(task)

    const request = {
        parent: parent,
    };
    const dayIndex = getDayIndex(firestore.Timestamp.now(), utcOffset || 0);
    const weekNr = getWeekNumber(ath.metadata.creationTimestamp, firestore.Timestamp.now(), utcOffset || 0);
    db.collection(WellAggregateCollection.AthleteWeekly)
    return request

}




export const resetAthletesIpScore = async (athleteUID?: string, utcOffset?: number, startDate?: Date, useAggregateDocs?: boolean) => {

    // const athleteWeeklyIpScoreDocs = await GetValuesByIPScoreWeeklyAggregateValue(athleteUID, utcOffset);
    const useDailyAggregateValues = true

    if (useDailyAggregateValues) {
        
        const athlete = await getDocFromRef(getAthleteDocRef(athleteUID)) as Athlete;

        const inacutrateResults = await GetValuesByIPScoreWeeklyAggregateValue(athlete, utcOffset, athlete.runningTotalIpScore, startDate);

        return inacutrateResults

    }
    // else if (useAggregateDocs) {

    //     const athlete = await getDocFromRef(getAthleteDocRef(athleteUID)) as Athlete;


    //     const athAggregateDocs = getAthleteAggregateDocs(athlete, startDate)

    //     return athlete
    // }
    else {



        const athlete = await getDocFromRef(getAthleteDocRef(athleteUID)) as Athlete;

        // const allAthletes = await getAthleteCollectionRef()
        // .where('firebaseMessagingId', '>=', '0')
        // .get().then((qurySnap) => {

        //     console.log(`${qurySnap.docs.length} docs found`)
        //     return qurySnap.docs.map((queryDocSnap) => {

        //         const athlete = getDocumentFromSnapshot(queryDocSnap) as Athlete;

        //         return athlete
        //     })
        // })

        // const results = allAthletes.map(async (athlete) => {
        const directive = await reRunIPScoreAggregates(undefined, athlete, utcOffset, startDate);
        athlete.runningTotalIpScore = athlete.currentIpScoreTracking.ipScore || 0;
        // athlete.currentIpScoreTracking.ipScore = 0;
        // if (!directive.skipSaveDocs) {
        console.log(athlete.currentIpScoreTracking.ipScore)  // await updateAthleteIPScoreValue(athlete, athlete.runningTotalIpScore, athlete.currentIpScoreTracking.ipScore)
        // }

        return athlete
        // })




        // // const athlete = await getDocFromRef(getAthleteDocRef(athleteUID)) as Athlete;

        // const allAthletes = await getAthleteCollectionRef()
        // .where('firebaseMessagingId', '>=', '0')
        // .get().then((qurySnap) => {

        //     console.log(`${qurySnap.docs.length} docs found`)
        //     return qurySnap.docs.map((queryDocSnap) => {

        //         const athlete = getDocumentFromSnapshot(queryDocSnap) as Athlete;

        //         return athlete
        //     })
        // })

        // const results = allAthletes.map(async (athlete) => {
        //     // const directive = await reRunIPScoreAggregates(athlete, utcOffset);

        //     // if (!directive.skipSaveDocs) {
        //         await updateAthleteIPScoreValue(athlete, athlete.runningTotalIpScore, athlete.currentIpScoreTracking.ipScore)
        //     // }

        //     return athlete
        // })

        // TODO: Update Docs
        // return {
        //     message: 'string',
        //     responseCode: 200,
        //     data: await Promise.all(results),
        // }

    }
}