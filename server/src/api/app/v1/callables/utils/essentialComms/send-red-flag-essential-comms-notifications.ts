import { removeUndefinedProps } from './../../../../../../db/biometricEntries/services/helpers/save-aggregates/utils/remove-undefined-props';
import { RedFlagNotificationUser } from './../../../../../../../../.history/server/src/api/app/v1/callables/utils/essentialComms/types_20210120103045';
import { FirestoreCollection } from "../../../../../../db/biometricEntries/enums/firestore-collections";
import { PushNotificationsType } from "../../../../../../models/enums/enums.model";
import { KeyValuePair, NotificationTemplate } from "../../../../../../models/notifications/interfaces/schedule-notification-payload";
import { admin, db } from "../../../../../../shared/init/initialise-firebase";
import { isArray } from 'lodash';
import { EmailMessageTemplate } from '../../../sendGrid/templates/email-message-template';
import { handleComms } from '../comms/handle-comms';
import { NotificationConfig } from '../../../controllers/athletes';

const tokenizeMessageString = (templateString: string, keyValuePairs: KeyValuePair[]) => {
    let newString: string = templateString;
    keyValuePairs.forEach(pair => {
        newString = newString.replace('$'+pair.key, pair.value);
    });

    return newString;
}

export const sendRedFlagEssentialCommsNotifications = async (emailTemplate: EmailMessageTemplate, notificationUser: RedFlagNotificationUser, templateName: string, parameters: KeyValuePair[], appIdentifier: string, sendEmail: boolean, path?: string, icon?: string): Promise<boolean> => {
    console.log(`sendRedFlagEssentialCommsNotifications called with parameters - notificationUser:  ${JSON.stringify(notificationUser)}, templateName: ${templateName}, parameters:  ${JSON.stringify(parameters)}, appIdentifier: ${appIdentifier}, path: ${path}, icon: ${icon}`);
    const template: NotificationTemplate = await db
            .collection(FirestoreCollection.NotificationTemplates)
            .doc(templateName)
            .get()
            .then((d) => {
                console.log(`Template document retrieved: ${JSON.stringify(d)}`);
                return d.data() as NotificationTemplate;
            })
            .catch((err) => {
                console.error(`Failed to retrieve template document.  Error: ${JSON.stringify(err)}`);
                return undefined;
            });

    if (template) {
        if (isArray(notificationUser.firebaseMessagingIds) && notificationUser.firebaseMessagingIds.length) {
            if (!template.excludePush) {
                const message = template.message;
                message.notification = {
                    ...message.notification,
                    title: tokenizeMessageString(template.message.notification.title, parameters),
                    body: tokenizeMessageString(template.message.notification.body, parameters)
                }

                if (icon) {
                    message.data.icon = icon;
                }

                if (path) {
                    message.data.path = path;
                }
                message.data.dateTime = (new Date()).toISOString();
                removeUndefinedProps(message);
                console.log(`Attempting to send push notification: ${JSON.stringify(message)}`);

                for (const index in notificationUser.firebaseMessagingIds) {
                    const token = notificationUser.firebaseMessagingIds[index];
                    console.log(`Firebase Messaging Id Token: ${token}`);
                    message.token = token;
                    console.log(`Attempting to send push notification: ${JSON.stringify(message)} to token ${token}`);
                    const response = await admin.messaging().send({
                        ...message,
                        // token,
                        // fcmOptions: {
                        //     analyticsLabel: 'new-content-feed-item-notification'
                        // }
                    })
                    .catch((err) => {
                        console.warn(`An error occurred when attempting to send push notification to firebaseMessagingId ${token}`, err)
                    });

                    console.log(`Send message response: ${JSON.stringify(response)}`);
                }

                if (sendEmail) {
                    const config = {
                        sendEmail,
                        sendPushNotification: false,
                        sendSMS: false
                    } as NotificationConfig;

                    console.log(`Attempting to send redFlag email`);
                    const sendResult = await handleComms(
                        emailTemplate,
                        undefined,
                        '',
                        notificationUser,
                        config,
                        false,
                        notificationUser.uid,
                        '',
                        notificationUser.firstName,
                        true
                    );

                    console.log(`Send email response: ${JSON.stringify(sendResult)}`);
                }
            } else {
                console.log('Message global property excludePush is true.  Aborting push notifications.');
            }
        } else {
            console.log(`Notification user ${notificationUser.uid} has no configured firebaseMessagingIds`);
        }
        return true;
    } else {
        console.log(`Notification template document with doc id ${templateName} not found!`);
        console.log('Unable to send message:', {
            // ...message,
            templateName,
            parameters,
        });

        return false;
    }
}