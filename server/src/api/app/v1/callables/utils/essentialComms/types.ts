export interface RedFlagNotificationUser {
    uid: string;
    firstName: string;
    lastName: string;
    isCoach: boolean;
    isOrgRep: boolean;
    email: string;
    phoneNumber?: string;
    firebaseMessagingIds?: Array<string>;
}
