import { GroupIndicator } from './../../../../../../db/biometricEntries/well/interfaces/group-indicator';
import { FirestoreCollection } from "../../../../../../db/biometricEntries/enums/firestore-collections";
import { Athlete } from "../../../../../../models/athlete/interfaces/athlete";
import { db } from "../../../../../../shared/init/initialise-firebase";
import { RedFlagNotificationUser } from "./types";
import { intersectionWith, isEqual } from 'lodash';

// Helpers
const groupsIntersect = (athleteGroups: Array<string>, coachGroups: Array<string>) => {
    console.log(`Running intersection on ${JSON.stringify(athleteGroups)} and ${JSON.stringify(coachGroups)}`)
    const intersect = intersectionWith(athleteGroups, coachGroups, isEqual);
    console.log(`Intersection result: ${JSON.stringify(intersect)}`);

    return intersect.length > 0;
}

export const getRedFlagNotificationUsers = async (orgId: string, groupIds: Array<string>, athleteId: string): Promise<Array<RedFlagNotificationUser>> => {
    console.log(`Querying for coaches in org ${orgId} for groups ${JSON.stringify(groupIds)} for athlete ${athleteId}`);
    const notificationUsers: Array<RedFlagNotificationUser> = [];
    let success = await db.collection(FirestoreCollection.Athletes)
        .where('organizationId', '==', orgId)
        .where('isCoach', '==', true)
        .get()
        .then((coachDocs: FirebaseFirestore.QuerySnapshot) => {
            if (coachDocs.docs.length) {
                coachDocs.docs.forEach((doc: FirebaseFirestore.QueryDocumentSnapshot) => {
                    if (doc.exists) {
                        const coach = doc.data() as Athlete;
                        const index = notificationUsers.findIndex((c: RedFlagNotificationUser) => {
                            return c.uid === coach.uid;
                        });

                        if (index < 0) {
                            const coachGroupIds = coach.groups.map((g: GroupIndicator) => {
                                return g.groupId;
                            })
                            if (groupsIntersect(groupIds, coachGroupIds)) {
                                console.log(`Groups intersect.  Adding coach ${coach.uid} for red flag notification.`);
                                notificationUsers.push({
                                    uid: coach.uid,
                                    firstName: coach.profile.firstName || '',
                                    lastName: coach.profile.lastName || '',
                                    isCoach: true,
                                    isOrgRep: false,
                                    email: coach.profile.email,
                                    firebaseMessagingIds: [...coach.firebaseMessagingIds]
                                } as RedFlagNotificationUser);
                            } else {
                                console.log(`Groups DO NOT intersect.  Skipping coach ${coach.uid} for red flag notification.`);
                            }
                        } else {
                            console.log(`Coach ${coach.uid} already included in notification array.`);
                        }
                    }
                });
            }
            return true;
        })
        .catch((err) => {
            console.error(`An error occurred when querying for coaches in organization ${orgId}`);

            return false;
        });
    
    success = await db.collection(FirestoreCollection.Athletes)
        .where('organizationId', '==', orgId)
        .where('isOrgRep', '==', true)
        .get()
        .then((orgRepDocs: FirebaseFirestore.QuerySnapshot) => {
            if (orgRepDocs.docs.length) {
                orgRepDocs.docs.forEach((doc: FirebaseFirestore.QueryDocumentSnapshot) => {
                    if (doc.exists) {
                        const orgRep = doc.data() as Athlete;
                        const index = notificationUsers.findIndex((c: RedFlagNotificationUser) => {
                            return c.uid === orgRep.uid;
                        });

                        if (index < 0) {
                            const orgRepGroupIds = orgRep.groups.map((g: GroupIndicator) => {
                                return g.groupId;
                            })
                            if (groupsIntersect(groupIds, orgRepGroupIds)) {
                                console.log(`Groups intersect.  Adding orgRep ${orgRep.uid} for red flag notification.`);
                                notificationUsers.push({
                                    uid: orgRep.uid,
                                    firstName: orgRep.profile.firstName || '',
                                    lastName: orgRep.profile.lastName || '',
                                    isCoach: false,
                                    isOrgRep: true,
                                    email: orgRep.profile.email,
                                    firebaseMessagingIds: [...orgRep.firebaseMessagingIds]
                                } as RedFlagNotificationUser);
                            } else {
                                console.log(`Groups DO NOT intersect.  Skipping orgRep ${orgRep.uid} for red flag notification.`);
                            }
                        } else {
                            console.log(`OrgRep ${orgRep.uid} already included in notification array.`);
                        }
                    }
                });

                return true;
            } else {
                return false;
            }
        })
        .catch((err) => {
            console.error(`An error occurred when querying for orgReps in organization ${orgId}`);

            return false;
        });

    return notificationUsers;
}
