import { FirestoreCollection } from "../../../../../../db/biometricEntries/enums/firestore-collections";
import { Organization } from "../../../../../../models/organization.model";
import { db } from "../../../../../../shared/init/initialise-firebase";

const getExcludedRedFlags = (org: Organization): Array<number> => {
    let excluded: Array<number> = [];
    console.log(`Examining org for red flag exclusion: ${JSON.stringify(org)}`);
    const redFlagConfig = org.redFlagNotifications;
    console.log(`Examining org for redFlagConfig: ${JSON.stringify(redFlagConfig)}`);
    if (!!redFlagConfig) {
        if (!redFlagConfig.processMood) {
            excluded = excluded.concat([20, 21]);
        }
        if (!redFlagConfig.processFatigue) {
            excluded = excluded.concat([23]);
        }
        if (!redFlagConfig.processInjury || !redFlagConfig.processPain) {
            excluded = excluded.concat([24]);
        }
        if (!redFlagConfig.processSleep) {
            excluded = excluded.concat([22]);
        }
        if (!redFlagConfig.processExcessiveTraining) {
            excluded = excluded.concat([25]);
        }
        if (!redFlagConfig.processPeriod) {
            excluded = excluded.concat([26, 27]);
        }
        if (!redFlagConfig.processSickness) {
            excluded = excluded.concat([28, 29]);
        }
    }
    console.log(`Returning excluded red flag types for org ${org.uid}, types: ${JSON.stringify(excluded)}`);
    return excluded;
}

export const getExcludedRedFlagTypesForOrganization = async (orgId: string): Promise<Array<number>> => {
    const org: Organization = await db.collection(FirestoreCollection.Organizations)
        .doc(orgId)
        .get()
        .then((doc: FirebaseFirestore.DocumentSnapshot) => {
            if (doc.exists) {
                return doc.data() as Organization;
            } else {
                return undefined;
            }
        })
        .catch((err) => {
            console.error(`An error occurred when querying for organization ${orgId}`);

            return undefined;
        });
    
    return !!org ? getExcludedRedFlags(org) : [];
}