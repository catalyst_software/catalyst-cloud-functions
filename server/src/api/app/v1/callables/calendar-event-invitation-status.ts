import * as functions from 'firebase-functions'

import { db } from '../../../../shared/init/initialise-firebase';

import { UpdateCalendarEventInvitationMetadataDirective } from "../../../../models/calendar/interfaces/UpdateCalendarEventInvitationMetadataDirective";
import { ICalendarEvent } from "../../../../models/calendar/interfaces/ICalendarEvent";
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { Config } from './../../../../shared/init/config/config';

export const updateCalendarEventInvitationStatus = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)
            try {

                const {
                    attendeeUid,
                    eventUid,
                    status,
                } = <UpdateCalendarEventInvitationMetadataDirective>data
                console.log('validateReceipt - data', data)

                const eventQuerySnap = await db.collection(FirestoreCollection.CalendarEventRegistry).where('eventUid', '==', eventUid).get();

                if (eventQuerySnap.size) {
                    const res = eventQuerySnap.docs.map((eventDocSnap) => {

                        const theEvent = getDocumentFromSnapshot(eventDocSnap) as ICalendarEvent;

                        const theIndex = theEvent.attendees.findIndex((a) => a.uid === attendeeUid);
                        console.log('theIndex', theIndex)
                        if (theIndex >= 0) {
                            theEvent.attendees[theIndex].invitationStatus = status;

                            return eventDocSnap.ref.update({
                                attendees: theEvent.attendees
                            })
                        } else {
                            return Promise.all(res).then(() => false)
                        }

                    })

                    return Promise.all(res).then(() => true)

                } else {
                    return true
                }

            } catch (error) {

                console.error('An unanticipated exception occurred in calendarEventInvitationStatus', error)
                return {
                    message:
                        'An unanticipated exception occurred in calendarEventInvitationStatus',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in calendarEventInvitationStatus',
                        exception: error,
                    },
                    method: {
                        name: 'calendarEventInvitationStatus'
                    }
                }
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)


