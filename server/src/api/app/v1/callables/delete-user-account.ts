import * as functions from 'firebase-functions'
import { theMainApp } from '../../../../shared/init/initialise-firebase';

import { getAthleteDocRef } from '../../../../db/refs';
import { Config } from './../../../../shared/init/config/config';

export const deleteUserAccount = functions.https._onCallWithOptions(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('data => ', data)


            const { uid: athleteUID } = context.auth.token;

            return await theMainApp.auth().deleteUser(athleteUID)
                .then(async () => {
                    console.log('Successfully deleted user')
                    const athleteDocSnap = await getAthleteDocRef(athleteUID).get()

                    if (athleteDocSnap.exists) {
                        return await athleteDocSnap.ref.delete().then(() => true).catch((err) => err)
                    }
                    return true
                })
                .catch(function (error) {
                    console.log('Error deleting user:', error);

                    throw error
                });


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)
