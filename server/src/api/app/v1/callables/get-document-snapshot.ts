import { AllAggregateCollectionType } from '../../../../models/types/biometric-aggregate-types';
import { db } from '../../../../shared/init/initialise-firebase';
import { runQuery } from '../../../../db/ipscore/services/run-query';

export const getDocumentSnapshot = async (
    collection: AllAggregateCollectionType,
    documentId: string,
    loggerService: boolean,
    // isConnected: boolean
    t: FirebaseFirestore.Transaction
) => {

    const documentRef = db
        .collection(collection)
        .doc(documentId);

    const documentSnapshot = await runQuery(documentRef, collection, documentId, loggerService, t);

    return documentSnapshot;
};


