
import { firestore } from "firebase-admin";

import moment from "moment";
import { isArray, isUndefined } from 'lodash'

import { AthleteSubscription } from "../../../../models/athlete/interfaces/athlete-subscription";
import { getUserFirstName, getUserLastName, getUserFullName, getUserEmail } from "../../../utils/getUserFullName";
import { getOrgIdFromGroupId } from "../controllers/utils/getOrgIdFromGroupId";
import { getDocumentFromSnapshot } from "../../../../analytics/triggers/utils/get-document-from-snapshot";
import { getAthleteCollectionRef } from "../../../../db/refs";
import { Athlete } from "../../../../models/athlete/athlete.model";

import { groupByMap } from "../../../../shared/utils/groupByMap";
import { OrganizationIndicator } from "../../../../models/athlete/interfaces/athlete";
import { SubscriptionStatus } from "../../../../models/enums/enums.model";
import { FirestoreCollection } from "../../../../db/biometricEntries/enums/firestore-collections";
import { db } from "../../../../shared/init/initialise-firebase";
import { Organization } from "../../../../models/organization.model";
import { getFirebaseUsers } from "../controllers/utils/getFirebaseUsers";

export const composeWeeklyAllAthleteReportData = async (limit: number, orgData: Array<{
    name: string;
    orgID: string;
}>) => {

    let allUsers = [];
    allUsers = await getFirebaseUsers(allUsers);

    const athsWithoutGroups = [];
    const payloadIssueData = [];
    let validateSubscription = false;

    const subAthetes = await getAthleteCollectionRef()
        // .where('profile.subscription.type', '>', 0)
        // .where('uid', '==', 'ljSnZu2DRrOVW35AP7LBgxff4wj2')
        .limit(limit || 10)
        .get()
        .then((athQuerySnap) => {
            return Promise.all(athQuerySnap.docs.map(async (athQueryDoscSnap) => {
                if (athQueryDoscSnap.exists) {
                    const athlete = getDocumentFromSnapshot(athQueryDoscSnap) as Athlete;
                    if (!(athlete.groups || []).length) {
                        athsWithoutGroups.push(athlete);
                    }
                    const mainGroup = athlete.groups && athlete.groups.length
                        ? {
                            groupId: athlete.groups[0].groupId || 'UNKNOWN',
                            groupName: athlete.groups[0].groupName || 'UNKNOWN'
                        }
                        : {
                            groupId: 'UNKNOWN',
                            groupName: 'UNKNOWN'
                        };
                    const mainProgram = athlete.currentPrograms && athlete.currentPrograms.length
                        ? {
                            programName: athlete.currentPrograms[0].name || 'UNKNOWN',
                        }
                        : {
                            name: 'UNKNOWN',
                        };
                    // if (athlete.organizationId === undefined || athlete.organizationId === '') {
                    //     console.error('athlete.organizationId', athlete.organizationId)
                    //     athlete.organizationId = athlete.groups ? athlete.groups[0].groupId.split('-')[0] : 'UNKNOWN'
                    // }
                    let organizationId;
                    let organizationName = 'UNKNOWN';
                    if (!athlete.organizations || !athlete.organizations.length) {
                        console.error(`athlete.organizations not set for ${getUserFirstName(athlete)}`, athlete.uid);
                        organizationId = athlete.groups ? getOrgIdFromGroupId(athlete) : 'UNKNOWN';
                        if (!organizationId) {
                            organizationId = (<any>athlete).organizationId;
                        }
                        // TODO: ?!? Or handleted below?!
                        // athlete.organizations = 
                    }
                    else {
                        organizationId = athlete.organizations[0].organizationId;
                        organizationName = athlete.organizations[0].organizationName;
                    }
                    if (!athlete.profile.subscription) {
                        validateSubscription = true;
                        const issues = {
                            orgID: organizationId,
                            orgName: organizationName,
                            message: 'Athlete Profile Missing Subscription',
                            athlete
                        };
                        payloadIssueData.push({ ...issues });
                        athlete.profile.subscription = {
                            status: undefined,
                            type: undefined,
                            expirationDate: undefined,
                            isPrePaid: undefined
                        } as AthleteSubscription;
                    }
                    let lastEngagementDate: string | moment.Moment;
                    try {
                        lastEngagementDate = (athlete.currentIpScoreTracking && athlete.currentIpScoreTracking.dateTime) ? moment(athlete.currentIpScoreTracking.dateTime.toDate()) : 'UNKNOWN';
                    }
                    catch (error) {
                        lastEngagementDate = 'UNKNOWN';
                    }
                    const now = moment();
                    const daysSinceLastEngagement = typeof lastEngagementDate === 'string' ? lastEngagementDate : now.diff(lastEngagementDate, 'days', true);
                    const weeksSinceLastEngagement = typeof lastEngagementDate === 'string' ? lastEngagementDate : now.diff(lastEngagementDate, 'weeks', true);
                    const monthsSinceLastEngagement = typeof lastEngagementDate === 'string' ? lastEngagementDate : now.diff(lastEngagementDate, 'months', true);
                    const firestoreUser = allUsers && isArray(allUsers) ? allUsers.find((user) => user.uid === athlete.uid) : undefined;
                    if (firestoreUser) {
                        allUsers = allUsers.filter((user) => user.uid !== firestoreUser.uid);
                        if (!athlete.metadata.creationTimestamp) {
                            athlete.metadata.creationTimestamp = firestore.Timestamp.fromDate(moment(firestoreUser.metadata.creationTime).toDate());
                        }
                        try {
                            let expirationDate: firestore.Timestamp = undefined;
                            try {
                                expirationDate = athlete.profile.subscription.expirationDate ? firestore.Timestamp.fromDate(athlete.profile.subscription.expirationDate.toDate()) : undefined;
                            }
                            catch (error) {
                                const toDateError = (<{
                                    message: string;
                                }>error).message.indexOf('athlete.profile.subscription.expirationDate.toDate is not a function');
                                if (toDateError < 0) {
                                    console.log(error);
                                }
                            }
                            const theAthleteBits = {
                                uid: athlete.uid,
                                emailVerified: firestoreUser.emailVerified,
                                organizations: athlete.organizations,
                                organizationUID: organizationId,
                                organizationName: organizationName,
                                ...mainGroup,
                                firstName: getUserFirstName(athlete),
                                lastName: getUserLastName(athlete),
                                fullName: getUserFullName(athlete),
                                email: getUserEmail(athlete),
                                isPrePaid: athlete.profile.subscription.isPrePaid || undefined,
                                subscriptionStatus: athlete.profile.subscription.status,
                                subscriptionCommencementDate: athlete.profile.subscription.commencementDate,
                                subscriptionExpiry: expirationDate,
                                ...mainProgram,
                                runningIPScore: !isUndefined(athlete.runningTotalIpScore) ? athlete.runningTotalIpScore : 'UNKNOWN',
                                creationTimestamp: athlete.metadata.creationTimestamp,
                                daysSinceLastEngagement,
                                weeksSinceLastEngagement,
                                monthsSinceLastEngagement,
                                validateSubscription,
                                // currentPrograms: athlete.currentPrograms.map((prog) => {
                                //     return {
                                //         programGuid: prog.programGuid,
                                //         activeDate: prog.activeDate
                                //     }
                                // }),
                                errorMessage: undefined
                            };
                            return theAthleteBits;
                        }
                        catch (error) {
                            const theAthleteErrorBits = {
                                uid: athlete.uid,
                                emailVerified: firestoreUser.emailVerified,
                                organizations: athlete.organizations,
                                organizationUID: organizationId,
                                organizationName: organizationName,
                                ...mainGroup,
                                firstName: getUserFirstName(athlete),
                                lastName: getUserLastName(athlete),
                                fullName: getUserFullName(athlete),
                                email: getUserEmail(athlete),
                                isPrePaid: athlete.profile.subscription.isPrePaid || undefined,
                                subscriptionStatus: undefined,
                                subscriptionCommencementDate: undefined,
                                subscriptionExpiry: undefined,
                                ...mainProgram,
                                runningIPScore: !isUndefined(athlete.runningTotalIpScore) ? athlete.runningTotalIpScore : 'UNKNOWN',
                                creationTimestamp: athlete.metadata.creationTimestamp,
                                daysSinceLastEngagement,
                                weeksSinceLastEngagement,
                                monthsSinceLastEngagement,
                                validateSubscription,
                                // currentPrograms: athlete.currentPrograms.map((prog) => {
                                //     return {
                                //         programGuid: prog.programGuid,
                                //         activeDate: prog.activeDate
                                //     }
                                // }),
                                errorMessage: error
                            };
                            return theAthleteErrorBits;
                        }
                    }
                    else {
                        console.log(`------- No Firebase User found for athlete ${getUserFirstName(athlete)} with UID`, athlete.uid);
                        // const athleteUID = athlete.uid
                        // const purgeAggregateResults = await purgeAggregateDocuments(athleteUID).catch((purgeAggregateDocumentsErr) => {
                        //     removeUndefinedProps(purgeAggregateDocumentsErr)
                        //     console.error('Error Purging Aggregate Documents', purgeAggregateDocumentsErr)
                        // })
                        // if (purgeAggregateResults) {
                        //     removeUndefinedProps(purgeAggregateResults)
                        //     console.log('Aggregate Documents Purged', purgeAggregateResults)
                        // }
                        // const purgeAthleteCollectionsResults = await cleanUpAthleteRelatedDocuments(athleteUID).catch((purgeAthleteCollectionsErr) => {
                        //     removeUndefinedProps(purgeAthleteCollectionsErr)
                        //     console.error('Error Purging Athlete Documents', purgeAthleteCollectionsErr)
                        // })
                        // if (purgeAthleteCollectionsResults) {
                        //     removeUndefinedProps(purgeAthleteCollectionsResults)
                        //     console.log('Athlete Document and Collections Purged', purgeAthleteCollectionsResults)
                        // }
                        const errorMessage = `User Account Does Not Exist: ${athQueryDoscSnap.id}`;
                        console.error(errorMessage);

                        if (!athlete) {
                           const  errorMessage2 = `athlete iss null: ${athQueryDoscSnap.id}`;
                            console.error(errorMessage2, {athlete});
                            return {
                                uid: athQueryDoscSnap.id,
                                organizations: undefined,
                                organizationUID: undefined,
                                organizationName: undefined,
                                firstName: undefined,
                                lastName: undefined,
                                fullName: undefined,
                                email: undefined,
                                isPrePaid: undefined,
                                subscriptionStatus: undefined,
                                subscriptionCommencementDate: undefined,
                                subscriptionExpiry: undefined,
                                creationTimestamp: undefined,
                                daysSinceLastEngagement: undefined,
                                weeksSinceLastEngagement: undefined,
                                monthsSinceLastEngagement: undefined,
                                validateSubscription: undefined,
                                errorMessage: errorMessage2
                            };
                        } else {
                            return {
                                uid: athlete.uid,
                                organizations: athlete.organizations,
                                organizationUID: athlete.organizationId,
                                organizationName,
                                firstName: getUserFirstName(athlete),
                                lastName: getUserLastName(athlete),
                                fullName: getUserFullName(athlete),
                                email: getUserEmail(athlete),
                                isPrePaid: athlete.profile.subscription.isPrePaid || undefined,
                                subscriptionStatus: undefined,
                                subscriptionCommencementDate: undefined,
                                subscriptionExpiry: undefined,
                                creationTimestamp: athlete.metadata ? athlete.metadata.creationTimestamp : undefined,
                                daysSinceLastEngagement,
                                weeksSinceLastEngagement,
                                monthsSinceLastEngagement,
                                validateSubscription,
                                errorMessage
                            };
                        }
                       
                    }
                }
                else {
                    const errorMessage = `Unable to find Athlete Doc. ID: ${athQueryDoscSnap.id}`;
                    console.error(errorMessage);
                    return {
                        uid: undefined,
                        organizations: undefined,
                        organizationUID: undefined,
                        organizationName: undefined,
                        firstName: undefined,
                        lastName: undefined,
                        fullName: undefined,
                        email: undefined,
                        isPrePaid: undefined,
                        subscriptionStatus: undefined,
                        subscriptionCommencementDate: undefined,
                        subscriptionExpiry: undefined,
                        creationTimestamp: undefined,
                        daysSinceLastEngagement: undefined,
                        weeksSinceLastEngagement: undefined,
                        monthsSinceLastEngagement: undefined,
                        validateSubscription: undefined,
                        errorMessage
                    };
                }
            }));
        });
    const groupedOrgsDocs = groupByMap(subAthetes.filter((theAth) => theAth.organizationUID !== undefined && theAth.organizationUID !== ''), (athlete: {
        organizationUID: string;
    }) => athlete.organizationUID) as Map<string, Array<{
        uid: string;
        organizations: OrganizationIndicator;
        organizationUID: string;
        organizationName: string;
        firstName: string;
        lastName: string;
        fullName: string;
        email: string;
        isPrePaid: string;
        subscriptionStatus: SubscriptionStatus;
        subscriptionCommencementDate: string;
        subscriptionExipry: firestore.Timestamp;
        creationTimestamp: firestore.Timestamp;
        daysSinceLastEngagement: string | number;
        weeksSinceLastEngagement: string | number;
        errorMessage: string;
    }>>;
    groupedOrgsDocs.forEach(async (athletes, orgID) => {
        orgData.push({
            name: undefined,
            orgID: orgID
        });
    });
    const orgDocsRefs = new Array<FirebaseFirestore.DocumentReference>();
    // Set up all the doc references for the the query to grab the groups by uid
    orgData.forEach((orgBlob) => {
        const ref = db
            .collection(FirestoreCollection.Organizations)
            .doc(orgBlob.orgID);
        orgDocsRefs.push(ref);
    });
    const theOrgs = await db.getAll(...orgDocsRefs)
        .then((orgDocSnaps: FirebaseFirestore.DocumentSnapshot[]) => {
            return orgDocSnaps.filter((docSnap) => docSnap.exists).map(orgDocSnap => getDocumentFromSnapshot(orgDocSnap) as Organization);
        });
    const payloadData = {
        theAthletes: [],
        athletesWithErrors: [],
        athsWithoutGroups: athsWithoutGroups.map((e) => {
            return {
                ...e,
                currentIpScoreTracking: {
                    ...e.currentIpScoreTracking,
                    dateTime: e.currentIpScoreTracking && e.currentIpScoreTracking.dateTime? e.currentIpScoreTracking.dateTime.toDate() : undefined
                },
                metadata: {
                    ...e.metadata,
                    creationTimestamp: e.metadata && e.metadata.creationTimestamp ? e.metadata.creationTimestamp.toDate() : undefined
                }
            };
        })
    };
    const updatedResponse = await Promise.all(orgData.map(async (orgDataBlob) => {
        const { orgID: organID } = orgDataBlob;
        let orgID = organID || 'NONE';
        const filteredAthletes = subAthetes.filter((ath) => !ath.errorMessage).filter((ath) => ath.organizationUID && ath.organizationUID === orgID);
        const listResults = await Promise.all(filteredAthletes.map(async (athlete) => {
            let updateAthlete = false;
            const theOrg = theOrgs.find(org => org.uid === athlete.organizationUID);
            let orgName = 'NONE';
            if (theOrg) {
                const { name, uid } = theOrg;
                orgID = uid;
                orgName = name;
                orgDataBlob.name = orgName;
            }
            let issues = subAthetes.filter((ath) => ath.errorMessage).map(issueAthlete => {
                return {
                    orgID,
                    orgName,
                    message: issueAthlete.errorMessage,
                    athlete: issueAthlete
                };
            });
            if (issues && issues.length) {
                payloadIssueData.push(...issues);
                issues = [];
            }
            updateAthlete = valiateCommencementDate(athlete, issues, orgID, orgName, theOrg);
            updateAthlete = validateIsPrePaid(athlete, issues, orgID, orgName, theOrg, payloadIssueData);
            updateAthlete = validateAthleteOrgsProp(athlete, theOrg);
            athlete.organizationName = theOrg ? theOrg.name : 'NONE';
            if (isUndefined(athlete.subscriptionStatus)) {
                if (athlete.subscriptionCommencementDate !== 'UNKNOWN' && athlete.subscriptionExpiry !== 'UNKNOWN') {
                    const startDate = moment(athlete.subscriptionCommencementDate);
                    const endDate = moment(athlete.subscriptionExpiry);
                    if (startDate < endDate) {
                        athlete.subscriptionStatus = SubscriptionStatus.ValidInitial;
                    }
                    else {
                        athlete.subscriptionStatus = SubscriptionStatus.Free;
                    }
                }
                else {
                    athlete.subscriptionStatus = SubscriptionStatus.Free;
                }
            }
            athlete.subscriptionStatus = SubscriptionStatus[athlete.subscriptionStatus] as any;
            // athlete.creationTimestamp = athlete.creationTimestamp.toDate()
            if (updateAthlete) {
                // const athleteDocRef = getAthleteDocRef(athlete.uid)
                // await athleteDocRef.update({
                //     'profile.subscription.expirationDate': athlete.subscriptionExpiry,
                //     'profile.subscription.commencementDate': athlete.subscriptionCommencementDate,
                //     'profile.subscription.isPrePaid': athlete.isPrePaid,
                //     'profile.subscription.status': subscriptionStatus,
                //     'organizations': athlete.organizations
                // })
            }
            if (issues && issues.length) {
                payloadIssueData.push(...issues);
                issues = [];
            }
            const { organizations, ...theRest } = athlete;
            return Promise.resolve({
                ...theRest,
                creationTimestamp: athlete.creationTimestamp.toDate()
            });
        })).catch((err) => {
            console.log(err);
            throw err;
        });
        console.log(listResults.length);
        const returnResults = {
            theAthletes: listResults,
            athletesWithErrors: subAthetes.filter((ath) => ath.errorMessage).filter((ath) => ath.organizationUID && ath.organizationUID === orgID)
        };
        return returnResults;
    }));
    updatedResponse.forEach((athletes) => {
        payloadData.theAthletes = payloadData.theAthletes.concat(athletes.theAthletes);
        payloadData.athletesWithErrors = payloadData.athletesWithErrors.concat(athletes.athletesWithErrors);
    });
    return { payloadData, allUsers };
};


function validateAthleteOrgsProp(athlete: {
    uid: string; organizations: OrganizationIndicator[]; organizationUID: any; organizationName: any; firstName: string; lastName: string; fullName: string; email: string; isPrePaid: true; subscriptionStatus: any; //athlete.profile.subscription.status,
    subscriptionCommencementDate: any; //athlete.profile.subscription.commencementDate,
    subscriptionExpiry: any; //athlete.profile.subscription.expirationDate
    creationTimestamp: firestore.Timestamp; daysSinceLastEngagement: string | number; weeksSinceLastEngagement: string | number; monthsSinceLastEngagement: string | number; validateSubscription: boolean;
    // currentPrograms: athlete.currentPrograms.map((prog) => {
    //     return {
    //         programGuid: prog.programGuid,
    //         activeDate: prog.activeDate
    //     }
    // }),
    errorMessage: any;
}, theOrg: Organization) {
    let updateAthlete = false;

    if (!athlete.organizations && theOrg) {
        updateAthlete = true;
        athlete.organizations = [{
            organizationId: theOrg.uid,
            organizationName: theOrg.name,
            organizationCode: theOrg.organizationCode,
            active: true,
            joinDate: firestore.Timestamp.now()
        }];
    }
    return updateAthlete;
}

function valiateCommencementDate(athlete: {
    uid: string; organizations: OrganizationIndicator[]; organizationUID: any; organizationName: any; firstName: string; lastName: string; fullName: string; email: string; isPrePaid: true; subscriptionStatus: any; //athlete.profile.subscription.status,
    subscriptionCommencementDate: any; //athlete.profile.subscription.commencementDate,
    subscriptionExpiry: any; //athlete.profile.subscription.expirationDate
    creationTimestamp: firestore.Timestamp; daysSinceLastEngagement: string | number; weeksSinceLastEngagement: string | number; monthsSinceLastEngagement: string | number; validateSubscription: boolean;
    // currentPrograms: athlete.currentPrograms.map((prog) => {
    //     return {
    //         programGuid: prog.programGuid,
    //         activeDate: prog.activeDate
    //     }
    // }),
    errorMessage: any;
}, issues: {
    orgID: string; orgName: string; message: any; athlete: {
        uid: string; organizations: OrganizationIndicator[]; organizationUID: any; organizationName: any; firstName: string; lastName: string; fullName: string; email: string; isPrePaid: true; subscriptionStatus: any; //athlete.profile.subscription.status,
        subscriptionCommencementDate: any; //athlete.profile.subscription.commencementDate,
        subscriptionExpiry: any; //athlete.profile.subscription.expirationDate
        creationTimestamp: firestore.Timestamp; daysSinceLastEngagement: string | number; weeksSinceLastEngagement: string | number; monthsSinceLastEngagement: string | number; validateSubscription: boolean;
        // currentPrograms: athlete.currentPrograms.map((prog) => {
        //     return {
        //         programGuid: prog.programGuid,
        //         activeDate: prog.activeDate
        //     }
        // }),
        errorMessage: any;
    };
}[], orgID: string, orgName: string, theOrg: Organization) {
    let commencementDate: any = 'UNKNOWN';
    let updateAthlete = false;
    try {


        commencementDate = athlete.subscriptionCommencementDate ? athlete.subscriptionCommencementDate.toDate() : 'UNKNOWN';
        if (commencementDate === 'UNKNOWN') {
            updateAthlete = true;
            issues.push({
                orgID,
                orgName,
                message: 'Athlete athlete.subscriptionCommencementDate not set',
                athlete
            });
            // commencementDate = athlete.creationTimestamp ? athlete.creationTimestamp.toDate() : 'UNKNOWN'
        }
        // if (commencementDate === 'UNKNOWN') {
        //     debugger
        // }
    }
    catch (error) {
        issues.push({
            orgID,
            orgName,
            message: 'EXCEPTION Athlete athlete.subscriptionCommencementDate not set => ' + JSON.stringify(error),
            athlete
        });
        updateAthlete = true;
        // commencementDate = athlete.creationTimestamp ? athlete.creationTimestamp.toDate() : 'UNKNOWN'
        // if (!commencementDate || commencementDate === 'UNKNOWN') {
        //     debugger
        // }
    }
    athlete.subscriptionCommencementDate = commencementDate;
    let expiryDate: any = 'UNKNOWN';
    try {
        expiryDate = athlete.subscriptionExpiry ? athlete.subscriptionExpiry.toDate() : undefined; // 'UNKNOWN'
        if (!expiryDate) {
            updateAthlete = true;
            const subscriptionStartDate = athlete.subscriptionCommencementDate;
            const subscriptionLengthInMonths = theOrg && theOrg.onboardingConfiguration && theOrg.onboardingConfiguration.prePaidNumberOfMonths ? theOrg.onboardingConfiguration.prePaidNumberOfMonths : undefined;
            try {
                // TODO: What to do here?! subscriptionLengthInMonths not defined?
                expiryDate = (subscriptionLengthInMonths !== undefined && subscriptionLengthInMonths > 0 && subscriptionStartDate !== 'UNKNOWN')
                    ? moment(subscriptionStartDate).clone().add(subscriptionLengthInMonths || 0, 'months').toDate()
                    : 'UNKNOWN';
                if (!expiryDate || expiryDate === 'UNKNOWN') {
                    issues.push({
                        orgID,
                        orgName,
                        message: 'Athlete athlete.subscriptionExpiry not set',
                        athlete
                    });
                }
            }
            catch (error) {
                debugger;
            }
        }
    }
    catch (error) {
        issues.push({
            orgID,
            orgName,
            message: 'EXCEPTION Athlete athlete.subscriptionExpiry not set => ' + JSON.stringify(error),
            athlete
        });
        try {
            if (commencementDate && commencementDate !== 'UNKNOWN') {
                updateAthlete = true;
                const subscriptionStartDate = commencementDate;
                const subscriptionLengthInMonths = theOrg && theOrg.onboardingConfiguration ? theOrg.onboardingConfiguration.prePaidNumberOfMonths : 0;
                expiryDate = moment(subscriptionStartDate).clone().add(subscriptionLengthInMonths || 0, 'months').toDate() || 'UNKNOWN';
            }
        }
        catch (error) {
            issues.push({
                orgID,
                orgName,
                message: 'EXCEPTION Athlete athlete.subscriptionExpiry unable to calculate using prePaidNumberOfMonths => ' + JSON.stringify(error),
                athlete
            });
        }
    }
    athlete.subscriptionExpiry = expiryDate;
    return updateAthlete;
}

function validateIsPrePaid(athlete: {
    uid: string; organizations: OrganizationIndicator[]; organizationUID: any; organizationName: any; firstName: string; lastName: string; fullName: string; email: string; isPrePaid: true; subscriptionStatus: any; //athlete.profile.subscription.status,
    subscriptionCommencementDate: any; //athlete.profile.subscription.commencementDate,
    subscriptionExpiry: any; //athlete.profile.subscription.expirationDate
    creationTimestamp: firestore.Timestamp; daysSinceLastEngagement: string | number; weeksSinceLastEngagement: string | number; monthsSinceLastEngagement: string | number; validateSubscription: boolean;
    // currentPrograms: athlete.currentPrograms.map((prog) => {
    //     return {
    //         programGuid: prog.programGuid,
    //         activeDate: prog.activeDate
    //     }
    // }),
    errorMessage: any;
}, issues: {
    orgID: string; orgName: string; message: any; athlete: {
        uid: string; organizations: OrganizationIndicator[]; organizationUID: any; organizationName: any; firstName: string; lastName: string; fullName: string; email: string; isPrePaid: true; subscriptionStatus: any; //athlete.profile.subscription.status,
        subscriptionCommencementDate: any; //athlete.profile.subscription.commencementDate,
        subscriptionExpiry: any; //athlete.profile.subscription.expirationDate
        creationTimestamp: firestore.Timestamp; daysSinceLastEngagement: string | number; weeksSinceLastEngagement: string | number; monthsSinceLastEngagement: string | number; validateSubscription: boolean;
        // currentPrograms: athlete.currentPrograms.map((prog) => {
        //     return {
        //         programGuid: prog.programGuid,
        //         activeDate: prog.activeDate
        //     }
        // }),
        errorMessage: any;
    };
}[], orgID: string, orgName: string, theOrg: Organization, payloadIssueData: any[]) {
    let isPrePaid: any = athlete.isPrePaid;
    let updateAthlete = false
    if ((isPrePaid === undefined || isPrePaid === null)) {
        updateAthlete = true;
        issues.push({
            orgID,
            orgName,
            message: 'isPrePaid not set on Athlete',
            athlete
        });
        const orgIsPrepaid = theOrg && theOrg.onboardingConfiguration && (theOrg.onboardingConfiguration.isPrePaid !== (undefined || null)) ? theOrg.onboardingConfiguration.isPrePaid : undefined;
        if (orgIsPrepaid !== undefined) {
            isPrePaid = orgIsPrepaid;
        }
        else {
            issues.push({
                orgID,
                orgName,
                message: 'isPrePaid not set on Org',
                athlete
            });
            isPrePaid = false;
            payloadIssueData.push(...issues);
        }
    }
    athlete.isPrePaid = isPrePaid;
    return updateAthlete;
}