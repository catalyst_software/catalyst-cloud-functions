import * as functions from 'firebase-functions'
import moment from 'moment';

import { firestore } from 'firebase-admin';
import { IpScoreTracking } from '../../../../db/ipscore/models/documents/tracking/ip-score-tracking.interface';
import { runGroupIPScoreAggergation } from './runGroupIPScoreAggergation';
import { updateAthleteIndicatorsIPScoreOnGroups } from './utils/update-athlete-indicators-ipscore-on-group';
import { Config } from './../../../../shared/init/config/config';

export interface IpScoreGroupAggregationDirective {
    // athlete: Athlete;
    groupIds: Array<string>;
    currentIpScoreTracking?: IpScoreTracking;
    utcOffset: number;
    commands: Array<IpScoreGroupAggregationCommand>;
}

export interface ObjectCacheItem {
    cacheId: number;
    cacheCreationTimestamp: Date;
    cacheUpdateTimestamp: Date;
    cacheObjectType: string;
}

export interface IpScoreGroupAggregationCommand extends ObjectCacheItem {
    runDate: string;
    paths: Array<string>;
}


export interface ObjectCacheItem {
    cacheId: number;
    cacheCreationTimestamp: Date;
    cacheUpdateTimestamp: Date;
    cacheObjectType: string;
}


export const setIPScoreTracking = (command: IpScoreGroupAggregationCommand, athlete: {
    includeGroupAggregation: boolean;
    currentIpScoreTracking: {
        dateTime: firestore.Timestamp;
        directive: {
            paths: any[];
        };
    };
}) => {

    debugger;
    const runDateTimestamp = firestore.Timestamp.fromDate(moment(command.runDate).toDate());

    const { currentIpScoreTracking } = athlete

    currentIpScoreTracking.dateTime = runDateTimestamp;
    athlete.currentIpScoreTracking.dateTime = runDateTimestamp;

    athlete.currentIpScoreTracking.directive.paths = command.paths;
}


export const runGroupIpScoreAggregation = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            const ipScoreGroupAggregationDirective = <IpScoreGroupAggregationDirective>data
            console.log('validateReceipt - data', data)


            if (ipScoreGroupAggregationDirective) {

                console.log('ipScoreGroupAggregationDirective', ipScoreGroupAggregationDirective)
                console.log('commands', JSON.stringify(ipScoreGroupAggregationDirective.commands))

                // const transactionResult = await db.runTransaction(async (t: FirebaseFirestore.Transaction) => {

                const t = undefined

                const { uid: athleteUID } = context.auth

                const { groupIds, utcOffset } = ipScoreGroupAggregationDirective

                console.log('athleteUID', athleteUID)
                console.log('running updateAthleteIndicatorsIPScoreOnGroups')
                const allTheResults = await updateAthleteIndicatorsIPScoreOnGroups(groupIds, utcOffset, athleteUID).then(async (ipScoreUpdateResults) => {
                    console.log('=================>>>>> Athlete indicators updated')
                    // const allResults =
                    await runGroupIPScoreAggergation(t, ipScoreGroupAggregationDirective).catch(err => {
                        console.log('Transaction failure:', err);
                        return false
                    });

                    return true // allResults
                })

                return allTheResults

                // }).then((results) => {
                //     console.log('Transaction success!', results);
                //     return true;
                // }).catch(err => {
                //     console.log('Transaction failure:', err);
                //     return false
                // });

                // return transactionResult


            } else {
                console.error('No document to process')
                return false
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)
