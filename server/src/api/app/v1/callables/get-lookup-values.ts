import { Athlete } from './../../../../models/athlete/interfaces/athlete';
import * as functions from 'firebase-functions';
import { Client, QueryResult } from 'pg';

import { initialiseCloudSql } from "../../../../shared/init/initialise-cloud-sql";

const cors = require('cors')({ origin: true });

export interface RequestCustomTokenPayload {
    authToken: string;
}
export interface AthleteSQLModel {
    id: number;
    first_name: string;
    last_name: string;
    full_name: string;
    email: string;
    image_url: string;
    organization_id: string;
    uid: string;
}

export interface AthleteLookupValue {
    id: number;
    uid: string;
    firstName: string;
    lastName: string;
    fullName: string;
    email: string;
    imageUrl: string;
    organizationId: string;
}

export const cleanString = (input: string): string => {
    return input.replace('\'', '\'\'').replace('"', '').trim()
}

export const addAthleteLookupRecord = async (athlete: Athlete, pg: Client): Promise<QueryResult<any>> => {
    const firstName = (athlete.profile && athlete.profile.firstName) ? cleanString(athlete.profile.firstName) : '';
    const lastName = (athlete.profile && athlete.profile.lastName) ? cleanString(athlete.profile.lastName) : '';
    const email = (athlete.profile && athlete.profile.email) ? cleanString(athlete.profile.email) : '';
    const o = {
        uid: athlete.uid,
        firstName,
        lastName,
        fullName: `${firstName} ${lastName}`,
        email,
        imageUrl: athlete.profile.imageUrl,
        organizationId: athlete.organizationId
    } as AthleteLookupValue;

    let sql = 'insert into athletes (uid, first_name, last_name, full_name, email, image_url, organization_id) '
    sql += `values('${o.uid}', '${o.firstName}', '${o.lastName}', '${o.fullName}', '${o.email}', '${o.imageUrl}', '${o.organizationId}')`;
    console.log(`Running postgres insert sql: ${sql}`);
    return pg.query(sql);
}

export const runGetLookupValues = async (tableName: string, fieldName: string, value: string): Promise<Array<AthleteLookupValue>> => {
    let db: Client;
    debugger
    try {
        db = await initialiseCloudSql();
        const val = value.toLowerCase()
        const sql = `select TOP 50 * from ${tableName} where lower(${fieldName}) LIKE '${val}%'`;
        const res = await db.query<AthleteSQLModel>(sql);
        return res.rows
            .map((i) => {
                return {

                    id: i.id,
                    uid: i.uid,
                    firstName: i.first_name,
                    lastName: i.last_name,
                    fullName: i.full_name,
                    email: i.email,
                    imageUrl: i.image_url,
                    organizationId: i.organization_id,
                }
            });
    }
    catch (err) {
        console.error(err.stack);

        return [];
    }
    finally {
        await db.end();
    }
}

export const getLookupValues = functions.https.onCall(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                );
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                };
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken);
            console.log('context.auth => ', context.auth);
            console.log('context.auth.token => ', context.auth.token);
            console.log('data => ', data);

            try {
                return runGetLookupValues(data.tableName, data.fieldName, data.val);
            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return {
                    message:
                        'An unanticipated exception occurred in getCustomAuthToken',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in getCustomAuthToken',
                        exception: exception,
                    },
                    method: {
                        name: 'getCustomAuthToken'
                    }
                }
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return error;
        }
    }
)