import { firestore } from 'firebase-admin';
import moment from 'moment';

import { AthleteBiometricEntryInterface } from '../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { getWeekNumber, getMonthStart } from '../../../../shared/utils/moment';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';

export const getWeeklyAggregateDocId = (entry: AthleteBiometricEntryInterface, uid: string, startTimestamp): string => {

    const entityId = uid ? uid : entry.uid ? entry.uid : entry.userId;

    const creationTimestamp = startTimestamp ? startTimestamp : entry.athleteCreationTimestamp;
    const dateTime = entry.dateTime;
    const lifestyleEntryType = +entry.lifestyleEntryType;
    const utcOffset = +entry.utcOffset || 0;

    // ID_%_WN / Weekly
    // ID_2_1 / ID_3

    let week = getWeekNumber(creationTimestamp, dateTime, utcOffset);
    if (week < 0) {
        week = 0;
    }

    const docId = `${entityId}_${lifestyleEntryType}_${week}`;

    return docId;
};
export const getMonthlyAggregateDocId = (entry: AthleteBiometricEntryInterface, uid: string): string => {

    const entityId = uid ? uid : entry.uid ? entry.uid : entry.userId;
    const dateTime = entry.dateTime;
    const lifestyleEntryType = +entry.lifestyleEntryType;
    const utcOffset = +entry.utcOffset || 0;

    // ID_%_MMYYYYY / Monthly
    // ID_1_012019 / ID_4_122018
    const docId = `${entityId}_${lifestyleEntryType}_${moment(getMonthStart(dateTime, utcOffset)).format('MMYYYY')}`;

    return docId;

};
export const getAggregateHistoryDocId = (entry: AthleteBiometricEntryInterface, uid: string): string => {
    // ID_%_WN / Weekly
    // ID_2_1 / ID_3

    const entityId = uid ? uid : entry.uid ? entry.uid : entry.userId;
    const lifestyleEntryType = +entry.lifestyleEntryType;

    const docId = `${entityId}_${lifestyleEntryType}`;

    return docId;
};

export const getWeeklyDocId = (
    entityId: string,
    creationTimestamp: firestore.Timestamp,
    dateTime: firestore.Timestamp,
    lifestyleEntryType: LifestyleEntryType,
    utcOffset: number,
): string => {
    // ID_%_WN / Weekly
    // ID_2_1 / ID_3
    const docId = `${entityId}_${lifestyleEntryType}_${getWeekNumber(creationTimestamp, dateTime, utcOffset)}`;

    return docId;
};

export const getMonthlyDocId = (
    entityId: string,
    dateTime: firestore.Timestamp,
    lifestyleEntryType: LifestyleEntryType,
    utcOffset: number,
    uid: string
): string => {
    // ID_%_MMYYYYY / Monthly
    // ID_1_012019 / ID_4_122018
    const docId = `${entityId}_${lifestyleEntryType}_${moment(getMonthStart(dateTime, utcOffset)).format('MMYYYY')}`;

    return docId;

};

export const getHistoryDocId = (
    entityId: string,
    lifestyleEntryType: number
): string => {
    // ID_%_WN / Weekly
    // ID_2_1 / ID_3

    const docId = `${entityId}_${lifestyleEntryType}`;

    return docId;
};
