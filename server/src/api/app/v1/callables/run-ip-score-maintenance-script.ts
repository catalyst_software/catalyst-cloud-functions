import * as functions from 'firebase-functions'

// import { resetAthletesIpScore } from './utils/reset-athletes-ip-score';

export const runIPScoreMaintenanceScript = functions.https.onCall(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request '
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context => ', context)

            return true
            // const { uid: athleteUID } = context.auth.token;

            // const { utcOffset }
            //     = <{ utcOffset: number }>data

            // const athleteWeeklyIpScoreDocs = await resetAthletesIpScore(athleteUID, utcOffset)

            // return athleteWeeklyIpScoreDocs

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }
)
