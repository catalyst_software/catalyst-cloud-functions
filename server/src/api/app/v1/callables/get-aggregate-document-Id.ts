
import { getAggregateDocID } from './get-aggregate-doc-id';
import { AggregateDirectiveType } from '../../../../models/types/biometric-aggregate-types';
import { AthleteBiometricEntryInterface } from '../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';

export const getAggregateDocumentID = (directive: AggregateDirectiveType, entry: AthleteBiometricEntryInterface, dateTime: FirebaseFirestore.Timestamp) => {

    const { athlete, collection, loggerService, group } = directive;

    const uid = group ? group.groupId : athlete.uid

    const creationTimestamp = group ? group.creationTimestamp : athlete.metadata.creationTimestamp

    const documentId = getAggregateDocID(collection, uid, creationTimestamp, entry, dateTime);

    if (documentId !== '') {
        if (loggerService) {
            console.info(`Doc Id for ${collection} Set as => ${documentId}`) // - Athlete UID -> ${athlete.uid}`);
        }

        return documentId;
    } else {

        console.error('Doc Id NOT Set');

        return undefined;
    }
};
