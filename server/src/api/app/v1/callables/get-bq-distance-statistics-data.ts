import * as functions from 'firebase-functions'
import cubejs from '@cubejs-client/core';
import moment from 'moment';
// tslint:disable-next-line: no-duplicate-imports
import { Filter, Query, ResultSet, TimeDimension } from '@cubejs-client/core';
import { getOlapJwtPackage, getSharedSystemConfiguration } from './utils/warehouse/olap/get-olap-jwt-package';
import { isArray } from 'lodash';
import { OlapGetStatisticsRequest } from './utils/warehouse/olap/interfaces/olap-get-statistics-request.interface';
import { DistanceStatisticsResponse } from './utils/warehouse/olap/interfaces/distance-statistics-response.interface';


export const getDistanceStatisticsFromCubeJs = async (request: OlapGetStatisticsRequest): Promise<DistanceStatisticsResponse> => {
    let results: ResultSet<any>;
    const config = await getSharedSystemConfiguration();
    const olapTokenPackage = await getOlapJwtPackage(request.requestorSecurityProfile, config);
    console.log(`OlapTokenPackage: ${JSON.stringify(olapTokenPackage)}`);

    const cubejsApi = cubejs(olapTokenPackage.token, {
        apiUrl: config.olap.apiUrl
    });

    // Total Distance Query
    let query = {
        measures: ['ScalarBiometricFact.totalValue'],
        dimensions: ['ScalarBiometricFact.segmentId'],
        filters: [{
            dimension: 'ScalarBiometricFact.segmentId',
            operator: 'equals',
            values: [request.requestorSecurityProfile.organizationId]
        } as Filter, {
            dimension: 'ScalarBiometricFact.athleteId',
            operator: 'notSet'
        } as Filter],
        timeDimensions: [{
            dimension: 'ScalarBiometricFact.created',
            dateRange: [request.startDate, request.endDate],
            granularity: request.granularity || 'day'
        } as TimeDimension],
        order: {
            'ScalarBiometricFact.totalValue': 'desc'
        }
    } as Query;

    const response = {
        startDate: request.startDate,
        endDate: request.endDate,
        requestorSecurityProfile: undefined,
        athleteDistance: 0,
        totalDistance: 0,
        numberOfParticipants: 0,
        numberOfCountries: 0
    } as unknown as DistanceStatisticsResponse;

    results = await cubejsApi.load(query);
    let rows = (results as any).loadResponse.data;
    response.totalDistance = isArray(rows) && rows.length ? rows[0] : 0;

    // Athlete Total Distance Query
    query = {
        measures: ['ScalarBiometricFact.totalValue'],
        dimensions: ['ScalarBiometricFact.athleteId'],
        filters: [{
            dimension: 'ScalarBiometricFact.athleteId',
            operator: 'equals',
            values: [request.requestorSecurityProfile.athleteId]
        } as Filter, {
            dimension: 'ScalarBiometricFact.segmentId',
            operator: 'notSet'
        }],
        timeDimensions: [{
            dimension: 'ScalarBiometricFact.created',
            dateRange: [request.startDate, request.endDate],
            granularity: request.granularity || 'day'
        } as TimeDimension]
    } as Query;

    results = await cubejsApi.load(query);
    rows = (results as any).loadResponse.data;
    response.athleteDistance = isArray(rows) && rows.length ? rows[0]['ScalarBiometricFact.totalValue'] : 0;

    // Athlete Overall Distance Query
    // const startDate = config.event.startDate;
    // endDate = config.event.endDate;
    // query = {
    //     measures: ['ScalarBiometricFact.totalValue'],
    //     dimensions: ['ScalarBiometricFact.athleteId','AthleteDim.firstName','AthleteDim.lastName','AthleteDim.imageUrl','AthleteDim.sport','AthleteDim.bio'],
    //     filters: [{
    //         dimension: 'ScalarBiometricFact.athleteId',
    //         operator: 'equals',
    //         values: [request.requestorSecurityProfile.athleteId]
    //     } as Filter, {
    //         dimension: 'ScalarBiometricFact.segmentId',
    //         operator: 'notSet'
    //     }],
    //     timeDimensions: [{
    //         dimension: 'ScalarBiometricFact.created',
    //         dateRange: [startDate, endDate],
    //         granularity: 'month'
    //     } as TimeDimension]
    // } as Query;

    // results = await cubejsApi.load(query);
    // rows = (results as any).loadResponse.data;
    // response.totalDistance = isArray(rows) && rows.length ? rows[0]['ScalarBiometricFact.totalValue'] : 0;
    
    // response.rank = await getAthleteDistanceRanking(request.requestorSecurityProfile.athleteId, request.startDate, request.endDate);
    response.requestorSecurityProfile = request.requestorSecurityProfile;
    return Promise.resolve(response);
}

export const getBqDistanceStatisticsData = functions.https.onCall(async (data, context) => {
    try {
        if (!context.auth.token.uid) {
            console.error(
                'Request not authorized. User must logged in to fulfill this request'
            )
            return {
                error:
                    'Request not authorized. User must logged in to fulfill this request ' +
                    data
            }
        }

        console.log('context.instanceIdToken => ', context.instanceIdToken);
        console.log('context.auth => ', context.auth);
        console.log('context.auth.token => ', context.auth.token);
        console.log('data => ', data);

        try {
            const request = <OlapGetStatisticsRequest>data;
            return await getDistanceStatisticsFromCubeJs(request);

        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return Promise.reject({
                message:
                    'An unanticipated exception occurred in getBqIpScoreData',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in getBqIpScoreData',
                    exception: exception,
                },
                method: {
                    name: 'getBqIpScoreData'
                }
            })
        }


    } catch (error) {
        console.error(
            '.........CATCH ERROR:',
            error
        )

        return Promise.reject(error)
    }
});

