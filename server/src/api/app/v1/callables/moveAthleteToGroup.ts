import moment from "moment";

import { ManageAthleteGroupRequest } from "../../../../models/report-model";
import { Group } from '../../../../models/group/interfaces/group';
import { getGroupDocRef, getAthleteDocRef, getOrgDocRef } from "../../../../db/refs";
// import { Athlete } from "../../../../models/athlete/athlete.model";
import { Athlete, OrganizationIndicator } from '../../../../models/athlete/interfaces/athlete';

import { firestore } from "firebase-admin";
import * as firebase_tools from 'firebase-tools'
import { Organization } from "../../../../models/organization.model";
import { getDocumentFromSnapshot } from "../../../../analytics/triggers/utils/get-document-from-snapshot";
import { AthleteIndicator } from "../../../../db/biometricEntries/well/interfaces/athlete-indicator";
import { db } from "../../../../shared/init/initialise-firebase";
import { FirestoreCollection } from "../../../../db/biometricEntries/enums/firestore-collections";
import { cloneDeep, isArray, differenceWith, isEqual } from "lodash";
import { purgeAthleteAggregateDocuments } from "../controllers/utils/purgeAggregateDocuments";
import { GroupIndicator } from '../../../../db/biometricEntries/well/interfaces/group-indicator';
import { getUniqueListBy } from "./utils/getUniqueListBy";
import { getQueryResults } from "./utils/big-query/get-query-results";
import { SegmentDimension } from "./utils/warehouse/segment-dimension.interface";
import { SegmentAthleteDimension } from "./utils/warehouse/segment-athlete-dimension.interface";
import { SegmentType } from "./utils/warehouse/segment-type.enum";
import { bigqueryClient } from "./utils/big-query/initBQ";
import * as crypto from 'crypto';
import { getParentOrganizationId } from "./utils/warehouse/get-athlete-org-segments";
import { SelfHealingTask } from '../../../../db/taskQueue/interfaces/self-healing-task.interface';
import { SelfHealingScheduleType, SelfHealingTaskWorkerType, ScheduledTaskStatusType } from "../../../../db/taskQueue/enums/scheduled-task.enums";
import { SelfHealingTaskPayload } from "../../../../db/taskQueue/interfaces/self-healing-task-payload.interface";
import { Config } from './../../../../shared/init/config/config';


const functions = require('firebase-functions');

const getSegmentExistsInBigQuery = async (segmentId: string): Promise<boolean> => {
    const datasetId = 'catalystDW';
    const tableId = 'segmentDim';
    const query = `select segment_id from ${datasetId}.${tableId} where segment_id = '${segmentId}'`;
    const rows = await getQueryResults<SegmentDimension>(query);

    return Promise.resolve((isArray(rows) && rows.length > 0));
  }

  const getSegmentAthleteExistsInBigQuery = async (segmentId: string, athleteId: string): Promise<boolean> => {
    const datasetId = 'catalystDW';
    const tableId = 'segmentAthleteDim';
    const query = `select * from ${datasetId}.${tableId} where segment_id = '${segmentId}' and athlete_id = '${athleteId}'`;
    const rows = await getQueryResults<SegmentDimension>(query);

    return Promise.resolve((isArray(rows) && rows.length > 0));
  }

  const getAddSegments = (newSegments: Array<SegmentDimension>, oldSegments: Array<SegmentDimension>): Array<SegmentDimension> => {
    // Returns all groups in new that are not in old
    return differenceWith(newSegments, oldSegments, isEqual);
  }

  const getDeleteSegments = (newSegments: Array<SegmentDimension>, oldSegments: Array<SegmentDimension>): Array<SegmentDimension> => {
    // Returns all groups in old that are not in new
    return differenceWith(oldSegments, newSegments, isEqual);
  }

  const manageAddSegments = async (athleteId: string, addSegments: Array<SegmentDimension>): Promise<void> => {
    const datasetId = 'catalystDW';
    const segmentTableId = 'segmentDim';
    const segmentAthleteTableId = 'segmentAthleteDim';

    const segmentRows: Array<SegmentDimension> = [];
    const segmentAthleteRows: Array<SegmentAthleteDimension> = [];

    for (const segment of addSegments) {
        if (!!segment) {
            let exists = await getSegmentExistsInBigQuery(segment.segment_id);
            
            if (!exists) {
                segmentRows.push(segment);
            }

            exists = await getSegmentAthleteExistsInBigQuery(segment.segment_id, athleteId);
            if (!exists) {
                segmentAthleteRows.push({
                    segment_id: segment.segment_id,
                    athlete_id: athleteId
                } as SegmentAthleteDimension);
            }
        }
    }

    let theRes;
    if (segmentRows.length) {
        theRes = await bigqueryClient
                .dataset(datasetId)
                .table(segmentTableId)
                .insert(segmentRows)
                .then((rowCount) => {
                    return { 
                        rowCount, 
                        errors: undefined }
                })
                .catch((e) => {
                    console.log(e.errors);
                    return {
                        rowCount: undefined,
                        errors: e.errors
                    }
                });
    }

    if (segmentAthleteRows.length) {
        theRes = await bigqueryClient
                .dataset(datasetId)
                .table(segmentAthleteTableId)
                .insert(segmentAthleteRows)
                .then((rowCount) => {
                    return { 
                        rowCount, 
                        errors: undefined }
                })
                .catch((e) => {
                    console.log(e.errors);
                    return {
                        rowCount: undefined,
                        errors: e.errors
                    }
                });
    }

    return Promise.resolve();
  }

  const manageDeleteSegments = async (athleteId: string, deleteSegments: Array<SegmentDimension>): Promise<void> => {
    const datasetId = 'catalystDW';
    const tableId = 'segmentAthleteDim';

    for (const segment of deleteSegments) {
        if (!!segment) {
            const sqlQuery = `DELETE FROM ${datasetId}.${tableId} WHERE segment_id = '${segment.segment_id}' and athlete_id = '${athleteId}';`;
            // Query options list: https://cloud.google.com/bigquery/docs/reference/v2/jobs/query
            const options = {
                query: sqlQuery,
                timeoutMs: 100000, // Time out after 100 seconds.
                useLegacySql: false, // Use standard SQL syntax for queries.
            };

            const theRes = await bigqueryClient
                    .query(options)
                    .then(() => {
                        return { 
                            success: true, 
                            error: undefined }
                    })
                    .catch(async (e) => {
                        console.log(e);
                        const success = await db.collection(FirestoreCollection.SelfHealingTaskQueue)
                                .add({
                                        creationTimestamp: new Date(),
                                        executionDelayInMinutes: 90,
                                        scheduleType: SelfHealingScheduleType.EveryTwoHour,
                                        worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                        payload: {
                                            worker: SelfHealingTaskWorkerType.BigQueryDataManipulationReattempt,
                                            sql: sqlQuery
                                        } as SelfHealingTaskPayload,
                                        status: ScheduledTaskStatusType.Scheduled
                                    } as SelfHealingTask)
                                .then(() => {
                                    console.log('BigQueryDataManipulationReattempt self healing task successfully added to firestore.');
                                    return true;
                                })
                                .catch((err) => {
                                    console.error(`An error occurred when attempting to to add BigQueryDataManipulationReattempt self healing task to firestore.  Error: ${err}`);
                                    return false;
                                });

                        return {
                            success: false, 
                            error: e
                        }
                    });
        }
    }

    return Promise.resolve();
  }

  const manageAthleteSegments = async (oldAthlete: Athlete, newAthlete: Athlete): Promise<void> => {
    const newOrgs = newAthlete.organizations;
    const oldOrgs = oldAthlete.organizations;

    const newSegments: Array<SegmentDimension> = [];
    const oldSegments: Array<SegmentDimension> = [];

    let parentOrgId = '';
    if (isArray(newOrgs)) {
        const newOrg = newOrgs.find((o: OrganizationIndicator) => {
            return !!o && o.organizationId === newAthlete.organizationId;
        });
        
        if (newOrg) {
            parentOrgId = await getParentOrganizationId(newOrg.organizationId);
            newSegments.push({
                segment_id: newOrg.organizationId,
                parent_segment_id: parentOrgId,
                segment_name: newOrg.organizationName,
                segment_type: SegmentType.Organization
            } as SegmentDimension);
        }
    }

    newOrgs.forEach((o: OrganizationIndicator) => {
        if (isArray(o.groups)) {
            o.groups.forEach((g: GroupIndicator) => {
                newSegments.push({
                    segment_id: g.groupId,
                    parent_segment_id: g.organizationId,
                    segment_name: g.groupName,
                    segment_type: SegmentType.Group
                } as SegmentDimension);
            })
        }
    });

    if (isArray(oldOrgs)) {
        const oldOrg = oldOrgs.find((o: OrganizationIndicator) => {
            return !!o && o.organizationId === oldAthlete.organizationId;
        });

        parentOrgId = '';
        if (oldOrg) {
            parentOrgId = await getParentOrganizationId(oldOrg.organizationId);
            oldSegments.push({
                segment_id: oldOrg.organizationId,
                parent_segment_id: parentOrgId,
                segment_name: oldOrg.organizationName,
                segment_type: SegmentType.Organization
            } as SegmentDimension);
        }
    }

    oldOrgs.forEach((o: OrganizationIndicator) => {
        if (isArray(o.groups)) {
            o.groups.forEach((g: GroupIndicator) => {
                oldSegments.push({
                    segment_id: g.groupId,
                    parent_segment_id: g.organizationId,
                    segment_name: g.groupName,
                    segment_type: SegmentType.Group
                } as SegmentDimension);
            })
        }
    });
    
    const addSegments = getAddSegments(newSegments, oldSegments);
    const deleteSegments = getDeleteSegments(newSegments, oldSegments);

    await manageAddSegments(newAthlete.uid, addSegments);
    await manageDeleteSegments(newAthlete.uid, deleteSegments);
    
    return Promise.resolve();
  }

export const doesAthleteExistInGroup = async (groupId: string, athleteId: string): Promise<boolean> => {
    const path = `/groups/${groupId}/athletes/${athleteId}`;
    return db.doc(path)
             .get()
             .then((doc) => {
                return doc.exists;
             })
             .catch((err) => {
                console.error(`An error occurred when retrieving athlete document ${athleteId} from group ${groupId}. Error: ${err}`);
                return false;
             });
}

export const updateAthlete = async (group: Group, athlete: Athlete, oldAthlete: Athlete, atheleteRef: firestore.DocumentReference,
  isCoach?: boolean, addToGroup?: boolean, orgId?: string, orgName?: string) => {

  const groupIndicator: GroupIndicator = {
    uid: group.uid,
    logoUrl: group.logoUrl,
    creationTimestamp: group.creationTimestamp,
    groupName: group.groupName,
    groupId: group.groupId,
    organizationId: orgId || group.organizationId,
    organizationName: orgName || group.organizationName
  };

  // if (!athlete.isCoach) {
  // if (!addToGroup) {
  //     removeAllAthleteIndicators(athlete, orgId);
  // }

  // TODO: Verify
  // Filter Groups in the Organization to which the user belongs.
  // only pertains to switching a user to a diffirent Organization
  // let groups = athlete.groups; // .filter(grp => grp.organizationId === athlete.organizationId);


  const clonedGroups = cloneDeep(athlete.groups);
  let athleteGroups = getUniqueListBy(clonedGroups, 'groupId') as GroupIndicator[]

  if (athleteGroups.length !== clonedGroups.length) {
    console.warn('dup groups removed', {
      groupAthletesWithDups: clonedGroups.map((a) => {
        return { uid: a.uid, name: a.groupName }
      }),
      groupAthletesCleaned: athleteGroups.map((a) => {
        return { uid: a.uid, name: a.groupName }
      })
    })
  }

  // isCoach || 
  if (addToGroup) {
    const isInGroup = athleteGroups.find((grp) => grp.groupId === groupIndicator.uid);
    if (!isInGroup)
      athleteGroups.push(groupIndicator)
  } else {
    // const athleteGroups = athlete.groups.filter(grp => grp.organizationId ? grp.organizationId : grp.groupId.split('-')[0] !== athlete.organizationId);
    // athleteGroups.push(groupIndicator)
    // groups = athleteGroups
    athleteGroups = [groupIndicator]
  }

  const athleteUpdate = {
    ...athlete,
    groups: athleteGroups.map((grp) => {
      const { uid, athletes, ...theGroup } = grp;
      return theGroup
    })
  };

  if (!!oldAthlete) {
    const newHash = crypto.createHash('md5').update(JSON.stringify(athleteUpdate.organizations)).digest("hex");
    const oldHash = crypto.createHash('md5').update(JSON.stringify(oldAthlete.organizations)).digest("hex");

    if (newHash !== oldHash) {
        await manageAthleteSegments(oldAthlete, athleteUpdate);
    }
  }

  return atheleteRef.update(athleteUpdate)
};

// 
export const removeAllAthleteIndicators = async (athlete: Athlete, groupUID: string, allowGroupDelete = false) => {

  const results = await db.collection(FirestoreCollection.Groups)
    .doc(groupUID).get()
    .then(async (groupSnapshot: firestore.DocumentSnapshot) => {
      if (groupSnapshot.exists) {
        const currentGrp = { uid: groupSnapshot.id, ...groupSnapshot.data() } as Group;
        let path = `/groups/${groupSnapshot.id}/athletes`;
        // if (currentGrp.groupId !== group.uid) {

        // const clonedAthletes = cloneDeep(currentGrp.athletes);
        // const groupAthletes = getUniqueListBy(clonedAthletes, 'uid') as AthleteIndicator[]

        // const retainedAthletes = groupAthletes.filter(currentAth => currentAth.uid !== athlete.uid);
        const groupAthletes = await db.collection(FirestoreCollection.Groups)
                                    .doc(groupUID)
                                    .collection('athletes')
                                    .get()
                                    .then((docs) => {
                                        const ln = docs.docs.length;
                                        console.log(`Retrieved ${ln} athlete documents for group ${groupUID}`);
                                        if (ln > 0) {
                                            return docs.docs.map((d) => {
                                                return { ...d.data(), uid: d.id };
                                            });
                                        } else {
                                            return [];
                                        }
                                    })
                                    .catch((err) => {
                                        console.error(`Error retrieved when querying for group ${groupUID} athletes.  Error: ${err}`);
                                        return [];
                                    });
        if (groupAthletes.length) {
            const retainedAthletes = groupAthletes.filter(currentAth => currentAth.uid !== athlete.uid);
            if (groupAthletes.length !== retainedAthletes.length && retainedAthletes.length > 0) {

                // return await groupSnapshot.ref.update({ ...currentGrp, athletes: retainedAthletes }).then(() => {
                //     return 'Athlete removed from Group';
                // });

                return await db.collection(FirestoreCollection.Groups)
                                .doc(groupUID)
                                .collection('athletes')
                                .doc(athlete.uid)
                                .delete()
                                .then((result) => {
                                    console.error(`Successfully deleted athlete ${athlete.uid} in group ${groupUID}.`);
                                    return;
                                })
                                .catch((err) => {
                                    console.error(`Error retrieved when deleting athlete ${athlete.uid} for group ${groupUID}.  Error: ${err}`);
                                    return err;
                                });
            }   
            else {

                const deleteGroup = retainedAthletes.length === 0 // && athlete.isCoach;
                const proceed = false

                // TRAVIS: THIS DELETION AND BACKUP IS ONLY FOR INSPIRE ONBOARDED USERS
                if (allowGroupDelete && deleteGroup && proceed) {

                    // INSPIRE ORG
                    return db.collection('removedEntities').doc(currentGrp.uid).set({
                        entityType: 'Group',
                        currentGrp,
                        deleteGroup
                    })
                    .then(async () => {

                        await purgeAthleteAggregateDocuments(groupSnapshot.id)
                    // TRAVIS: NEED TO DELETE THE athletes SUBCOLLECTION
                    //   return await groupSnapshot.ref.delete().then(async () => {

                    //     // await purgeAggregateDocuments(groupSnapshot.id)
                    //     // console.warn(`Org and Group Removed - Manual Cleanup Needed: Athlete UID: ${athlete.uid}, Group UID: ${groupSnapshot.id},  Org UID: ${orgId}`)
                    //     console.warn(`Group Removed: Athlete UID: ${athlete.uid}, Group UID: ${groupSnapshot.id}`)
                    //     return `Group removed: Athlete UID: ${athlete.uid}, Group UID: ${groupSnapshot.id}`;
                    //   });
                        path = `/groups/${groupSnapshot.id}`;
                        return firebase_tools.firestore
                            .delete(path, {
                                project: process.env.GCLOUD_PROJECT,
                                recursive: true,
                                yes: true,
                                token: functions.config().fb.token,
                            })
                            .then(() => {
                                console.warn(`Group Removed: Athlete UID: ${athlete.uid}, Group UID: ${groupSnapshot.id}`)
                                return `Group removed: Athlete UID: ${athlete.uid}, Group UID: ${groupSnapshot.id}`;
                            })
                            .catch((err) => {
                                console.error(`Error occured when executing recursive delete operation on path ${path}.  Error: ${err}`);
                                return err;
                            });
                    });


                } else {

                    // NOT INSPIRE - NEED TO DELETE ATHLETE FROM SUBCOLLECTION
                    // return await groupSnapshot.ref.update({ ...currentGrp, athletes: retainedAthletes }).then(() => {
                    // console.warn(`Athlete removed from Group - Manual Cleanup Needed: Athlete UID: ${athlete.uid}, Group UID: ${groupSnapshot.id}`)
                    // return `Athlete removed from Group - Manual Cleanup Needed: Athlete UID: ${athlete.uid}, Group UID: ${groupSnapshot.id}`;
                    // });

                    return await db.collection(FirestoreCollection.Groups)
                                .doc(groupUID)
                                .collection('athletes')
                                .doc(athlete.uid)
                                .delete()
                                .then((result) => {
                                    console.warn(`Athlete removed from Group - Manual Cleanup Needed: Athlete UID: ${athlete.uid}, Group UID: ${groupSnapshot.id}`)
                                    return `Athlete removed from Group - Manual Cleanup Needed: Athlete UID: ${athlete.uid}, Group UID: ${groupSnapshot.id}`;
                                })
                                .catch((err) => {
                                    console.error(`Error retrieved when deleting athlete ${athlete.uid} for group ${groupUID}.  Error: ${err}`);
                                    return err;
                                });
                }
            }
        } else {
            return 'No Athletes In Group';
        }
      }
      else {

        await purgeAthleteAggregateDocuments(groupSnapshot.id)
        return 'Group Not Found';
      }
    }
      // .then((writeResult: string) => {
      //     console.log(writeResult);
      //     return writeResult
      // });)
    )

  return results
}


export const executeMoveAthleteToGroup = async (userId: string, group: Group, moveUserOrg: boolean, allowGroupDelete: boolean, orgName?: string, orgUID?: string, organizationCode?: string,
  coachFlag?: boolean,
  firebaseMessagingIds?: string[]) => {

  const atheleteRef = getAthleteDocRef(userId);// getAthleteDocRef('5FqVZGNUqVV9rPpo48CtqPB8xYE3').get()
  
  let oldAthlete: Athlete;
  const updatedAthleteDoc = await atheleteRef.get()
    .then((athleteSnap) => {
      if (athleteSnap.exists) {

        const theAthlete = { ...athleteSnap.data(), uid: athleteSnap.id } as Athlete;
        oldAthlete = cloneDeep(theAthlete);
        debugger;
        // TODO: Test All this
        if (!theAthlete.organizations
          // && !theAthlete.organizations.length 
          // || theAthlete.profile.onboardingCode.toUpperCase() === 'INSPIRE'
        ) {
          debugger;
          theAthlete.organizations = [{
            organizationId: theAthlete.organizationId || orgUID,
            organizationName: orgName || theAthlete.profile.firstName + "'s Org",
            organizationCode: theAthlete.profile.onboardingCode.split('-')[0] || organizationCode || 'UNKOWN',
            joinDate: theAthlete.metadata.creationTimestamp,
            active: true
          }]
        }

        if (theAthlete.profile.onboardingCode.toUpperCase() === 'INSPIRE'
        ) {
          debugger;
          theAthlete.organizationId = theAthlete.organizations[0].organizationId || orgUID || 'UNKNOWN'
          theAthlete.organizations[0].organizationCode = 'INSPIRE'
          theAthlete.organizations[0].active = false
        }


        if (!theAthlete.groups) {
          theAthlete.groups = []
        }

        try {
          if (!theAthlete.metadata.creationTimestamp && theAthlete.profile.onboardingCode.toUpperCase() === 'INSPIRE') {
            theAthlete.metadata.creationTimestamp = theAthlete.groups[0].creationTimestamp
          }
        } catch (error) {
          console.error('Athlete Creation Timestamp Undefinded, setting to now', firestore.Timestamp.now().toDate())
          theAthlete.metadata.creationTimestamp = firestore.Timestamp.now()
        }

        return theAthlete

      } else {
        // console.error({ message: 'No ID supplied' })
        throw { error: `Can't find athlete with UID ${userId}` }
      }

    })



  // theAthlete.groups.forEach(group => group.organizationId = theAthlete.organizationId)
  // TODO: Verify
  // return theAthlete.organizationIds.map((orgId: string) => {

  // if ((!theAthlete.organizations) || (moveUserOrg && theAthlete.organizations.length)) {
  //     debugger;
  //     theAthlete.organizations = [{
  //         organizationId: group.organizationId,
  //         organizationName: group.organizationName,
  //         organizationCode: 'UNKOWN',
  //         active: true
  //     }]
  // } else {
  debugger;
  let currentAthleteOrg = updatedAthleteDoc.organizations[0]

  if (moveUserOrg || !updatedAthleteDoc.organizations || updatedAthleteDoc.organizations.find((org) => org.organizationId === group.organizationId) !== undefined) {
    debugger;
    if (!updatedAthleteDoc.organizations) {
      debugger;
      updatedAthleteDoc.organizations = [{
        organizationId: group.organizationId,
        organizationName: group.organizationName,
        organizationCode: 'UNKOWN',
        active: true,
        joinDate: firestore.Timestamp.now()
      }]
    } else {
      debugger;
      if (moveUserOrg) {
        // TODO:  Current Org Movign From Needs To Be Passes In
        currentAthleteOrg = updatedAthleteDoc.organizations.filter((o) => o.active).length ? updatedAthleteDoc.organizations.filter((o) => o.active)[0] : updatedAthleteDoc.organizations[0]

        updatedAthleteDoc.organizations.map(org => org.active = false)
      }
      // theAthlete.organizations.unshift({
      //     organizationId: group.organizationId,
      //     organizationName: group.organizationName,
      //     organizationCode: 'UNKOWN',
      //     active: true,
      //     joinDate: firestore.Timestamp.now()
      // })
    }
  }
  // }

  // if ((!theAthlete.organizationIds && theAthlete.organizationId) || (moveUserOrg && theAthlete.organizationId)) {
  //     theAthlete.organizationIds = [group.organizationId]
  // } else {
  //     if (!theAthlete.organizationIds || theAthlete.organizationIds.indexOf(group.organizationId) < 0) {
  //         if (moveUserOrg || !theAthlete.organizationIds) {
  //             theAthlete.organizationIds = [group.organizationId]
  //         } else {
  //             theAthlete.organizationIds.push(group.organizationId)
  //         }
  //     }
  // }


  const theOrg = await getOrgDocRef(currentAthleteOrg.organizationId).get()
    .then(async (orgSnapshot: firestore.DocumentSnapshot) => {

      // // Remove athlete from groups if not a coach or swithing Organizations
      // if (!athlete.isCoach) {
      if (
        !updatedAthleteDoc.isCoach
        // moveUserOrg
      ) {


        // TRAVIS: REMOVING ATHLETE INDICATOR FROM GROUP OBJECT
        await Promise.all(updatedAthleteDoc.groups.filter((g) => g.groupId !== group.groupId).map((grp) => {
          return removeAllAthleteIndicators(updatedAthleteDoc, grp.groupId, allowGroupDelete);
        }))

      }
      // }

      let org: Organization = undefined
      if (orgSnapshot.exists && !moveUserOrg) {

        org = { ...orgSnapshot.data(), uid: orgSnapshot.id } as Organization

      } else {
        if (orgSnapshot.exists) {
          console.log(`Org Not Found with UID = (orgUID) => ${currentAthleteOrg.organizationId} = (orgName) => ${currentAthleteOrg.organizationName}`)

        } else {
          console.log(`Org Not Found with UID = (orgUID) => ${currentAthleteOrg.organizationId} = (orgName) => ${currentAthleteOrg.organizationName}`)

        }

        org = await getOrgDocRef(group.organizationId).get().then((docSnap) => { return getDocumentFromSnapshot(docSnap) as Organization })
        // return res.status(204).json({ message: `Org Not Found with UID = (orgUID) => ${currentAthleteOrgId}` })
      }
      return org

    }).catch((e) => {
      throw e
    });

  const orgCode = theOrg.organizationCode

  const isCoachEmailList = theOrg.coaches.find((email) => email === updatedAthleteDoc.profile.email)

  const isCoachEmail = isCoachEmailList && isCoachEmailList.length !== 0 ? true : false

  let isCoach = coachFlag ? coachFlag : false;
  if (isCoach !== isCoachEmail) {
    console.warn(`isCoach !== isCoachEmail, setting to ${isCoachEmail}`)
    isCoach = isCoachEmail
  }

  // const newOrg = await getOrgDocFromSnapshot(orgUID)

  if (moveUserOrg) {
    updatedAthleteDoc.organizationId = theOrg.uid;

    updatedAthleteDoc.organizations.unshift({
      active: true,
      organizationId: theOrg.uid,
      organizationName: theOrg.name,
      organizationCode: theOrg.organizationCode,
      joinDate: firestore.Timestamp.now()
    })
  }
  const athleteIndicator: AthleteIndicator = {
    creationTimestamp: updatedAthleteDoc.metadata.creationTimestamp,
    email: updatedAthleteDoc.profile.email || updatedAthleteDoc.email,
    firstName: updatedAthleteDoc.profile.firstName,
    includeGroupAggregation: !isCoach,
    ipScore: updatedAthleteDoc.currentIpScoreTracking.ipScore,
    runningTotalIpScore: updatedAthleteDoc.runningTotalIpScore,
    // isCoach: coachFlag ? coachFlag : false,
    isCoach,
    isIpScoreDecrease: false,
    isIpScoreIncrease: false,
    orgTools: updatedAthleteDoc.orgTools || null,
    bio: updatedAthleteDoc.profile.bio || '',
    lastName: updatedAthleteDoc.profile.lastName,
    organizations: updatedAthleteDoc.organizations || [currentAthleteOrg],
    organizationId: moveUserOrg ? group.organizationId : currentAthleteOrg.organizationId,
    // organizationName: moveUserOrg ? group.organizationName : currentAthleteOrg.organizationName,
    // TODO:
    // organizationIds: [group.organizationId],
    profileImageURL: updatedAthleteDoc.profile.imageUrl || '',

    firebaseMessagingIds: firebaseMessagingIds || updatedAthleteDoc.firebaseMessagingIds || updatedAthleteDoc.metadata.firebaseMessagingIds || [updatedAthleteDoc.metadata.firebaseMessagingId],
    subscriptionType: updatedAthleteDoc.profile.subscription.type,
    uid: updatedAthleteDoc.uid
  };

  // const currentAthletes = cloneDeep(group.athletes);
  // const currentAthlete = currentAthletes.find(ath => ath.uid === theAthlete.uid);
  // if (!currentAthlete)
  //     currentAthletes.push(athlete);

  // if (!athleteIndicator.firebaseMessagingIds) {
  //   console.warn('firebaseMessagingIds not set for ' + athleteIndicator.firstName, athleteIndicator.uid)
  //   athleteIndicator.firebaseMessagingIds = firebaseMessagingIds || []
  // }

//   const clonedAthletes = cloneDeep(group.athletes || []);

//   let updateresult

//   const groupAthletes = getUniqueListBy(clonedAthletes, 'uid') as AthleteIndicator[];

//   if (groupAthletes.length !== clonedAthletes.length) {
//     console.warn('dup athletes removed', {
//       groupAthletesWithDups: clonedAthletes.map((a) => {
//         return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
//       }),
//       groupAthletesCleaned: groupAthletes.map((a) => {
//         return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
//       })
//     })
//   }

//   const groupRef = getGroupDocRef(group.groupId)
  
//   // TRAVIS: ADDING ATHLETE TO NEW GROUP
//   const athIndex = groupAthletes.findIndex((a) => a.uid === athleteIndicator.uid)
//   if (athIndex < 0) {
//     updateresult = await groupRef.update({ ...group, athletes: firestore.FieldValue.arrayUnion(athleteIndicator) })
//   } else {
//     groupAthletes[athIndex] = athleteIndicator

//     updateresult = await groupRef.update({ ...group, athletes: groupAthletes })
//   }

  const exists = await doesAthleteExistInGroup(group.groupId, athleteIndicator.uid);

  if (!exists) {

    group.athleteIndex[athleteIndicator.uid]= true;
    await db.collection(FirestoreCollection.Groups)
            .doc(group.groupId)
            .update({ ...group });
  
    await db.collection(FirestoreCollection.Groups)
                .doc(group.groupId)
                .collection('athletes')
                .doc(athleteIndicator.uid)
                .set(athleteIndicator)
                .then((result) => {
                    console.log(`Athlete added to Group: Athlete UID: ${athleteIndicator.uid}, Group UID: ${group.groupId}`)
                    return `Athlete added to Group: Athlete UID: ${athleteIndicator.uid}, Group UID: ${group.groupId}`;
                })
                .catch((err) => {
                    console.error(`Error retrieved when adding athlete ${athleteIndicator.uid} to group ${group.groupId}.  Error: ${err}`);
                    return err;
                });

    let onboardingCode = updatedAthleteDoc.profile.onboardingCode
    if (moveUserOrg || onboardingCode.toUpperCase() === 'INSPIRE') {
        onboardingCode = `${orgCode}-${group.uid.split('-')[1]}`
    }

    const updatedAthlete: Athlete = {
        ...updatedAthleteDoc,
        isCoach,
        includeGroupAggregation: athleteIndicator.includeGroupAggregation,
        // organizationId: athleteIndicator.organizationId,
        organizations: updatedAthleteDoc.organizations,
        uid: athleteIndicator.uid,
        profile: {
        ...updatedAthleteDoc.profile,
        onboardingCode: onboardingCode
        }
    }

    const { isPrePaid: isOrgPrePaid, subscriptionType, prePaidNumberOfMonths: orgPrePaidNumberOfMonths } = theOrg.onboardingConfiguration;

    const prePaidGroup =
        theOrg.onboardingConfiguration &&
        theOrg.onboardingConfiguration.paidGroups &&
        theOrg.onboardingConfiguration.paidGroups.find((paidGroup) =>
        paidGroup.groupName === `${theOrg.organizationCode}-${group.groupIdentifier}`)

    if (isOrgPrePaid || prePaidGroup || group.onboardingConfiguration && group.onboardingConfiguration.isPrePaid) {

        if (prePaidGroup) {
        const paidGroup = theOrg.onboardingConfiguration.paidGroups.find((thePaidGroup) =>
            thePaidGroup.groupName === `${theOrg.organizationCode}-${group.groupIdentifier}`)

        const {
            prePaidNumberOfMonths: subscriptionLengthInMonths } = paidGroup

        const nowMoment = moment();
        const subscriptionEndDate = nowMoment.clone().add(subscriptionLengthInMonths, 'months');

        updatedAthlete.profile.subscription.type = subscriptionType || 1

        updatedAthlete.profile.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
        updatedAthlete.profile.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());


        } else {

        const subscriptionLengthInMonths =
            group && group.onboardingConfiguration && group.onboardingConfiguration.prePaidNumberOfMonths ||
            prePaidGroup && prePaidGroup.prePaidNumberOfMonths ||
            isOrgPrePaid ? orgPrePaidNumberOfMonths || 1 : 0 ||
            0

        const nowMoment = moment();
        const subscriptionEndDate = nowMoment.clone().add(subscriptionLengthInMonths, 'months');

        updatedAthlete.profile.subscription.type = 1 //subscriptionType || group.onboardingConfiguration.subscriptionType

        updatedAthlete.profile.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
        updatedAthlete.profile.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());
        }

    }

    if (!updatedAthlete.firebaseMessagingIds) {
        console.warn('firebaseMessagingIds not set for ' + updatedAthlete.profile.firstName, updatedAthlete.uid)
        updatedAthlete.firebaseMessagingIds = []
    }

    return updateAthlete({
        // ...group, athletes: groupAthletes,
        ...group
    },

        updatedAthlete,
        oldAthlete,
        atheleteRef,
        isCoach,
        //  coachFlag, 
        false, theOrg.uid, theOrg.name)
        .then(() => {
        return {
            message: 'Athlete Group Updated',
            responseCode: 201,
            data: {
            group,
            },
            method: {
            name: 'UpdateAthlete',
            line: 58,
            },
        }
        })
        .catch((err) => {
        return { message: 'Unable to update athlete group details', err }
        })
    } else {
        return {
            message: 'Athlete Already Exists In Group',
            responseCode: 200,
            data: {
            group,
            },
            method: {
            name: 'UpdateAthlete',
            line: 58,
            },
        }
    }





  // return 'Athlete Updated'

  // const currentGroup = { uid: orgSnapshot.id, ...orgSnapshot.data() } as Group
  // const clonedAthletes = cloneDeep(currentGroup.athletes)
  // const retainedAthletes = clonedAthletes.filter(currentAth => currentAth.uid !== theAthlete.uid)
  // return orgSnapshot.ref.update({ ...currentGroup, athletes: retainedAthletes }).then(() => {
  //     return 'Athlete removed from Group'
  // })



  // .then((writeResult) => {
  //     console.log(writeResult);
  //     return writeResult
  // })
  // })

  //includeGroupAggregation: true

}

export const moveAthleteToGroup = functions.https._onCallWithOptions(
  async (data: ManageAthleteGroupRequest, context) => {

    try {

      if (!context.auth.token.uid) {
        console.error(
          'Request not authorized. User must logged in to fulfill this request'
        )
        return {
          error:
            'Request not authorized. User must logged in to fulfill this request ' +
            data,
        }
      }

      // const { uid, reportType, requestDateTime: requestDateTimeString, requestProcessed, requestSuccess, requestProcessedDateTime, requestingUser, entity, errorLogs } = data;

      const { groupId, userId, moveUserOrg, allowGroupDelete } = <{
        groupId: string, userId: string, coachFlag?: boolean, moveUserOrg?: boolean,
        orgUID: string, currentOrgName: string, organizationCode: string,
        firebaseMessagingIds: string[], allowGroupDelete: boolean
      }>data;
      console.log(data);

      // const group = {
      //     athletes: [],
      //     creationTimestamp: new Date(),
      //     groupId: "8aRllwKnpOw01lvhB2j8-QLD1",
      //     groupIdentifier: "QLD1",
      //     groupName: "Diving Australia: Queensland",
      //     logoUrl: "https://s3-ap-southeast-2.amazonaws.com/piano.revolutionise.com.au/logos/yuojmbb9cnx1akfn.png",
      //     organizationId: "8aRllwKnpOw01lvhB2j8",
      //     uid: "8aRllwKnpOw01lvhB2j8-QLD1"
      // }

      const pass = true;
      if (pass && groupId && userId) {

        const group = await getGroupDocRef(groupId).get()
          .then((groupSnap) => {
            if (groupSnap.exists) {

              const theGroup = { ...groupSnap.data(), uid: groupSnap.id } as Group;
              if (!theGroup.athleteIndex) {
              // if (!theGroup.athletes) {
                // theGroup.athletes = []
                // TRAVIS
                theGroup.athleteIndex = {}; 
              }

              return theGroup
            } else {
              console.error({ message: `Group  Not Found with UID = (groupID) => ${groupId}` })

              throw {

                error: `Group Not Found with UID = (groupID) => ${groupId}`
              }
            }
          }).catch((err) => {
            throw new Error(err)
          })


        return executeMoveAthleteToGroup(userId, group, moveUserOrg, allowGroupDelete)

      } else {
        console.error({ message: 'No ID supplied' })

        return { message: 'No ID supplied' }
      }
    }
    catch (e) {
      console.error({ e })

      throw e

    }
  }, {regions: Config.regions});

  

  

