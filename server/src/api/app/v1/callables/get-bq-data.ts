import * as functions from 'firebase-functions'
import { ReportRequest } from '../../../../models/report-model';
import { generateReportRequest } from '../controllers/utils/reports/generateReportRequest';
import { Config } from './../../../../shared/init/config/config';

// const cors = require('cors')({ origin: true });

export const getBQData = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            try {
                const theData
                    = <ReportRequest>data
                console.warn('theData.requestDateTime => ', theData.requestDateTime)
                const reportRequestResponse = await generateReportRequest(theData);
                console.log('reportRequestResponse => ', (reportRequestResponse as any).length)
                return reportRequestResponse;

            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return Promise.reject({
                    message:
                        'An unanticipated exception occurred in getBQData',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in getBQData',
                        exception: exception,
                    },
                    method: {
                        name: 'getBQData'
                    }
                })
            }


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)

