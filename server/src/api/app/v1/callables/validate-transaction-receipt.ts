import * as functions from 'firebase-functions'
import moment from 'moment';
import * as iap from 'in-app-purchase'

import { db } from '../../../../shared/init/initialise-firebase';
import { TransactionReceiptValidationRequest } from '../../../../models/purchase/interfaces/transaction-receipt-validation-request';

import { validateIOS } from './utils/validate-transaction-receipt/validate-ios';
import { validateAndroid } from './utils/validate-transaction-receipt/validate-android';
import { initIOSPaymentValidationPlugin } from './utils/validate-transaction-receipt/init-ios-payment-validation-plugin';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { isProductionProject } from '../is-production';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { Config } from './../../../../shared/init/config/config';

declare function atob(data: string): string;

export const validateTransactionReceipt = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            // if (!context.auth.token.uid) {
            //     console.error(
            //         'Request not authorized. User must logged in to fulfill this request'
            //     )
            //     return {
            //         error:
            //             'Request not authorized. User must logged in to fulfill this request ' +
            //             data,
            //     }
            // }

            // console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            // console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            console.log('validateReceipt - data', data)

            const {

                useReceiptFromDb,
                receiptID,

            } = <TransactionReceiptValidationRequest>data

            let theData = data

            if (useReceiptFromDb) {
                const theDataItem = await db.collection('receiptTests').doc(receiptID).get().then((docSnap) => getDocumentFromSnapshot(docSnap) as { uid: string; body: any })
                theData = theDataItem.body
            } else {
                await db.collection('receiptTests').add({
                    body: theData,
                    transactionReceipt: theData.transactionReceipt,
                    creationTimestamp: moment().toDate(),
                })
            }


            const {
                athleteUid,
                platform,
                transactionReceipt,
                packageName,
                ...thePayload
            } = <TransactionReceiptValidationRequest>theData

            const { productIdentifier } = <TransactionReceiptValidationRequest>theData

            const thePackageName = packageName ? packageName : isProductionProject() ? platform === 'Android' ? 'com.inspiresportonline.main' : 'com.inspiresportonline.ios' : 'com.inspiresportonline.dev';

            // console.warn('theData', theData)
            console.log('thePackageName', thePackageName)
            console.log('productIdentifier', productIdentifier)

            try {
                if (platform === 'Android') {
                    if (!productIdentifier) {
                        console.warn({ productIdentifier })
                    }
                    return await validateAndroid(theData, thePackageName, productIdentifier || '', athleteUid)
                } else {


                    const applePasswordFromDB = await db.collection(FirestoreCollection.SystemConfiguration).doc(thePackageName)
                        .get().then(async (docSnap) => {

                            if (docSnap.exists) {
                                return {
                                    uid: docSnap.id,
                                    ...docSnap.data() as { applePassword: string }
                                }.applePassword;
                            } else {
                                console.warn(`Password not available for ${thePackageName}, defaulting to ${isProductionProject() ? platform === 'Android' ? 'com.inspiresportonline.main' : 'com.inspiresportonline.ios' : 'com.inspiresportonline.dev'}`)
                                const theConfig = await db.collection(FirestoreCollection.SystemConfiguration).doc(isProductionProject() ? platform === 'Android' ? 'com.inspiresportonline.main' : 'com.inspiresportonline.ios' : 'com.inspiresportonline.dev')
                                    .get().then((settingsSnap) => {

                                        if (settingsSnap.exists) {
                                            return {
                                                uid: settingsSnap.id,
                                                ...settingsSnap.data() as { applePassword: string }
                                            };
                                        } else {
                                            return undefined
                                        }

                                    })

                                return theConfig.applePassword
                            }

                        })


                    const applePassword = theData.applePassword

                    // const theAppelePassword = applePassword || decryptFunc(applePasswordFromDB)
                    const theAppelePassword = applePassword || applePasswordFromDB

                    console.log('applePasswordFromDB', { applePasswordFromDB })

                    initIOSPaymentValidationPlugin(iap, theAppelePassword)


                    try {
                        const theIOSResult = await validateIOS(
                            transactionReceipt,
                            theData,
                            athleteUid,
                            thePayload
                        )
                        console.log('finally returning theIOSResult - value => ', theIOSResult);

                        return theIOSResult

                    } catch (error) {
                        atob(transactionReceipt)

                        console.error('An unanticipated exception occurred in validateTransactionReceipt/validateIOS', error)

                        throw error
                    }
                }
            } catch (error) {

                console.error('An unanticipated exception occurred in validateTransactionReceipt', error)
                return {
                    message:
                        'An unanticipated exception occurred in validateTransactionReceipt',
                    responseCode: 500,
                    data: { message: error },
                    error: {
                        message:
                            'An unanticipated exception occurred in validateTransactionReceipt',
                        exception: error,
                    },
                    method: {
                        name: 'validateTransactionReceipt'
                    }
                }
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)


