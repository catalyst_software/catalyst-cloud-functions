import { getSharedSystemConfiguration } from './utils/warehouse/olap/get-olap-jwt-package';

import * as functions from 'firebase-functions'
import { initialiseFirebase } from '../../../../shared/init/initialise-firebase';
import { Config } from './../../../../shared/init/config/config';

const cors = require('cors')({ origin: true });
const jwt = require('jwt-simple');

export enum ConsentRequestResponse {
    RequestGranted = 'requestGranted',
    RequestDenied = 'requestDenied',
    RequestPending = 'requestPending',
    RequestInvalid = 'requestInvalid'
}

export interface RequestCustomTokenPayload {
    authToken: string;
}

export interface CustomClaimsPayload {
    userVerified: boolean;
    uid?: string;
    creationTimestamp?: number;
    guid?: string;
}

export interface ConsentRequestDocument {
    athleteId: string;
    guid: string;
    valid: boolean;
    consentResponse: ConsentRequestResponse
}

export const createToken = async (useSimpleToken: boolean, uid: string, userVerified: boolean, includeTimestamp: boolean, guid?: string): Promise<string> => {
    const liveAdmin = initialiseFirebase('API').admin;
    const claims: CustomClaimsPayload = { userVerified };
    if (includeTimestamp) {
        claims.creationTimestamp = (new Date()).getTime();
    }

    if (guid) {
        claims.guid = guid;
    }
    let token;
    if (useSimpleToken) {
        claims.uid = uid;
        const config = await getSharedSystemConfiguration();
        token = await jwt.encode(claims, config.jwtSecret);
    } else {
        token = await liveAdmin.auth().createCustomToken(uid, claims);
    }
    return Promise.resolve(token);
}

export const getCustomAuthToken = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            try {
                const liveAdmin = initialiseFirebase('API').admin;
    
                const { uid } = context.auth.token
                // Use iNSPIRE Main app to create the custom auth token with the appropriate UID set
                // Create Custom Token 
                try {
                    const customToken = await createToken(false, uid, true, true);
                    console.log('Successfully created Custom Token', { customToken });
                    return customToken;
                } catch (error) {
                    console.log('Error creatimg Custom Token:', error);
                    return error;
                }

            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return {
                    message:
                        'An unanticipated exception occurred in getCustomAuthToken',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in getCustomAuthToken',
                        exception: exception,
                    },
                    method: {
                        name: 'getCustomAuthToken'
                    }
                }
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return error
        }
    }, {regions: Config.regions}
)