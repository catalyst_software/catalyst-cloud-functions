import * as functions from 'firebase-functions'

import { theMainApp } from '../../../../shared/init/initialise-firebase';
import { Athlete } from '../../../../models/athlete/interfaces/athlete';
import { getAthleteDocRef } from '../../../../db/refs';
import { Config } from './../../../../shared/init/config/config';

export interface EmailVerificationPayload {
    uid: string;
    emailAddress: string;
    isVerified?: boolean;
}

export const setUserEmail = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            try {
                const { uid, emailAddress }
                    = <EmailVerificationPayload>data

                return await theMainApp.auth().updateUser(uid, {
                    email: emailAddress
                })
                    .then(async function (userRecord) {
                        // See the UserRecord reference doc for the contents of userRecord.
                        console.log('Successfully updated user', userRecord.toJSON());

                        const athleteRef = getAthleteDocRef(uid)


                        const athlete: Athlete = await athleteRef.get().then((athleteDocSnap) => {
                            if (athleteDocSnap.exists) {
                                return {
                                    uid: athleteDocSnap.id,
                                    ...athleteDocSnap.data()
                                } as Athlete
                            } else {
                                return undefined
                            }

                        })

                        if (athlete) {
                            if (athlete.profile.email === '') {
                                athlete.profile.email = emailAddress
                                await athleteRef.update({
                                    profile: { email: emailAddress }
                                })
                            }

                        }

                        return true


                    })
                    .catch(function (error) {
                        console.log('Error updating user:', error);
                        return error
                    });

            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return {
                    message:
                        'An unanticipated exception occurred in setUserEmail',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in setUserEmail',
                        exception: exception,
                    },
                    method: {
                        name: 'setUserEmail'
                    }
                }
            }


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)

