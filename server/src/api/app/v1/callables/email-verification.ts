import * as functions from 'firebase-functions'

import { admin } from '../../../../shared/init/initialise-firebase';
import { getUserByID } from '../controllers/get-user-by-id';
import { Config } from './../../../../shared/init/config/config';


export interface EmailVerificationPayload {
    uid: string;
    isVerified?: boolean;
}

export const setUserEmailVerified = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            try {
                const { uid, isVerified }
                    = <EmailVerificationPayload>data

                return admin.auth().updateUser(uid, {

                    emailVerified: isVerified,
                    // password: 'newPassword',
                    // displayName: 'Jane Doe',
                    // photoURL: 'http://www.example.com/12345678/photo.png',
                    // disabled: true
                })
                    .then(function (userRecord) {
                        // See the UserRecord reference doc for the contents of userRecord.
                        console.log('Successfully updated user', userRecord.toJSON());
                        return true
                    })
                    .catch(function (error) {
                        console.log('Error updating user:', error);
                        return error
                    });

            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return {
                    message:
                        'An unanticipated exception occurred in setUserEmailVerification',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in setUserEmailVerification',
                        exception: exception,
                    },
                    method: {
                        name: 'setUserEmailVerification'
                    }
                }
            }
            // console.log('groupid ', groupId)

            // return await updateAthleteIndicatorsIPScoreOnGroup(groupId).then((results) => {

            //     return results
            // }).catch((err) => {
            //     return err

            // })

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)

export const getUserEmailVerified = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            try {
                const { uid }
                    = <EmailVerificationPayload>data

                const user = await getUserByID(uid)

                if (user) {
                    return user.emailVerified
                } else {
                    return false
                }

            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return {
                    message:
                        'An unanticipated exception occurred in setUserEmailVerification',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in setUserEmailVerification',
                        exception: exception,
                    },
                    method: {
                        name: 'setUserEmailVerification'
                    }
                }
            }








            // console.log('groupid ', groupId)

            // return await updateAthleteIndicatorsIPScoreOnGroup(groupId).then((results) => {

            //     return results
            // }).catch((err) => {
            //     return err

            // })

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)
