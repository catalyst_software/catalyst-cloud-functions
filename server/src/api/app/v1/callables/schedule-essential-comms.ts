import { AthleteIndicator } from './../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import * as functions from 'firebase-functions'
import moment from 'moment';

import { EmailMessageTemplate } from "../sendGrid/templates/email-message-template";
import { getAthleteCoaches } from '../../../../analytics/triggers/utils/get-athlete-coaches';
import { notifyCoaches } from './utils/comms/notify-coaches';
import { NonEngagementConfiguration } from '../../../../models/organization.model';
import { KeyValuePair } from '../../../../models/notifications/interfaces/schedule-notification-payload';
import { Config } from './../../../../shared/init/config/config';
import { getRedFlagNotificationUsers } from './utils/essentialComms/get-red-flag-notification-users';
import { getExcludedRedFlagTypesForOrganization } from './utils/essentialComms/get-excluded-red-flag-types-for-org';
import { sendRedFlagEssentialCommsNotifications } from './utils/essentialComms/send-red-flag-essential-comms-notifications';

export enum AnalyticesEventParameterType {
    USER_ID = 'USER_ID',
    USER_FULL_NAME = 'USER_FULL_NAME',
    USER_FIRST_NAME = 'USER_FIRST_NAME',
    USER_ONBOARDING_CODE = 'USER_ONBOARDING_CODE',
    GROUP_IDS = 'GROUP_IDS',
    ORGANIZATION_ID = 'ORGANIZATION_ID'
}

export interface ScheduleEssentialCommsRequest {
    emailMessageTemplate: EmailMessageTemplate;
    groupIds: Array<string>;
    parameters: KeyValuePair[];
    payload: ScheduleEssentialCommsPayload;
}

export interface ScheduleEssentialCommsPayload {
    analyticsFamily?: string;
    userID?: string;
    organizationID?: string;
    userFirstName?: string;
    userLastName?: string;
    userFullName?: string;
    userOnboardingCode?: string;
    value?: number;
    groupIDs?: string;
}

const getUtcDateTimeNow = (): number => {
    const now = new Date();

    return Date.parse(now.toISOString());
}

const getParameterValue = (key: AnalyticesEventParameterType, parameters: KeyValuePair[]): string => {
    const pair = parameters.find((kv: KeyValuePair) => {
        return kv.key.toUpperCase() === key;
    });

    return (pair) ? pair.value : '';
}

const removeDuplicateCoaches = (coaches: Array<AthleteIndicator>) => {
    const uniques: Array<AthleteIndicator> = [];
    for (const coach of coaches) {
        if (coach) {
            const existing = uniques.find((c: AthleteIndicator) => {
                return (c) ? c.uid === coach.uid : undefined;
            });

            if (!existing) {
                uniques.push(coach);
            }
        }
    }
    return uniques;
}

const isRedFlagTemplateType = (type: number) => {
    return type >= 20 && type < 30;
}

const isRedFlagTypeExcluded = async (type: number, orgId: string) => {
    const orgLevelExcludedTypes = await getExcludedRedFlagTypesForOrganization(orgId);
    console.log(`orgLevelExcludedTypes: ${JSON.stringify(orgLevelExcludedTypes)}`);
    const index = orgLevelExcludedTypes.findIndex((t: number) => {
        return t === type;
    });
    console.log(`index: ${index}`);
    const isExcluded = (index >= 0);

    return isExcluded;
}

const getRedFlagTypeAsFriendlyString = (templateName: string): string => {
    switch (templateName) {
        case 'badMood':
            return 'Poor Mood';
        case 'distraughtMood':
            return 'Distraught Mood';
        case 'extremeFatigue':
            return 'Extreme Fatigue';
        case 'moderatePainOrAbove':
            return 'Experiencing Pain';
        case 'multipleDailyTraining':
            return 'Excessive Daily Training';
        case 'periodTurnedOff':
            return 'Menstration Cycle Finished';
        case 'periodTurnedOn':
            return 'Menstration Cycle Started';
        case 'sicknessTurnedOff':
            return 'No Longer Feeling Sick';
        case 'sicknessTurnedOn':
            return 'Feeling Sick';
        case 'sleepDeprivation':
            return 'Sleep Deprivation';
        default:
            return 'Unknown';
    }
}

export const sgKey = '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';

export const scheduleEssentialComms = functions.https._onCallWithOptions(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('data => ', data)

            const { uid: athleteUID } = context.auth.token;

            console.log(`athleteUID: ${athleteUID}`);

            const essentialCommsRequest = <ScheduleEssentialCommsRequest>data;
            console.log(`essentialCommsRequest: ${JSON.stringify(essentialCommsRequest)}`);

            const templateType = essentialCommsRequest.emailMessageTemplate.notificationTemplateType;
            if (isRedFlagTemplateType(templateType)) {
                console.log(`Template type ${templateType} is a red flag.  Proceeding with red flag notification.`);
                const orgId = essentialCommsRequest.payload.organizationID;
                const isExcluded = await isRedFlagTypeExcluded(templateType, orgId);
                console.log(`Template type ${templateType} configured for exclusion: ${isExcluded}`);
                if (isExcluded) {
                    console.log(`Red flag notification for type ${templateType} is configured for exclusion.  Aborting red flag notification.`);
                } else {
                    console.log(`Red flag type ${templateType} is not excluded.  Proceeding with red flag notification.`);
                    const groupIds = JSON.parse(essentialCommsRequest.payload.groupIDs);
                    const parameters = essentialCommsRequest.parameters;
                    const templateName = essentialCommsRequest.emailMessageTemplate.emailTemplateName;
                    const appIdentifierPair = parameters.find((kv: KeyValuePair) => {
                        return kv.key.toUpperCase() === 'APP_IDENTIFIER';
                    });

                    const appIdentifier = (appIdentifierPair) ? appIdentifierPair.value : 'com.catalystapp.app';
                    const path = essentialCommsRequest.emailMessageTemplate.notificationPath || '';
                    const icon = essentialCommsRequest.emailMessageTemplate.notificationIcon || '';
                    console.log(`Calling getRedFlagNotificationUsers with params orgId: ${orgId} and groupIds: ${JSON.stringify(groupIds)}`);
                    const notificationUsers = await getRedFlagNotificationUsers(orgId, groupIds, athleteUID);

                    // Red Flag Email
                    essentialCommsRequest.emailMessageTemplate.templateId = 'd-fe0fd90e3c624070868a9dee67bb0b11';
                    
                    essentialCommsRequest.emailMessageTemplate.emailTemplateData = {
                        ...essentialCommsRequest.payload,
                        name: '',
                        redFlagType: getRedFlagTypeAsFriendlyString(essentialCommsRequest.emailMessageTemplate.emailTemplateName)
                    } as any;

                    for (const notificationUser of notificationUsers) {
                        essentialCommsRequest.emailMessageTemplate.to = {
                            name: `${notificationUser.firstName} ${notificationUser.lastName}`,
                            email: notificationUser.email
                        } as any;
                        essentialCommsRequest.emailMessageTemplate.emailTemplateData.senderFullName = essentialCommsRequest.payload.userFullName;
                        essentialCommsRequest.emailMessageTemplate.emailTemplateData.userFirstName = notificationUser.firstName;
                        essentialCommsRequest.emailMessageTemplate.emailTemplateData.name = notificationUser.firstName;
                        console.log(`Attempting to send red flag essential comms to user ${notificationUser.uid}`);
                        const success = await sendRedFlagEssentialCommsNotifications(essentialCommsRequest.emailMessageTemplate, notificationUser, templateName, parameters, appIdentifier, true, path, icon);

                        console.log(`Send red flag notifications result: ${success}`);
                    }

                    console.log(`notificationUsers for athlete ${athleteUID}: ${JSON.stringify(notificationUsers)}`);

                }
            } else {
                console.log(`Template type ${templateType} is NOT a red flag.  Aborting red flag notification.`);
            }

            return true;
        } catch (error) {
            console.error(
                'GLOBAL CATCH ERROR IN scheduleEssentialComm:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions})
            

export const scheduleEssentialCommsOld = functions.https._onCallWithOptions(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('data => ', data)

            const { uid: athleteUID } = context.auth.token;
            

            const { emailMessageTemplate, groupIds, parameters, payload }
                = <{ emailMessageTemplate: EmailMessageTemplate, groupIds: Array<string>, parameters: KeyValuePair[], payload: any }>data

            const flagType = data.key;
            const creationTimestamp = (new Date()).toJSON();
            // const athleteUID = getParameterValue(AnalyticesEventParameterType.USER_ID, parameters);
            const userFullName = getParameterValue(AnalyticesEventParameterType.USER_FULL_NAME, parameters);
            const userFirstName = getParameterValue(AnalyticesEventParameterType.USER_FIRST_NAME, parameters);
            const userOnboardingCode = getParameterValue(AnalyticesEventParameterType.USER_ONBOARDING_CODE, parameters);
            const organizationID = getParameterValue(AnalyticesEventParameterType.ORGANIZATION_ID, parameters);
            const appIdentifier = payload.appIdentifier;

            console.log(`Configured appIdentifier: ${appIdentifier}`);

            
            const fType = flagType || '';
            console.log(`Essential comms GroupIDs: ${groupIds}`);

            const coachesToEmailMap = await getAthleteCoaches(athleteUID, organizationID, groupIds, userOnboardingCode)

            const theDate = creationTimestamp || (new Date()).toJSON();
                
            const time = moment(theDate).format('hh:mm A DD MM Y')
            // const flagID = athleteUID + '_' + time.replace(/ /g, '_') + '_' + fType.replace(/ /g, '_');

            if (coachesToEmailMap.length) {
                // TB: 17/11/2020 RED FLAG CARDS TEMPORARILY REMOVED
                // try {
                //     const batch = db.batch();
                //     const redFlagCard = {

                //         entityId: undefined,
                //         entityName: undefined,
                //         creatorId: athleteUID,
                //         creatorName: userFullName,
                //         creationTimestamp,
                //         channelType: "athlete",
                //         notificationCardType: "redflag",
                //         cardType: "notification",
                //         notificationCardBehaviorType: "acknowledgement",
                //         title: userFullName + " has registered a red flag",
                //         description: "",
                //         trackViews: false,
                //         dismissable: true,
                //         isActive: true,
                //         isSticky: false,
                //         creatorImageUrl: creatorImageUrl || "",
                //         flagType,
                //         flagID: flagID,
                //         geoPoint: geoPoint || '',
                //         lifestyleEntryType,
                //         lifestyleEntryValue: lifestyleEntryValue || value,
                //     }

                //     coachesToEmailMap.forEach((c) => {

                //         redFlagCard.entityId = c.coachRest.uid
                //         redFlagCard.entityName = c.coachRest.firstName.trim() + ' ' + c.coachRest.lastName.trim()

                //         const cardDocRef = db.collection(FirestoreCollection.NotificationCardRegistry).doc();
                //         batch.set(cardDocRef, redFlagCard);
                //     })

                //     //TB - 15/11/2020 RED FLAG CARDS TEMP DISABLED
                //     // await batch
                //     //     .commit()
                //     //     .then((result) => {
                //     //         console.log('Red Flag Cards Saved', { result });
                //     //     })
                //     //     .catch(err => {
                //     //         console.error('unable to create Red Flag Cards', { err });
                //     //     });

                // } catch (error) {
                //     console.error('CATCH ERROR : unable to create Red Flag Cards', { error });
                // }

                const orgConfigs = [].concat(...coachesToEmailMap.map((confSet) => confSet.orgConfig)) as Array<{
                    orgID: string;
                    nonEngagementConfiguration: NonEngagementConfiguration;
                }>
                let coachesToEmail = coachesToEmailMap.filter((coach) => coach.coachRest.uid !== undefined && !coach.skipComms).map((oeg) => oeg.coachRest)
                coachesToEmail = removeDuplicateCoaches(coachesToEmail);

                // return await notifyCoaches(emailMessageTemplate, coachesToEmail, athleteUID, userFullName, userFirstName);

                console.log(`Attempting to notify coaches: ${JSON.stringify(coachesToEmail)}`);

                const response = await notifyCoaches(emailMessageTemplate, parameters, coachesToEmail, athleteUID, userFullName, userFirstName, orgConfigs, appIdentifier);

                return await Promise.all(response).then((docs) => {
                    console.log('the scheduleEssentialComms Result', docs)

                    // return res.status(200).json(docs)
                    return true
                })
            } else {
                console.warn(`No Coach found for Athlete having uid ${athleteUID}`);

                return false;
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)



export const scheduleEssentialCommsTest = async (data) => {
        try {
            // if (!context.auth.token.uid) {
            //     console.error(
            //         'Request not authorized. User must logged in to fulfill this request'
            //     )
            //     return {
            //         error:
            //             'Request not authorized. User must logged in to fulfill this request ' +
            //             data,
            //     }
            // }

            // console.log('context.instanceIdToken => ', context.instanceIdToken)
            // console.log('context.auth => ', context.auth)
            // console.log('data => ', data)

            // const { uid: athleteUID } = context.auth.token;
            

            const { emailMessageTemplate, groupIds, parameters }
            = <{ emailMessageTemplate: EmailMessageTemplate, groupIds: Array<string>, parameters: KeyValuePair[] }>data

            const flagType = data.key;
            const creationTimestamp = (new Date()).toJSON();
            const athleteUID = getParameterValue(AnalyticesEventParameterType.USER_ID, parameters);
            const userFirstName = getParameterValue(AnalyticesEventParameterType.USER_FIRST_NAME, parameters);
            const userFullName = getParameterValue(AnalyticesEventParameterType.USER_FULL_NAME, parameters);
            const userOnboardingCode = getParameterValue(AnalyticesEventParameterType.USER_ONBOARDING_CODE, parameters);
            const organizationID = getParameterValue(AnalyticesEventParameterType.ORGANIZATION_ID, parameters);

            const fType = flagType || '';

            const coachesToEmailMap = await getAthleteCoaches(athleteUID, organizationID, groupIds, userOnboardingCode)

            const theDate = creationTimestamp || (new Date()).toJSON();
                
            const time = moment(theDate).format('hh:mm A DD MM Y')
            // const flagID = athleteUID + '_' + time.replace(/ /g, '_') + '_' + fType.replace(/ /g, '_');

            if (coachesToEmailMap.length) {
                // TB: 17/11/2020 RED FLAG CARDS TEMPORARILY REMOVED
                // try {
                //     const batch = db.batch();
                //     const redFlagCard = {

                //         entityId: undefined,
                //         entityName: undefined,
                //         creatorId: athleteUID,
                //         creatorName: userFullName,
                //         creationTimestamp,
                //         channelType: "athlete",
                //         notificationCardType: "redflag",
                //         cardType: "notification",
                //         notificationCardBehaviorType: "acknowledgement",
                //         title: userFullName + " has registered a red flag",
                //         description: "",
                //         trackViews: false,
                //         dismissable: true,
                //         isActive: true,
                //         isSticky: false,
                //         creatorImageUrl: creatorImageUrl || "",
                //         flagType,
                //         flagID: flagID,
                //         geoPoint: geoPoint || '',
                //         lifestyleEntryType,
                //         lifestyleEntryValue: lifestyleEntryValue || value,
                //     }

                //     coachesToEmailMap.forEach((c) => {

                //         redFlagCard.entityId = c.coachRest.uid
                //         redFlagCard.entityName = c.coachRest.firstName.trim() + ' ' + c.coachRest.lastName.trim()

                //         const cardDocRef = db.collection(FirestoreCollection.NotificationCardRegistry).doc();
                //         batch.set(cardDocRef, redFlagCard);
                //     })

                //     // await batch
                //     //     .commit()
                //     //     .then((result) => {
                //     //         console.log('Red Flag Cards Saved', { result });
                //     //     })
                //     //     .catch(err => {
                //     //         console.error('unable to create Red Flag Cards', { err });
                //     //     });

                // } catch (error) {
                //     console.error('CATCH ERROR : unable to create Red Flag Cards', { error });
                // }

                //TB 16-07-2020: Publish redflag task to pubsub topic
                // if (!!task) {
                //     try {
                //         if (task.worker === ScheduledTaskWorkerType.RedFlag) {
                //             console.log('Attempting to publish RedFlag task.');
                //             task.creationTimestamp = moment(task.creationTimestamp).toDate();
                //             (task.payload as RedFlagTask).redFlagId = flagID;
                //             const taskBuffer = Buffer.from(JSON.stringify(task), 'utf8');
                //             const messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.RedFlag).publish(taskBuffer);
                //             console.log(`Message ${messageId} published.`);
                //         }
                //     } catch (err) {
                //         console.error('CATCH ERROR : unable to publish RedFlag task on pubsub topic', { err });
                //     }
                // }


                const orgConfigs = [].concat(...coachesToEmailMap.map((confSet) => confSet.orgConfig)) as Array<{
                    orgID: string;
                    nonEngagementConfiguration: NonEngagementConfiguration;
                }>
                const coachesToEmail = coachesToEmailMap.filter((coach) => coach.coachRest.uid !== undefined && !coach.skipComms).map((oeg) => oeg.coachRest)


                // return await notifyCoaches(emailMessageTemplate, coachesToEmail, athleteUID, userFullName, userFirstName);

                const response = await notifyCoaches(emailMessageTemplate, parameters, coachesToEmail, athleteUID, userFullName, userFirstName, orgConfigs);

                return await Promise.all(response).then((docs) => {
                    console.log('the scheduleEssentialComms Result', docs)

                    // return res.status(200).json(docs)
                    return true
                })
            } else {
                console.warn(`No Coach found for Athlete having uid ${athleteUID}`);

                // if (!!task) {
                //     try {
                //         if (task.worker === ScheduledTaskWorkerType.RedFlag) {
                //             console.log('Attempting to publish RedFlag task.');
                //             task.creationTimestamp = moment(task.creationTimestamp).toDate();
                //             (task.payload as RedFlagTask).redFlagId = flagID;
                //             const taskBuffer = Buffer.from(JSON.stringify(task), 'utf8');
                //             const messageId = await pubSubClient.topic(ScheduledTaskWorkerTopicType.RedFlag).publish(taskBuffer);
                //             console.log(`Message ${messageId} published.`);
                //         }
                //     } catch (err) {
                //         console.error('CATCH ERROR : unable to publish RedFlag task on pubsub topic', { err });
                //     }
                // }

                return false;
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
}


