import * as functions from 'firebase-functions'
import { firestore } from 'firebase-admin'
import { isObject, isArray } from 'lodash'
import moment from 'moment'

import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { LogInfo } from '../../../../shared/logger/logger'
import { DocMngrActionTypes } from '../../../../db/biometricEntries/enums/document-manager-action-types'
import { WellGroupAggregationDirective } from '../controllers/biometrics'
import { Config } from './../../../../shared/init/config/config'

export const convertAllDatesStringsToTimeStamps = (theDoc: {
    [key: string]: any
}) => {
    Object.keys(theDoc).forEach(key => {
        try {
            if (typeof theDoc[key] === 'string') {
                if (
                    key.toLowerCase().indexOf('date') > -1 ||
                    key.toLowerCase().indexOf('timestamp') > -1
                ) {
                    const theTimestamp = firestore.Timestamp.fromDate(
                        moment(theDoc[key].replace('at ', '')).toDate()
                    )
                    console.warn(
                        `Converting  ${key} date => ${
                            theDoc[key]
                        } to TimeStamp(.toDate()) => ${theTimestamp.toDate()}`
                    )
                    theDoc[key] = theTimestamp
                }
            }

            if (isObject(theDoc[key])) {
                convertAllDatesStringsToTimeStamps(theDoc[key])
            }
        } catch (error) {
            console.log(error)
        }
    })
}

export const runGroupWellnessDataAggregation = functions.https._onCallWithOptions(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            // console.log('context.instanceIdToken => ', context.instanceIdToken)
            // console.log('context.auth => ', context.auth)
            // console.log('context.auth.token => ', context.auth.token)
            // console.log('data => ', data)

            const directive = <WellGroupAggregationDirective>data
            console.log('request - payload', data)

            const { uid: AthleteUID } = context.auth

            if (
                !!directive &&
                !!directive.entries &&
                isArray(directive.entries)
            ) {
                const theUpdateResults = await Promise.all(
                    directive.entries.map(
                        async (biometricEntry, idx, allEntries) => {
                            const { logLevel } = directive.options

                            const aggregateDocumentManager = new AggregateDocumentManager()

                            if (biometricEntry) {
                                console.error('biometricEntry', biometricEntry)

                                // const transactionResult = await db.runTransaction(async t => {
                                biometricEntry.skipAggregate = true
                                const t = undefined

                                convertAllDatesStringsToTimeStamps(
                                    biometricEntry
                                )

                                const groupUpdateResults = await aggregateDocumentManager
                                    .doRead(
                                        biometricEntry,
                                        biometricEntry.userId || AthleteUID,
                                        DocMngrActionTypes.Read,
                                        'onCreate',
                                        logLevel,
                                        t
                                    )

                                    .then(docDirective => {
                                        LogInfo(
                                            `OnCreate Promise Received, aggregation all done for entry ${
                                                allEntries[idx].collection
                                            } (${idx + 1} of ${
                                                directive.entries.length
                                            })!!!!!!!!`,
                                            docDirective
                                        )
                                        return Promise.resolve(docDirective)
                                    })
                                    .catch(err => {
                                        debugger

                                        LogInfo(
                                            `updateBiometricEntries.onCreate() execution failed! - err -> ${err}`
                                        )
                                        return Promise.reject(
                                            `updateBiometricEntries.onCreate() execution failed! - err -> ${err}`
                                        )
                                    })

                                return groupUpdateResults

                                // }).then((results) => {
                                //     console.log('Transaction success!', results);
                                //     return true;
                                // }).catch(err => {
                                //     console.log('Transaction failure:', err);
                                //     return false
                                // });

                                // return transactionResult
                            } else {
                                console.error('No document to process')
                                return false
                            }
                        }
                    )
                )
                    .catch(err => {
                        console.error(
                            '.........  const theUpdateResults = await Promise.all() CATCH ERROR:',
                            err
                        )

                        return false
                    })
                    .then(() => true)

                return theUpdateResults
            } else {
                console.error('.........  No Entries in Payload')

                return false
            }
        } catch (error) {
            console.error('.........CATCH ERROR:', error)

            throw error
        }
    },
    { regions: Config.regions }
)
