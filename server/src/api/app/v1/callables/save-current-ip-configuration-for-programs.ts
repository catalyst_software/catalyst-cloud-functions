import { Athlete } from './../../../../models/athlete/athlete.model';
import * as functions from 'firebase-functions'

import { getAthleteDocRef } from '../../../../db/refs';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';

// export interface CurrentIpConfigurationForProgramsRequest {
//     athleteUid: string;

//     currentPrograms: Array<Program>;
//     currentIpScoreTracking?: IpScoreTracking;

//     saveIpScoreTracking?: boolean;
// }

interface SaveEntryEngagementForProgramsRequest {
    programIndex: number;
    contentIndex: number;
    entryIndex: number;
    engagement: boolean;
}

export const saveEntryEngagementForPrograms = functions.https.onCall(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            try {

                console.log('context.instanceIdToken => ', context.instanceIdToken)
                console.log('context.auth => ', context.auth)
                console.log('context.auth.token => ', context.auth.token)
                console.log('data => ', data)

                const {
                    programIndex,
                    contentIndex,
                    entryIndex,
                    engagement
                } = <SaveEntryEngagementForProgramsRequest>data

                const theAThleteRef = getAthleteDocRef(context.auth.uid)
                const theAThlete = await theAThleteRef.get().then(docSnap => getDocumentFromSnapshot(docSnap) as Athlete)

                theAThlete.currentPrograms[programIndex].content[contentIndex].entries[entryIndex].engagement = engagement

                await theAThleteRef.update(theAThlete)
                return {
                    message: 'Program Content Entry Engagement updated',
                    responseCode: 200,
                    data: theAThlete.currentPrograms[programIndex].content[contentIndex].entries[entryIndex]
                }

            } catch (error) {

                console.error('An unanticipated exception occurred in saveEntryEngagementForPrograms', error)
                return {
                    message:
                        error.message || 'An unanticipated exception occurred in saveEntryEngagementForPrograms',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'Could not update Program Content Entry Engagement ',
                        exception: error,
                    },
                    method: {
                        name: 'saveEntryEngagementForPrograms'
                    }
                }
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }
)


