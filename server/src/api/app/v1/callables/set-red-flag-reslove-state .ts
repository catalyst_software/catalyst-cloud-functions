import * as functions from 'firebase-functions'
import moment from 'moment';

import { db } from '../../../../shared/init/initialise-firebase';

import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { SetRedFlagResloveStateRequest } from '../../../../models/purchase/interfaces/SetRedFlagResloveStateRequest';
import { firestore } from 'firebase-admin';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { RedFlagType } from '../sendGrid/conrollers/email';
import { Config } from './../../../../shared/init/config/config';


export const getEnumNameString = (lifestyleEntryType: LifestyleEntryType) => {
    return LifestyleEntryType[lifestyleEntryType]
}

export type RedFlagDialogData = {
    // id?: number;
    flagID: string;
    flag: string; // 'Mood' / lifestyleEntryType
    value: number;
    flagType: RedFlagType; // Sad Entry
    time?: string;
    creationTimestamp: Date,
    userFullName: string;
    athleteUid: string;
    redFlagDismissed: boolean;
    hasRedFlagsForPeriod?: boolean;
}

export const removeDismissedRedFlag = (coachUID: string, redFlagDataItems: RedFlagDialogData[]): Promise<{ updated: boolean, error: any }> => {
    return db
        .collection(FirestoreCollection.Athletes)
        .doc(coachUID)
        .collection('dashboardConfig')
        .doc('redFlags')
        .set({ dismissedItems: redFlagDataItems }, { merge: true })
        .then(() => {
            return { updated: true, error: undefined };
        })
        .catch(e => {
            return { updated: false, error: e };
        });
}

export const addDismissedRedFlag = async (coachUID: string, redFlagDataItem: RedFlagDialogData): Promise<{ updated: boolean, error: any }> => {

    return db
        .collection(FirestoreCollection.Athletes)
        .doc(coachUID)
        .collection('dashboardConfig')
        .doc('redFlags')
        .update({ dismissedItems: firestore.FieldValue.arrayUnion(redFlagDataItem) })
        // .set({ dismissedItems: redFlagDataItems }, { merge: true })
        .then(() => {
            return { updated: true, error: undefined };
        })
        .catch(e => {
            return { updated: false, error: e };
        });
}

export const getDismissedRedFlags = (coachUID: string): Promise<RedFlagDialogData[]> => {
    const dismissedRedFlagDocuments = db.collection(FirestoreCollection.Athletes)
        .doc(coachUID)
        .collection('dashboardConfig')
        .doc('redFlags');
    return dismissedRedFlagDocuments.get().then((docSnap) => {
        // dismissedItems: RedFlagDialogData[] 
        const data = docSnap.data() as { dismissedItems: RedFlagDialogData[] };
        if (data) {

            data.dismissedItems.forEach((item) => {
                const time = moment((item as any).creationTimestamp.toDate()).format('hh:mm A, ddd DD MMM YYYY');


                item.time = time;
            })
            return {
                ...data.dismissedItems || [],
            };
        } else {
            return [];
        }

    });
}

export const setRedFlagResloveState = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            // console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            // console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            let {
                ...redFlagDataItem
            } = <SetRedFlagResloveStateRequest>data
            console.log('validateReceipt - data', data)

            const coachUid = context.auth.token.uid;

            let updateResult

            const time = moment(redFlagDataItem.creationTimestamp).format('hh:mm A, ddd DD MMM YYYY');
            const flagID = redFlagDataItem.athleteUid + '_' + time.replace(/ /g, '_') + '_' + redFlagDataItem.flagType.replace(/ /g, '_')

            const flagTypeString = getEnumNameString(redFlagDataItem.flag)

            if (redFlagDataItem.redFlagDismissed) {

                console.log('redFlagItem', {
                    ...redFlagDataItem,
                    flagID,
                    flag: flagTypeString,
                    creationTimestamp: moment(redFlagDataItem.creationTimestamp).toDate()
                })
                updateResult = await addDismissedRedFlag(coachUid, {
                    ...redFlagDataItem,
                    flagID,
                    flag: flagTypeString,
                    creationTimestamp: moment(redFlagDataItem.creationTimestamp).toDate()
                })
            } else {
                let dismissedItems = await getDismissedRedFlags(coachUid) // [...this.dismissedRedFlags || []]
                dismissedItems = dismissedItems.filter((item) =>
                    !(item.flagID === flagID))

                updateResult = await removeDismissedRedFlag(coachUid, dismissedItems);
            }


            return {
                message: 'setRedFlagResloveState',
                responseCode: 510,
                data: updateResult,
                error: undefined,
                method: {
                    name: 'setRedFlagResloveState',
                    line: 42,
                },
            }


        } catch (error) {

            console.error('.........CATCH ERROR - something went wrong...', error)
            return {
                message: 'setRedFlagResloveState',
                responseCode: 510,
                // data: response,
                error,
                method: {
                    name: 'setRedFlagResloveState',
                    line: 42,
                },
            }

        }
    }, {regions: Config.regions}
)


