import { ManageAthleteRemoveGroupRequest } from "../../../../models/report-model";
import { Group } from "../../../../models/group/interfaces/group";
import { getGroupDocRef, getAthleteDocRef } from "../../../../db/refs";
// import { Athlete } from "../../../../models/athlete/athlete.model";
import { Athlete } from '../../../../models/athlete/interfaces/athlete';

import { cloneDeep } from "lodash";
import { getUniqueListBy } from "./utils/getUniqueListBy";
import { AthleteIndicator } from "../../../../db/biometricEntries/well/interfaces/athlete-indicator";
import { GroupIndicator } from "../../../../db/biometricEntries/well/interfaces/group-indicator";
import { Config } from './../../../../shared/init/config/config';

const functions = require('firebase-functions');

export const removeAthleteFromGroup = functions.https._onCallWithOptions(
  async (data: ManageAthleteRemoveGroupRequest, context) => {

    try {

      if (!context.auth.token.uid) {
        console.error(
          'Request not authorized. User must logged in to fulfill this request'
        )
        return {
          error:
            'Request not authorized. User must logged in to fulfill this request ' +
            data,
        }
      }

      // const { uid, reportType, requestDateTime: requestDateTimeString, requestProcessed, requestSuccess, requestProcessedDateTime, requestingUser, entity, errorLogs } = data;

      const { groupId, userId } = <{
        groupId: string, userId: string, allowGroupDelete: boolean
      }>data;

      console.log(data);

      const pass = true;

      if (pass && groupId && userId) {

        const theGroupSnap = getGroupDocRef(groupId)
        console.log('Fetching Group')

        const theGroup = await theGroupSnap
          .get()
          .then(async (groupSnap) => {
            if (groupSnap.exists) {

              const group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
              if (!group.athletes) {
                group.athletes = []
              }
              console.log('Group Found')
              return group

            } else {
              // console.error({ message: 'No ID supplied' })

              throw new Error(`Group Not Found with UID = (groupID) => ${groupId}`)

            }
          }).catch((err) => {
            // return { error: err }
            throw { message: 'Unable to GET Group Doc', error: err }
          })

        const currentAthletes = cloneDeep(theGroup.athletes).filter(ath => ath.uid !== userId);

        const groupAthletes = getUniqueListBy(currentAthletes, 'uid') as AthleteIndicator[]

        if (currentAthletes.length !== groupAthletes.length) {
          console.warn('Duplicate Athelete Indicators Removed')
        }
        await theGroupSnap.update({ athletes: groupAthletes })
          .catch((e) => {
            throw { message: 'Unable to UPDATE Group Doc', error: e }

          });

        console.log('Group Update')

        const atheleteRef = getAthleteDocRef(userId);// getAthleteDocRef('5FqVZGNUqVV9rPpo48CtqPB8xYE3').get()

        console.log('Fetching Athlete')

        const theAthlete = await atheleteRef.get()
          .then(async (athleteSnap) => {
            if (athleteSnap.exists) {
              console.log('Athlete Found')

              const ath = { ...athleteSnap.data(), uid: athleteSnap.id } as Athlete;
              debugger;

              if (!ath.groups) {
                ath.groups = []
              }
              console.log({ currentGroups: ath.groups })

              return ath
            } else {
              console.error({ message: `Can't find athlete with UID ${userId}` })
              throw { error: `Can't find athlete with UID ${userId}` }
            }

          })

        const clonedGroups = cloneDeep(theAthlete.groups)

        const athleteGroups = getUniqueListBy(clonedGroups, 'groupId') as GroupIndicator[]

        if (athleteGroups.length !== clonedGroups.length) {
          console.warn('dup groups removed', {
            groupAthletesWithDups: clonedGroups.map((a) => {
              return { uid: a.uid, name: a.groupName }
            }),
            groupAthletesCleaned: athleteGroups.map((a) => {
              return { uid: a.uid, name: a.groupName }
            })
          })
        }

        const updatedGroups = athleteGroups.filter((g) => g.groupId !== groupId).map((grp) => {
          const { athletes, uid, ...theGroupRes } = grp;
          return theGroupRes
        })

        console.log({ updatedGroups })

        const theRes = await atheleteRef.update({ groups: updatedGroups })

          .then(() => {
            console.log('Athlete Updated')

            return {
              message: 'Athlete Removed from Group',
              responseCode: 201,
              data: {
                group: theGroup,
              },
              method: {
                name: 'removeAthleteFromGroup',
                line: 58,
              },
            }
          })
          .catch((err) => {
            return { message: 'Unable to update athlete group details', err }
          })

        console.log({ theRes })

        return theRes
        // return 'Athlete Updated'

      } else {
        console.error({ message: 'No ID supplied' })

        return { message: 'No ID supplied' }
      }
    }
    catch (e) {
      console.error({ e })

      throw e

    }
  }, {regions: Config.regions})

