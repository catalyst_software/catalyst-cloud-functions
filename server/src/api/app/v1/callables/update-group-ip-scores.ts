
import * as functions from 'firebase-functions'

import { updateAthleteIndicatorsIPScoreOnGroup } from './utils/update-athlete-indicators-ipscore-on-group';

export const updateGroupIpScores = functions.https.onCall(
    
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('data => ', data)

            const { groupId }
                = <{ groupId: string }>data

            console.log('groupid ', groupId)

            // TODO: Pass utcOffset
            return await updateAthleteIndicatorsIPScoreOnGroup(groupId, 0).then((results) => {

                return true //results
            }).catch((err) => {
                return err

            })

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }
)
