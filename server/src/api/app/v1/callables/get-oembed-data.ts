import * as functions from 'firebase-functions'
import { Config } from './../../../../shared/init/config/config';

const oembed = require("oembed-auto");

export interface GetOembedPayload {
    url: string;
}

export const getOembedData = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            try {
                const { url }
                    = <GetOembedPayload>data

                const promise = new Promise((resolve, reject) => {

                    oembed(url, (err, resData) => {
                        if (err) {
                            console.error('oEmbed error occured', err)

                            reject(err)
                        }
                        console.log('==============================>>>>>>>>>>>>>>>>>>>>>>>  oEmbed DATA!!!!!!!!');
                        console.log(resData);
                        resolve(resData)
                    });
                });

                // promise.then(function (value) {
                //     console.log(value);
                //     // expected output: "foo"
                // });



                return promise;

            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return Promise.reject({
                    message:
                        'An unanticipated exception occurred in getOembedData',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in getOembedData',
                        exception: exception,
                    },
                    method: {
                        name: 'getOembedData'
                    }
                })
            }


        } catch (error) {
    console.error(
        '.........CATCH ERROR:',
        error
    )

    return Promise.reject(error)
}
    }, {regions: Config.regions}
)

