import { firestore } from 'firebase-admin';
import * as functions from 'firebase-functions'

import { getNotificationCardMetadataRef, getNotificationCardRef } from '../../../../db/refs';
import { UpdateNotificationCardMetadataDirective, NotificationCardMetadataActionType } from "../../../../models/enums/UpdateNotificationCardMetadataDirective";
import { NotificationCardPackageBase, NotificationCardState } from '../../../../models/report-model';
import moment from 'moment';
import { Config } from './../../../../shared/init/config/config';


export const updateNotificationCardMetadata = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            console.log('data => ', data)

            try {
                const { cardUid,
                    athleteUid,
                    // notiticationCardType,
                    actionType }
                    = <UpdateNotificationCardMetadataDirective>data







                const theCardRef = getNotificationCardRef().doc(cardUid)



                const theCard = await theCardRef.get().then((docSnap) => {
                    return {
                        ...docSnap.data(),
                        uid: docSnap.id
                    } as NotificationCardPackageBase
                })

                console.log('theCard.recipients', { recipients: theCard.recipients })





                const notificationCardMetadataRef = getNotificationCardMetadataRef()

                switch (actionType) {
                    case NotificationCardMetadataActionType.UpdateAcknowledgmentCount:
                        await notificationCardMetadataRef.doc(cardUid).set({ acknowledgmentCount: firestore.FieldValue.increment(1) }, { merge: true })
                       
                        theCard.recipients.find((r) => r.uid === athleteUid).state = NotificationCardState.Acknowledged
                        theCard.recipients.find((r) => r.uid === athleteUid).acknowledgeTimestamp = moment().toDate()

                        console.log('theCard.recipients updated', { recipients: theCard.recipients })

                        await theCardRef.update({
                            recipients: theCard.recipients
                        })
                        return true
                    case NotificationCardMetadataActionType.UpdateImpressionCount:
                        await notificationCardMetadataRef.doc(cardUid).set({ impressionCount: firestore.FieldValue.increment(1) }, { merge: true })

                        return true

                    default:
                        return false
                }






            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return {
                    message:
                        'An unanticipated exception occurred in updateNotificationCardMetadata',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in updateNotificationCardMetadata',
                        exception: exception,
                    },
                    method: {
                        name: 'updateNotificationCardMetadata'
                    }
                }
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)
