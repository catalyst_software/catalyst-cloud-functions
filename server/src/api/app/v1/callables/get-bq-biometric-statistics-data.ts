import * as functions from 'firebase-functions'
import cubejs from '@cubejs-client/core';
import moment from 'moment';
// tslint:disable-next-line: no-duplicate-imports
import { Filter, Query, ResultSet, TimeDimension } from '@cubejs-client/core';
import { getOlapJwtPackage, getSharedSystemConfiguration } from './utils/warehouse/olap/get-olap-jwt-package';
import { isArray } from 'lodash';
import { OlapGetStatisticsRequest, OlapGetLifestyleEntryTypeRequest } from './utils/warehouse/olap/interfaces/olap-get-statistics-request.interface';
import { LifestyleEntryStatisticsMetadataResponse, BiometricStatisticsResponse, LifestyleEntryStatisticsResponse } from './utils/warehouse/olap/interfaces/biometrics-statistics-response.interface';
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type';
import { OlapCubeEntityType } from '../../../../models/enums/olap-entity-type';


export const getAthleteBiometricStatisticsFromCubeJs = async (request: OlapGetLifestyleEntryTypeRequest): Promise<LifestyleEntryStatisticsResponse> => {
    debugger;
    let results: ResultSet<any>;
    const config = await getSharedSystemConfiguration();
    const olapTokenPackage = await getOlapJwtPackage(request.requestorSecurityProfile, config);
    console.log(`OlapTokenPackage: ${JSON.stringify(olapTokenPackage)}`);

    const cubejsApi = cubejs(olapTokenPackage.token, {
        apiUrl: config.olap.apiUrl
    });
    const startDate = moment(request.startDate).startOf('day');
    const endDate = moment(request.endDate).endOf('day');
    let measure = request.measure || 'totalValue';
    measure = `${request.factTable}.${measure}`;

    const query = {
        measures: [measure],
        dimensions: [`${request.factTable}.athleteId`],
        filters: [{
                dimension: `${request.factTable}.athleteId`,
                operator: 'equals',
                values: [request.entityId]
            } as unknown as Filter, 
            {
                dimension: `${request.factTable}.lifestyleEntryType`,
                operator: 'equals',
                values: [request.lifestyleEntryType]
            } as unknown as Filter, 
            {
                dimension: `${request.factTable}.segmentId`,
                operator: 'notSet'
            } as Filter],
        timeDimensions: [{
            dimension: `${request.factTable}.created`,
            dateRange: [startDate.format('YYYY-MM-DDTHH:mm:ss'), endDate.format('YYYY-MM-DDTHH:mm:ss')],
            granularity: request.granularity || 'day'
        } as TimeDimension],
        order: {}
    } as Query;

    query.order[`${request.factTable}.created`] = 'asc';

    const response = {
        data: [],
        metadata: {
            entityId: request.entityId,
            entityType: request.entityType,
            lifestyleEntryType: request.lifestyleEntryType,
            measure: measure.split('.')[1],
            startDate: startDate.format('YYYY-MM-DDTHH:mm:ss'),
            endDate: endDate.format('YYYY-MM-DDTHH:mm:ss')
        } as unknown as LifestyleEntryStatisticsMetadataResponse
    } as unknown as LifestyleEntryStatisticsResponse;

    if (!!request.lifestyleEntrySubType1 && (request.factTable === 'ComplexBiometricFact' || request.factTable === 'SimpleFoodFact')) {
        response.metadata.lifestyleEntrySubType1 = request.lifestyleEntrySubType1;
        query.filters.push({
            dimension: `${request.factTable}.lifestyleEntrySubType1`,
            operator: 'equals',
            values: [request.lifestyleEntrySubType1]
        } as unknown as Filter);
    }

    if (!!request.lifestyleEntrySubType2 && request.factTable === 'ComplexBiometricFact') {
        response.metadata.lifestyleEntrySubType2 = request.lifestyleEntrySubType2;
        query.filters.push({
            dimension: `${request.factTable}.lifestyleEntrySubType2`,
            operator: 'equals',
            values: [request.lifestyleEntrySubType2]
        } as unknown as Filter);
    }

    results = await cubejsApi.load(query);
    const rows = (results as any).loadResponse.data;
    const metadata = response.metadata;

    while (startDate.diff(endDate.startOf('day')) <= 0) {
        const date = startDate.startOf('day').format('YYYY-MM-DDTHH:mm:ss.SSS');
        const entry = rows.find((e: any) => {
            return e[`${request.factTable}.created.day`] === date;
        });
        let val = (!!entry) ? entry[measure] : 0;
    
        if (metadata.measure === 'count') {
            if (metadata.lifestyleEntryType === LifestyleEntryType.Sick || metadata.lifestyleEntryType === LifestyleEntryType.Period) {
                val = (val > 0) ? Math.ceil(val / 2) : 0;
            }
        }
        response.data.push(val);
        startDate.add(1, 'day');
    }

    return Promise.resolve(response);
}

export const getSegmentBiometricStatisticsFromCubeJs = async (request: OlapGetLifestyleEntryTypeRequest): Promise<LifestyleEntryStatisticsResponse> => {
    debugger;
    let results: ResultSet<any>;
    const config = await getSharedSystemConfiguration();
    const olapTokenPackage = await getOlapJwtPackage(request.requestorSecurityProfile, config);
    console.log(`OlapTokenPackage: ${JSON.stringify(olapTokenPackage)}`);

    const cubejsApi = cubejs(olapTokenPackage.token, {
        apiUrl: config.olap.apiUrl
    });
    const startDate = moment(request.startDate).startOf('day');
    const endDate = moment(request.endDate).endOf('day');
    let measure = request.measure || 'totalValue';
    measure = `${request.factTable}.${measure}`;

    const query = {
        measures: [measure],
        dimensions: [`${request.factTable}.segmentId`],
        filters: [{
                dimension: `${request.factTable}.segmentId`,
                operator: 'equals',
                values: [request.entityId]
            } as unknown as Filter, 
            {
                dimension: `${request.factTable}.lifestyleEntryType`,
                operator: 'equals',
                values: [request.lifestyleEntryType]
            } as unknown as Filter
        ],
        timeDimensions: [{
            dimension: `${request.factTable}.created`,
            dateRange: [startDate.format('YYYY-MM-DDTHH:mm:ss'), endDate.format('YYYY-MM-DDTHH:mm:ss')],
            granularity: request.granularity || 'day'
        } as TimeDimension],
        order: {}
    } as Query;

    query.order[`${request.factTable}.created`] = 'asc';

    const response = {
        data: [],
        metadata: {
            entityId: request.entityId,
            entityType: request.entityType,
            lifestyleEntryType: request.lifestyleEntryType,
            measure: measure.split('.')[1],
            startDate: startDate.format('YYYY-MM-DDTHH:mm:ss'),
            endDate: endDate.format('YYYY-MM-DDTHH:mm:ss')
        } as unknown as LifestyleEntryStatisticsMetadataResponse
    } as unknown as LifestyleEntryStatisticsResponse;

    if (!!request.lifestyleEntrySubType1 && (request.factTable === 'ComplexBiometricFact' || request.factTable === 'SimpleFoodFact')) {
        response.metadata.lifestyleEntrySubType1 = request.lifestyleEntrySubType1;
        query.filters.push({
            dimension: `${request.factTable}.lifestyleEntrySubType1`,
            operator: 'equals',
            values: [request.lifestyleEntrySubType1]
        } as unknown as Filter);
    }

    if (!!request.lifestyleEntrySubType2 && request.factTable === 'ComplexBiometricFact') {
        response.metadata.lifestyleEntrySubType2 = request.lifestyleEntrySubType2;
        query.filters.push({
            dimension: `${request.factTable}.lifestyleEntrySubType2`,
            operator: 'equals',
            values: [request.lifestyleEntrySubType2]
        } as unknown as Filter);
    }

    results = await cubejsApi.load(query);
    const rows = (results as any).loadResponse.data;
    const metadata = response.metadata;

    while (startDate.diff(endDate.startOf('day')) <= 0) {
        const date = startDate.startOf('day').format('YYYY-MM-DDTHH:mm:ss.SSS');
        const entry = rows.find((e: any) => {
            return e[`${request.factTable}.created.day`] === date;
        });
        let val = (!!entry) ? entry[measure] : 0;
    
        if (metadata.measure === 'count') {
            if (metadata.lifestyleEntryType === LifestyleEntryType.Sick || metadata.lifestyleEntryType === LifestyleEntryType.Period) {
                val = (val > 0) ? Math.ceil(val / 2) : 0;
            }
        }
        response.data.push(val);
        startDate.add(1, 'day');
    }

    return Promise.resolve(response);
}

export const getBqBiometricsStatisticsData = functions.https.onCall(async (data, context) => {
    try {
        if (!context.auth.token.uid) {
            console.error(
                'Request not authorized. User must logged in to fulfill this request'
            )
            return {
                error:
                    'Request not authorized. User must logged in to fulfill this request ' +
                    data
            }
        }

        console.log('context.instanceIdToken => ', context.instanceIdToken);
        console.log('context.auth => ', context.auth);
        console.log('context.auth.token => ', context.auth.token);
        console.log('data => ', data);

        try {
            const request = <OlapGetStatisticsRequest>data;
            const measure = request.measure || '';
            
            const response = {
                datasets: []
            } as BiometricStatisticsResponse;

            const lifestyleEntryTypeRequest = {
                requestorSecurityProfile: request.requestorSecurityProfile,
                entityId: '',
                entityType: request.entityType || 'athlete',
                lifestyleEntryType: request.lifestyleEntryType,
                startDate: request.startDate,
                endDate: request.endDate
            } as unknown as OlapGetLifestyleEntryTypeRequest;
            
            if (isArray(request.entityIds)) {
                let dataset: LifestyleEntryStatisticsResponse;
                for (const id of request.entityIds) {
                    lifestyleEntryTypeRequest.entityId = id;
                    switch (lifestyleEntryTypeRequest.lifestyleEntryType) {
                        case LifestyleEntryType.Sleep:
                        case LifestyleEntryType.Distance:
                        case LifestyleEntryType.Steps:
                            lifestyleEntryTypeRequest.factTable = 'ScalarBiometricFact';
                            lifestyleEntryTypeRequest.measure = measure || 'totalValue';
                            if (lifestyleEntryTypeRequest.entityType === OlapCubeEntityType.Athlete) {
                                dataset = await getAthleteBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            } else {
                                dataset = await getSegmentBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            }
                            response.datasets.push(dataset);
                            break;
                        case LifestyleEntryType.Mood:
                        case LifestyleEntryType.Fatigue:
                        case LifestyleEntryType.HeartRate:
                            lifestyleEntryTypeRequest.factTable = 'ScalarBiometricFact';
                            lifestyleEntryTypeRequest.measure = measure || 'averageValue';
                            if (lifestyleEntryTypeRequest.entityType === OlapCubeEntityType.Athlete) {
                                dataset = await getAthleteBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            } else {
                                dataset = await getSegmentBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            }
                            response.datasets.push(dataset);
                            break;
                        case LifestyleEntryType.Period:
                        case LifestyleEntryType.Sick:
                            lifestyleEntryTypeRequest.factTable = 'ScalarBiometricFact';
                            lifestyleEntryTypeRequest.measure = measure || 'count';
                            if (lifestyleEntryTypeRequest.entityType === OlapCubeEntityType.Athlete) {
                                dataset = await getAthleteBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            } else {
                                dataset = await getSegmentBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            }
                            response.datasets.push(dataset);
                            break;
                        case LifestyleEntryType.Pain:
                            lifestyleEntryTypeRequest.factTable = 'ComplexBiometricFact';
                            lifestyleEntryTypeRequest.measure = measure || 'averageValue';
                            if (lifestyleEntryTypeRequest.entityType === OlapCubeEntityType.Athlete) {
                                dataset = await getAthleteBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            } else {
                                dataset = await getSegmentBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            }
                            response.datasets.push(dataset);
                            break;
                        case LifestyleEntryType.Training:
                            lifestyleEntryTypeRequest.factTable = 'ComplexBiometricFact';
                            lifestyleEntryTypeRequest.measure = measure || 'totalValue';
                            if (lifestyleEntryTypeRequest.entityType === OlapCubeEntityType.Athlete) {
                                dataset = await getAthleteBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            } else {
                                dataset = await getSegmentBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            }
                            response.datasets.push(dataset);
                            break;
                        case LifestyleEntryType.SimpleFood:
                            lifestyleEntryTypeRequest.factTable = 'SimpleFoodFact';
                            lifestyleEntryTypeRequest.measure = measure || 'totalValue';
                            if (lifestyleEntryTypeRequest.entityType === OlapCubeEntityType.Athlete) {
                                dataset = await getAthleteBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            } else {
                                dataset = await getSegmentBiometricStatisticsFromCubeJs(lifestyleEntryTypeRequest);
                            }
                            response.datasets.push(dataset);
                            break;
                    }
                        
                }
            }

            return response;

        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return Promise.reject({
                message:
                    'An unanticipated exception occurred in getBqBiometricsStatisticsData',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in getBqBiometricsStatisticsData',
                    exception: exception,
                },
                method: {
                    name: 'getBqBiometricsStatisticsData '
                }
            })
        }


    } catch (error) {
        console.error(
            '.........CATCH ERROR:',
            error
        )

        return Promise.reject(error)
    }
});

