import { OlapAthleteLeaderboardEntry } from './utils/warehouse/olap/interfaces/olap-athlete-leaderboard-entry.interface';
import * as functions from 'firebase-functions'
import cubejs from '@cubejs-client/core';
import moment from 'moment';
// tslint:disable-next-line: no-duplicate-imports
import { Filter, Query, ResultSet, TimeDimension } from '@cubejs-client/core';
import { OlapGetLeaderboardRequest } from './utils/warehouse/olap/interfaces/olap-get-leaderboard-request.interface';
import { getOlapJwtPackage, getSharedSystemConfiguration } from './utils/warehouse/olap/get-olap-jwt-package';
import { DistanceLeaderboardRecord, DistanceLeaderboardResponse } from './utils/warehouse/olap/interfaces/distance-leaderboard-response.interface';
import { QueryRowsResponse } from '@google-cloud/bigquery';
import { bigqueryClient } from './utils/big-query/initBQ';
import { isArray } from 'lodash';

export const getAthleteDistanceRanking = async (athleteId: string, startDate: string, endDate: string): Promise<string> => {
    let query = 'WITH subQ1 AS (SELECT * ';
    query += 'FROM UNNEST(ARRAY( ';
    query += '  SELECT athlete_id FROM catalystDW.biometricFact ';
    query += '  where segment_id is null and ';
    query += `  created_at >= '${startDate}' and created_at <= '${endDate}' `;
    query += '  group by athlete_id ';
    query += '  order by sum(value) desc ';
    query += ')) AS athlete_id WITH OFFSET off) ';
    query += 'select * from subQ1 ';
    query += `where athlete_id = '${athleteId}' `;

    console.log(`Querying for athlete distance rank with sql: ${query}`);

    const data: QueryRowsResponse  = await bigqueryClient.query(query);
    const rows = data[0] as Array<{athlete_id: string, off: number}>;
    let val = 'Last';
    if (rows.length) {
        val = (rows[0].off + 1) + '';
    }

    return Promise.resolve(val);
}

export const getDistanceLeaderboardFromCubeJs = async (request: OlapGetLeaderboardRequest): Promise<DistanceLeaderboardResponse> => {
    let results: ResultSet<any>;
    const config = await getSharedSystemConfiguration();
    const olapTokenPackage = await getOlapJwtPackage(request.requestorSecurityProfile, config);
    console.log(`OlapTokenPackage: ${JSON.stringify(olapTokenPackage)}`);

    const cubejsApi = cubejs(olapTokenPackage.token, {
        apiUrl: config.olap.apiUrl
    });

    // Org Top Ten Query
    let query = {
        measures: ['ScalarBiometricFact.totalValue'],
        dimensions: ['ScalarBiometricFact.athleteId','AthleteDim.firstName','AthleteDim.lastName','AthleteDim.imageUrl','AthleteDim.sport','AthleteDim.bio'],
        filters: [{
            dimension: 'ScalarBiometricFact.segmentId',
            operator: 'equals',
            values: [request.queryGroupId]
        } as Filter, {
            dimension: 'ScalarBiometricFact.athleteId',
            operator: 'set'
        } as Filter, {
            dimension: 'ScalarBiometricFact.totalValue',
            operator: 'gt',
            values: ['0']
        } as Filter],
        timeDimensions: [{
            dimension: 'ScalarBiometricFact.created',
            dateRange: [request.startDate, request.endDate],
            granularity: request.granularity || 'day'
        } as TimeDimension],
        order: {
            'ScalarBiometricFact.totalValue': 'desc'
        },
        limit: 10
    } as Query;

    const response = {
        topTen: [],
        startDate: request.startDate,
        endDate: request.endDate,
        requestorSecurityProfile: undefined,
        distance: 0,
        totalDistance: 0,
        rank: ''
    } as unknown as DistanceLeaderboardResponse;

    results = await cubejsApi.load(query);
    response.topTen = (results as any).loadResponse.data || [];

    if (!request.excludeAthleteData) {
        // Athlete Today Distance Query
        const today = moment().format('YYYY-MM-DD');
        let startDate = `${today}T00:00:00`;
        let endDate = `${today}T23:59:59`;
        query = {
            measures: ['ScalarBiometricFact.totalValue'],
            dimensions: ['ScalarBiometricFact.athleteId','AthleteDim.firstName','AthleteDim.lastName','AthleteDim.imageUrl','AthleteDim.sport','AthleteDim.bio'],
            filters: [{
                dimension: 'ScalarBiometricFact.athleteId',
                operator: 'equals',
                values: [request.requestorSecurityProfile.athleteId]
            } as Filter, {
                dimension: 'ScalarBiometricFact.segmentId',
                operator: 'notSet'
            }],
            timeDimensions: [{
                dimension: 'ScalarBiometricFact.created',
                dateRange: [startDate, endDate],
                granularity: request.granularity || 'day'
            } as TimeDimension]
        } as Query;

        results = await cubejsApi.load(query);
        let rows = (results as any).loadResponse.data;
        response.distance = isArray(rows) && rows.length ? rows[0]['ScalarBiometricFact.totalValue'] : 0;

        // Athlete Overall Distance Query
        startDate = config.event.startDate;
        endDate = config.event.endDate;
        query = {
            measures: ['ScalarBiometricFact.totalValue'],
            dimensions: ['ScalarBiometricFact.athleteId','AthleteDim.firstName','AthleteDim.lastName','AthleteDim.imageUrl','AthleteDim.sport','AthleteDim.bio'],
            filters: [{
                dimension: 'ScalarBiometricFact.athleteId',
                operator: 'equals',
                values: [request.requestorSecurityProfile.athleteId]
            } as Filter, {
                dimension: 'ScalarBiometricFact.segmentId',
                operator: 'notSet'
            }],
            timeDimensions: [{
                dimension: 'ScalarBiometricFact.created',
                dateRange: [startDate, endDate],
                granularity: 'month'
            } as TimeDimension]
        } as Query;

        results = await cubejsApi.load(query);
        rows = (results as any).loadResponse.data;
        response.totalDistance = isArray(rows) && rows.length ? rows[0]['ScalarBiometricFact.totalValue'] : 0;
        
        response.rank = await getAthleteDistanceRanking(request.requestorSecurityProfile.athleteId, request.startDate, request.endDate);
        response.requestorSecurityProfile = request.requestorSecurityProfile;
    }
    
    return Promise.resolve(response);
}

export const getBqDistanceLeaderboardData = functions.https.onCall(async (data, context) => {
    try {
        if (!context.auth.token.uid) {
            console.error(
                'Request not authorized. User must logged in to fulfill this request'
            )
            return {
                error:
                    'Request not authorized. User must logged in to fulfill this request ' +
                    data
            }
        }

        console.log('context.instanceIdToken => ', context.instanceIdToken);
        console.log('context.auth => ', context.auth);
        console.log('context.auth.token => ', context.auth.token);
        console.log('data => ', data);

        try {
            const request = <OlapGetLeaderboardRequest>data;
            return await getDistanceLeaderboardFromCubeJs(request);

        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return Promise.reject({
                message:
                    'An unanticipated exception occurred in getBqDistanceLeaderboardData',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in getBqDistanceLeaderboardData',
                    exception: exception,
                },
                method: {
                    name: 'getBqDistanceLeaderboardData'
                }
            })
        }


    } catch (error) {
        console.error(
            '.........CATCH ERROR:',
            error
        )

        return Promise.reject(error)
    }
});

