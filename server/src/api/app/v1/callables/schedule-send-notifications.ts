import * as functions from 'firebase-functions'

// import { InAppMessageTemplate } from '../sendGrid/templates/in-app-message-template';
import { handleInAppNotifications } from './utils/comms/handle-comms';
import { PushNotificationType } from '../../../../models/enums/push-notification-type';
import { AthleteIndicator } from '../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { NotificationCardType } from '../../../../models/enums/UpdateNotificationCardMetadataDirective';
import { PushNotificationsType } from '../../../../models/enums/enums.model';
import { Config } from './../../../../shared/init/config/config';

export interface InAppMessageTemplate {
    senderName: string;
    recipients: Array<AthleteIndicator>;
    cardType: NotificationCardType;
    cardTitle: string,
    pushNotificationType: PushNotificationType;
    notificationPath: string;
    icon: PushNotificationsType;
    sendSMS: boolean;
    useTopic?: boolean
    topicIds?: string[];
    appIdentifier?: string;
}

export const scheduleSendNotifications = functions.https._onCallWithOptions(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('data => ', data)


            const { payload }
                = <{ payload: InAppMessageTemplate }>data

            console.log('THE REQ PAYLOAD', payload)
            const response = await handleInAppNotifications({
                senderName: payload.senderName,
                recipients: payload.recipients,
                cardType: payload.cardType,
                cardTitle: payload.cardTitle,
                notificationPath: payload.notificationPath,
                notificationType: PushNotificationType.NewContentFeedItem,
                icon: payload.icon,
                sendSMS: payload.sendSMS || false,
                useTopic: payload.useTopic || false,
                topicIds: payload.topicIds,
                appIdentidier: payload.appIdentifier

            });

            console.log('the scheduleSendNotifications Result', response)

            return true


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)

