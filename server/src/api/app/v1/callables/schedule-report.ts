import { firestore } from 'firebase-admin';
import moment from 'moment';
import * as functions from 'firebase-functions'

import { OnboardingValidationRequest } from '../interfaces/onboarding-validation-request';
import { validateOrgCode } from './utils/onboarding/validate-org-code';
import { sanitizeOnboardingCode } from './utils/onboarding/sanitize-onboarding-code';
import { createEntities } from './utils/onboarding/create-entities';
import { saveAthleteProfile } from './saveAthleteProfile';
import { getUserByID } from '../controllers/get-user-by-id';
import { OnboardingValidationResponse } from '../interfaces/onboarding-validation-response';
import { ReportRequest } from '../../../../models/report-model';
import { generateReportRequest } from "../controllers/utils/reports/generateReportRequest";
import { db } from '../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { isProductionProject } from '../is-production';
import { createTask } from '../../../../db/reports/createTask';
import { Config } from './../../../../shared/init/config/config';

const generateTaskCode = (uid: string) => {
    const uidSlice = uid.slice(0, 5);
    const code = Math.random().toString(36).substring(6, 12);
    return Promise.resolve(uidSlice + '_' + code)
    // return this.db.collection(this.collection)
    //   .where('organizationCode', '==', code)
    //   .get()
    //   .then((snapshot) => {

    //     if (snapshot.empty) {
    //       return code
    //     }

    //     else {
    //       return undefined
    //     }
    //   })
}


export const scheduleReport = functions.https._onCallWithOptions(

    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('context.auth.token => ', context.auth.token)
            // console.log('data => ', data)


            // return generateTaskCode().then((taskId) => {

            // //    const report.taskId = taskId;

            //     console.log('taskId', taskId);

            //     return {
            //         success: true,
            //         message: 'Task Added',
            //         taskId
            //     }
            //     // return db
            //     //     .collection(FirestoreCollection.Reports)
            //     //     .add(report)
            //     //     .then((docRef: firestore.DocumentReference) => {

            //     //         const isProd = false;
            //     //         let project = '';
            //     //         let queue = '';
            //     //         let location = '';
            //     //         let options: { payload: ReportRequest; };
            //     //         // // PRODUCTION
            //     //         if (isProductionProject() || isProd) {
            //     //             project = 'inspire-219716';
            //     //             queue = 'create-reports-queue';
            //     //             location = 'europe-west3';
            //     //             options = { payload: report };
            //     //         } else {
            //     //             project = 'inspire-1540046634666';
            //     //             queue = 'create-reports-queue';
            //     //             location = 'us-central1';
            //     //             options = { payload: report };
            //     //         }

            //     //         console.log('options', options);
            //     //         return createTask(project, location, queue, options)
            //     //             .then((data) => {
            //     //                 return me.handleResponse({
            //     //                     message: 'Report added',
            //     //                     responseCode: 201,
            //     //                     data: { taskId, data: data },
            //     //                     error: undefined,
            //     //                     method: {
            //     //                         name: 'addReport',
            //     //                         line: 58,
            //     //                     },
            //     //                     res: this.res
            //     //                 })
            //     //             })


            //     //     })
            //     //     .catch(err => {
            //     //         return res.status(502).json({
            //     //             message: 'Error adding Report',
            //     //             responseCode: 501,
            //     //             data: {
            //     //                 report: report,
            //     //             },
            //     //             error: err,
            //     //             method: {
            //     //                 name: 'getReport',
            //     //                 line: 58,
            //     //             },
            //     //         })
            //     //     })
            // })

            // const { document: report } = <{
            //     document: ReportRequest
            // }>data;
            const reportRequest = data;
            console.log('reportRequest => ', reportRequest)

            if (reportRequest) {

                return generateTaskCode(reportRequest.requestingUser.uid).then((taskId) => {

                    reportRequest.taskId = taskId

                    console.log('taskId', taskId);
                    return db
                        .collection(FirestoreCollection.Reports)
                        .add(reportRequest)
                        .then((docRef: firestore.DocumentReference) => {

                            const isProd = false;
                            let project = '';
                            let queue = '';
                            let location = '';
                            let options: { payload: ReportRequest; };
                            // // PRODUCTION
                            if (isProductionProject() || isProd) {
                                project = 'inspire-219716';
                                queue = 'create-reports-queue';
                                location = 'europe-west3';
                                options = { payload: reportRequest };
                            } else {
                                project = 'inspire-1540046634666';
                                queue = 'create-reports-queue';
                                location = 'us-central1';
                                options = { payload: reportRequest };
                            }

                            console.log('options', options);
                            return createTask(project, location, queue, options)
                                .then((theData) => {

                                    console.log('create response data', theData)
                                    return {
                                        message: 'Report Task added',
                                        success: true,
                                        taskId
                                    }
                                })


                        })
                        .catch(err => {
                            return {
                                message: 'Error adding Report Task',
                                success: false,
                                taskId
                            }
                        })
                })

            } else {
                return {
                    message: 'Error adding Report Task, reportRequest is undefined',
                    success: false,
                    // taskId: 
                }
            }



            const reportRequestResponse = await generateReportRequest(<ReportRequest>data);

            return reportRequestResponse;
            try {
                const { organizationCode: code, athlete, profile }
                    = <OnboardingValidationRequest>data

                const athletUID = context.auth.token.uid
                const user = await getUserByID(athletUID)

                const athleteIndicator = {
                    // organizationId: undefined,
                    // // organizationName: string;
                    // organizations: undefined,

                    // uid: '6eIhSgMOWnRprLql6mWF6QpOboB2',
                    // firstName: 'Ian Insp 2 Coach',
                    // lastName: 'Gouws',
                    // email: 'ian.inspire.2@gmx.com',

                    // profileImageURL: '',

                    // isCoach: undefined,
                    // includeGroupAggregation: undefined,

                    // ipScore: 0,
                    // isIpScoreIncrease: false,
                    // isIpScoreDecrease: false,
                    runningTotalIpScore: 0,

                    // recentEntries: undefined,

                    // subscriptionType: undefined,

                    // // // TODO: IG - Needed here?
                    firebaseMessagingIds: [context.instanceIdToken],
                    // commsConfig: undefined,

                    ...athlete,

                    creationTimestamp: firestore.Timestamp.fromDate(moment(user.metadata.creationTime).toDate()),
                }

                console.log('===========>>>>> athleteIndicator => ', athleteIndicator)
                console.log('athlete.creationTimestamp => ', athleteIndicator.creationTimestamp)
                const theCode = sanitizeOnboardingCode(code)

                athleteIndicator.ipScore = athleteIndicator.ipScore || 0
                athleteIndicator.runningTotalIpScore = athleteIndicator.runningTotalIpScore || 0
                let results // c
                if (theCode === 'INSPIRE') {
                    results = await createEntities(athleteIndicator, theCode)
                } else {
                    results = await validateOrgCode(theCode, athleteIndicator, 0)
                }
                const force = false

                if (force && profile && results.responseCode === 200) {
                    const saveProfileResults: OnboardingValidationResponse = await saveAthleteProfile(user, results, athleteIndicator, profile, context.instanceIdToken || 'UNKNOWN');
                    const responseWithAthlete = { ...results, ...saveProfileResults }

                    responseWithAthlete.data = { athlete: responseWithAthlete.data.athlete }
                    return responseWithAthlete
                } else {
                    const { athleteIndicator: toBeRemoved, ...theRestData } = results.data
                    console.warn('====>>>>> theRestData', theRestData)
                    const theOnboardingResponse = {
                        ...results, data: {
                            ...theRestData,
                            groups: theRestData.groups.map((group) => {
                                return {
                                    ...group,
                                    creationTimestamp: group.creationTimestamp.toDate().toJSON()
                                }
                            }),
                            onboardingProgram: {
                                ...theRestData.onboardingProgram,
                                activeDate: theRestData.onboardingProgram.activeDate.toDate().toJSON(),
                                // creationTimestamp: theRestData.onboardingProgram.creationTimestamp.toDate().toJSON(),
                                startDate: new Date().toJSON()
                            }
                            // onboardingProgram: {
                            //     ...theRestData.onboardingProgram,
                            //     startDate: theRestData.onboardingProgram.startDate.toDate().toJSON()
                            // }
                        }
                    }


                    return theOnboardingResponse
                }

            } catch (exception) {
                console.error('catchAll exception!!!! => ', exception)
                // return res.status(505).send(exception);
                return {
                    message:
                        'An unanticipated exception occurred in validateOrganizationCode',
                    responseCode: 500,
                    data: undefined,
                    error: {
                        message:
                            'An unanticipated exception occurred in validateOrganizationCode',
                        exception: exception,
                    },
                    method: {
                        name: 'validateOrganizationCode'
                    }
                }
            }

            // console.log('groupid ', groupId)

            // return await updateAthleteIndicatorsIPScoreOnGroup(groupId).then((results) => {

            //     return results
            // }).catch((err) => {
            //     return err

            // })

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )
            return {
                message: 'An exception occured  adding Report Task',
                success: false,
                error
                // taskId
            }
        }
    }, {regions: Config.regions}
)
