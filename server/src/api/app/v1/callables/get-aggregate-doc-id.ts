import { firestore } from 'firebase-admin';

import { WellAggregateHistoryCollection } from "../../../../db/biometricEntries/enums/firestore-aggregate-history-collection";
import { WellAggregateCollection } from "../../../../db/biometricEntries/enums/firestore-aggregate-collection";
import { WellAggregateBinaryHistoryCollection } from "../../../../db/biometricEntries/enums/firestore-aggregate-binary-history-collection";
import { IpScoreAggregateCollection } from "../../../../db/ipscore/enums/firestore-ipscore-aggregate-collection";
import { IpScoreAggregateHistoryCollection } from "../../../../db/ipscore/enums/firestore-ipscore-aggregate-history-collection";
import { AthleteBiometricEntryInterface } from "../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry";
import { isHistoryCollection } from "../../../../db/biometricEntries/aliases/collections";
import { WellAggregateCollectionType, IPScoreAggregateCollectionType } from "../../../../models/types/biometric-aggregate-types";
import { LifestyleEntryType } from "../../../../models/enums/lifestyle-entry-type";
import { isIPScoreWeeklyCollection } from "../../../../db/ipscore/services/ipscore-aggregate-collections";
import { getWeekNumber } from "../../../../shared/utils/moment";
import { isWeeklyCollection } from "../../../../db/biometricEntries/aliases/interfaces";
import { getAggregateHistoryDocId, getWeeklyAggregateDocId, getMonthlyAggregateDocId } from "./get-document-id";

export const getAggregateDocID = (
    collection:
        WellAggregateCollection |
        WellAggregateHistoryCollection |
        WellAggregateBinaryHistoryCollection |
        IpScoreAggregateCollection |
        IpScoreAggregateHistoryCollection,
    uid: string,
    creationTimestamp: firestore.Timestamp,
    entry: AthleteBiometricEntryInterface,
    dateTime = firestore.Timestamp.fromDate(new Date())
) => {


    let documentId;


    if (!!entry && isHistoryCollection(collection as WellAggregateCollectionType)) {

        documentId = getAggregateHistoryDocId(entry, uid);

        // const docId = `${entityId}_${lifestyleEntryType}`;

    } else {

        const lifestyleEntryType = !!entry ? entry.lifestyleEntryType : LifestyleEntryType.IpScore;

        switch (lifestyleEntryType) {
            case LifestyleEntryType.Period:
            case LifestyleEntryType.Sick:
                debugger;
                documentId = getAggregateHistoryDocId(entry, uid);

                console.log(documentId);

                break;

            case LifestyleEntryType.IpScore:

                if (isIPScoreWeeklyCollection(collection as IPScoreAggregateCollectionType)) {
                    // TODO: getWeeklyAggregateDocId
                    let week = getWeekNumber(creationTimestamp, dateTime, 0);
                    if (week < 0) {
                        week = 0;
                    }

                    documentId = `${uid}_${lifestyleEntryType}_${week}`;
                } else {
                    // TODO: getAggregateHistoryDocId
                    documentId = `${uid}_${lifestyleEntryType}`;
                }

                break;

            default:

                documentId = isWeeklyCollection(collection)
                    ? getWeeklyAggregateDocId(entry, uid, creationTimestamp)
                    : getMonthlyAggregateDocId(entry, uid);

                break;
        }
    }

    return documentId;
};
