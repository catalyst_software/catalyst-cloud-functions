import { handleError } from './handle-error'
import { AggregateDirectiveType } from '../../../../models/types/biometric-aggregate-types'
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type'
import { castAggregateDocument } from '../../../../db/ipscore/services/cast-to-cast-aggregate-document'
import { getDocumentSnapshot } from './get-document-snapshot'
import { AthleteBiometricEntryInterface } from '../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry'
import { AggregateDocumentDirective } from '../../../../db/biometricEntries/services/interfaces/aggregate-document-directive'
import { getGroupAthletes } from './utils/get-group-athletes'

export const executeAggregate = async (
    directive: AggregateDirectiveType,
    documentId: string,
    entry: AthleteBiometricEntryInterface
): Promise<AggregateDirectiveType> => {
    const { athlete, collection, loggerService, t } = directive

    const lifestyleEntryType = !!entry
        ? LifestyleEntryType[entry.lifestyleEntryType]
        : LifestyleEntryType[LifestyleEntryType.IpScore]

    if (documentId !== '') {
        if (loggerService) {
            console.info(
                `Doc Id signaling ${directive.collection} ${lifestyleEntryType} query. DocId: ${documentId}`
            )
        }

        try {
            const aggregateDocSnap = await getDocumentSnapshot(
                collection,
                documentId,
                loggerService,
                t
            )

            if (!!aggregateDocSnap) {
                const theCastedDoc = castAggregateDocument(
                    directive,
                    documentId,
                    athlete,
                    entry,
                    aggregateDocSnap
                )

                if (!theCastedDoc) {
                    const msg = `Doc Could Not Be Casted to Class: ${collection}, docID - ${documentId}, userID ${athlete.uid}`

                    return handleError(msg, directive)
                }

                const isIPSCORE =
                    LifestyleEntryType[lifestyleEntryType] ===
                    LifestyleEntryType.IpScore
                if (
                    LifestyleEntryType[lifestyleEntryType] ===
                        LifestyleEntryType.Weight ||
                    LifestyleEntryType[lifestyleEntryType] ===
                        LifestyleEntryType.BodyMassIndex
                ) {
                    if (!!(directive as AggregateDocumentDirective).group) {
                        //TODO: LOAD GROUP ATHLETE DOCUMENTS AND ADD TO DIRECTIVE
                        console.log(
                            `Loading group athlete docs for group ${theCastedDoc.entityId}`
                        )
                        ;(directive as AggregateDocumentDirective).groupAthletes = await getGroupAthletes(
                            theCastedDoc.entityId
                        )
                        console.log(
                            `Successfully loaded ${
                                (directive as AggregateDocumentDirective)
                                    .groupAthletes
                            } athlete documents`
                        )
                    }
                }

                const updateResult: AggregateDirectiveType = await theCastedDoc.calculateAggregates(
                    {
                        ...directive,
                        aggregateDocument: isIPSCORE
                            ? theCastedDoc.toJS()
                            : theCastedDoc,
                    }
                )

                return updateResult
            } else {
                console.error(
                    `documentSnapshot is undefined: ${directive.collection} ${documentId}`
                )

                return undefined
            }
        } catch (error) {
            console.error(
                `Error Querying DB for ${directive.collection} DocId: ${documentId}`,
                error || 'NO ERROR OBJ'
            )

            return undefined
        }
    } else {
        console.error(`${directive.collection} Doc Id is UNDEFINED`)

        return undefined
    }
}
