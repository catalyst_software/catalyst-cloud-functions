import { isArray } from 'lodash';

import { getAthleteDocs } from '../../../../db/ipscore/services/getAthleteDocs';
import { AggregateIpScoreDirective } from '../../../../db/ipscore/services/interfaces/aggregate-ipscore-directive';
import { getAggregateDocumentID } from './get-aggregate-document-Id';
import { executeAggregate } from './execute-aggregate';
import { saveAggregateDoc } from '../../../../db/ipscore/services/save-aggregates';

export const processGroupIPScoreAggregationRequest = async (groupIds: Array<string>, athleteDirective: AggregateIpScoreDirective, loggerService: boolean) => {

    const allTheResults = await Promise.all(groupIds.map(async (groupUID) => {

        const groupDirective = await getAthleteDocs(athleteDirective, groupUID)
        if (groupDirective.athleteDocuments) {

            const { dateTime } = athleteDirective.athleteWIP.currentIpScoreTracking
            groupDirective.athleteWIP.organizationId = groupDirective.group.organizationId
            groupDirective.athlete = groupDirective.athleteWIP as unknown as any

            const docID = getAggregateDocumentID(groupDirective, undefined, dateTime);// TODO: Check dateTime here

            const theRes: any = await executeAggregate(groupDirective, docID, undefined)
                .then(async (fetchAthleteDocsDirective) => {

                    if (loggerService) {
                        // console.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
                        console.log('Group Aggregation Complete');
                    }

                    // if (loggerService) {
                    //     console.warn('athlete.includeGroupAggregation', athlete.includeGroupAggregation);
                    // }

                    if (athleteDirective !== fetchAthleteDocsDirective) {
                        debugger;
                    }

                    return fetchAthleteDocsDirective;
                });

            return theRes;
        }
        else {
            console.warn('No Athletes returned for Group Aggregation', groupDirective);

            return groupDirective;
        }
        // });

        // return theGroupRes;
    }));


    if (allTheResults && isArray(allTheResults)) {
        allTheResults.map((directive: AggregateIpScoreDirective) => {
            debugger;
            return saveAggregateDoc(directive.updatedDocument, directive)
        })
    } else {
        debugger
    }

    return allTheResults
};
