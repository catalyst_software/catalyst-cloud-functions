import moment from "moment";
import { getDataFromBQ } from "../controllers/utils/reports/getDataFromBQ";
import { ReportRequest } from "../../../../models/report-model";
import { bigqueryClient } from './utils/big-query/initBQ';
import { Config } from './../../../../shared/init/config/config';

const functions = require('firebase-functions');
const cors = require('cors')({
  origin: true
});

// Imports the Google Cloud client library
const {
  BigQuery
 } = require('@google-cloud/bigquery');


export const createDatasetIfNotExists = async (datasetName) => {
  // Creates a client
//  const bigqueryClient = new BigQuery();

  // Lists all datasets in the specified project
  const [datasets] = await bigqueryClient .getDatasets();
  const isExist = datasets.find(dataset => dataset.id === datasetName);

  if (isExist)
    return Promise.resolve([isExist]);

  // Create the dataset
  return bigqueryClient.createDataset(datasetName);
}

export const tableExists = async (datasetName: string, tableName: string) => {
    // Creates a client
  //  const bigqueryClient = new BigQuery();
  
    // Lists all datasets in the specified project
    const ds = await bigqueryClient.dataset(datasetName);
    const [tables] = await ds.getTables();
    const isExist = tables.find(table => table.id === tableName);
  
    return Promise.resolve([isExist]);
  }



export const executeQuery = async (query) => {
  // Creates a client
  // const bigqueryClient = new BigQuery();
  const sqlQuery = query;

  const options = {
    query: sqlQuery,
    timeoutMs: 100000, // Time out after 100 seconds.
    useLegacySql: false, // Use standard SQL syntax for queries.
  };

  // Runs the query
  return bigqueryClient.query(options);
}

// Cors response to handle the result from BigQuery
export const responseCors = (req, res, data) => {
  return cors(req, res, () => {
    res.json({
      result: data
    });
  });
}

// Cors error when the function throw an error
export const errorCors = (req, res, error) => {
  return cors(req, res, () => {
    res.status(500).send(error);
  });
}

export const bigQSqlQuery = functions.https._onCallWithOptions(
  async (data: ReportRequest, context) => {

    try {

      if (!context.auth.token.uid) {
        console.error(
          'Request not authorized. User must logged in to fulfill this request'
        )
        return {
          error:
            'Request not authorized. User must logged in to fulfill this request ' +
            data,
        }
      }

      const {
        //  uid, reportType,
        requestDateTime: requestDateTimeString,
        //  requestProcessed, requestSuccess, requestProcessedDateTime, requestingUser,
        entity
        // , errorLogs
      } = data;

      const requestDateTime = requestDateTimeString as any as string

      const toDate = moment().subtract(1, 'day').format("YYYYMMDD")// '20180226';

      console.log('Start Date - inclusive = (date) => ', requestDateTime);
      console.log('To Date = (date) => ', toDate);

      const { biometricEntriesMulti, startDate } = await getDataFromBQ(requestDateTime, toDate, entity);
      const result = { biometricEntriesMulti, startDate, toDate }; //await executeQuery(query);#

      return result;

    } catch (error) {

      // Otherwise throw an error
      // errorCors(req, res, error);
      return error;
    }
  }, {regions: Config.regions}
)
