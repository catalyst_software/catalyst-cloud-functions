import { AggregateDirectiveType } from "../../../../models/types/biometric-aggregate-types";

export const handleError = (msg: string, directive: AggregateDirectiveType) => {
    
    const { loggerService } = directive;
    if (loggerService) {
        console.error(msg);
    }
    return {
        ...directive,
        message: msg
    };
};
