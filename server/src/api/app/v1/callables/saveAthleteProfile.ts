import { Athlete } from '../../../../models/athlete/interfaces/athlete';
import _ from 'lodash';
import { auth, firestore } from 'firebase-admin';
import { Observable } from 'rxjs/internal/Observable';
import { from } from 'rxjs/internal/observable/from';
import { take } from 'rxjs/internal/operators/take';
import { map } from 'rxjs/internal/operators/map';

import { AthleteUpdateSubscriptionRequest } from '../../../../models/athlete/interfaces/athlete-subscription';
import { OnboardingValidationResponse } from '../interfaces/onboarding-validation-response';

import { GroupIndicator } from '../../../../db/biometricEntries/well/interfaces/group-indicator';

import { SubscriptionStatus, SubscriptionType } from '../../../../models/enums/enums.model';
import { AthleteIndicator } from '../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { AthleteProfile } from '../../../../models/athlete/interfaces/athlete-profile';

import { createNewIpScoreTracking } from './utils/maintenance/resetIPTracking';
import { updateAthleteSubscription as updateAthleteSubscriptions } from './utils/update-athlete-subscription';
import { createAthleteProfile } from "./utils/onboarding/createAthleteProfile";

export const updateAtheleteSubscription = (athlete: Athlete, includeAthlete: boolean, includeGroups: boolean): Observable<Array<{
    message: string;
    athleteUID: string;
    groupUID: string;

}>> => {
    const request = {
        athleteUid: athlete.uid,
        includeAthlete,
        includeGroups,
        groupIds: athlete.groups.map((g: GroupIndicator) => {
            return g.groupId;
        }),
        subscription: {
            type: athlete.profile.subscription.type,
            status: athlete.profile.subscription.status
        }
    } as AthleteUpdateSubscriptionRequest;


    const updateResults = from(updateAthleteSubscriptions(
        request.athleteUid,
        request.groupIds,
        request.subscription,
        request.includeAthlete,
        request.includeGroups
    ));

    return updateResults;

}

export const saveAthleteProfile = async (user: auth.UserRecord, results: OnboardingValidationResponse, athleteIndicator: AthleteIndicator, profile: AthleteProfile, firebaseMessagingId: string) => {

    const data = results.data
    debugger;
    console.log(`Returned organization1`, data.organizations[0]);
    console.log(`Returned organization id: ${data.organizations[0].organizationId}`);

    const orgId = data.organizationId;
    const athlete = {
        uid: athleteIndicator.uid,
        profile,
        organizationId: orgId,
        organizations: data.organizations,
        isCoach: data.isCoach,
        includeGroupAggregation: data.includeGroupAggregation,

        currentPrograms: []
    } as Athlete;

    console.warn(`New athlete: ${JSON.stringify(athlete)}`);
    console.log(`data: ${JSON.stringify(data)}`);

    athlete.groups = [];

    if (_.isArray(data.groups)) {
        data.groups.forEach((g) => {
            const existing = athlete.groups.find((gp: GroupIndicator) => {
                return gp.groupId === g.groupId;
            });
            if (!existing) {
                if (_.isString(g.creationTimestamp)) {
                    g.creationTimestamp = firestore.Timestamp.fromDate(new Date((g as any).creationTimestamp));
                }
                athlete.groups.push(g);
            }
        });
    }

    athlete.ipScoreConfiguration = data.ipScoreConfiguration;
    athlete.currentIpScoreTracking = createNewIpScoreTracking(athlete.ipScoreConfiguration);
    athlete.runningTotalIpScore = athleteIndicator.runningTotalIpScore;

    athlete.metadata = {
        creationTimestamp: athleteIndicator.creationTimestamp,
        lastSignInTimestamp: athleteIndicator.creationTimestamp,
        firebaseMessagingIds: athleteIndicator.firebaseMessagingIds,
        // TODO:
        onboardingLocation: null,
        userPropertiesValidationDate: null
    }

    let isPrePaid = false;

    if (data.subscription && data.subscription.isPrePaid) {
        isPrePaid = true;
        console.log('Organization is PrePaid.  Setting athlete subscription accordingly.');
        athlete.profile.subscription = {
            status: SubscriptionStatus.ValidInitial,
            type: data.subscription.subscriptionType,
            commencementDate: data.subscription.commencementDate,
            expirationDate: data.subscription.expirationDate,
            isPrePaid: true
        };

        await updateAtheleteSubscription(athlete, false, true)
            .pipe(
                take(1),
                map((response) => {
                    if (response.length) {
                        console.log('Update Athelete Subscription Response', response)

                        return true
                    } else {
                        return false
                    }
                })
            )

            .subscribe((ok: boolean) => {
                if (ok) {
                    console.log('Updated athlete subscription successfully.');
                } else {
                    console.log('Failed to update athlete subscription.');
                }
            })

    }
    else {
        console.log('Organization is NOT PrePaid.  Setting athlete subscription to FREE.');
        athlete.profile.subscription = {
            status: SubscriptionStatus.Free,
            type: SubscriptionType.Free,
            // commencementDate: firestore.Timestamp.now(),
            isPrePaid: false
        };
    }

    const theAthleteObj = await createAthleteProfile(athlete, firebaseMessagingId)
    // await getAthleteDocRef(athlete.uid).set(athlete)
    // results: any, athleteIndicator: AthleteIndicator, profile: AthleteProfile, data: any
    console.warn('results.data', results.data)
    console.warn('theAthleteObj', theAthleteObj)
    results.data.athlete = theAthleteObj

    console.warn('results.data', results.data)
    return results
};
