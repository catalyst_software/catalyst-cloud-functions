import { pubsub } from 'firebase-functions'
import { composeWeeklyAllAthleteReportData } from './composeWeeklyAllAthleteReportData';
import { insertRowsAsStream } from '../controllers/insertBQRowsAsStream';

export const scheduledFunction = pubsub.schedule('every monday 16:25').onRun(async (context) => {
    console.log('This will be run every monday 16:15!');


    // const orgData: Array<{ orgID: string, name: string }> = []

    // const limit = 10000000000;

    // const { payloadData, allUsers } = await composeWeeklyAllAthleteReportData(limit, orgData)

    // // console.log(JSON.stringify(payloadData))
    // // const dfddf = updatedResponse
    // // usersWithoutDocs

    // console.log('payloadData.theAthletes length', payloadData.theAthletes.length)

    // await insertRowsAsStream(payloadData.theAthletes as any)

    return null;
});

export const scheduledFunctionGB = pubsub.schedule('every monday 16:50').timeZone('GB').onRun(async (context) => {
    console.log('This will be run every monday 16:50 GB!');


    const orgData: Array<{ orgID: string, name: string }> = []

    const limit = 10000000000;

    const { payloadData,
        // allUsers
    } = await composeWeeklyAllAthleteReportData(limit, orgData)

    // console.log(JSON.stringify(payloadData))
    // const dfddf = updatedResponse
    // usersWithoutDocs

    console.log('payloadData.theAthletes length', payloadData.theAthletes.length)

    await insertRowsAsStream(payloadData.theAthletes as any)

    return null;
});