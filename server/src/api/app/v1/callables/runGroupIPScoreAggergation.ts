import { firestore } from 'firebase-admin';

import { AggregateIpScoreDirective } from '../../../../db/ipscore/services/interfaces/aggregate-ipscore-directive';
import { allIPScoreAggregateCollections } from '../../../../db/ipscore/services/ip-score-aggregate-collections';
import { processGroupIPScoreAggregationRequest } from './processAggregationRequest';
import { IpScoreGroupAggregationDirective, setIPScoreTracking } from './run-group-ip-score-aggregation';

export const runGroupIPScoreAggergation = async (t, ipScoreGroupAggregationDirective: IpScoreGroupAggregationDirective) => {


    // const aggregateDocsRefs = new Array<FirebaseFirestore.DocumentReference>();


    const { utcOffset, commands } = ipScoreGroupAggregationDirective;

    const { uid } = ipScoreGroupAggregationDirective as any;
    let { groupIds } = ipScoreGroupAggregationDirective as any;

    if (!(groupIds && groupIds.length)) {
        groupIds = [];
    }

    const athleteWIP = {
        uid,
        organizationId: '',
        includeGroupAggregation: true,
        currentIpScoreTracking: {
            dateTime: firestore.Timestamp.now(),
            directive: { paths: [] }
        }
    };


    // allIPScoreAggregateCollections.athlete.forEach((groupUID) => {

    //     const { collection } = groupUID;
    //     const loggerService = true;

    //     const updateAthleteDirective: AggregateIpScoreDirective = {
    //         athleteDocuments: undefined,
    //         collection: collection,
    //         message: `Update ${collection} Docs.`,
    //         athleteWIP,
    //         utcOffset,
    //         loggerService,
    //         t
    //     };


    //     const docID = getAggregateDocumentID(updateAthleteDirective, undefined);



    //     const ref = db
    //         .collection(FirestoreCollection.Groups)
    //         .doc(docID);
    //     aggregateDocsRefs.push(ref);
    // })

    // const theRes = await db.getAll(...aggregateDocsRefs)
    //     .then(async (groupDocs: FirebaseFirestore.DocumentSnapshot[]) => {
    //         debugger;
    //     })


    const allResults = await Promise.all(commands.map(async (command) => {

        console.warn('===============>>>>>>>>>>>>>>>>> RUNNING THE FOLLOWING COMMANDS', commands)

        setIPScoreTracking(command, athleteWIP);

        // run aggregation
        const theResult = await Promise.all(allIPScoreAggregateCollections.athlete.map(async (athletecollectionMeta) => {
            const { collection } = athletecollectionMeta;
            const loggerService = true;
            const updateAthleteDirective: AggregateIpScoreDirective = {
                athleteDocuments: undefined,
                collection: collection,
                message: `Update ${collection} Docs.`,
                athleteWIP,
                utcOffset,
                loggerService,
                t
            };

            debugger;

            if (athleteWIP.includeGroupAggregation && groupIds) {

                const groupResults = await processGroupIPScoreAggregationRequest(groupIds, updateAthleteDirective, loggerService);
                return groupResults;
            }

            else
                return [updateAthleteDirective];
        }));



        const test = [].concat(...theResult);

        return test;
    }));

    return allResults;
};
