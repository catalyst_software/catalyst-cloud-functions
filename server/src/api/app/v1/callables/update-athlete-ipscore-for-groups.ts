import * as functions from 'firebase-functions'

import { resetAthleteIndicatorsDailyIPScore } from '../../../../analytics/triggers/reset-athlete-indicators-daily-ipscore'
import { isArray } from 'lodash'
import { Config } from './../../../../shared/init/config/config'

export const updateAthleteIpScoreForGroups = functions.https._onCallWithOptions(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('data => ', data)

            console.log('context => ', context)

            const {
                // uid: athleteUID,
                ipScore,
                runningTotalIpScore,
                groupIds,
                utcOffset,
            } = <
                {
                    uid: string
                    ipScore: string
                    runningTotalIpScore: string
                    groupIds: string | Array<string>
                    utcOffset: number
                }
            >data
            console.log({
                // uid: athleteUID,
                ipScore,
                runningTotalIpScore,
                groupIds,
                utcOffset,
            })

            const theGroupIds = isArray(groupIds)
                ? () => {
                      console.log('isArray(groupIds) => ', isArray(groupIds))
                      return groupIds
                  }
                : () => {
                      console.log('isArray(groupIds) => ', isArray(groupIds))
                      return JSON.parse(groupIds) as Array<string>
                  }

            const theDailyIPScore = ipScore !== undefined ? +ipScore : 0
            const theRunningTotalIpScore =
                runningTotalIpScore !== undefined
                    ? +runningTotalIpScore
                    : undefined
            debugger

            console.log('=====>>>>> theDailyIPScore', theDailyIPScore)
            console.log(
                '=====>>>>> therunningTotalIpScore',
                theRunningTotalIpScore
            )
            const updateResults = await resetAthleteIndicatorsDailyIPScore(
                theGroupIds(),
                theRunningTotalIpScore,
                theDailyIPScore,
                utcOffset || 0
            )

            if (updateResults.length) {
                // const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName

                return {
                    message: `IPScore Group Indicators update results for ${updateResults[0].groupUID}`,
                    updateResults,
                }
            } else {
                console.warn(
                    `No ipscore GroupIndicator update results ${groupIds}`
                )

                return {
                    message: `No ipscore GroupIndicator update results ${groupIds}`,
                }
            }
        } catch (error) {
            console.error('.........CATCH ERROR:', error)

            return error
        }
    },
    { regions: Config.regions }
)
