import * as functions from 'firebase-functions'

import { updateMessagingIDInConfig } from './utils/comms/update-messaging-id-in-config';
import { Config } from './../../../../shared/init/config/config';


export const updateCoachMessaginIDsOnConfig = functions.https._onCallWithOptions(
    async (data, context) => {
        try {
            if (!context.auth.token.uid) {
                console.error(
                    'Request not authorized. User must logged in to fulfill this request'
                )
                return {
                    error:
                        'Request not authorized. User must logged in to fulfill this request ' +
                        data,
                }
            }

            console.log('context.instanceIdToken => ', context.instanceIdToken)
            console.log('context.auth => ', context.auth)
            console.log('data => ', data)

            const { organizationId, firstName, lastName, notificationIds }
                = <{
                    organizationId: string,
                    firstName: string,
                    lastName: string,
                    notificationIds: Array<string>
                }>data

            const { uid: athleteUID } = context.auth.token;
            console.log('firstName ', firstName)

            return await updateMessagingIDInConfig(athleteUID, organizationId, firstName, lastName, notificationIds).then((results) => {
                // return res.status(200).json({
                //     message: 'Group added',
                //     responseCode: 201,
                //     data: {
                //         group,
                //         result: results,
                //     },
                //     error: undefined,
                //     method: {
                //         name: 'addGroup',
                //         line: 58,
                //     },
                // })
                return results
            }).catch((err) => {
                return err
                // return res.status(200).json({
                //     message: 'Group added',
                //     responseCode: 201,
                //     data: {
                //         group,
                //     },
                //     error: err,
                //     method: {
                //         name: 'addGroup',
                //         line: 58,
                //     },
                // })
            })

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    }, {regions: Config.regions}
)