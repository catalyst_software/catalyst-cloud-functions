// import * as firebaseHelper from 'firebase-functions-helper';

// Import the Firebase SDK for Google Cloud Functions.
import * as functions from 'firebase-functions'



// import csvParser from 'csv-parser';
// const csvParser = require('csv-parser');

import { db } from '../../../shared/init/initialise-firebase';
const fs = require('fs');

const csvFilePath = './path/to/csv';


const handleError = (error) => {
    // Do something with the error...
};

const commitMultiple = (batchFactories) => {
    let result = Promise.resolve();
    /** Waiting 1.2 seconds between writes */
    const TIMEOUT = 1200;

    batchFactories.forEach((promiseFactory, index) => {
        result = result
            .then(() => {
                return new Promise((resolve) => {
                    setTimeout(() => {
                        return Promise.resolve()
                    }, TIMEOUT);
                });
            })
            .then(promiseFactory)
            .then(() => console.log(`Commited ${index + 1} of ${batchFactories.length}`));
    });

    return result;
};

const api = (req, res) => {
    let currentBatchIndex = 0;
    const batchFactories = [];
    const batchesArray = [];
    let batchDocsCount = 0;

    batchFactories.push()

    return Promise
        .resolve()
        .then(() => {
            const data = [];

            return fs
                .createReadStream(csvFilePath)
                // .pipe(csvParser())
                .on('data', (row) => {
                    const batch = (() => {
                        const batchPart = db.batch();

                        if (batchesArray.length === 0) {
                            batchesArray.push(batchPart);
                        }

                        if (batchDocsCount === 499) {
                            // reset count
                            batchDocsCount = 0;
                            batchesArray.push(batchPart);

                            currentBatchIndex++;
                            batchFactories.push(() => batchPart.commit());
                        }

                        return batchesArray[currentBatchIndex];
                    })();

                    batchDocsCount++;

                    const ref = db.collection('contacts').doc();

                    batch.set(ref, JSON.parse(row));
                })
                .on('end', () => Promise.resolve());
        })
        .then(() => {
            if (batchesArray.length === 1) {
                return batchesArray[0]
                    .commit()
                    .catch(handleError);
            }

            return commitMultiple(batchFactories);
        })
        .then(() => res.json({ success: true }))
        .catch(handleError);
};

export const csvAPI: functions.HttpsFunction = functions.https.onRequest(api)

