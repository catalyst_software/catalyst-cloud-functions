
import { Response } from 'express'

import BaseCtrl from '../base';
import { General } from '../general';
import { SportsNutrition } from '../sportsNutrition';
import { SportsPsychology } from '../sportsPsychology';
import { SportsScience } from '../sportsScience';
import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections';
import { AggregateDocumentManager } from '../../../../../db/biometricEntries/managers/interfaces/aggregate-document-manager';
import { TemplateData } from '../../sendGrid/conrollers/email';
import { db, catalystDB } from '../../../../../shared/init/initialise-firebase';

import client from '@sendgrid/client';

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class TestCtrl
 * @extends {BaseCtrl}
 */
export default class CopyEmailTemplateCtrl extends BaseCtrl {

    res: Response

    // logger: Logger;
    model = General
    sportsNutritionModel = SportsNutrition
    sportsPsychologyModel = SportsPsychology
    sportsScienceModel = SportsScience
    entries

    collection = FirestoreCollection.Tests
    aggregateDocumentManager: AggregateDocumentManager

    constructor() {
        super()
    }

    copyTemplate = async (req): Promise<any> => {

        const emailTemplate = 'badMood'

        const name = req.body.name

        const dbTemplateData = await db.collection(FirestoreCollection.EmailTemplates).doc(emailTemplate)
            .get().then((docSnap) => {

                return {
                    uid: docSnap.id,
                    ...docSnap.data() as TemplateData,
                }
            })

        const sdssd = await db.collection(FirestoreCollection.EmailTemplates).doc(name).set({ ...dbTemplateData, uid: name })
    }

    addToMainingList = async (): Promise<any> => {


        client.setApiKey('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');


        const listreq = {
            method: 'GET',
            url: '/v3/contactdb/lists'
        } as any
        return await client.request(listreq)
            .then(async ([response, body]) => {
                console.log(response.statusCode);
                console.log(response.body);


                const { lists } = response.body as {
                    lists: Array<{
                        id: number; //8993961
                        name: string; // "Test"
                        recipient_count: number // 1
                    }>
                }

                const data = [
                    {
                        "email": "example2@example.com",
                        "first_name": "Woo2t",
                        "last_name": "User2"
                    }
                ];

                const addRecpientRequest = {
                    body: data,
                    method: 'POST',
                    url: '/v3/contactdb/recipients'
                } as any

                const listID = lists.find((list) => list.name === 'Test').id

                const recipientIDs = await client.request(addRecpientRequest)
                    .then(async ([addRecpientResponse]) => {
                        console.log(addRecpientResponse.statusCode);
                        console.log(addRecpientResponse.body);

                        return addRecpientResponse.body[
                            // "recipient_id1",
                            // "recipient_id2"
                            'persisted_recipients'
                        ]
                    })


                const addToListDataRequest = {
                    body: recipientIDs,
                    method: 'POST',
                    url: `/v3/contactdb/lists/${listID}/recipients`
                } as any

                await client.request(addToListDataRequest)
                    .then(([addToListResponse, addToListBody]) => {
                        console.log(addToListResponse.statusCode);
                        console.log(addToListResponse.body);

                    })

            }).catch((err) => {
                console.error(err);

                return err

            })

    }

    copyEmailTemplatesToDev = async (): Promise<any> => {

        // const emailTemplate = 'badMood'


        const dbTemplateData = await db.collection(FirestoreCollection.EmailTemplates) // .doc(emailTemplate)
            .get().then((queryDocSnap) => {

                return queryDocSnap.docs.map((docSnap) => {
                    return {
                        uid: docSnap.id,
                        ...docSnap.data() as TemplateData,
                    }
                })


            })

        const copyResults = await Promise.all(dbTemplateData.map(async (emailTemplate) => {

            const { uid, ...templateRest } = emailTemplate

            return await catalystDB.collection(FirestoreCollection.EmailTemplates).doc(uid).set(templateRest).then(() => {
                return true
            })
        }))

        return copyResults
    }

    copyNotificationTemplatesToDev = async (): Promise<any> => {

        // const emailTemplate = 'badMood'


        const dbTemplateData = await db.collection(FirestoreCollection.NotificationTemplates) // .doc(emailTemplate)
            .get().then((queryDocSnap) => {

                return queryDocSnap.docs.map((docSnap) => {
                    return {
                        uid: docSnap.id,
                        ...docSnap.data() as TemplateData,
                    }
                })


            })

        const copyResults = await Promise.all(dbTemplateData.map(async (emailTemplate) => {

            const { uid, ...templateRest } = emailTemplate

            return await catalystDB.collection(FirestoreCollection.NotificationTemplates).doc(uid).set(templateRest).then(() => {
                return true
            })
        }))

        return copyResults
    }

}



