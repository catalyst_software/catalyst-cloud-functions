
import { Response, Request } from 'express'
import moment from 'moment';

import BaseCtrl from '../base';
import { General } from '../general';
import { SportsNutrition } from '../sportsNutrition';
import { SportsPsychology } from '../sportsPsychology';
import { SportsScience } from '../sportsScience';
import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections';
import { AggregateDocumentManager } from '../../../../../db/biometricEntries/managers/interfaces/aggregate-document-manager';
import { NotificationSeverity } from '../../sendGrid/conrollers/email';
import { getGroupCoaches } from '../../../../../analytics/triggers/utils/get-athlete-coaches';
import { notifyCoachesIPScoreNonEngagement } from '../../callables/utils/comms/notify-coaches';
import { theMainApp } from '../../../../../shared/init/initialise-firebase';
import { Organization } from '../../../../../models/organization.model';
import { Group } from '../../../../../models/group/interfaces/group';
import { Athlete } from '../../../../../models/athlete/interfaces/athlete';
import { WellAggregateCollection } from '../../../../../db/biometricEntries/enums/firestore-aggregate-collection';
import { GroupWeeklyAggregate } from '../../../../../models/documents/well';
import { getDocumentFromSnapshot } from '../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { GroupIndicator } from '../../../../../db/biometricEntries/well/interfaces/group-indicator';
import { IPScoreNonEngagementTemplate } from '../../sendGrid/templates/ipscore-non-engagement';
import { getOrgCollectionRef, getCollectionRef, getGroupCollectionRef, getAthleteDocRef, getCollectionDocs } from '../../../../../db/refs';
import { getNonEngagementAthletes } from '../utils/getNonEngagementAthletes';
import { isArray } from 'lodash';


/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class TestCtrl
 * @extends {BaseCtrl}
 */
export default class IPScoreNonEngagementCtrl extends BaseCtrl {

    res: Response

    // logger: Logger;
    model = General
    sportsNutritionModel = SportsNutrition
    sportsPsychologyModel = SportsPsychology
    sportsScienceModel = SportsScience
    entries

    collection = FirestoreCollection.Tests
    aggregateDocumentManager: AggregateDocumentManager

    constructor() {
        super()
    }

    ipScoreNonEngagement = async (req: Request, res: Response): Promise<any> => {


        const utcMoment = moment.utc();
        // console.log('utcMoment string = ' + utcMoment.format());

        // let startOfHour = utcMoment.startOf('hour').toDate();


        // const endOfHour = utcMoment.endOf('hour').toDate();
        // console.log('utcMoment AND NOW?!? string = ' + utcMoment.format());

        // console.log(`Querying hours between ${startOfHour} and ${endOfHour} `)

        // console.log("\ncurrent  dateTime \n", JSON.stringify(currentIpScoreTrackingTime, null, 4) + " UTC\n");



        // const dataString = await Buffer.from(message.data, 'base64').toString();
        // console.log(`PubSub Message Data: ${dataString}`); // hourlyIpScoreNonEngagement
        // chosenFunction = 'ipscore-non-engagement';

        const utcHour = +utcMoment.hour().toString()

        console.log(`Querying UTC hour ${utcHour}`)

        const allNonEngagementAthletes = await getAllNonEngagementAthletes(utcHour)
        // const allNonEngagementAthletes = await getAllNonEngagementAthletes(startOfHour, endOfHour)

        if (isArray(allNonEngagementAthletes)) {
            if (allNonEngagementAthletes.length) {

                const response = [].concat(allNonEngagementAthletes.map(async (orgAthletes) => {
                    await persistToBQ({ ...orgAthletes, orgUID: orgAthletes.org.uid })
                    const endResult = await notifyNonEngagementFlags(orgAthletes)

                    console.warn('endResult', endResult)

                    return endResult

                }))


                const ddd = await Promise.all(response)
                return res.status(201).json({
                    message: 'allNonEngagementAthletes coaches notified',
                    response: ddd
                })

            } else {

                console.log('allNonEngagementAthletes is an empty array', allNonEngagementAthletes)
                return res.status(201).json({
                    message: 'allNonEngagementAthletes is an empty array',
                    allNonEngagementAthletes
                })
            }
        } else {

            console.warn('allNonEngagementAthletes is NOT an ARRAY', allNonEngagementAthletes)

            return res.status(500).json({
                message: 'allNonEngagementAthletes is NOT an ARRAY',
                allNonEngagementAthletes
            })
        }

    }



    async fixIsCoach(res: Response): Promise<any> {
        const allOrgs = await getOrgCollectionRef()
            .get().then((querySnap) => {
                const orgs = querySnap.docs.map((queryDocSnap) => {
                    const org = {
                        uid: queryDocSnap.id, ...queryDocSnap.data() as Organization
                    }
                    return org
                })

                return orgs

            });

        const updates = allOrgs.map(async (org) => {
            const orgGroups = await getCollectionRef(FirestoreCollection.Groups)
                .where('organizationId', '==', org.uid)
                .get().then(async (querySnap) => {

                    const { coaches: orgCoaches } = org

                    const groups = await querySnap.docs.map(async (groupQueryDocSnap) => {
                        const group = {
                            uid: groupQueryDocSnap.id, ...groupQueryDocSnap.data() as Group
                        }

                        if (!group.athletes) {
                            group.athletes = []
                            console.log('Group has no athletes', group)
                        }

                        let updateGroup = false;
                        await group.athletes.map((athlete) => {
                            if (orgCoaches.indexOf(athlete.email) !== -1 && !athlete.isCoach) {
                                console.warn('Setting Athelete as coach', {
                                    athlete,
                                    group,
                                    coaches: org.coaches
                                })
                                athlete.isCoach = true
                                updateGroup = true
                            }

                        })

                        if (updateGroup) {
                            console.log('updatingGroup', group)
                            await groupQueryDocSnap.ref.update({ athletes: group.athletes })
                        }


                        return group
                    })

                    return groups

                });

            return orgGroups
        })

        return res.status(200).send({ message: 'Org Groups', updates })
    }

    async addMessagingIdToAthleteIndicator(res: Response): Promise<any> {
        const allOrgs = await getOrgCollectionRef()
            .get().then((querySnap) => {
                const orgs = querySnap.docs.map((queryDocSnap) => {
                    const org = {
                        uid: queryDocSnap.id, ...queryDocSnap.data() as Organization
                    }
                    return org
                })

                return orgs

            });

        const updates = await allOrgs.map(async (org) => {
            const orgGroups = await getGroupCollectionRef()
                .where('organizationId', '==', org.uid)
                .get().then(async (orgQuerySnap) => {

                    // const { coaches: orgCoaches } = org

                    const athleteIndicators = await orgQuerySnap.docs.map(async (queryDocSnap) => {
                        const group = {
                            uid: queryDocSnap.id, ...queryDocSnap.data() as Group
                        }

                        if (!group.athletes) {
                            group.athletes = []
                            console.log('Group has no athletes', group)
                        }

                        const updateGroup = true;
                        const athleteUpdates = await group.athletes.map(async (athlete) => {

                            return await getAthleteDocRef(athlete.uid)
                                .get().then(async (athleteQuerySnap) => {

                                    const ath = {
                                        uid: athleteQuerySnap.id,
                                        ...athleteQuerySnap.data() as Athlete
                                    }
                                    athlete.firebaseMessagingIds = ath.firebaseMessagingIds || ath.metadata.firebaseMessagingIds || [ath.metadata.firebaseMessagingId]
                                    return athlete
                                })

                        })
                        const ereere = await Promise.all(athleteUpdates)
                        if (updateGroup) {
                            console.log('updatingGroup', group)
                            await queryDocSnap.ref.update({ athletes: group.athletes })
                        }


                        return ereere
                    })


                    return await Promise.all(athleteIndicators)

                });

            return orgGroups
        })
        const allUpdates = await Promise.all(updates)
        return res.status(200).send({ message: 'Org Groups', results: [].concat(...allUpdates) })
    }
    getAthleteGroups = async (req: Request, res: Response): Promise<any> => {
        // const athleteGroups =

        const orgGroups = await getGroupCollectionRef()
            .where('organizationId', '==', '4B8sHmqfFHOzerPqVA82')
            .get().then((querySnap) => {
                const groups = querySnap.docs.map((docSnap) => {
                    const group = {
                        uid: docSnap.id,
                        ...docSnap.data()
                    } as Group

                    return group
                })

                return groups
            })

        const athleteGroups = orgGroups
            .filter((group) => group.athletes
                .filter((athlete) => athlete.uid === 'npAD8qnmzVcnjjMJPHYpgucAMB82').length > 0 ? true : false)

        return athleteGroups
        return res.status(200).send({ message: 'Athlete Groups', athleteGroups })

    }

    fixGroupWeeklyEntityNames = async (req: Request, res: Response): Promise<any> => {

        const groupsSNaps = await getGroupCollectionRef()
            .get()
        const groups = groupsSNaps.docs.map((groupSnap) => {
            return { uid: groupSnap.id, ...groupSnap.data() as Group }
        })

        const result = getCollectionDocs(WellAggregateCollection.GroupWeekly)
            .then((querySnap: FirebaseFirestore.QuerySnapshot) => {

                const promises = []
                querySnap.docs.map(docSnap => {
                    if (docSnap.exists) {
                        const aggregateDoc = docSnap.data() as GroupWeeklyAggregate

                        const entityId = aggregateDoc.uid.split('_')[0]
                        const entityName = groups.find((grp) => grp.uid === entityId).groupName

                        promises.push(docSnap.ref.update({ entityId, entityName }))
                    } else promises.push(Promise.resolve('Not Found'))
                })
                return Promise.all(promises).then((results) => {

                    return res.status(200).send({ message: 'Documents Updated', results })
                }).catch((error) => {

                    return res.status(500).send(error)
                })
            })
        return result
    }


    getAllUsers = async (req: Request, res: Response): Promise<any> => {
        theMainApp
            .auth()
            .listUsers()
            .then(function (listUsersResult) {
                listUsersResult.users.forEach(function (userRecord) {
                    console.log('user', userRecord.toJSON())
                })
                // if (listUsersResult.pageToken) {
                //     // List next batch of users.
                //     listAllUsers(listUsersResult.pageToken)
                // }

                return res.status(200).send(listUsersResult.users)
            })
            .catch(function (error) {
                console.log('Error listing users:', error)
                return res.status(500).send(error)
            })
    }

    updateSurveys = async (req: Request, res: Response) => {

        return res.status(500).send('NOT IMPLIMENTED')
    }

    getIOSNotifications = async (req: Request, res: Response) => {
        const { date } = req.body

        const query = getCollectionRef(FirestoreCollection.IOSNotifications)
        let result

        if (date) {
            const start = new Date(date)

            result = await query
                .where('creationTimestamp', '>', start)
                .get()
                .then((querySnap: FirebaseFirestore.QuerySnapshot) => {
                    const notifications = querySnap.docs.map(docSnap => {
                        const data = docSnap.data()
                        data.creationTimestamp = data.creationTimestamp.toDate()
                        return data
                    })

                    return notifications
                })
        } else {
            result = await query
                .get()
                .then((querySnap: FirebaseFirestore.QuerySnapshot) => {
                    const notifications = querySnap.docs.map(docSnap => {
                        const data = docSnap.data()

                        return data
                    })

                    return notifications
                })
        }

        return res.status(200).send(result)
    }

    getAndroidNotifications = async (req: Request, res: Response) => {
        const result = getCollectionDocs(FirestoreCollection.AndroidNotifications)
            .then((querySnap: FirebaseFirestore.QuerySnapshot) => {
                const notifications = querySnap.docs.map(docSnap => {
                    return docSnap.data()
                })

                return notifications
            })

        return res.status(200).send(result)
    }


}

export let chosenFunction = "";

async function persistToBQ(orgAthletes: {
    orgUID: string;
    athletes: Athlete[];
}) {

    const bqData: Array<{
        athleteUID: string;
        orgUID: string;
        groupUID: string;
        date: Date;
    }> = [].concat(...orgAthletes.athletes.map((athlete) => {

        const groupRes = athlete.groups.map((group) => {
            const entry = {
                athleteUID: athlete.uid,
                orgUID: orgAthletes.orgUID || athlete.organizations[0].organizationId || group.organizationId,
                orgName: orgAthletes.orgUID || athlete.organizations[0].organizationName || group.organizationName,
                groupUID: group.uid || group.groupId,
                date: moment().subtract(1, 'day').toDate()
            }

            console.log(`Adding entry to BQ`, { name: athlete.name || `${athlete.profile.firstName} ${athlete.profile.lastName}`, uid: athlete.uid, entry });

            return entry
        })

        return groupRes

        // ipscoreNonEngagementDataset
    }));


    console.log('data being saved to to BQ', bqData)
    // let ipscoreNonEngagementDataset = await getDataset('ip_score_non_engagement')

    // Inserts data into a table
    // const rows = await insertRowsAsStream(
    //     'ip_score_non_engagement',
    //     'data',
    //     results)

    // console.log(`Inserted ${rows.length} rows`);
    console.log(`Inserted ${bqData.length} rows`);

    return bqData
}



async function notifyNonEngagementFlags(flattenedAthletes: {
    org: Organization;
    athletes: Athlete[];
}) {
debugger
// TODO: Now passing ORG!!
    const athleteGroups = [].concat(...flattenedAthletes.athletes.map((ath, idx) => {

        if (!ath.groups || !ath.groups.length) {
            console.warn('ATHLETE HAS NO GROUPS')
        }
        const group = ath.groups.map((grp) => {
            return {
                uid: grp.groupId,
                ...grp
            }
        })

        console.log('MAPPED GROUP ' + idx, group)

        return group
    }))

    console.warn('athleteGroups', athleteGroups)

    const distinctGroupIDs = [...new Set(athleteGroups.map(item => { return item.groupId }))];
    // console.warn('distinctGroups', distinctGroupIDs)
    const groupedAthetes = distinctGroupIDs.map((groupId) => {

        const groupAthletes = flattenedAthletes.athletes.filter((ath) => {
            const athGroups = ath.groups
                .filter((grp) => grp.groupId === groupId)

            if (athGroups.length)
                return true
            else return false
        })


        return { groupId, athletes: groupAthletes }
    })
    const theResults = await groupedAthetes.map(async (groupAthletes) => {

        const { groupId, athletes } = groupAthletes

        const group: GroupIndicator = athleteGroups.filter(grp => grp.groupId === groupId).shift()
        if (group) {

            const coachesToEmail = await getGroupCoaches(group.uid || group.groupId)

            console.warn('Coaches EMAILED!!!!!')
            return {
                group: {
                    uid: group.groupId,
                    groupName: group.groupName,
                    groupId: group.groupId
                },
                coachesToEmail: coachesToEmail.map((coach) => {
                    return {
                        ...coach,
                        athletes:
                            athletes
                                .filter((me) => me.uid !== coach.uid)
                                .map((theAth) => {
                                    return {
                                        uid: theAth.uid,
                                        fullName: `${theAth.profile.firstName} ${theAth.profile.lastName}`,
                                        email: theAth.profile.email,
                                        currentIpScoreTracking: theAth.currentIpScoreTracking
                                    }
                                })

                    }
                })
            }


        } else {
            console.warn('GROUP NOT FOUND')
            return false
        }

    })

    return await Promise.all(theResults).then((results) => {
        console.warn('COMPLETED!!!')
        return results.map(async (grouped) => {

            if (grouped !== false) {
                const { group, coachesToEmail } = grouped

                if (coachesToEmail.length) {

                    coachesToEmail.map(async (coach) => {

                        const emailMessageTemplate: IPScoreNonEngagementTemplate = {
                            to: [],
                            from: {
                                name: "iNSPIRE",
                                email: "athleteservices@inspiresportonline.com"
                            },

                            templateId: "d-b95ae611e7124aafa1b948ca09ad3c2d",
                            // emailTemplateName: "badMood",
                            emailTemplateName: "ipScoreNonEngagement",
                            notificationSeverity: NotificationSeverity.Warn,
                            emailTemplateData: {

                                name: group.groupName,
                                athletes: coach.athletes.map((athlete) => {

                                    const startOfToday = moment.utc().startOf('day');
                                    const lastLoggedDay = moment(athlete.currentIpScoreTracking.dateTime.toDate()).startOf('day');
                                    const daysSinceLastEntry = startOfToday.diff(lastLoggedDay, 'days');
                                    console.log('Days Since Last Entry', daysSinceLastEntry);

                                    return {
                                        uid: athlete.uid,
                                        fullName: athlete.fullName,
                                        email: athlete.email,
                                        daysSinceLastEntry
                                    }
                                }),

                                numberOfAthletes: coach.athletes.length,
                                dayCountNonEngagement: flattenedAthletes.org.nonEngagementConfiguration.dayCountNonEngagement,
                                    // subject: "Custom Email Subject",

                                    // message: "<h1>Custom messtage</h1>",

                                    showDownloadButton: false,

                                barType: {
                                    danger: true
                                }
                            }
                        }

                        return await notifyCoachesIPScoreNonEngagement(emailMessageTemplate, [coach]);

                    })



                } else {
                    // console.warn(`No Coach found for Athlete having uid ${athleteUID}`);

                    return false
                }

            }

            return grouped
        })
    }).catch((err) => {
        console.warn('oops, error', err)
        throw err
    })

}

async function getAllNonEngagementAthletes(utcHour: number): Promise<Array<{
    org: Organization;
    athletes: Array<Athlete>;
}>> {

    const allNonEngagementAthletesGroupedPromises = await getOrgCollectionRef()
        .where('nonEngagementConfiguration.utcRunHour', '==', utcHour)
        .get()
        .then(async (orgsQuerySnap: FirebaseFirestore.QuerySnapshot) => {
            if (orgsQuerySnap.size) {
                const orgs = await orgsQuerySnap.docs
                    .map((queryDocSnap: FirebaseFirestore.QueryDocumentSnapshot) => {
                        return getDocumentFromSnapshot(queryDocSnap) as Organization;
                    });
                console.log('orgs', orgs);
                return orgs;
            }
            else {
                console.log('No Organizations Found to Query');
                return [];
            }
        }).then(async (orgs: Organization[]) => {
            return await orgs.map(async (theOrg: Organization) => {
                return await getNonEngagementAthletes(theOrg);
            });
        });

    const allAthletes = await Promise.all(allNonEngagementAthletesGroupedPromises)

    return allAthletes
    // TODO: 
    // return {athletes: allAthletes, orgConfig }

}

