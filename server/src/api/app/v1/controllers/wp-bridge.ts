import { OrganizationInterface } from './../../../../models/organization.model';

import BaseCtrl from './base'
import { initialiseFirebase } from '../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';

import { ResponseObject } from '../../../../shared/ResponseObject';
import { isArray } from 'lodash';
import { getDistanceLeaderboardFromCubeJs } from '../callables/get-bq-leaderboard-distance-data';
import { OlapGetLeaderboardRequest } from '../callables/utils/warehouse/olap/interfaces/olap-get-leaderboard-request.interface';
import { OlapSecurityProfile } from '../callables/utils/warehouse/olap/interfaces/olap-security-profile.interface';
import { getDistanceStatisticsFromCubeJs } from '../callables/get-bq-distance-statistics-data';
import { Athlete } from '../../../../models/athlete/athlete.model';
import { Response, Request, response } from 'express';
import { getSharedSystemConfiguration } from '../callables/utils/warehouse/olap/get-olap-jwt-package';
import { DistanceLeaderboardResponse } from '../callables/utils/warehouse/olap/interfaces/distance-leaderboard-response.interface';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';

interface WordpressBridgeRequest {
    token: string;
}
/**
* The controller handles the requests passed along by the router
// * @param logger: winston.Logger
*/
export default class WordpressBridgeCtrl extends BaseCtrl {
    model = Athlete;
    collection = FirestoreCollection.Athletes;
    res: Response;
    orgId: string = 'WHNiCyWShXAfFJtLtUYq';

    constructor() {
        super()
    }

    getTotalNumberOfUsers = async (req: Request, res: Response) => {
        this.res = res;

        const password = await this.getRestPassword();
        const request = req.body as WordpressBridgeRequest;
        if (request.token !== password) {
            console.error('Restricted Access Attempt');
            return this.handleResponse({
                message:
                    'Restricted Access',
                responseCode: 403,
                data: undefined,
                error: {
                    message:
                        'Restricted Access',
                    exception: undefined,
                },
                method: {
                    name: 'getTotalNumberOfUsers',
                    line: 1,
                },
                res
            });
        } else {
            console.log('Password is correct');
        }

        try {
            const db = initialiseFirebase('API').db; 
            const userCount = await db.collection(FirestoreCollection.Athletes)
                    .get()
                    .then((querySnap) => {
                        return querySnap.size;
                    })
                    .catch((e) => {
                        console.error(`Error raised retrieving athletes collection.  Message: ${e.message}`);
                        return 0;
                    });
            
            return this.handleResponse({
                message: '',
                res: this.res,
                responseCode: 200,
                data: {
                    userCount
                }
            });
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception);
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /wp-bridge/users',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /wp-bridge/users',
                    exception: exception,
                },
                method: {
                    name: 'getTotalNumberOfUsers',
                    line: 76,
                },
                res
            });
        }
    }

    getTotalDistance = async (req: Request, res: Response) => {
        this.res = res;

        const password = await this.getRestPassword();
        const request = req.body as WordpressBridgeRequest;
        if (request.token !== password) {
            console.error('Restricted Access Attempt');
            return this.handleResponse({
                message:
                    'Restricted Access',
                responseCode: 403,
                data: undefined,
                error: {
                    message:
                        'Restricted Access',
                    exception: undefined,
                },
                method: {
                    name: 'getTotalNumberOfUsers',
                    line: 1,
                },
                res
            });
        } else {
            console.log('Password is correct');
        }

        try {
            const db = initialiseFirebase('API').db; 
            const org: OrganizationInterface = await db.collection(FirestoreCollection.Organizations)
                    .doc(this.orgId)
                    .get()
                    .then((doc) => {
                        return getDocumentFromSnapshot(doc) as OrganizationInterface;
                    })
                    .catch((e) => {
                        console.log(`Error raised retrieving Shared SystemConfiguration document.  Message: ${e.message}`);
                        return undefined;
                    });
            return this.handleResponse({
                message: '',
                res: this.res,
                responseCode: 200,
                data: {
                    totalDistance: (org && org.currentEvolution && org.currentEvolution.totalDistance) ? org.currentEvolution.totalDistance : 0
                }
            });
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /wp-bridge/distance',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /wp-bridge/distance',
                    exception: exception,
                },
                method: {
                    name: 'getDistanceLeaderboard',
                    line: 76,
                },
                res
            });
        }
    }

    getDistanceLeaderboard = async (req: Request, res: Response) => {
        this.res = res;

        const password = await this.getRestPassword();
        const request = req.body as WordpressBridgeRequest;
        if (request.token !== password) {
            console.error('Restricted Access Attempt');
            return this.handleResponse({
                message:
                    'Restricted Access',
                responseCode: 403,
                data: undefined,
                error: {
                    message:
                        'Restricted Access',
                    exception: undefined,
                },
                method: {
                    name: 'getTotalNumberOfUsers',
                    line: 1,
                },
                res
            });
        } else {
            console.log('Password is correct');
        }

        try {
            const olapRequest = await this.getOlapLeaderboardRequest();
            const data = await getDistanceLeaderboardFromCubeJs(olapRequest);

            return this.handleResponse({
                message: '',
                res: this.res,
                responseCode: 200,
                data
            });

        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /wp-bridge/leaderboard',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /wp-bridge/leaderboard',
                    exception: exception,
                },
                method: {
                    name: 'getDistanceLeaderboard',
                    line: 76,
                },
                res
            });
        }
    }

    private getRestPassword = async (): Promise<string> => {
        const config = await getSharedSystemConfiguration(); 
        return (config && config.rest && config.rest.password) ? config.rest.password : 'c8k488ud3umxqHvF'; 
    }

    private getOlapLeaderboardRequest = async (): Promise<OlapGetLeaderboardRequest> => {
        const config = await getSharedSystemConfiguration(); 

        return {
            requestorSecurityProfile: this.getTempSecurityProfile(),
            queryGroupId: this.orgId,
            queryDaySets: [],
            startDate: config.event.startDate,
            endDate: config.event.endDate,
            granularity: 'month',
            excludeAthleteData: true
        } as unknown as OlapGetLeaderboardRequest;
    }

    private getTempSecurityProfile = (): OlapSecurityProfile => {
        return {
            athleteId: '123456',
            groups: [],
            organizationId: this.orgId
        } as unknown as OlapSecurityProfile;
    }
}
