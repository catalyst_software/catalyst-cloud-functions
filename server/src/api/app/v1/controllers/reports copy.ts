
// import { Athlete } from './../../../../models/athlete/interfaces/athlete';
// import { Response, Request } from 'express';

// import BaseCtrl from './base'
// import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'
// import { db } from '../../../../shared/init/initialise-firebase'
// import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
// import { ReportRequest } from '../../../../models/report-model';
// import { createTask } from '../../../../db/reports/createTask';
// import { isProductionProject } from '../is-production';


// import { generateReportRequest } from './utils/reports/generateReportRequest';
// import { getBiometricEntriesDeprecated } from './utils/reports/helpers/getBiometricEntriesDeprecated';
// import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
// import { Organization } from '../../../../models/organization.model';
// import moment from 'moment';
// import { firestore } from 'firebase-admin';
// import { generateInspireOrgSubs, iosNotification, TransactionAuditRecord } from './utils/reports/generateInspireOrgSubs';
// import { pick } from 'lodash';
// import { groupByMap } from '../../../../shared/utils/groupByMap';


// // export const pick = <T extends object, U extends keyof T>(
// //     obj: T,
// //     paths: Array<U>
// // ): Pick<T, U> => {
// //     const ret = Object.create(null);
// //     for (const k of paths) {
// //         ret[k] = obj[k];
// //     }
// //     return ret;
// // }
// // function pick<T extends object, U extends keyof T>(
// //     obj: T,
// //     paths: Array<U>
// //   ): Pick<T, U> {
// //     const ret = Object.create(null);
// //     for (const k of paths) {
// //       ret[k] = obj[k];
// //     }
// //     return ret;
// //   }

// const getSubset = (obj, ...keys) => keys.reduce((a, c) => ({ ...a, [c]: obj[c] }), {});


// /**
//  * The controller is next in the pipeline to handle the requests,
//  * as you can see the req and res has just been routed here from the router
//  * @export
//  * @class ReportCtrl
//  * @extends {BaseCtrl}
//  */
// export default class ReportsCtrl extends BaseCtrl {
//     res: Response;

//     // logger: Logger;
//     model = ReportRequest;
//     entries;

//     collection = FirestoreCollection.Reports;
//     aggregateDocumentManager: AggregateDocumentManager;

//     constructor() {
//         super()
//     }

//     private generateTaskCode() {
//         const code = Math.random().toString(36).substring(6, 12);
//         return Promise.resolve(code)
//         // return this.db.collection(this.collection)
//         //   .where('organizationCode', '==', code)
//         //   .get()
//         //   .then((snapshot) => {

//         //     if (snapshot.empty) {
//         //       return code
//         //     }

//         //     else {
//         //       return undefined
//         //     }
//         //   })
//     }


//     /**
//         * Get all Reports from the database
//         *
//         * @memberof ReportsCtrl
//         */
//     getInspireOrgSubs = async (req: Request, res: Response): Promise<Response> => {

//         // console.log(req.params);

//         this.res = res;

//         // console.log('theQuery', theQuery)

//         // return res.send(Buffer.from(reportRequestResponse.allResults));


//         try {

//             const reportRequestResponse = await generateInspireOrgSubs(<ReportRequest>req.body);

//             // if (reportRequestResponse.allResults.length) {
//             if (reportRequestResponse.allNotifications.length) {
//                 // notifications collection - not used any longer
//                 // const latest_receipt_info = {

//                 //     app_item_id: '1504901226',
//                 //     athleteUid: 'P19Z3hbipjVktVDdzaRih2xX01I3',
//                 //     bid: 'com.inspiresportonline.sof.ios',
//                 //     bvrs: '1.0.5',
//                 //     creationTimestamp: 'Mon Jun 29 2020 22: 58: 54 GMT + 0100(British Summer Time)',
//                 //     expires_date: '1624348140000',
//                 //     expires_date_formatted: '2021-06-22 07:49:00 Etc/GMT',
//                 //     expires_date_formatted_pst: '2021-06-22 00:49:00 America/Los_Angeles',
//                 //     is_in_intro_offer_period: 'false',
//                 //     is_trial_period: 'false',
//                 //     item_id: '1504901777',
//                 //     original_purchase_date: '2020-06-15 07:49:01 Etc/GMT',
//                 //     original_purchase_date_ms: '1592207341000',
//                 //     original_purchase_date_pst: '2020-06-15 00:49:01 America/Los_Angeles',
//                 //     original_transaction_id: '90000769875811',
//                 //     platform: 'iOS',
//                 //     product_id: 'com.sof.prod.subscriptions.annual.basic',
//                 //     purchase_date: '2020-06-22 07:49:00 Etc/GMT',
//                 //     purchase_date_ms: '1592812140000',
//                 //     purchase_date_pst: '2020-06-22 00:49:00 America/Los_Angeles',
//                 //     quantity: '1',
//                 //     subscription_group_identifier: '20613404',
//                 //     transaction_id: '90000773381620',
//                 //     uid: 'HpV7DqRDdIBAJaISmPAm',
//                 //     unique_identifier: '00008030-001E4D8C2ED8802E',
//                 //     unique_vendor_identifier: 'E3D6C9B5-7B34-4BAF-AE46-77231D6FD326',
//                 //     version_external_identifier: '835761949',
//                 //     web_order_line_item_id: '90000287678620',
//                 // }

//                 // const iosdNoti = {

//                 //     auto_renew_product_id: 'com.sof.prod.subscriptions.annual.basic',
//                 //     auto_renew_status: 'false',
//                 //     auto_renew_status_change_date: '2020-06-27 21:57:32 Etc/GMT',
//                 //     auto_renew_status_change_date_ms: '1593295052000',
//                 //     auto_renew_status_change_date_pst: '2020-06-27 14:57:32 America/Los_Angeles',
//                 //     bid: 'com.inspiresportonline.sof.ios',
//                 //     bvrs: '1.0.5',
//                 //     environment: 'PROD',
//                 //     notification_type: 'DID_CHANGE_RENEWAL_STATUS',
//                 // }


//                 const doc
//                     : TransactionAuditRecord
//                     = {
//                     athleteUid: '',
//                     "notifications": [
//                         {
//                             "notificationType": "DID_CHANGE_RENEWAL_STATUS",
//                             "originalTransactionId": 160000741988518,
//                             "originalPurchaseDate": new Date(),
//                             "unhandledProps": {
//                                 "subscription_group_identifier": "20613404",
//                                 "app_item_id": "1504901226"
//                             },
//                             "uniqueIdentifier": "00008030-001D59D62142802E",
//                             "isTrialPeriod": true,
//                             "webOrderLineItemId": 160000272608264,
//                             "platform": "iOS",
//                             "productId": "com.sof.prod.subscriptions.monthly.basic",
//                             "itemId": 1504902462,
//                             "uniqueVendorIdentifier": "75F9B01F-D74A-4956-BBB0-158E492961F0",
//                             "bundleId": "com.inspiresportonline.sof.ios",
//                             "transactionId": 160000741988518,
//                             "expirationDate": new Date(),
//                             "creationTimestamp": new Date(),
//                             "purchaseDate": new Date(),
//                             "isInIntroOfferPeriod": true
//                         }
//                     ],
//                     "purchaseReceipt": {
//                         "originalPurchaseDate": new Date(),
//                         "creationTimestamp": new Date(),
//                         "startDate": new Date(),
//                         "autoRenewProductId": "com.sof.prod.subscriptions.monthly.basic",
//                         "app_item_id": "1504901226",
//                         "notificationType": "VALIDATING_INITIAL_BUY",
//                         "isTrialPeriod": true,
//                         "platform": "iOS",
//                         "sandbox": false,
//                         "status": "success",
//                         "quantity": 1,
//                         "webOrderLineItemId": 160000272608264,
//                         "bundleId": "com.inspiresportonline.sof.ios",
//                         "autoRenewing": true,
//                         "purchaseDate": new Date(),
//                         "productId": "com.sof.prod.subscriptions.monthly.basic",
//                         "expirationDate": new Date(),
//                         "uniqueIdentifier": "00008030-001D59D62142802E",
//                         "isTrial": true,
//                         "isInIntroOfferPeriod": false,
//                         "subscription_group_identifier": "20613404",
//                         "cancellationDate": 0,
//                         "originalTransactionId": 160000741988518,
//                         "isInBillingRetryPeriod": false,
//                         "itemId": 1504902462,
//                         "transactionId": 160000741988518
//                     },
//                     "uid": "160000741988518"
//                 }
//                 const { allNotifications } = reportRequestResponse

//                 const groupedByAthlete = groupByMap([].concat(...allNotifications), (entry) => entry.athlteUid)

//                 const length = groupedByAthlete.size;
//                 groupedByAthlete.forEach((an) => {

//                     const groupedByExpirationDate = groupByMap([].concat(...an), (entry: iosNotification) => moment(entry.expirationDate).format('YYMMDD'));

//                     groupedByExpirationDate.forEach((ed) => {
//                         const fff = JSON.stringify(ed[0])
//                         const ffff = ed.length > 1 ? JSON.stringify(ed[1]) : JSON.stringify(ed[0])

//                         if (fff !== ffff) {
//                             console.log()

//                         } else {
//                             console.log()

//                         }


//                     });
//                 });


//                 // const theData = {
//                 //     ...theNotifications
//                 //     // uid: transactionAuditRecord.uid,
//                 //     // athleteUid: transactionAuditRecord.athleteUid,
//                 //     // creationTimestamp: (transactionAuditRecord.creationTimestamp as any).toDate(),
//                 //     // platform: transactionAuditRecord.platform,

//                 //     // // creationTimestamp: i.iosNotification.latest_receipt_info.creationTimestamp.toDate(),
//                 //     // originalPurchaseDate: transactionAuditRecord.iosNotification.unified_receipt.latest_receipt_info.original_purchase_date,
//                 //     // expireDate: transactionAuditRecord.iosNotification.unified_receipt.latest_receipt_info.expires_date_formatted,
//                 //     // producID: transactionAuditRecord.iosNotification.unified_receipt.latest_receipt_info.length ? transactionAuditRecord.iosNotification.unified_receipt.latest_receipt_info[0].product_id : 'not known',
//                 //     // notificationType: transactionAuditRecord.iosNotification.notification_type,
//                 //     // autoRenewStatus: transactionAuditRecord.iosNotification.auto_renew_status,
//                 //     // autoRenewStatusChangeDate: transactionAuditRecord.iosNotification.auto_renew_status_change_date
//                 //     // ...i.iosNotification.latest_receipt_info
//                 // }



//                 // // USE Notifications
//                 // const csvData = [].concat(...reportRequestResponse.allNotifications
//                 //     .map((i) => {

//                 //         const theData = {
//                 //             uid: i.uid,
//                 //             athleteUid: i.athleteUid,
//                 //             creationTimestamp: (i.creationTimestamp as any).toDate(),
//                 //             platform: i.platform,

//                 //             // creationTimestamp: i.iosNotification.latest_receipt_info.creationTimestamp.toDate(),
//                 //             originalPurchaseDate: i.iosNotification.unified_receipt.latest_receipt_info.original_purchase_date,
//                 //             expireDate: i.iosNotification.unified_receipt.latest_receipt_info.expires_date_formatted,
//                 //             producID: i.iosNotification.unified_receipt.latest_receipt_info.length ? i.iosNotification.unified_receipt.latest_receipt_info[0].product_id : 'not known',
//                 //             notificationType: i.iosNotification.notification_type,
//                 //             autoRenewStatus: i.iosNotification.auto_renew_status,
//                 //             autoRenewStatusChangeDate: i.iosNotification.auto_renew_status_change_date
//                 //             // ...i.iosNotification.latest_receipt_info
//                 //         }

//                 //         return theData
//                 //     }))


//                 USE TransactoinReceipts
//                 const csvData = [].concat(...reportRequestResponse.allResults
//                     .map((transactionAuditRecord) => {


//                         const { purchaseReceipt, notifications = [],
//                             athleteUid,
//                             firstName,
//                             lastName } = transactionAuditRecord


//                         const theData = []
//                         if (!purchaseReceipt.sandbox) {
//                             // const hasInitialBuyNotification = !!notifications.find((n) => n.notificationType === 'VALIDATING_INITIAL_BUY')


//                             const theNotifications = []

//                             // if (!hasInitialBuyNotification) {

//                             //     const {
//                             //         webOrderLineItemId,
//                             //         itemId,
//                             //         uniqueIdentifier,
//                             //         startDate,
//                             //         autoRenewProductId,
//                             //         app_item_id,
//                             //         sandbox,
//                             //         status,
//                             //         quantity,
//                             //         bundleId,
//                             //         autoRenewing,
//                             //         isTrial,
//                             //         isInIntroOfferPeriod,
//                             //         subscription_group_identifier,
//                             //         cancellationDate,
//                             //         isInBillingRetryPeriod,


//                             //         originalPurchaseDate,
//                             //         purchaseDate,
//                             //         expirationDate,
//                             //         creationTimestamp,

//                             //         ...theCSVData

//                             //     } = purchaseReceipt

//                             //     theNotifications.push({
//                             //         ...theCSVData,
//                             //         athleteUid,
//                             //         firstName,
//                             //         lastName,
//                             //         productId: autoRenewProductId,

//                             //         originalPurchaseDate: (originalPurchaseDate as any).toDate(),
//                             //         purchaseDate: (purchaseDate as any).toDate(),
//                             //         expirationDate: (expirationDate as any).toDate(),
//                             //         creationTimestamp: (creationTimestamp as any).toDate(),
//                             //     })
//                             // }

//                             notifications.forEach((n, i) => {

//                                 const {
//                                     webOrderLineItemId,
//                                     itemId,
//                                     uniqueVendorIdentifier,
//                                     uniqueIdentifier,
//                                     unhandledProps,

//                                     originalPurchaseDate,
//                                     purchaseDate,
//                                     expirationDate,
//                                     creationTimestamp,

//                                     ...theCSVData

//                                 } = n


//                                 const ff = allNotifications[0]

//                                 theNotifications.push({
//                                     athleteUid,
//                                     firstName,
//                                     lastName,
//                                     originalPurchaseDate: !!originalPurchaseDate ? (originalPurchaseDate as any).toDate() : 'NOT SET',
//                                     purchaseDate: !!purchaseDate ? (purchaseDate as any).toDate() : 'NOT SET',
//                                     expirationDate: !!expirationDate ? (expirationDate as any).toDate() : 'NOT SET',
//                                     ...theCSVData,
//                                     creationTimestamp: !!creationTimestamp ? (creationTimestamp as any).toDate() : 'NOT SET',
//                                 })
//                                 // const subset = getSubset(notifications, 'color', 'annoying')
//                                 // const n = pick(notifications,
//                                 //     [
//                                 //         "notificationType",
//                                 //         "originalTransactionId"
//                                 //     ])\
//                             })
//                             // theNotifications.concat(theData2)


//                             const groupedByExpirationDate = groupByMap([].concat(...theNotifications), (entry: iosNotification) => moment(entry.expirationDate).format('YYMMDD'));


//                             // const theData = {
//                             //     ...theNotifications
//                             //     // uid: transactionAuditRecord.uid,
//                             //     // athleteUid: transactionAuditRecord.athleteUid,
//                             //     // creationTimestamp: (transactionAuditRecord.creationTimestamp as any).toDate(),
//                             //     // platform: transactionAuditRecord.platform,

//                             //     // // creationTimestamp: i.iosNotification.latest_receipt_info.creationTimestamp.toDate(),
//                             //     // originalPurchaseDate: transactionAuditRecord.iosNotification.unified_receipt.latest_receipt_info.original_purchase_date,
//                             //     // expireDate: transactionAuditRecord.iosNotification.unified_receipt.latest_receipt_info.expires_date_formatted,
//                             //     // producID: transactionAuditRecord.iosNotification.unified_receipt.latest_receipt_info.length ? transactionAuditRecord.iosNotification.unified_receipt.latest_receipt_info[0].product_id : 'not known',
//                             //     // notificationType: transactionAuditRecord.iosNotification.notification_type,
//                             //     // autoRenewStatus: transactionAuditRecord.iosNotification.auto_renew_status,
//                             //     // autoRenewStatusChangeDate: transactionAuditRecord.iosNotification.auto_renew_status_change_date
//                             //     // ...i.iosNotification.latest_receipt_info
//                             // }

//                             groupedByExpirationDate.forEach((subs, date) => {
//                                 // const athleteFullName = uid.split('__')[0];
//                                 // const athleteID = uid.split('__')[1];
//                                 // const userLinerChartData = [];

//                                 const sub = subs[0] as iosNotification

//                                 // const ff = allNotifications.find((n) => n.iosNotification.unified_receipt.latest_receipt_info
//                                 //     ? n.iosNotification.unified_receipt.latest_receipt_info.transaction_id === sub.transactionId
//                                 //     : (n.iosNotification as any).latest_receipt_info.transaction_id === sub.transactionId)
//                                 // const n = allNotifications.find((n) => n.iosNotification.)
//                                 theData.push(sub);
//                             });

//                         }

//                         return theData
//                     }))

//                 const { parse } = require('json2csv');

//                 const fields = Object.keys(csvData[0]);
//                 const opts = { fields };

//                 const csv = parse(csvData, opts);
//                 // console.log(csv)
//                 // return res.send(csv);
//                 // res.set('Content-Type', 'application/octet-stream');
//                 // res.send(csv);

//                 res.attachment('subs.csv');
//                 return res.status(200).send(csv);

//                 // return res.send(Buffer.from(csv));
//             } else {

//                 return res.json({ message: 'No Data' });
//             }

//         } catch (err) {
//             console.error(err);
//             return res.json(err);
//         }




//     };



//     /**
//         * Get all Reports from the database
//         *
//         * @memberof ReportsCtrl
//         */
//     generateReports = async (req: Request, res: Response): Promise<Response> => {

//         // console.log(req.params);

//         this.res = res;

//         // console.log('theQuery', theQuery)

//         const reportRequestResponse = await generateReportRequest(<ReportRequest>req.body);

//         return res.json(reportRequestResponse);



//     };

//     getInspireOrgCount = async (req: Request, res: Response): Promise<any> => {


//         const allAthletes: Array<Athlete> = []
//         const allBroekAthletes: Array<Athlete> = []

//         const response = db.collection('organizations').where('licensesPurchased', '==', 1).get().then(async (orgsSNap) => {


//             const allOrgs = await Promise.all(orgsSNap.docs.map((theOrgSnap) => {
//                 return getDocumentFromSnapshot(theOrgSnap) as Organization
//             }))


//             const inspireOrgs = allOrgs.filter((org) => org.licensesPurchased === 1)


//             await Promise.all(inspireOrgs.map(async (org) => {
//                 await db.collection('athletes')
//                     .where('organizationId', '==', org.uid)
//                     // .where('profile.subscription.type', '==', 0)
//                     .get().then(async (athQUesyrSNap) => {

//                         if (athQUesyrSNap.size) {
//                             console.log('ath found')
//                         }


//                         return athQUesyrSNap.docs.map(async (athSNap) => {
//                             const at = getDocumentFromSnapshot(athSNap) as Athlete

//                             if (at.profile && at.profile.subscription) {


//                                 const updateAth = verifyTimestamp(at);

//                                 if (updateAth) {
//                                     await athSNap.ref.update(at)
//                                 }

//                                 if (at.profile.subscription.type > 0) {

//                                     allAthletes.push(at)
//                                 }

//                             } else {
//                                 allBroekAthletes.push(at)
//                             }
//                         })

//                     })

//             }))

//             const res2 = JSON.stringify(allAthletes.map((ath) => {

//                 let theExpireDate

//                 try {
//                     theExpireDate = ath.profile && ath.profile.subscription && ath.profile.subscription.expirationDate ? ath.profile.subscription.expirationDate.toDate() : 'UNKNOWN'
//                 } catch (error) {
//                     theExpireDate = ath.profile && ath.profile.subscription ? ath.profile.subscription.expirationDate : 'UNKNOWN'
//                 }

//                 let theCommencementDate

//                 try {
//                     theExpireDate = ath.profile && ath.profile.subscription && ath.profile.subscription.commencementDate ? ath.profile.subscription.commencementDate.toDate() : 'UNKNOWN'
//                 } catch (error) {
//                     theExpireDate = ath.profile && ath.profile.subscription ? ath.profile.subscription.commencementDate : 'UNKNOWN'
//                 }

//                 return { uid: ath.uid, subs: { ...ath.profile.subscription, expirationDate: theExpireDate, commencementDate: theCommencementDate } }
//             }))
//             console.log(JSON.parse(res2))
//             console.log(res2)
//             // const theResults = [].concat(...theAthletes)

//             return allAthletes
//             // const theGroups = await Promise.all(inspireOrgs.map(async (org) => {


//             //     // const organizationsRef = getOrgCollectionRef();

//             //     // // Create a query against the collection.
//             //     // //   const query = organizationsRef.where('state', '==', 'CA')
//             //     // const query = organizationsRef.where(
//             //     //     'tasks',
//             //     //     'array-contains',
//             //     //     'taskName.ipScoreNonEnagagement'
//             //     // );

//             //     // return query.get().then(() => {
//             //     //     return directive
//             //     // })

//             //     const groups = await db.collection('groups').where('organizatoinId', '==', org.uid).get().then(async (groupsSNap) => {

//             //         const allOrgGroups = await Promise.all(groupsSNap.docs.map((theOrgSnap) => {
//             //             return getDocumentFromSnapshot(theOrgSnap) as Group
//             //         }))

//             //         return allOrgGroups;
//             //     })

//             //     return groups;
//             //     // return true

//             // }));


//             // [].concat(...theGroups).map((group: Group) => {

//             //     !!group.athletes && group.athletes.map(async (ath) => {


//             //         const allAthletes = await db.collection('athletes')
//             //             .where('', '==', 1)
//             //             .where('organizationId', '==', 1)
//             //             .get().then(async (athQUesyrSNap) => {

//             //                 return athQUesyrSNap.docs.map((athSNap) => {
//             //                     return getDocumentFromSnapshot(athSNap) as Athlete
//             //                 })

//             //             })

//             //         const allOrgGroups = await Promise.all(groupsSNap.docs.map((theOrgSnap) => {
//             //             return getDocumentFromSnapshot(theOrgSnap) as Group
//             //         }))

//             //         return allOrgGroups;
//             //     })
//             // })
//         })

//         return res.status(200).json(await response)

//     }


//     /**
//      * Get all Reports from the database
//      *
//      * @memberof ReportsCtrl
//      */
//     getAllReports = async (req: Request, res: Response): Promise<Response> => {
//         // const { id } = <{ id: any }>req.params
//         console.log(req.params);

//         this.res = res;


//         const snapshot = await db.collection(FirestoreCollection.Reports).get();
//         return res.json(snapshot.docs.map(doc => doc.data()));

//     };

//     /**
//      * Gets a Report by uid from the database
//      *
//      * @memberof ReportsCtrl
//      */
//     getReport = async (req: Request, res: Response): Promise<any> => {
//         const { uid } = <{ uid: any }>req.params;
//         const me = this;

//         this.res = res;

//         const docRef = db.collection(FirestoreCollection.Reports).doc(uid);

//         return docRef
//             .get()
//             .then(function (doc) {
//                 if (doc.exists) {
//                     console.log('Document data:', doc.data());
//                     return me.handleResponse({
//                         message: 'Report Retrieved',
//                         responseCode: 201,
//                         data: {
//                             report: doc.data(),
//                         },
//                         method: {
//                             name: 'getReport',
//                             line: 52,
//                         },
//                         res: this.res
//                     })
//                 } else {
//                     console.log('No such document!');
//                     return me.handleResponse({
//                         message: 'Unable to retrieve Report - No such document!',
//                         responseCode: 204,
//                         method: {
//                             name: 'getReport',
//                             line: 52,
//                         },
//                         res: me.res
//                     })
//                 }
//             })
//             .catch(function (error) {
//                 console.log('Error getting document:', error);
//                 return me.handleResponse({
//                     message: 'Unable to retrieve Report - An error occured',
//                     responseCode: 500,
//                     method: {
//                         name: 'getReport',
//                         line: 52,
//                     },
//                     error,
//                     res: this.res
//                 })
//             })
//     };

//     /**
//      * Adds a Report to the database
//      *
//      * @memberof ReportCtrl
//      */
//     addReport = async (
//         req: Request,
//         res: Response
//     ): Promise<boolean | string> => {

//         // const { document: report } = <{
//         //     document: ReportRequest
//         // }>req.body;
//         const report = req.body;

//         console.log(req.body);
//         const me = this;

//         this.res = res;

//         return this.generateTaskCode().then((taskId) => {

//             report.taskId = taskId;

//             console.log('taskId', taskId);
//             return db
//                 .collection(FirestoreCollection.Reports)
//                 .add(report)
//                 .then(() => {

//                     const isProd = false;
//                     let project = '';
//                     let queue = '';
//                     let location = '';
//                     let options: { payload: ReportRequest; };
//                     // // PRODUCTION
//                     if (isProductionProject() || isProd) {
//                         project = 'inspire-219716';
//                         queue = 'create-reports-queue';
//                         location = 'europe-west3';
//                         options = { payload: report };
//                     } else {
//                         project = 'inspire-1540046634666';
//                         queue = 'create-reports-queue';
//                         location = 'us-central1';
//                         options = { payload: report };
//                     }

//                     console.log('options', options);
//                     return createTask(project, location, queue, options)
//                         .then((data) => {
//                             return me.handleResponse({
//                                 message: 'Report added',
//                                 responseCode: 201,
//                                 data: { taskId, data: data },
//                                 error: undefined,
//                                 method: {
//                                     name: 'addReport',
//                                     line: 58,
//                                 },
//                                 res: this.res
//                             })
//                         })


//                 })
//                 .catch(err => {
//                     return res.status(502).json({
//                         message: 'Error adding Report',
//                         responseCode: 501,
//                         data: {
//                             report: report,
//                         },
//                         error: err,
//                         method: {
//                             name: 'getReport',
//                             line: 58,
//                         },
//                     })
//                 })
//         })

//     };

//     deleteReport = async (
//         req: Request,
//         res: Response
//     ): Promise<boolean | string> => {
//         console.log(req.body);

//         this.res = res;

//         // TODO:
//         return true
//         // return admin
//         //     .auth()
//         //     .deleteReport(id)
//         //     .then(reportRecord => {
//         //         // See the ReportRecord reference doc for the contents of reportRecord.
//         //         console.log('...: ', reportRecord.uid);

//         //         return me.handleResponse({
//         //             message: 'Report added',
//         //             responseCode: 201,
//         //             data: {
//         //                 report: reportRecord,
//         //             },
//         //             error: undefined,
//         //             method: {
//         //                 name: 'deleteReport',
//         //                 line: 129,
//         //             },
//         //             res: this.res
//         //         })

//         //         // return res.status(200).json({ report: reportRecord });
//         //     })
//         //     .catch(function (error) {
//         //         console.log('Error creating new report: ', error);
//         //         return res.status(502).json({ error })
//         //     })
//     }
//     getBiometricsDeprecated = async (
//         req: Request,
//         res: Response
//     ) => {
//         console.log(req.body);

//         this.res = res;

//         const { entity } = req.body


//         // TODO:
//         return getBiometricEntriesDeprecated({ athleteId: entity.entityId })
//         // return admin
//         //     .auth()
//         //     .deleteReport(id)
//         //     .then(reportRecord => {
//         //         // See the ReportRecord reference doc for the contents of reportRecord.
//         //         console.log('...: ', reportRecord.uid);

//         //         return me.handleResponse({
//         //             message: 'Report added',
//         //             responseCode: 201,
//         //             data: {
//         //                 report: reportRecord,
//         //             },
//         //             error: undefined,
//         //             method: {
//         //                 name: 'deleteReport',
//         //                 line: 129,
//         //             },
//         //             res: this.res
//         //         })

//         //         // return res.status(200).json({ report: reportRecord });
//         //     })
//         //     .catch(function (error) {
//         //         console.log('Error creating new report: ', error);
//         //         return res.status(502).json({ error })
//         //     })
//     }
// }
// function verifyTimestamp(at: Athlete) {

//     let updateAth: boolean;
//     try {
//         if (typeof at.profile.subscription.expirationDate === 'string' && (at as any).profile.subscription.expirationDate !== 'UNKNOWN') {
//             at.profile.subscription.expirationDate = firestore.Timestamp.fromDate(moment(at.profile.subscription.expirationDate).toDate());
//             updateAth = true;
//         }
//         if (typeof at.profile.subscription.commencementDate === 'string' && (at as any).profile.subscription.commencementDate !== 'UNKNOWN') {
//             at.profile.subscription.commencementDate = firestore.Timestamp.fromDate(moment(at.profile.subscription.commencementDate).toDate());
//             updateAth = true;
//         }
//     }
//     catch (error) {
//         console.warn(error);
//     }
//     return updateAth;
// }

