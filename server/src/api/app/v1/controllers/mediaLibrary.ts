import { Response, Request } from 'express'
// import { FieldValue } from '@google-cloud/firestore'
import moment from "moment";
import * as functions from 'firebase-functions'
// import algoliasearch from 'algoliasearch';

const APP_ID = 'DA7JUIHYUT'; // functions.config().algolia.app;
const ADMIN_KEY = '8fd5cf5a821e6b84bd35a8f83ac453ae'; //unctions.config().algolia.key;

// const client = algoliasearch(APP_ID, ADMIN_KEY);
// const index = client.initIndex('media-library-catalogue');


import BaseCtrl from './base'
import {
    FirestoreCollection,
} from '../../../../db/biometricEntries/enums/firestore-collections'
import { db } from '../../../../shared/init/initialise-firebase'
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { SportsNutrition } from './sportsNutrition'
import { SportsPsychology } from './sportsPsychology'
import { SportsScience } from './sportsScience'
import { General } from './general'

import * as  faker from 'faker';
import { cloneDeep } from 'lodash'
import { removeUndefinedProps } from './utils/remove-undefined-props'
import { ContentFeedCardPackageBase, VideoCardPackageBase } from '../../../../models/report-model'
import { NotificationCardType } from '../../../../models/enums/UpdateNotificationCardMetadataDirective'

// import { Program } from '../../../../models/athlete/interfaces/athlete'

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class ProgramCtrl
 * @extends {BaseCtrl}
 */
export default class MediaLibraryCtrl extends BaseCtrl {
    res: Response

    generalModel = General
    sportsNutritionModel = SportsNutrition
    sportsPsychologyModel = SportsPsychology
    sportsScienceModel = SportsScience

    // logger: Logger;
    model = MediaLibraryCtrl
    collection = FirestoreCollection.DummyMedia
    entries
    aggregateDocumentManager: AggregateDocumentManager
    dummyMedia

    constructor() {
        super()
    }


    addVideoCard = async (req: Request, res: Response): Promise<any> => {

        this.res = res
        try {
            let doc: VideoCardPackageBase = req.body
            const now = new Date()
            
            doc = {
                ...doc,
                creationTimestamp: now,
                actualCreationTimestamp: now,
                stickyStaticExpiryDate: moment(doc.stickyStaticExpiryDate, 'DD/MM/YYYY').toDate(),
                mediaContentEntry: JSON.stringify(doc.mediaContentEntry)
            }

            if (doc.isStickyStatic) {
                doc.creationTimestamp = moment('01/01/1970', 'DD/MM/YYYY').toDate()
            }

            const theRes = await (db.collection(FirestoreCollection.NotificationCardRegistry).doc().set(doc)
                .then((querySnap) => {
                    if (querySnap) {
                        return true
                    } else {
                        return false
                    }

                }))

            return res.json(theRes)

        } catch (err) {
            console.log(
                { err }
            )
            throw err
        }
    }

    addLibraryToIndex = async (req: Request, res: Response) => {

        this.res = res
        try {
            const theRes = await (db.collection(FirestoreCollection.DummyMedia).get()
                .then((querySnap) => {

                    return Promise.all((querySnap.docs || []).map((docSnap) => {
                        const data = {
                            ...docSnap.data(),
                            creationTimestamp: docSnap.data().creationTimestamp.toDate()
                        } as {
                            creationTimestamp: Date;
                            parentUid?: string;
                            tags?: string[]

                        };
                        const objectID = docSnap.id;

                        if (!data.parentUid) {
                            data.tags = ['isRootItem'];
                        }

                        return true;

                        // return index.saveObject({ ...data, objectID }).then((r) => {
                        //     console.log(r)
                        //     return true
                        // }).catch((e) => {
                        //     console.error(e)
                        //     return false
                        // })

                    }))


                }))

            return res.json(theRes)

        } catch (err) {
            console.log(
                { err }
            )
            throw err
        }
    }

    /**
     * Adds a Historic Program to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof MedaiLibraryCtrl
     */

    addDummyMedia = async (req: Request, res: Response): Promise<any> => {
        // const { id } = <{ id: any }>req.params

        this.res = res
        this.dummyMedia = Array(100)
            .fill(1).map((i, idx) => {
                return {
                    folder: 'Folder ' + idx,
                    name: faker.name.findName(),
                    org: faker.company.companyName(),
                    date: faker.date.between('2020-01-01', '2020-08-05'),
                    itemCount: faker.random.number({
                        'min': 3,
                        'max': 67
                    })
                }
            })

        try {
            const tagsCollection = db.collection(this.collection);

            // Begin a new batch
            const batch = db.batch();

            // Set each document, as part of the batch
            this.dummyMedia.forEach(t => {
                const ref = tagsCollection.doc();
                batch.set(ref, t);
            })

            // Commit the entire batch
            return res.json(batch.commit())

        } catch (err) {
            console.log(
                { err }
            )
            throw err
        }
    }
    updateDummyMedia = async (req: Request, res: Response): Promise<any> => {
        // const { id } = <{ id: any }>req.params

        this.res = res
        try {

            // let folderCount = 1
            // let fileCount = 1
            const dummyMedia = await (db.collection(this.collection).get()
                .then((querySnap) => {

                    return Promise.all(querySnap.docs.map(async (queryDocSnap) => {


                        const mediaType = Math.random() >= 0.7 ? "folder" : 'file'


                        const theRes = {
                            uid: queryDocSnap.id,
                            ...queryDocSnap.data(),
                            // // date: queryDocSnap.data().date.toDate(),
                            creationTimestamp: queryDocSnap.data().creationTimestamp.toDate(),
                            parentUid: queryDocSnap.data().parentUid ? queryDocSnap.data().parentUid : null,
                            // itemTitle: mediaType === 'folder' ? 'Folder ' + folderCount : 'File ' + fileCount,
                            // // "Title": "Folder A",
                            // mediaType,
                            // filesInFolder: mediaType === 'folder' ? [] : undefined,
                            // itemCount: mediaType === 'folder' ? 0 : undefined
                        } as {

                            author: string;
                            creationTimestamp: Date;
                            itemCount: number;
                            itemTitle: string;
                            mediaType: string,
                            name: string,
                            org: string,
                            parentUid: string;
                            uid: string;
                            folder: string;
                            itemUid: string;
                            filesInFolder: string[];
                        }

                        // mediaType === 'folder' ? folderCount++ : fileCount++

                        const { ...theRest } = theRes

                        removeUndefinedProps(theRest)
                        await queryDocSnap.ref.set(theRest).catch((e) => {
                            console.log(e)
                        })
                        return theRest
                    }))

                }));



            function randChunkSplit(arr, theMin, theMax) {
                // uncomment this line if you don't want the original array to be affected
                // var arr = arr.slice();
                const arrs = [];
                let size = 1;
                const min = theMin || 1;
                const max = theMax || min || 1;
                while (arr.length > 0) {
                    size = Math.min(max, Math.floor((Math.random() * max) + min));
                    arrs.push(arr.splice(0, size));
                }
                return arrs;
            }
            const allFolderMedia = cloneDeep(dummyMedia.filter((d) => d.mediaType === 'folder'))
            const allFileMedia = cloneDeep(dummyMedia.filter((d) => d.mediaType === 'file'))
            const batched = randChunkSplit(allFileMedia, 0, 6)
            for (let mlIndex = 0; mlIndex < (allFolderMedia.length / 2 - 5); mlIndex++) {

                const randIndex = Math.floor((Math.random() * allFolderMedia.length))
                const elements = batched[mlIndex];

                let randomFolder = allFolderMedia[randIndex]

                if (randomFolder) {
                    elements.forEach(element => {
                        element.parentUid = randomFolder.uid

                    });

                    randomFolder.itemCount = randomFolder.itemCount += elements.length
                    randomFolder.filesInFolder = elements.map((e) => e.uid)
                } else {
                    debugger

                    randomFolder = allFolderMedia[randIndex]
                    elements.forEach(element => {
                        element.parentUid = randomFolder.uid

                    });

                    randomFolder.itemCount = randomFolder.itemCount += elements.length
                    randomFolder.filesInFolder = elements.map((e) => e.uid)
                }
                allFolderMedia[randIndex] = randomFolder
            }
            const batchConcated = [].concat(...batched)
            this.dummyMedia = allFolderMedia.concat(...batchConcated)


            const updatedMedia = await (db.collection(this.collection).get()
                .then((querySnap) => {

                    return Promise.all(querySnap.docs.map(async (queryDocSnap) => {


                        // const mediaType = Math.random() >= 0.7 ? "folder" : 'file'


                        // const theRes = {
                        //     uid: queryDocSnap.id,
                        //     ...queryDocSnap.data(),
                        //     // // date: queryDocSnap.data().date.toDate(),
                        //     creationTimestamp: queryDocSnap.data().creationTimestamp.toDate(),
                        //     parentUid: queryDocSnap.data().parentUid ? queryDocSnap.data().parentUid : null,
                        //     // itemTitle: mediaType === 'folder' ? 'Folder ' + folderCount : 'File ' + fileCount,
                        //     // // "Title": "Folder A",
                        //     // mediaType,
                        //     // filesInFolder: mediaType === 'folder' ? [] : undefined,
                        //     // itemCount: mediaType === 'folder' ? 0 : undefined
                        // } as {

                        //     author: string;
                        //     creationTimestamp: Date;
                        //     itemCount: number;
                        //     itemTitle: string;
                        //     mediaType: string,
                        //     name: string,
                        //     org: string,
                        //     parentUid: string;
                        //     uid: string;
                        //     folder: string;
                        //     itemUid: string;
                        //     filesInFolder: string[];
                        // }

                        // // mediaType === 'folder' ? folderCount++ : fileCount++

                        // const { ...theRest } = theRes
                        const theUpdate = this.dummyMedia.find((d) => d.uid === queryDocSnap.id)
                        // removeUndefinedProps(theUpdate)
                        await queryDocSnap.ref.set(theUpdate).catch((e) => {
                            console.log(e)
                        })
                        return theUpdate
                    }))

                }));



            return res.json(dummyMedia)

        } catch (err) {
            console.log(
                { err }
            )
            throw err
        }
    }

    updateDummyMeta = async (req: Request, res: Response): Promise<any> => {

        this.res = res
        try {

            // let folderCount = 1
            // let fileCount = 1

            const cardPackages: ContentFeedCardPackageBase[] = await (db.collection(FirestoreCollection.NotificationCardRegistry).get()
                .then((querySnap) => {
                    if (querySnap.size) {
                        return Promise.all(querySnap.docs.map((queryDocSnap) => {
                            return {
                                uid: queryDocSnap.id,
                                ...queryDocSnap.data(),
                                // // // date: queryDocSnap.data().date.toDate(),
                                // creationTimestamp: queryDocSnap.data().creationTimestamp.toDate(),
                                // parentUid: queryDocSnap.data().parentUid ? queryDocSnap.data().parentUid : null,
                                // // itemTitle: mediaType === 'folder' ? 'Folder ' + folderCount : 'File ' + fileCount,
                                // // // "Title": "Folder A",
                                // // mediaType,
                                // // filesInFolder: mediaType === 'folder' ? [] : undefined,
                                // // itemCount: mediaType === 'folder' ? 0 : undefined
                            } as ContentFeedCardPackageBase
                        }))
                    } else {
                        return undefined
                    }

                }))

            const imgUrls = cardPackages.filter((c) => c.notificationCardType === NotificationCardType.Image)
            const pdfUrls = cardPackages.filter((c) => c.notificationCardType === NotificationCardType.Pdf)
            const videoUrls = cardPackages.filter((c) => c.notificationCardType === NotificationCardType.Video)

            const dummyMedia = await (db.collection(this.collection).get()
                .then((querySnap) => {

                    return Promise.all(querySnap.docs.map(async (queryDocSnap) => {


                        // const mediaType = Math.random() >= 0.7 ? "folder" : 'file'


                        const theRes = {
                            uid: queryDocSnap.id,
                            ...queryDocSnap.data(),
                            creationTimestamp: queryDocSnap.data().creationTimestamp.toDate()
                            // // // date: queryDocSnap.data().date.toDate(),
                            // creationTimestamp: queryDocSnap.data().creationTimestamp.toDate(),
                            // parentUid: queryDocSnap.data().parentUid ? queryDocSnap.data().parentUid : null,
                            // // itemTitle: mediaType === 'folder' ? 'Folder ' + folderCount : 'File ' + fileCount,
                            // // // "Title": "Folder A",
                            // // mediaType,
                            // // filesInFolder: mediaType === 'folder' ? [] : undefined,
                            // // itemCount: mediaType === 'folder' ? 0 : undefined
                        } as {

                            author: string;
                            creationTimestamp: Date;
                            itemCount: number;
                            itemTitle: string;
                            originalFileName: string;
                            mediaType: string,
                            name: string,
                            org: string,
                            parentUid: string;
                            uid: string;
                            folder: string;
                            itemUid: string;
                            fileType: string;
                            filesInFolder: string[];
                            link: string
                        }

                        // // mediaType === 'folder' ? folderCount++ : fileCount++

                        // const { ...theRest } = theRes

                        // removeUndefinedProps(theRest)
                        // await queryDocSnap.ref.set(theRest).catch((e) => {
                        //     console.log(e)
                        // })
                        return theRes
                    }))

                }));



            let imageInndex = 0
            let pdfIndex = 0
            const videoIndex = 0

            dummyMedia.filter((item) => item.mediaType === 'file').forEach((item, idx) => {
                // const fileType = Math.random() >= 0.7 ? Math.random() >= 0.3 ? "pdf" : 'image' : 'video'
                const fileType = Math.random() >= 0.3 ? "image" : 'pdf'
                item.fileType = fileType


                switch (fileType) {
                    case 'image':
                        if (imageInndex === imgUrls.length) imageInndex = 0
                        item.itemTitle = imgUrls[imageInndex].title
                        item.originalFileName = 'not set'
                        item.link = `https://storage.cloud.google.com/inspire-1540046634666.appspot.com/${imgUrls[imageInndex].imageUrl}`

                        imageInndex++
                        // case 'video':
                        //     item.link = videoUrls[index].imageUrl
                        // videoIndex++
                        break;
                    case 'pdf':
                        // tslint:disable-next-line: triple-equals
                        if (pdfIndex == pdfUrls.length) pdfIndex = 0
                        item.itemTitle = pdfUrls[pdfIndex].title
                        item.originalFileName = pdfUrls[pdfIndex].pdfFileName
                        item.link = `https://storage.cloud.google.com/inspire-1540046634666.appspot.com/INSPIRE/worksheet/${pdfUrls[pdfIndex].pdfFileGuid}`

                        const isPdf = item.link.slice(item.link.length - 4, item.link.length) === '.pdf'
                        if (!isPdf) {
                            item.link += '.pdf'
                        }

                        pdfIndex++
                        break;
                    default:
                        break;
                }
            })
            await Promise.all(dummyMedia.map(async (med) => {
                const updatedMedia = await db.collection(this.collection).doc(med.uid).set(med)
                    .then((querySnap) => {

                        return true

                    });

            }))

            // function randChunkSplit(arr, min, max) {
            //     // uncomment this line if you don't want the original array to be affected
            //     // var arr = arr.slice();
            //     var arrs = [], size = 1;
            //     var min = min || 1;
            //     var max = max || min || 1;
            //     while (arr.length > 0) {
            //         size = Math.min(max, Math.floor((Math.random() * max) + min));
            //         arrs.push(arr.splice(0, size));
            //     }
            //     return arrs;
            // }
            // const allFolderMedia = cloneDeep(dummyMedia.filter((d) => d.mediaType === 'folder'))
            // const allFileMedia = cloneDeep(dummyMedia.filter((d) => d.mediaType === 'file'))
            // const batched = randChunkSplit(allFileMedia, 0, 6)
            // for (let index = 0; index < (allFolderMedia.length / 2 - 5); index++) {

            //     let randIndex = Math.floor((Math.random() * allFolderMedia.length))
            //     const elements = batched[index];

            //     let randomFolder = allFolderMedia[randIndex]

            //     if (randomFolder) {
            //         elements.forEach(element => {
            //             element.parentUid = randomFolder.uid

            //         });

            //         randomFolder.itemCount = randomFolder.itemCount += elements.length
            //         randomFolder.filesInFolder = elements.map((e) => e.uid)
            //     } else {
            //         debugger

            //         let randomFolder = allFolderMedia[randIndex]
            //         elements.forEach(element => {
            //             element.parentUid = randomFolder.uid

            //         });

            //         randomFolder.itemCount = randomFolder.itemCount += elements.length
            //         randomFolder.filesInFolder = elements.map((e) => e.uid)
            //     }
            //     allFolderMedia[randIndex] = randomFolder
            // }
            // let batchConcated = [].concat(...batched)
            // this.dummyMedia = allFolderMedia.concat(...batchConcated)


            // const updatedMedia = await (db.collection(this.collection).get()
            //     .then((querySnap) => {

            //         return Promise.all(querySnap.docs.map(async (queryDocSnap) => {


            //             // const mediaType = Math.random() >= 0.7 ? "folder" : 'file'


            //             // const theRes = {
            //             //     uid: queryDocSnap.id,
            //             //     ...queryDocSnap.data(),
            //             //     // // date: queryDocSnap.data().date.toDate(),
            //             //     creationTimestamp: queryDocSnap.data().creationTimestamp.toDate(),
            //             //     parentUid: queryDocSnap.data().parentUid ? queryDocSnap.data().parentUid : null,
            //             //     // itemTitle: mediaType === 'folder' ? 'Folder ' + folderCount : 'File ' + fileCount,
            //             //     // // "Title": "Folder A",
            //             //     // mediaType,
            //             //     // filesInFolder: mediaType === 'folder' ? [] : undefined,
            //             //     // itemCount: mediaType === 'folder' ? 0 : undefined
            //             // } as {

            //             //     author: string;
            //             //     creationTimestamp: Date;
            //             //     itemCount: number;
            //             //     itemTitle: string;
            //             //     mediaType: string,
            //             //     name: string,
            //             //     org: string,
            //             //     parentUid: string;
            //             //     uid: string;
            //             //     folder: string;
            //             //     itemUid: string;
            //             //     filesInFolder: string[];
            //             // }

            //             // // mediaType === 'folder' ? folderCount++ : fileCount++

            //             // const { ...theRest } = theRes
            //             const theUpdate = this.dummyMedia.find((d) => d.uid === queryDocSnap.id)
            //             // removeUndefinedProps(theUpdate)
            //             await queryDocSnap.ref.set(theUpdate).catch((e) => {
            //                 console.log(e)
            //             })
            //             return theUpdate
            //         }))

            //     }));
            // await queryDocSnap.ref.set(theUpdate).catch((e) => {
            //     console.log(e)
            // })


            return res.json(dummyMedia)

        } catch (err) {
            console.log(
                { err }
            )
            throw err
        }
    }

    queryDummyMedia = async (req: Request, res: Response): Promise<any> => {
        this.res = res
        this.dummyMedia = Array(100)
            .fill(1).map((i, idx) => {
                return {
                    folder: 'Folder ' + idx,
                    name: faker.name.findName(),
                    org: faker.company.companyName(),
                    date: faker.date.between('2020-01-01', '2020-08-05'),
                    itemCount: faker.random.number({
                        'min': 3,
                        'max': 67
                    })
                }
            })



        try {
            const tagsCollection = db.collection(this.collection);

            //    const q =     tagsCollection
            //         .where('field', '>=',  'searchtext');
            //        q .where('field', '<', 'searchtext'.substring(0, 'searchtext'.length - 1) + String.fromCharCode(q.codeUnitAt('searchtext'.length - 1) + 1) )

            // Begin a new batch
            const batch = db.batch();

            // Set each document, as part of the batch
            this.dummyMedia.forEach(t => {
                const ref = tagsCollection.doc();
                batch.set(ref, t);
            })

            // Commit the entire batch
            return res.json(batch.commit())

        } catch (err) {
            console.log(
                { err }
            )
            throw err
        }
    }
}
