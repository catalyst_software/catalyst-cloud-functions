import { Athlete } from '../../../../../models/athlete/athlete.model';

export const getOrgIdFromGroupId = (athlete: Athlete): string => {
    return athlete.groups && athlete.groups.length
        ? athlete.groups[0].groupId.split('-').length
            ? athlete.groups[0].groupId.split('-')[0]
            : undefined
        : undefined;
};

export const getorgUIDFromGroupUID = (athlete: Athlete): string => {
    const orgIDVIAGroupUID = athlete.groups && athlete.groups.length ? athlete.groups[0].uid.split('-')[0] : undefined;

    if (orgIDVIAGroupUID === undefined) {
        return athlete.groups && athlete.groups.length ? athlete.groups[0].groupId.split('-')[0] : undefined;
    } else {

        return orgIDVIAGroupUID
    }
}