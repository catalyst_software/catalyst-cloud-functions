import { IOSReceipt } from '../../../../../models/purchase/interfaces/ios-receipt';
import { isEmptyObject } from './is-empty-object';
import { PlatformType } from '../../../../../models/purchase/enums/platfom_type';
export const extractIOSData = (latestReceiptInfo: {
    // quantity: number
    // unique_vendor_identifier: string
    bvrs: number;
    expires_date_formatted: string;
    original_purchase_date_ms: number;
    expires_date_formatted_pst: string;
    // is_in_intro_offer_period: boolean
    purchase_date_ms: number;
    is_in_intro_offer_period: boolean;
    original_purchase_date: string;
    original_purchase_date_pst: string;
    purchase_date: string;
    purchase_date_pst: string;
    is_trial_period: boolean;
    item_id: number;
    unique_identifier: string;
    original_transaction_id: string;
    expires_date: string;
    transaction_id: string;
    unique_vendor_identifier: string;
    version_external_identifier: number;
    web_order_line_item_id: number; // Posted only if the notification_type is CANCEL
    bid: string;
    product_id: string;
    quantity: number;
}) => {
    const { bid: bundleId, bvrs, product_id: productId, original_transaction_id: originalTransactionId, transaction_id: transactionId,
        // app_item_id: appItemId,
        original_purchase_date_ms: originalPurchaseDate, purchase_date_ms: purchaseDate, expires_date_formatted_pst, expires_date: expirationDate,
        is_trial_period: isTrialPeriod, item_id: itemId, unique_identifier: uniqueIdentifier, web_order_line_item_id: webOrderLineItemId,
        unique_vendor_identifier: uniqueVendorIdentifier, expires_date_formatted, is_in_intro_offer_period: isInIntroOfferPeriod,
        original_purchase_date, original_purchase_date_pst, purchase_date, purchase_date_pst, quantity, version_external_identifier, ...rest }
        = latestReceiptInfo;
    const purchaseDataExtractedFromReceipt: IOSReceipt = {
        platform: PlatformType.IOS,
        bundleId,
        productId,
        itemId: +itemId,
        uniqueIdentifier,
        uniqueVendorIdentifier,
        originalTransactionId: +originalTransactionId,
        transactionId: +transactionId,
        originalPurchaseDate: new Date(+originalPurchaseDate),
        purchaseDate: new Date(+purchaseDate),
        expirationDate: new Date(+expirationDate),
        isTrialPeriod: <any>isTrialPeriod === 'false' ? false : true,
        isInIntroOfferPeriod: <any>isInIntroOfferPeriod === 'false' ? false : true,
    };
    if (!isEmptyObject(rest))
        purchaseDataExtractedFromReceipt.unhandledProps = rest;
    if (webOrderLineItemId !== undefined && (webOrderLineItemId as any) !== '')
        purchaseDataExtractedFromReceipt.webOrderLineItemId = +webOrderLineItemId;
    return purchaseDataExtractedFromReceipt;
};
