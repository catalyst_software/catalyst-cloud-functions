import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections';
import { db } from '../../../../../shared/init/initialise-firebase';
import { Athlete } from '../../../../../models/athlete/athlete.model';
export const getAthleteByTransactionId = async (transactionId: string) => {
    return await db
        .collection(FirestoreCollection.Athletes)
        .where('transactionIds', 'array-contains', transactionId)
        .get()
        .then(async (querySnap: FirebaseFirestore.QuerySnapshot) => {

            if (!querySnap.empty) {
                const snap = querySnap.docs[0];
                const athlete = {
                    ...snap.data(),
                    uid: snap.id
                } as Athlete;
                return athlete;
            } else
                return undefined
        });
};
