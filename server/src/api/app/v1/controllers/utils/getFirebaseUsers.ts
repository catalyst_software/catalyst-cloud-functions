import { liveAdmin } from '../../../../../shared/init/initialise-firebase';

export const getFirebaseUsers = async (allUsers, pageToken?) => {
    return await liveAdmin
        .auth()
        .listUsers(1000, pageToken)
        .then((listUsersResult) => {
            listUsersResult.users.forEach(function (userRecord) {
                // console.log('user', userRecord.toJSON())
                allUsers.push(userRecord.toJSON());
            });
            // if (listUsersResult.pageToken) {
            //     // List next batch of users.
            //     listAllUsers(listUsersResult.pageToken)
            // }
            if (listUsersResult.pageToken) {
                return getFirebaseUsers(allUsers, listUsersResult.pageToken);
            }
            else {
                return allUsers;
            }
            // return res.status(200).send(listUsersResult.users)
        })
        .catch((error) => {
            console.log('Error listing users:', error);
            // return res.status(500).send(error)
            return error;
        });
};
