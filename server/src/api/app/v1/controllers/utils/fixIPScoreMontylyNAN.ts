import { firestore } from 'firebase-admin';

import { db } from '../../../../../shared/init/initialise-firebase';

import { IpScoreAggregateHistoryCollection } from '../../../../../db/ipscore/enums/firestore-ipscore-aggregate-history-collection';

import { setNANtoZero } from '../../../../../db/ipscore/services/save-aggregates';
import { getDocumentFromSnapshot } from '../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { IpScoreAggregateHistoryRecord } from '../../../../../db/ipscore/models/documents/aggregates/ipscore-aggregate-history-record';
import { IpScoreAggregateComponentInterface } from '../../../../../db/ipscore/models/interfaces/ipscore-component-aggregate';
import { isUndefined } from 'util';

const ipScoreAggregateHistoryCollectionKeys = getKeys(IpScoreAggregateHistoryCollection);

export const fixIPScoreMontylyNAN = async () => {

    const collections = [];

    // addDocumentReferences(aggregateCollectionKeys, AggregateCollection, collections);
    // addDocumentReferences(aggregateHistoryCollectionKeys, AggregateHistoryCollection, collections);
    // addDocumentReferences(ipScoreAggregateCollectionKeys, IpScoreAggregateCollection, collections);
    addDocumentReferences(ipScoreAggregateHistoryCollectionKeys, IpScoreAggregateHistoryCollection, collections);

    const promises = [];

    collections
        .filter((collection) => collection === 'athleteIpScoreMonthlyHistoryAggregates').map(async (collection) => {
            const docRef = db
                .collection(collection)
            // .where('entityId', '==', entityId || 'rPSzxsM4vAZ2aNgD15qG');
            // .where('entityId', '==', entityId || 'rPSzxsM4vAZ2aNgD15qG-YR9');
            const promise = await docRef
                .get()
                .then(async (docsQuerySnap: firestore.QuerySnapshot) => {
                    if (!docsQuerySnap.empty) {


                        const updateResult = await Promise.all(docsQuerySnap.docs.map(async docSnap => {

                            const theAggregateDoc: IpScoreAggregateHistoryRecord = getDocumentFromSnapshot(docSnap) as IpScoreAggregateHistoryRecord

                            const { body, brain, food, groupId, groupName, programs, training, ...theAggregateDocRest } = theAggregateDoc as any

                            if (!isUndefined(body)) {
                                return await docSnap.ref.set(theAggregateDocRest).then(() => {
                                    return {
                                        success: true,
                                        docUID: theAggregateDoc.uid,
                                        collection: theAggregateDoc.collection,
                                        entityName: theAggregateDoc.entityName,
                                        entityId: theAggregateDoc.entityId,
                                    }
                                }).catch(() => {
                                    return {
                                        success: false,
                                        docUID: theAggregateDoc.uid,
                                        collection: theAggregateDoc.collection,
                                        entityName: theAggregateDoc.entityName,
                                        entityId: theAggregateDoc.entityId,
                                    }
                                })
                            } else {
                                return {
                                    success: true,
                                    docUID: theAggregateDoc.uid,
                                    collection: theAggregateDoc.collection,
                                    entityName: theAggregateDoc.entityName,
                                    entityId: theAggregateDoc.entityId,
                                }
                            }




                            if (theAggregateDoc.values) {

                                setNANtoZero(theAggregateDoc.values, true)
                            } else {
                                console.log('Values UNDEFINED')
                            }

                            const { componentValues, recentEntries, trackingCache } = theAggregateDoc

                            let saveDoc = false

                            if (componentValues) {

                                componentValues.map((recentEntry: IpScoreAggregateComponentInterface) => {
                                    saveDoc = setNANtoZero(recentEntry, saveDoc) || saveDoc
                                })

                            }

                            if (recentEntries) {

                                const recentEntriesValuesOnly = recentEntries.map((recentEntry: IpScoreAggregateComponentInterface) => {
                                    saveDoc = setNANtoZero(recentEntry, saveDoc) || saveDoc
                                    return {
                                        value: recentEntry.value
                                    }

                                })
                                if (theAggregateDoc.recentEntries.length > 7 || theAggregateDoc.recentEntries[0].body !== undefined) {
                                    saveDoc = true;
                                    theAggregateDoc.recentEntries = recentEntriesValuesOnly.slice(0, 7);
                                }
                            }

                            if (trackingCache) {

                                // trackingCache.map((recentEntry: IpScoreAggregateComponentInterface) => { 
                                saveDoc = setNANtoZero(trackingCache, saveDoc) || saveDoc
                                // })

                            }

                            // if (saveDoc) {

                            return await docSnap.ref.update(theAggregateDocRest).then(() => {
                                return {
                                    success: true,
                                    docUID: theAggregateDoc.uid,
                                    collection: theAggregateDoc.collection,
                                    entityName: theAggregateDoc.entityName,
                                    entityId: theAggregateDoc.entityId,
                                }
                            }).catch(() => {
                                return {
                                    success: false,
                                    docUID: theAggregateDoc.uid,
                                    collection: theAggregateDoc.collection,
                                    entityName: theAggregateDoc.entityName,
                                    entityId: theAggregateDoc.entityId,
                                }
                            })
                            // }

                        }));
                        const message = `Documents Updated! Collection ${collection}`;
                        console.log(message);
                        return {
                            status: 200,
                            message,
                            updateResult
                        };
                    }
                    else {
                        const message = `No documents found for Collection ${collection}!`;
                        console.log(message);
                        return { message };
                    }
                })
                .catch(error => {
                    const errorMessage = `Error getting document for Collection ${collection}!`;
                    console.log('Error getting document:', error);
                    return errorMessage;
                });
            promises.push(promise);
        });

    return await Promise.all(promises)
        .then(() => {
            const mse = { updatedCount: promises.length, error: undefined };
            return mse;
        })
        .catch(err => {
            const mse = { updatedCount: promises.length, error: err };
            console.log(mse);
            throw err;
        });
};

function addDocumentReferences(collectionKeys: string[], enumObject, collections: any[]) {

    collectionKeys.forEach(collection => {
        collections.push(enumObject[collection]);
    });
}

function getKeys(object) {
    return Object.keys(object).filter(k => typeof object[k as any] === 'string');
}