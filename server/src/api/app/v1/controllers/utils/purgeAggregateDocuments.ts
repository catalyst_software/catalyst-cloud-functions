import { firestore } from 'firebase-admin';
import * as functions from 'firebase-functions'
import firebase_tools from 'firebase-tools'

import { db } from '../../../../../shared/init/initialise-firebase';

import { Athlete } from '../../../../../models/athlete/interfaces/athlete';
import { Group } from './../../../../../models/group/group.model';

import { AthleteIndicator } from '../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { GroupIndicator } from '../../../../../db/biometricEntries/well/interfaces/group-indicator';

import { WellAggregateCollection } from '../../../../../db/biometricEntries/enums/firestore-aggregate-collection';
import { WellAggregateHistoryCollection } from '../../../../../db/biometricEntries/enums/firestore-aggregate-history-collection';

import { IpScoreAggregateCollection } from '../../../../../db/ipscore/enums/firestore-ipscore-aggregate-collection';
import { IpScoreAggregateHistoryCollection } from '../../../../../db/ipscore/enums/firestore-ipscore-aggregate-history-collection';

import { isProductionProject } from '../../is-production';

import { getAthleteDocRef, getDocFromRef, getGroupDocRef, getOrgDocRef, getGroupDocFromSnapshot } from '../../../../../db/refs';

import { removeUndefinedProps } from '../../../../../db/ipscore/services/save-aggregates';
import { WellAggregateBinaryHistoryCollection } from '../../../../../db/biometricEntries/enums/firestore-aggregate-binary-history-collection';
import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections';
import { getDocumentFromSnapshot } from '../../../../../analytics/triggers/utils/get-document-from-snapshot';

const aggregateCollectionKeys = getKeys(WellAggregateCollection);
const aggregateHistoryCollectionKeys = getKeys(WellAggregateHistoryCollection);
const ipScoreAggregateCollectionKeys = getKeys(IpScoreAggregateCollection);
const ipScoreAggregateHistoryCollectionKeys = getKeys(IpScoreAggregateHistoryCollection);
const wellAggregateBinaryHistoryCollectionKeys = getKeys(WellAggregateBinaryHistoryCollection);

export const purgeAthleteAggregateDocuments = async (entityId: string) => {

    const collections = [];

    addDocumentReferences(aggregateCollectionKeys, WellAggregateCollection, collections);
    addDocumentReferences(aggregateHistoryCollectionKeys, WellAggregateHistoryCollection, collections);
    addDocumentReferences(ipScoreAggregateCollectionKeys, IpScoreAggregateCollection, collections);
    addDocumentReferences(ipScoreAggregateHistoryCollectionKeys, IpScoreAggregateHistoryCollection, collections);
    addDocumentReferences(wellAggregateBinaryHistoryCollectionKeys, WellAggregateBinaryHistoryCollection, collections);

    const promises = [];

    collections.map(async (collection) => {
        const docRef = db
            .collection(collection)
            .where('entityId', '==', entityId);
        const promise = docRef
            .get()
            .then(async (docsQuerySnap: firestore.QuerySnapshot) => {
                if (!docsQuerySnap.empty) {

                    const deleteResult = await Promise.all(docsQuerySnap.docs.map(async doc => {
                        return await doc.ref.delete().then(() => {
                            return {
                                uid: doc.id,
                                path: doc.ref.path
                            }
                        });
                    }));
                    const message = `Documents Deleted for entityUid ${entityId}! Collection ${collection}`;
                    console.log(message);
                    return {
                        message, deleteResult
                    };
                }
                else {
                    // doc.data() will be undefined in this case
                    const message = `No documents found for entityUid ${entityId}! Collection ${collection}`;
                    console.log(message);
                    return { message };
                }
            })
            .catch(error => {
                const errorMessage = `Error getting document for entityUid ${entityId}! Collection ${collection}`;
                console.log('Error getting document:', error);
                return errorMessage;
            });
        promises.push(promise);
    });

    return await Promise.all(promises)
        .then(() => {
            const mse = { deletedCount: promises.length, error: undefined };
            return mse;
        })
        .catch(err => {
            const mse = { deletedCount: promises.length, error: err };
            console.log(mse);
            throw err;
        });
};

export const purgeGroupAggregateDocuments = async (entityId: string) => {

    const collections = [];

    addDocumentReferences(aggregateCollectionKeys, WellAggregateCollection, collections);
    addDocumentReferences(aggregateHistoryCollectionKeys, WellAggregateHistoryCollection, collections);
    addDocumentReferences(ipScoreAggregateCollectionKeys, IpScoreAggregateCollection, collections);
    addDocumentReferences(ipScoreAggregateHistoryCollectionKeys, IpScoreAggregateHistoryCollection, collections);
    addDocumentReferences(wellAggregateBinaryHistoryCollectionKeys, WellAggregateBinaryHistoryCollection, collections);

    const promises = [];

    collections.map(async (collection) => {
        const docRef = db
            .collection(collection)
            .where('entityId', '==', entityId);
        const promise = docRef
            .get()
            .then(async (docsQuerySnap: firestore.QuerySnapshot) => {
                if (!docsQuerySnap.empty) {

                    const deleteResult = await Promise.all(docsQuerySnap.docs.map(async doc => {
                        return await doc.ref.delete().then(() => {
                            return {
                                uid: doc.id,
                                path: doc.ref.path
                            }
                        });
                    }));
                    const message = `Documents Deleted for entityUid ${entityId}! Collection ${collection}`;
                    console.log(message);
                    return {
                        message, deleteResult
                    };
                }
                else {
                    // doc.data() will be undefined in this case
                    const message = `No documents found for entityUid ${entityId}! Collection ${collection}`;
                    console.log(message);
                    return { message };
                }
            })
            .catch(error => {
                const errorMessage = `Error getting document for entityUid ${entityId}! Collection ${collection}`;
                console.log('Error getting document:', error);
                return errorMessage;
            });
        promises.push(promise);
    });

    return await Promise.all(promises)
        .then(() => {
            const mse = { deletedCount: promises.length, error: undefined };
            return mse;
        })
        .catch(err => {
            const mse = { deletedCount: promises.length, error: err };
            console.log(mse);
            throw err;
        });
};




function addDocumentReferences(collectionKeys: string[], enumObject, collections: any[]) {

    collectionKeys.forEach(collection => {
        collections.push(enumObject[collection]);
    });
}

function getKeys(object) {
    return Object.keys(object).filter(k => typeof object[k as any] === 'string');
}


export const purgeAllIPScoreAggregateDocuments = async (forceOnProd = false) => {

    const collections = [];

    if (!isProductionProject() || forceOnProd) {

        addDocumentReferences(ipScoreAggregateCollectionKeys, IpScoreAggregateCollection, collections);
        addDocumentReferences(ipScoreAggregateHistoryCollectionKeys, IpScoreAggregateHistoryCollection, collections);

        const promises = [];

        collections.map(async (collection) => {
            const docRef = db
                .collection(collection)
            // .where('entityId', '==', entityId);
            const promise = docRef
                .get()
                .then(async (docsQuerySnap: firestore.QuerySnapshot) => {
                    if (!docsQuerySnap.empty) {
                        const deletePromises = [];
                        docsQuerySnap.docs.forEach(doc => {
                            deletePromises.push(doc.ref.delete());
                        });
                        return await Promise.all(deletePromises);
                    }
                    else {
                        // doc.data() will be undefined in this case
                        const message = `No documents found!`;
                        console.log(message);
                        return message;
                    }
                })
                .catch(error => {
                    const errorMessage = `Error getting documents!`;
                    console.log('Error getting document:', error);
                    return errorMessage;
                });
            promises.push(promise);
        });

        return await Promise.all(promises)
            .then(() => {
                const mse = { deletedCount: promises.length, error: undefined };
                return mse;
            })
            .catch(err => {
                const mse = { deletedCount: promises.length, error: err };
                console.log(mse);
                throw err;
            });

    } else {
        const mse = { deletedCount: 0, error: undefined };
        return mse;

    }
};

export const cleanUpAthleteRelatedDocuments = async (athleteUID: string) => {


    let deleteOrg = false
    const athleteRef = getAthleteDocRef(athleteUID)

    const athlete = await getDocFromRef(athleteRef) as Athlete

    if (athlete) {
        const removedIndicatorsResult = await Promise.all((athlete.groups || []).map(async (groupIndicator: GroupIndicator) => {

            const allOrgGroups = await db.collection(FirestoreCollection.Groups).where('organizattionId', '==', groupIndicator.organizationId).get()
                .then((querySnap) => {
                    return querySnap.docs.map((groupDocSnap) => {
                        return getDocumentFromSnapshot(groupDocSnap) as Group
                    })
                })
            const groupDocRef = getGroupDocRef(groupIndicator.groupId)

            const group: Group = await getDocFromRef(groupDocRef) as Group
            const athleteIndicator: AthleteIndicator = group.athletes.find((ath) => ath.uid === athleteUID)

            if (athleteIndicator) {
                const removeResult = await groupDocRef.update({
                    athletes: firestore.FieldValue.arrayRemove(athleteIndicator)
                }).then(() => {
                    return {
                        message: 'Athlete Indicator Removed grom group',
                        athleteIndicator,
                        group: {
                            uid: group.groupId,
                            name: group.groupName
                        },
                        err: undefined
                    }
                }).catch((err) => {
                    return {
                        message: 'ERROR removing Athlete Indicator grom group',
                        athleteIndicator,
                        group: {
                            uid: group.groupId,
                            name: group.groupName
                        },
                        err
                    }
                })
                if (removeResult.err) {
                    removeUndefinedProps(removeResult)
                    console.warn(removeResult)
                } else {
                    removeUndefinedProps(removeResult)
                    console.log(removeResult)
                }

                return removeResult
            } else {

                if (group.athletes.length === 0) {
                    if (!isProductionProject()) {
//
                        // console.log('DELETING GROUP', group.uid)

                        // await groupDocRef.delete()

                        // await purgeGroupAggregateDocuments(groupIndicator.groupId)

                        // deleteOrg = athlete.profile.onboardingCode.toUpperCase() === 'INSPIRE' && allOrgGroups.filter((g) => g.athletes.length > 0).length > 0

                    }
                }
                return {
                    message: 'ERROR Athlete Indicator not found on group',
                    athleteIndicator,
                    group: {
                        uid: group.groupId,
                        name: group.groupName
                    },
                    err: undefined
                }

            }

        }))
//
        // if (athlete.profile.onboardingCode.toUpperCase() === 'INSPIRE' && deleteOrg) {

        //     const orgId = athlete.organizations ? athlete.organizations[0].organizationId : athlete.organizationId || undefined
        //     const orgName = athlete.organizations ? athlete.organizations[0].organizationName : 'UNKNOWN'

        //     if (orgId) {

        //         await db.collection('orgsToDelete').doc(orgId).set({
        //             organizationId: orgId,
        //             organizationName: orgName,
        //             groups: athlete.groups || [],
        //             athleteHasGroups: athlete.groups && athlete.groups.length
        //         })
        //         if (deleteOrg) {
        //             const orgDocRef = getOrgDocRef(orgId)
        //             console.log('DELETING ORG', orgId)
        //             await orgDocRef.delete()
        //         }
        //         // else {
        //         //     await db.collection('orgsToDelete').doc(orgId).set({
        //         //         organizationId: orgId,
        //         //         organizationName: orgName,
        //         //         groups: athlete.groups || [],
        //         //         athleteHasGroups: athlete.groups && athlete.groups.length
        //         //     })
        //         // }

        //     }

        // }


        if (!isProductionProject()) {

            const orgId = athlete.organizations ? athlete.organizations[0].organizationId : athlete.organizationId || undefined

            if (orgId) {
                const groupDocRef = getOrgDocRef(orgId)


                await groupDocRef.delete()

            }

            const path = `athletes/${athleteUID}`;

            const project =
                //  process.env.GCLOUD_PROJECT || 
                'inspire-1540046634666'

            console.log('>>> functions.config()', functions.config())
            console.log('Purging Athlete Document and sub collections')
            try {
                const purgeResult = firebase_tools.firestore
                    .delete(path, {
                        project: project,
                        recursive: true,
                        yes: true,
                        // token: '1/_FbrZb-uLNo0UIOFNpfdDmuzQxFsKWuML55DV1zI5Fk' // functions.config().fb.token
                        // token: functions.config().fb.token
                        token: '1//03Gk1YhtZ6SbJCgYIARAAGAMSNwF-L9IrUnJwE8jse8eSxgrI-GSsqv41QbnjKVKHySsGFY_F6MSYXAKQzuO7gHuhtOXMXgnHGT4' // functions.config().fb.token
                    })
                    .then(() => {
                        return {
                            message: 'Athlete Document and sub collections purged and Athlete Indicator removed from related groups',
                            path: path,
                            athlete,
                            removedIndicatorsResult,
                            err: undefined
                        };
                    }).catch((err) => {
                        return {
                            message: 'ERROR Retrieving Athlete Document and sub collections, Athlete Indicator removed from related groups',
                            path: path,
                            athlete,
                            removedIndicatorsResult,
                            err
                        };
                    });

                return await purgeResult;


            } catch (error) {
                console.error(error)

                throw error
            }


        } else {
            const mse = { deletedCount: 0, error: undefined };
            return mse;

        }
    } else {

        console.error({
            message: 'ERROR  Athlete Document does not excist, sub collections are stale',

            athlete,

            err: 'Athlete Doc Does Not Exist, clear out sub collections'
        })
        return {
            message: 'ERROR  Athlete Document does not excist, sub collections are stale',

            athlete,

            err: 'Athlete Doc Does Not Exist, clear out sub collections'
        };
    }

};
