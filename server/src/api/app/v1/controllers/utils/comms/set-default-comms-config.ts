import { FirestoreCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';
import { Athlete } from '../../../../../../models/athlete/athlete.model';
import { Group } from '../../../../../../models/group/interfaces/group';
import { GroupIndicator } from '../../../../../../db/biometricEntries/well/interfaces/group-indicator';
import { getCoachConfigDocName } from '../../../callables/utils/get-coach-config-doc-name';
import { NotificationsConfig } from '../../athletes';
import { setCurrentGroupConfig } from "../../../callables/utils/comms/set-current-group-config";
import { getCurrentConfig } from "./get-current-config";
import { getOrgDocRef, getGroupDocRef, getAthleteCollectionRef } from '../../../../../../db/refs';

export const setDefaulCommsConfig = async (orgId: string, notificationIds?: Array<string>, athleteUID?: string, includeAllAthleteDefaults?: boolean) => {

    const allAthletes = [];
    const promises = [];

    const query = getAthleteCollectionRef()
        // .where(
        //     'metadata.creationTimestamp',
        //     '>=',
        //     start
        // )
        // .where('organizationId', '==', '0nMq6KEtonuKqzysz0ky')
        .where('organizationId', '==', orgId);


    // TODO: Check Promises
    promises.push(await query.get().then(docs => {
        if (docs.size) {
            if (allAthletes.length === 0) {
                docs.docs.map(athleteDocSnap => {
                    // this.updateAthlete(req, res)
                    const ath = athleteDocSnap.data() as Athlete;
                    // allAthletes.push({
                    //     uid: ddd.id,
                    //     profile: ath.profile,
                    //     creationTimestamp: ath.metadata.creationTimestamp.toDate(),
                    // })

                    // Only update requested Athlete
                    if (athleteUID) {
                        if (athleteUID === athleteDocSnap.id) {
                            allAthletes.push({
                                uid: athleteDocSnap.id,
                                ...ath
                            });
                        }
                    } else {
                        allAthletes.push({
                            uid: athleteDocSnap.id,
                            ...ath
                        });
                    }
                });
            }
        }
        return docs
    }));

    await Promise.all(promises);

    await allAthletes.map((athlete: Athlete) => {
        athlete.groups.map(async (groupIndicator: GroupIndicator) => {
            const theGroup = await getGroupDocRef(groupIndicator.uid || groupIndicator.groupId)
                .get().then((docSnap) => {
                    return {
                        uid: docSnap.id,
                        ...(docSnap.data() as Group)
                    };
                });
            if (!theGroup.athletes) {
                theGroup.athletes = [];
            }
            const groupCoaches = theGroup.athletes.filter((currAthlete) => currAthlete.isCoach);
            if (groupCoaches.length) {
                groupCoaches.map(async (groupCoach) => {
                    const theCoach = (allAthletes.find((ath_1) => ath_1.uid === groupCoach.uid) as Athlete);
                    if (theCoach) {
                        const athletesCommsConfig = theGroup.athletes.filter((groupAthlete) => groupAthlete.uid !== theCoach.uid
                            && !groupAthlete.isCoach).map((nonCoach) => {
                                return {
                                    uid: nonCoach.uid,
                                    danger: {
                                        sendEmail: true,
                                        sendPushNotification: true,
                                        sendSMS: false
                                    },
                                    warn: {
                                        sendEmail: true,
                                        sendPushNotification: true,
                                        sendSMS: false
                                    },
                                    success: {
                                        sendEmail: true,
                                        sendPushNotification: true,
                                        sendSMS: false
                                    },
                                };
                            });
                        //TODO:!! Check athletes here, is it not config?!
                        if (athletesCommsConfig && athletesCommsConfig.length) { // && includeAllAthleteDefaults
                            const { firstName, lastName } = theCoach.profile;
                            const configDocName = getCoachConfigDocName(firstName, lastName, theCoach.uid);
                            const configRef = getOrgDocRef(theGroup.organizationId)
                                .collection(FirestoreCollection.CoachEssentialCommsConfiguration)
                                .doc(configDocName);


                            const firebaseMessagingIDs = theCoach.firebaseMessagingIds || theCoach.metadata.firebaseMessagingIds || [theCoach.metadata.firebaseMessagingId]

                            const currentConfig = await getCurrentConfig(configRef);
                            // const groupConfig = getCurrentGroupConfig(currentConfig, theGroup, athletesCommsConfig);
                            const groupConfig = setCurrentGroupConfig(configRef, { ...currentConfig, firebaseMessagingIDs }, theGroup);

                            let essentailCommsConfig: NotificationsConfig = {
                                uid: configDocName,
                                firebaseMessagingIDs,
                                groups: groupConfig,
                                excludedNotificationTypes: []
                            };
                            if (currentConfig) {
                                essentailCommsConfig = { ...essentailCommsConfig, ...currentConfig, firebaseMessagingIDs };
                            }
                            await configRef.set({ ...essentailCommsConfig }).then(() => {
                                console.log(`Essential Comms Config Saved for ${theCoach.name}. UID: {${theCoach.uid}}`, `${theGroup.organizationId}_${FirestoreCollection.CoachEssentialCommsConfiguration}_${configDocName}`);
                            });
                        }
                        else {
                            console.warn('No Athletes found for group', theGroup);
                        }
                    }
                    else {
                        console.warn('Coach Object Not Found In Athletes Documents Collection', groupCoach);
                    }
                });
            }
            else {
                console.warn('No coaches Found for group', theGroup);
            }
        });
    });
    return allAthletes;
};
