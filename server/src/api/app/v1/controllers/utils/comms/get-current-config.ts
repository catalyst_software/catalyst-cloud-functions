// import firestore = require('@google-cloud/firestore');
import { firestore } from 'firebase-admin';

import { NotificationsConfig } from '../../athletes';

export const getCurrentConfig = async (configRef: firestore.DocumentReference) => {

    return await configRef.get().then((docSnap) => {
        if (docSnap.exists) {
            return {
                uid: docSnap.id,
                ...docSnap.data() as NotificationsConfig
            };
        }
        else {
            return undefined;
        }
    });
    
};
