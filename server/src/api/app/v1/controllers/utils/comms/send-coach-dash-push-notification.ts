import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { LogInfo } from "../../../../../../shared/logger/logger";
import { AnalyticsPushNotificationType } from '../../../../../../models/enums/analytics-push-notification-type';
import { KeyValuePair } from '../../../../../../models/notifications/interfaces/schedule-notification-payload';
import { PushNotificationType } from '../../../../../../models/enums/push-notification-type';
import { sendNotification } from '../send-message';
import { PushNotificationsType } from '../../../../../../models/enums/enums.model';
import { isArray } from 'lodash';

export const sendCoachDashPushNotification = async (
    recipients: AthleteIndicator[],
    keyValuePairs: KeyValuePair[],
    type: AnalyticsPushNotificationType | PushNotificationType,
    notificationPath: string,
    icon: PushNotificationsType,
    useTopic: boolean,
    topicIds: string[],
    appIdentidier?: string
): Promise<boolean[]> => {
    if (useTopic) {
        if (topicIds) {
            try {

                const additionalKeyValuePairs: KeyValuePair[] = [
                    // {
                    //     key: '$ATHLETE_UID',
                    //     value: recipient.uid
                    // },
                    // {
                    //     key: '$FIRST_NAME',
                    //     value: recipient.firstName
                    // },
                    // {
                    //     key: '$LAST_NAME',
                    //     value: recipient.lastName
                    // }
                ];


                const mergedKeyValuePairs = keyValuePairs.concat(additionalKeyValuePairs || [])

                console.log('mergedKeyValuePairs', mergedKeyValuePairs)
                // TODO: 
                const isRedFlags = true;
                const sendResult = await sendNotification(isRedFlags, type, notificationPath,
                    mergedKeyValuePairs, undefined, icon,
                    useTopic,
                    topicIds,
                    'newContentFeed',
                    appIdentidier).then(response => {
                        if (response) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }).catch((ex) => {
                        console.warn('Error Occured trying to send notifications', ex)
                        return false
                    });

                return [sendResult];
            }
            catch (error) {
                console.error('An error occured while trying trying to send a notification', {
                    error,
                    topicIds,
                    type,
                    keyValuePairs
                });
                return Promise.resolve([true]);
            }
        }
        else {
            LogInfo(`The token is empty.... Message not sent for topicIds ${topicIds}`);
            return Promise.resolve([false]);
        }
    } else {

        return [].concat(... await Promise.all(recipients.map(async (recipient) => {
            console.log('recipient')
            console.log(JSON.stringify(recipient))
            if (recipient.firebaseMessagingIds && isArray(recipient.firebaseMessagingIds)) {
                const allResults = await Promise.all(recipient.firebaseMessagingIds.map(async (firebaseMessagingId) => {
                    const token = firebaseMessagingId;
                    if (token) {
                        try {

                            const additionalKeyValuePairs: KeyValuePair[] = [
                                {
                                    key: '$ATHLETE_UID',
                                    value: recipient.uid
                                },
                                {
                                    key: '$FIRST_NAME',
                                    value: recipient.firstName
                                },
                                {
                                    key: '$LAST_NAME',
                                    value: recipient.lastName
                                }];


                            const mergedKeyValuePairs = keyValuePairs.concat(additionalKeyValuePairs || [])

                            console.log('mergedKeyValuePairs', mergedKeyValuePairs)
                            // TODO: 
                            const isRedFlags = true;
                            const sendResult = await sendNotification(isRedFlags, type, notificationPath,
                                mergedKeyValuePairs, token, icon,
                                useTopic,
                                topicIds,
                                'newContentFeed',
                                appIdentidier).then(response => {
                                    if (response) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }).catch((ex) => {
                                    console.warn('Error Occured trying to send notifications', ex)
                                    return false
                                });

                            return sendResult;
                        }
                        catch (error) {
                            console.error('An error occured while trying trying to send a notification', {
                                error,
                                token,
                                type,
                                keyValuePairs
                            });
                            return true;
                        }
                    }
                    else {
                        LogInfo(`The token is empty.... Message not sent for Athlete with UID ${recipient.uid}`);
                        return false;
                    }
                }));

                return allResults
            }
            else {
                console.warn(`athlete.firebaseMessagingIds is not an array..... Message not sent for Athlete with UID ${recipient.uid}`);
                console.info(`athlete.firebaseMessagingIds`, recipient.firebaseMessagingIds);
                return [false];
            }
        }))) as boolean[]
    }

};
