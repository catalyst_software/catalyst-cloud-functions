import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { LogInfo } from "../../../../../../shared/logger/logger";
import { AnalyticsPushNotificationType } from '../../../../../../models/enums/analytics-push-notification-type';
import { KeyValuePair } from '../../../../../../models/notifications/interfaces/schedule-notification-payload';
import { PushNotificationType } from '../../../../../../models/enums/push-notification-type';
import { sendNotification } from '../send-message';
import { isArray } from 'lodash';

export const sendPushNotification =
    async (isRedFlags: boolean,
        coach: AthleteIndicator,
        type: AnalyticsPushNotificationType | PushNotificationType,
        emailTemplateName?: string,
        path?: string,
        fullname?: string,
        firstName?: string,
        lastName?: string,
        additionalKeyValuePairs?: KeyValuePair[],
        appIdentifier?: string
    ) => {
        const keyValuePairs: KeyValuePair[] = [
            {
                key: '$NAME',
                value: fullname ? fullname : `${coach.firstName} ${coach.lastName}`
            },
            {
                key: '$FIRST_NAME',
                value: firstName ? firstName : coach.firstName
            },
            {
                key: '$LAST_NAME',
                value: lastName ? lastName : ''
            }
        ];
        if (coach.firebaseMessagingIds && isArray(coach.firebaseMessagingIds)) {
            const allResults = await Promise.all(coach.firebaseMessagingIds.map(async (firebaseMessagingId) => {
                const token = firebaseMessagingId;
                if (token) {
                    try {

                        const mergedKeyValuePairs = keyValuePairs.concat(additionalKeyValuePairs || [])
                        console.log('mergedKeyValuePairs', mergedKeyValuePairs)
                        const sendResult = await sendNotification(isRedFlags, type, path, mergedKeyValuePairs, token, undefined, false, undefined, emailTemplateName, appIdentifier).then(response => {
                            if (response) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        }).catch((ex) => {
                            console.warn('Error Occured trying to send notifications', ex)
                            return false
                        });

                        return sendResult;
                    }
                    catch (error) {
                        console.error('An error occured while trying trying to send a notification', {
                            error,
                            token,
                            type,
                            keyValuePairs
                        });
                        return true;
                    }
                }
                else {
                    LogInfo(`The token is empty.... Message not sent for Athlete with UID ${coach.uid}`);
                    return false;
                }
            }));

            return allResults
        }
        else {
            console.warn(`athlete.firebaseMessagingIds is not an array..... Message not sent for Athlete with UID ${coach.uid}`);
            console.info(`athlete.firebaseMessagingIds`, coach.firebaseMessagingIds);
            return [false];
        }
    };
