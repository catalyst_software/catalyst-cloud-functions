import moment from 'moment';

import { Athlete } from '../../../../../models/athlete/athlete.model'
import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections'
import { db } from '../../../../../shared/init/initialise-firebase'
import {
    SubscriptionType,
    SubscriptionStatus,
    SubscriptionPaymentType,
} from '../../../../../models/enums/enums.model'
import { AthleteSubscription } from '../../../../../models/athlete/interfaces/athlete-subscription'
import { firestore } from 'firebase-admin';

import { getDocumentFromSnapshot } from '../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { updateExistingAthleteForPurchasing } from '../../callables/utils/validate-transaction-receipt/updateExistingAthleteForPurchasing';
import { updateAthleteForPurchasing } from '../../callables/utils/validate-transaction-receipt/updateAthleteForPurchasing';
import { AndroidReceipt } from '../../../../../models/purchase/interfaces/android-receipt';
import { IOSReceipt } from '../../../../../models/purchase/interfaces/ios-receipt';
import { getAthleteSub } from './getAthleteSub';

const updateSubcription = (athleteUid: string, subscription: AthleteSubscription, expiryDate: any,
    status: SubscriptionStatus, type: SubscriptionType, subscriptionPaymentType: SubscriptionPaymentType,
    actionAnalytics: any, productIdentifier: string): AthleteSubscription => {

    let theSubscription = subscription
    theSubscription.paymentType = subscriptionPaymentType
    let commencementDateOnAthleteSubscription: firestore.Timestamp = undefined
    let expirationDateOnAthleteSubscription: firestore.Timestamp = undefined
    let passedExpirationDate: firestore.Timestamp = undefined;


    ({ commencementDateOnAthleteSubscription, expirationDateOnAthleteSubscription, passedExpirationDate } = sortDates(theSubscription, commencementDateOnAthleteSubscription, expirationDateOnAthleteSubscription, expiryDate, passedExpirationDate));


    let theStatus

    const theType = type !== undefined ? type : theSubscription.type
    let theExpiryDate: firestore.Timestamp = undefined

    let isPrePaid

    if (status === SubscriptionStatus.CanceledForceLogout) {
        theStatus = SubscriptionStatus.CanceledForceLogout // theSubscription.status
        theExpiryDate = passedExpirationDate || expirationDateOnAthleteSubscription
        isPrePaid = false
    } else {
        theStatus = status === SubscriptionStatus.TrailPeriod ? SubscriptionStatus.ValidInitial : status
        theExpiryDate = passedExpirationDate
        isPrePaid = theSubscription.isPrePaid
    }


    let commencementDate = commencementDateOnAthleteSubscription
    if (status === SubscriptionStatus.ValidRenewed) {
        theStatus = SubscriptionStatus.ValidRenewed // theSubscription.status
        // commencementDate = firestore.Timestamp.now()

    } else {
        if (!theSubscription.commencementDate) {
            console.warn(`theSubscription.commencementDate`, theSubscription.commencementDate)

        } else {
            commencementDate = theSubscription.commencementDate || firestore.Timestamp.now()
        }
    }

    if (status === undefined) {
        console.log(`not updating type/status ${athleteUid}`)
        console.log(`type ${theType}`)
        console.log(`status ${theStatus}`)
        return undefined// subscription // Just add TransactionAuditRecord
    } else {

        console.log(`updating type/status ${athleteUid}`)
        console.warn(`theSubscription`, theSubscription)
        // console.warn(`athlete.currentIpScoreTracking.directive.execute`, athlete.currentIpScoreTracking.directive.execute)

        console.log(`type ${theType}`)
        console.log(`status ${theStatus}`)

        theSubscription = {
            ...theSubscription,
            status: theStatus,
            type: theType,
            expirationDate: theExpiryDate || firestore.Timestamp.fromDate(moment().toDate()),
            commencementDate,
            isPrePaid
        } as AthleteSubscription

        console.warn(`UPDATED theSubscription`, theSubscription)

        if (actionAnalytics) {

            const analyticsData = {
                pending: true,
                action: actionAnalytics,
                payload: {
                    productIdentifier
                }
            }

            console.log(`Setting Analytics Data ${theSubscription.analytics}`)
            theSubscription.analytics = analyticsData

            console.log(`Setting Analytics Data NOW`, theSubscription.analytics)
        }

        return theSubscription

    }

}

export const updateAthleteSubscriptionStatus = async (
    athlete: Athlete,
    parsedReceipt: AndroidReceipt | IOSReceipt,
    expiryDate: Date,
    status: SubscriptionStatus,
    type: SubscriptionType,
    actionAnalytics: string,
    productIdentifier: string,
    appIdentifier: string,
    originalTransactionId: string,
    save = true,
    notificationType?: string
): Promise<boolean> => {


    console.log(`querying athlete with uid ${athlete.uid}`, athlete)
    const athleteRef = db
        .collection(FirestoreCollection.Athletes)
        .doc(athlete.uid)

    console.log(`productIdentifier`, productIdentifier)
    console.log(`appIdentifier`, appIdentifier)

    if (!parsedReceipt.productId) {
        parsedReceipt.productId = productIdentifier || 'NOT SET'
    }

    console.log(
        `creating doc in ${FirestoreCollection.TransactionAuditRecord}`,
        parsedReceipt
    )

    // console.log('checking appSubscriptions')
    // const subIndex = (athlete.profile.appSubscriptions || []).findIndex((s) => s.appIdentifier === appIdentifier)
    let theSubscription

    const theAth = getDocumentFromSnapshot(await athleteRef.get()) as Athlete
    // const sub = athlete.profile.appSubscriptions[subIndex]
    let sub: AthleteSubscription = getAthleteSub(theAth, appIdentifier);

    const paymentType: SubscriptionPaymentType = sub.paymentType;

    theSubscription = updateSubcription(athlete.uid, sub, expiryDate, status, type, paymentType, actionAnalytics, productIdentifier)

    console.log('updating appSubscriptions arryaUnion', theSubscription)

    if (theSubscription && save) {

        let updateInitialBuy = ((notificationType && notificationType === 'INITIAL_BUY') && sub.expirationDate !== null && !moment(sub.expirationDate).isSame(theSubscription.expirationDate))

        if (updateInitialBuy || notificationType !== 'INITIAL_BUY') {

            const theUpdatedAthlete = updateExistingAthleteForPurchasing(
                theAth, theSubscription.type, theSubscription.status, theSubscription.paymentType,
                theSubscription.commencementDate,
                // TODO
                theSubscription.expirationDate, //TODO
                theSubscription.appIdentifier, theSubscription.productIdentifier, 'serverNotifications')
            return updateAthleteForPurchasing(theUpdatedAthlete, originalTransactionId) //transactionId)
                .then(() => {
                    return true
                })
                .catch(err => {
                    return err
                })
        } else {
            return true
        }

    } else {
        return true
    }
}

function sortDates(theSub: AthleteSubscription, commencementDateOnAthleteSub: firestore.Timestamp, expirationDateOnAthleteSub: firestore.Timestamp, expiryDate: any,
    passedExpiration: firestore.Timestamp) {

    let theSubscription = theSub

    let commencementDateOnAthleteSubscription = commencementDateOnAthleteSub
    let expirationDateOnAthleteSubscription = expirationDateOnAthleteSub

    let passedExpirationDate = passedExpiration

    if (typeof theSubscription.commencementDate === 'string') {
        commencementDateOnAthleteSubscription = firestore.Timestamp.fromDate(theSubscription.commencementDate ? new Date(theSubscription.commencementDate) : new Date());
        theSubscription.commencementDate = commencementDateOnAthleteSubscription;
    }
    else {
        commencementDateOnAthleteSubscription = theSubscription.commencementDate || firestore.Timestamp.fromDate(new Date());
    }
    if (typeof theSubscription.expirationDate === 'string') {
        expirationDateOnAthleteSubscription = firestore.Timestamp.fromDate(new Date(theSubscription.expirationDate));
        theSubscription.expirationDate = expirationDateOnAthleteSubscription;
    }
    else {
        expirationDateOnAthleteSubscription = theSubscription.expirationDate;
    }
    if (typeof expiryDate === 'string') {
        passedExpirationDate = firestore.Timestamp.fromDate(new Date(expiryDate));
    }
    else {
        passedExpirationDate = firestore.Timestamp.fromDate(expiryDate);
    }
    return { commencementDateOnAthleteSubscription, expirationDateOnAthleteSubscription, passedExpirationDate };
}

