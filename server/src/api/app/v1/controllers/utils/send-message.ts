import { getSharedSystemConfiguration } from './../../callables/utils/warehouse/olap/get-olap-jwt-package';
import * as fAdmin from 'firebase-admin';
import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections';
import { db, theMainApp, theSecondaryApp } from '../../../../../shared/init/initialise-firebase';

import { AnalyticsPushNotificationType } from '../../../../../models/enums/analytics-push-notification-type';

import {
    NotificationTemplate,
    KeyValuePair
} from '../../../../../models/notifications/interfaces/schedule-notification-payload'
import { replaceParts } from './send-notification';
import { PushNotificationType } from '../../../../../models/enums/push-notification-type';
import { PushNotificationsType } from '../../../../../models/enums/enums.model';
import { removeUndefinedProps } from '../../../../../db/ipscore/services/save-aggregates';

const getMessagingApp = async (appIdentifier?: string): Promise<fAdmin.app.App> => {
    const config = await getSharedSystemConfiguration();
    let app = theMainApp;
    if (appIdentifier && config.secondaryProjectIdentifiers) {
        const secondaries = config.secondaryProjectIdentifiers['inspire-secondary-1'];
        if (secondaries[appIdentifier]) {
            console.log(`AppIdentifier ${appIdentifier} is configured as secondary project.`);
            app = theSecondaryApp;
        }
    }

    return app;
}

// TB: THIS IS BEING USED
export const sendNotification = async (
    isRedFlags: boolean,
    type: AnalyticsPushNotificationType | PushNotificationType,
    path: string,
    keyValuePairs: KeyValuePair[],
    token: string,
    icon: PushNotificationsType,
    useTopic: boolean,
    topicIds: string[] = ['news'],
    emailTemplateName?: string,
    appIdentifier?: string
): Promise<{
    message: any
    response?: any
}> => {
    console.log(`emailTemplateName: ${emailTemplateName}`);
    const notificationTemplate = await db
            .collection(FirestoreCollection.NotificationTemplates)
            .doc(emailTemplateName)
            .get()
            .then((d) => {
                console.log(`Template document retrieved: ${JSON.stringify(d)}`);
                return d.data() as NotificationTemplate;
            })
            .catch((err) => {
                console.error(`Failed to retrieve template document.  Error: ${JSON.stringify(err)}`);
                return undefined;
            });

    if (notificationTemplate) {
        console.log(`Attempting to send notification using template document ${JSON.stringify(notificationTemplate)}`);
        const notification = replaceParts(
            notificationTemplate,
            keyValuePairs
        );

        const { message, ...rest } = notification;

        if (icon) {
            message.data.icon = icon;
        }
        if (path) {
            message.data.path = path;
        }

        const athleteUid = keyValuePairs.find(o => o.key === '$ATHLETE_UID');
        if (!useTopic && athleteUid) {
            message.data.athleteUid = athleteUid.value;
        }

        if (useTopic) {
            // Define a condition which will send to devices which are subscribed
            // to either the Google stock or the tech industry topics.
            // var condition = "'stock-GOOG' in topics || 'industry-tech' in topics";

            const firstTopic = topicIds.shift();
            let condition = `'${firstTopic}' in topics`;

            // topicIds.forEach((topic) => {
            //    condition += ` || '${topic}' in topics`;
            // });
            console.log(`Sending cloud message to topic: ${condition}`);
            message.condition = condition;
        } else {
            console.log(`Attempting to send push notification to device: ${token}`);
            message.token = token;
        }
        removeUndefinedProps(message);
        const theApp = await getMessagingApp(appIdentifier);
        const response = await theApp.messaging().send({
            ...message,
            // token,
            // fcmOptions: {
            //     analyticsLabel: 'new-content-feed-item-notification'
            // }
        }).catch((err) => {
            console.warn('ERROR TRYING TO SEND NOTIFICATION', err)

            return { message: notification, err }
        });

        // Response is a message ID string.
        console.log('Successfully sent notification message:', response, {
            message,
            ...rest,
            token,
            topicIds
        });
        return { message: notification, response };
    } else {
        console.log(`Notification template document with doc id ${emailTemplateName} not found!`);
        console.log('Unable to send message:', {
            // ...message,
            type,
            token,
            keyValuePairs,
        });
        return {
            message: `Could not load notification from then DB for type ${type}`
        };
    }
}
