
import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections'
import { getAthleteCollectionRef } from '../../../../../db/refs';
import { purgeAthleteAggregateDocuments } from './purgeAggregateDocuments';

export const setAthletesOrgID = async () => {
    // console.log(`querying athlete with uid ${athlete.uid}`, athlete)
    const athleteRef = getAthleteCollectionRef()
    // console.log(
    //     `creating doc in ${FirestoreCollection.TransactionAuditRecord}`,
    //     validationResponse
    // )
    // console.log(`athlete.uid ${athlete.uid}`)

    // let theStatus

    const updates = []
    const athleteDocsRefs = new Array<FirebaseFirestore.DocumentReference>();
    const athleteIDs = [
        '09VVNHkk6lOPyYh2dMf0pJ8TqHZ2',
        '19rmFdwLEpYBc3p7do9jo2VVQHu2',
        '1AkYDhENHQgLz6hXNOZevztvIdo2',
        '1QaoVoKxgtg0XP1gDZW1MuPgFus1',
        'HWMGgG5WagNC4h0ZiowB7ag6nii1'
    ]

    athleteIDs.forEach(async (groupUID) => {
        await purgeAthleteAggregateDocuments(groupUID)
        console.log('Adding groupUID to groupDocsRefs ', groupUID)
        console.log('FirestoreCollection ', FirestoreCollection.Groups)
        const ref = athleteRef
            .doc(groupUID);
        athleteDocsRefs.push(ref);
    })
    await athleteDocsRefs.map(async (ref) => {

        console.log('Deleting Docs for AThlete with UID', ref.id)

        await ref.collection('biometricEntries').get().then(async (docQuerySnaps) => {

            docQuerySnaps.docs.forEach(async (docQuerySnap) => {

                await docQuerySnap.ref.delete()
            })
        })

        await ref.collection('completedPrograms').get().then(async (docQuerySnaps) => {

            docQuerySnaps.docs.forEach(async (docQuerySnap) => {

                await docQuerySnap.ref.delete()
            })
        })

        await ref.collection('completedSurveys').get().then(async (docQuerySnaps) => {

            docQuerySnaps.docs.forEach(async (docQuerySnap) => {

                await docQuerySnap.ref.delete()
            })
        })

        await ref.collection('ipScoreTrackingHistory').get().then(async (docQuerySnaps) => {

            docQuerySnaps.docs.forEach(async (docQuerySnap) => {

                await docQuerySnap.ref.delete()
            })
        })

        await ref.collection('transactionAuditRecord').get().then(async (docQuerySnaps) => {

            docQuerySnaps.docs.forEach(async (docQuerySnap) => {

                await docQuerySnap.ref.delete()
            })
        })
    })




    // const athQueryDocSnaps: firestore.QuerySnapshot = await athleteRef
    //     .get()

    // // .then(async (athletesQuerySnap) => {
    // //     const ressss = athletesQuerySnap.docs.map(async (queryDocSnap) => {


    // //         const ath = getDocumentFromSnapshot(queryDocSnap) as Athlete

    // //         return ath

    // //     })
    // //     return await Promise.all(ressss)
    // // })
    // // .catch((err) => {
    // //     console.log(err)
    // //     return Promise.reject([{
    // //         updateStatus: 'Fail - Exception Thrown',
    // //         message: `Exception Thrown`,
    // //         // uid: ath.uid,
    // //         // name: ath.profile.firstName + ' ' + ath.profile.lastName
    // //     }])
    // // })





    // athQueryDocSnaps.docs.forEach(async (athQueryDocSnap: firestore.QueryDocumentSnapshot) => {

    //     const ath = getDocumentFromSnapshot(athQueryDocSnap) as Athlete
    //     let result;

    //     if (ath.metadata === undefined) {
    //         if (ath.profile === undefined) {
    //             result = {
    //                 updateStatus: 'Pass - Ath Org Is Set',
    //                 message: `======= Ath Org Is Set ${ath.profile.firstName}`,
    //                 uid: ath.uid,
    //                 name: ath.profile.firstName + ' ' + ath.profile.lastName
    //             };
    //             updates.push(result);
    //         }

    //     }
    //     // if (!ath.organizationId || ath.organizationId === '' && ath.metadata !== undefined) {
    //     //     if (isArray(ath.groups)) {

    //     //         const groupID = ath.groups[0].uid
    //     //             ? ath.groups[0].uid
    //     //             : ath.groups[0].groupId

    //     //         const orgID = ath.groups[0].organizationId
    //     //             ? ath.groups[0].organizationId
    //     //             : groupID.split('-')[0]

    //     //         if (orgID && orgID !== '') {
    //     //             await athQueryDocSnap.ref.update({ organizationId: orgID })

    //     //             result = {
    //     //                 updateStatus: 'Success',
    //     //                 message: `======= Athlete Updated`,
    //     //                 orgID,
    //     //                 uid: ath.uid,
    //     //                 name: ath.profile.firstName + ' ' + ath.profile.lastName
    //     //             }
    //     //         }
    //     //         else {
    //     //             result = {
    //     //                 updateStatus: 'Fail - Grab OrgId from Group',
    //     //                 message: `======= OrgID not found for athlete ${ath.profile.firstName} as ath using groups`,
    //     //                 orgID,
    //     //                 uid: ath.uid,
    //     //                 name: ath.profile.firstName + ' ' + ath.profile.lastName
    //     //             }
    //     //             console.warn(`======= OrgID not found for athlete ${ath.profile.firstName} as ath using groups`, ath.uid)
    //     //         }
    //     //     } else {
    //     //         result = {
    //     //             updateStatus: 'Fail - Ath has no groups',
    //     //             message: `======= OrgID not found for athlete ${ath.profile.firstName} as ath using groups`,
    //     //             uid: ath.uid,
    //     //             name: ath.profile.firstName + ' ' + ath.profile.lastName
    //     //         }
    //     //         console.warn(`======= OrgID not found for athlete ${ath.profile.firstName} as ath has no groups`, ath.uid)
    //     //     }

    //     //     updates.push(result)

    //     //     return result
    //     // } else if (ath.metadata === undefined) {
    //     //     result = {
    //     //         updateStatus: 'Fail - Ath Object is corrupt',
    //     //         message: `======= Ath Object is corrupt for athlete ${ath.profile.firstName}`,
    //     //         uid: ath.uid,
    //     //         name: ath.profile.firstName + ' ' + ath.profile.lastName
    //     //     }

    //     //     updates.push(result)

    //     //     return result
    //     // } else {
    //     //     result = {
    //     //         updateStatus: 'Pass - Ath Org Is Set',
    //     //         message: `======= Ath Org Is Set ${ath.profile.firstName}`,
    //     //         uid: ath.uid,
    //     //         name: ath.profile.firstName + ' ' + ath.profile.lastName
    //     //     }

    //     //     updates.push(result)

    //     //     return result
    //     // }

    // })


    // const theResults = await Promise.all(athletes)

    // return theResults
    return updates

}
