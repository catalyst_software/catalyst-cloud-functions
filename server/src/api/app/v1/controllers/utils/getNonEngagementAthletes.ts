import moment from 'moment';
import { Organization } from '../../../../../models/organization.model';
import { Athlete } from '../../../../../models/athlete/interfaces/athlete';
import { getDocumentFromSnapshot } from '../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { getAthleteCollectionRef } from '../../../../../db/refs';
import { chosenFunction } from '../tests/ipscore-non-engagement';
export const getNonEngagementAthletes = async (org: Organization): Promise<{
    org: Organization;
    athletes: Athlete[];
}> => {
    console.log(`getNonEngagementAthletes for ${org.name}`, org);

    const currentTime = moment(new Date()).format('hh:mm');
    const currentTimeUTC = moment.utc(new Date()).format('hh:mm');

    console.log(chosenFunction + ' CRON CLOUD FUNCTION executed at ' + currentTimeUTC + ' UTC (local client currentTime = ' + currentTime + ')');

    const latestReportDate = moment.utc().startOf('day')
        .subtract(2, 'days');
    // .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })

    console.log('latestReportDate to check (2 days) = (date) => ', latestReportDate.toDate());

    const nonEngagementAthletes = getAthleteCollectionRef()
        .where('organizationId', '==', org.uid)
        .where('currentIpScoreTracking.dateTime', '<=', latestReportDate.toDate())
        .orderBy("currentIpScoreTracking.dateTime", 'desc');

    const athletes = await nonEngagementAthletes.get()
        .then((athleteQuerySnap) => {
            if (athleteQuerySnap.size) {
                console.log(`${athleteQuerySnap.size} Non Engagement Athletes found`);
                const athlete = athleteQuerySnap.docs.map((athleteDocSnap) => {
                    return getDocumentFromSnapshot(athleteDocSnap) as Athlete;
                });
                return athlete;
            }
            else {
                console.log('No Non Engagement Athletes found');
                return [];
            }
        })
        .catch((err) => {
            console.error('Error Running The  Querying on Non Engagement Documents', err);
            // process.exit(203)
            throw err;
        });
    return {
        org,
        athletes
    };
};
