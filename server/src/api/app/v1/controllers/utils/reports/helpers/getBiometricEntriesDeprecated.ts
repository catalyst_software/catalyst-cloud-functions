import moment from 'moment';

import { db } from '../../../../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../../../../db/biometricEntries/enums/firestore-collections';
import { AthleteBiometricEntryInterface } from '../../../../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';

import { SimpleFoodMealClassificationType } from '../../../../../../../models/enums/enums.model';
import { createCSVData } from '../../../../../../../db/createCSVData';
import { LifestyleEntryType } from '../../../../../../../models/enums/lifestyle-entry-type';

export const getBiometricEntriesDeprecated = async (data: { athleteId: string }) => {


    try {

        const { athleteId } = data


        // const reportEntity: ReportEntity = {
        //     entityId: athleteId,
        //     type: undefined,
        //     entityIndicator: ReportRequestEntityType.Athlete
        // }
        // const { biometricEntriesMulti, defaultStartDate, latestReportDate } = await getDataFromBQ(moment('2019-08-31T18:11:19.991Z').toDate(), reportEntity);
        // console.info('Getting Biomentric Entries By Id', athleteId)

        // const biometricEntries = []

        // biometricEntriesMulti.map(
        //     (
        //         snap
        //     ) => {


        //         snap.map((biometricEntry) => {
        //             createReport(
        //                 biometricEntry,
        //                 biometricEntries
        //             )
        //         })



        //     }
        // )

        // console.log('biometricEntries', biometricEntries)
        // return biometricEntries
        if (athleteId) {
            return db
                .collection(FirestoreCollection.Athletes)
                .doc(athleteId)
                .collection(FirestoreCollection.AthleteBiometricEntries)
                .orderBy('lifestyleEntryType')
                .get()
                .then(querySnap => {
                    if (querySnap.empty) {
                        console.info(
                            'NO documents found for user with id ',
                            athleteId
                        )
                        return []
                    } else {
                        console.info(
                            'Documents FOUND for user with id ',
                            athleteId,
                            querySnap.size
                        )
                        const biometricEntries = []

                        querySnap.docs.map(
                            (
                                snap: FirebaseFirestore.QueryDocumentSnapshot
                            ) => {
                                if (!snap.exists) {
                                    console.log(
                                        'snap.exists ==================== FALSE'
                                    )
                                } else {
                                    const biometricEntry = {
                                        uid: snap.id,
                                        ...(snap.data() as AthleteBiometricEntryInterface),
                                    }

                                    createReport(
                                        biometricEntry,
                                        biometricEntries
                                    )
                                }
                            }
                        )

                        console.log('biometricEntries', biometricEntries)
                        return biometricEntries
                    }
                })
                .catch(error => {
                    console.error(
                        'error fetching Biometric Entries for user with id ' +
                        athleteId +
                        '........!!! => error: ',
                        error
                    )
                    throw error
                })
        } else {
            console.error('.........ERROR: athleteId is not defined')
            return Promise.resolve([])
        }
    } catch (error) {
        console.error(
            '.........CATCH ERROR: athleteId is not defined',
            error
        )
        return Promise.reject(error)
    }
}

function createReport(
    biometricEntry: AthleteBiometricEntryInterface,
    biometricEntries: any[]
) {
    const {
        uid,
        userId,
        fullName,
        groups,
        dateTime,
        lifestyleEntryType,
        healthComponentType,
        value,
        mealComponents,
        simpleMealComponents,
        painComponents,
        trainingComponents,
    } = biometricEntry
    const groupName =
        groups && groups.length ? groups[0].groupName : 'No Group Assigned'
    const momentDate = moment(dateTime.toDate(), 'YYYY-MM-DD HH:MM:SS')

    let theValue;
    if (value !== undefined && isNaN(value)) {
        theValue = 0
        console.log(value)
        // snap.ref.update({
        //     ...biometricEntry,
        //     value: 0,
        // })
    } else {
        theValue = value
    }

    switch (lifestyleEntryType) {

        case LifestyleEntryType.SimpleFood:

            if (simpleMealComponents && simpleMealComponents.length) {
                simpleMealComponents.map(simpleMealComponent => {
                    const { mealType, classificationType } = simpleMealComponent
                    biometricEntries.push(
                        createCSVData(
                            uid,
                            userId,
                            fullName,
                            groupName,
                            momentDate,
                            lifestyleEntryType,
                            healthComponentType,
                            // value
                            undefined,
                            // Meal
                            mealType,
                            undefined,
                            undefined,
                            SimpleFoodMealClassificationType[classificationType]
                        )
                    )
                    // return mealComponent
                })
            }

            break

        case LifestyleEntryType.Food:
            if (mealComponents && mealComponents.length) {
                mealComponents.map(mealComponent => {
                    const { mealType, foodType, servingCount } = mealComponent
                    biometricEntries.push(
                        createCSVData(
                            uid,
                            userId,
                            fullName,
                            groupName,
                            momentDate,
                            lifestyleEntryType,
                            healthComponentType,
                            // value
                            undefined,
                            // Meal
                            mealType,
                            foodType,
                            servingCount
                        )
                    )
                    // return mealComponent
                })
            }

            break

        case LifestyleEntryType.Pain:
            if (painComponents && painComponents.length) {
                painComponents.map(painComponent => {
                    const { painType, bodyLocation, painLevel } = painComponent
                    biometricEntries.push(
                        createCSVData(
                            uid,
                            userId,
                            fullName,
                            groupName,
                            momentDate,
                            lifestyleEntryType,
                            healthComponentType,
                            // value
                            undefined,
                            // Meal
                            undefined,
                            undefined,
                            undefined,
                            undefined,
                            // Pain
                            painType,
                            bodyLocation,
                            painLevel
                        )
                    )
                })
            } else {
                biometricEntries.push(
                    createCSVData(
                        uid,
                        userId,
                        fullName,
                        groupName,
                        momentDate,
                        lifestyleEntryType,
                        healthComponentType,
                        // value
                        undefined,
                        // Meal
                        undefined,
                        undefined,
                        undefined,
                        undefined,
                        // Pain
                        undefined,
                        undefined,
                        theValue
                    )
                )
            }

            break

        case LifestyleEntryType.Training:
            if (trainingComponents && trainingComponents.length) {
                trainingComponents.map(trainingComponent => {
                    const {
                        sportType,
                        sportName,
                        intensityLevel,
                        minutes,
                    } = trainingComponent

                    const theIntesityLevel =
                        intensityLevel !== undefined && intensityLevel > 2
                            ? 2
                            : intensityLevel

                    biometricEntries.push(
                        createCSVData(
                            uid,
                            userId,
                            fullName,
                            groupName,
                            momentDate,
                            lifestyleEntryType,
                            healthComponentType,
                            // value
                            undefined,
                            // Meal
                            undefined,
                            undefined,
                            undefined,
                            undefined,
                            // Pain
                            undefined,
                            undefined,
                            undefined,
                            // Sport
                            sportType,
                            sportName,
                            theIntesityLevel,
                            minutes
                        )
                    )
                    // return trainingComponent
                })
            }
            break

        default:
            biometricEntries.push(
                createCSVData(
                    uid,
                    userId,
                    fullName,
                    groupName,
                    momentDate,
                    lifestyleEntryType,
                    healthComponentType,
                    theValue
                )
            )
            break
    }
}

