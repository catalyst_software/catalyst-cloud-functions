import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { getGroupedByDay } from './getGroupedByDay';
import { sum } from 'lodash';
import { athleteColors } from '../../composeReportData';

export const getStackedChartData = (athleteEntries: BiometricEntrySchema[], athleteID: string, athleteFullName: string, startOfWeek, daysOfWeek: {
    date: Date;
    uid?: string;
    lineChartData: any;
    entries: BiometricEntrySchema[];
}[], strackedChartDatasets: any[]) => {

    const setAllToToday = false;
    getGroupedByDay(setAllToToday, athleteEntries, startOfWeek, daysOfWeek);

    const colourIndicator = athleteColors.find((athColour) => athColour.uid === athleteID);

    const colour = colourIndicator
        ? colourIndicator.colour
        : "#dc16ff"


    // const dataPack = [40, 47, 44, 38, 27, 31, 25];
    const athleteDataset = {
        label: athleteFullName,
        data: daysOfWeek.map((day) => sum(day.entries.map((entry) => entry.value))),
        backgroundColor: colour,
        hoverBackgroundColor: "#7E57C2",
        hoverBorderWidth: 0
    }


    // const athleteDataset = {

    //     "label": athleteFullName,
    //     "data": daysOfWeek.map((day) => sum(day.entries.map((entry) => entry.value))),
    //     "borderColor": colour,
    //     "fill": false,
    //     "borderJoinStyle": "round"
    // };

    strackedChartDatasets.push(athleteDataset);

};
