
import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { SimpleMealTypeData } from '../../types/training-trend-data';
import { SimpleFoodMealClassificationType, MealType } from '../../../../../../../../models/enums/enums.model';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';

export const getGroupedByMealType = (athlteteSimpleMealEntries: BiometricEntrySchema[]) => {

    // const mealClassificationData: SimpleMealClassificationData = [];
    const mealTypeData: SimpleMealTypeData = [];


    const clasificationTypeKeys = Object.keys(SimpleFoodMealClassificationType).filter(
        k => typeof SimpleFoodMealClassificationType[k as any] === 'string'
    ).map((name) => SimpleFoodMealClassificationType[name])
    // ["Unspecified", "Skipped", "Unhealthy", "Healthy"]

    // clasificationTypeKeys.forEach(clasificationType => {
    //     // Light
    //     console.log(clasificationType)

    //     mealClassificationData.push({
    //         classificationType: clasificationType,
    //         meanValue: 0,
    //         lowValue: 0,
    //         highValue: 0,
    //         totalValue: 0,
    //         avg: 0
    //     })

    // })

    const mealTypeKeys = Object.keys(MealType).filter(
        k => typeof MealType[k as any] === 'string'
    ).map((name) => MealType[name])
    // ["Breakfast", "Lunch", "Dinner", "Snack"]

    // mealTypeKeys.forEach(mealType => {
    //     // Snack
    //     console.log(mealType)

    //     mealTypeData.push({
    //         mealType,
    //         percentagePerclassification: []
    //     })

    // })


    // from(athlteteSimpleMealEntries)
    //     .pipe(
    //         groupBy(entry => entry.mealType),
    //         mergeMap(mealTypeGroup => zip(of(toTitleCase(mealTypeGroup.key)), mealTypeGroup.pipe(toArray())))
    //     )
    //     .subscribe((theMealTypeData) => {

    //         const mealType: MealType = MealType[theMealTypeData[0]];


    //         const groupedByMealTypeEntries = theMealTypeData[1];

    //         const groupedByClassificationTypeEntries = groupByMap(groupedByMealTypeEntries, (entry: BiometricEntrySchema) => SimpleFoodMealClassificationType[entry.value]) as Map<string, Array<BiometricEntrySchema>>;
    //         // TODO: Group Entries By Day
    //         groupedByClassificationTypeEntries.forEach((classificationTypeEntries, classificationType) => {
    //             // const dayIndex = moment(athleteEntries[0].creationTimestamp).weekday() - 1;
    //             const dayIndex = 0;


    //             const dayIndex2 = mealTypeData.find((mt) => {
    //                 return mt
    //             });


    //             const meal = theMealTypeData[0]
    //             const clasificationType = classificationType
    //             // const date = moment(athleteEntries[0].creationTimestamp).startOf('day').toDate() // athleteEntries[0].creationTimestamp

    //             // const dayOfWeek = daysOfWeek.find((entry) => entry.date.toDateString() === date.toDateString())
    //             // if (dayOfWeek)
    //             //     // daysOfWeek.find((entry) => entry.date === date).entries = athleteEntries;
    //             //     dayOfWeek.entries = athleteEntries;
    //             // else
    //             //     console.error('Date For Entry Not Found in daysOfWeek Array')

    //             const classificationTypeIndex = mealClassificationData.findIndex((levle) => levle.classificationType === classificationType)

    //             const intenisityLevelValues = {

    //                 // classificationType: mealType,
    //                 classificationType: theMealTypeData[0],
    //                 groupedEntries: classificationTypeEntries,

    //                 // uid: athlteteTrainingEntries[0].athleteID,
    //                 // firstName: athlteteTrainingEntries[0].userFirstName,
    //                 // fullName: athlteteTrainingEntries[0].userFullName
    //             }


    //             mealClassificationData[classificationTypeIndex] = {
    //                 ...mealClassificationData[classificationTypeIndex],
    //                 ...intenisityLevelValues
    //             }

    //             const mealTypeDataIndex = mealTypeData.findIndex((levle) => levle.mealType === meal)


    //             console.log("mealTypeDataIndex", mealTypeDataIndex)
    //             // mealTypeData[mealTypeDataIndex].chartData = undefined;
    //         });




    //         // intensityLevelData.push(intenisityLevelValues);
    //     });


    // const totalCount = sum(mealClassificationData.map((level) => level.totalValue));


    // mealClassificationData.map((intesityGroup) => {
    //     const avgOutOfHundred = intesityGroup.totalValue / totalCount * 100;
    //     intesityGroup.avg = avgOutOfHundred;
    // });


    const groupedByMealType = groupByMap(athlteteSimpleMealEntries, (entry: BiometricEntrySchema) => entry.mealType) as Map<string, BiometricEntrySchema[]>;

    // const breakfastEntries = groupedByMealType.get('breakfast')
    // const lunchEntries = groupedByMealType.get('lunch')
    // const dinnerEntries = groupedByMealType.get('dinner')
    // const snackEntries = groupedByMealType.get('snack')

    mealTypeKeys.forEach((mealType: string) => {

        const mealTypeEntries = groupedByMealType.get(mealType.toLowerCase())

        if (mealTypeEntries) {
            debugger;

            const theMealTypeData = {
                mealType,
                percentagePerclassification: [],
                // meanValue: 0,
                // lowValue: 0,
                // highValue: 0,
                // totalValue: 0,
                // avg: 0
            }



            const totalMealTypeCount = mealTypeEntries.length;

            clasificationTypeKeys.forEach(clasificationType => {
                // Light
                console.log(clasificationType)
                const classificationType = SimpleFoodMealClassificationType[clasificationType] as any
                const totalMealTypeByClassificationCount = mealTypeEntries.filter((entry) => entry.value === classificationType).length
                const avgOutOfHundred = totalMealTypeByClassificationCount / totalMealTypeCount * 100;
                theMealTypeData.percentagePerclassification.push({
                    clasificationType,
                    totalMealTypeCount: totalMealTypeByClassificationCount,
                    avg: avgOutOfHundred
                })

            })

            mealTypeData.push(theMealTypeData)


        }
        // else {

        // }

    })
    return mealTypeData
};
