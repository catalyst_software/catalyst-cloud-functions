import { BiometricEntrySchema, BiometricEntryIndicator } from '../../../../../../pubsub/interfaces/red-flag-reports';
import { firestore } from 'firebase-admin';
import moment from 'moment';

export const sortByValue = (a: BiometricEntrySchema | BiometricEntryIndicator, b: BiometricEntrySchema | BiometricEntryIndicator) => {
    if (a.value < b.value)
        return -1;
    if (a.value > b.value)
        return 1;
    return 0;
};
export const sortByValueDesc = (a: BiometricEntrySchema | {
    uid: string;
    name: string;
    value: number;
}, b: BiometricEntrySchema | {
    uid: string;
    name: string;
    value: number;
}) => {
    if (a.value < b.value)
        return 1;
    if (a.value > b.value)
        return -1;
    return 0;
};

export const sortByDate: (a: { date: Date }, b: { date: Date }) => number = (a, b) => a.date.getTime() - b.date.getTime();

export const sortByCreationTimestamp: (a:  BiometricEntrySchema , b:  BiometricEntrySchema ) => number = (a, b) =>
    moment(a.creationTimestamp).toDate().getTime() - moment(b.creationTimestamp).toDate().getTime();

export const sortByCountDesc = (a: {
    count: number;
}, b: {
    count: number;
}) => {
    if (a.count < b.count)
        return 1;
    if (a.count > b.count)
        return -1;
    return 0;
};
export const sortByAvgDesc = (a: {
    avg: number;
}, b: {
    avg: number;
}) => {
    if (a.avg < b.avg)
        return 1;
    if (a.avg > b.avg)
        return -1;
    return 0;
};
export const sortPainByValue = (a: {
    name: string;
    type: string;
    level: number;
}, b: {
    name: string;
    type: string;
    level: number;
}) => {
    if (a.level < b.level)
        return -1;
    if (a.level > b.level)
        return 1;
    return 0;
};

export const sortByLabelDesc = (a: {
    label: string;
}, b: {
    label: string;
}) => {
    if (a.label < b.label)
        return 1;
    if (a.label > b.label)
        return -1;
    return 0;
};