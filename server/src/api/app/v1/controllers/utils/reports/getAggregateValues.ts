import { BiometricEntrySchema } from '../../../../../../pubsub/interfaces/red-flag-reports';
import { getMeanOfValue } from "./getMeanOfValue";
import { getLowestValue } from './getLowestValue';
import { getHighestValue } from './getHighestValue';
import { getSumOfValue } from './getSumOfValue';

export const getAggregateValues = (athlteteEntries: BiometricEntrySchema[]) => {

    const meanValue = getMeanOfValue(athlteteEntries);
    const lowValue = getLowestValue(athlteteEntries);
    const highValue = getHighestValue(athlteteEntries);
    const totalValue = getSumOfValue(athlteteEntries);
    const countValue = athlteteEntries.length;

    return { meanValue, lowValue, highValue, totalValue, countValue };
};
