import { ChartType } from '../../../../../../../../pubsub/interfaces/red-flag-reports';

export const getTrends = (moodEntries, sleepEntries, fatigueEntries): {
    type: string;
    "include": boolean;
    "data": {
        "isTrends": boolean;
        "lineChart": {
            "chartType": ChartType;
            "chartData": {
                "labels": string[];
                "datasets": {
                    "data": number[];
                    "label": string;
                    "borderColor": string;
                    "fill": boolean;
                    "borderJoinStyle": string;
                }[];
            };
        };
        "barChart": {
            "chartType": ChartType;
            "chartData": {
                "labels": string[];
                "datasets": [{
                    "data": number[];
                    "backgroundColor": string[];
                }];
            };
        };
    };
}[] => {

    // const groupByBodyLocation = groupByMap(moodEntries, (entry: BiometricMoodEntrySchema) => entry.bodyLocation);

    return [
        {
            "type": "mood",
            "include": true,
            "data": {
                "isTrends": true,
                "lineChart": {
                    "chartType": ChartType.Line,
                    "chartData": moodEntries.lineChartData.chartData
                },
                "barChart": {
                    "chartType": ChartType.HorizontalBar,
                    "chartData": moodEntries.barChartData.chartData
                }
            }
        },
        {
            "type": "sleep",
            "include": true,
            "data": {
                "isTrends": true,
                "lineChart": {
                    "chartType": ChartType.Line,
                    "chartData": sleepEntries.lineChartData.chartData
                },
                "barChart": {
                    "chartType": ChartType.HorizontalBar,
                    "chartData": sleepEntries.barChartData.chartData
                }
            }
        },
        {
            "type": "fatigue",
            "include": true,
            "data": {
                "isTrends": true,
                "lineChart": {
                    "chartType": ChartType.Line,
                    "chartData": fatigueEntries.lineChartData.chartData
                },
                "barChart": {
                    "chartType": ChartType.HorizontalBar,
                    "chartData": fatigueEntries.barChartData.chartData
                }
            }
        }
    ];
};
