import { athleteColors } from '../../composeReportData';
export const getAthleteColour = (athleteID: string): string => {
    const colourIndicator = athleteColors.find((athColour) => athColour.uid === athleteID);
    const colour = colourIndicator
        ? colourIndicator.colour
        : "#ffffff";
    return colour;
};
