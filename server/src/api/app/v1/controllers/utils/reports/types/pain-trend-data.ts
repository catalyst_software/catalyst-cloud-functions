import { BiometricPainEntrySchema, BiometricEntryIndicator, BiometricEntrySchema } from "../../../../../../../pubsub/interfaces/red-flag-reports";

export type PainTrendData = Array<{
    athleteID?: string;
    firstName?: string;
    fullName?: string;
    meanValue: number;
    lowValue: number;
    highValue: number;
    totalValue: number;
    entries:  BiometricEntrySchema[];
    avg: number;
}>;

export type PainByBodyLocationData = Array<{
    athleteID?: string;
    firstName?: string;
    fullName?: string;
    bodyLocation: string;
    meanValue: number;
    lowValue: number;
    highValue: number;
    countValue: number;
    totalValue: number;
    entries:  BiometricEntrySchema[];
    avg: number;
}>;
