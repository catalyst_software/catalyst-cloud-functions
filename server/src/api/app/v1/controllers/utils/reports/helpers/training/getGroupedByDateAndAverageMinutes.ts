import { from, of, zip } from 'rxjs';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { map } from 'rxjs/internal/operators/map';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { toArray } from 'rxjs/internal/operators/toArray';
import { sum } from 'lodash';

import { BiometricTrainingEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { TrainingTrendData } from '../../types/training-trend-data';
import { getAggregateValues } from '../../getAggregateValues';

export const getGroupedByDateAndAverageMinutes = (athlteteTrainingEntries: BiometricTrainingEntrySchema[]) => {

    const intensityLevelData: TrainingTrendData = [];
    const selectedIntesitylevels = [];

    from(athlteteTrainingEntries)
        .pipe(
            groupBy(entry => entry.intensityLevel),
            map((groupedLevel) => {

                // console.log('grouped', groupedLevel);
                selectedIntesitylevels.push(groupedLevel.key);

                return groupedLevel;
            }),
            mergeMap(intensityLevelGroup => zip(of(intensityLevelGroup.key), intensityLevelGroup.pipe(toArray())))
        )
        .subscribe((groupedByIntensityLevel) => {

            const intensityLevel = groupedByIntensityLevel[0];
            const groupedEntries = groupedByIntensityLevel[1];

            const intenisityLevelValues = {

                intensityLevel,
                ...getAggregateValues(groupedEntries),

                uid: athlteteTrainingEntries[0].athleteID,
                firstName: athlteteTrainingEntries[0].userFirstName,
                fullName: athlteteTrainingEntries[0].userFullName,
                
                avg: null
            }

            intensityLevelData.push(intenisityLevelValues);
        });

    const totalCount = sum(intensityLevelData.map((level) => level.totalValue));

    intensityLevelData.map((intesityGroup) => {
        const avgOutOfHundred = intesityGroup.totalValue / totalCount * 100;
        intesityGroup.avg = avgOutOfHundred;
    });

    return intensityLevelData;
};


