import { from, of, zip } from 'rxjs';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { toArray } from 'rxjs/internal/operators/toArray';
import { map } from 'rxjs/internal/operators/map';
import { sum } from 'lodash';

import { BiometricPainEntrySchema } from '../../../../../../../pubsub/interfaces/red-flag-reports';
import { getMeanOfValue } from '../getMeanOfValue';
import { getLowestValue } from '../getLowestValue';
import { getHighestValue } from '../getHighestValue';
import { PainByBodyLocationData } from '../types/pain-trend-data';
import { getSumOfValue } from '../getSumOfValue';
import { getEntriesIndicator } from '../getEntriesIndicator';
import { sortByValue } from '../sortByHelpers';

export const getGroupedByBodyLocation = (athlteteTrainingEntries: BiometricPainEntrySchema[]) => {

    const bodyLocationData: PainByBodyLocationData = [];
    const selectedbodyLocations = [];

    from(athlteteTrainingEntries)
        .pipe(
            groupBy(entry => entry.bodyLocation),
            map((bodyLocationGrouped) => {

                // console.log('grouped', bodyLocationGrouped);
                selectedbodyLocations.push(bodyLocationGrouped.key);

                return bodyLocationGrouped;
            }),
            mergeMap(bodyLocationGroup => zip(of(bodyLocationGroup.key), bodyLocationGroup.pipe(toArray())))
        )
        .subscribe((groupedByBodyLocation) => {
            const bodyLocation = groupedByBodyLocation[0];
            const athlteteByBodyLocationEntries = groupedByBodyLocation[1];

            const meanValueOfPain = getMeanOfValue(athlteteByBodyLocationEntries);
            const lowValueOfPain = getLowestValue(athlteteByBodyLocationEntries);
            const highValueOfPain = getHighestValue(athlteteByBodyLocationEntries);
            const totalValueOfPain = getSumOfValue(athlteteByBodyLocationEntries);

            const painLevelValues = {
                bodyLocation,
                meanValue: meanValueOfPain,
                lowValue: lowValueOfPain,
                highValue: highValueOfPain,
                totalValue: totalValueOfPain,
                countValue: athlteteByBodyLocationEntries.length,
                entries: getEntriesIndicator(athlteteByBodyLocationEntries).sort(sortByValue),
                avg: null
            };

            bodyLocationData.push({
                athleteID: athlteteTrainingEntries[0].athleteID,
                firstName: athlteteTrainingEntries[0].userFirstName,
                fullName: athlteteTrainingEntries[0].userFullName,
                ...painLevelValues
            });
        });

    const totalCount = sum(bodyLocationData.map((level) => level.totalValue));

    bodyLocationData.map((intesityGroup) => {
        const avgOutOfHundred = intesityGroup.totalValue / totalCount * 100;
        intesityGroup.avg = avgOutOfHundred;
    });
    
    return bodyLocationData;
};
