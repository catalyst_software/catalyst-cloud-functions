import { meanBy } from 'lodash';
import { BiometricEntrySchema, ChartType, BiometricScalarEntrySchema, BiometricPainEntrySchema, RedFlagSchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { getAthleteColour } from './getAthleteColour';

const randomColor = require('randomcolor');

export const getRedFlags = (
    moodEntries: BiometricScalarEntrySchema[],
    sleepEntries: BiometricScalarEntrySchema[],
    sadAthletes: BiometricEntrySchema[],
    lessThanFourHoursSleepEntries: BiometricEntrySchema[],
    fatigueEntries: BiometricScalarEntrySchema[],
    painEntries: BiometricPainEntrySchema[],
    fatiguedThanFourHoursSleepEntries: BiometricScalarEntrySchema[],
    athletesWithPain: BiometricPainEntrySchema[],
    foodTypeCounts: any[],
    simpleFoodEntries: {
        lowValue: number;
        highValue: number;
        meanValue: number;
        totalValue: number;
        countValue: number;
        barChartData: Array<{
            mealType: string;
            percentagePerclassification: Array<{
                clasificationType: string;
                totalMealTypeCount: number;
                avg: number;
            }>;
            theBarChart: {
                chartType: ChartType;
                label: string;
                chartData: {
                    labels: Array<string>;
                    datasets: Array<{
                        // label: string;
                        data: Array<number>;
                        backgroundColor: Array<string>;
                    }>;
                };
            };
        }>;
        entries: Array<BiometricEntrySchema>;
    },
    mealTypeAverages: any[],
    minutesBySportType: Array<{
        sportType: string,
        value: number
    }>): RedFlagSchema => {

    const thePayload: RedFlagSchema = {
        brainSummary: {
            include: true,
            data: {
                useBarChart: true,
                summaryData: {
                    "title": "Brain",
                    dataSets: [
                        [{
                            "subject": "average mood!!!",
                            value: meanBy(moodEntries, (e) => e.value),
                        }],
                        [{
                            "subject": "average sleep!!!",
                            value: meanBy(sleepEntries, (e: {
                                value: number;
                            }) => e.value),
                        }]
                    ]
                },
                redFlagGroups: [
                    {
                        "image": "icons8-sad-96.png",
                        "label": "entries with sad or very sad!",
                        chartType: ChartType.HorizontalBar,
                        chartData: {
                            labels: sadAthletes.map(sadAthlete => sadAthlete.name),
                            datasets: [
                                {
                                    data: sadAthletes.map(sadAthlete => sadAthlete.value),
                                    backgroundColor: sadAthletes.map(sadSleepAthlete => getAthleteColour(sadSleepAthlete.athleteID))
                                }
                            ]
                        }
                    }, {
                        "image": "icons8-insomnia-96.png",
                        "label": "less than 4 hours sleep!",
                        chartType: ChartType.HorizontalBar,
                        chartData: {
                            labels: lessThanFourHoursSleepEntries.map(fatiguedAthlete => fatiguedAthlete.name),
                            datasets: [
                                {
                                    data: lessThanFourHoursSleepEntries.map(littleSleepAthlete => littleSleepAthlete.value),
                                    backgroundColor: lessThanFourHoursSleepEntries.map(littleSleepAthlete => getAthleteColour(littleSleepAthlete.athleteID))
                                }
                            ]
                        }
                    }
                ]
            }
        },
        bodySummary: {
            include: true,
            data: {
                useBarChart: true,
                summaryData: {
                    "title": "Body",
                    dataSets: [
                        [{
                            "subject": "average fatigue!!!",
                            value: meanBy(fatigueEntries, (e: {
                                value: number;
                            }) => e.value)
                        }],
                        [{
                            "subject": "average pain!!!",
                            value: meanBy(painEntries, (e: {
                                value: number;
                            }) => e.value),
                        }]
                    ]
                },
                redFlagGroups: [
                    {
                        "image": "icons8-bored-96.png",
                        "label": "entries >= 4!!",
                        chartType: ChartType.HorizontalBar,
                        chartData: {
                            labels: fatiguedThanFourHoursSleepEntries.map(fatiguedAthlete => fatiguedAthlete.name),
                            datasets: [
                                {
                                    data: fatiguedThanFourHoursSleepEntries.map(fatiguedAthlete => fatiguedAthlete.value),
                                    backgroundColor: fatiguedThanFourHoursSleepEntries.map(athleteWithFatigue => getAthleteColour(athleteWithFatigue.athleteID)),
                                }
                            ]
                        }
                    }, {
                        "image": "icons8-crutch-96.png",
                        "label": "entries >= 4!!",
                        chartType: ChartType.HorizontalBar,
                        chartData: {
                            labels: athletesWithPain.map(athleteWithPain => athleteWithPain.name),
                            datasets: [
                                {
                                    data: athletesWithPain.map(athleteWithPain => athleteWithPain.value),
                                    backgroundColor: athletesWithPain.map(athleteWithPain => getAthleteColour(athleteWithPain.athleteID)),
                                }
                            ]
                        }
                    }
                ]
            }
        },
        foodSummary: {
            include: true,
            data: {
                usePieChart: true,
                summaryData: {
                    "title": "Food",
                    "heading": "average serve counts",
                    dataSets:
                        [
                            mealTypeAverages.map((mealAvg) => {
                                return {
                                    subject: mealAvg.mealType,
                                    value: mealAvg.avg
                                }
                            })
                        ]
                },
                // TODO:
                "pieChartData": {
                    "label": "entries >= 4!!",
                    chartType: undefined, // simpleFoodEntries.barChartData.chartType, // ChartType.Pie,
                    chartData: undefined // simpleFoodEntries.barChartData.chartData, // {
                    //     labels: mealTypeAverages.map(mealTypeGroup => mealTypeGroup.mealType),
                    //     datasets: [{
                    //         // data: foodTypeCounts.map(foodTypeGroup => foodTypeGroup.count),
                    //         data: mealTypeAverages.map(mealTypeGroup => mealTypeGroup.avg),
                    //         backgroundColor: mealTypeAverages.map(() => randomColor()),
                    //     }]
                    // }
                }
            }
        },
        trainingSummary: {
            include: true,
            data: {
                usePieChart: true,
                summaryData: {
                    "title": "Training",
                    dataSets: [
                        [{
                            "subject": "average session length!!!",
                            value: meanBy(minutesBySportType, (e) => e.value)
                        }]
                    ]
                },
                pieChartData: {
                    "label": "entries >= 4!!",
                    chartType: ChartType.Pie,
                    chartData: {
                        labels: minutesBySportType.map((set) => set.sportType),
                        datasets: [{
                            data: minutesBySportType.map((set) => set.value),
                            backgroundColor: minutesBySportType.map((set) => randomColor()),
                        }]
                    }
                }
            }
        }
    };

    return thePayload
};
