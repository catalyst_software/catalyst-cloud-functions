import { ReportRequestEntityType } from './../../../../../../models/enums/enums.model';

import { EventTypes } from '../../../../../../models/enums/event-types';
import { BiometricEntrySchema } from '../../../../../../pubsub/interfaces/red-flag-reports';
import { getBQSQL } from '../../biometricEntriesSQLQuery';

import { bigqueryClient } from '../../../callables/utils/big-query/initBQ';
import { ReportEntity } from '../../../../../../models/report-model';


export const getDataFromBQ = async (startDate: string, toDate: string, entity: ReportEntity) => {
    // const biometricEntriesDate = moment()
    //     .set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).toDate();

    // const currentDayTimestamp = firestore.Timestamp.fromDate(biometricEntriesDate);
    // const reportResults2 = await generateReport(undefined, requestingUser);
    // return reportResults2

    // const toData = moment(requestDateTime)
    //     .subtract(1, 'day')
    //     .endOf('day');


    const theBiometricEntriesQuery = [...getBQSQL('biometricEntriesSQLQuery')];
    // const startTime = moment();
    const theStartDate = startDate

    console.warn('theStartDate', theStartDate);

    const theEndDate = toDate;

    switch (entity.type) {
        case ReportRequestEntityType.Athlete:
            theBiometricEntriesQuery[2] += `\n AND (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'USER_ID') = '${entity.entityId}'`
            break;
        case ReportRequestEntityType.Group:

            const groupIDs = entity.groupIDs || [];
            const numOfGroups = groupIDs.length;
            let clause = '';

            groupIDs.forEach((id, idx) => {
                clause += `\n (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'GROUP_IDS') LIKE '%"${id}"%'`
                if (idx + 1 < numOfGroups) {
                    clause += ` OR`
                }
            })


            theBiometricEntriesQuery[2] += `\n AND (${clause})`
            break;


        case ReportRequestEntityType.Organization:

            theBiometricEntriesQuery[2] += `\n AND (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'ORGANIZATION_ID') = '${entity.entityId}'`
            break;

        default:
            break;
    }
    const theQ = theBiometricEntriesQuery.join(' ')

    console.warn('theQ', theQ);
    console.log('startDate', theStartDate);
    console.log('endDate', theEndDate);

    let biometricEntriesMulti = []
    try {
        biometricEntriesMulti = await bigqueryClient.query({
            query: theQ,
            params: {
                // eventName: EventTypes.biometricMoodCreated,
                // eventName: 'BIOMETRIC/\/\_%',
                startDate: theStartDate,
                endDate: theEndDate // requestDateTime || latestReportDate.format("YYYYMMDD")
            }
        }).catch((err) => {
            console.error(`Error retrieving BigQuery (theQuery) query inspire-*.analytics_193010412.events_${toDate}.${EventTypes.biometricTrainingCreated}`, { err });
            return err;
        }) as Array<Array<BiometricEntrySchema>>;
    } catch (error) {
        console.error(`CATCH ERROR: > Error retrieving BigQuery (theQuery) query inspire-*.analytics_193010412.events_${toDate}.${EventTypes.biometricTrainingCreated}`, { error });

    }


    // const endTime = moment();
    // const duration = moment.utc(moment(startTime.toDate(), "DD/MM/YYYY HH:mm:ss").diff(moment(endTime.toDate(), "DD/MM/YYYY HH:mm:ss"))).format("ss ms");


    // const du = moment.duration(endTime.diff(startTime));

    // const time = endTime.toDate().getMilliseconds() - startTime.toDate().getMilliseconds();
    // const duration = moment.duration(time).seconds();
    // console.log('startTime', startTime.toDate());
    // console.log('endTime', endTime.toDate());
    // console.log('Query Duration', du.asSeconds());

    return { biometricEntriesMulti, startDate, toDate };
};
