import { from, of, zip } from 'rxjs';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { toArray } from 'rxjs/internal/operators/toArray';
import { map } from 'rxjs/internal/operators/map';
import { sum } from 'lodash';

import { BiometricPainEntrySchema } from '../../../../../../../pubsub/interfaces/red-flag-reports';
import { getMeanOfValue } from '../getMeanOfValue';
import { getLowestValue } from '../getLowestValue';
import { getHighestValue } from '../getHighestValue';
import { PainTrendData } from '../types/pain-trend-data';
import { getSumOfValue } from '../getSumOfValue';

import { getGroupedByBodyLocation } from './getroupedByBodyLocation';

export const getroupedByPainType = (athlteteTrainingEntries: BiometricPainEntrySchema[]) => {
    
    const painLevelData: PainTrendData = [];
    const selectedIntesitylevels = [];

    from(athlteteTrainingEntries)
        .pipe(
            groupBy(entry => entry.painType),
            map((painTypeGrouped) => {

                // console.log('grouped', painTypeGrouped);
                selectedIntesitylevels.push(painTypeGrouped.key);

                return painTypeGrouped;
            }),
            mergeMap(intensityLevelGroup => zip(of(intensityLevelGroup.key), intensityLevelGroup.pipe(toArray())))
        )
        .subscribe((groupedByPainType) => {

            const painType = groupedByPainType[0];
            const athltetePainEntries = groupedByPainType[1];

            const painByBodyLocationMap = getGroupedByBodyLocation(athltetePainEntries);
            const meanValueOfPain = getMeanOfValue(athltetePainEntries);
            const lowValueOfPain = getLowestValue(athltetePainEntries);
            const highValueOfPain = getHighestValue(athltetePainEntries);
            const totalValueOfPain = getSumOfValue(athltetePainEntries);

            const painLevelValues = {
                painType,
                painByBodyLocations: painByBodyLocationMap,
                meanValue: meanValueOfPain,
                lowValue: lowValueOfPain,
                highValue: highValueOfPain,
                totalValue: totalValueOfPain,
                entries: athltetePainEntries,
                avg: null
            };

            painLevelData.push({
                athleteID: athlteteTrainingEntries[0].athleteID,
                firstName: athlteteTrainingEntries[0].userFirstName,
                fullName: athlteteTrainingEntries[0].userFullName,
                ...painLevelValues
            });

        });
    const totalCount = sum(painLevelData.map((level) => level.totalValue));

    painLevelData.map((intesityGroup) => {
        const avgOutOfHundred = intesityGroup.totalValue / totalCount * 100;
        intesityGroup.avg = avgOutOfHundred;
    });

    return painLevelData;
};
