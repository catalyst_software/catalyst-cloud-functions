export type TrainingTrendData = Array<{
    uid?: string;
    firstName?: string;
    fullName?: string;
    intensityLevel: string;
    meanValue: number;
    lowValue: number;
    highValue: number;
    totalValue: number;
    avg: number;
}>;

export type SimpleMealClassificationData = Array<{
    uid?: string;
    firstName?: string;
    fullName?: string;
    classificationType: string;
    meanValue: number;
    lowValue: number;
    highValue: number;
    totalValue: number;
    avg: number;
}>;

export type SimpleMealTypeData = Array<

    // uid?: string;
    // firstName?: string;
    // fullName?: string;
    // mealType: string;
    // meanValue: number;
    // lowValue: number;
    // highValue: number;
    // totalValue: number;
    // avg: number;
    // }
    {
        mealType: string;
        percentagePerclassification: Array<{
            clasificationType: string; // SimpleFoodMealClassificationType,
            totalMealTypeCount: number;
            avg: number

        }>;
        // meanValue: number;
        // lowValue: number;
        // highValue: number;
        // totalValue: number;
        // avg: number;
    }

>;

export type AverageTrainingMinutesTrendData = Array<{
    uid: string;
    firstName: string;
    fullName: string;
    intensityLevel: string;
    meanValue: number;
    lowValue: number;
    highValue: number;
    totalValue: number;
    avg: number;
}>;


export type SportTypeData = Array<{
    uid?: string;
    firstName?: string;
    fullName?: string;
    sportType?: string;
    meanValue: number;
    lowValue: number;
    highValue: number;
    totalValue: number;
    avg: number;
}>;
