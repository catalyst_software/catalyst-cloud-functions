import { from, of, zip } from 'rxjs';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { toArray } from 'rxjs/internal/operators/toArray';
import { map } from 'rxjs/internal/operators/map';
import { sum } from 'lodash';

import { BiometricPainEntrySchema, BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { PainTrendData } from '../../types/pain-trend-data';
import { getAggregateValues } from '../../getAggregateValues';
import { getEntriesIndicator } from '../../getEntriesIndicator';
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { sortByValue } from '../../sortByHelpers';

export const getGroupedByPainType = (athlteteTrainingEntries: BiometricEntrySchema[]) => {

    const painLevelData: PainTrendData = [];
    const selectedIntesitylevels = [];
    removeUndefinedProps(athlteteTrainingEntries)
    from(athlteteTrainingEntries)
        .pipe(
            groupBy(entry => entry.painType),
            map((painTypeGrouped) => {

                // console.log('grouped', painTypeGrouped);
                selectedIntesitylevels.push(painTypeGrouped.key);

                return painTypeGrouped;
            }),
            mergeMap(intensityLevelGroup => zip(of(intensityLevelGroup.key), intensityLevelGroup.pipe(toArray())))
        )
        .subscribe((groupedByPainType) => {

            const painType = groupedByPainType[0];
            const athltetePainEntries = groupedByPainType[1] as BiometricPainEntrySchema[];

            const {
                meanValue: meanValueOfBodyLocation,
                lowValue: lowValueOfBodyLocation,
                highValue: highValueOfBodyLocation,
                totalValue: totalValueOfBodyLocation,
                countValue: countValueOfBodyLocation
            } = getAggregateValues(athltetePainEntries);

            const painLevelValues = {
                painType,
                meanValue: meanValueOfBodyLocation,
                lowValue: lowValueOfBodyLocation,
                highValue: highValueOfBodyLocation,
                totalValue: totalValueOfBodyLocation,
                countValue: countValueOfBodyLocation,
                avg: null
            };

            painLevelData.push({
                athleteID: athlteteTrainingEntries[0].athleteID,
                firstName: athlteteTrainingEntries[0].userFirstName,
                fullName: athlteteTrainingEntries[0].userFullName,
                entries: getEntriesIndicator(athltetePainEntries).sort(sortByValue),
                ...painLevelValues
            });
        });

    const totalCount = sum(painLevelData.map((level) => level.totalValue));

   const theResultData = painLevelData.map((intesityGroup) => {
        const { firstName, fullName, athleteID, ...theRestData } = intesityGroup
        const avgOutOfHundred = theRestData.totalValue / totalCount * 100;
        theRestData.avg = avgOutOfHundred;
        return theRestData
    });



    // removeUndefinedProps(painLevelData)
    removeUndefinedProps(theResultData)

    return theResultData;
};
