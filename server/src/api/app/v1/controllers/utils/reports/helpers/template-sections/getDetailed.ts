import { startCase } from 'lodash';
import moment from 'moment';

import { ChartType, ChartOptions } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
import { PainMapPerAthleteMeta } from '../maps/getPainMap';
import { TrainingMapMeta } from '../maps/getTrainingMap';

import { DetailedChartMeta } from './types/DetailedChartMeta';
import { DetailedChartMeta2 } from './types/DetailedChartMeta2';


const randomColor = require('randomcolor');

export const getDetailed = (
    trainingEntries: {
        complexChartData: TrainingMapMeta[];
        // groupedByDateAndIntensityLevel: any;
        groupedByDate: Array<{

            countValue: number;
            date: string;
            groupedByDateAndAverageTrainingMinutes: Array<any>;
            groupedByDateAndIntensityLevel: Array<any>;
            highValue: number;
            lowValue: number;
            meanValue: number;
            minutesBySportType: Array<any>;
            theMinutesperIntensityEntries: Array<any>;
            totalValue: number;
        }>
        theMinutesperIntensityEntries
        minutesBySportType,
        stakedChartData: {
            datasets: Array<{
                backgroundColor: string;
                data: Array<number>;

                label: string;
            }>
            labels: Array<string>
        }

        // groupedByDateAndAverageMinutes: any;
    },


    painEntries: {
        complexChartData: Array<PainMapPerAthleteMeta>,
        painLevelPerType: any,
        levelByBodyLocation: any,
        stakedChartData: {
            datasets: Array<{
                backgroundColor: string;
                data: Array<number>;
                label: string;
            }>
            labels: Array<string>
        }
    }
): {
    training: {
        include: boolean;
        data: {
            title: string;
            detailedChartMeta: DetailedChartMeta;
            stackedChartMeta: {
                title: string;
                chartType: ChartType;
                chartOptions: ChartOptions;
                chartLabel?: string;
                chartData: {
                    labels: Array<string>;
                    datasets: Array<{
                        label: string;
                        data: Array<number>;
                        backgroundColor: string;
                    }>;
                };
            };
            polarChartMeta: {
                title: string;
                chartType: ChartType;
                chartOptions: ChartOptions;
                chartData: {
                    labels: string[];
                    datasets: [{
                        "label": "My dataset";
                        "data": number[];
                        "backgroundColor": string[];
                    }];
                };
            };
            pieChartMeta: {
                title: string;
                chartType: ChartType;
                chartOptions: ChartOptions;
                chartData: {
                    labels: string[];
                    datasets: [{
                        "data": number[];
                        "backgroundColor": string[];
                    }];
                };
            };
        };
    };
    "pain": {
        "include": boolean;
        "data": {
            "title": string;
            "detailedChartMeta": DetailedChartMeta2;
            stackedChartMeta: {
                title: string;
                chartType: ChartType;
                chartOptions: ChartOptions;
                chartLabel?: string;
                chartData: {
                    labels: Array<string>;
                    datasets: Array<{
                        label: string;
                        data: Array<number>;
                        backgroundColor: string;
                    }>;
                };
            };
            "polarChartMeta": {
                "title": string;
                "chartType": ChartType;
                "chartOptions": ChartOptions;
                "chartData": {
                    "labels": string[];
                    "datasets": [{
                        "data": number[];
                        "backgroundColor": string[];
                        "label": string;
                    }];
                };
            };
            "pieChartMeta": {
                "title": string;
                "chartType": ChartType;
                "chartOptions": ChartOptions;
                "chartData": {
                    "labels": string[];
                    "datasets": [{
                        "data": number[];
                        "backgroundColor": string[];
                    }];
                };
            };
            "painInjuryDetail": {
                "include": boolean;
                "data": {
                    "athletes": {
                        "name": string;
                        "date": string;
                        "bodyLocation": string;
                        "type": string;
                        "painLevel": number;
                    }[];
                };
            };
        };
    };
} => {






    // const barChartData = {
    //     "series": [2014, 2015, 2016],
    //     "labels": [
    //         "Lille", "Maubeuge", "Poissy SG", "Morlaix", "Sud Gironde", "Elbeuf", "Douarnenez",
    //         "Dieppe", "Le Havre", "Limoges", "Lorient", "Fécamp", "Paris", "Aurillac", "Etampes", "Quimperlé",
    //         "Meulan", "Fougères", "Melun", "Quimper", "Roscoff", "Saint-Malo", "Redon", "La Roche-sur-Yon", "Villefranche"
    //     ],
    //     "data": [
    //         [147, 49, 26, 22, 45, 11, 29, 3, 22, 24, 4, 18, 22, 21, 16, 4, 19, 12, 0, 21, 15, 6, 15, 14, 9],
    //         [130, 58, 59, 49, 20, 37, 20, 40, 30, 19, 33, 34, 20, 11, 16, 21, 8, 15, 24, 5, 7, 12, 7, 8, 8],
    //         [31, 24, 8, 14, 9, 17, 13, 19, 9, 17, 19, 3, 7, 2, 2, 8, 5, 5, 7, 1, 3, 7, 2, 0, 3]
    //     ]
    // };

    // const stackedBarChartData = {
    //     labels: barChartData.labels,
    //     datasets: [{
    //         label: barChartData.series[0],
    //         backgroundColor: "rgba(249, 108, 110, 0.8)",
    //         borderColor: "rgba(249, 108, 110, 1)",
    //         data: barChartData.data[0]
    //     }]
    // };

    // stackedBarChartData.datasets.push({
    //     label: barChartData.series[1],
    //     backgroundColor: "rgba(179, 153, 255, 0.9)",
    //     borderColor: "rgba(179, 153, 255, 1)",
    //     data: barChartData.data[1]
    // });

    // stackedBarChartData.datasets.push({
    //     label: barChartData.series[2],
    //     backgroundColor: "rgba(128, 191, 255, 0.9)",
    //     borderColor: "rgba(128, 191, 255, 1)",
    //     data: barChartData.data[2]
    // });


    const { stakedChartData, minutesBySportType, theMinutesperIntensityEntries } = trainingEntries


    const { stakedChartData: painStakedChartData, levelByBodyLocation, painLevelPerType, complexChartData } = painEntries

    const athletePainEntries = complexChartData[0].entries

    return {
        training: {
            include: true,
            data: {
                title: "Training",

                detailedChartMeta: {
                    title: "Detailed training metrics for the selected Period",
                    charts: getComplexCharts(trainingEntries, painEntries)
                },

                stackedChartMeta: {
                    "title": "Average training minutes by athlete",
                    "chartOptions": ChartOptions.Stacked,
                    "chartLabel": "Minutes per intensity??",
                    "chartType": ChartType.Bar,
                    "chartData": stakedChartData


                },

                polarChartMeta: {
                    "title": "Minutes per intensity! WOOT!",
                    "chartType": ChartType.PolarArea,
                    "chartOptions": ChartOptions.PolarArea,
                    "chartData": {

                        "labels": theMinutesperIntensityEntries.map((entry) => entry.intensityLevel),

                        "datasets": [{
                            "data": theMinutesperIntensityEntries.map((entry) => entry.avg),
                            "backgroundColor": theMinutesperIntensityEntries.map(() => randomColor()),
                            "label": "My dataset"
                        }]
                    }
                },

                pieChartMeta: {
                    "title": "Minutes per spor! WOOT!",
                    "chartType": ChartType.Pie,
                    "chartOptions": ChartOptions.Pie,
                    "chartData": {
                        "labels": minutesBySportType.map((entry) => startCase(entry.sportType)),
                        "datasets": [{
                            "data": minutesBySportType.map((entry) => entry.value),
                            "backgroundColor": minutesBySportType.map(() => randomColor())
                        }]
                    }
                }
            }
        },
        "pain": {
            "include": true,
            "data": {
                "title": "Pain",
                "detailedChartMeta": {
                    "title": "Average pain per athlete!!",
                    "charts": [{
                        "chartTitle": "Swimming",
                        "chartType": ChartType.Bar,
                        "chartOptions": ChartOptions.TrendsMixed,
                        "chartData": {
                            "labels": [
                                "January",
                                "February",
                                "March",
                                "April",
                                "May",
                                "June",
                                "July",
                                "August",
                                "September",
                                "October",
                                "November",
                                "December"
                            ],
                            "datasets": [
                                {
                                    "data": [
                                        50,
                                        30,
                                        60,
                                        70,
                                        80,
                                        90,
                                        95,
                                        70,
                                        90,
                                        20,
                                        60,
                                        95
                                    ],
                                    "type": ChartType.Line,
                                    "label": "Avg Minutes",
                                    "fill": false,
                                    "backgroundColor": "#fff",
                                    "borderColor": "#70cbf4",
                                    "borderCapStyle": "butt",
                                    "borderDash": [],
                                    "borderDashOffset": 0,
                                    "borderJoinStyle": "miter",
                                    "lineTension": 0.3,
                                    "pointBackgroundColor": "#fff",
                                    "pointBorderColor": "#70cbf4",
                                    "pointBorderWidth": 1,
                                    "pointHoverRadius": 5,
                                    "pointHoverBackgroundColor": "#70cbf4",
                                    "pointHoverBorderColor": "#70cbf4",
                                    "pointHoverBorderWidth": 2,
                                    "pointRadius": 4,
                                    "pointHitRadius": 10
                                },
                                {
                                    "data": [
                                        25,
                                        40,
                                        30,
                                        70,
                                        60,
                                        50,
                                        40,
                                        70,
                                        40,
                                        80,
                                        30,
                                        90
                                    ],
                                    "type": ChartType.Line,
                                    "label": "Agv Pain",
                                    "fill": false,
                                    "backgroundColor": "#fff",
                                    "borderColor": "#737373",
                                    "borderCapStyle": "butt",
                                    "borderDash": [
                                        10,
                                        10
                                    ],
                                    "borderDashOffset": 0,
                                    "borderJoinStyle": "miter",
                                    "lineTension": 0.3,
                                    "pointBackgroundColor": "#fff",
                                    "pointBorderColor": "#737373",
                                    "pointBorderWidth": 1,
                                    "pointHoverRadius": 5,
                                    "pointHoverBackgroundColor": "#737373",
                                    "pointHoverBorderColor": "#737373",
                                    "pointHoverBorderWidth": 2,
                                    "pointRadius": 4,
                                    "pointHitRadius": 10
                                },
                                {
                                    "label": "Feeplbe",
                                    "backgroundColor": "#269B5C",
                                    "yAxisID": "bar-y-axis",
                                    "data": [
                                        50,
                                        44,
                                        52,
                                        62,
                                        48,
                                        58,
                                        59,
                                        50,
                                        51,
                                        52,
                                        53,
                                        54
                                    ]
                                },
                                {
                                    "label": "Enthusiastic",
                                    "backgroundColor": "#F0B23C",
                                    "yAxisID": "bar-y-axis",
                                    "data": [
                                        20,
                                        21,
                                        24,
                                        25,
                                        26,
                                        17,
                                        28,
                                        19,
                                        20,
                                        11,
                                        22,
                                        33
                                    ]
                                },
                                {
                                    "label": "Vigorous",
                                    "backgroundColor": "#D74742",
                                    "yAxisID": "bar-y-axis",
                                    "data": [
                                        30,
                                        35,
                                        24,
                                        13,
                                        26,
                                        25,
                                        13,
                                        31,
                                        29,
                                        37,
                                        25,
                                        13
                                    ]
                                }
                            ]
                        }
                    }]
                },
                "stackedChartMeta": {
                    "title": "Average pain per athlete! WOOT",
                    "chartType": ChartType.Bar,
                    "chartOptions": ChartOptions.Stacked,
                    "chartData": painStakedChartData
                },
                "polarChartMeta": {
                    "title": "Pain levels by body location! WOOT!",
                    "chartType": ChartType.PolarArea,
                    "chartOptions": ChartOptions.PolarArea,
                    "chartData": {
                        "labels": levelByBodyLocation.map(levelByLocation => startCase(levelByLocation.bodyLocation)),
                        "datasets": [{
                            "data": levelByBodyLocation.map(levelByLocation => levelByLocation.avg),
                            "backgroundColor": levelByBodyLocation.map(() => randomColor()),
                            // [
                            //     "#FF6384",
                            //     "#4BC0C0",
                            //     "#FFCE56",
                            //     "#E7E9ED",
                            //     "#36A2EB"
                            // ],
                            "label": "My dataset"
                        }]

                    }
                },
                "pieChartMeta": {
                    "title": "Pain levels by type! WOOT",
                    "chartType": ChartType.Pie,
                    "chartOptions": ChartOptions.Pie,
                    "chartData": {
                        "labels": painLevelPerType.map(levelByType => startCase(levelByType.painType)),
                        "datasets": [{
                            "data": painLevelPerType.map(levelByType => levelByType.avg),
                            "backgroundColor": painLevelPerType.map(() => randomColor()) // ["#4688F1", "#D9463D"]
                        }]
                    }
                },
                painInjuryDetail: {
                    "include": true,
                    data: {
                        athletes: athletePainEntries.map((entry) => {
                            return {
                                name: entry.userFullName,
                                date: moment(entry.creationTimestamp).toDate().toDateString(),
                                bodyLocation: startCase(entry.bodyLocation),
                                type: startCase(entry.painType),
                                painLevel: entry.value
                            }
                        })
                    }
                }
            }
        }
    };
};
function getComplexCharts(
    trainingEntries: {
        complexChartData: TrainingMapMeta[];
        // groupedByDateAndIntensityLevel: [{
        //     countValue: number;
        //     date: string;
        //     groupedByDateAndIntensityLevel: [{
        //         avg: number;
        //         countValue: number;
        //         firstName: string;
        //         fullName: string;
        //         highValue: number;
        //         intensityLevel: string;
        //         lowValue: number;
        //         meanValue: number;
        //         totalValue: number;
        //         uid: string;
        //     }]
        //     highValue: number;
        //     lowValue: number;
        //     meanValue: number;
        //     totalValue: number;
        // }];
        groupedByDate: Array<{

            countValue: number;
            date: string;
            groupedByDateAndAverageTrainingMinutes: Array<any>;
            groupedByDateAndIntensityLevel: Array<any>;
            highValue: number;
            lowValue: number;
            meanValue: number;
            minutesBySportType: Array<any>;
            theMinutesperIntensityEntries: Array<any>;
            totalValue: number;
        }>
        minutesBySportType,
        theMinutesperIntensityEntries
        // groupedByDateAndAverageMinutes: any;
    },
    painEntries: {
        complexChartData: Array<PainMapPerAthleteMeta>,
        painLevelPerType: any,
        levelByBodyLocation: any
    }

): Array<{
    chartTitle: string;
    chartType: ChartType;
    chartOptions: ChartOptions;
    chartLabel?: string;
    chartData: {
        labels: string[];
        datasets: Array<{
            label: string;
            type?: ChartType;
            data: number[];
            fill?: boolean;
            backgroundColor: string;
            borderColor?: string;
            borderCapStyle?: string;
            borderDash?: number[];
            borderDashOffset?: number;
            borderJoinStyle?: string;
            lineTension?: number;
            pointBackgroundColor?: string;
            pointBorderColor?: string;
            pointBorderWidth?: number;
            pointHoverRadius?: number;
            pointHoverBackgroundColor?: string;
            pointHoverBorderColor?: string;
            pointHoverBorderWidth?: number;
            pointRadius?: number;
            pointHitRadius?: number;
            yAxisID?: string;
        }>;
    };
}> {

    const { groupedByDate, complexChartData: traininComplexChartData } = trainingEntries

    const intensityLevelEntreies = []
    traininComplexChartData[0].groupedByDate.forEach((entry) => {

        entry.groupedByDateAndIntensityLevel.forEach((level) => {

            intensityLevelEntreies.push(level)
        })
    })

    const groupedAthleteBiometricEntryDocs = groupByMap(intensityLevelEntreies, (entry: any) => entry.intensityLevel) as Map<string, Array<any>>;


    const intensityLevelCharData = []

    groupedAthleteBiometricEntryDocs.forEach((items, key) => {

        intensityLevelCharData.push({
            intensityLevel: key,
            values: items.map(item => item.avg)
        })
        console.log(items, key)
    })

    // groupedAthleteBiometricEntryDocs.map((ad) => ad.groupedByDateAndIntensityLevel.map((entry) => {
    //     return {
    //         intensityLevel: entry.intensityLevel,
    //         value: entry.value
    //     }
    // }))[0]

    const { complexChartData: painComplexChartData } = painEntries

    const painLevels = painComplexChartData.map((ad) => ad.groupedByDateAndAverageMinutes.entries.map((entry) => {
        return entry.meanValue
    }))[0]


    // const theEntries = trainingEntries.complexChartData

    // theEntries.map((entry) => {
    // })

    // const res = groupedByDate.map((entries) => {



    const avgTrainingMinutesValues = groupedByDate.map((theTRainingData) => theTRainingData.meanValue)

    const labels = groupedByDate.map((theTRainingData) => moment(theTRainingData.date).toDate().toDateString())

    // const vigorousData = groupedByDateAndIntensityLevel[0].entries.map((entry) => {

    //     const sdsd = entry //.groupedByDateAndIntensityLevel.
    //     return entry
    // })


    // const intensityLevel = grouped.groupedByDateAndIntensityLevel[0].intensityLevel
    return [{
        "chartTitle": "Swimming",
        "chartType": ChartType.Bar,
        "chartOptions": ChartOptions.TrendsMixed,
        "chartLabel": "Average training minutes by athlete??",
        "chartData": {
            labels,
            "datasets": [
                {
                    label: "Avg Minutes",
                    yAxisID: "agvMinutes",
                    data: avgTrainingMinutesValues,
                    type: ChartType.Line,
                    "fill": false,
                    "backgroundColor": "#fff",
                    "borderColor": "#70cbf4",
                    "borderCapStyle": "butt",
                    "borderDash": [],
                    "borderDashOffset": 0,
                    "borderJoinStyle": "miter",
                    "lineTension": 0.3,
                    "pointBackgroundColor": "#fff",
                    "pointBorderColor": "#70cbf4",
                    "pointBorderWidth": 1,
                    "pointHoverRadius": 5,
                    "pointHoverBackgroundColor": "#70cbf4",
                    "pointHoverBorderColor": "#70cbf4",
                    "pointHoverBorderWidth": 2,
                    "pointRadius": 4,
                    "pointHitRadius": 10
                },
                {
                    label: "Agv Pain",
                    yAxisID: "agvPain",
                    data: painLevels,
                    "type": ChartType.Line,
                    "fill": false,
                    "backgroundColor": "#fff",
                    "borderColor": "#737373",
                    "borderCapStyle": "butt",
                    "borderDash": [
                        10,
                        10
                    ],
                    "borderDashOffset": 0,
                    "borderJoinStyle": "miter",
                    "lineTension": 0.3,
                    "pointBackgroundColor": "#fff",
                    "pointBorderColor": "#737373",
                    "pointBorderWidth": 1,
                    "pointHoverRadius": 5,
                    "pointHoverBackgroundColor": "#737373",
                    "pointHoverBorderColor": "#737373",
                    "pointHoverBorderWidth": 2,
                    "pointRadius": 4,
                    "pointHitRadius": 10
                },
                {
                    label: intensityLevelCharData[0].intensityLevel,
                    data: intensityLevelCharData[0].values,
                    "backgroundColor": "#269B5C",
                    "yAxisID": "bar-y-axis"
                },
                {
                    label: intensityLevelCharData[1].intensityLevel,
                    data: intensityLevelCharData[1].values,
                    "backgroundColor": "#F0B23C",
                    "yAxisID": "bar-y-axis"
                },
                {
                    label: intensityLevelCharData[2].intensityLevel,
                    data: intensityLevelCharData[2].values,
                    "backgroundColor": "#D74742",
                    "yAxisID": "bar-y-axis"
                }
            ]
        }
    }]

}

