import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { sum } from 'lodash';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
import moment from 'moment';

export const getTrendsLineChartData = (
    athleteTrendTypeEntries: BiometricEntrySchema[],
    date: Date,
    athleteID: string,
    athleteFullName: string,
    startOfWeek,
    daysOfWeek: {
        date: Date;
        uid?: string;
        lineChartData: any;
        entries: BiometricEntrySchema[];
    }[], lineChartDatasets = []) => {

    const setAllToToday = false;


    // const groupedMoodEntriesByAthlete = groupByMap(athleteTrendTypeEntries, (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID);
    const groupedMoodEntriesByDate = groupByMap(athleteTrendTypeEntries, (entry: BiometricEntrySchema) => (<any>entry).date);
    // const groupedDaysOfWeek = groupByMap(daysOfWeek, (day: {
    //     date: Date;
    //     uid?: string;
    //     lineChartData: any[];
    //     entries: BiometricEntrySchema[];
    // }) => day.date);


    const chartData: Array<{
        date: Date,
        theData: number
    }> = []

    // groupedDaysOfWeek.forEach((days, date: Date) => {

    // const day = days[0]

    // const userDayEntries = groupedMoodEntriesByDate.get(date)
    // const userEntries = entries;
    // const dayEntries = entries

    const dailyValues = (athleteTrendTypeEntries || []).map((entry) => entry.value || 0) || [];
    const theData = sum(dailyValues)



    chartData.push({
        date,
        theData: theData || 0
    })
    // })


    // const data = daysOfWeek.map((day) => {
    //     // const userEntries = day.entries.filter((entry) => entry.athleteID === athleteID);
    //     const dayEntries = athleteTrendTypeEntries.filter((entry) => {

    //         return day.date === (<any>entry).date;
    //     })

    //     const dailyValues = dayEntries.map((entry) => entry.value || 0) || [];
    //     const theData = sum(dailyValues)

    //     return theData || 0
    // })
    // 0:48
    // 1:56
    // 2:744
    // 3:0
    // 4:144
    // 5:908
    // 6:0



    // const labels = daysOfWeek.map((day) => day.date.toDateString())
    // const colour = athleteColors.find((athColour) => athColour.uid === athleteID).colour;
    // const athleteDataset = {
    //     uid: athleteID,
    //     data: chartData.map((cd) => cd.theData),
    //     label: athleteFullName,
    //     borderColor: colour,
    //     fill: false,
    //     borderJoinStyle: "round"
    // };
    // lineChartDatasets.push(athleteDataset);
    // const lineChartData = {
    //     chartType: ChartType.Line,
    //     chartData: {
    //         labels,
    //         datasets: lineChartDatasets,
    //         uid: athleteID,
    //     }
    // }

    return theData || 0 //lineChartData;

};
