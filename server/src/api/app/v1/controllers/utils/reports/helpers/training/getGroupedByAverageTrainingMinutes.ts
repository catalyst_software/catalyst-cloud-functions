import { sum } from 'lodash';

import { BiometricTrainingEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';

export const getGroupedByAverageTrainingMinutes = (athlteteTrainingEntries: BiometricTrainingEntrySchema[]) => {

    const totalTrainingMinutesCount = sum(athlteteTrainingEntries.map((entry) => entry.value));
    
    const theAvgTrainingMinutesByAthleteEntries = [];

    const groupedAthlete = groupByMap(athlteteTrainingEntries, (entry: BiometricTrainingEntrySchema) => entry.athleteID) as Map<string, Array<BiometricTrainingEntrySchema>>;

    groupedAthlete.forEach((athleteEntries, athleteUID) => {
        const res = {
            athleteID: athleteUID,
            name: athleteEntries[0].userFullName,
            value: sum(athleteEntries.filter((entry) => entry.athleteID === athleteUID).map((theEntry) => {
                return theEntry.value
            })) / totalTrainingMinutesCount * 100
        };
        theAvgTrainingMinutesByAthleteEntries.push(res);
    });

    removeUndefinedProps(theAvgTrainingMinutesByAthleteEntries)

    return theAvgTrainingMinutesByAthleteEntries;


};

