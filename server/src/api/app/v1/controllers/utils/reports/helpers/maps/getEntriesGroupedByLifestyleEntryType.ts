
import { getAggregateValuesMap, MoodMapMeta } from './getMoodMap';

import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { LifestyleEntryType } from '../../../../../../../../models/enums/lifestyle-entry-type';


export const getEntriesGroupedByLifestyleEntryType = (
    dailyGroupedByTypeEntries: Map<string, Array<BiometricEntrySchema>>,
    groupedByAthletUIDs: Map<string, BiometricEntrySchema[]>,
    dayDate: Date): Array<{
    entryType: string;
    dailyEntryTypeEntriesMap: MoodMapMeta;
}> => {


    const allEntries = []


    const lifestyleEntryTypekeys = Object.keys(LifestyleEntryType).filter(
        k => typeof LifestyleEntryType[k as any] === 'string' &&
            LifestyleEntryType[k as any] !== LifestyleEntryType[LifestyleEntryType.IpScore] &&
            LifestyleEntryType[k as any] !== LifestyleEntryType[LifestyleEntryType.Steps] &&
            LifestyleEntryType[k as any] !== LifestyleEntryType[LifestyleEntryType.Distance] &&
            LifestyleEntryType[k as any] !== LifestyleEntryType[LifestyleEntryType.HeartRate] &&
            LifestyleEntryType[k as any] !== LifestyleEntryType[LifestyleEntryType.Video] &&
            LifestyleEntryType[k as any] !== LifestyleEntryType[LifestyleEntryType.Survey] &&
            LifestyleEntryType[k as any] !== LifestyleEntryType[LifestyleEntryType.Worksheet]

    )
    // ["Mood", "Pain", ....]

    lifestyleEntryTypekeys.forEach(entryTypeNumner => {
        // Mood
        // LogInfo(LifestyleEntryType[entryType])

        const entryType = LifestyleEntryType[entryTypeNumner]
        const allDailyTypeEntries = dailyGroupedByTypeEntries.get(entryType) || [];
        const dailyEntryTypeEntriesMap = getAggregateValuesMap(allDailyTypeEntries, dayDate);
        dailyEntryTypeEntriesMap.date = dayDate


        const dailyEntries = {
            entryType,
            dailyEntryTypeEntriesMap
        }

        allEntries.push(
            dailyEntries
        )
    })

    return allEntries;
    
    // // BRAIN

    // // MOOD Entries
    // const allDailyMoodEntries = dailyGroupedByTypeEntries.get('Mood') || [];
    // const dailyMoodEntryTypeEntriesMap = getAggregateValuesMap(allDailyMoodEntries, dayDate);
    // dailyMoodEntryTypeEntriesMap.date = dayDate

    // // SLEEP Entries
    // const allDailySleepEntries = dailyGroupedByTypeEntries.get('Sleep') || [];
    // const dailySleepEntryTypeEntriesMap = getAggregateValuesMap(allDailySleepEntries, dayDate);
    // dailySleepEntryTypeEntriesMap.date = dayDate

    // // BODY

    // // FATIGUE Entries

    // const allDailyFatigueEntries = dailyGroupedByTypeEntries.get('Fatigue') || [];
    // const dailyFatigueEntryTypeEntriesMap = getAggregateValuesMap(allDailyFatigueEntries, dayDate);
    // dailyFatigueEntryTypeEntriesMap.date = dayDate

    // // Pain Entries

    // const allDailyPainEntries = dailyGroupedByTypeEntries.get('Pain') || [];
    // const dailyPainEntryTypeEntriesMap = getAggregateValuesMap(allDailyPainEntries, dayDate);
    // dailyFatigueEntryTypeEntriesMap.date = dayDate

    // return {
    //     moodEntries: dailyMoodEntryTypeEntriesMap,
    //     sleepEntries: dailySleepEntryTypeEntriesMap,
    //     fatigueEntries: dailyFatigueEntryTypeEntriesMap
    // };
};
