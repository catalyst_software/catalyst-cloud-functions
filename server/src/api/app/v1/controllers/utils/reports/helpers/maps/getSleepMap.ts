import moment from 'moment';

import { ChartType } from './../../../../../../../../pubsub/interfaces/red-flag-reports';
import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
import { sortByValue, sortByValueDesc } from "../../sortByHelpers";
import { mapScalarEntities } from "./mapScalarEntities";
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { getMeanOfValue } from '../../getMeanOfValue';
import { getLowestValue } from '../../getLowestValue';
import { getHighestValue } from '../../getHighestValue';
import { getSumOfValue } from '../../getSumOfValue';
import { getEntriesIndicator } from '../../getEntriesIndicator';
import { athleteColors } from '../../composeReportData';
import { getTrendsLineChartData } from './getTrendsLineChartData';

// function getPainEntries(painEntries: Array<BiometricEntrySchema>): number {
//     return painEntries.length || 0;
// }

export type SleepMapMeta = {
    uid?: string;
    entries: Array<BiometricEntrySchema>;
    lineChartData: {
        chartType: ChartType;
        chartData: {
            labels: Array<string>;
            datasets: Array<any>;
        };
    };
    barChartData: {
        chartType: ChartType;
        chartData: {
            labels: Array<string>;
            datasets: Array<{
                data: Array<any>;
                backgroundColor: string[];
            }>
        };
    };
    lowValue: number;
    highValue: number;
    meanValue: number;
    totalValue: number;
    countValue: number;

};

// export const getSleepMap = (entries: Array<BiometricEntrySchema>): Array<BiometricScalarEntrySchema> => {
export const getSleepMap = (
    entries: Array<BiometricEntrySchema>,
    date: Date,
    setAllToToday: boolean,
    startOfWeek: moment.Moment,
    daysOfWeek: {
        date: Date;
        uid?: string;
        lineChartData: any;
        entries: BiometricEntrySchema[]
    }[]): SleepMapMeta => {

    const groupedEntriesByAthlete = groupByMap(entries.sort(sortByValue), (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID);

    groupedEntriesByAthlete.forEach((athleteEntries) => {
        mapScalarEntities(athleteEntries);
    });

    // const theSleepEntries: Array<SleepMapMeta> = [];

    const lineChartDatasets = []
    const barChartDatasets = []

    groupedEntriesByAthlete.forEach((athleteEntries: Array<BiometricEntrySchema>, uid: string) => {

        const athleteFullName = uid.split('__')[0]
        const athleteID = uid.split('__')[1]

        // Line Chart Data

        getTrendsLineChartData(athleteEntries, date, athleteID, athleteFullName, startOfWeek, daysOfWeek, lineChartDatasets);

        // Bar Chart Data
        // getTrendsBarChartData(athleteEntries, athleteID, athleteFullName, barChartDatasets);
    })

    removeUndefinedProps(entries);

    const meanValue = getMeanOfValue(entries);
    const lowValue = getLowestValue(entries);
    const highValue = getHighestValue(entries);
    const totalValue = getSumOfValue(entries);
    const countValue = entries.length;

    const labels = daysOfWeek.map((day) => day.date.toDateString())
    const colours = barChartDatasets.sort(sortByValueDesc).map((chartData) => athleteColors.find((athColour) => athColour.uid === chartData.uid).colour)

    const lineChart = {
        chartType: ChartType.Line,
        chartData: {
            labels,
            datasets: lineChartDatasets
        }
    }

    const barChart = {
        chartType: ChartType.HorizontalBar,
        chartData: {
            labels: barChartDatasets.sort(sortByValueDesc).map((chartData) => chartData.label),
            // ["Ian Kaapi", "Annie Flamsteed", "Travis Brannon", "Ayva Alter", "John Askew", "Luke Skywalker", "Pierce Brosnan", "Anke Shake", "John Snow", "Jake Butler"],
            datasets: [{
                data: barChartDatasets.sort(sortByValueDesc).map((chartData) => chartData.value),
                // [2.9, 4, 4, 5, 4, 4, 2.9, 4, 5, 4],
                backgroundColor: colours,
                // ["#e6e600", "#68838F", "#00ff00", "#C42963", "#ff66ff", "#4dd2ff", "#007399", "#3333ff", "#ff9900", "#88cc00"]
            }]
        }
    }

    const mappedData: SleepMapMeta = {
        lowValue,
        highValue,
        meanValue,
        totalValue,
        countValue,
        lineChartData: lineChart,
        barChartData: barChart,
        entries: getEntriesIndicator(entries)
            .sort(sortByValue)

    }

    removeUndefinedProps(mappedData)

    return mappedData;
};
