import moment from 'moment';
import { from, of, zip } from 'rxjs';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { toArray } from 'rxjs/internal/operators/toArray';

import { BiometricEntrySchema, BiometricFoodEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { sortByValue } from "../../sortByHelpers";
import { getMeanOfValue } from "../../getMeanOfValue";
import { getLowestValue } from '../../getLowestValue';
import { getHighestValue } from '../../getHighestValue';

export const getFoodMap = (entries: Array<BiometricEntrySchema>, setAllToToday: boolean, startOfWeek: moment.Moment,
    // daysOfWeek: {
    //     date: Date;
    //     uid?: string;
    //     lineChartData: any;
    //     entries: BiometricEntrySchema[]
    // }[]
): Array<BiometricFoodEntrySchema> => {

    const theEntries: Array<BiometricFoodEntrySchema> = [];

    if (entries) {
        from(entries
            // .map((entry) => {
            //     const defaultValues = getDefaultValues(entry)
            //     return {
            //         ...defaultValues,
            //         foodType: entry.foodType,
            //         mealType: entry.mealType,
            //         name: entry.userFullName,
            //         value: entry.value
            //     };
            // })
        )
            .pipe(groupBy(person => person.athleteID, p => p), mergeMap(group2 => zip(of(group2.key), group2.pipe(toArray())))
                // take(1)
            )
            .subscribe((grouped) => {
                // console.log(grouped);
                const foodEntries = grouped[1] as Array<BiometricFoodEntrySchema>;
                const meanValue = getMeanOfValue(foodEntries);
                const lowValue = getLowestValue(foodEntries);
                const highValue = getHighestValue(foodEntries);

                foodEntries.forEach((entry) => {
                    removeUndefinedProps(entry);
                    entry.lowValue = lowValue;
                    entry.highValue = highValue;
                    entry.meanValue = meanValue;
                    entry.countValue = foodEntries.length;
                });
                theEntries.push(...foodEntries);
            });
    } else {
        console.log('No Food Entries Found')
    }

    return theEntries.sort(sortByValue);
    // return foodEntries
    //     .map((entry) => {
    //         const defaultValues = getDefaultValues(entry)
    //         return {
    //             ...defaultValues,
    //             foodType: entry.foodType,
    //             mealType: entry.mealType,
    //             name: entry.userFullName,
    //             value: entry.value
    //         };
    //     }).sort(sortByValue)
};
