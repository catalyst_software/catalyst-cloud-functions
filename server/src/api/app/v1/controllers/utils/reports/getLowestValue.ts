import { BiometricEntrySchema } from '../../../../../../pubsub/interfaces/red-flag-reports';

export const getLowestValue = (athlteteEntries: BiometricEntrySchema[]) => {
    return athlteteEntries && athlteteEntries.length ? athlteteEntries.reduce((prev, current) => (prev.value < current.value) ? prev : current).value : 0;
};
