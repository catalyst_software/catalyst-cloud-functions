import moment from 'moment';

import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';

export const initDaysOfWeek = (
    daysOfWeek: {
        date: Date;
        uid?: string;
        lineChartData: any;
        entries: BiometricEntrySchema[];
    }[], startDate: moment.Moment,
    toDate: moment.Moment,
    groupedEntries: Map<string, BiometricEntrySchema[]>,
    utcOffset: number) => {

    const endOf = moment(toDate).clone().endOf('day');
    const startOf = moment(startDate).clone().startOf('day');
    const dayCount = endOf.clone().diff(startOf, 'days');

    for (let index = 0; index < dayCount; index++) {
        const theDay = startOf.clone().startOf('day').utcOffset(utcOffset || 0).add(index, 'day').format("YYYYMMDD")

        const dateString = theDay;

        const dayEntries = groupedEntries.get(dateString);

        const theRes = {
            date: moment(theDay).toDate(),
            uid: undefined,
            lineChartData: [],
            entries: dayEntries || []
        }
        daysOfWeek.push(theRes);
    }

    return startOf;
};
