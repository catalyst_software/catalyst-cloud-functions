import { from, of, zip } from 'rxjs';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { toArray } from 'rxjs/internal/operators/toArray';
import { sum } from 'lodash';

import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { SimpleMealClassificationData } from '../../types/training-trend-data';
import { getAggregateValues } from '../../getAggregateValues';
import { SimpleFoodMealClassificationType } from '../../../../../../../../models/enums/enums.model';

export const getGroupedBySimpleMealClassificationType = (athlteteSimpleMealEntries: BiometricEntrySchema[]) => {

    const mealClassificationData: SimpleMealClassificationData = [];


    const simpleMealClassificationTypeKeys = Object.keys(SimpleFoodMealClassificationType).filter(
        k => typeof SimpleFoodMealClassificationType[k as any] === 'string'
    ).map((name) => SimpleFoodMealClassificationType[name])
    // ["Unspecified", "Skipped", "Unhealthy", "Healthy"]

    simpleMealClassificationTypeKeys.forEach(clasificationType => {
        // Light
        console.log(clasificationType)

        mealClassificationData.push({
            classificationType: clasificationType,
            meanValue: 0,
            lowValue: 0,
            highValue: 0,
            totalValue: 0,
            avg: 0
        })

    })


    from(athlteteSimpleMealEntries)
        .pipe(
            // TODO: Group By Classification Type here?
            groupBy(entry => entry.value),
            mergeMap(intensityLevelGroup => zip(of(intensityLevelGroup.key), intensityLevelGroup.pipe(toArray())))
        )
        .subscribe((groupedByMealClassification) => {

            const classificationType = SimpleFoodMealClassificationType[groupedByMealClassification[0]];
            const groupedEntries = groupedByMealClassification[1];

            const mealClassificationValues = {

                classificationType,
                ...getAggregateValues(groupedEntries),

                // uid: athlteteTrainingEntries[0].athleteID,
                // firstName: athlteteTrainingEntries[0].userFirstName,
                // fullName: athlteteTrainingEntries[0].userFullName
            }

            const classificationDataIndex = mealClassificationData.findIndex((levle) => levle.classificationType === classificationType)

            mealClassificationData[classificationDataIndex] = {
                ...mealClassificationData[classificationDataIndex],
                ...mealClassificationValues
            }

            // intensityLevelData.push(intenisityLevelValues);
        });

    const totalCount = sum(mealClassificationData.map((level) => level.totalValue));


    mealClassificationData.map((mealClassificationGroup) => {
        const avgOutOfHundred = mealClassificationGroup.totalValue / totalCount * 100;
        mealClassificationGroup.avg = avgOutOfHundred;
    });

    return mealClassificationData
};
