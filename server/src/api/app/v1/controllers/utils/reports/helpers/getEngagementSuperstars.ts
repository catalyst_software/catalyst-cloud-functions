import { BiometricEntrySchema } from '../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../shared/utils/groupByMap';
import { sortByValueDesc } from "../sortByHelpers";

export const getEngagementSuperstars = (biometricEntries: Array<BiometricEntrySchema>): Array<{
    uid: string;
    name: string;
    value: number;
}> => {
    // const entries = biometricEntries.filter((entry) =>
    //     entry.value < 4) || [];
    const theEntries: Array<{
        uid: string;
        name: string;
        value: number;
    }> = [];
    const groupedEntries = groupByMap(biometricEntries, (entry: BiometricEntrySchema) => entry.userFirstName + '__' + entry.athleteID) as Map<string, Array<BiometricEntrySchema>>;
    groupedEntries.forEach((athleteEntries, kk) => {
        const res = {
            uid: athleteEntries[0].athleteID,
            name: athleteEntries[0].userFullName,
            value: athleteEntries.length
        };
        theEntries.push(res);
    });
    return theEntries.sort(sortByValueDesc);
};
