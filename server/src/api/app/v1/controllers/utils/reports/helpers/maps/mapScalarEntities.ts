import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { getMeanOfValue } from "../../getMeanOfValue";
import { getHighestValue } from '../../getHighestValue';
import { getLowestValue } from '../../getLowestValue';
import { getSumOfValue } from '../../getSumOfValue';

export const mapScalarEntities = (athleteDailyTypeEntries: BiometricEntrySchema[]) => {

    const meanValue = getMeanOfValue(athleteDailyTypeEntries);
    const lowValue = getLowestValue(athleteDailyTypeEntries);
    const highValue = getHighestValue(athleteDailyTypeEntries);
    const totalValue = getSumOfValue(athleteDailyTypeEntries);
    const countValue = athleteDailyTypeEntries.length;

    athleteDailyTypeEntries.forEach((entry) => {
        removeUndefinedProps(entry);
        entry.lowValue = lowValue;
        entry.highValue = highValue;
        entry.meanValue = meanValue;
        entry.totalValue = totalValue;
        entry.countValue = countValue;
    });
    
};
