import { BiometricEntrySchema } from '../../../../../../pubsub/interfaces/red-flag-reports';
import { removeUndefinedProps } from '../remove-undefined-props';

export const getEntriesIndicator = (entries: BiometricEntrySchema[]): BiometricEntrySchema[] => {

    return entries.map((entry) => {

        const { 
            // organizationID, groupID,
             groupIDs, bodyLocationID, lifestyleEntryTypeID, painTypeID, analysticsFamily, eventName, ...theRest } = entry
        
        removeUndefinedProps(theRest)
        
        return theRest;
    })
    
};
