import { BiometricEntrySchema } from '../../../../../../pubsub/interfaces/red-flag-reports';
import { removeUndefinedProps } from '../../../../../../db/ipscore/services/save-aggregates';

export const getDefaultValues = (entry: BiometricEntrySchema): BiometricEntrySchema => {
    const test1 = JSON.parse(JSON.stringify(entry));
    removeUndefinedProps(entry);
    const defaultValues: BiometricEntrySchema = {
        ...entry
    };
    // grouped[1] as Array<BiometricScalarEntrySchema>;
    return defaultValues;
};
