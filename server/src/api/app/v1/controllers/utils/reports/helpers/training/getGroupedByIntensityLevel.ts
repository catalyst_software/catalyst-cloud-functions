import { from, of, zip } from 'rxjs';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { toArray } from 'rxjs/internal/operators/toArray';
import { sum } from 'lodash';

import { BiometricTrainingEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { TrainingTrendData } from '../../types/training-trend-data';
import { getAggregateValues } from '../../getAggregateValues';
import { IntensityLevelType } from '../../../../../../../../models/enums/enums.model';

export const getGroupedByIntensityLevel = (athlteteTrainingEntries: BiometricTrainingEntrySchema[]) => {

    const intensityLevelData: TrainingTrendData = [];


    const intensityLevelTypeKeys = Object.keys(IntensityLevelType).filter(
        k => typeof IntensityLevelType[k as any] === 'string'
    ).map((name) => IntensityLevelType[name])
    // ["Light", "Moderate", "Vigorous", ....]

    intensityLevelTypeKeys.forEach(intensityLevel => {
        // Light
        console.log(intensityLevel)

        intensityLevelData.push({
            intensityLevel,
            meanValue: 0,
            lowValue: 0,
            highValue: 0,
            totalValue: 0,
            avg: 0
        })

    })


    from(athlteteTrainingEntries)
        .pipe(
            groupBy(entry => entry.intensityLevel),
            mergeMap(intensityLevelGroup => zip(of(intensityLevelGroup.key), intensityLevelGroup.pipe(toArray())))
        )
        .subscribe((groupedByIntensityLevel) => {

            const intensityLevel = groupedByIntensityLevel[0];
            const groupedEntries = groupedByIntensityLevel[1];

            const intenisityLevelValues = {

                intensityLevel,
                ...getAggregateValues(groupedEntries),

                // uid: athlteteTrainingEntries[0].athleteID,
                // firstName: athlteteTrainingEntries[0].userFirstName,
                // fullName: athlteteTrainingEntries[0].userFullName
            }

            const levelDataIndex = intensityLevelData.findIndex((levle) => levle.intensityLevel === intensityLevel)

            intensityLevelData[levelDataIndex] = {
                ...intensityLevelData[levelDataIndex],
                ...intenisityLevelValues
            }

            // intensityLevelData.push(intenisityLevelValues);
        });


    const totalCount = sum(intensityLevelData.map((level) => level.totalValue));


    intensityLevelData.map((intesityGroup) => {
        const avgOutOfHundred = intesityGroup.totalValue / totalCount * 100;
        intesityGroup.avg = avgOutOfHundred;
    });

    return intensityLevelData
};
