import { mean } from 'lodash';

import { BiometricEntrySchema } from '../../../../../../pubsub/interfaces/red-flag-reports';

export const getMeanOfValue = (entries: Array<BiometricEntrySchema>) => {

    const theValues = entries.map((entry) => {
        let theValue = 0;
        if (entry.value !== undefined) {
            theValue = !isNaN(entry.value) ? entry.value | 0 : 0;
        }
        else {
            debugger;
            entry.value = 0
            theValue = 0
        }
        return theValue;
    });

    
    const theMean = mean(theValues);
    return !isNaN(theMean)
        ? theMean || 0
        : 0;
};
