import moment from 'moment';

import { RedFlagReportSchema, BiometricEntrySchema } from '../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../shared/utils/groupByMap';
import { getEntriesGroupedByLifestyleEntryType } from './helpers/maps/getEntriesGroupedByLifestyleEntryType';
import { ReportEntity } from '../../../../../../models/report-model';
import { getTrendsLineChartData } from './helpers/maps/getTrendsLineChartData';
import { MoodMapMeta } from './helpers/maps/getMoodMap';
import { LifestyleEntryType } from '../../../../../../models/enums/lifestyle-entry-type';

import * as  faker from 'faker';

// export const getLowestValue = (theEntries: { bodyLocation: string; bodyLocationID: number; painType: string; painTypeID: number; name: string; level: number; }[]) => {
//     return theEntries.reduce((prev, current) => (prev.level > current.level) ? prev : current);
// }

// export const getHighestValue = (theEntries: { bodyLocation: string; bodyLocationID: number; painType: string; painTypeID: number; name: string; level: number; }[]) => {
//     return theEntries.reduce((prev, current) => (prev.level < current.level) ? prev : current);
// }


export const athleteColors: Array<{
    uid: string,
    colour: string
}> = []

const randomColor = require('randomcolor');

export const composeReportData = (biometricEntries: BiometricEntrySchema[], entity: ReportEntity, setAllToToday: boolean, startOfWeek: moment.Moment, toDate: moment.Moment, daysOfWeek: {
    date: Date;
    uid?: string;
    lineChartData: any;
    entries: BiometricEntrySchema[]
}[]) => {



    const groupedByAthletUIDs = groupByMap(biometricEntries, (entry: BiometricEntrySchema) => entry.athleteID) as Map<string, BiometricEntrySchema[]>;


    groupedByAthletUIDs.forEach((entries, uid) => {
        const gender = Math.round(Math.random());
        const firstName = faker.name.firstName(gender);
        const lastName = faker.name.lastName(gender);
        entries.forEach((e) => {
            e.userFirstName = firstName;
            e.userFullName = firstName + ' ' + lastName
        })

        const colour = randomColor();

        athleteColors.push({ uid, colour })

    })
    let redFlagTemplatedata, superstarsTemplatedata;
    const trendsTemplatedataArray = [];
    const detailedTemplatedataArray = [];
    const theMoodEntries: Array<MoodMapMeta> = [];
    const allEntries: Array<{
        entryType: string;
        dailyEntryTypeEntriesMap: MoodMapMeta;
    }[]> = [];
    const theSleepEntries: Array<MoodMapMeta> = [];



    daysOfWeek.forEach((day) => {

        let entries
            //  {
            //     // redFlagTemplatedata: r1,
            //     // superstarsTemplatedata: r2,
            //     moodEntries,
            //     sleepEntries
            // }
            // trendsTemplatedata,
            // detailedTemplatedata }
            = mapAggregateData(day.entries, groupedByAthletUIDs, day.date);

        // redFlagTemplatedata = r1;
        // superstarsTemplatedata = r2;
        // trendsTemplatedataArray.push(trendsTemplatedata);
        // detailedTemplatedataArray.push(detailedTemplatedata);
        entries.map((entr) => {

            const typeEntries = allEntries[entr.entryType];

            if (typeEntries) {
                typeEntries.push(entr.dailyEntryTypeEntriesMap)
            } else {
                allEntries[entr.entryType] = [entr.dailyEntryTypeEntriesMap]
            }
            // allEntries[entr.entryType].push(entr.dailyEntryTypeEntriesMap)
        })


        theMoodEntries.push(getTypeEntries(entries, LifestyleEntryType.Mood));
        theSleepEntries.push(getTypeEntries(entries, LifestyleEntryType.Sleep));
        // moodEntries.entries.map(())
    })


    const lifestyleEntryTypekeys = Object.keys(allEntries)
    // ["Mood", "Pain", ....]

    const allEntriesAmal = []
    lifestyleEntryTypekeys.forEach((entryTypeByName) => {
        const daylyEntriesByType = allEntries[entryTypeByName]
        daylyEntriesByType.forEach((ent) => {

            allEntriesAmal.push(...ent.entries)

        })
    })


    const groupedMoodEntriesByAthlete = groupByMap([].concat(...theMoodEntries.map((d) => d.entries)), (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID);

    const lineChartDatasets = []
    // const groupedMoodEntriesByAthlete = groupByMap(theMoodEntries, (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID);
    getChartData(groupedMoodEntriesByAthlete, daysOfWeek, startOfWeek, lineChartDatasets);


    const groupedSleepEntriesByAthlete = groupByMap([].concat(...theSleepEntries.map((d) => d.entries)), (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID);

    const sleepLineChartDatasets = []
    // const groupedMoodEntriesByAthlete = groupByMap(theMoodEntries, (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID);
    getChartData(groupedSleepEntriesByAthlete, daysOfWeek, startOfWeek, sleepLineChartDatasets);





    // const userEntries =
    //     theMoodEntries
    //         .sort(sortByDate)
    //         .map((data) => data.entries);
    // .map((entry) => {
    //     const theDate = {
    //         ...entry,
    //         // creationTimestamp: moment(entry.creationTimestamp).toDate()
    //     };
    //     return theDate;

    // })
    // .sort(sortByCreationTimestamp));



    const theData: RedFlagReportSchema = {
        "header": {
            "fromDate": "Apr, 2019",
            "toDate": "Apr 21, 2019",
            "headerType": "athlete",
            "title": {
                "organization": "CARMEL COLLEGE",
                "team": "The Rebels",
                "group": "Group 1",
                "athlete": ""
            }
        },
        "redFlags": redFlagTemplatedata,
        "superstars": superstarsTemplatedata,
        "trends": trendsTemplatedataArray,
        "detailed": detailedTemplatedataArray as any,
        daysOfWeek
    };






    // const dummyReportData = reportData

    return theData;
};
function getTypeEntries(entries: { entryType: string; dailyEntryTypeEntriesMap: MoodMapMeta; }[], lifestyleEntryType: LifestyleEntryType): MoodMapMeta {
    return entries.find((type) => LifestyleEntryType[type.entryType] === lifestyleEntryType).dailyEntryTypeEntriesMap;
}

function getChartData(groupedMoodEntriesByAthlete: Map<any, any>, daysOfWeek: { date: Date; uid?: string; lineChartData: any; entries: BiometricEntrySchema[]; }[], startOfWeek: moment.Moment, lineChartDatasets: any[]) {
    groupedMoodEntriesByAthlete.forEach((athleteDailyMoodEntries, uid) => {
        const athleteFullName = uid.split('__')[0];
        const athleteID = uid.split('__')[1];
        const userLinerChartData = [];

        const groupedMoodEntriesByDate = groupByMap(athleteDailyMoodEntries, (entry: BiometricEntrySchema) => moment(entry.creationTimestamp).format(('MMMDDDYYY')));
        daysOfWeek.map((day) => {
            const entries = groupedMoodEntriesByDate.get(moment(day.date).format(('MMMDDDYYY')));
            if (entries) {
                userLinerChartData.push(getTrendsLineChartData(entries, day.date, athleteID, athleteFullName, startOfWeek, daysOfWeek));
            }
            else {
                userLinerChartData.push(0);
            }
        });
        const colour = athleteColors.find((athColour) => athColour.uid === athleteID).colour;
        const athleteDataset = {
            uid: athleteID,
            data: userLinerChartData,
            label: athleteFullName,
            borderColor: colour,
            fill: false,
            borderJoinStyle: "round"
        };
        lineChartDatasets.push(athleteDataset);
    });
}

function mapAggregateData(
    dayEntries: BiometricEntrySchema[],
    groupedByAthletUIDs: Map<string, BiometricEntrySchema[]>,
    dayDate: Date) {

    const groupedByLifestyleEntryType = groupByMap(dayEntries, (entry: BiometricEntrySchema) => entry.lifestyleEntryType) as Map<string, BiometricEntrySchema[]>;

    const entries = getEntriesGroupedByLifestyleEntryType(groupedByLifestyleEntryType, groupedByAthletUIDs, dayDate);


    // const lineChartDatasets = []


    // getTrendsLineChartData(athleteEntries, athleteID, athleteFullName, startOfWeek, daysOfWeek, lineChartDatasets);















    // const thePainEntriesList = [].concat(painEntriesGroupedByAthlete.complexChartData.map((as) => as.entries))[0] as BiometricPainEntrySchema[];

    // const { athletesWithPain, mealTypeAverages, foodTypeCounts } = getAggregatePayload(foodEntries, simpleFoodEntries.entries, thePainEntriesList, setAllToToday, startOfWeek);

    // const lessThanFourHoursSleepEntries = getLessThanFourHoursSleepEntries(sleepEntries.entries);

    // const fatiguedEntries = getFatiguedAthletes(sleepEntries.entries);

    // const sadAthletes = getSadEntries(moodEntries.entries);

    // const totalEngagements = dayEntries.length;
    // const engagementSuperstars = getEngagementSuperstars(dayEntries);
    // const trainingEntriesGroupedByAthlete = groupByMap(trainingEntries, (entry: BiometricTrainingEntrySchema) => entry.athleteID);
    // trainingEntriesGroupedByAthlete.forEach((gp, kk) => {
    // })
    // const allTrainingEntries: Array<BiometricTrainingEntrySchema> = [].concat(trainingEntries.complexChartData
    //     .map(grouped => grouped.entries))[0];

    // const totalTrainingMinutes = allTrainingEntries && allTrainingEntries.length ? allTrainingEntries
    //     .map((entry) => entry.value)
    //     .reduce((a, b) => a + b) : 0;

    // const trainingSuperstars = allTrainingEntries && allTrainingEntries.length ? allTrainingEntries
    //     .filter((entry) => entry.value > 3)
    //     .sort(sortByValueDesc) : [];

    // const theTrainingSuperstars = getTrainingSuperstars(trainingSuperstars).filter((entry) => entry.value > 200);

    // const sumOfMoodEntries = moodEntries.entries.length;

    // const happyChappies = moodEntries.entries
    //     .filter((entry) => entry.value > 3 && entry.userFirstName !== undefined)
    //     .sort(sortByValueDesc);

    // const theHappyChappies = getHappyChappySuperstars(happyChappies);

    // const theConfig: RedFlagConfigSchema = templateConfig;
    // const test = trainingEntries

    // const redFlagTemplatedata = getRedFlags(moodEntries.entries, sleepEntries.entries, sadAthletes, lessThanFourHoursSleepEntries, fatigueEntries.entries, thePainEntriesList, fatiguedEntries, athletesWithPain, foodTypeCounts, simpleFoodEntries, mealTypeAverages, trainingEntries.minutesBySportType);

    // const superstarsTemplatedata = getSuperstars(engagementSuperstars, theTrainingSuperstars, theHappyChappies);

    // const trendsTemplatedata = getTrends(moodEntries, sleepEntries, fatigueEntries);

    // const detailedTemplatedata = getDetailed(trainingEntries, painEntriesGroupedByAthlete);

    return entries
    // {
    //     // redFlagTemplatedata,
    //     // superstarsTemplatedata,
    //     moodEntries,
    //     sleepEntries
    //     // trendsTemplatedata, detailedTemplatedata
    // };
}

