
import { ChartType } from './../../../../../../../../pubsub/interfaces/red-flag-reports';
import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { sortByValue } from "../../sortByHelpers";
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { getMeanOfValue } from '../../getMeanOfValue';
import { getLowestValue } from '../../getLowestValue';
import { getHighestValue } from '../../getHighestValue';
import { getSumOfValue } from '../../getSumOfValue';
import { getEntriesIndicator } from '../../getEntriesIndicator';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';

// function getPainEntries(painEntries: Array<BiometricEntrySchema>): number {
//     return painEntries.length || 0;
// }

export type MoodMapMeta = {
    uid?: string;
    entries: Array<BiometricEntrySchema>;
    lineChartData?: {
        chartType: ChartType;
        chartData: {
            labels: Array<string>;
            datasets: Array<any>;
        };
    };
    barChartData?: {
        chartType: ChartType;
        chartData: {
            labels: Array<string>;
            datasets: Array<{
                data: Array<any>;
                backgroundColor: string[];
            }>
        };
    };
    lowValue: number;
    highValue: number;
    meanValue: number;
    totalValue: number;
    countValue: number;

    date: Date;

};

// export const getMoodMap = (entries: Array<BiometricEntrySchema>): Array<BiometricScalarEntrySchema> => {
export const getAggregateValuesMap = (
    allDailyTypeEntries: Array<BiometricEntrySchema>,
    dailyDate: Date,
): MoodMapMeta => {

    // const theMoodEntries: Array<MoodMapMeta> = [];


    const { lowValue, highValue, meanValue, totalValue, countValue } = calculateAggValues(allDailyTypeEntries);


    const mappedData = {
        lowValue,
        highValue,
        meanValue,
        totalValue,
        countValue,
        // lineChartData: undefined,
        // barChartData: undefined,
        // daysOfWeek,
        date: dailyDate,
        entries: allDailyTypeEntries
            .sort(sortByValue),
        indicators: getEntriesIndicator(allDailyTypeEntries)
            .sort(sortByValue)


    }



    const groupedByAthletUIDsd = groupByMap(mappedData.entries, (entry: BiometricEntrySchema) => entry.athleteID) as Map<string, BiometricEntrySchema[]>;
    const allMappedEntries = []
    const allMappedIndicators = []

    getAthleteMapData(groupedByAthletUIDsd, allMappedEntries, dailyDate, allMappedIndicators);

    mappedData.entries = allMappedEntries;
    mappedData.indicators = allMappedIndicators;
    removeUndefinedProps(mappedData)

    return mappedData;
};
function getAthleteMapData(groupedByAthletUIDsd: Map<string, BiometricEntrySchema[]>, allMappedEntries: any[], dailyDate: Date, allMappedIndicators: any[]) {
    groupedByAthletUIDsd.forEach((entries, uid) => {
        const { lowValue, highValue, meanValue, totalValue, countValue } = calculateAggValues(entries);
        allMappedEntries.push(...entries.map((entry => { return { ...entry, lowValue, highValue, meanValue, totalValue, countValue, date: dailyDate }; })));
        allMappedIndicators.push(...getEntriesIndicator(entries.map((entry => { return { ...entry, lowValue, highValue, meanValue, totalValue, countValue, date: dailyDate }; }))));
    });
}

function calculateAggValues(allDailyTypeEntries: BiometricEntrySchema[]) {
    const meanValue = getMeanOfValue(allDailyTypeEntries);
    const lowValue = getLowestValue(allDailyTypeEntries);
    const highValue = getHighestValue(allDailyTypeEntries);
    const totalValue = getSumOfValue(allDailyTypeEntries);
    const countValue = allDailyTypeEntries.length;
    return { lowValue, highValue, meanValue, totalValue, countValue };
}

