import { getStackedChartData } from './getStackedChartData';
import moment from 'moment';
import { sum } from 'lodash';

import { BiometricEntrySchema, BiometricTrainingEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { sortByValue } from "../../sortByHelpers";
import { getMeanOfValue } from "../../getMeanOfValue";
import { getLowestValue } from '../../getLowestValue';
import { getHighestValue } from '../../getHighestValue';

import { getGroupedByIntensityLevel } from '../training/getGroupedByIntensityLevel';
import { getEntriesIndicator } from '../../getEntriesIndicator';

import { getSumOfValue } from '../../getSumOfValue';
import { getGroupedByAverageTrainingMinutes } from '../training/getGroupedByAverageTrainingMinutes';
import { getGroupedBySportType } from '../training/getGroupedBySportType';
import { getGroupedByDay } from './getGroupedByDay';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
import { initDaysOfWeek } from './initDaysOfWeek';

export type TrainingMapMeta = {
    uid?: string;
    firstName?: string;
    fullName?: string;

    lowValue: number;
    highValue: number;
    meanValue: number;
    totalValue: number;
    countValue: number;

    avg: number;

    entries: Array<BiometricEntrySchema>;

    groupedByDate?: Array<{

        countValue: number;
        date: string;
        groupedByAverageTrainingMinutes: Array<any>;
        groupedByDateAndIntensityLevel: Array<any>;
        highValue: number;
        lowValue: number;
        meanValue: number;
        minutesBySportType: Array<any>;
        theMinutesperIntensityEntries: Array<any>;
        totalValue: number;
    }>
    // groupedByDateAndAverageMinutes?: any;
};

export const getTrainingMap = (trainingEntries: Array<BiometricEntrySchema>, startDate: moment.Moment, toDate: moment.Moment,
    groupedEntries: Map<string, Array<BiometricEntrySchema>>): {
    complexChartData: Array<TrainingMapMeta>;
    groupedByDate: any;
    minutesBySportType;
    theMinutesperIntensityEntries;
    stakedChartData;
    // groupedByDateAndAverageMinutes: any
} => {
    // (trainingEntries: Array<BiometricEntrySchema>): Array<{
    //     uid?: string;
    //     firstName?: string;
    //     fullName?: string;

    //     lowValue: number;
    //     highValue: number;
    //     meanValue: number;
    //     totalValue: number;
    //     countValue: number;

    //     avg: number;

    //     entries: Array<BiometricEntrySchema>;

    //     groupedByDateAndIntensityLevel: any;
    //     groupedByDateAndAverageMinutes: any;
    // }> => {



    const theTrainingEntries: Array<TrainingMapMeta> = [];

    const groupedByDate = []

    // const groupedByDateAndAverageMinutes = []

    // from(trainingEntries)
    //     .pipe(
    //         groupBy(athleteEntry => athleteEntry.athleteID, p => p),
    //         mergeMap(athleteGroup =>
    //             zip(of(athleteGroup.key), athleteGroup.pipe(toArray())))
    //         // take(1)
    //     )
    //     .subscribe((trainingEntriesGroupedByAthlete) => {



    // const athleteID = trainingEntriesGroupedByAthlete[0];

    const athlteteTrainingEntries = trainingEntries // trainingEntriesGroupedByAthlete[1] as Array<BiometricTrainingEntrySchema>;

    const atData = {
        // uid: athleteID,
        entries: []
    }

    const daysOfWeek: {
        date: Date;
        uid?: string;
        lineChartData: any;
        entries: BiometricEntrySchema[]
    }[] = []

    const utcoffset = trainingEntries && trainingEntries.length ? moment(trainingEntries[0].creationTimestamp).utcOffset() : 0 
    const startOfWeek = initDaysOfWeek(daysOfWeek, startDate, toDate, groupedEntries, utcoffset);

    let setAllToToday = false;
    getGroupedByDay(setAllToToday, athlteteTrainingEntries, startDate, daysOfWeek);



    // const theEntries = [];
    // const groupedEntries = groupByMap(daysOfWeek, (entry: { date: Date; uid: string; entries: BiometricEntrySchema[] }) => entry.date.toDateString()) as Map<string, Array<{ date: Date; uid: string; entries: BiometricEntrySchema[] }>>;
    // groupedEntries.forEach((athleteEntries) => {
    //     // const res = {
    // athleteID: athleteEntries[0].athleteID,
    //     //     name: athleteEntries[0].userFullName,
    //     //     value: athleteEntries[0].highValue
    //     // };
    //     theEntries.push(athleteEntries);
    // });
    // // return theEntries.sort(sortByValueDesc);

    // const groupByDate = groupByMap(daysOfWeek.map(day => day.date.toISOString()),
    //     (day: { date: Date; uid: string; entries: BiometricEntrySchema[] }) => {
    //         return {
    //             ...day,
    //             date: moment(day.date).toDate()
    //         }
    //     });


    const stackedChartDatasets = []

    const groupedEntriesByAthlete = groupByMap(trainingEntries.sort(sortByValue), (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID);

    groupedEntriesByAthlete.forEach((athleteEntries: Array<BiometricEntrySchema>, uid: string) => {

        const athleteFullName = uid.split('__')[0]
        const athleteID = uid.split('__')[1]

        // Line Chart Data

        getStackedChartData(athleteEntries, athleteID, athleteFullName, startOfWeek, daysOfWeek, stackedChartDatasets);

        // Bar Chart Data
        // getTrendsBarChartData(athleteEntries, athleteID, athleteFullName, barChartDatasets);
    })

    daysOfWeek.forEach((day: { date: Date; uid: string; entries: BiometricTrainingEntrySchema[], lineChartData: any; }) => {

        const groupedByDateAndIntensityLevelMap = getGroupedByIntensityLevel(day.entries);
        const groupedByAverageTrainingMinutesMap = getGroupedByAverageTrainingMinutes(day.entries);




        // if (groupedByDateAndIntensityLevel[athleteID] === undefined)
        //     groupedByDateAndIntensityLevel[athleteID] = []

        const periodMeanValue = getMeanOfValue(day.entries);
        const periodLowValue = getLowestValue(day.entries);
        const periodHighValue = getHighestValue(day.entries);
        const periodTotalValue = getSumOfValue(day.entries);
        const periodCountValue = day.entries.length;

        atData.entries.push({
            date: day.date.toISOString(),
            groupedByDateAndIntensityLevel: groupedByDateAndIntensityLevelMap,
            groupedByAverageTrainingMinutes: groupedByAverageTrainingMinutesMap,

            meanValue: periodMeanValue,
            lowValue: periodLowValue,
            highValue: periodHighValue,
            totalValue: periodTotalValue,
            countValue: periodCountValue
        })

        // groupedByDateAndIntensityLevel[athleteID].push({

        //     date: day.date.toISOString(),
        //     groupedByDateAndIntensityLevel: groupedByDateAndIntensityLevelMap,
        //     meanValue: periodMeanValue,
        //     lowValue: periodLowValue,
        //     highValue: periodHighValue,
        //     totalValue: periodTotalValue,
        //     countValue: periodCountValue
        // })

    });


    // groupedByDateAndIntensityLevel.map((val, ent) => {
    //     console.log('val', val)
    //     console.log('ent', ent)
    // })

    removeUndefinedProps(athlteteTrainingEntries);

    const meanValue = getMeanOfValue(athlteteTrainingEntries);
    const lowValue = getLowestValue(athlteteTrainingEntries);
    const highValue = getHighestValue(athlteteTrainingEntries);
    const totalValue = getSumOfValue(athlteteTrainingEntries);
    const countValue = athlteteTrainingEntries.length;

    groupedByDate.push(atData)


    const mappedData = {
        // uid: trainingEntriesGroupedByAthlete[0],
        // firstName: athlteteTrainingEntries[0].userFirstName,
        // fullName: athlteteTrainingEntries[0].userFullName,
        lowValue,
        highValue,
        meanValue,
        totalValue,
        countValue,
        groupedByDate: atData.entries,// groupedByDateAndIntensityLevel, //.filter((at) => at.uid === trainingEntriesGroupedByAthlete[0]),
        // groupedByDateAndAverageMinutes,
        entries: getEntriesIndicator(athlteteTrainingEntries)
            // .map((entry) => {
            //     return {
            //         creationTimestamp: entry.creationTimestamp,
            //         lifestyleEntryType: entry.lifestyleEntryType,
            //         lifestyleEntryTypeID: entry.lifestyleEntryTypeID,
            //         painType: entry.painType,
            //         painTypeID: entry.painTypeID,
            //         value: entry.value
            //     }
            // })
            .sort(sortByValue),
        avg: undefined,

    }

    theTrainingEntries.push(mappedData);

    // });






    // const groupedByTotalPainLevelPerTypeMap: PainTrendData = getGroupedByPainType(athlteteTrainingEntries).map((data) => {
    //     // const { firstName, fullName, athleteID, ...theRestData } = data
    //     const theRestData = data;

    //     return {
    //         ...theRestData
    //     }
    // });

    // const levelByTotalBodyLocationMap: PainByBodyLocationData = getGroupedByBodyLocation(painEntries).map((data) => {

    //     // const { firstName, fullName, athleteID: uid, ...theRestData } = data
    //     const theRestData = data;
    //     return {
    //         ...theRestData
    //     }
    // });



    // Grouped By Intensity Level
    const theMinutesperIntensityEntriesMap = getGroupedByIntensityLevel(athlteteTrainingEntries as BiometricTrainingEntrySchema[]);

    // Grouped By Sport Type
    const minutesBySportTypeMap = getGroupedBySportType(athlteteTrainingEntries as BiometricTrainingEntrySchema[]);



    const totalCount = sum(theTrainingEntries.map((level) => level.totalValue));

    theTrainingEntries.map((trainingGroup) => {
        const avgOutOfHundred = trainingGroup.totalValue / totalCount * 100;
        trainingGroup.avg = avgOutOfHundred;
    });

    const labels = daysOfWeek.map((day) => day.date.toDateString())

    const stakedData = {
        labels: labels,
        datasets: stackedChartDatasets
    }

    const theResults = {
        complexChartData: theTrainingEntries,
        stakedChartData: stakedData,
        groupedByDate: groupedByDate[0].entries,
        theMinutesperIntensityEntries: theMinutesperIntensityEntriesMap,
        minutesBySportType: minutesBySportTypeMap
    };

    removeUndefinedProps(theResults)

    return theResults;
};



