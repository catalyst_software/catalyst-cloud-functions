import moment from 'moment';

import { BiometricEntrySchema, BiometricPainEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
import { sortByValueDesc } from "../../sortByHelpers";

export const getPainAthletes = (painEntries: BiometricPainEntrySchema[], setAllToToday: boolean, startOfWeek: moment.Moment,
    // daysOfWeek: {
    //     date: Date;
    //     uid?: string;
    //     lineChartData: any;
    //     entries: BiometricEntrySchema[]
    // }[]
): Array<BiometricPainEntrySchema> => {

    const extremePainEntries = painEntries.filter((entry) => entry.value >= 4) || [];

    const theEntries = [];

    const groupedEntries = groupByMap(extremePainEntries, (entry: BiometricEntrySchema) => entry.userFirstName + '__' + entry.athleteID) as Map<string, Array<BiometricEntrySchema>>;

    groupedEntries.forEach((athleteEntries) => {
        const res = {
            athleteID: athleteEntries[0].athleteID,
            name: athleteEntries[0].userFullName,
            value: athleteEntries[0].value
        };
        theEntries.push(res);
    });

    return theEntries.sort(sortByValueDesc);
};
