import { EmailMessage } from '../../../sendGrid/conrollers/email';

export const message: EmailMessage = {
    personalizations: [
        {
            to: [
                {
                    name: "Ian Kaapisoft",
                    email: "ian.gouws@kaapisoft.com"
                }
            ]
        }
    ],
    from: {
        name: "iNSPIRE",
        email: "development@inspiresportaustralia.com"
    },
    templateId: "d-b95ae611e7124aafa1b948ca09ad3c2d",
    emailTemplateName: "ipScoreNonEngagement",
    dynamic_template_data: {
        name: "Ian Gouws!!",
        // subject: "Encoding bit's!!!",
        // message: "Mail TGemplate Update>",
        showDownloadButton: false,
        barType: {
            danger: true
        }
    }
};
