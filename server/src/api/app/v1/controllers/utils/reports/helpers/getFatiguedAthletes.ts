import { BiometricEntrySchema, BiometricScalarEntrySchema } from '../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../shared/utils/groupByMap';
import { sortByValueDesc } from "../sortByHelpers";

export const getFatiguedAthletes = (fatigueEntries: BiometricScalarEntrySchema[]): Array<BiometricScalarEntrySchema> => {
    const entries = fatigueEntries.filter((entry) => entry.value >= 4) || [];
    const theEntries = [];
    const groupedEntries = groupByMap(entries, (entry: BiometricEntrySchema) => entry.userFirstName + '__' + entry.athleteID) as Map<string, Array<BiometricEntrySchema>>;
    groupedEntries.forEach((athleteEntries) => {
        const res = {
            athleteID: athleteEntries[0].athleteID,
            name: athleteEntries[0].userFullName,
            value: athleteEntries[0].highValue
        };
        theEntries.push(res);
    });
    return theEntries.sort(sortByValueDesc);
};
