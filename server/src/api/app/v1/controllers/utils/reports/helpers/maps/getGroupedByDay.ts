import moment from 'moment';
import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
import { chunk } from '../../../chunk';
import { sortByDate } from '../../sortByHelpers';

export const getGroupedByDay = (setAllToToday: boolean, athlteteTrainingEntries: BiometricEntrySchema[], startOfWeek: moment.Moment, daysOfWeek: {
    date: Date;
    uid?: string;
    entries: BiometricEntrySchema[];
}[]) => {
    
    if (setAllToToday) {
        const division = Math.ceil(athlteteTrainingEntries.length / 7);
        const athlteteTrainingEntriesChunked = chunk(athlteteTrainingEntries, division);
        // const startOfWeek = moment().startOf('week').add(1, 'day').startOf('day')
        athlteteTrainingEntriesChunked.forEach((chunks: Array<BiometricEntrySchema>, idx) => {
            chunks.forEach((entry) => {
                entry.creationTimestamp = startOfWeek.add(20, 'minutes').toDate().toISOString();
            });
            daysOfWeek[idx].entries = chunks;
            startOfWeek.add(1, 'day').startOf('day').toDate().toISOString();
        });
    }
    else {
        const groupedEntriesByDate = groupByMap(athlteteTrainingEntries, (entry: BiometricEntrySchema) => moment(entry.creationTimestamp).toDate().toDateString()) as Map<string, Array<BiometricEntrySchema>>;
        // TODO: Group Entries By Day
        groupedEntriesByDate.forEach((athleteEntries, theDate) => {
            // const dayIndex = moment(athleteEntries[0].creationTimestamp).weekday() - 1;

            const date = moment(theDate).startOf('day').toDate() // athleteEntries[0].creationTimestamp

            const dayOfWeek = daysOfWeek.find((entry) => entry.date.toDateString() === date.toDateString())
            if (dayOfWeek)
                // daysOfWeek.find((entry) => entry.date === date).entries = athleteEntries;
                dayOfWeek.entries = athleteEntries;
            else {
                console.error(`Date =>  ${theDate} Not Found in daysOfWeek Array, number of entries for the day => `, athleteEntries.length || 'set to 0')
                daysOfWeek.unshift({ date, entries: athleteEntries });
            }
        });

        daysOfWeek.sort(sortByDate);
    }
};


// (function(){
//     if (typeof Object.defineProperty === 'function'){
//       try{Object.defineProperty(Array.prototype,'sortBy',{value:sb}); }catch(e){}
//     }
//     if (!Array.prototype.sortBy) Array.prototype.sortBy = sb;

//     function sb(f){
//       for (var i=this.length;i;){
//         var o = this[--i];
//         this[i] = [].concat(f.call(o,o,i),o);
//       }
//       this.sort(function(a,b){
//         for (var i=0,len=a.length;i<len;++i){
//           if (a[i]!=b[i]) return a[i]<b[i]?-1:1;
//         }
//         return 0;
//       });
//       for (var i=this.length;i;){
//         this[--i]=this[i][this[i].length-1];
//       }
//       return this;
//     }
//   })();