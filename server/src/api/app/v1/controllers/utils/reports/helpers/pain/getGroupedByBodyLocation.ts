import { from, of, zip } from 'rxjs';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { toArray } from 'rxjs/internal/operators/toArray';
import { map } from 'rxjs/internal/operators/map';
import { sum } from 'lodash';

import { BiometricPainEntrySchema, BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { PainByBodyLocationData } from '../../types/pain-trend-data';
import { getAggregateValues } from '../../getAggregateValues';
import { getEntriesIndicator } from '../../getEntriesIndicator';
import { removeUndefinedProps } from '../../../remove-undefined-props';
import { sortByValue } from '../../sortByHelpers';

export const getGroupedByBodyLocation = (athlteteTrainingEntries: BiometricEntrySchema[]) => {

    const bodyLocationData: PainByBodyLocationData = [];
    const selectedBodyLocations = [];

    from(athlteteTrainingEntries)
        .pipe(
            groupBy(entry => entry.bodyLocation),
            map((bodyLocationGrouped) => {

                // console.log('grouped', bodyLocationGrouped);
                selectedBodyLocations.push(bodyLocationGrouped.key);

                return bodyLocationGrouped;
            }),
            mergeMap(bodyLocationGroup => zip(of(bodyLocationGroup.key), bodyLocationGroup.pipe(toArray())))
        )
        .subscribe((groupedByBodyLocation) => {

            const bodyLocation = groupedByBodyLocation[0];
            const athlteteByBodyLocationEntries = groupedByBodyLocation[1] as BiometricPainEntrySchema[];

            const {
                meanValue: meanValueOfPain,
                lowValue: lowValueOfPain,
                highValue: highValueOfPain,
                totalValue: totalValueOfPain,
                countValue: countValueOfPain
            } = getAggregateValues(athlteteByBodyLocationEntries);

            const painLevelValues = {
                bodyLocation,
                meanValue: meanValueOfPain,
                lowValue: lowValueOfPain,
                highValue: highValueOfPain,
                totalValue: totalValueOfPain,
                countValue: countValueOfPain,
                avg: null
            };

            bodyLocationData.push({
                athleteID: athlteteTrainingEntries[0].athleteID,
                firstName: athlteteTrainingEntries[0].userFirstName,
                fullName: athlteteTrainingEntries[0].userFullName,
                entries: getEntriesIndicator(athlteteByBodyLocationEntries).sort(sortByValue),
                ...painLevelValues
            });
        });

    const totalCount = sum(bodyLocationData.map((level) => level.totalValue));

    const theResultData = bodyLocationData.map((bodyLocationGroup) => {

        const { firstName, fullName, athleteID, ...theRestData } = bodyLocationGroup
        const avgOutOfHundred = theRestData.totalValue / totalCount * 100;
        theRestData.avg = avgOutOfHundred;

        return theRestData;
    });

    // removeUndefinedProps(bodyLocationData)
    removeUndefinedProps(theResultData)

    return theResultData;
};


