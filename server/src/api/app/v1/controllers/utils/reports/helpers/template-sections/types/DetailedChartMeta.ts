import { ChartType, ChartOptions } from '../../../../../../../../../pubsub/interfaces/red-flag-reports';
export type DetailedChartMeta = {
    title: string;
    charts: Array<{
        chartTitle: string;
        chartType: ChartType;
        chartOptions: ChartOptions;
        chartLabel?: string;
        chartData: {
            labels: string[];
            datasets: Array<{
                label: string;
                type?: ChartType;
                data: number[];
                fill?: boolean;
                backgroundColor: string;
                borderColor?: string;
                borderCapStyle?: string;
                borderDash?: number[];
                borderDashOffset?: number;
                borderJoinStyle?: string;
                lineTension?: number;
                pointBackgroundColor?: string;
                pointBorderColor?: string;
                pointBorderWidth?: number;
                pointHoverRadius?: number;
                pointHoverBackgroundColor?: string;
                pointHoverBorderColor?: string;
                pointHoverBorderWidth?: number;
                pointRadius?: number;
                pointHitRadius?: number;
                yAxisID?: string;
            }>;
        };
    }>;
};
