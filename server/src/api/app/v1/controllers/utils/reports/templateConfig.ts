import { ChartType, ChartOptions } from '../../../../../../pubsub/interfaces/red-flag-reports';
import { RedFlagConfigSchema } from '../../../../../../pubsub/interfaces/RedFlagConfigSchema';

export const templateConfig: RedFlagConfigSchema = {
    "header": {
        "fromDate": "May 12, 2019",
        "toDate": "May 18, 2019",
        "headerType": "athlete",
        "title": {
            "organization": "CARMEL COLLEGE",
            "team": "The Rebels",
            "group": "Group 1",
            "athlete": ""
        }
    },

    "redFlags": {

        "brainSummary": {
            "include": true,
            "data": {
                "useBarChart": true,
                "redFlagGroups": [
                    {
                        "image": "icons8-sad-96.png",
                        "chartType": ChartType.HorizontalBar,
                        "chartData": {
                            "datasets": [
                                {
                                    "backgroundColor": [
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1"
                                    ]
                                }
                            ]
                        }
                    }, {
                        "image": "icons8-insomnia-96.png",
                        "chartType": ChartType.HorizontalBar,
                        "chartData": {
                            "datasets": [
                                {
                                    "backgroundColor": [
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1"
                                    ]
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "bodySummary": {
            "include": true,
            "data": {
                "useBarChart": true,
                "redFlagGroups": [
                    {
                        "image": "icons8-bored-96.png",
                        "chartType": ChartType.HorizontalBar,
                        "chartData": {
                            "datasets": [
                                {
                                    "backgroundColor": [
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1"
                                    ]
                                }
                            ]
                        }
                    }, {
                        "image": "icons8-crutch-96.png",
                        "chartType": ChartType.HorizontalBar,
                        "chartData": {
                            "datasets": [
                                {
                                    "backgroundColor": [
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1"
                                    ]
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "foodSummary": {
            "include": true,
            "data": {
                "usePieChart": true,
                "pieChartData": {
                    "chartType": ChartType.Pie,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": [
                                "#FD704B",
                                "#4688F1",
                                "#9E9C31",
                                "#1D9C5A",
                                "#D9453D",
                                "#1BACC0",
                                "#F2B32A"
                            ]
                        }]
                    }
                }
            }
        },
        "trainingSummary": {
            "include": true,
            "data": {
                "usePieChart": true,
                "pieChartData": {
                    "chartType": ChartType.Pie,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": [
                                "#FD704B",
                                "#4688F1",
                                "#9E9C31",
                                "#1D9C5A",
                                "#D9453D",
                                "#1BACC0"
                            ]
                        }]
                    }
                }
            }
        }
    },

    "superstars": {
        "include": true,
        "data": {
            "superstarsGroups": [
                {
                    "isSuperstars": true,
                    "chartType": ChartType.HorizontalBar,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": ["#F3C7C4", "#F3C7C4", "#F3C7C4", "#F3C7C4",
                                "#F3C7C4", "#F3C7C4", "#F3C7C4"
                            ]
                        }]
                    }
                },
                {
                    "isSuperstars": true,
                    "chartType": ChartType.HorizontalBar,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": ["#283879", "#283879", "#283879", "#283879",
                                "#283879", "#283879", "#283879"
                            ]
                        }]
                    }
                },
                {
                    "isSuperstars": true,
                    "chartType": ChartType.HorizontalBar,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": ["#F3C7C4", "#F3C7C4", "#F3C7C4", "#F3C7C4",
                                "#F3C7C4", "#F3C7C4", "#F3C7C4"
                            ]
                        }]
                    }
                }
            ]
        }
    },

    "trends": [
        {
            "include": true,
            "data": {
                "isTrends": true,
                "lineChart": {
                    "chartType": ChartType.Line,
                    "chartData": {
                        "datasets": [
                            {
                                "borderColor": "#e6e600",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#68838F",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#00ff00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#C42963",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#ff66ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#4dd2ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#007399",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#3333ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#ff9900",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#88cc00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            }
                        ]
                    }
                },
                "barChart": {
                    "chartType": ChartType.HorizontalBar,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": ["#e6e600", "#68838F", "#00ff00", "#C42963", "#ff66ff", "#4dd2ff", "#007399", "#3333ff", "#ff9900", "#88cc00"]
                        }]
                    }
                }
            }
        },
        {
            "include": true,
            "data": {
                "isTrends": true,
                "lineChart": {
                    "chartType": ChartType.Line,
                    "chartData": {
                        "datasets": [
                            {
                                "borderColor": "#e6e600",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#68838F",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#00ff00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#C42963",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#ff66ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#4dd2ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#007399",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#3333ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#ff9900",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#88cc00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            }
                        ]
                    }
                },
                "barChart": {
                    "chartType": ChartType.HorizontalBar,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": ["#e6e600", "#68838F", "#00ff00", "#C42963", "#ff66ff", "#4dd2ff", "#007399", "#3333ff", "#ff9900", "#88cc00"]
                        }]
                    }
                }
            }
        },
        {
            "include": true,
            "data": {
                "isTrends": true,
                "lineChart": {
                    "chartType": ChartType.Line,
                    "chartData": {
                        "datasets": [
                            {
                                "borderColor": "#e6e600",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#68838F",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#00ff00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#C42963",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#ff66ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#4dd2ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#007399",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#3333ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#ff9900",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "borderColor": "#88cc00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            }
                        ]
                    }
                },
                "barChart": {
                    "chartType": ChartType.HorizontalBar,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": ["#e6e600", "#68838F", "#00ff00", "#C42963", "#ff66ff", "#4dd2ff", "#007399", "#3333ff", "#ff9900", "#88cc00"]
                        }]
                    }
                }
            }
        }
    ],


    "detailed": {
        "training": {
            "include": true,
            "data": {
                "detailedChartMeta": {
                    "charts": [{
                        "chartType": ChartType.Bar,
                        "chartOptions": ChartOptions.TrendsMixed,
                        "chartData": {
                            "datasets": [
                                {
                                    "type": ChartType.Line,
                                    "fill": false,
                                    "backgroundColor": "#fff",
                                    "borderColor": "#70cbf4",
                                    "borderCapStyle": "butt",
                                    "borderDash": [],
                                    "borderDashOffset": 0,
                                    "borderJoinStyle": "miter",
                                    "lineTension": 0.3,
                                    "pointBackgroundColor": "#fff",
                                    "pointBorderColor": "#70cbf4",
                                    "pointBorderWidth": 1,
                                    "pointHoverRadius": 5,
                                    "pointHoverBackgroundColor": "#70cbf4",
                                    "pointHoverBorderColor": "#70cbf4",
                                    "pointHoverBorderWidth": 2,
                                    "pointRadius": 4,
                                    "pointHitRadius": 10
                                },
                                {
                                    "type": ChartType.Line,
                                    "fill": false,
                                    "backgroundColor": "#fff",
                                    "borderColor": "#737373",
                                    "borderCapStyle": "butt",
                                    "borderDash": [
                                        10,
                                        10
                                    ],
                                    "borderDashOffset": 0,
                                    "borderJoinStyle": "miter",
                                    "lineTension": 0.3,
                                    "pointBackgroundColor": "#fff",
                                    "pointBorderColor": "#737373",
                                    "pointBorderWidth": 1,
                                    "pointHoverRadius": 5,
                                    "pointHoverBackgroundColor": "#737373",
                                    "pointHoverBorderColor": "#737373",
                                    "pointHoverBorderWidth": 2,
                                    "pointRadius": 4,
                                    "pointHitRadius": 10
                                },
                                {
                                    "backgroundColor": "#269B5C",
                                    "yAxisID": "bar-y-axis"
                                },
                                {
                                    "backgroundColor": "#F0B23C",
                                    "yAxisID": "bar-y-axis"
                                },
                                {
                                    "backgroundColor": "#D74742",
                                    "yAxisID": "bar-y-axis"
                                }
                            ]
                        }
                    }]
                },

                "chartOne": {
                    "chartOptions": ChartOptions.TrendsMixed,
                    "chartType": ChartType.Bar,
                    "chartData": {
                       "datasets": [
                            {
                                "type": ChartType.Line,
                                "fill": false,
                                "backgroundColor": "#fff",
                                "borderColor": "#70cbf4",
                                "borderCapStyle": "butt",
                                "borderDash": [],
                                "borderDashOffset": 0,
                                "borderJoinStyle": "miter",
                                "lineTension": 0.3,
                                "pointBackgroundColor": "#fff",
                                "pointBorderColor": "#70cbf4",
                                "pointBorderWidth": 1,
                                "pointHoverRadius": 5,
                                "pointHoverBackgroundColor": "#70cbf4",
                                "pointHoverBorderColor": "#70cbf4",
                                "pointHoverBorderWidth": 2,
                                "pointRadius": 4,
                                "pointHitRadius": 10
                            },
                            {
                                "type": ChartType.Line,
                                "fill": false,
                                "backgroundColor": "#fff",
                                "borderColor": "#737373",
                                "borderCapStyle": "butt",
                                "borderDash": [
                                    10,
                                    10
                                ],
                                "borderDashOffset": 0,
                                "borderJoinStyle": "miter",
                                "lineTension": 0.3,
                                "pointBackgroundColor": "#fff",
                                "pointBorderColor": "#737373",
                                "pointBorderWidth": 1,
                                "pointHoverRadius": 5,
                                "pointHoverBackgroundColor": "#737373",
                                "pointHoverBorderColor": "#737373",
                                "pointHoverBorderWidth": 2,
                                "pointRadius": 4,
                                "pointHitRadius": 10
                            },
                            {
                                "backgroundColor": "#269B5C",
                                "yAxisID": "bar-y-axis"
                            },
                            {
                                "backgroundColor": "#F0B23C",
                                "yAxisID": "bar-y-axis"
                            },
                            {
                                "backgroundColor": "#D74742",
                                "yAxisID": "bar-y-axis"
                            }
                        ]
                    }
                },
                "chartTwo": {
                    "chartType": ChartType.PolarArea,
                    "chartOptions": ChartOptions.PolarArea,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": [
                                "#4688F1",
                                "#D9463D",
                                "#FC714A",
                                "#1D9C5A",
                                "#1DAAF1",
                                "#5C6DBD"
                            ]
                        }],
                    }
                },
                "chartThree": {
                    "chartType": ChartType.Pie,
                    "chartOptions": ChartOptions.Pie,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": ["#4688F1", "#D9463D"]
                        }]
                    }
                }
            }
        },
        "pain": {
            "include": true,
            "data": {

                "detailedChartMeta": {
                    "charts": [{
                        "chartType": ChartType.Bar,
                        "chartOptions": ChartOptions.TrendsMixed,
                        "chartData": {
                           "datasets": [
                                {
                                    "type": ChartType.Line,
                                    "fill": false,
                                    "backgroundColor": "#fff",
                                    "borderColor": "#70cbf4",
                                    "borderCapStyle": "butt",
                                    "borderDash": [],
                                    "borderDashOffset": 0,
                                    "borderJoinStyle": "miter",
                                    "lineTension": 0.3,
                                    "pointBackgroundColor": "#fff",
                                    "pointBorderColor": "#70cbf4",
                                    "pointBorderWidth": 1,
                                    "pointHoverRadius": 5,
                                    "pointHoverBackgroundColor": "#70cbf4",
                                    "pointHoverBorderColor": "#70cbf4",
                                    "pointHoverBorderWidth": 2,
                                    "pointRadius": 4,
                                    "pointHitRadius": 10
                                },
                                {
                                    "type": ChartType.Line,
                                    "fill": false,
                                    "backgroundColor": "#fff",
                                    "borderColor": "#737373",
                                    "borderCapStyle": "butt",
                                    "borderDash": [
                                        10,
                                        10
                                    ],
                                    "borderDashOffset": 0,
                                    "borderJoinStyle": "miter",
                                    "lineTension": 0.3,
                                    "pointBackgroundColor": "#fff",
                                    "pointBorderColor": "#737373",
                                    "pointBorderWidth": 1,
                                    "pointHoverRadius": 5,
                                    "pointHoverBackgroundColor": "#737373",
                                    "pointHoverBorderColor": "#737373",
                                    "pointHoverBorderWidth": 2,
                                    "pointRadius": 4,
                                    "pointHitRadius": 10
                                },
                                {
                                    "backgroundColor": "#269B5C",
                                    "yAxisID": "bar-y-axis"
                                },
                                {
                                    "backgroundColor": "#F0B23C",
                                    "yAxisID": "bar-y-axis"
                                },
                                {
                                    "backgroundColor": "#D74742",
                                    "yAxisID": "bar-y-axis"
                                }
                            ]
                        }
                    }]
                },

                "chartOne": {
                    "chartType": ChartType.Bar,
                    "chartOptions": ChartOptions.TrendsMixed,
                    "chartData": {
                        "datasets": [
                            {
                                "type": ChartType.Line,
                                "fill": false,
                                "backgroundColor": "#fff",
                                "borderColor": "#70cbf4",
                                "borderCapStyle": "butt",
                                "borderDash": [],
                                "borderDashOffset": 0,
                                "borderJoinStyle": "miter",
                                "lineTension": 0.3,
                                "pointBackgroundColor": "#fff",
                                "pointBorderColor": "#70cbf4",
                                "pointBorderWidth": 1,
                                "pointHoverRadius": 5,
                                "pointHoverBackgroundColor": "#70cbf4",
                                "pointHoverBorderColor": "#70cbf4",
                                "pointHoverBorderWidth": 2,
                                "pointRadius": 4,
                                "pointHitRadius": 10
                            },
                            {
                                "type": ChartType.Line,
                                "fill": false,
                                "backgroundColor": "#fff",
                                "borderColor": "#737373",
                                "borderCapStyle": "butt",
                                "borderDash": [
                                    10,
                                    10
                                ],
                                "borderDashOffset": 0,
                                "borderJoinStyle": "miter",
                                "lineTension": 0.3,
                                "pointBackgroundColor": "#fff",
                                "pointBorderColor": "#737373",
                                "pointBorderWidth": 1,
                                "pointHoverRadius": 5,
                                "pointHoverBackgroundColor": "#737373",
                                "pointHoverBorderColor": "#737373",
                                "pointHoverBorderWidth": 2,
                                "pointRadius": 4,
                                "pointHitRadius": 10
                            },
                            {
                                "backgroundColor": "#269B5C",
                                "yAxisID": "bar-y-axis"
                            },
                            {
                                "backgroundColor": "#F0B23C",
                                "yAxisID": "bar-y-axis"
                            },
                            {
                                "backgroundColor": "#D74742",
                                "yAxisID": "bar-y-axis"
                            }
                        ]
                    }
                },
                "chartTwo": {
                    "chartType": ChartType.PolarArea,
                    "chartOptions": ChartOptions.PolarArea,
                    "chartData": {
                        "datasets": [{
                           "backgroundColor": [
                                "#FF6384",
                                "#4BC0C0",
                                "#FFCE56",
                                "#E7E9ED",
                                "#36A2EB"
                            ]
                        }]
                    }
                },
                "chartThree": {
                    "chartType": ChartType.Pie,
                    "chartOptions": ChartOptions.Pie,
                    "chartData": {
                        "datasets": [{
                            "backgroundColor": ["#4688F1", "#D9463D"]
                        }]
                    }
                }
            }
        }
    }
};
