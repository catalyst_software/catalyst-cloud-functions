import { sum } from 'lodash';

import { BiometricPainEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { PainTrendData } from '../../types/pain-trend-data';
import { getAggregateValues } from '../../getAggregateValues';
import { getEntriesIndicator } from '../../getEntriesIndicator';
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { sortByValue } from '../../sortByHelpers';

export const getGroupedByAveragePain = (athlteteTrainingEntries: BiometricPainEntrySchema[]) => {

    const painLevelData: PainTrendData = [];
    // const selectedIntesitylevels = [];
    // from(groupedByDateAndAverageMinutes)
    //     .pipe(
    //         // groupBy(entry => entry.intensityLevel),
    //         // map((grouped) => {
    //         //     console.log('grouped', grouped)
    //         //     selectedIntesitylevels.push(grouped.key)
    //         //     return grouped
    //         // }),
    //         // mergeMap(intensityLevelGroup => zip(of(intensityLevelGroup.key), intensityLevelGroup.pipe(toArray())))
    //     )
    //     .subscribe((groupedByIntensityLevel) => {

    const {
        meanValue: meanValueOfPain,
        lowValue: lowValueOfPain,
        highValue: highValueOfPain,
        totalValue: totalValueOfPain,
        countValue: countValueOfPain
    } = getAggregateValues(athlteteTrainingEntries);

    const painLevelValues = {
        meanValue: meanValueOfPain,
        lowValue: lowValueOfPain,
        highValue: highValueOfPain,
        totalValue: totalValueOfPain,
        countValue: countValueOfPain,
        entries: getEntriesIndicator(athlteteTrainingEntries).sort(sortByValue),
        avg: null
    };

    painLevelData.push({
        athleteID: athlteteTrainingEntries[0].athleteID,
        firstName: athlteteTrainingEntries[0].userFirstName,
        fullName: athlteteTrainingEntries[0].userFullName,
        ...painLevelValues
    });
    // });

    const totalCount = sum(painLevelData.map((level) => level.totalValue));

    painLevelData.map((intesityGroup) => {
        const avgOutOfHundred = intesityGroup.totalValue / totalCount * 100;
        intesityGroup.avg = avgOutOfHundred;
    });
    
    removeUndefinedProps(painLevelData)

    return painLevelData;
};
