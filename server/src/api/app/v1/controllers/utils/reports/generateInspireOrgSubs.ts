import { ReportRequest } from '../../../../../../models/report-model';

import { db } from '../../../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../../../db/biometricEntries/enums/firestore-collections';
import { getDocumentFromSnapshot } from '../../../../../../analytics/triggers/utils/get-document-from-snapshot';
import { getAthleteDocFromSnapshot } from '../../../../../../db/refs';
import { Athlete } from '../../../../../../models/athlete/athlete.model';

export type iosNotification = {
    notificationType: string;
    originalTransactionId: number;
    originalPurchaseDate: Date;
    unhandledProps: {
        subscription_group_identifier: string;
        app_item_id: string;
    };
    uniqueIdentifier: string;
    isTrialPeriod: boolean;
    webOrderLineItemId: number;
    platform: string;
    productId: string;
    itemId: number;
    uniqueVendorIdentifier: string;
    bundleId: string;
    transactionId: number;
    expirationDate: Date;
    creationTimestamp: Date;
    purchaseDate: Date;
    isInIntroOfferPeriod: boolean;
};

type PurchaseReceipt = {
    originalPurchaseDate: Date;
    creationTimestamp: Date;
    startDate: Date;
    autoRenewProductId: string;
    app_item_id: string;
    notificationType: string;
    isTrialPeriod: boolean;
    platform: string;
    sandbox: boolean;
    status: string;
    quantity: number;
    webOrderLineItemId: number;
    bundleId: string;
    autoRenewing: boolean;
    purchaseDate: Date;
    productId: string;
    expirationDate: Date;
    uniqueIdentifier: string;
    isTrial: boolean;
    isInIntroOfferPeriod: boolean;
    subscription_group_identifier: string;
    cancellationDate: number;
    originalTransactionId: number;
    isInBillingRetryPeriod: boolean;
    itemId: number;
    transactionId: number;
};

export type TransactionAuditRecord = {
    notifications: iosNotification[];
    purchaseReceipt: PurchaseReceipt;
    uid: string;
    athleteUid: string;
    firstName?: string;
    lastName?: string;
};


const doc
    : TransactionAuditRecord
    = {
    athleteUid: '',
    "notifications": [
        {
            "notificationType": "DID_CHANGE_RENEWAL_STATUS",
            "originalTransactionId": 160000741988518,
            "originalPurchaseDate": new Date(),
            "unhandledProps": {
                "subscription_group_identifier": "20613404",
                "app_item_id": "1504901226"
            },
            "uniqueIdentifier": "00008030-001D59D62142802E",
            "isTrialPeriod": true,
            "webOrderLineItemId": 160000272608264,
            "platform": "iOS",
            "productId": "com.sof.prod.subscriptions.monthly.basic",
            "itemId": 1504902462,
            "uniqueVendorIdentifier": "75F9B01F-D74A-4956-BBB0-158E492961F0",
            "bundleId": "com.inspiresportonline.sof.ios",
            "transactionId": 160000741988518,
            "expirationDate": new Date(),
            "creationTimestamp": new Date(),
            "purchaseDate": new Date(),
            "isInIntroOfferPeriod": true
        }
    ],
    "purchaseReceipt": {
        "originalPurchaseDate": new Date(),
        "creationTimestamp": new Date(),
        "startDate": new Date(),
        "autoRenewProductId": "com.sof.prod.subscriptions.monthly.basic",
        "app_item_id": "1504901226",
        "notificationType": "VALIDATING_INITIAL_BUY",
        "isTrialPeriod": true,
        "platform": "iOS",
        "sandbox": false,
        "status": "success",
        "quantity": 1,
        "webOrderLineItemId": 160000272608264,
        "bundleId": "com.inspiresportonline.sof.ios",
        "autoRenewing": true,
        "purchaseDate": new Date(),
        "productId": "com.sof.prod.subscriptions.monthly.basic",
        "expirationDate": new Date(),
        "uniqueIdentifier": "00008030-001D59D62142802E",
        "isTrial": true,
        "isInIntroOfferPeriod": false,
        "subscription_group_identifier": "20613404",
        "cancellationDate": 0,
        "originalTransactionId": 160000741988518,
        "isInBillingRetryPeriod": false,
        "itemId": 1504902462,
        "transactionId": 160000741988518,
    },
    "uid": "160000741988518"
}

export const generateInspireOrgSubs = async (request: ReportRequest) => {

    // const reportResults2 = await generateReport(undefined, requestingUser);
    // return reportResults2

    const allNotifications = []
    // = await db.collection(FirestoreCollection.IOSNotifications)
    //     .where("iosNotification.auto_renew_product_id", ">=", "com.sof")

    //    //     .where("iosNotification.unified_receipt.latest_receipt_info[0].transaction_id", ">=", "com.sof")

    //     .orderBy("iosNotification.auto_renew_product_id", "asc")
    //     .get()
    //     .then((querySnap) => {
    //         return querySnap.docs.map((docSnap) => {
    //             return getDocumentFromSnapshot(docSnap) as {
    //                 uid: string,
    //                 athleteUid: string,
    //                 creationTimestamp: Date,
    //                 iosNotification: {
    //                     notification_type: string,

    //                     auto_renew_status: string,

    //                     auto_renew_status_change_date: string,
    //                     unified_receipt: {
    //                         latest_receipt: string,
    //                         auto_renew_product_id: string,
    //                         latest_receipt_info: any,
    //                     },
    //                 }
    //                 platform: string
    //             }
    //         })
    //     })

    // const athletesRef = await db.collection(FirestoreCollection.Athletes)
    //     .where("organizationId", "==", "bgRE62Gjr8vWfNDizBye")
    //     .orderBy("metadata.creationTimestamp", "asc")

    const allResults // = [];
        = await db.collectionGroup('transactionAuditRecord')
            .where("purchaseReceipt.bundleId", ">=", "com.inspiresportonline.sof.")
            .get()
            .then((querySnap) => {
                return Promise.all(querySnap.docs.map(async (docSnap) => {

                    const athleteSnap = await docSnap.ref.parent.parent.get();
                    const athDoc = getDocumentFromSnapshot(athleteSnap) as Athlete
                    const transactionRecord = (getDocumentFromSnapshot(docSnap) as TransactionAuditRecord);

                    return {
                        ...transactionRecord,
                        athleteUid: docSnap.ref.parent.parent.id,
                        firstName: athDoc.profile.firstName,
                        lastName: athDoc.profile.lastName

                    }
                }))
            })

    return {
        success: true,
        message: `allResults length: ${allResults.length}`,
        // TODO: 
        allResults: allResults.filter((r) => r.purchaseReceipt.bundleId.indexOf('com.inspiresportonline.sof') !== -1),
        allNotifications,
        taskId: 'allResults'
    };
};
