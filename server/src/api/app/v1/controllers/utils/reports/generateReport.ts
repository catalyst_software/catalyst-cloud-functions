import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { RedFlagReportSchema, HeaderType } from '../../../../../../pubsub/interfaces/red-flag-reports';
import { sendNotification } from './sendNotification';
import { message } from "./message";

export const generateReport = async (theData: RedFlagReportSchema, coach: AthleteIndicator) => {
    // const jsreportAdmin = 'johannes@inspiresportonline.com';
    // const jsreportPassword = 'YgQAkkkQFxehuV6';
    // // const client = jsreport('https://inspire.jsreportonline.net/', jsreportAdmin, jsreportPassword); 
    // // const client = require('jsreport-client')('http://localhost:5488')
    // const client = require('jsreport-client')('https://inspire.jsreportonline.net/', jsreportAdmin, jsreportPassword);
    // const thee = {
    //     header: {
    //         fromDate: "string",
    //         toDate: "",
    //         headerType: HeaderType.Organization,
    //         title: {
    //             organization: "Test", group: "Group 1", athlete: ""
    //         }
    //     },
    //     brainSummary: {
    //         include: true,
    //         data: {
    //             mood: {
    //                 average: "2.5714285714285716",
    //                 athletes: [{ name: "Travis Brannon", value: 3 }, { name: "Travis Brannon", value: 2 }, { name: "Ian Kaapi", value: 1.5 }]
    //             },
    //             sleep: {
    //                 average: "5",
    //                 athletes: [{ name: "Ian Kaapi", value: 5 }]
    //             }
    //         }
    //     }, bodySummary: {
    //         include: true,
    //         data: {
    //             fatigue: {
    //                 average: "3.2857142857142856",
    //                 athletes: [{ name: "Ian Kaapi", value: 8 }, { name: "Travis Brannon", value: 5 }]
    //             },
    //             pain: {
    //                 average: "3.875",
    //                 athletes: [{ name: "Ian Kaapi", value: 5 }, { name: "Travis Brannon", value: 4 }]
    //             }
    //         }
    //     },
    //     foodSummary: {
    //         include: true,
    //         data: {}
    //     },
    //     trainingSummary: {
    //         include: true,
    //         data: {}
    //     },
    //     engagementSummary: {
    //         include: true,
    //         data: {}
    //     },
    //     moodTrends: {
    //         include: true,
    //         data: {}
    //     },
    //     sleepTrends: {
    //         include: true,
    //         data: {}
    //     },
    //     fatigueTrends: {
    //         include: true,
    //         data: {}
    //     },
    //     trainingTrends: {
    //         include: true,
    //         data: {}
    //     },
    //     painTrends: {
    //         include: true,
    //         data: {}
    //     }
    // };
    async function render(theCoach: AthleteIndicator) {
        // const renderResponse = await client.render({
        //     // template: { "shortid": "Bye_dtxnV" },
        //     template: { "shortid": "r1K-R8l6E" },
        //     recipe: 'chrome-pdf',
        //     engine: 'handlebars',
        //     data: {
        //         ...theData,
        //         "A": [12, 19, 15, 15, 12, 13, 20],
        //         "B": [24, 19, 23, 17, 26, 23, 27]
        //     }
        // });
        // const bodyBuffer = await renderResponse.body();
        // message.attachments = [{
        //     content: bodyBuffer.toString('base64'),
        //     filename: 'thereport.pdf',
        //     type: 'application/pdf',
        //     disposition: 'attachment',
        //     contentId: 'Reports!'
        // }];
        // const sendMailStatus = await sendEmail([{
        //     name: theCoach.firstName + ' ' + theCoach.lastName,
        //     email: theCoach.email
        // }], message, sgKey);
        // console.log(bodyBuffer.toString());
        // console.log(sendMailStatus);
        return await sendNotification(coach, message);
        // return sendMailStatus;
    }
    const allResults = await render(coach).catch((err) => {
        console.error(err);
        throw err;
    });
    return allResults;
};
