import { BiometricEntrySchema } from '../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../shared/utils/groupByMap';
import { sortByValueDesc } from "../sortByHelpers";

export const getSadEntries = (moodEntries: Array<BiometricEntrySchema>): Array<BiometricEntrySchema> => {
    const entries = moodEntries.sort(sortByValueDesc).filter((entry) => entry.value <= 2) || [];
    const theEntries = [];
    const groupedEntries = groupByMap(entries, (entry: BiometricEntrySchema) => entry.userFirstName + '__' + entry.athleteID) as Map<string, Array<BiometricEntrySchema>>;
    groupedEntries.forEach((athleteEntries) => {
        const res = {
            athleteID: athleteEntries[0].athleteID,
            name: athleteEntries[0].userFullName,
            value: athleteEntries.length //[0].meanValue
        };
        theEntries.push(res);
    });
    return theEntries.sort(sortByValueDesc);
};
