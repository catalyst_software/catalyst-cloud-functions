import moment from 'moment';
import { sum } from 'lodash';

import { BiometricPainEntrySchema, BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';

import { PainTrendData, PainByBodyLocationData } from '../../types/pain-trend-data';

import { sortByValue } from "../../sortByHelpers";
import { getMeanOfValue } from "../../getMeanOfValue";
import { getLowestValue } from '../../getLowestValue';
import { getHighestValue } from '../../getHighestValue';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
import { getGroupedByPainType } from '../pain/getGroupedByPainType';
import { getGroupedByBodyLocation } from '../pain/getGroupedByBodyLocation';
import { getSumOfValue } from '../../getSumOfValue';
import { getEntriesIndicator } from '../../getEntriesIndicator';
import { getStackedChartData } from './getStackedChartData';
import { getGroupedByDay } from './getGroupedByDay';
// import { getLevelByBodyLocation } from '../pain/getLevelByBodyLocation';

export type PainMapPerAthleteMeta = {
    uid?: string;
    firstName?: string;
    fullName?: string;

    lowValue: number;
    highValue: number;
    meanValue: number;
    totalValue: number;
    countValue: number;

    avg: number;

    entries: Array<BiometricEntrySchema>;
    groupedByDateAndAverageMinutes?: {
        entries: Array<PainMapPerAthleteMeta>
    };
    painLevelPerType?: any;
    levelByBodyLocation?: any;
};

export const getPainMap = (painEntries: Array<BiometricEntrySchema>, setAllToToday: boolean, startOfWeek: moment.Moment, daysOfWeek: {
    date: Date;
    uid?: string;
    lineChartData: any;
    entries: BiometricEntrySchema[]
}[]): {
    complexChartData: Array<PainMapPerAthleteMeta>,
    painLevelPerType: any,
    levelByBodyLocation: any,
    stakedChartData
} => {


    const thePainEntries: Array<PainMapPerAthleteMeta> = [];

    // from(painEntries
    //     // .filter((entry) => entry.value < 4)
    //     // .map((entry) => {
    //     //     const defaultValues = getDefaultValues(entry)
    //     //     return {
    //     //         ...defaultValues,
    //     //         painType: entry.painType,
    //     //         paynTypeID: entry.painTypeID,
    //     //         bodyLocation: entry.bodyLocation,
    //     //         bodyLocationID: entry.bodyLocationID,
    //     //         name: entry.userFullName,
    //     //         value: entry.value
    //     //     };
    //     // })
    // )
    //     .pipe(
    //         groupBy(person => person.athleteID, p => p),
    //         mergeMap(personGroup => zip(of(personGroup.key), personGroup.pipe(toArray()))))
    //     .subscribe((painEntriesGroupedByAthlete) => {

    //         // console.log(painEntriesGroupedByAthlete);

    //         const athletePainEntries = painEntriesGroupedByAthlete[1] as BiometricPainEntrySchema[];


    //         // getAggreateValues(athletePainEntries);


    //         const groupedByPainLevelPerTypeMap = getGroupedByPainType(athletePainEntries);

    //         const levelByBodyLocationMap = getGroupedByBodyLocation(athletePainEntries);

    //         const groupByDate = groupByMap(athletePainEntries, (entry: BiometricPainEntrySchema) => moment(entry.creationTimestamp).format('DD-MM-YYYY'));

    //         const groupedByAveragePain = []

    //         groupByDate.forEach((entries: Array<BiometricPainEntrySchema>, theDate: String) => {

    //             const groupedByAveragePainMap = getGroupedByAveragePain(entries);

    //             groupedByAveragePain.push({
    //                 date: theDate,
    //                 groupedByDateAndIntensityLevel: groupedByAveragePainMap
    //             })

    //         });

    //         // groupedByAveragePain.map((val, ent) => {
    //         //     console.log('val', val)
    //         //     console.log('ent', ent)
    //         // })

    //         removeUndefinedProps(athletePainEntries);


    //         const lowValue = getLowestValue(athletePainEntries);
    //         const highValue = getHighestValue(athletePainEntries);
    //         const meanValue = getMeanOfValue(athletePainEntries);
    //         const totalValue = getSumOfValue(athletePainEntries);
    //         const countValue = athletePainEntries.length;

    //         const mappedData = {

    //             lowValue,
    //             highValue,
    //             meanValue,
    //             totalValue,
    //             countValue,

    //             groupedByAveragePain,
    //             painLevelPerType: groupedByPainLevelPerTypeMap,
    //             levelByBodyLocation: levelByBodyLocationMap,

    //             entries: getEntriesIndicator(athletePainEntries)
    //                 .sort(sortByValue),

    //             avg: undefined
    //         }

    //         thePainEntries.push(mappedData);

    //         // athletePainEntries.forEach((entry) => {
    //         //     removeUndefinedProps(entry);
    //         //     entry.lowValue = lowValue;
    //         //     entry.highValue = highValue;
    //         //     entry.meanValue = meanValue;
    //         //     entry.countValue = athletePainEntries.length;
    //         // });

    //         // theEntries.push(...athletePainEntries);
    //     });








    const athltetePainEntries = painEntries // trainingEntriesGroupedByAthlete[1] as Array<BiometricTrainingEntrySchema>;

    getGroupedByDay(setAllToToday, athltetePainEntries, startOfWeek, daysOfWeek);

    const groupedByDateAndAverageMinutes = {
        // uid: athleteID,
        entries: []
    }

    daysOfWeek.forEach((day: { date: Date; uid: string; entries: BiometricPainEntrySchema[], lineChartData: any }) => {

        // const groupedByDateAndIntensityLevelMap = getGroupedByIntensityLevel(day.entries);
        // if (groupedByDateAndIntensityLevel[athleteID] === undefined)
        //     groupedByDateAndIntensityLevel[athleteID] = []

        const periodMeanValue = getMeanOfValue(day.entries);
        const periodLowValue = getLowestValue(day.entries);
        const periodHighValue = getHighestValue(day.entries);
        const periodTotalValue = getSumOfValue(day.entries);
        const periodCountValue = day.entries.length;

        groupedByDateAndAverageMinutes.entries.push({
            date: day.date.toISOString(),
            // groupedByDateAndIntensityLevel: groupedByDateAndIntensityLevelMap,
            meanValue: periodMeanValue,
            lowValue: periodLowValue,
            highValue: periodHighValue,
            totalValue: periodTotalValue,
            countValue: periodCountValue
        })

        // groupedByDateAndIntensityLevel[athleteID].push({

        //     date: day.date.toISOString(),
        //     groupedByDateAndIntensityLevel: groupedByDateAndIntensityLevelMap,
        //     meanValue: periodMeanValue,
        //     lowValue: periodLowValue,
        //     highValue: periodHighValue,
        //     totalValue: periodTotalValue,
        //     countValue: periodCountValue
        // })

    });

    // groupedByDateAndIntensityLevel.map((val, ent) => {
    //     console.log('val', val)
    //     console.log('ent', ent)
    // })

    removeUndefinedProps(athltetePainEntries);

    const meanValue = getMeanOfValue(athltetePainEntries);
    const lowValue = getLowestValue(athltetePainEntries);
    const highValue = getHighestValue(athltetePainEntries);
    const totalValue = getSumOfValue(athltetePainEntries);
    const countValue = athltetePainEntries.length;

    // groupedByDateAndIntensityLevel.push(atData)

    const mappedData = {
        // uid: trainingEntriesGroupedByAthlete[0],
        // firstName: athltetePainEntries[0].userFirstName,
        // fullName: athltetePainEntries[0].userFullName,
        lowValue,
        highValue,
        meanValue,
        totalValue,
        countValue,
        // groupedByDateAndIntensityLevel: groupedByDateAndIntensityLevel, //.filter((at) => at.uid === trainingEntriesGroupedByAthlete[0]),
        groupedByDateAndAverageMinutes,
        entries: getEntriesIndicator(athltetePainEntries)
            // .map((entry) => {
            //     return {
            //         creationTimestamp: entry.creationTimestamp,
            //         lifestyleEntryType: entry.lifestyleEntryType,
            //         lifestyleEntryTypeID: entry.lifestyleEntryTypeID,
            //         painType: entry.painType,
            //         painTypeID: entry.painTypeID,
            //         value: entry.value
            //     }
            // })
            .sort(sortByValue),
        avg: undefined
    }



    thePainEntries.push(mappedData);



    const groupedByTotalPainLevelPerTypeMap: PainTrendData = getGroupedByPainType(painEntries).map((data) => {
        // const { firstName, fullName, athleteID, ...theRestData } = data
        const theRestData = data;

        return {
            ...theRestData
        }
    });

    const levelByTotalBodyLocationMap: PainByBodyLocationData = getGroupedByBodyLocation(painEntries).map((data) => {

        // const { firstName, fullName, athleteID: uid, ...theRestData } = data
        const theRestData = data;
        return {
            ...theRestData
        }
    });


    const totalCount = sum(thePainEntries.map((level) => level.totalValue));

    thePainEntries.map((intesityGroup) => {
        const avgOutOfHundred = intesityGroup.totalValue / totalCount * 100;
        intesityGroup.avg = avgOutOfHundred;
    });

    const stackedChartDatasets = []

    const groupedEntriesByAthlete = groupByMap(painEntries.sort(sortByValue), (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID);

    groupedEntriesByAthlete.forEach((athleteEntries: Array<BiometricEntrySchema>, uid: string) => {

        const athleteFullName = uid.split('__')[0]
        const athleteID = uid.split('__')[1]

        // Stacked Chart Data

        getStackedChartData(athleteEntries, athleteID, athleteFullName, startOfWeek, daysOfWeek, stackedChartDatasets);

    })

    const labels = daysOfWeek.map((day) => day.date.toDateString())

    const stakedData = {
        labels: labels,
        datasets: stackedChartDatasets
    }

    const theResults = {
        complexChartData: thePainEntries,
        stakedChartData: stakedData,
        painLevelPerType: groupedByTotalPainLevelPerTypeMap,
        levelByBodyLocation: levelByTotalBodyLocationMap
    };


    removeUndefinedProps(theResults)

    return theResults
};


