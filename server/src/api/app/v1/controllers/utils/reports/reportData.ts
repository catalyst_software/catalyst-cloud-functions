export const reportData = {
    "header": {
        "fromDate": "May 12, 2019",
        "toDate": "May 18, 2019",
        "headerType": "athlete",
        "title": {
            "organization": "CARMEL COLLEGE",
            "team": "The Rebels",
            "group": "Group 1",
            "athlete": ""
        }
    },
    "redFlags": {
        "brainSummary": {
            "include": true,
            "data": {
                "useBarChart": true,
                "summaryData": {
                    "title": "Brain",
                    "dataSets": [
                        [
                            {
                                "subject": "average mood!!!",
                                "value": 3.5314686
                            }
                        ],
                        [
                            {
                                "subject": "average sleep!!!",
                                "value": 6.826923
                            }
                        ]
                    ]
                },
                "redFlagGroups": [
                    {
                        "image": "icons8-sad-96.png",
                        "label": "entries with sad or very sad!",
                        "chartType": "horizontalBar",
                        "chartData": {
                            "labels": [
                                "Chris 185",
                                "Chris Wb",
                                "Travis Coach",
                                "Ian Ins 9 Athlete Gouws",
                                "Travis Brannon",
                                "Ian Inspire 2 Gouws",
                                "Travis Brannon",
                                "Kathryn Mellon",
                                "Ian Kaapi",
                                "Kathryn Mellon",
                                "Annie Flamsteed",
                                "Johannes De Boeck",
                                "Travis Brannon",
                                "Travis Athlete",
                                "Travis Athlete"
                            ],
                            "datasets": [
                                {
                                    "data": [
                                        4,
                                        3.5555556,
                                        3.5,
                                        3.25,
                                        3.142857,
                                        3,
                                        3,
                                        2.6666667,
                                        2.5,
                                        2.5,
                                        2.3333333,
                                        2.2,
                                        2,
                                        1.4,
                                        1
                                    ],
                                    "backgroundColor": [
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1"
                                    ]
                                }
                            ]
                        }
                    },
                    {
                        "image": "icons8-insomnia-96.png",
                        "label": "less than 4 hours sleep!",
                        "chartType": "horizontalBar",
                        "chartData": {
                            "labels": [
                                "Travis Coach",
                                "Chris Wb",
                                "Ian Ins 9 Athlete Gouws",
                                "Ian Kaapi",
                                "Travis Brannon",
                                "Johannes De Boeck"
                            ],
                            "datasets": [
                                {
                                    "data": [
                                        7.6666665,
                                        6.6666665,
                                        4.5,
                                        4.4,
                                        4.3333335,
                                        1
                                    ],
                                    "backgroundColor": [
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1"
                                    ]
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "bodySummary": {
            "include": true,
            "data": {
                "useBarChart": true,
                "summaryData": {
                    "title": "Body",
                    "dataSets": [
                        [
                            {
                                "subject": "average fatigue!!!",
                                "value": 2.9038463
                            }
                        ],
                        [
                            {
                                "subject": "average pain!!!",
                                "value": 3.2105262
                            }
                        ]
                    ]
                },
                "redFlagGroups": [
                    {
                        "image": "icons8-bored-96.png",
                        "label": "entries >= 4!!",
                        "chartType": "horizontalBar",
                        "chartData": {
                            "labels": [
                                "Travis Coach",
                                "Chris Test",
                                "Chris Wb",
                                "Travis Brannon",
                                "Travis Brannon",
                                "Travis Athlete",
                                "Kathryn Mellon",
                                "Ian Ins5 Athlete Gouws",
                                "Ian Ins4 Athlete Gouws",
                                "Ian Ins 4-2 Athlete Gouws",
                                "Ian Ins 6 Gouws",
                                "Ian in 5 Gouws",
                                "ian.inspire Gouws",
                                "Ian Kaapi",
                                "Kathryn Mellon",
                                "Travis Dev",
                                "Annie Flamsteed",
                                "Travis Brannon",
                                "Travis Athlete",
                                "Ian Inspire 2 Gouws",
                                "Chris Wb",
                                "Travis Coach",
                                "Ian Insp 4 Gouws",
                                "Travis Brannon",
                                "Travis Brannon",
                                "Ian Ins 9 Athlete Gouws",
                                "Travis Athlete",
                                "Travis Brannon",
                                "Travis Brannon"
                            ],
                            "datasets": [
                                {
                                    "data": [
                                        12,
                                        11,
                                        10,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        8,
                                        6,
                                        6,
                                        5
                                    ],
                                    "backgroundColor": [
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1"
                                    ]
                                }
                            ]
                        }
                    },
                    {
                        "image": "icons8-crutch-96.png",
                        "label": "entries >= 4!!",
                        "chartType": "horizontalBar",
                        "chartData": {
                            "labels": [
                                "Ian Ins5 Athlete Gouws",
                                "Ian Kaapi",
                                "Chris Wb",
                                "Travis Athlete",
                                "Johannes De Boeck",
                                "Travis Brannon",
                                "Ian in 5 Gouws",
                                "Travis Coach",
                                "Travis Brannon",
                                "Ian Insp 4 Gouws",
                                "Ian Ins4 Athlete Gouws",
                                "Travis Brannon",
                                "Travis Coach",
                                "Ian Ins 4 Gouws"
                            ],
                            "datasets": [
                                {
                                    "data": [
                                        5,
                                        5,
                                        5,
                                        5,
                                        5,
                                        4,
                                        4,
                                        4,
                                        4,
                                        4,
                                        4,
                                        4,
                                        4,
                                        4
                                    ],
                                    "backgroundColor": [
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1",
                                        "#F3C7C4",
                                        "#4688F1"
                                    ]
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "foodSummary": {
            "include": true,
            "data": {
                "usePieChart": true,
                "summaryData": {
                    "title": "Food",
                    "heading": "average serve counts",
                    "dataSets": [
                        [
                            {
                                "subject": "breakfast!",
                                "value": 3.1795263
                            },
                            {
                                "subject": "lunch!",
                                "value": 3.0284615
                            }
                        ],
                        [
                            {
                                "subject": "snack!",
                                "value": 2.5384614
                            },
                            {
                                "subject": "dinner!",
                                "value": 2.4484615
                            }
                        ],
                        [
                            {
                                "subject": "other!",
                                "value": 1.9384615
                            }
                        ]
                    ]
                },
                "pieChartData": {
                    "label": "entries >= 4!!",
                    "chartType": "pie",
                    "chartData": {
                        "labels": [
                            "Grains & cereal foods",
                            "Fruits",
                            "Vegetables and Legumes",
                            "Lean meats and poultry, fish, eggs, tofu, nuts, seeds and beans.",
                            "Milk, yogurt, cheese and alternatives",
                            "Other",
                            "shake",
                            "Treats"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    49,
                                    45,
                                    30,
                                    27,
                                    25,
                                    16,
                                    14,
                                    10
                                ],
                                "backgroundColor": [
                                    "#FD704B",
                                    "#4688F1",
                                    "#9E9C31",
                                    "#1D9C5A",
                                    "#D9453D",
                                    "#1BACC0",
                                    "#F2B32A"
                                ]
                            }
                        ]
                    }
                }
            }
        },
        "trainingSummary": {
            "include": true,
            "data": {
                "usePieChart": true,
                "summaryData": {
                    "title": "Training",
                    "dataSets": [
                        [
                            {
                                "subject": "average session length!!!",
                                "value": 43.210526
                            }
                        ]
                    ]
                },
                "pieChartData": {
                    "label": "entries >= 4!!",
                    "chartType": "pie",
                    "chartData": {
                        "labels": [
                            "Triathlon!",
                            "Cycling!",
                            "Rugby!",
                            "Athletics!",
                            "Yoga!",
                            "Other!"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    49,
                                    45,
                                    30,
                                    27,
                                    25,
                                    100
                                ],
                                "backgroundColor": [
                                    "#FD704B",
                                    "#4688F1",
                                    "#9E9C31",
                                    "#1D9C5A",
                                    "#D9453D",
                                    "#1BACC0"
                                ]
                            }
                        ]
                    }
                }
            }
        }
    },
    "superstars": {
        "include": true,
        "data": {
            "superstarsGroups": [
                {
                    "label": "Engagement Superstars!",
                    "isSuperstars": true,
                    "chartType": "horizontalBar",
                    "chartData": {
                        "labels": [
                            "Ian Kaapi",
                            "Ian Insp 4 Gouws",
                            "Travis Coach",
                            "Ian Ins 9 Athlete Gouws",
                            "Travis Athlete",
                            "Johannes De Boeck",
                            "Chris Wb",
                            "Travis Brannon",
                            "Ian Inspire 2 Gouws",
                            "Travis Brannon",
                            "Travis Athlete",
                            "Travis Dev",
                            "Travis Coach",
                            "Ian Ins5 Athlete Gouws",
                            "Annie Flamsteed",
                            "Ian in 5 Gouws",
                            "ian.inspire Gouws",
                            "Kathryn Mellon",
                            "Chris 185",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Ian Ins4 Athlete Gouws",
                            "Ian Ins 6 Gouws",
                            "Kathryn Mellon",
                            "Travis Brannon",
                            "Lucas Terry",
                            "Travis Brannon",
                            "Chris Test",
                            "Travis Brannon",
                            "Chris Wb",
                            "Johannes DeBroeck",
                            "Travis Brannon",
                            "Lucas Terry",
                            "Eva Brannon",
                            "Travis Athlete",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Ian Inspire",
                            "Travis Brannon",
                            "Ian Ins 4-2 Athlete Gouws",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Ian 2 Gouws 2",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Chris 145-1",
                            "Travis Brannon",
                            "Travis Coach",
                            "Travis Brannon",
                            "Ian Ins 4 Gouws",
                            "Travis Coach",
                            "Ian Ins3 Coach Gouws",
                            "Ian Inspire 2 Gouws",
                            "Travis Coach",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Dddd"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    51,
                                    51,
                                    48,
                                    36,
                                    33,
                                    29,
                                    29,
                                    28,
                                    26,
                                    25,
                                    24,
                                    23,
                                    23,
                                    19,
                                    18,
                                    14,
                                    14,
                                    13,
                                    12,
                                    12,
                                    12,
                                    12,
                                    11,
                                    11,
                                    11,
                                    10,
                                    10,
                                    10,
                                    10,
                                    8,
                                    7,
                                    7,
                                    5,
                                    5,
                                    5,
                                    5,
                                    4,
                                    4,
                                    4,
                                    4,
                                    4,
                                    3,
                                    3,
                                    2,
                                    2,
                                    2,
                                    2,
                                    2,
                                    2,
                                    2,
                                    2,
                                    2,
                                    2,
                                    1,
                                    1,
                                    1,
                                    1,
                                    1,
                                    1
                                ],
                                "backgroundColor": [
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4"
                                ]
                            }
                        ]
                    }
                },
                {
                    "label": "Training Superstars!",
                    "isSuperstars": true,
                    "chartType": "horizontalBar",
                    "chartData": {
                        "labels": [
                            "Travis Coach",
                            "Travis Coach",
                            "Travis Coach",
                            "Chris Wb",
                            "Chris Wb",
                            "Chris Wb",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Ian Ins 9 Athlete Gouws",
                            "Ian Ins 9 Athlete Gouws",
                            "Ian Ins 9 Athlete Gouws",
                            "Ian Kaapi",
                            "Ian Insp 4 Gouws",
                            "Ian Insp 4 Gouws",
                            "Ian Kaapi",
                            "Ian Kaapi",
                            "Ian Kaapi",
                            "Ian Insp 4 Gouws",
                            "Ian Insp 4 Gouws",
                            "Ian Kaapi",
                            "Ian Kaapi",
                            "Ian Kaapi",
                            "Ian Kaapi",
                            "Ian Kaapi",
                            "Ian Kaapi",
                            "Travis Athlete",
                            "Chris 185",
                            "Travis Athlete",
                            "Travis Athlete",
                            "Travis Athlete",
                            "Travis Athlete",
                            "Travis Athlete",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Dev",
                            "Travis Dev",
                            "Travis Dev",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Johannes De Boeck",
                            "Johannes De Boeck",
                            "Johannes De Boeck",
                            "Lucas Terry",
                            "Chris Test",
                            "Travis Brannon",
                            "Ian Inspire 2 Gouws",
                            "Ian Inspire 2 Gouws",
                            "Travis Brannon",
                            "Kathryn Mellon",
                            "Annie Flamsteed",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Athlete",
                            "Travis Athlete",
                            "Travis Athlete",
                            "Travis Athlete",
                            "Annie Flamsteed",
                            "Kathryn Mellon",
                            "Travis Brannon",
                            "Chris Wb",
                            "Johannes DeBroeck",
                            "Ian Ins4 Athlete Gouws",
                            "Ian Ins 6 Gouws",
                            "Travis Brannon",
                            "Ian in 5 Gouws",
                            "Ian Ins5 Athlete Gouws",
                            "Eva Brannon",
                            "Travis Brannon",
                            "Travis Athlete",
                            "Ian Inspire 2 Gouws",
                            "Travis Coach",
                            "Travis Coach",
                            "Travis Coach"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    35,
                                    35,
                                    35,
                                    32,
                                    32,
                                    32,
                                    27,
                                    27,
                                    27,
                                    26,
                                    26,
                                    26,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    20,
                                    16,
                                    16,
                                    16,
                                    16,
                                    16,
                                    16,
                                    16,
                                    15,
                                    15,
                                    13,
                                    13,
                                    13,
                                    12,
                                    12,
                                    12,
                                    11,
                                    11,
                                    11,
                                    10,
                                    10,
                                    9,
                                    9,
                                    9,
                                    9,
                                    8,
                                    7,
                                    7,
                                    7,
                                    7,
                                    7,
                                    7,
                                    7,
                                    7,
                                    7,
                                    5,
                                    5,
                                    5,
                                    4,
                                    4,
                                    4,
                                    4,
                                    3,
                                    3,
                                    3,
                                    2,
                                    1,
                                    0,
                                    0,
                                    0,
                                    0
                                ],
                                "backgroundColor": [
                                    "#283879",
                                    "#283879",
                                    "#283879",
                                    "#283879",
                                    "#283879",
                                    "#283879",
                                    "#283879"
                                ]
                            }
                        ]
                    }
                },
                {
                    "label": "HAPPY CHAPPIES!",
                    "isSuperstars": true,
                    "chartType": "horizontalBar",
                    "chartData": {
                        "labels": [
                            "ian.inspire Gouws",
                            "ian.inspire Gouws",
                            "ian.inspire Gouws",
                            "ian.inspire Gouws",
                            "ian.inspire Gouws",
                            "ian.inspire Gouws",
                            "ian.inspire Gouws",
                            "Travis Coach",
                            "Travis Coach",
                            "Travis Coach",
                            "Travis Coach",
                            "Travis Coach",
                            "Travis Coach",
                            "Chris Wb",
                            "Chris Wb",
                            "Chris Wb",
                            "Chris Wb",
                            "Chris Wb",
                            "Chris Wb",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Ian Ins 9 Athlete Gouws",
                            "Ian Ins 9 Athlete Gouws",
                            "Ian Ins 9 Athlete Gouws",
                            "Ian Ins 9 Athlete Gouws",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Ian Insp 4 Gouws",
                            "Ian Kaapi",
                            "Ian Insp 4 Gouws",
                            "Travis Athlete",
                            "Chris 185",
                            "Chris 185",
                            "Travis Athlete",
                            "Travis Athlete",
                            "Travis Athlete",
                            "Chris 185",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Dev",
                            "Travis Dev",
                            "Travis Dev",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Johannes De Boeck",
                            "Chris Test",
                            "Chris Test",
                            "Lucas Terry",
                            "Lucas Terry",
                            "Travis Brannon",
                            "Ian Inspire 2 Gouws",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Coach",
                            "Kathryn Mellon",
                            "Travis Coach",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Kathryn Mellon",
                            "Chris 145-1",
                            "Lucas Terry",
                            "Chris Wb",
                            "Ian Ins 4-2 Athlete Gouws",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Ian Ins3 Coach Gouws",
                            "Travis Dddd",
                            "Travis Brannon",
                            "Ian Ins 6 Gouws",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Ian Ins4 Athlete Gouws",
                            "Travis Coach",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Travis Brannon",
                            "Johannes DeBroeck",
                            "Travis Brannon"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    38,
                                    38,
                                    38,
                                    38,
                                    38,
                                    38,
                                    38,
                                    35,
                                    35,
                                    35,
                                    35,
                                    35,
                                    35,
                                    32,
                                    32,
                                    32,
                                    32,
                                    32,
                                    32,
                                    27,
                                    27,
                                    27,
                                    27,
                                    27,
                                    27,
                                    26,
                                    26,
                                    26,
                                    26,
                                    22,
                                    22,
                                    22,
                                    22,
                                    20,
                                    20,
                                    20,
                                    16,
                                    16,
                                    16,
                                    16,
                                    16,
                                    16,
                                    16,
                                    15,
                                    15,
                                    15,
                                    13,
                                    13,
                                    13,
                                    12,
                                    12,
                                    11,
                                    10,
                                    10,
                                    10,
                                    10,
                                    9,
                                    9,
                                    9,
                                    9,
                                    9,
                                    8,
                                    8,
                                    8,
                                    7,
                                    7,
                                    5,
                                    5,
                                    5,
                                    5,
                                    5,
                                    5,
                                    5,
                                    5,
                                    5,
                                    4,
                                    4,
                                    4,
                                    4,
                                    4,
                                    4,
                                    4,
                                    4,
                                    4,
                                    4,
                                    4,
                                    4
                                ],
                                "backgroundColor": [
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4",
                                    "#F3C7C4"
                                ]
                            }
                        ]
                    }
                }
            ]
        }
    },
    "trends": [
        {
            "type": "mood",
            "include": true,
            "data": {
                "isTrends": true,
                "lineChart": {
                    "chartType": "line",
                    "chartData": {
                        "labels": [
                            "May 12,2019!",
                            "May 13,2019!",
                            "May 14,2019!",
                            "May 15,2019!",
                            "May 16,2019!",
                            "May 17,2019!",
                            "May 18,2019!"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    22,
                                    25,
                                    28,
                                    30,
                                    27,
                                    10,
                                    30
                                ],
                                "label": "Ian Kaapi",
                                "borderColor": "#e6e600",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    40,
                                    45,
                                    37,
                                    24,
                                    21,
                                    15,
                                    30
                                ],
                                "label": "Annie Flamsteed",
                                "borderColor": "#68838F",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    33,
                                    29,
                                    15,
                                    5,
                                    8,
                                    6,
                                    25
                                ],
                                "label": "Travis Branon",
                                "borderColor": "#00ff00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    22,
                                    14,
                                    15,
                                    25,
                                    48,
                                    26,
                                    5
                                ],
                                "label": "Ayva Alter",
                                "borderColor": "#C42963",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    3,
                                    9,
                                    5,
                                    15,
                                    28,
                                    16,
                                    35
                                ],
                                "label": "John Askew",
                                "borderColor": "#ff66ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    23,
                                    9,
                                    5,
                                    35,
                                    18,
                                    26,
                                    5
                                ],
                                "label": "Luke Skywalker",
                                "borderColor": "#4dd2ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    23,
                                    9,
                                    5,
                                    35,
                                    18,
                                    26,
                                    5
                                ],
                                "label": "Pierce Brosnan",
                                "borderColor": "#007399",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    23,
                                    9,
                                    5,
                                    35,
                                    18,
                                    26,
                                    5
                                ],
                                "label": "Anke Shake",
                                "borderColor": "#3333ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    3,
                                    29,
                                    15,
                                    5,
                                    28,
                                    36,
                                    15
                                ],
                                "label": "John Snow",
                                "borderColor": "#ff9900",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    13,
                                    19,
                                    25,
                                    15,
                                    48,
                                    16,
                                    25
                                ],
                                "label": "Jake Butler",
                                "borderColor": "#88cc00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            }
                        ]
                    }
                },
                "barChart": {
                    "chartType": "horizontalBar",
                    "chartData": {
                        "labels": [
                            "Ian Kaapi",
                            "Annie Flamsteed",
                            "Travis Brannon",
                            "Ayva Alter",
                            "John Askew",
                            "Luke Skywalker",
                            "Pierce Brosnan",
                            "Anke Shake",
                            "John Snow",
                            "Jake Butler"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    2.9,
                                    4,
                                    4,
                                    5,
                                    4,
                                    4,
                                    2.9,
                                    4,
                                    5,
                                    4
                                ],
                                "backgroundColor": [
                                    "#e6e600",
                                    "#68838F",
                                    "#00ff00",
                                    "#C42963",
                                    "#ff66ff",
                                    "#4dd2ff",
                                    "#007399",
                                    "#3333ff",
                                    "#ff9900",
                                    "#88cc00"
                                ]
                            }
                        ]
                    }
                }
            }
        },
        {
            "type": "sleep",
            "include": true,
            "data": {
                "isTrends": true,
                "lineChart": {
                    "chartType": "line",
                    "chartData": {
                        "labels": [
                            "May 12,2019!",
                            "May 13,2019!",
                            "May 14,2019!",
                            "May 15,2019!",
                            "May 16,2019!",
                            "May 17,2019!",
                            "May 18,2019!"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    22,
                                    25,
                                    28,
                                    30,
                                    27,
                                    10,
                                    30
                                ],
                                "label": "Ian Kaapi",
                                "borderColor": "#e6e600",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    40,
                                    45,
                                    37,
                                    24,
                                    21,
                                    15,
                                    30
                                ],
                                "label": "Annie Flamsteed",
                                "borderColor": "#68838F",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    33,
                                    29,
                                    15,
                                    5,
                                    8,
                                    6,
                                    25
                                ],
                                "label": "Travis Branon",
                                "borderColor": "#00ff00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    22,
                                    14,
                                    15,
                                    25,
                                    48,
                                    26,
                                    5
                                ],
                                "label": "Ayva Alter",
                                "borderColor": "#C42963",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    3,
                                    9,
                                    5,
                                    15,
                                    28,
                                    16,
                                    35
                                ],
                                "label": "John Askew",
                                "borderColor": "#ff66ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    23,
                                    9,
                                    5,
                                    35,
                                    18,
                                    26,
                                    5
                                ],
                                "label": "Luke Skywalker",
                                "borderColor": "#4dd2ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    23,
                                    9,
                                    5,
                                    35,
                                    18,
                                    26,
                                    5
                                ],
                                "label": "Pierce Brosnan",
                                "borderColor": "#007399",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    23,
                                    9,
                                    5,
                                    35,
                                    18,
                                    26,
                                    5
                                ],
                                "label": "Anke Shake",
                                "borderColor": "#3333ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    3,
                                    29,
                                    15,
                                    5,
                                    28,
                                    36,
                                    15
                                ],
                                "label": "John Snow",
                                "borderColor": "#ff9900",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    13,
                                    19,
                                    25,
                                    15,
                                    48,
                                    16,
                                    25
                                ],
                                "label": "Jake Butler",
                                "borderColor": "#88cc00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            }
                        ]
                    }
                },
                "barChart": {
                    "chartType": "horizontalBar",
                    "chartData": {
                        "labels": [
                            "Ian Kaapi",
                            "Annie Flamsteed",
                            "Travis Brannon",
                            "Ayva Alter",
                            "John Askew",
                            "Luke Skywalker",
                            "Pierce Brosnan",
                            "Anke Shake",
                            "John Snow",
                            "Jake Butler"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    2.9,
                                    4,
                                    4,
                                    5,
                                    4,
                                    4,
                                    2.9,
                                    4,
                                    5,
                                    4
                                ],
                                "backgroundColor": [
                                    "#e6e600",
                                    "#68838F",
                                    "#00ff00",
                                    "#C42963",
                                    "#ff66ff",
                                    "#4dd2ff",
                                    "#007399",
                                    "#3333ff",
                                    "#ff9900",
                                    "#88cc00"
                                ]
                            }
                        ]
                    }
                }
            }
        },
        {
            "type": "fatigue",
            "include": true,
            "data": {
                "isTrends": true,
                "lineChart": {
                    "chartType": "line",
                    "chartData": {
                        "labels": [
                            "May 12,2019!",
                            "May 13,2019!",
                            "May 14,2019!",
                            "May 15,2019!",
                            "May 16,2019!",
                            "May 17,2019!",
                            "May 18,2019!"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    22,
                                    25,
                                    28,
                                    30,
                                    27,
                                    10,
                                    30
                                ],
                                "label": "Ian Kaapi",
                                "borderColor": "#e6e600",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    40,
                                    45,
                                    37,
                                    24,
                                    21,
                                    15,
                                    30
                                ],
                                "label": "Annie Flamsteed",
                                "borderColor": "#68838F",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    33,
                                    29,
                                    15,
                                    5,
                                    8,
                                    6,
                                    25
                                ],
                                "label": "Travis Branon",
                                "borderColor": "#00ff00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    22,
                                    14,
                                    15,
                                    25,
                                    48,
                                    26,
                                    5
                                ],
                                "label": "Ayva Alter",
                                "borderColor": "#C42963",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    3,
                                    9,
                                    5,
                                    15,
                                    28,
                                    16,
                                    35
                                ],
                                "label": "John Askew",
                                "borderColor": "#ff66ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    23,
                                    9,
                                    5,
                                    35,
                                    18,
                                    26,
                                    5
                                ],
                                "label": "Luke Skywalker",
                                "borderColor": "#4dd2ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    23,
                                    9,
                                    5,
                                    35,
                                    18,
                                    26,
                                    5
                                ],
                                "label": "Pierce Brosnan",
                                "borderColor": "#007399",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    23,
                                    9,
                                    5,
                                    35,
                                    18,
                                    26,
                                    5
                                ],
                                "label": "Anke Shake",
                                "borderColor": "#3333ff",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    3,
                                    29,
                                    15,
                                    5,
                                    28,
                                    36,
                                    15
                                ],
                                "label": "John Snow",
                                "borderColor": "#ff9900",
                                "fill": false,
                                "borderJoinStyle": "round"
                            },
                            {
                                "data": [
                                    13,
                                    19,
                                    25,
                                    15,
                                    48,
                                    16,
                                    25
                                ],
                                "label": "Jake Butler",
                                "borderColor": "#88cc00",
                                "fill": false,
                                "borderJoinStyle": "round"
                            }
                        ]
                    }
                },
                "barChart": {
                    "chartType": "horizontalBar",
                    "chartData": {
                        "labels": [
                            "Ian Kaapi",
                            "Annie Flamsteed",
                            "Travis Brannon",
                            "Ayva Alter",
                            "John Askew",
                            "Luke Skywalker",
                            "Pierce Brosnan",
                            "Anke Shake",
                            "John Snow",
                            "Jake Butler"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    2.9,
                                    4,
                                    4,
                                    5,
                                    4,
                                    4,
                                    2.9,
                                    4,
                                    5,
                                    4
                                ],
                                "backgroundColor": [
                                    "#e6e600",
                                    "#68838F",
                                    "#00ff00",
                                    "#C42963",
                                    "#ff66ff",
                                    "#4dd2ff",
                                    "#007399",
                                    "#3333ff",
                                    "#ff9900",
                                    "#88cc00"
                                ]
                            }
                        ]
                    }
                }
            }
        }
    ],
    "detailed": {
        "training": {
            "include": true,
            "data": {
                "title": "Training",
                "detailedChartMeta": {
                    "title": "Average training minutes by Day (PERIOD)",
                    "charts": [
                        {
                            "chartTitle": "Swimming",
                            "chartType": "bar",
                            "chartOptions": "optionsTrendsMixedChart",
                            "chartLabel": "Average training minutes by athlete??",
                            "chartData": {
                                "labels": [
                                    "Mon Aug 12 2019",
                                    "Tue Aug 13 2019",
                                    "Wed Aug 14 2019",
                                    "Thu Aug 15 2019",
                                    "Fri Aug 16 2019",
                                    "Sat Aug 17 2019",
                                    "Sun Aug 18 2019"
                                ],
                                "datasets": [
                                    {
                                        "label": "Avg Minutes",
                                        "data": [
                                            229.09091,
                                            81.818184,
                                            76.36364,
                                            91.36364,
                                            61.363636,
                                            57.272728,
                                            39
                                        ],
                                        "type": "line",
                                        "fill": false,
                                        "backgroundColor": "#fff",
                                        "borderColor": "#70cbf4",
                                        "borderCapStyle": "butt",
                                        "borderDash": [],
                                        "borderDashOffset": 0,
                                        "borderJoinStyle": "miter",
                                        "lineTension": 0.3,
                                        "pointBackgroundColor": "#fff",
                                        "pointBorderColor": "#70cbf4",
                                        "pointBorderWidth": 1,
                                        "pointHoverRadius": 5,
                                        "pointHoverBackgroundColor": "#70cbf4",
                                        "pointHoverBorderColor": "#70cbf4",
                                        "pointHoverBorderWidth": 2,
                                        "pointRadius": 4,
                                        "pointHitRadius": 10
                                    },
                                    {
                                        "label": "Agv Pain",
                                        "data": [
                                            3.4545455,
                                            2.5454545,
                                            4.090909,
                                            2.8181818,
                                            3.8181818,
                                            2.4545455,
                                            3.3
                                        ],
                                        "type": "line",
                                        "fill": false,
                                        "backgroundColor": "#fff",
                                        "borderColor": "#737373",
                                        "borderCapStyle": "butt",
                                        "borderDash": [
                                            10,
                                            10
                                        ],
                                        "borderDashOffset": 0,
                                        "borderJoinStyle": "miter",
                                        "lineTension": 0.3,
                                        "pointBackgroundColor": "#fff",
                                        "pointBorderColor": "#737373",
                                        "pointBorderWidth": 1,
                                        "pointHoverRadius": 5,
                                        "pointHoverBackgroundColor": "#737373",
                                        "pointHoverBorderColor": "#737373",
                                        "pointHoverBorderWidth": 2,
                                        "pointRadius": 4,
                                        "pointHitRadius": 10
                                    },
                                    {
                                        "label": "Light",
                                        "data": [
                                            0,
                                            0,
                                            0,
                                            0,
                                            0,
                                            0,
                                            0
                                        ],
                                        "backgroundColor": "#269B5C",
                                        "yAxisID": "bar-y-axis"
                                    },
                                    {
                                        "label": "Moderate",
                                        "data": [
                                            0,
                                            23.333334,
                                            23.214285,
                                            22.38806,
                                            60,
                                            28.571428,
                                            61.53846
                                        ],
                                        "backgroundColor": "#F0B23C",
                                        "yAxisID": "bar-y-axis"
                                    },
                                    {
                                        "label": "Vigorous",
                                        "data": [
                                            100,
                                            76.666664,
                                            76.78571,
                                            77.61194,
                                            40,
                                            71.42857,
                                            38.46154
                                        ],
                                        "backgroundColor": "#D74742",
                                        "yAxisID": "bar-y-axis"
                                    }
                                ]
                            }
                        }
                    ]
                },
                "chartOne": {
                    "title": "Average training minutes by athlete!! (PERIOD)",
                    "chartOptions": "optionsTrendsMixedChart",
                    "chartLabel": "Minutes per intensity??",
                    "chartType": "bar",
                    "chartData": {
                        "labels": [
                            "Lille",
                            "Maubeuge",
                            "Poissy SG",
                            "Morlaix",
                            "Sud Gironde",
                            "Elbeuf",
                            "Douarnenez",
                            "Dieppe",
                            "Le Havre",
                            "Limoges",
                            "Lorient",
                            "Fécamp",
                            "Paris",
                            "Aurillac",
                            "Etampes",
                            "Quimperlé",
                            "Meulan",
                            "Fougères",
                            "Melun",
                            "Quimper",
                            "Roscoff",
                            "Saint-Malo",
                            "Redon",
                            "La Roche-sur-Yon",
                            "Villefranche"
                        ],
                        "datasets": [
                            {
                                "label": 2014,
                                "backgroundColor": "rgba(249, 108, 110, 0.8)",
                                "borderColor": "rgba(249, 108, 110, 1)",
                                "data": [
                                    147,
                                    49,
                                    26,
                                    22,
                                    45,
                                    11,
                                    29,
                                    3,
                                    22,
                                    24,
                                    4,
                                    18,
                                    22,
                                    21,
                                    16,
                                    4,
                                    19,
                                    12,
                                    0,
                                    21,
                                    15,
                                    6,
                                    15,
                                    14,
                                    9
                                ]
                            },
                            {
                                "label": 2015,
                                "backgroundColor": "rgba(179, 153, 255, 0.9)",
                                "borderColor": "rgba(179, 153, 255, 1)",
                                "data": [
                                    130,
                                    58,
                                    59,
                                    49,
                                    20,
                                    37,
                                    20,
                                    40,
                                    30,
                                    19,
                                    33,
                                    34,
                                    20,
                                    11,
                                    16,
                                    21,
                                    8,
                                    15,
                                    24,
                                    5,
                                    7,
                                    12,
                                    7,
                                    8,
                                    8
                                ]
                            },
                            {
                                "label": 2016,
                                "backgroundColor": "rgba(128, 191, 255, 0.9)",
                                "borderColor": "rgba(128, 191, 255, 1)",
                                "data": [
                                    31,
                                    24,
                                    8,
                                    14,
                                    9,
                                    17,
                                    13,
                                    19,
                                    9,
                                    17,
                                    19,
                                    3,
                                    7,
                                    2,
                                    2,
                                    8,
                                    5,
                                    5,
                                    7,
                                    1,
                                    3,
                                    7,
                                    2,
                                    0,
                                    3
                                ]
                            }
                        ]
                    }
                },
                "chartTwo": {
                    "title": "Minutes per intensity!!",
                    "chartType": "polarArea",
                    "chartOptions": "optionsPolarAreaChart",
                    "chartData": {
                        "labels": [
                            "Light",
                            "Moderate",
                            "Vigorous"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    0,
                                    20.905172,
                                    79.094826
                                ],
                                "backgroundColor": [
                                    "#4688F1",
                                    "#D9463D",
                                    "#FC714A",
                                    "#1D9C5A",
                                    "#1DAAF1",
                                    "#5C6DBD"
                                ],
                                "label": "My dataset"
                            }
                        ]
                    }
                },
                "chartThree": {
                    "title": "Minutes per spor!!",
                    "chartType": "pie",
                    "chartOptions": "optionsPieChart",
                    "chartData": {
                        "labels": [
                            "AFL",
                            "AthleticsTrack",
                            "BMX",
                            "AthleticsField",
                            "Cardiovascular",
                            "Cricket",
                            "CircuitTraining",
                            "Gymnastics",
                            "Basketball",
                            "Dancing",
                            "Cycling",
                            "Diving",
                            "Hockey"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    16.37931,
                                    8.189655,
                                    20.474138,
                                    14.655172,
                                    16.163794,
                                    8.189655,
                                    0.6465517,
                                    0.86206895,
                                    6.25,
                                    4.3103447,
                                    0.43103448,
                                    2.1551723,
                                    1.2931035
                                ],
                                "backgroundColor": [
                                    "#4688F1",
                                    "#D9463D"
                                ]
                            }
                        ]
                    }
                }
            }
        },
        "pain": {
            "include": true,
            "data": {
                "title": "Pain",
                "detailedChartMeta": {
                    "title": "Average pain per athlete!!",
                    "charts": [
                        {
                            "chartTitle": "Swimming",
                            "chartType": "bar",
                            "chartOptions": "optionsTrendsMixedChart",
                            "chartData": {
                                "labels": [
                                    "January",
                                    "February",
                                    "March",
                                    "April",
                                    "May",
                                    "June",
                                    "July",
                                    "August",
                                    "September",
                                    "October",
                                    "November",
                                    "December"
                                ],
                                "datasets": [
                                    {
                                        "data": [
                                            50,
                                            30,
                                            60,
                                            70,
                                            80,
                                            90,
                                            95,
                                            70,
                                            90,
                                            20,
                                            60,
                                            95
                                        ],
                                        "type": "line",
                                        "label": "Avg Minutes",
                                        "fill": false,
                                        "backgroundColor": "#fff",
                                        "borderColor": "#70cbf4",
                                        "borderCapStyle": "butt",
                                        "borderDash": [],
                                        "borderDashOffset": 0,
                                        "borderJoinStyle": "miter",
                                        "lineTension": 0.3,
                                        "pointBackgroundColor": "#fff",
                                        "pointBorderColor": "#70cbf4",
                                        "pointBorderWidth": 1,
                                        "pointHoverRadius": 5,
                                        "pointHoverBackgroundColor": "#70cbf4",
                                        "pointHoverBorderColor": "#70cbf4",
                                        "pointHoverBorderWidth": 2,
                                        "pointRadius": 4,
                                        "pointHitRadius": 10
                                    },
                                    {
                                        "data": [
                                            25,
                                            40,
                                            30,
                                            70,
                                            60,
                                            50,
                                            40,
                                            70,
                                            40,
                                            80,
                                            30,
                                            90
                                        ],
                                        "type": "line",
                                        "label": "Agv Pain",
                                        "fill": false,
                                        "backgroundColor": "#fff",
                                        "borderColor": "#737373",
                                        "borderCapStyle": "butt",
                                        "borderDash": [
                                            10,
                                            10
                                        ],
                                        "borderDashOffset": 0,
                                        "borderJoinStyle": "miter",
                                        "lineTension": 0.3,
                                        "pointBackgroundColor": "#fff",
                                        "pointBorderColor": "#737373",
                                        "pointBorderWidth": 1,
                                        "pointHoverRadius": 5,
                                        "pointHoverBackgroundColor": "#737373",
                                        "pointHoverBorderColor": "#737373",
                                        "pointHoverBorderWidth": 2,
                                        "pointRadius": 4,
                                        "pointHitRadius": 10
                                    },
                                    {
                                        "label": "Feeplbe",
                                        "backgroundColor": "#269B5C",
                                        "yAxisID": "bar-y-axis",
                                        "data": [
                                            50,
                                            44,
                                            52,
                                            62,
                                            48,
                                            58,
                                            59,
                                            50,
                                            51,
                                            52,
                                            53,
                                            54
                                        ]
                                    },
                                    {
                                        "label": "Enthusiastic",
                                        "backgroundColor": "#F0B23C",
                                        "yAxisID": "bar-y-axis",
                                        "data": [
                                            20,
                                            21,
                                            24,
                                            25,
                                            26,
                                            17,
                                            28,
                                            19,
                                            20,
                                            11,
                                            22,
                                            33
                                        ]
                                    },
                                    {
                                        "label": "Vigorous",
                                        "backgroundColor": "#D74742",
                                        "yAxisID": "bar-y-axis",
                                        "data": [
                                            30,
                                            35,
                                            24,
                                            13,
                                            26,
                                            25,
                                            13,
                                            31,
                                            29,
                                            37,
                                            25,
                                            13
                                        ]
                                    }
                                ]
                            }
                        }
                    ]
                },
                "chartOne": {
                    "title": "Average pain per athlete!!",
                    "chartType": "bar",
                    "chartOptions": "optionsTrendsMixedChart",
                    "chartData": {
                        "labels": [
                            "Lille",
                            "Maubeuge",
                            "Poissy SG",
                            "Morlaix",
                            "Sud Gironde",
                            "Elbeuf",
                            "Douarnenez",
                            "Dieppe",
                            "Le Havre",
                            "Limoges",
                            "Lorient",
                            "Fécamp",
                            "Paris",
                            "Aurillac",
                            "Etampes",
                            "Quimperlé",
                            "Meulan",
                            "Fougères",
                            "Melun",
                            "Quimper",
                            "Roscoff",
                            "Saint-Malo",
                            "Redon",
                            "La Roche-sur-Yon",
                            "Villefranche"
                        ],
                        "datasets": [
                            {
                                "label": 2014,
                                "backgroundColor": "rgba(249, 108, 110, 0.8)",
                                "borderColor": "rgba(249, 108, 110, 1)",
                                "data": [
                                    147,
                                    49,
                                    26,
                                    22,
                                    45,
                                    11,
                                    29,
                                    3,
                                    22,
                                    24,
                                    4,
                                    18,
                                    22,
                                    21,
                                    16,
                                    4,
                                    19,
                                    12,
                                    0,
                                    21,
                                    15,
                                    6,
                                    15,
                                    14,
                                    9
                                ]
                            },
                            {
                                "label": 2015,
                                "backgroundColor": "rgba(179, 153, 255, 0.9)",
                                "borderColor": "rgba(179, 153, 255, 1)",
                                "data": [
                                    130,
                                    58,
                                    59,
                                    49,
                                    20,
                                    37,
                                    20,
                                    40,
                                    30,
                                    19,
                                    33,
                                    34,
                                    20,
                                    11,
                                    16,
                                    21,
                                    8,
                                    15,
                                    24,
                                    5,
                                    7,
                                    12,
                                    7,
                                    8,
                                    8
                                ]
                            },
                            {
                                "label": 2016,
                                "backgroundColor": "rgba(128, 191, 255, 0.9)",
                                "borderColor": "rgba(128, 191, 255, 1)",
                                "data": [
                                    31,
                                    24,
                                    8,
                                    14,
                                    9,
                                    17,
                                    13,
                                    19,
                                    9,
                                    17,
                                    19,
                                    3,
                                    7,
                                    2,
                                    2,
                                    8,
                                    5,
                                    5,
                                    7,
                                    1,
                                    3,
                                    7,
                                    2,
                                    0,
                                    3
                                ]
                            }
                        ]
                    }
                },
                "chartTwo": {
                    "title": "Pain levels by body location!!",
                    "chartType": "polarArea",
                    "chartOptions": "optionsPolarAreaChart",
                    "chartData": {
                        "datasets": [
                            {
                                "data": [
                                    11,
                                    16,
                                    7,
                                    3,
                                    14
                                ],
                                "backgroundColor": [
                                    "#FF6384",
                                    "#4BC0C0",
                                    "#FFCE56",
                                    "#E7E9ED",
                                    "#36A2EB"
                                ],
                                "label": "My dataset"
                            }
                        ],
                        "labels": [
                            "Neck",
                            "Elbow",
                            "Shin",
                            "Ancle",
                            "Back"
                        ]
                    }
                },
                "chartThree": {
                    "title": "Pain levels by type!!",
                    "chartType": "pie",
                    "chartOptions": "optionsPieChart",
                    "chartData": {
                        "labels": [
                            "Vigorous",
                            "Moderate"
                        ],
                        "datasets": [
                            {
                                "data": [
                                    87.3,
                                    12.7
                                ],
                                "backgroundColor": [
                                    "#4688F1",
                                    "#D9463D"
                                ]
                            }
                        ]
                    }
                },
                "painInjuryDetail": {
                    "include": true,
                    "data": {
                        "athletes": [
                            {
                                "name": "Ian Kaapi",
                                "date": "May 18,2019",
                                "bodyLocation": "RightShoulder",
                                "type": "Training",
                                "painLevel": 2
                            },
                            {
                                "name": "Annie Flamsteed",
                                "date": "May 18,2019",
                                "bodyLocation": "Neck",
                                "type": "Training",
                                "painLevel": 1
                            },
                            {
                                "name": "Travis Brannon",
                                "date": "May 18,2019",
                                "bodyLocation": "LeftAnkle",
                                "type": "Injury",
                                "painLevel": 3
                            }
                        ]
                    }
                }
            }
        }
    }
};
