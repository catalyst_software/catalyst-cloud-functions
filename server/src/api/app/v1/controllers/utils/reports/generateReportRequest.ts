import moment from 'moment';

import { ReportRequest } from '../../../../../../models/report-model';
import { getDataFromBQ } from './getDataFromBQ';

import { removeUndefinedProps } from '../../../../../../db/ipscore/services/save-aggregates';
import { isArray } from 'lodash';

export const generateReportRequest = async (request: ReportRequest) => {
    // const requestObject: ReportRequest = {
    //     reportType: 1,
    //     uid: '',
    //     requestProcessed: false,
    //     requestDateTime: firestore.Timestamp.fromDate(moment('Tue Feb 12 2020 15:54:02 GMT+0100').toDate()),
    //     requestingUser: {
    //         uid: 'O4mL8mLC3XXFkKpQ1IS365gjmrG2',
    //         email: 'ian+asas@inspiresportonline.com',
    //         firstName: 'Ian',
    //         lastName: 'Gouws',
    //         creationTimestamp: firestore.Timestamp.fromDate(moment('Wed Jun 9  2019 15:54:02 GMT+0100').toDate()),
    //         isCoach: false,
    //         runningTotalIpScore: undefined,
    //         subscriptionType: undefined,
    //         bio:undefined,
    //         orgTools: undefined
    //     },
    //     entity: {
    //         type: ReportRequestEntityType.Group,
    //         entityId: 'STARFISH-BOOKS',
    //         entityIndicator: {
    //             organizationId: '96WTqSOTgxSBMYDMBsrs',
    //             groupName: 'STARFISH-BOOKS',
    //             organizationName: `Ian IPSCORE Coach's Organization`,
    //             groupId: '96WTqSOTgxSBMYDMBsrs-BOOKS',
    //             creationTimestamp: firestore.Timestamp.fromDate(moment('2019-02-14T08:45:25.270Z').toDate())
    //         },
    //         groupIDs: []
    //     }
    // };

    const { requestDateTime: requestDateTimeString, requestEndDateTime: requestEndDateTimeString, entity } = request;
    console.warn('=======================> requestDateTimeString', requestDateTimeString)
    const requestDateTime = moment(requestDateTimeString as any).toDate()
    const requestEndDateTime = !!requestEndDateTimeString ? moment(requestEndDateTimeString as any).toDate() : undefined
    console.warn('=======================> requestDateTime', requestDateTime)
    console.warn('=======================> requestEndDateTime', requestEndDateTime || ' Not Set')




    const biometricEntriesDate = moment()
        .set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).toDate();

    // const reportResults2 = await generateReport(undefined, requestingUser);
    // return reportResults2

    const startDate = moment(requestDateTime)
        // .subtract(1, 'week')//.add(1, 'day')
        .startOf('day')

    console.warn('=======================> startDate', startDate)
    // .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    const toDate = requestEndDateTime !== undefined ? moment(requestEndDateTime) : moment(moment().subtract(1, 'day'))
        // .subtract(1, 'day')
        .endOf('day');

    //    const startDate = moment(toDate)
    //         // .subtract(3, 'months').add(1, 'day')
    //         .startOf('day')
    //         // .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });

    // const toDate = moment(requestDateTime)
    //     .subtract(2, 'months')
    //     .endOf('day');
    // .subtract(1, 'day').startOf('day')
    // .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })

    // const toDate = moment(requestDateTime)
    //     .subtract(2, 'months')
    //     .endOf('day');
    // // .subtract(1, 'day').startOf('day')
    // // .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })

    // const startDate = moment(toDate)
    //     .subtract(3, 'weeks').add(1, 'day')
    //     .set({ hour: 0, minute: 0, second: 0, millisecond: 0 });

    // TODO: refactor enity.groupIDs
    entity.groupIDs = request.groupIDs;
    const { biometricEntriesMulti } = await getDataFromBQ(startDate.format("YYYYMMDD"), toDate.format("YYYYMMDD"), entity);

    if (biometricEntriesMulti && biometricEntriesMulti.length) {
        // const biometricEntries = biometricEntriesMulti[0]
        const allResults = await Promise.all(biometricEntriesMulti.map((biometricEntries) => {

            if (isArray(biometricEntries) && biometricEntries.length) {
                console.log(`Entries Count for BigQuery report dated from ${startDate.toDate()} to ${toDate.toDate()}  => `, biometricEntries.length);
                // Fri, 31 May 2019 07: 56: 25 GMT
                removeUndefinedProps(biometricEntries)
                // const utcOffset = moment(biometricEntries[0].creationTimestamp).utcOffset()
                // const groupedEntriesByDate = groupByMap(biometricEntries, (entry: BiometricEntrySchema) => moment(entry.creationTimestamp).startOf('day').format("YYYYMMDD")) as Map<string, Array<BiometricEntrySchema>>;


                // const daysOfWeek: {
                //     date: Date;
                //     uid?: string;
                //     lineChartData: any;
                //     entries: BiometricEntrySchema[]
                // }[] = []

                // initDaysOfWeek(daysOfWeek, startDate, toDate, groupedEntriesByDate, utcOffset);


                // const theData: RedFlagReportSchema = composeReportData(biometricEntries, entity, setAllToToday, startDate, toDate, daysOfWeek);
                // const reportResults = await generateReport(theData, requestingUser);
                // // biometricEntries.forEach((item => {
                // //     console.log(`${item.lifestyleEntryType} Entry for value for BigQuery report dated from ${threeDaysBackDate.toDate()} to ${latestReportDate.toDate()} = (Mood Entries Count) => `, item.value)
                // // }))

                // console.log(JSON.stringify(theData))
                // return { ...reportResults, theData };
                return biometricEntries;
            }
            else {
                console.warn(`NO Entries found for BigQuery report dated from ${startDate.toDate()} to ${toDate.toDate()}  => `, biometricEntries.length);

                return biometricEntries;
            }
            // console.log(`Mood Entries for Count for BigQuery report dated from ${threeDaysBackDate.toDate()} to ${latestReportDate.toDate()} = (Mood Entries Count) => `, row.value)
        }));

        return {
            success: true,
            message: `allResults length: ${allResults.length}`,
            // TODO: 
            allResults,
            taskId: 'allResults'
        };
    }
    else {

        return {
            success: false,
            message: `biometricEntriesMulti.length is ${biometricEntriesMulti.length}`,
            // TODO: 
            taskId: 'biometricEntriesMulti'
        };
    }
};
