import { ChartType } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { getAthleteColour } from "./getAthleteColour";


export const getSuperstars = (
    engagementSuperstars: {
        uid: string;
        name: string;
        value: number;
    }[],
    trainingSuperstars: {
        uid: string;
        name: string;
        value: number;
    }[],
    happyChappies: {
        uid: string;
        name: string;
        value: number;
    }[]): {
        "include": boolean;
        "data": {
            "superstarsGroups": {
                "label": string;
                "isSuperstars": boolean;
                "chartType": ChartType;
                "chartData": {
                    "labels": string[];
                    "datasets": [{
                        "data": number[];
                        "backgroundColor": string[];
                    }];
                };
            }[];
        };
    } => {
    return {
        "include": true,
        "data": {
            "superstarsGroups": [
                {
                    "label": "Engagement Superstars!",
                    "isSuperstars": true,
                    "chartType": ChartType.HorizontalBar,
                    "chartData": {
                        "labels": engagementSuperstars.map(engagementSuperstar => engagementSuperstar.name),
                        "datasets": [{
                            "data": engagementSuperstars.map(engagementSuperstar => engagementSuperstar.value),
                            "backgroundColor": engagementSuperstars.map(engagementSuperstar => getAthleteColour(engagementSuperstar.uid)),
                            // ["#F3C7C4", "#F3C7C4", "#F3C7C4", "#F3C7C4",
                            //     "#F3C7C4", "#F3C7C4", "#F3C7C4"
                            // ]
                        }]
                    }
                },
                {
                    "label": "Training Superstars!",
                    "isSuperstars": true,
                    "chartType": ChartType.HorizontalBar,
                    "chartData": {
                        "labels": trainingSuperstars.map(trainingSuperstar => trainingSuperstar.name),
                        "datasets": [{
                            "data": trainingSuperstars.map(trainingSuperstar => trainingSuperstar.value),
                            "backgroundColor": trainingSuperstars.map(trainingSuperstar => getAthleteColour(trainingSuperstar.uid)),
                        }]
                    }
                },
                {
                    "label": "HAPPY CHAPPIES!",
                    "isSuperstars": true,
                    "chartType": ChartType.HorizontalBar,
                    "chartData": {
                        "labels": happyChappies.map(happyChappy => happyChappy.name),
                        "datasets": [{
                            "data": happyChappies.map(happyChappy => happyChappy.value),
                            "backgroundColor": happyChappies.map(happyChappy => getAthleteColour(happyChappy.uid)),
                        }]
                    }
                }
            ]
        }
    };
};
