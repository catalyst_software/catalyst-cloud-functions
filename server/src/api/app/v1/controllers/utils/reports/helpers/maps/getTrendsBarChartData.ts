import { BiometricEntrySchema, ChartType } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { sortByValueDesc } from '../../sortByHelpers';
import { athleteColors } from '../../composeReportData';

export const getTrendsBarChartData = (athleteEntries: BiometricEntrySchema[], athleteID: string, athleteFullName: string) => {
    const barChartDatasets = []

    barChartDatasets.push({
        uid: athleteID,
        label: athleteFullName,
        value: athleteEntries.length
        // value: getMeanOfValue(athleteEntries)
    });

    const colours
        = barChartDatasets
            .sort(sortByValueDesc)
            .map((chartData) =>
                athleteColors
                    .find((athColour) => athColour.uid === chartData.uid).colour)


    const barChartData = {
        chartType: ChartType.HorizontalBar,
        chartData: {
            labels: barChartDatasets.sort(sortByValueDesc).map((chartData) => chartData.label),
            // ["Ian Kaapi", "Annie Flamsteed", "Travis Brannon", "Ayva Alter", "John Askew", "Luke Skywalker", "Pierce Brosnan", "Anke Shake", "John Snow", "Jake Butler"],
            datasets: [{
                data: barChartDatasets.sort(sortByValueDesc).map((chartData) => chartData.value),
                // [2.9, 4, 4, 5, 4, 4, 2.9, 4, 5, 4],
                backgroundColor: colours,
                // ["#e6e600", "#68838F", "#00ff00", "#C42963", "#ff66ff", "#4dd2ff", "#007399", "#3333ff", "#ff9900", "#88cc00"]
            }]
        }
    }

    return barChartData;
};
