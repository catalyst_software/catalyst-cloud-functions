import { from, of, zip } from 'rxjs';
import { groupBy } from 'rxjs/internal/operators/groupBy';
import { map } from 'rxjs/internal/operators/map';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';
import { toArray } from 'rxjs/internal/operators/toArray';
import { sum } from 'lodash';

import { BiometricTrainingEntrySchema } from '../../../../../../../pubsub/interfaces/red-flag-reports';
import { getMeanOfValue } from "../getMeanOfValue";
import { TrainingTrendData } from '../types/training-trend-data';
import { getLowestValue } from '../getLowestValue';
import { getHighestValue } from '../getHighestValue';
import { getSumOfValue } from '../getSumOfValue';

export const getGroupedByIntensityLevel = (athlteteTrainingEntries: BiometricTrainingEntrySchema[]) => {

    const intensityLevelData: TrainingTrendData = [];
    const selectedIntesitylevels = [];

    from(athlteteTrainingEntries)
        .pipe(
            groupBy(entry => entry.intensityLevel),
            map((groupedLevel) => {

                // console.log('grouped', groupedLevel);
                selectedIntesitylevels.push(groupedLevel.key);

                return groupedLevel;
            }),
            mergeMap(intensityLevelGroup => zip(of(intensityLevelGroup.key), intensityLevelGroup.pipe(toArray())))
        )
        .subscribe((groupedByIntensityLevel) => {

            const intensityLevel = groupedByIntensityLevel[0];
            const groupedEntries = groupedByIntensityLevel[1];

            const meanValueOfLevel = getMeanOfValue(groupedEntries);
            const lowValueOfLevel = getLowestValue(groupedEntries);
            const highValueOfLevel = getHighestValue(groupedEntries);
            const totalValueOfLevel = getSumOfValue(groupedEntries);

            const intenisityLevelValues = {
                intensityLevel,
                meanValue: meanValueOfLevel,
                lowValue: lowValueOfLevel,
                highValue: highValueOfLevel,
                totalValue: totalValueOfLevel,
                avg: null
            };

            intensityLevelData.push({
                uid: athlteteTrainingEntries[0].athleteID,
                firstName: athlteteTrainingEntries[0].userFirstName,
                fullName: athlteteTrainingEntries[0].userFullName,
                ...intenisityLevelValues
            });
        });

    const totalCount = sum(intensityLevelData.map((level) => level.totalValue));

    intensityLevelData.map((intesityGroup) => {
        const avgOutOfHundred = intesityGroup.totalValue / totalCount * 100;
        intesityGroup.avg = avgOutOfHundred;
    });
    
    return intensityLevelData;
};
