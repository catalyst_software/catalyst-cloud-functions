
import { BiometricEntrySchema, ChartType } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { sortByValue, sortByLabelDesc } from "../../sortByHelpers";
import { getMeanOfValue } from "../../getMeanOfValue";
import { getLowestValue } from '../../getLowestValue';
import { getHighestValue } from '../../getHighestValue';
import { getSumOfValue } from '../../getSumOfValue';
import { getEntriesIndicator } from '../../getEntriesIndicator';
import { getGroupedByMealType } from './getGroupedByMealType';

export const getSimpleFoodMap = (
    entries: Array<BiometricEntrySchema>): {
    lowValue: number;
    highValue: number;
    meanValue: number;
    totalValue: number;
    countValue: number;
    barChartData: Array<{
        mealType: string;
        percentagePerclassification: Array<{
            clasificationType: string;
            totalMealTypeCount: number;
            avg: number;
        }>;
        theBarChart: {
            chartType: ChartType;
            label: string;
            chartData: {
                labels: Array<string>;
                datasets: Array<{
                    // label: string;
                    data: Array<number>;
                    backgroundColor: Array<string>;
                }>;
            };
        };
    }>;
    entries: Array<BiometricEntrySchema>;
} => {

    if (entries) {


        // // Grouped By Meal Classification
        // const groupedBySimpleMealClassificationTypeMap = getGroupedBySimpleMealClassificationType(entries);
        // Grouped By Meal Type
        const groupedByMealTypeMap = getGroupedByMealType(entries);


        const barChartDatasets: Array<{
            mealType: string;
            percentagePerclassification: Array<{
                clasificationType: string;
                totalMealTypeCount: number;
                avg: number;
            }>;
            theBarChart: {
                chartType: ChartType;
                label: string;
                chartData: {
                    labels: Array<string>;
                    datasets: Array<{
                        // label: string;
                        data: Array<number>;
                        backgroundColor: Array<string>;
                    }>;
                };
            };
        }> = groupedByMealTypeMap.map((groupedBySimpleMealClassificationType) => {


            const { mealType, percentagePerclassification } = groupedBySimpleMealClassificationType

            const theChartMeta = percentagePerclassification.map((classificationData) => {
                return {
                    label: classificationData.clasificationType,
                    avg: classificationData.avg
                }
            }).sort(sortByLabelDesc)

            const theBarChart: {
                chartType: ChartType;
                label: string,
                chartData: {
                    labels: string[];
                    datasets: Array<{
                        // label: string;
                        data: Array<number>;
                        backgroundColor: Array<string>;
                    }>;
                };
            } = {
                chartType: ChartType.Pie,
                label: mealType,
                chartData: {
                    labels: theChartMeta.map((chartMeta) => chartMeta.label),
                    // ["Ian Kaapi", "Annie Flamsteed", "Travis Brannon", "Ayva Alter", "John Askew", "Luke Skywalker", "Pierce Brosnan", "Anke Shake", "John Snow", "Jake Butler"],
                    datasets: [{
                        data: theChartMeta.map((chartMeta) => chartMeta.avg),
                        // [2.9, 4, 4, 5, 4, 4, 2.9, 4, 5, 4],
                        backgroundColor: // colours,
                            ["#e6e600", "#68838F", "#00ff00", "#C42963", "#ff66ff", "#4dd2ff", "#007399", "#3333ff", "#ff9900", "#88cc00"]
                    }]
                }
            }

            return {
                // value: groupedBySimpleMealClassificationType.mealType,
                theBarChart,
                ...groupedBySimpleMealClassificationType
            }
        })


        removeUndefinedProps(entries);

        const meanValue = getMeanOfValue(entries);
        const lowValue = getLowestValue(entries);
        const highValue = getHighestValue(entries);
        const totalValue = getSumOfValue(entries);
        const countValue = entries.length;

        // const labels = (<any>barChartDatasets).sort(sortByValueDesc).map((mealType) => mealType.classificationType)
        // const colours = theMinutesperIntensityEntriesMap.sort(sortByValueDesc).map((chartData) => athleteColors.find((athColour) => athColour.uid === chartData.uid).colour)


        // const barChart = {
        //     chartType: ChartType.Pie,
        //     chartData: {
        //         labels: labels.map((label) => label),
        //         // ["Ian Kaapi", "Annie Flamsteed", "Travis Brannon", "Ayva Alter", "John Askew", "Luke Skywalker", "Pierce Brosnan", "Anke Shake", "John Snow", "Jake Butler"],
        //         datasets: [{
        //             data: (<any>barChartDatasets).sort(sortByValueDesc).map((chartData) => chartData.value),
        //             // [2.9, 4, 4, 5, 4, 4, 2.9, 4, 5, 4],
        //             backgroundColor: // colours,
        //                 ["#e6e600", "#68838F", "#00ff00", "#C42963", "#ff66ff", "#4dd2ff", "#007399", "#3333ff", "#ff9900", "#88cc00"]
        //         }]
        //     }
        // }

        const mappedData = {
            lowValue,
            highValue,
            meanValue,
            totalValue,
            countValue,
            barChartData: barChartDatasets,
            entries: getEntriesIndicator(entries)
                .sort(sortByValue)

        }
        return mappedData

    } else {
        const mappedData = {
            lowValue: 0,
            highValue: 0,
            meanValue: 0,
            totalValue: 0,
            countValue: 0,
            barChartData: undefined,// barChart,
            entries: [] // getEntriesIndicator(entries)
            // .sort(sortByValue)

        }
        return mappedData
    }







    // return theEntries.sort(sortByValue);
    // return foodEntries
    //     .map((entry) => {
    //         const defaultValues = getDefaultValues(entry)
    //         return {
    //             ...defaultValues,
    //             foodType: entry.foodType,
    //             mealType: entry.mealType,
    //             name: entry.userFullName,
    //             value: entry.value
    //         };
    //     }).sort(sortByValue)
};
