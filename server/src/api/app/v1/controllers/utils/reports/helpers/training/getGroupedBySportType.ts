import { sum } from 'lodash';
import { BiometricTrainingEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
export const getGroupedBySportType = (athlteteTrainingEntries: BiometricTrainingEntrySchema[]) => {

    const totalTrainingMinutesCount = sum(athlteteTrainingEntries.map((entry) => entry.value));

    const theMinutesperSportEntries = [];
    const groupedBySportType = groupByMap(athlteteTrainingEntries, (entry: BiometricTrainingEntrySchema) => entry.sportType) as Map<string, Array<BiometricTrainingEntrySchema>>;
    groupedBySportType.forEach((athleteEntries, sportType) => {
        const res = {
            sportType,
            value: sum(athleteEntries.filter((entry) => entry.sportType === sportType).map((theEntry) => {
                return theEntry.value;
            })) / totalTrainingMinutesCount * 100
        };
        theMinutesperSportEntries.push(res);
    });
    return theMinutesperSportEntries;
};
