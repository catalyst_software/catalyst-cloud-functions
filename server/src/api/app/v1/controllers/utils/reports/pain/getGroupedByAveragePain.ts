import { sum } from 'lodash';

import { BiometricPainEntrySchema } from '../../../../../../../pubsub/interfaces/red-flag-reports';
import { getMeanOfValue } from "../getMeanOfValue";
import { getLowestValue } from '../getLowestValue';
import { getHighestValue } from '../getHighestValue';
import { PainTrendData } from '../types/pain-trend-data';
import { getSumOfValue } from '../getSumOfValue';

export const getGroupedByAveragePain = (athlteteTrainingEntries: BiometricPainEntrySchema[]) => {

    const intensityLevelData: PainTrendData = [];
    // const selectedIntesitylevels = [];
    // from(groupedByDateAndAverageMinutes)
    //     .pipe(
    //         // groupBy(entry => entry.intensityLevel),
    //         // map((grouped) => {
    //         //     console.log('grouped', grouped)
    //         //     selectedIntesitylevels.push(grouped.key)
    //         //     return grouped
    //         // }),
    //         // mergeMap(intensityLevelGroup => zip(of(intensityLevelGroup.key), intensityLevelGroup.pipe(toArray())))
    //     )
    //     .subscribe((groupedByIntensityLevel) => {

    const meanValueOfPain = getMeanOfValue(athlteteTrainingEntries);
    const lowValueOfPain = getLowestValue(athlteteTrainingEntries);
    const highValueOfPain = getHighestValue(athlteteTrainingEntries);
    const totalValueOfPain = getSumOfValue(athlteteTrainingEntries);

    const painLevelValues = {
        meanValue: meanValueOfPain,
        lowValue: lowValueOfPain,
        highValue: highValueOfPain,
        totalValue: totalValueOfPain,
        entries: athlteteTrainingEntries,
        avg: null
    };

    intensityLevelData.push({
        athleteID: athlteteTrainingEntries[0].athleteID,
        firstName: athlteteTrainingEntries[0].userFirstName,
        fullName: athlteteTrainingEntries[0].userFullName,
        ...painLevelValues
    });

    // });
    const totalCount = sum(intensityLevelData.map((level) => level.totalValue));

    intensityLevelData.map((intesityGroup) => {
        const avgOutOfHundred = intesityGroup.totalValue / totalCount * 100;
        intesityGroup.avg = avgOutOfHundred;
    });

    return intensityLevelData;
};
