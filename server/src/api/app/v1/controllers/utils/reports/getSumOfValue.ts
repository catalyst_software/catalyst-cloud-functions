import { sum } from 'lodash';

import { BiometricEntrySchema } from '../../../../../../pubsub/interfaces/red-flag-reports';

export const getSumOfValue = (athleteEntries: Array<BiometricEntrySchema>) => {

    const theSum = sum(athleteEntries.map((entry) => {
        let theValue = 0;
        if (entry.value !== undefined) {
            theValue = entry.value;
        }
        else {
            debugger;
        }

        return theValue;
    })) || 0;

    return theSum;
};
