import moment from 'moment';

import { ChartType } from './../../../../../../../../pubsub/interfaces/red-flag-reports';
import { BiometricEntrySchema } from '../../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../../shared/utils/groupByMap';
import { sortByValue } from "../../sortByHelpers";
import { mapScalarEntities } from "./mapScalarEntities";
import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { getMeanOfValue } from '../../getMeanOfValue';
import { getLowestValue } from '../../getLowestValue';
import { getHighestValue } from '../../getHighestValue';
import { getSumOfValue } from '../../getSumOfValue';
import { getEntriesIndicator } from '../../getEntriesIndicator';
import { getTrendsLineChartData } from './getTrendsLineChartData';
import { getTrendsBarChartData } from './getTrendsBarChartData';


// function getPainEntries(painEntries: Array<BiometricEntrySchema>): number {
//     return painEntries.length || 0;
// }

export type FatigueMapMeta = {
    uid?: string;
    entries: Array<BiometricEntrySchema>;
    lineChartData: {
        chartType: ChartType;
        chartData: {
            labels: Array<string>;
            datasets: Array<any>;
        };
    };
    barChartData: {
        chartType: ChartType;
        chartData: {
            labels: Array<string>;
            datasets: Array<{
                data: Array<any>;
                backgroundColor: string[];
            }>
        };
    };
    lowValue: number;
    highValue: number;
    meanValue: number;
    totalValue: number;
    countValue: number;

};

// export const getFatigueMap = (entries: Array<BiometricEntrySchema>): Array<BiometricScalarEntrySchema> => {
export const getFatigueMap = (
    entries: Array<BiometricEntrySchema>,
    date: Date,
    setAllToToday: boolean,
    startOfWeek: moment.Moment,
    daysOfWeek: {
        date: Date;
        uid?: string;
        lineChartData: any;
        entries: BiometricEntrySchema[]
    }[]): FatigueMapMeta => {

    const groupedEntriesByAthlete = groupByMap(entries.sort(sortByValue), (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID);

    groupedEntriesByAthlete.forEach((athleteEntries) => {
        mapScalarEntities(athleteEntries);
    });

    // const theFatigueEntries: Array<FatigueMapMeta> = [];


    const lineChartDatasets = []

    let barChart = undefined;

    let lineChart = undefined;

    groupedEntriesByAthlete.forEach((athleteEntries: Array<BiometricEntrySchema>, uid: string) => {

        const athleteFullName = uid.split('__')[0]
        const athleteID = uid.split('__')[1]

        // Line Chart Data

        lineChart = getTrendsLineChartData(athleteEntries, date, athleteID, athleteFullName, startOfWeek, daysOfWeek, lineChartDatasets);

        // Bar Chart Data
        barChart = getTrendsBarChartData(athleteEntries, athleteID, athleteFullName);
    })

    removeUndefinedProps(entries);

    const meanValue = getMeanOfValue(entries);
    const lowValue = getLowestValue(entries);
    const highValue = getHighestValue(entries);
    const totalValue = getSumOfValue(entries);
    const countValue = entries.length;

    const mappedData = {
        lowValue,
        highValue,
        meanValue,
        totalValue,
        countValue,
        lineChartData: lineChart,
        barChartData: barChart,
        entries: getEntriesIndicator(entries)
            .sort(sortByValue)

    }

    removeUndefinedProps(mappedData)

    return mappedData;
};
