import { sum } from 'lodash';

import { BiometricEntrySchema } from '../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../shared/utils/groupByMap';
import { sortByValueDesc } from "../sortByHelpers";


export const getHappyChappySuperstars = (biometricEntries: Array<BiometricEntrySchema>): Array<{
    uid: string;
    name: string;
    value: number;
}> => {
    // const entries = biometricEntries.filter((entry) =>
    //     entry.value < 4) || [];
    const theEntries = [];
    const groupedEntries = groupByMap(biometricEntries, (entry: BiometricEntrySchema) => entry.userFullName + '__' + entry.athleteID) as Map<string, Array<BiometricEntrySchema>>;
    groupedEntries.forEach((athleteEntries, uid) => {

        const athleteFullName = uid.split('__')[0]
        const athleteID = uid.split('__')[1]


        const res = {
            uid: athleteID,
            name: athleteFullName,
            value: sum(athleteEntries
                .map((ent) => ent.value))
        };

        // const res = {
        //     name: athleteEntries[0].name,
        //     value: athleteEntries.length
        // };
        theEntries.push(res);
    });
    return theEntries.sort(sortByValueDesc);
};
