import { mean } from 'lodash';

import { BiometricFoodEntrySchema, BiometricPainEntrySchema, BiometricEntrySchema } from '../../../../../../../pubsub/interfaces/red-flag-reports';
import { groupByMap } from '../../../../../../../shared/utils/groupByMap';
import { getPainAthletes } from './pain/getPainAthletes';
import { getFoodMap } from './maps/getFoodMap';
import { sortByCountDesc, sortByAvgDesc } from '../sortByHelpers';
import moment from 'moment';

export const getFoodTypeCounts = (
    foodEntries: BiometricFoodEntrySchema[],
    setAllToToday: boolean,
    startOfWeek: moment.Moment
) => {
    const groupedFoodEntries = groupByMap(foodEntries, (entry: BiometricFoodEntrySchema) => entry.foodType);
    const distinctFoodTypes = foodEntries.filter((thing, i, arr) => {
        return arr.indexOf(arr.find(t => t.foodType === thing.foodType)) === i;
    });
    const foodTypeCounts = [];
    distinctFoodTypes.forEach((t) => {
        const foodTypeEntries = getFoodMap(groupedFoodEntries.get(t.foodType), setAllToToday, startOfWeek);
        foodTypeCounts.push({ foodType: t.foodType, count: foodTypeEntries.length });
    });
    return foodTypeCounts.sort(sortByCountDesc);
};

export const getMealTypeAverages = (
    foodEntries: BiometricFoodEntrySchema[],
    simpleFoodEntries: BiometricEntrySchema[],
    setAllToToday: boolean,
    startOfWeek: moment.Moment
) => {
    const groupedMealEntries = groupByMap(foodEntries.length ? foodEntries : simpleFoodEntries, (entry: BiometricFoodEntrySchema) => entry.mealType);
    const distinctMealType = foodEntries.length
        ? foodEntries.filter(filterByMealType())
        : simpleFoodEntries.filter(filterByMealType());
    const mealTypeAverages = [];
    distinctMealType.forEach((t) => {
        const mealTypeEntries = getFoodMap(groupedMealEntries.get(t.mealType), setAllToToday, startOfWeek);
        mealTypeAverages.push({ mealType: t.mealType, avg: mean(mealTypeEntries.map(entry => entry.value)) });
    });
    return mealTypeAverages.sort(sortByAvgDesc);
};

export const getAggregatePayload = (
    foodEntries: BiometricFoodEntrySchema[],
    simpleFoodEntries: BiometricEntrySchema[],
    painEntries: BiometricPainEntrySchema[],
    setAllToToday: boolean,
    startOfWeek: moment.Moment

) => {
    const mealTypeAverages = getMealTypeAverages(foodEntries, simpleFoodEntries, setAllToToday, startOfWeek);
    const foodTypeCounts = getFoodTypeCounts(foodEntries, setAllToToday, startOfWeek);
    const athletesWithPain = getPainAthletes(painEntries, setAllToToday, startOfWeek);
    return { athletesWithPain, mealTypeAverages, foodTypeCounts };
};

function filterByMealType(): (
    value: BiometricFoodEntrySchema,
    index: number,
    array: BiometricFoodEntrySchema[]
) => unknown {
    return (thing, i, arr) => {
        return arr.indexOf(arr.find(t => t.mealType === thing.mealType)) === i;
    };
}
