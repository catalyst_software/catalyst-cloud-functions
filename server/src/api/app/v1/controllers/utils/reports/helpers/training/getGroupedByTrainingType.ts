import { sum } from 'lodash';

import { removeUndefinedProps } from '../../../../../../../../db/ipscore/services/save-aggregates';
import { TrainingTrendData } from '../../types/training-trend-data';

export const getGroupedByTrainingType = (athlteteTrainingEntries: TrainingTrendData[]) => {

    const sportTypeData: TrainingTrendData = [];

    removeUndefinedProps(athlteteTrainingEntries)



    const totalCount = sum(sportTypeData.map((sportType) => sportType.totalValue));

    const theResultData = sportTypeData.map((intesityGroup) => {
        const { firstName, fullName, ...theRestData } = intesityGroup

        const avgOutOfHundred = theRestData.totalValue / totalCount * 100;
        theRestData.avg = avgOutOfHundred;
        return theRestData
    });



    // removeUndefinedProps(painLevelData)
    removeUndefinedProps(theResultData)

    return theResultData;
};
