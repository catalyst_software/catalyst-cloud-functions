import { AthleteIndicator } from '../../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { EmailMessage, NotificationSeverity } from '../../../sendGrid/conrollers/email';
import { ReportMessagingTemplate } from '../../../sendGrid/templates/ipscore-non-engagement';
import { notifyCoachesReportsReady } from '../../../callables/utils/comms/notify-coaches';

export const sendNotification = async (coach: AthleteIndicator, theEmailMessage: EmailMessage) => {
    const emailMessageTemplate: ReportMessagingTemplate = {
        to: [],
        from: {
            name: "iNSPIRE",
            email: "development@inspiresportaustralia.com"
        },
        templateId: "d-b95ae611e7124aafa1b948ca09ad3c2d",
        // emailTemplateName: "badMood",
        emailTemplateName: "ipScoreNonEngagement",
        notificationSeverity: NotificationSeverity.Success,
        emailTemplateData: {
            name: 'group.groupName',
            // athletes: coach.athletes.map((athlete) => {
            //     const startOfToday = moment.utc().startOf('day');
            //     const lastLoggedDay = moment(athlete.currentIpScoreTracking.dateTime.toDate()).startOf('day');
            //     const daysSinceLastEntry = startOfToday.diff(lastLoggedDay, 'days');
            //     console.log('Days Since Last Entry', daysSinceLastEntry);
            //     return {
            //         uid: athlete.uid,
            //         fullName: athlete.fullName,
            //         email: athlete.email,
            //         daysSinceLastEntry
            //     }
            // }),
            // numberOfAthletes: coach.athletes.length,
            // dayCountNonEngagement: flattenedAthletes.org.nonEngagementConfiguration.dayCountNonEngagement,
            // subject: "Custom Email Subject",
            // message: "<h1>Custom messtage</h1>",
            showDownloadButton: false,
            barType: {
                danger: true
            }
        }
    };
    const theRes = await notifyCoachesReportsReady(emailMessageTemplate, [coach]);
    return theRes;
};
