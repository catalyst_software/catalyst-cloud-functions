import { isArray } from 'lodash';

import { Athlete } from '../../../../../models/athlete/athlete.model';
import { AthleteSubscription } from '../../../../../models/athlete/interfaces/athlete-subscription';

export const getAthleteSub = (theAth: Athlete, appIdentifier: string): AthleteSubscription => {
    
    let sub: AthleteSubscription;
    // let force = false;
    const prof = theAth.profile;
    if (!prof.subscription.appIdentifier) {
        sub = prof.subscription;
    }
    else {
        if (prof.subscription.appIdentifier.toLowerCase() === appIdentifier.toLowerCase()) {
            sub = prof.subscription;
        }
        else {
            if (isArray(prof.appSubscriptions)) {
                sub = prof.appSubscriptions.find((s: AthleteSubscription) => {
                    return s.appIdentifier.toLowerCase() === appIdentifier.toLowerCase();
                });
                if (!sub) {
                    // force = true;
                }
            }
            else {
                // force = true;
            }
        }
    }
    return sub;
};
