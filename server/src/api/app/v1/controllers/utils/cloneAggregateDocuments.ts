import { firestore } from 'firebase-admin';
import * as functions from 'firebase-functions'
import firebase_tools from 'firebase-tools'

import { db, catalystDB } from '../../../../../shared/init/initialise-firebase';

import { Athlete } from '../../../../../models/athlete/interfaces/athlete';
import { Group } from '../../../../../models/group/group.model';

import { AthleteIndicator } from '../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { GroupIndicator } from '../../../../../db/biometricEntries/well/interfaces/group-indicator';

import { WellAggregateCollection } from '../../../../../db/biometricEntries/enums/firestore-aggregate-collection';
import { WellAggregateHistoryCollection } from '../../../../../db/biometricEntries/enums/firestore-aggregate-history-collection';

import { IpScoreAggregateCollection } from '../../../../../db/ipscore/enums/firestore-ipscore-aggregate-collection';
import { IpScoreAggregateHistoryCollection } from '../../../../../db/ipscore/enums/firestore-ipscore-aggregate-history-collection';

import { isProductionProject } from '../../is-production';

import { getAthleteDocRef, getDocFromRef, getGroupDocRef } from '../../../../../db/refs';

import { removeUndefinedProps } from '../../../../../db/ipscore/services/save-aggregates';

const aggregateCollectionKeys = getKeys(WellAggregateCollection);
const aggregateHistoryCollectionKeys = getKeys(WellAggregateHistoryCollection);
const ipScoreAggregateCollectionKeys = getKeys(IpScoreAggregateCollection);
const ipScoreAggregateHistoryCollectionKeys = getKeys(IpScoreAggregateHistoryCollection);

export const cloneAggregateDocuments = async (entityId: string) => {

    const collections = [];

    addDocumentReferences(aggregateCollectionKeys, WellAggregateCollection, collections);
    addDocumentReferences(aggregateHistoryCollectionKeys, WellAggregateHistoryCollection, collections);
    addDocumentReferences(ipScoreAggregateCollectionKeys, IpScoreAggregateCollection, collections);
    addDocumentReferences(ipScoreAggregateHistoryCollectionKeys, IpScoreAggregateHistoryCollection, collections);

    const promises = [];

    collections.map(async (collection) => {
        const docRef = catalystDB
            .collection(collection)
            .where('entityId', '==', entityId);
        const promise = docRef
            .get()
            .then(async (docsQuerySnap: firestore.QuerySnapshot) => {
                if (!docsQuerySnap.empty) {

                    const addResult = await Promise.all(docsQuerySnap.docs.map(async doc => {
                        const entity = await db.collection(collection).doc(doc.id).get()
                        if (!entity.exists) {
                            return await db.collection(collection).doc(doc.id).set(doc.data()).then(() => true)
                        } else {
                            return false
                        }

                        // return await doc.ref.delete().then(() => {
                        //     return {
                        //         uid: doc.id,
                        //         path: doc.ref.path
                        //     }
                        // });
                    }));
                    const message = `Documents Added for entityUid ${entityId}! Collection ${collection}`;
                    console.log(message);
                    return {
                        message, deleteResult: addResult
                    };
                }
                else {
                    // doc.data() will be undefined in this case
                    const message = `No documents found for entityUid ${entityId}! Collection ${collection}`;
                    console.log(message);
                    return { message };
                }
            })
            .catch(error => {
                const errorMessage = `Error getting document for entityUid ${entityId}! Collection ${collection}`;
                console.log('Error getting document:', error);
                return errorMessage;
            });
        promises.push(promise);
    });

    return await Promise.all(promises)
        .then(() => {
            const mse = { addCount: promises.length, error: undefined };
            return mse;
        })
        .catch(err => {
            const mse = { addCount: promises.length, error: err };
            console.log(mse);
            throw err;
        });
};




function addDocumentReferences(collectionKeys: string[], enumObject, collections: any[]) {

    collectionKeys.forEach(collection => {
        collections.push(enumObject[collection]);
    });
}

function getKeys(object) {
    return Object.keys(object).filter(k => typeof object[k as any] === 'string');
}


export const purgeAllIPScoreAggregateDocuments = async (forceOnProd = false) => {

    const collections = [];

    if (!isProductionProject() || forceOnProd) {

        addDocumentReferences(ipScoreAggregateCollectionKeys, IpScoreAggregateCollection, collections);
        addDocumentReferences(ipScoreAggregateHistoryCollectionKeys, IpScoreAggregateHistoryCollection, collections);

        const promises = [];

        collections.map(async (collection) => {
            const docRef = db
                .collection(collection)
            // .where('entityId', '==', entityId);
            const promise = docRef
                .get()
                .then(async (docsQuerySnap: firestore.QuerySnapshot) => {
                    if (!docsQuerySnap.empty) {
                        const deletePromises = [];
                        docsQuerySnap.docs.forEach(doc => {
                            deletePromises.push(doc.ref.delete());
                        });
                        return await Promise.all(deletePromises);
                    }
                    else {
                        // doc.data() will be undefined in this case
                        const message = `No documents found!`;
                        console.log(message);
                        return message;
                    }
                })
                .catch(error => {
                    const errorMessage = `Error getting documents!`;
                    console.log('Error getting document:', error);
                    return errorMessage;
                });
            promises.push(promise);
        });

        return await Promise.all(promises)
            .then(() => {
                const mse = { deletedCount: promises.length, error: undefined };
                return mse;
            })
            .catch(err => {
                const mse = { deletedCount: promises.length, error: err };
                console.log(mse);
                throw err;
            });

    } else {
        const mse = { deletedCount: 0, error: undefined };
        return mse;

    }
};

export const cleanUpAthleteRelatedDocuments = async (athleteUID: string) => {


    const athleteRef = getAthleteDocRef(athleteUID)

    const athlete = await getDocFromRef(athleteRef) as Athlete

    if (athlete) {
        const removedIndicatorsResult = await Promise.all((athlete.groups || []).map(async (groupIndicator: GroupIndicator) => {
            const groupDocRef = getGroupDocRef(groupIndicator.groupId)
            const group: Group = await getDocFromRef(groupDocRef) as Group
            const athleteIndicator: AthleteIndicator = group.athletes.find((ath) => ath.uid === athleteUID)

            if (athleteIndicator) {
                const removeResult = await groupDocRef.update({
                    athletes: firestore.FieldValue.arrayRemove(athleteIndicator)
                }).then(() => {
                    return {
                        message: 'Athlete Indicator Removed grom group',
                        athleteIndicator,
                        group: {
                            uid: group.groupId,
                            name: group.groupName
                        },
                        err: undefined
                    }
                }).catch((err) => {
                    return {
                        message: 'ERROR removing Athlete Indicator grom group',
                        athleteIndicator,
                        group: {
                            uid: group.groupId,
                            name: group.groupName
                        },
                        err
                    }
                })
                if (removeResult.err) {
                    removeUndefinedProps(removeResult)
                    console.warn(removeResult)
                } else {
                    removeUndefinedProps(removeResult)
                    console.log(removeResult)
                }

                return removeResult
            } else {

                return {
                    message: 'ERROR Athlete Indicator not found on group',
                    athleteIndicator,
                    group: {
                        uid: group.groupId,
                        name: group.groupName
                    },
                    err: undefined
                }

            }

        }))


        if (athlete.profile.onboardingCode.toUpperCase() === 'INSPIRE') {

            const orgId = athlete.organizations ? athlete.organizations[0].organizationId : athlete.organizationId || 'UNKNOWN'
            const orgName = athlete.organizations ? athlete.organizations[0].organizationName : 'UNKNOWN'

            await db.collection('orgsToDelete').doc(orgId).set({
                organizationId: orgId,
                organizationName: orgName,
                groups: athlete.groups || [],
                athleteHasGroups: athlete.groups && athlete.groups.length
            })
        }

        if (!isProductionProject()) {

            const path = `athletes/${athleteUID}`;

            const project =
                //  process.env.GCLOUD_PROJECT || 
                'inspire-1540046634666'

            console.log('functions.config()', functions.config())

            const purgeResult = firebase_tools.firestore
                .delete(path, {
                    project: project,
                    recursive: true,
                    yes: true,
                    token: '1/_FbrZb-uLNo0UIOFNpfdDmuzQxFsKWuML55DV1zI5Fk' // functions.config().fb.token
                })
                .then(() => {
                    return {
                        message: 'Athlete Document and sub collections purged and Athlete Indicator removed from related groups',
                        path: path,
                        athlete,
                        removedIndicatorsResult,
                        err: undefined
                    };
                }).catch((err) => {
                    return {
                        message: 'ERROR Retrieving Athlete Document and sub collections, Athlete Indicator removed from related groups',
                        path: path,
                        athlete,
                        removedIndicatorsResult,
                        err
                    };
                });

            return await purgeResult;


        } else {
            const mse = { deletedCount: 0, error: undefined };
            return mse;

        }
    } else {

        console.error({
            message: 'ERROR  Athlete Document does not excist, sub collections are stale',

            athlete,

            err: 'Athlete Doc Does Not Exist, clear out sub collections'
        })
        return {
            message: 'ERROR  Athlete Document does not excist, sub collections are stale',

            athlete,

            err: 'Athlete Doc Does Not Exist, clear out sub collections'
        };
    }

};
