import { resetIPTracking } from './../../callables/utils/maintenance/resetIPTracking';

import { Athlete } from './../../../../../models/athlete/interfaces/athlete';
import { getAthleteDocRef } from '../../../../../db/refs';
import { updateAthleteIndicatorsIPScoreOnGroups } from '../../../../../analytics/triggers/update-athlete-indicators-ipscore';
import { getUserByEmail } from '../get-user-by-email';
import { getUserFullName } from '../../../../utils/getUserFullName';
import { isArray } from 'lodash';

export const updateAthleteIPScoreValue = async (
    athlete: Athlete,
    runningIPScore: number,
    dailyIPScore: number,
    updateAthleteDocument?: boolean) => {

    console.warn('updateAthleteIPScoreValue', {
        uid: athlete.uid,
        name: getUserFullName(athlete),
        dailyIPScore,
        runningIPScore
    })
    // console.log('running updateAthleteIndicatorsIPScore', {
    //     runningIPScore,
    //     dailyIPScore
    // })
    // Update Athlete Indicators on Groups
    if (athlete.groups && isArray(athlete.groups) && athlete.groups.length) {

        const updateResults: {
            message: string;
            athleteUID: string;
            groupUID: string;
        }[] = await updateAthleteIndicatorsIPScoreOnGroups(
            athlete.uid,
            athlete.groups.map((group) => group.uid || group.groupId),
            runningIPScore,
            dailyIPScore,
            // TODO: Get utcOffset
            0, // utcOffset 
            athlete,
        )
            .catch(err => {
                console.error('Error Occured while trying to run updateAthleteIndicatorsIPScore', err)

                return err
            });

        console.log('end run updateAthleteIndicatorsIPScore', updateResults.length)
    }

    // Update Athlete Document
    if (updateAthleteDocument === undefined || updateAthleteDocument) {

        if (!athlete.currentIpScoreTracking.directive) {
            resetIPTracking(athlete, athlete.currentIpScoreTracking.dateTime)
        }
        try {
            athlete.currentIpScoreTracking.directive.execute = false
            athlete.profile.onboardingSurveryCompleted = true

            athlete.currentIpScoreTracking.ipScore = dailyIPScore
            athlete.runningTotalIpScore = runningIPScore

        } catch (error) {
            debugger;
        }

        if (!athlete.uid) {
            const athleteUID = await getUserByEmail(athlete.profile.email).then((record) => record.uid)
            if (!athleteUID) {
                return undefined
            }
            athlete.uid = athleteUID
        }

        // const {
        //     firebaseMessagingID, firebaseMessagingId, isAnonymous, additionalUserInfo, anonymous, email,
        //     emailVerified, name, phoneNumber, ipScore,
        //     // ...rest
        // } = athlete as any

        // return await getAthleteDocRef(athlete.uid)
        // // .update(rest).catch(err => {
        // .update({
        //     ...rest
        // }
        // ).catch(err => {
        //     console.error('Error Occured while trying to run updateAthleteIPScoreValue', err)

        //     return err
        // });
        return await getAthleteDocRef(athlete.uid)
            .update({
                runningTotalIpScore: runningIPScore,
                'currentIpScoreTracking.ipScore': dailyIPScore,
                'currentIpScoreTracking.directive.execute': false,
                'profile.onboardingSurveryCompleted': true
            }).catch(err => {
                console.error('Error Occured while trying to run updateAthleteIPScoreValue', err)

                return err
            });
    }

};
