import { FirestoreCollection } from '../../../../../db/biometricEntries/enums/firestore-collections'
import { db, theMainApp } from '../../../../../shared/init/initialise-firebase'
import {
    NotificationTemplate,
    KeyValuePair,
} from '../../../../../models/notifications/interfaces/schedule-notification-payload'
import { PushNotificationType } from '../../../../../models/enums/push-notification-type'

export const replaceParts = (
    notificationTemplate: NotificationTemplate,
    keyValuePairs: KeyValuePair[]
) => {
    let theTitle = notificationTemplate.message.notification.title
    keyValuePairs.forEach(pair => {
        theTitle = theTitle.replace(pair.key, pair.value)
    })

    console.warn('notificationTemplate', notificationTemplate)

    let theBody = notificationTemplate.message.notification.body
    keyValuePairs.forEach(pair => {
        console.warn('-----REPLACING BODY KEY/VALUE', pair)
        console.log('theBody', theBody)
        theBody = theBody.replace(pair.key, pair.value)
        console.log('theBody NOW', theBody)

    })

    const res: NotificationTemplate = {
        ...notificationTemplate,
        message: {
            ...notificationTemplate.message,
            notification: {
                ...notificationTemplate.message.notification,
                title: theTitle,
                body: theBody,
            },
        },
    }
    return res
}

export const setAnalytics = (
    // notificationTemplate: PushNotificationTemplate,
    type: PushNotificationType,
    productIdentifier: string
) => {
    let theType: string

    const theProductIdentifier = productIdentifier.toUpperCase().replace(/\./g, '_')

    console.log('-------------------------->>>>>>>>>>>>>>>>>>>>>>>>>> Push Notification Type', type)

    switch (type) {
        case PushNotificationType.SubscriptionRenewed:
            // Renewal Success
            theType = `IAP_RENEWAL_${theProductIdentifier}_SUCCESS`
            break;

        case PushNotificationType.SubscriptionExpired:
        // TODO:
        case PushNotificationType.SubscriptionRenewalFailed:
            // Renewal Failed or Lapsed
            theType = `IAP_RENEWAL_${theProductIdentifier}_FAILED`
            break;

        case PushNotificationType.SubscriptionCanceled:
            // Renewal Cancelled
            theType = `IAP_CANCELLATION_${theProductIdentifier}`
            break;

        case PushNotificationType.SubscriptionLapsed:
            // Renewal Lapsed
            theType = `IAP_${theProductIdentifier}_LAPSED`
            break;

        default:
            break;
    }

    if (theType)
        console.log('-------------------------->>>>>>>>>>>>>>>>>>>>>>>>>> Analytics Type', theType)
    //     notificationTemplate.message.data.actionAnalytics = theType

    return theType

}


    console.log(``)

// TB: THIS IS BEING USED
export const sendNotification = async (
    type: PushNotificationType,
    keyValuePairs: KeyValuePair[],
    token: string
): Promise<{
    message: any
    response?: string
}> => {
    const docSnap = await db
        .collection(FirestoreCollection.NotificationTemplates)
        .where('notificationType', '==', type)
        .get()
    if (docSnap.size) {

        const snap = docSnap.docs[0]
        const notificationTemplate = snap.data() as NotificationTemplate

        // console.log('-------------------------->>>>>>>>>>>>>>>>>>>>>>>>>> Firebase Messaging ID', token)

        const notification = replaceParts(
            notificationTemplate,
            keyValuePairs
        )

        // let actionAnalyticsPair = keyValuePairs.find(o => o.key === 'actionAnalytics');

        const { message, ...rest } = notification

        // if (actionAnalyticsPair) {
        //     console.log(actionAnalyticsPair);
        //     message.data.actionAnalytics =
        //         actionAnalyticsPair.value
        // }

        const response = await theMainApp.messaging().send({
            ...message,
            token,
        })

        // Response is a message ID string.
        console.log('Successfully sent message:', response, {
            message,
            ...rest,
            token,
        })
        return { message: notification, response }
    } else {
        console.log(`Unable to send message: can't find template`, {
            // ...message,
            type,
            token,
            keyValuePairs,
        })
        return {
            message: `Could not load notification from then DB for type ${token}`,
        }
    }
}
