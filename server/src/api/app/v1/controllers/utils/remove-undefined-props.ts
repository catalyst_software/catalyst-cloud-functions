import { isObject } from 'util';
export function removeUndefinedProps(theDoc: {
    [key: string]: any;
}) {
    Object.keys(theDoc).forEach(key => {

        (theDoc[key] === undefined ||
            // theDoc[key] === null ||
            theDoc[key] === '_data') &&
            delete theDoc[key];
        if (isObject(theDoc[key])) {
            removeUndefinedProps(theDoc[key]);
        }
    });
}
