import { Response, Request } from 'express'

import BaseCtrl from './base'

// import { CollectionHistoryDocumentManager } from '../../../../db/biometricHistory/collections/managers/history-document-manager'

import { db } from '../../../../shared/init/initialise-firebase'

import { IpScoreAggregateHistoryCollection } from '../../../../db/ipscore/enums/firestore-ipscore-aggregate-history-collection';
import { IpScoreAggregateCollection } from '../../../../db/ipscore/enums/firestore-ipscore-aggregate-collection';
import { isProductionProject } from '../is-production';

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class CollectionCtrl
 * @extends {BaseCtrl}
 */
export default class CollectionsCtrl extends BaseCtrl {
    res: Response;

    // logger: Logger;
    model = undefined;

    collection = 'FirestoreCollection.Collections';

    // collectionHistoryDocumentManager: CollectionHistoryDocumentManager

    constructor() {
        super()
    }



    // /**
    //  * Adds a biometric entry to the database
    //  * Currently using so that I can test the onCreate events! ;P
    //  *
    //  * @memberof CollectionsCtrl
    //  */
    // addCollection = async (req: Request, res: Response) => {
    //     const { collection } = <{ collection: Collection }>req.body
    //     console.log(req.body)
    //     // let {collection} = <{ a: number, b: string }>groupBy(x);
    //     // See the CollectionRecord reference doc for the contents of collectionRecord.
    //     console.log('...: ', collection.uid)

    //     // save profile
    //     return db
    //         .collection(FirestoreCollection.Collections)
    //         .doc(collection.uid)
    //         .set(collection)
    //         .then(writeResult => {
    //             collection.groups.map(group => {
    //                 if (group.groupId && group.groupId !== '') {
    //                     //Create an empty document, or do nothing if it exists.
    //                     db.collection(FirestoreCollection.Groups)
    //                         .doc(group.groupId)
    //                         .set(group, { merge: true })

    //                         .then(groupWriteResult => {
    //                             return groupWriteResult
    //                         })
    //                 }
    //             })
    //             return res.status(200).json({
    //                 message: 'Collection added',
    //                 responseCode: 201,
    //                 data: {
    //                     collection,
    //                     result: writeResult,
    //                 },
    //                 error: undefined,
    //                 method: {
    //                     name: 'addCollection',
    //                     line: 58,
    //                 },
    //             })
    //         })
    //         .catch(err => {
    //             return res.status(502).json({
    //                 message: 'Error adding Collection',
    //                 responseCode: 201,
    //                 data: {
    //                     collection,
    //                 },
    //                 error: err,
    //                 method: {
    //                     name: 'addCollection',
    //                     line: 58,
    //                 },
    //             })
    //         })
    // }

    deleteIPScoreCollections = async (req: Request, res: Response) => {
        // const { collection } = <{ collection: string }>req.body;
        console.log(req.body);

        const promises = [];

        if (!isProductionProject()) {
            for (const item in IpScoreAggregateCollection) {
                // if (isNaN(Number(item))) {
                //     console.log(item);
                // }

                // let {collection} = <{ a: number, b: string }>groupBy(x);
                // See the CollectionRecord reference doc for the contents of collectionRecord.
                // console.log('...: ', item)
                // See the CollectionRecord reference doc for the contents of collectionRecord.
                console.log('Collection to be deleted ', IpScoreAggregateCollection[item]);
                promises.push(deleteCollection(db, IpScoreAggregateCollection[item], 10).then((response) => {
                    return { message: 'Collection deleted', response }
                }).catch((err) => {
                    return { message: 'Unable to delete collection', error: err }
                }))
            }

            for (let item in IpScoreAggregateHistoryCollection) {
                // if (isNaN(Number(item))) {
                //     console.log(item);
                // }

                // let {collection} = <{ a: number, b: string }>groupBy(x);
                // See the CollectionRecord reference doc for the contents of collectionRecord.
                // console.log('... ', item)
                // See the CollectionRecord reference doc for the contents of collectionRecord.
                console.log('Collection to be deleted ', IpScoreAggregateHistoryCollection[item]);
                promises.push(deleteCollection(db, IpScoreAggregateHistoryCollection[item], 10).then((response) => {
                    return { message: 'Collection deleted', response }
                }))
            }
        }
        return Promise.all(promises).then((response) => {
            res.status(200).json({ message: 'Collection deleted', response })
        }).catch((err) => {
            res.status(500).json({ message: 'Unable to delete collection', error: err })
        })

    }



}


function deleteCollection(theDbdb, collectionPath, batchSize) {
    const collectionRef = theDbdb.collection(collectionPath);
    const query = collectionRef.orderBy('__name__').limit(batchSize);

    return new Promise((resolve, reject) => {
        return deleteQueryBatch(theDbdb, query, batchSize, resolve, reject);
    });
}

function deleteQueryBatch(theDb, query, batchSize, resolve, reject) {
    return query.get()
        .then((snapshot) => {
            // When there are no documents left, we are done
            if (snapshot.size === 0) {
                return 0;
            }

            // Delete documents in a batch
            const batch = theDb.batch();
            snapshot.docs.forEach((doc) => {
                batch.delete(doc.ref);
            });

            return batch.commit().then(() => {
                return snapshot.size;
            });
        }).then((numDeleted) => {
            if (numDeleted === 0) {
                resolve();
                return numDeleted;
            }

            // Recurse on the next process tick, to avoid
            // exploding the stack.
            process.nextTick(() => {
                return deleteQueryBatch(theDb, query, batchSize, resolve, reject);
            });
        })
        .catch(reject);
}
