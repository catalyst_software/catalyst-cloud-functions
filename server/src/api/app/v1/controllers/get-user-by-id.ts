import { UserRecord } from "firebase-functions/lib/providers/auth";

import { admin } from "../../../../shared/init/initialise-firebase";

export const getUserByID = async (uid) => {
    return await admin.auth().getUser(uid)
        .then(function (userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            // console.log("Successfully fetched user data getUserByID:", userRecord.toJSON());
            // console.log("Successfully fetched user data UserID:", uid);
            return userRecord.toJSON() as UserRecord;
        })
        .catch(function (error) {
            console.log("Error fetching user data By ID:" + ' ' + uid, error);
            return undefined
        });
};
