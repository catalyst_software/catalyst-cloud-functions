
import { Athlete } from './../../../../models/athlete/interfaces/athlete';
import { Response, Request } from 'express';

import BaseCtrl from './base'
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'
import { db } from '../../../../shared/init/initialise-firebase'
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { ReportRequest } from '../../../../models/report-model';
import { createTask } from '../../../../db/reports/createTask';
import { isProductionProject } from '../is-production';


import { generateReportRequest } from './utils/reports/generateReportRequest';
import { getBiometricEntriesDeprecated } from './utils/reports/helpers/getBiometricEntriesDeprecated';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { Organization } from '../../../../models/organization.model';
import moment from 'moment';
import { firestore } from 'firebase-admin';
import { generateInspireOrgSubs, iosNotification, TransactionAuditRecord } from './utils/reports/generateInspireOrgSubs';
import { pick } from 'lodash';
import { groupByMap } from '../../../../shared/utils/groupByMap';


// export const pick = <T extends object, U extends keyof T>(
//     obj: T,
//     paths: Array<U>
// ): Pick<T, U> => {
//     const ret = Object.create(null);
//     for (const k of paths) {
//         ret[k] = obj[k];
//     }
//     return ret;
// }
// function pick<T extends object, U extends keyof T>(
//     obj: T,
//     paths: Array<U>
//   ): Pick<T, U> {
//     const ret = Object.create(null);
//     for (const k of paths) {
//       ret[k] = obj[k];
//     }
//     return ret;
//   }

const getSubset = (obj, ...keys) => keys.reduce((a, c) => ({ ...a, [c]: obj[c] }), {});


/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class ReportCtrl
 * @extends {BaseCtrl}
 */
export default class ReportsCtrl extends BaseCtrl {
    res: Response;

    // logger: Logger;
    model = ReportRequest;
    entries;

    collection = FirestoreCollection.Reports;
    aggregateDocumentManager: AggregateDocumentManager;

    constructor() {
        super()
    }

    private generateTaskCode() {
        const code = Math.random().toString(36).substring(6, 12);
        return Promise.resolve(code)
        // return this.db.collection(this.collection)
        //   .where('organizationCode', '==', code)
        //   .get()
        //   .then((snapshot) => {

        //     if (snapshot.empty) {
        //       return code
        //     }

        //     else {
        //       return undefined
        //     }
        //   })
    }


    /**
        * Get all Reports from the database
        *
        * @memberof ReportsCtrl
        */
    getInspireOrgSubs = async (req: Request, res: Response): Promise<Response> => {

        // console.log(req.params);

        this.res = res;

        // console.log('theQuery', theQuery)

        // return res.send(Buffer.from(reportRequestResponse.allResults));


        try {

            const reportRequestResponse = await generateInspireOrgSubs(<ReportRequest>req.body);

            if (reportRequestResponse.allResults.length) {
              
                // USE TransactionReceipts
                const csvData = [].concat(...reportRequestResponse.allResults
                    .map((transactionAuditRecord) => {


                        const { purchaseReceipt, notifications = [],
                            athleteUid,
                            firstName,
                            lastName } = transactionAuditRecord


                        const theData = []
                        if (!purchaseReceipt.sandbox) {
       

                            const athleteNotifications = mapNotifications(notifications, athleteUid, firstName, lastName);
 
                            const groupedByExpirationDate = groupByMap([].concat(...athleteNotifications), (entry: iosNotification) => moment(entry.expirationDate).format('YYMMDD'));

                            groupedByExpirationDate.forEach((subs, date) => {
        
                                const sub = subs[0] as iosNotification

                                theData.push(sub);
                            });

                        }

                        return theData
                    }))

                const { parse } = require('json2csv');

                const fields = Object.keys(csvData[0]);
                const opts = { fields };

                const csv = parse(csvData, opts);

                res.attachment('subs.csv');
                return res.status(200).send(csv);

                // return res.send(Buffer.from(csv));
            } else {

                return res.json({ message: 'No Data' });
            }

        } catch (err) {
            console.error(err);
            return res.json(err);
        }
    };



    /**
        * Get all Reports from the database
        *
        * @memberof ReportsCtrl
        */
    generateReports = async (req: Request, res: Response): Promise<Response> => {

        // console.log(req.params);

        this.res = res;

        // console.log('theQuery', theQuery)

        const reportRequestResponse = await generateReportRequest(<ReportRequest>req.body);

        return res.json(reportRequestResponse);



    };

    getInspireOrgCount = async (req: Request, res: Response): Promise<any> => {


        const allAthletes: Array<Athlete> = []
        const allBroekAthletes: Array<Athlete> = []

        const response = db.collection('organizations').where('licensesPurchased', '==', 1).get().then(async (orgsSNap) => {


            const allOrgs = await Promise.all(orgsSNap.docs.map((theOrgSnap) => {
                return getDocumentFromSnapshot(theOrgSnap) as Organization
            }))


            const inspireOrgs = allOrgs.filter((org) => org.licensesPurchased === 1)


            await Promise.all(inspireOrgs.map(async (org) => {
                await db.collection('athletes')
                    .where('organizationId', '==', org.uid)
                    // .where('profile.subscription.type', '==', 0)
                    .get().then(async (athQUesyrSNap) => {

                        if (athQUesyrSNap.size) {
                            console.log('ath found')
                        }


                        return athQUesyrSNap.docs.map(async (athSNap) => {
                            const at = getDocumentFromSnapshot(athSNap) as Athlete

                            if (at.profile && at.profile.subscription) {


                                const updateAth = verifyTimestamp(at);

                                if (updateAth) {
                                    await athSNap.ref.update(at)
                                }

                                if (at.profile.subscription.type > 0) {

                                    allAthletes.push(at)
                                }

                            } else {
                                allBroekAthletes.push(at)
                            }
                        })

                    })

            }))

            const res2 = JSON.stringify(allAthletes.map((ath) => {

                let theExpireDate

                try {
                    theExpireDate = ath.profile && ath.profile.subscription && ath.profile.subscription.expirationDate ? ath.profile.subscription.expirationDate.toDate() : 'UNKNOWN'
                } catch (error) {
                    theExpireDate = ath.profile && ath.profile.subscription ? ath.profile.subscription.expirationDate : 'UNKNOWN'
                }

                let theCommencementDate

                try {
                    theExpireDate = ath.profile && ath.profile.subscription && ath.profile.subscription.commencementDate ? ath.profile.subscription.commencementDate.toDate() : 'UNKNOWN'
                } catch (error) {
                    theExpireDate = ath.profile && ath.profile.subscription ? ath.profile.subscription.commencementDate : 'UNKNOWN'
                }

                return { uid: ath.uid, subs: { ...ath.profile.subscription, expirationDate: theExpireDate, commencementDate: theCommencementDate } }
            }))
            console.log(JSON.parse(res2))
            console.log(res2)
            // const theResults = [].concat(...theAthletes)

            return allAthletes
            // const theGroups = await Promise.all(inspireOrgs.map(async (org) => {


            //     // const organizationsRef = getOrgCollectionRef();

            //     // // Create a query against the collection.
            //     // //   const query = organizationsRef.where('state', '==', 'CA')
            //     // const query = organizationsRef.where(
            //     //     'tasks',
            //     //     'array-contains',
            //     //     'taskName.ipScoreNonEnagagement'
            //     // );

            //     // return query.get().then(() => {
            //     //     return directive
            //     // })

            //     const groups = await db.collection('groups').where('organizatoinId', '==', org.uid).get().then(async (groupsSNap) => {

            //         const allOrgGroups = await Promise.all(groupsSNap.docs.map((theOrgSnap) => {
            //             return getDocumentFromSnapshot(theOrgSnap) as Group
            //         }))

            //         return allOrgGroups;
            //     })

            //     return groups;
            //     // return true

            // }));


            // [].concat(...theGroups).map((group: Group) => {

            //     !!group.athletes && group.athletes.map(async (ath) => {


            //         const allAthletes = await db.collection('athletes')
            //             .where('', '==', 1)
            //             .where('organizationId', '==', 1)
            //             .get().then(async (athQUesyrSNap) => {

            //                 return athQUesyrSNap.docs.map((athSNap) => {
            //                     return getDocumentFromSnapshot(athSNap) as Athlete
            //                 })

            //             })

            //         const allOrgGroups = await Promise.all(groupsSNap.docs.map((theOrgSnap) => {
            //             return getDocumentFromSnapshot(theOrgSnap) as Group
            //         }))

            //         return allOrgGroups;
            //     })
            // })
        })

        return res.status(200).json(await response)

    }


    /**
     * Get all Reports from the database
     *
     * @memberof ReportsCtrl
     */
    getAllReports = async (req: Request, res: Response): Promise<Response> => {
        // const { id } = <{ id: any }>req.params
        console.log(req.params);

        this.res = res;


        const snapshot = await db.collection(FirestoreCollection.Reports).get();
        return res.json(snapshot.docs.map(doc => doc.data()));

    };

    /**
     * Gets a Report by uid from the database
     *
     * @memberof ReportsCtrl
     */
    getReport = async (req: Request, res: Response): Promise<any> => {
        const { uid } = <{ uid: any }>req.params;
        const me = this;

        this.res = res;

        const docRef = db.collection(FirestoreCollection.Reports).doc(uid);

        return docRef
            .get()
            .then(function (doc) {
                if (doc.exists) {
                    console.log('Document data:', doc.data());
                    return me.handleResponse({
                        message: 'Report Retrieved',
                        responseCode: 201,
                        data: {
                            report: doc.data(),
                        },
                        method: {
                            name: 'getReport',
                            line: 52,
                        },
                        res: this.res
                    })
                } else {
                    console.log('No such document!');
                    return me.handleResponse({
                        message: 'Unable to retrieve Report - No such document!',
                        responseCode: 204,
                        method: {
                            name: 'getReport',
                            line: 52,
                        },
                        res: me.res
                    })
                }
            })
            .catch(function (error) {
                console.log('Error getting document:', error);
                return me.handleResponse({
                    message: 'Unable to retrieve Report - An error occured',
                    responseCode: 500,
                    method: {
                        name: 'getReport',
                        line: 52,
                    },
                    error,
                    res: this.res
                })
            })
    };

    /**
     * Adds a Report to the database
     *
     * @memberof ReportCtrl
     */
    addReport = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {

        // const { document: report } = <{
        //     document: ReportRequest
        // }>req.body;
        const report = req.body;

        console.log(req.body);
        const me = this;

        this.res = res;

        return this.generateTaskCode().then((taskId) => {

            report.taskId = taskId;

            console.log('taskId', taskId);
            return db
                .collection(FirestoreCollection.Reports)
                .add(report)
                .then(() => {

                    const isProd = false;
                    let project = '';
                    let queue = '';
                    let location = '';
                    let options: { payload: ReportRequest; };
                    // // PRODUCTION
                    if (isProductionProject() || isProd) {
                        project = 'inspire-219716';
                        queue = 'create-reports-queue';
                        location = 'europe-west3';
                        options = { payload: report };
                    } else {
                        project = 'inspire-1540046634666';
                        queue = 'create-reports-queue';
                        location = 'us-central1';
                        options = { payload: report };
                    }

                    console.log('options', options);
                    return createTask(project, location, queue, options)
                        .then((data) => {
                            return me.handleResponse({
                                message: 'Report added',
                                responseCode: 201,
                                data: { taskId, data: data },
                                error: undefined,
                                method: {
                                    name: 'addReport',
                                    line: 58,
                                },
                                res: this.res
                            })
                        })


                })
                .catch(err => {
                    return res.status(502).json({
                        message: 'Error adding Report',
                        responseCode: 501,
                        data: {
                            report: report,
                        },
                        error: err,
                        method: {
                            name: 'getReport',
                            line: 58,
                        },
                    })
                })
        })

    };

    deleteReport = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        console.log(req.body);

        this.res = res;

        // TODO:
        return true
        // return admin
        //     .auth()
        //     .deleteReport(id)
        //     .then(reportRecord => {
        //         // See the ReportRecord reference doc for the contents of reportRecord.
        //         console.log('...: ', reportRecord.uid);

        //         return me.handleResponse({
        //             message: 'Report added',
        //             responseCode: 201,
        //             data: {
        //                 report: reportRecord,
        //             },
        //             error: undefined,
        //             method: {
        //                 name: 'deleteReport',
        //                 line: 129,
        //             },
        //             res: this.res
        //         })

        //         // return res.status(200).json({ report: reportRecord });
        //     })
        //     .catch(function (error) {
        //         console.log('Error creating new report: ', error);
        //         return res.status(502).json({ error })
        //     })
    }
    getBiometricsDeprecated = async (
        req: Request,
        res: Response
    ) => {
        console.log(req.body);

        this.res = res;

        const { entity } = req.body


        // TODO:
        return getBiometricEntriesDeprecated({ athleteId: entity.entityId })
        // return admin
        //     .auth()
        //     .deleteReport(id)
        //     .then(reportRecord => {
        //         // See the ReportRecord reference doc for the contents of reportRecord.
        //         console.log('...: ', reportRecord.uid);

        //         return me.handleResponse({
        //             message: 'Report added',
        //             responseCode: 201,
        //             data: {
        //                 report: reportRecord,
        //             },
        //             error: undefined,
        //             method: {
        //                 name: 'deleteReport',
        //                 line: 129,
        //             },
        //             res: this.res
        //         })

        //         // return res.status(200).json({ report: reportRecord });
        //     })
        //     .catch(function (error) {
        //         console.log('Error creating new report: ', error);
        //         return res.status(502).json({ error })
        //     })
    }
}
function mapNotifications(notifications: iosNotification[], athleteUid: string, firstName: string, lastName: string) {
    const theNotifications = []
    notifications.forEach((n, i) => {

        const {
            webOrderLineItemId,
            itemId,
            uniqueVendorIdentifier,
            uniqueIdentifier,
            unhandledProps,

            originalPurchaseDate,
            purchaseDate,
            expirationDate,
            creationTimestamp,

            ...theCSVData

        } = n;



        theNotifications.push({
            athleteUid,
            firstName,
            lastName,
            originalPurchaseDate: !!originalPurchaseDate ? (originalPurchaseDate as any).toDate() : 'NOT SET',
            purchaseDate: !!purchaseDate ? (purchaseDate as any).toDate() : 'NOT SET',
            expirationDate: !!expirationDate ? (expirationDate as any).toDate() : 'NOT SET',
            ...theCSVData,
            creationTimestamp: !!creationTimestamp ? (creationTimestamp as any).toDate() : 'NOT SET',
        });
    });

    return theNotifications
}

function verifyTimestamp(at: Athlete) {

    let updateAth: boolean;
    try {
        if (typeof at.profile.subscription.expirationDate === 'string' && (at as any).profile.subscription.expirationDate !== 'UNKNOWN') {
            at.profile.subscription.expirationDate = firestore.Timestamp.fromDate(moment(at.profile.subscription.expirationDate).toDate());
            updateAth = true;
        }
        if (typeof at.profile.subscription.commencementDate === 'string' && (at as any).profile.subscription.commencementDate !== 'UNKNOWN') {
            at.profile.subscription.commencementDate = firestore.Timestamp.fromDate(moment(at.profile.subscription.commencementDate).toDate());
            updateAth = true;
        }
    }
    catch (error) {
        console.warn(error);
    }
    return updateAth;
}

