import { WhiteLabelTheme } from './../interfaces/WhiteLabelTheme'
import { OrganizationInterface } from './../../../../models/organization.model'
import { addAthleteLookupRecord } from './../callables/get-lookup-values'
import { Response, Request } from 'express'
import { firestore } from 'firebase-admin'
import * as firebase_tools from 'firebase-tools'
import { Client } from 'pg'
import * as uuid from 'uuid'

import BaseCtrl from './base'
import { Organization } from '../../../../models/organization.model'
import {
    liveAdmin,
    catalystDB,
} from '../../../../shared/init/initialise-firebase'
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'

import {
    Athlete,
    OrganizationIndicator,
} from './../../../../models/athlete/interfaces/athlete'

import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot'
import {
    SubscriptionType,
    SubscriptionStatus,
    ProgramContentType,
} from '../../../../models/enums/enums.model'
import {
    getOrgCollectionRef,
    getCollectionRef,
    getAthleteDocRef,
    getAthleteCollectionRef,
} from '../../../../db/refs'

import { OnboardingValidationResponse } from '../interfaces/onboarding-validation-response'
import { fixIPScoreMontylyNAN } from './utils/fixIPScoreMontylyNAN'
import { NotificationTemplate } from '../../../../models/notifications/interfaces/schedule-notification-payload'
import { QuerySnapshot } from '@google-cloud/firestore'
import { DocMngrActionTypes } from '../../../../db/biometricEntries/enums/document-manager-action-types'

import { DoReadPayload } from './interfaces/do-read-payload'
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { AthleteBiometricEntryModel } from '../../../../db/biometricEntries/well/athelete-biometric-entry.model'
import moment from 'moment'
import { copyTheTheme } from './copyTheTheme'
import { replaceAthletesTheme } from './replaceAthletesTheme'
import { Group } from '../../../../models/group/interfaces/group'
import { ResponseObject } from '../../../../shared/ResponseObject'
import { isArray } from 'lodash'
import { AthleteIndicator } from '../../../../db/biometricEntries/well/interfaces/athlete-indicator'
import { getGroupDocRef } from '../../../../db/refs/index'
import { initialiseCloudSql } from '../../../../shared/init/initialise-cloud-sql'
import { Program } from '../../../../db/programs/models/interfaces/program'
import {
    manageDataWarehouseProgramContentDimension,
    manageDataWarehouseProgramDimension,
} from '../../../../pubsub/subscriptions/taskWorkers/helpers/helpers'
import { ProgramCatalogue } from '../../../../db/programs/models/interfaces/program-catalogue'
import {
    ProgramContent,
    ProgramDay,
} from '../../../../db/programs/models/interfaces/program-day'
import { AthleteBiometricEntryInterface } from '../../../../db/biometricEntries/well/aggregate/interfaces'
import { HealthComponentType } from '../../../../db/biometricEntries/well/enums/health-component-type'
import {
    ScheduledTaskStatusType,
    ScheduledTaskWorkerType,
} from '../../../../db/taskQueue/enums/scheduled-task.enums'
import { BiometricTask } from '../../../../db/taskQueue/interfaces/scheduled-task-payload.interface'
import { ScheduledTask } from '../../../../db/taskQueue/interfaces/scheduled-task.interface'
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type'
import { Config } from '../../../../shared/init/config/config'
import {
    AthleteDistanceAggregate,
    AthleteDailyAggregate,
} from './../../../../db/evolution/onUpdate'
import { secondaryDB } from './../../../../shared/init/initialise-firebase'
import { foodLexicon } from './diet'

const functions = require('firebase-functions')

export interface HcpReportRecord {
    date_time: string
    region: string
    total_distance_in_meters: number
    total_time_in_mins: number
}

/**
* The controller handles the requests passed along by the router
// * @param logger: winston.Logger
*/
export default class MaintenanceCtrl extends BaseCtrl {
    res: Response

    model = Organization
    collection = undefined // FirestoreCollection.Organizations
    aggregateDocumentManager: AggregateDocumentManager
    // db = initialiseFirebase('API').db;

    constructor() {
        super()
        // this.db = initialiseFirebase('API').db;
    }

    private getAllGroupDocs = (): Promise<Array<Group>> => {
        const groups: Array<Group> = []

        return this.db
            .collection(FirestoreCollection.Groups)
            .get()
            .then(async docs => {
                const ln = docs.docs.length
                console.log(`Current groups length: ${ln}`)

                if (ln) {
                    for (const groupDoc of docs.docs) {
                        if (groupDoc.exists) {
                            const group = groupDoc.data() as Group
                            group.uid = groupDoc.id

                            groups.push(group)
                        }
                    }
                }

                return groups
            })
            .catch(err => {
                throw err
            })
    }

    private getAllAthleteIndicatorsInGroup = (
        group: Group
    ): Promise<Array<AthleteIndicator>> => {
        const athletes: Array<AthleteIndicator> = []
        return this.db
            .collection(FirestoreCollection.Groups)
            .doc(group.groupId)
            .collection(FirestoreCollection.Athletes)
            .get()
            .then(async docs => {
                const ln = docs.docs.length
                console.log(`Current athletes length: ${ln}`)

                if (ln) {
                    for (const athleteDoc of docs.docs) {
                        if (athleteDoc.exists) {
                            const ath = athleteDoc.data() as AthleteIndicator
                            ath.uid = athleteDoc.id

                            athletes.push(ath)
                        }
                    }
                }

                return athletes
            })
            .catch(err => {
                throw err
            })
    }

    private removeAthleteSubcollection = (group: Group): Promise<boolean> => {
        const path = `/groups/${group.uid}/athletes`
        return firebase_tools.firestore
            .delete(path, {
                project: process.env.GCLOUD_PROJECT,
                recursive: true,
                yes: true,
                token: functions.config().fb.token,
            })
            .then(() => {
                console.log(
                    `Athletes Subcollection Removed: Group UID: ${group.uid}`
                )
                return true
            })
            .catch(err => {
                console.error(
                    `Error occured when executing recursive delete operation on path ${path}.  Error: ${err}`
                )
                return false
            })
    }

    private async enqueueTask(
        athleteId: string,
        orgId: string,
        task: ScheduledTask
    ): Promise<boolean> {
        task.bundleId = 'LEGACY_MIGRATION'
        task.segmentId = orgId
        task.athleteId = athleteId
        task.status = ScheduledTaskStatusType.Scheduled
        task.creationTimestamp = new Date()
        task.timezoneOffset = task.creationTimestamp.getTimezoneOffset()
        task.deviceType = ''
        task.deviceManufacturer = ''
        task.deviceModel = ''
        task.deviceOs = ''
        task.deviceOsVersion = ''
        task.deviceSdkVersion = ''
        task.deviceUuid = ''

        const success = await this.db
            .collection('taskQueue')
            .add(task)
            .then(docRef => {
                console.log('Task successfully queued.')
                return true
            })
            .catch(err => {
                console.error(`Task queue failed: ${err}`)
                return false
            })

        return success
    }

    private getScheduleTask(athleteId: string, entry: any): ScheduledTask {
        let task: ScheduledTask
        const guid = uuid.v4()
        switch (entry.lifestyleEntryType) {
            case LifestyleEntryType.Mood:
            case LifestyleEntryType.Sleep:
            case LifestyleEntryType.Fatigue:
            case LifestyleEntryType.Sick:
            case LifestyleEntryType.Period:
            case LifestyleEntryType.Steps:
            case LifestyleEntryType.Distance:
            case LifestyleEntryType.HeartRate:
                task = {
                    performAtUtc: 0,
                    worker: ScheduledTaskWorkerType.Biometric,
                    payload: {
                        worker: ScheduledTaskWorkerType.Biometric,
                        globalSessionID: guid,
                        screenSessionID: guid,
                        athleteId,
                        bundleId: 'LEGACY_MIGRATION',
                        programId: null,
                        evolutionInstanceId: null,
                        healthComponentType:
                            HealthComponentType[entry.healthComponentType] ||
                            '',
                        lifestyleEntryType:
                            LifestyleEntryType[entry.lifestyleEntryType] || '',
                        biometricEntryId: entry.uid,
                        value: entry.value,
                    } as BiometricTask,
                } as ScheduledTask
                break
            case LifestyleEntryType.SimpleFood:
                task = {
                    performAtUtc: 0,
                    worker: ScheduledTaskWorkerType.Biometric,
                    payload: {
                        worker: ScheduledTaskWorkerType.Biometric,
                        globalSessionID: guid,
                        screenSessionID: guid,
                        athleteId,
                        bundleId: 'LEGACY_MIGRATION',
                        programId: null,
                        evolutionInstanceId: null,
                        healthComponentType:
                            HealthComponentType[entry.healthComponentType] ||
                            '',
                        lifestyleEntryType:
                            LifestyleEntryType[entry.lifestyleEntryType] || '',
                        biometricEntryId: entry.uid,
                        components: entry.simpleMealComponents,
                    } as BiometricTask,
                } as ScheduledTask
                break
            case LifestyleEntryType.Pain:
                task = {
                    performAtUtc: 0,
                    worker: ScheduledTaskWorkerType.Biometric,
                    payload: {
                        worker: ScheduledTaskWorkerType.Biometric,
                        globalSessionID: guid,
                        screenSessionID: guid,
                        athleteId,
                        bundleId: 'LEGACY_MIGRATION',
                        programId: null,
                        evolutionInstanceId: null,
                        healthComponentType:
                            HealthComponentType[entry.healthComponentType] ||
                            '',
                        lifestyleEntryType:
                            LifestyleEntryType[entry.lifestyleEntryType] || '',
                        biometricEntryId: entry.uid,
                        components: entry.painComponents,
                    } as BiometricTask,
                } as ScheduledTask
                break
            case LifestyleEntryType.Training:
                task = {
                    performAtUtc: 0,
                    worker: ScheduledTaskWorkerType.Biometric,
                    payload: {
                        worker: ScheduledTaskWorkerType.Biometric,
                        globalSessionID: guid,
                        screenSessionID: guid,
                        athleteId,
                        bundleId: 'LEGACY_MIGRATION',
                        programId: null,
                        evolutionInstanceId: null,
                        healthComponentType:
                            HealthComponentType[entry.healthComponentType] ||
                            '',
                        lifestyleEntryType:
                            LifestyleEntryType[entry.lifestyleEntryType] || '',
                        biometricEntryId: entry.uid,
                        components: entry.trainingComponents,
                    } as BiometricTask,
                } as ScheduledTask
                break
        }

        return task
    }

    fixIPScoreNAN = async (req: Request, res: Response) => {
        this.res = res

        try {
            // const { organizationCode: code, athlete, profile, firebaseMessagingId } = <
            //     OnboardingValidationRequest
            //     >req.body
            // return Promise.resolve({})

            const theResponse = await fixIPScoreMontylyNAN()

            // if (force && profile && theResponse.responseCode === 200) {
            // const saveResponse: OnboardingValidationResponse = await saveAthleteProfile(user, theResponse, athleteIndicator, profile, firebaseMessagingId || 'context.instanceIdToken'
            // athleteIndicator.firebaseMessagingIds[0]
            // );
            return this.handleResponse({
                ...theResponse,
                message: ' Aggregate Docs Updated',
                // ...saveResponse,
                res: this.res,
            })
            // } else
            //     return this.handleResponse({
            //         ...theResponse,
            //         data: {
            //             ...theResponse,
            //             onboardingProgram: {
            //                 ...theResponse.onboardingProgram,
            //                 activeDate: theResponse.onboardingProgram.activeDate.toDate().toJSON()
            //             }
            //         },
            //         res: this.res
            //     })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message: 'An unanticipated exception occurred fixIPScoreNAN',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in fixIPScoreNAN',
                    exception: exception,
                },
                method: {
                    name: 'fixIPScoreNAN',
                    line: 30,
                },
                res,
            })
        }
    }

    copyNotificationTemplate = async (req: Request, res: Response) => {
        this.res = res

        try {
            const { name } = <{ name: string }>req.body
            // return Promise.resolve({})

            const template = (await getCollectionRef('notificationTemplates')
                .get()
                .then(querySnap =>
                    getDocumentFromSnapshot(querySnap.docs[0])
                )) as NotificationTemplate

            const theResponse = await getCollectionRef('notificationTemplates')
                .doc(name)
                .set({
                    ...template,
                    uid: name,
                    message: {
                        ...template.message,
                        data: {
                            ...template.message.data,
                            icon: 'happyFace',
                        },
                        notification: {
                            ...template.message.notification,
                            title: 'Your Requested Report is READY!!',
                            body: 'You will find the report you requested in your Inbox!',
                        },
                    },
                    notificationType: 30,
                })

            // if (force && profile && theResponse.responseCode === 200) {
            // const saveResponse: OnboardingValidationResponse = await saveAthleteProfile(user, theResponse, athleteIndicator, profile, firebaseMessagingId || 'context.instanceIdToken'
            // athleteIndicator.firebaseMessagingIds[0]
            // );
            return this.handleResponse({
                ...theResponse,
                message: 'Template Created',
                // ...saveResponse,
                res: this.res,
            })
            // } else
            //     return this.handleResponse({
            //         ...theResponse,
            //         data: {
            //             ...theResponse,
            //             onboardingProgram: {
            //                 ...theResponse.onboardingProgram,
            //                 activeDate: theResponse.onboardingProgram.activeDate.toDate().toJSON()
            //             }
            //         },
            //         res: this.res
            //     })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred copyNotificationTemplate',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in copyNotificationTemplate',
                    exception: exception,
                },
                method: {
                    name: 'copyNotificationTemplate',
                    line: 30,
                },
                res,
            })
        }
    }

    copyTheme = async (req: Request, res: Response) => {
        this.res = res

        try {
            const { themeUID } = <{ themeUID: string }>req.body

            return copyTheTheme(themeUID).then(response => {
                return this.handleResponse({
                    ...response,
                    method: {
                        name: 'copyTheme',
                        line: 30,
                    },
                    res,
                })
            })
            //
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message: 'An unanticipated exception occurred copyTheme',
                responseCode: 500,
                data: undefined,
                error: {
                    message: 'An unanticipated exception occurred in copyTheme',
                    exception: exception,
                },
                method: {
                    name: 'copyTheme',
                    line: 30,
                },
                res,
            })
        }
    }

    replaceAthletesTheme = async (req: Request, res: Response) => {
        this.res = res

        try {
            const { orgUid, athleteUid, themeUid, skipContent } = <
                {
                    orgUid: string
                    athleteUid: string
                    themeUid: string
                    skipContent: boolean
                }
            >req.body

            return replaceAthletesTheme(
                orgUid,
                athleteUid,
                themeUid,
                skipContent
            ).then(response => {
                return this.handleResponse({
                    ...response,
                    method: {
                        name: 'replaceAthletesTheme',
                        line: 30,
                    },
                    res,
                })
            })
            //
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred replaceAthletesTheme',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in replaceAthletesTheme',
                    exception: exception,
                },
                method: {
                    name: 'replaceAthletesTheme',
                    line: 30,
                },
                res,
            })
        }
    }

    copyEmailTemplate = async (req: Request, res: Response) => {
        this.res = res

        try {
            const { name } = <{ name: string }>req.body
            const template = await getCollectionRef('emailTemplates')
                .get()
                .then(querySnap => getDocumentFromSnapshot(querySnap.docs[0]))

            const theResponse = await getCollectionRef('emailTemplates')
                .doc(name)
                .set({
                    ...template,
                    uid: name,
                    // subject: "Your Report is READY!",
                    // title: 'Your Report',
                    showDownloadButton: false,
                    // heading: 'Report Ready',
                    barType: {
                        error: true,
                    },
                    // message: 'Please find your iNSPIRE PDF Report attached with this email',
                })

            // if (force && profile && theResponse.responseCode === 200) {
            // const saveResponse: OnboardingValidationResponse = await saveAthleteProfile(user, theResponse, athleteIndicator, profile, firebaseMessagingId || 'context.instanceIdToken'
            // athleteIndicator.firebaseMessagingIds[0]
            // );
            return this.handleResponse({
                ...theResponse,
                message: 'Template Created',
                // ...saveResponse,
                res: this.res,
            })
            // } else
            //     return this.handleResponse({
            //         ...theResponse,
            //         data: {
            //             ...theResponse,
            //             onboardingProgram: {
            //                 ...theResponse.onboardingProgram,
            //                 activeDate: theResponse.onboardingProgram.activeDate.toDate().toJSON()
            //             }
            //         },
            //         res: this.res
            //     })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred copyNotificationTemplate',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in copyNotificationTemplate',
                    exception: exception,
                },
                method: {
                    name: 'copyNotificationTemplate',
                    line: 30,
                },
                res,
            })
        }
    }

    copyRootCollection = async (req: Request, res: Response) => {
        this.res = res

        try {
            const { collectionName } = <{ collectionName: string }>req.body
            // const template = await getCollectionRef(collectionName).get().then(())

            const collectionDocs = (await getCollectionRef(collectionName)
                .get()
                .then(querySnap =>
                    (querySnap.docs || []).map(docSnap =>
                        getDocumentFromSnapshot(docSnap)
                    )
                )) as { uid: string; channelType: string }[]

            const theResponse = await Promise.all(
                collectionDocs.map(async doc => {
                    const theRes = await catalystDB
                        .collection(collectionName)
                        .doc(doc.channelType)
                        .set(doc)

                    return theRes
                })
            )

            return this.handleResponse({
                ...theResponse,
                message: 'Collection Copied',
                // ...saveResponse,
                res: this.res,
            })

            // const theResponse = await getCollectionRef('emailTemplates').doc(name).set({
            //     ...template,
            //     uid: name,
            //     // subject: "Your Report is READY!",
            //     // title: 'Your Report',
            //     showDownloadButton: false,
            //     // heading: 'Report Ready',
            //     barType: {
            //         error: true
            //     },
            //     // message: 'Please find your iNSPIRE PDF Report attached with this email',
            // })

            // // if (force && profile && theResponse.responseCode === 200) {
            // // const saveResponse: OnboardingValidationResponse = await saveAthleteProfile(user, theResponse, athleteIndicator, profile, firebaseMessagingId || 'context.instanceIdToken'
            // // athleteIndicator.firebaseMessagingIds[0]
            // // );
            // return this.handleResponse({
            //     ...theResponse,
            //     message: 'Template Created',
            //     // ...saveResponse,
            //     res: this.res
            // })
            // } else
            //     return this.handleResponse({
            //         ...theResponse,
            //         data: {
            //             ...theResponse,
            //             onboardingProgram: {
            //                 ...theResponse.onboardingProgram,
            //                 activeDate: theResponse.onboardingProgram.activeDate.toDate().toJSON()
            //             }
            //         },
            //         res: this.res
            //     })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred copyNotificationTemplate',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in copyNotificationTemplate',
                    exception: exception,
                },
                method: {
                    name: 'copyNotificationTemplate',
                    line: 30,
                },
                res,
            })
        }
    }

    updateAthleteSponsoredSubscription = async (
        req: Request,
        res: Response
    ) => {
        this.res = res

        try {
            const { orgCode, groupId } = <
                { name: string; orgCode: string; groupId: string }
            >req.body
            // return Promise.resolve({})

            const responsePromise = await getOrgCollectionRef()
                .where('organizationCode', '==', orgCode)
                .get()
                .then(async querySnapshot => {
                    if (querySnapshot.size === 1) {
                        const orgDocSnap = querySnapshot.docs.pop()
                        const org = getDocumentFromSnapshot(
                            orgDocSnap
                        ) as Organization

                        const {
                            isPrePaid,
                            subscriptionType,
                            prePaidNumberOfMonths: subscriptionLengthInMonths,
                        } = org.onboardingConfiguration

                        if (isPrePaid) {
                            const orgId = org.uid

                            let athletesToUpdate =
                                await getAthleteCollectionRef()
                                    .where('organizationId', '==', orgId)
                                    .get()
                                    .then(querySnap => {
                                        return querySnap.docs.map(
                                            queryDocSnap =>
                                                getDocumentFromSnapshot(
                                                    queryDocSnap
                                                ) as Athlete
                                        )
                                    })
                                    .catch(err => {
                                        console.error('ERROR', err)
                                        throw err
                                    })

                            if (groupId) {
                                athletesToUpdate = athletesToUpdate.filter(
                                    ath =>
                                        ath.groups.find(
                                            grp => grp.groupId === groupId
                                        )
                                )
                            }

                            // const forcePrepaid = (isPrePaid || isCoach) || false

                            const subStartDate = moment()
                            const subEndDate = subStartDate
                                .clone()
                                .add(subscriptionLengthInMonths || 12, 'months')

                            athletesToUpdate = await Promise.all(
                                athletesToUpdate.map(async ath => {
                                    const updated = {
                                        ...ath,
                                        profile: {
                                            ...ath.profile,
                                            subscription: {
                                                isPrePaid:
                                                    ath.transactionIds !==
                                                        undefined &&
                                                    !ath.profile.subscription
                                                        .isPrePaid
                                                        ? !isPrePaid
                                                        : isPrePaid,
                                                // TODO: Check Subscription Type Here
                                                type:
                                                    subscriptionType ||
                                                    SubscriptionType.Basic,
                                                status: SubscriptionStatus.ValidInitial,
                                                commencementDate:
                                                    ath.profile.subscription &&
                                                    ath.profile.subscription
                                                        .commencementDate
                                                        ? ath.profile
                                                              .subscription
                                                              .commencementDate
                                                        : firestore.Timestamp.fromDate(
                                                              subStartDate.toDate()
                                                          ),
                                                expirationDate:
                                                    ath.profile.subscription &&
                                                    ath.profile.subscription
                                                        .expirationDate
                                                        ? ath.profile
                                                              .subscription
                                                              .expirationDate
                                                        : firestore.Timestamp.fromDate(
                                                              subEndDate.toDate()
                                                          ),
                                            },
                                        },
                                    }

                                    // return await getAthleteDocRef(ath.uid).update(updated).then(() => updated)

                                    return updated
                                })
                            )

                            // TODO: isPrePaid || forcePrepaid??
                            // if (isPrePaid && !isCoach) {
                            //     await orgDocSnap.ref
                            //         .update({
                            //             ...org,
                            //             uid: orgDocSnap.id,
                            //             licensesConsumed: org.licensesConsumed + 1,
                            //         })
                            // }
                            const theUpdateResponse = {
                                updatedAthletes: athletesToUpdate,
                                message: 'Athletes Updated',
                                responseCode: 200,
                                data: undefined,
                                method: {
                                    name: 'updateAthleteSponsoredSubscription',
                                    line: 70,
                                },
                            } as OnboardingValidationResponse

                            return theUpdateResponse
                        } else {
                            const theConsumedResponse = {
                                message: 'All licences consumed',
                                responseCode: 400,
                                data: undefined,
                                method: {
                                    name: 'updateAthleteSponsoredSubscription',
                                    line: 70,
                                },
                            } as OnboardingValidationResponse

                            return theConsumedResponse
                        }
                    } else {
                        const theInvalidKeyResponse = {
                            message: 'Invalid Key',
                            responseCode: 404,
                            data: undefined,
                            error: { message: 'Invalid Key' },
                            method: {
                                name: 'updateAthleteSponsoredSubscription',
                                line: 82,
                            },
                        } as OnboardingValidationResponse

                        return theInvalidKeyResponse
                    }
                })
                .catch(exception => {
                    console.log('exception!!!! => ', exception)
                    const theExceptionResponse = {
                        message:
                            'An error exception in updateAthleteSponsoredSubscription err ->' +
                            { ...exception },
                        responseCode: 500,
                        data: undefined,
                        error: {
                            message: 'The exception ->' + { ...exception },
                            exception,
                        },
                    } as OnboardingValidationResponse

                    return theExceptionResponse
                })

            return this.handleResponse({
                ...responsePromise,
                message: 'Athletes Updated',
                // ...saveResponse,
                res: this.res,
            })
            // } else
            //     return this.handleResponse({
            //         ...theResponse,
            //         data: {
            //             ...theResponse,
            //             onboardingProgram: {
            //                 ...theResponse.onboardingProgram,
            //                 activeDate: theResponse.onboardingProgram.activeDate.toDate().toJSON()
            //             }
            //         },
            //         res: this.res
            //     })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred copyNotificationTemplate',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in copyNotificationTemplate',
                    exception: exception,
                },
                method: {
                    name: 'copyNotificationTemplate',
                    line: 30,
                },
                res,
            })
        }
    }

    billingTest = async (req: Request, res: Response) => {
        this.res = res

        try {
            const projectId = 'inspire-219716'
            const keyFilename = './billing-inspire-219716.json'

            const { Storage } = require('@google-cloud/storage')

            // Instantiates a client. Explicitly use service account credentials by
            // specifying the private key file. All clients in google-cloud-node have this
            // helper, see https://github.com/GoogleCloudPlatform/google-cloud-node/blob/master/docs/authentication.md
            // const projectId = 'project-id'
            // const keyFilename = '/path/to/keyfile.json'
            // const storage = new Storage({projectId, keyFilename});

            // // Makes an authenticated API request.
            // try {
            //   const [buckets] = await storage.getBuckets();

            //   console.log('Buckets:');
            //   buckets.forEach((bucket) => {
            //     console.log(bucket.name);
            //   });
            // } catch (err) {
            //   console.error('ERROR:', err);
            // }

            // Imports the Google Cloud client library
            const { CloudBillingClient } = require('@google-cloud/billing')

            // Creates a client
            const client = new CloudBillingClient(
                { projectId, keyFilename }
                // { projectId, keyFilename }
                //     {
                //     "type": "service_account",
                //     "project_id": "inspire-219716",
                //     "private_key_id": "fd177858392f28ad8f9a715baad0be4a5735b9ab",
                //     "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCdsjLEqvr3MrXm\naebZvKVhbwciNQpSrNUjAoTRofGvlFRhBrZ4I3tYAeFv00gyRDRLNClU8P1ivROB\nKcQ3f1nHqhkYMdQctWWiAzlhyvRboXz+kVVYvgKzEpmWFs6WIyvOfbib7DTxSFaI\n+W360cW31qOitrXjezJJ29sVwyvzR3v8HeCVYKkzZPU800r0fUUGup+KBdP0TwWw\nYUjyGg4HO6mlWuv+EjYuHggspn1nqclFD8K7B+k2axQ30cJj6kjDx9WGTSRl1e+f\noQZD5rIMr8J8KcZoAjXz7HQjjWWiVMoHA+FSK0JiFX3DQSUlfSSRvaexHNpUnTUz\n1oChtRDLAgMBAAECggEATQBTe+bFTulrGsyBVbagxpfRUum1VbNhC4xSUI6UFhkG\nrq35cWZZ9xpL+y+e5DSbmFErEXfvMkSC/qHnVvYPX8h2InNKUI5exyJt5YuVkByl\njSRoGUIGzWv6pOgUDgm0fUq0Vyaan2qkHpQ1GLFPqznagzxWSnLAloINU24OyuZL\nm2FB/fVuBJ3dS3US84XXB1b9g1siYJljzrjHEtf2xRHsHx2G/Q/suku+mstZU11F\ne8YneyCZI2GK7IkjtUZ9q+i57HgDlV5udW6sFhoRL2sEwUaZb4icMdRuLMFn2zOB\nApCQZQj08tEVucP/Q82RxaAQBabqcZtHdiSKUqLaEQKBgQDPKg8+/cMOdOlf7WAu\n7BRX5Vd2993A11sQvGPuHmWzznXreUQPrv7gaLxQO2Ilw7d+6oxe9zwVGnTIvld0\nwHTWymYZtwVsSMr2/pzfYwXMS3rR+1Zem6dUrA5m/ue8+MLOue3qgZuGAQms/cgJ\n3fqhLFedPRUNmvDjztFDbmlpSQKBgQDC3tV2R6WSiTSR9sDjRyEsECG4wXIlf6fe\nM25WPwNJ/30ahWKkMaUn+otVglvaR1Qc053JbnAwdWC6o2+ZADbbCBsrTwM9rJkm\n8tg4mYT+emIlCQTKTFGSCikKfFoKUYe1UZnfUIvxsPSUdFeB4r8M/4wO96aeM9sD\nDsRjrYGdcwKBgEJAgjWz9BCIhSYfhRYce/kMKGesp2eHt+DnTtIVOJNDYOTYmFKn\ntW1RMYA7LY1ERUe6fP/V84oQNu8IKHH3JqaWfgaVSI+zy+0nykWKpo0KSpIi3iFN\niDsE92oq9TOzHqH8RkEsB+/YHx8K3lGBgyjORIrNwRDjz0wblUMKaX25AoGAeFOF\n9VIKL7Be8J4vvmcGWaHgYKe6JuNrMS4KvGCOVFvY0M55+S7abxQqgvNKaj7O9jnI\n3T8/MPzZbIVMfGoAdUDNHfHc63Eu5DjhQBmzYDsLbRjjh1KWr5lsfBgKtQJgzJoF\nbDndH7EHZL9t5Tqwg4Mr5XYPwPH+EyZEJ9RUC3cCgYB+x0mjtMbWtk464B3romOB\n79Y6diM/MTQJZeGn8mpTkMUHfr111fjn0vrti4g4+RZnyPpmnfwlVsmLdtZmLY9L\nmg+478HnzKeOGrfuYZbZQUetgvMPt/G9CUJGfJGBGjSXQfYUcgZksn2pqznUaiWk\nIifTiT/SeIo4Dbv4UUE9Fg==\n-----END PRIVATE KEY-----\n",
                //     "client_email": "billing-api@inspire-219716.iam.gserviceaccount.com",
                //     "client_id": "107494381884598203213",
                //     "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                //     "token_uri": "https://oauth2.googleapis.com/token",
                //     "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                //     "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/billing-api%40inspire-219716.iam.gserviceaccount.com"
                // }
            )

            const name = 'inspire' //'inspire-219716' // Project name to list billing accounts for. inspire-1540046634666

            async function listBillingAccounts() {
                const [accounts] = await client.listBillingAccounts({
                    name,
                })
                console.info(`found ${accounts.length} billing accounts:`)
                for (const account of accounts) {
                    console.info(`${account.displayName}:`)
                    console.info(`\topen: ${account.open}`)
                    console.info(
                        `\tparentBillingAccount: ${account.masterBillingAccount}`
                    )
                }
            }
            await listBillingAccounts()

            return this.handleResponse({
                // ...responsePromise,
                message: 'Athletes Updated',
                // ...saveResponse,
                res: this.res,
            })
            // } else
            //     return this.handleResponse({
            //         ...theResponse,
            //         data: {
            //             ...theResponse,
            //             onboardingProgram: {
            //                 ...theResponse.onboardingProgram,
            //                 activeDate: theResponse.onboardingProgram.activeDate.toDate().toJSON()
            //             }
            //         },
            //         res: this.res
            //     })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred copyNotificationTemplate',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in copyNotificationTemplate',
                    exception: exception,
                },
                method: {
                    name: 'copyNotificationTemplate',
                    line: 30,
                },
                res,
            })
        }
    }

    reRunAggregation = async (req: Request, res: Response) => {
        this.res = res

        try {
            const payload = req.body as DoReadPayload

            const { userId: athleteUID } = payload.entry
            // return Promise.resolve({})

            // const athlete = await catalystDB.collection(FirestoreCollection.Athletes).doc(athleteUID).get().then((docSnap) => getDocumentFromSnapshot(docSnap) as Athlete)

            // await getAthleteDocRef(athleteUID).set(athlete)

            const athleteAggregateDocs = await catalystDB
                .collection(FirestoreCollection.Athletes)
                .doc(athleteUID)
                .collection(FirestoreCollection.AthleteBiometricEntries)
                .get()
                .then((querySnap: QuerySnapshot) => {
                    const allDocs = querySnap.docs.map(
                        docSnap =>
                            getDocumentFromSnapshot(
                                docSnap
                            ) as AthleteBiometricEntryModel
                    )

                    const theRes = allDocs.map(async entry => {
                        return getAthleteDocRef(athleteUID)
                            .collection(
                                `${FirestoreCollection.AthleteBiometricEntries}_backup`
                            )
                            .doc(entry.uid)
                            .set(entry)

                        return await this.ProcessRequest({ ...payload, entry })
                    })

                    return this.handleResponse({
                        message: 'reRunAggregation Complete',
                        responseCode: 201,
                        data: theRes,

                        res,
                    })
                })
                .catch(err => {
                    console.error('catchAll exception!!!! => ', err)
                    return this.handleResponse({
                        message:
                            'An unanticipated exception occurred reRunAggregation',
                        responseCode: 500,
                        data: undefined,
                        error: {
                            message:
                                'An unanticipated exception occurred in reRunAggregation',
                            exception: err,
                        },
                        method: {
                            name: 'reRunAggregation',
                            line: 297,
                        },
                        res,
                    })
                })
            // } else
            //     return this.handleResponse({
            //         ...theResponse,
            //         data: {
            //             ...theResponse,
            //             onboardingProgram: {
            //                 ...theResponse.onboardingProgram,
            //                 activeDate: theResponse.onboardingProgram.activeDate.toDate().toJSON()
            //             }
            //         },
            //         res: this.res
            //     })

            return athleteAggregateDocs
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message: 'An unanticipated exception occurred reRunAggregation',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in reRunAggregation',
                    exception: exception,
                },
                method: {
                    name: 'reRunAggregation',
                    line: 297,
                },
                res,
            })
        }
    }

    // Travis
    migrateGroupAthleteIndicators = async (req: Request, res: Response) => {
        this.res = res

        try {
            const groups: Array<Group> = []

            return this.db
                .collection(FirestoreCollection.Groups)
                .get()
                .then(async docs => {
                    const ln = docs.docs.length
                    console.log(`Current groups length: ${ln}`)

                    if (ln) {
                        for (const groupDoc of docs.docs) {
                            if (groupDoc.exists) {
                                const group = groupDoc.data() as Group
                                group.uid = groupDoc.id

                                if (isArray(group.athletes)) {
                                    for (const indicator of group.athletes) {
                                        if (indicator.uid) {
                                            const athleteRef =
                                                await groupDoc.ref
                                                    .collection(
                                                        FirestoreCollection.Athletes
                                                    )
                                                    .doc(indicator.uid)
                                                    .set(indicator)
                                                    .then(doc => {
                                                        console.log(
                                                            `Athlete indicator ${indicator.uid} saved in subcolletion.`
                                                        )
                                                        return doc
                                                    })
                                                    .catch(err => {
                                                        console.error(
                                                            `An error occurred when saving athlete indicator document ${
                                                                indicator.uid
                                                            }.  Error: ${JSON.stringify(
                                                                err
                                                            )}`
                                                        )
                                                    })
                                        }
                                    }

                                    delete group.athletes
                                    const groupRef = await groupDoc.ref
                                        .set(group)
                                        .then(doc => {
                                            console.log(
                                                `Group document ${group.uid} successfully updated.`
                                            )
                                            return doc
                                        })
                                        .catch(err => {
                                            console.error(
                                                `An error occurred when saving group document ${
                                                    group.uid
                                                }.  Error: ${JSON.stringify(
                                                    err
                                                )}`
                                            )
                                        })
                                }
                            }
                        }

                        return this.handleResponse({
                            message: `Successfully migrated ${ln} groups and athlete indicators.`,
                            responseCode: 200,
                            res: this.res,
                        } as ResponseObject)
                    } else {
                        // Return response
                        return this.handleResponse({
                            message: `No groups to migrate`,
                            responseCode: 200,
                            res: this.res,
                        } as ResponseObject)
                    }
                })
                .catch(err => {
                    return this.handleResponse({
                        message:
                            'An unanticipated exception occurred migrateGroupAthleteIndicators',
                        responseCode: 500,
                        data: undefined,
                        error: {
                            message:
                                'An unanticipated exception occurred in migrateGroupAthleteIndicators',
                            exception: err,
                        },
                        method: {
                            name: 'migrateGroupAthleteIndicators',
                            line: 297,
                        },
                        res,
                    })
                })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred migrateGroupAthleteIndicators',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in migrateGroupAthleteIndicators',
                    exception: exception,
                },
                method: {
                    name: 'migrateGroupAthleteIndicators',
                    line: 297,
                },
                res,
            })
        }
    }

    updateFoodLexicon = async (req: Request, res: Response) => {
        this.res = res
        debugger
        try {
            return this.db
                .collection(FirestoreCollection.VoiceRecognitionLexicon)
                .doc('diet')
                .set(foodLexicon)
                .then(() => {
                    return this.handleResponse({
                        message: `Successfully updated diet voice recognition lexicon.`,
                        responseCode: 200,
                        res: this.res,
                    } as ResponseObject)
                })
                .catch(err => {
                    return this.handleResponse({
                        message:
                            'An unanticipated exception occurred updateFoodLexicon',
                        responseCode: 500,
                        data: undefined,
                        error: {
                            message:
                                'An unanticipated exception occurred in updateFoodLexicon',
                            exception: err,
                        },
                        method: {
                            name: 'updateFoodLexicon',
                            line: 297,
                        },
                        res,
                    })
                })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred updateFoodLexicon',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in updateFoodLexicon',
                    exception: exception,
                },
                method: {
                    name: 'updateFoodLexicon',
                    line: 297,
                },
                res,
            })
        }
    }

    createAthleteIndexForGroups = async (req: Request, res: Response) => {
        this.res = res

        try {
            const groups: Array<Group> = []

            return this.db
                .collection(FirestoreCollection.Groups)
                .get()
                .then(async docs => {
                    const ln = docs.docs.length
                    console.log(`Current groups length: ${ln}`)

                    if (ln) {
                        for (const groupDoc of docs.docs) {
                            if (groupDoc.exists) {
                                const group = groupDoc.data() as Group
                                group.uid = groupDoc.id

                                const athleteUids = await groupDoc.ref
                                    .collection(FirestoreCollection.Athletes)
                                    .get()
                                    .then(athDocs => {
                                        const athLn = athDocs.docs.length
                                        console.log(
                                            `Current athletes length for group ${group.uid}: ${athLn}`
                                        )
                                        const ids = []
                                        athDocs.forEach(doc => {
                                            if (doc.exists) {
                                                ids.push(doc.id)
                                            }
                                        })

                                        return ids
                                    })
                                    .catch(err => {
                                        console.error(
                                            `An error occurred when queryinh athlete indicators ${
                                                group.uid
                                            }.  Error: ${JSON.stringify(err)}`
                                        )
                                        return []
                                    })

                                group.athleteIndex = {}
                                athleteUids.forEach((id: string) => {
                                    group.athleteIndex[id] = true
                                })

                                const groupRef = await groupDoc.ref
                                    .set(group)
                                    .then(doc => {
                                        console.log(
                                            `Group document ${group.uid} successfully updated.`
                                        )
                                        return doc
                                    })
                                    .catch(err => {
                                        console.error(
                                            `An error occurred when saving group document ${
                                                group.uid
                                            }.  Error: ${JSON.stringify(err)}`
                                        )
                                    })
                            }
                        }

                        return this.handleResponse({
                            message: `Successfully migrated ${ln} groups and athlete indicators.`,
                            responseCode: 200,
                            res: this.res,
                        } as ResponseObject)
                    } else {
                        // Return response
                        return this.handleResponse({
                            message: `No groups to migrate`,
                            responseCode: 200,
                            res: this.res,
                        } as ResponseObject)
                    }
                })
                .catch(err => {
                    return this.handleResponse({
                        message:
                            'An unanticipated exception occurred createAthleteIndexForGroups',
                        responseCode: 500,
                        data: undefined,
                        error: {
                            message:
                                'An unanticipated exception occurred in createAthleteIndexForGroups',
                            exception: err,
                        },
                        method: {
                            name: 'createAthleteIndexForGroups',
                            line: 297,
                        },
                        res,
                    })
                })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred createAthleteIndexForGroups',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in createAthleteIndexForGroups',
                    exception: exception,
                },
                method: {
                    name: 'createAthleteIndexForGroups',
                    line: 297,
                },
                res,
            })
        }
    }

    revertAthleteSubcollectionOnGroups = async (
        req: Request,
        res: Response
    ) => {
        this.res = res

        try {
            const groups = await this.getAllGroupDocs()
            if (groups.length) {
                for (const group of groups) {
                    const athletes = await this.getAllAthleteIndicatorsInGroup(
                        group
                    )
                    delete group.athleteIndex
                    group.athletes = athletes

                    let ok = await getGroupDocRef(group.uid)
                        .set(group)
                        .then(() => {
                            console.log(
                                `Group ${group.uid} successfully reverted.`
                            )
                            return true
                        })
                        .catch(err => {
                            console.error(
                                `An error occurred reverting group doc ${group.uid}.  Message: ${err.message}`
                            )
                            return false
                        })

                    if (ok) {
                        // ok = await this.removeAthleteSubcollection(group);
                        // if (ok) {
                        //     console.log('Athlete subcollection removed.');
                        // } else {
                        //     console.error('Athlete subcollection removal failure!');
                        // }
                    } else {
                        console.error(`Group ${group.uid} failed to update.`)
                    }
                }

                // Return response
                return this.handleResponse({
                    message: `Reversion complete.`,
                    responseCode: 200,
                    res: this.res,
                } as ResponseObject)
            } else {
                // Return response
                return this.handleResponse({
                    message: `No groups to migrate`,
                    responseCode: 200,
                    res: this.res,
                } as ResponseObject)
            }
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred revertAthleteSubcollectionOnGroups',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in revertAthleteSubcollectionOnGroups',
                    exception: exception,
                },
                method: {
                    name: 'revertAthleteSubcollectionOnGroups',
                    line: 297,
                },
                res,
            })
        }
    }

    migrateAthleteLookupValues = async (req: Request, res: Response) => {
        this.res = res
        let pg: Client
        debugger
        try {
            pg = await initialiseCloudSql()

            return this.db
                .collection(FirestoreCollection.Athletes)
                .get()
                .then(async docs => {
                    const ln = docs.docs.length
                    console.log(`Current athletes length: ${ln}`)

                    if (ln) {
                        let count = 1
                        for (const athleteDoc of docs.docs) {
                            if (athleteDoc.exists) {
                                const athlete = athleteDoc.data() as Athlete
                                await addAthleteLookupRecord(athlete, pg)
                                console.log(
                                    `UID: ${athlete.uid} Count: ${count}`
                                )
                                count++
                            }
                        }
                        return this.handleResponse({
                            message: `Successfully migrated ${ln} athlete lookup values.`,
                            responseCode: 200,
                            res: this.res,
                        } as ResponseObject)
                    } else {
                        // Return response
                        return this.handleResponse({
                            message: `No athletes to migrate`,
                            responseCode: 200,
                            res: this.res,
                        } as ResponseObject)
                    }
                })
                .catch(err => {
                    return this.handleResponse({
                        message:
                            'An unanticipated exception occurred migrateAthleteLookupValues',
                        responseCode: 500,
                        data: undefined,
                        error: {
                            message:
                                'An unanticipated exception occurred in migrateAthleteLookupValues',
                            exception: err,
                        },
                        method: {
                            name: 'migrateAthleteLookupValues',
                            line: 297,
                        },
                        res,
                    })
                })
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred migrateAthleteLookupValues',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in migrateAthleteLookupValues',
                    exception: exception,
                },
                method: {
                    name: 'migrateAthleteLookupValues',
                    line: 297,
                },
                res,
            })
        }
    }

    migrateProgramDimensionValues = async (req: Request, res: Response) => {
        this.res = res
        debugger
        try {
            const catalogues = [
                'general',
                'sportsNutrition',
                'sportsPsychology',
                'sportsScience',
            ]
            const promises = catalogues.map((cat: string) => {
                return new Promise(async resolve => {
                    try {
                        const catalogue = await this.db
                            .collection(FirestoreCollection.ProgramCatalogue)
                            .doc(cat)
                            .get()
                            .then(doc => {
                                return doc.data() as ProgramCatalogue
                            })
                        if (!!catalogue) {
                            for (const program of catalogue.programs) {
                                await manageDataWarehouseProgramDimension(
                                    program.programGuid,
                                    program.name,
                                    this.capitalize(cat)
                                )
                                const programContent = await this.db
                                    .collection(
                                        `${FirestoreCollection.ProgramCatalogue}/${cat}/${FirestoreCollection.ProgramContent}`
                                    )
                                    .doc(program.programGuid)
                                    .get()
                                    .then(doc => {
                                        return doc.data() as ProgramContent
                                    })
                                if (!!programContent) {
                                    if (isArray(programContent.content)) {
                                        for (const day of programContent.content) {
                                            if (isArray(day.entries)) {
                                                for (const entry of day.entries) {
                                                    const type =
                                                        ProgramContentType[
                                                            entry.type
                                                        ]
                                                    await manageDataWarehouseProgramContentDimension(
                                                        program.programGuid,
                                                        program.name,
                                                        program.categoryType,
                                                        entry.fileGuid,
                                                        entry.title,
                                                        type
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (e) {
                        console.error(e)
                    }
                })
            })

            await Promise.all(promises)

            return this.handleResponse({
                message: `Successfully migrated program values.`,
                responseCode: 200,
                res: this.res,
            } as ResponseObject)
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred migrateProgramDimensionValues',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in migrateProgramDimensionValues',
                    exception: exception,
                },
                method: {
                    name: 'migrateProgramDimensionValues',
                    line: 297,
                },
                res,
            })
        }
    }

    migrateOrg = async (req: Request, res: Response) => {
        this.res = res
        debugger
        try {
            const orgClone = await this.db
                .collection(FirestoreCollection.Organizations)
                .doc('h47F8tLIT36zwO2XvCtN')
                .get()
                .then(doc => {
                    return doc.data() as OrganizationInterface
                })

            const newOrg = await this.db
                .collection(FirestoreCollection.Organizations)
                .doc()
            orgClone.uid = newOrg.id
            orgClone.name = 'Peak Evolution 1'
            orgClone.organizationCode = 'PEAK'
            orgClone.theme = 'catalyst'
            orgClone.licensesConsumed = 0
            orgClone.licensesPurchased = 10000
            orgClone.numberUsersOnboarded = 0
            orgClone.onboardingConfiguration.isPrePaid = true
            orgClone.coaches.push('pullicinoroweena@gmail.com')

            const success = await newOrg.set(orgClone).then(() => {
                return true
            })

            return this.handleResponse({
                message: `Successfully created new org.`,
                responseCode: 200,
                res: this.res,
            } as ResponseObject)
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message: 'An unanticipated exception occurred migrateHcpOrg',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in migrateHcpOrg',
                    exception: exception,
                },
                method: {
                    name: 'migrateHcpOrg',
                    line: 297,
                },
                res,
            })
        }
    }

    migrateOrgBiometrics = async (req: Request, res: Response) => {
        this.res = res
        debugger
        try {
            const orgId = req.body.orgId
            let successCount = 0
            let failCount = 0
            if (orgId) {
                console.log(`Running migrateOrgBiometrics with orgId ${orgId}`)
                const athleteUids = await this.db
                    .collection(FirestoreCollection.Athletes)
                    .where('organizationId', '==', orgId)
                    .get()
                    .then(docs => {
                        if (docs.docs.length) {
                            const ids = docs.docs.map(d => {
                                return d.id
                            })

                            return ids
                        } else {
                            return []
                        }
                    })
                    .catch(err => {
                        console.error('Error => ', err)
                        return []
                    })

                for (const uid of athleteUids) {
                    const athleteBiometricEntries = await getAthleteDocRef(uid)
                        .collection(FirestoreCollection.AthleteBiometricEntries)
                        .get()
                        .then(querySnap => {
                            return querySnap.docs.length
                                ? querySnap.docs.map(
                                      queryDocSnap =>
                                          getDocumentFromSnapshot(
                                              queryDocSnap
                                          ) as AthleteBiometricEntryInterface
                                  )
                                : []
                        })

                    if (
                        isArray(athleteBiometricEntries) &&
                        athleteBiometricEntries.length
                    ) {
                        for (const entry of athleteBiometricEntries) {
                            const task = this.getScheduleTask(uid, entry)
                            const success = await this.enqueueTask(
                                uid,
                                orgId,
                                task
                            )

                            if (success) {
                                successCount++
                                console.log(
                                    `Task ${
                                        (task.payload as any).biometricEntryId
                                    } success`
                                )
                            } else {
                                failCount++
                                console.error(
                                    `Task ${
                                        (task.payload as any).biometricEntryId
                                    } failed`
                                )
                            }
                        }
                    }
                }
                return this.handleResponse({
                    message: `Successly scheduled ${successCount} migration tasks.  ${failCount} failed.`,
                    responseCode: 200,
                    res: this.res,
                } as ResponseObject)
            } else {
                return this.handleResponse({
                    message: `OrgId not specified.`,
                    responseCode: 404,
                    res: this.res,
                } as ResponseObject)
            }
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred migrateOrgBiometrics',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in migrateOrgBiometrics',
                    exception: exception,
                },
                method: {
                    name: 'migrateOrgBiometrics',
                    line: 297,
                },
                res,
            })
        }
    }

    extractHcpEventReport = async (req: Request, res: Response) => {
        this.res = res
        debugger
        try {
            const start = moment('2020-10-09', 'YYYY-MM-DD')
            const end = moment('2020-11-01', 'YYYY-MM-DD')
            const records: Array<HcpReportRecord> = []
            const db: firestore.Firestore = secondaryDB
            const athletes = await db
                .collection(FirestoreCollection.Athletes)
                .get()
                .then(docs => {
                    return docs.docs.map(doc => {
                        return { uid: doc.id, ...doc.data() } as Athlete
                    })
                })
            const locations = this.getUniqueLocations(athletes)
            let current = start
            while (current.toDate() < end.toDate()) {
                locations.forEach((loc: string) => {
                    records.push({
                        date_time: current.format('YYYY-MM-DD'),
                        region: loc,
                        total_distance_in_meters: 0,
                        total_time_in_mins: 0,
                    } as HcpReportRecord)
                })

                current = current.add(1, 'day')
            }

            for (const athlete of athletes) {
                const location = athlete.profile
                    ? athlete.profile.location || ''
                    : ''
                const aggregate = await db
                    .collection(FirestoreCollection.AthleteDistanceAggregates)
                    .doc(athlete.uid)
                    .get()
                    .then(doc => {
                        if (doc.exists) {
                            return doc.data() as AthleteDistanceAggregate
                        } else {
                            return undefined
                        }
                    })
                if (aggregate) {
                    aggregate.dailies.forEach(
                        (daily: AthleteDailyAggregate) => {
                            if (daily.dateTime) {
                                const date_time = moment(
                                    (daily.dateTime as any).toDate()
                                ).format('YYYY-MM-DD')
                                const record = records.find(
                                    (rec: HcpReportRecord) => {
                                        return (
                                            rec.date_time === date_time &&
                                            rec.region === location
                                        )
                                    }
                                )

                                if (record) {
                                    record.total_distance_in_meters +=
                                        daily.distance
                                    record.total_time_in_mins += daily.duration
                                }
                            }
                        }
                    )
                }
            }

            const stringRecs: Array<string> = [
                'date_time,region,total_distance_in_meters,total_time_in_mins',
            ]
            records.forEach((rec: HcpReportRecord) => {
                stringRecs.push(
                    `${rec.date_time},${rec.region},${rec.total_distance_in_meters},${rec.total_time_in_mins}`
                )
            })
            const data = stringRecs.join('||')

            return this.handleResponse({
                message: `Successfully created hcp record report.`,
                data,
                responseCode: 200,
                res: this.res,
            } as ResponseObject)
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred extractHcpEventReport',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in extractHcpEventReport',
                    exception: exception,
                },
                method: {
                    name: 'extractHcpEventReport',
                    line: 297,
                },
                res,
            })
        }
    }

    getAllUsersInOrg = async (req: Request, res: Response) => {
        this.res = res
        debugger

        try {
            const output: Array<any> = []
            const orgId = req.body.organizationId
            console.log(`Running getAllUsersInOrg with orgId: ${orgId}`)
            const athletes = await this.db
                .collection(FirestoreCollection.Athletes)
                .where('organizationId', '==', orgId)
                .get()
                .then(docs => {
                    return docs.docs.map(doc => {
                        return { uid: doc.id, ...doc.data() } as Athlete
                    })
                })
            for (const athlete of athletes) {
                const uid = athlete.uid
                const fullName = athlete.profile
                    ? `${athlete.profile.firstName} ${athlete.profile.lastName}`
                    : ''
                const email = athlete.profile ? athlete.profile.email : ''
                const line = `${fullName}, ${email}, ${uid}`
                output.push(line)
            }

            return this.handleResponse({
                message: `Successfully created Aurora user report.`,
                data: output.join('||'),
                responseCode: 200,
                res: this.res,
            } as ResponseObject)
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message: 'An unanticipated exception occurred getAllUsersInOrg',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in getAllUsersInOrg',
                    exception: exception,
                },
                method: {
                    name: 'getAllUsersInOrg',
                    line: 297,
                },
                res,
            })
        }
    }

    removeAllOrgUsers = async (req: Request, res: Response) => {
        this.res = res
        debugger

        return this.handleResponse({
            message: `Disabled.`,
            data: {},
            responseCode: 200,
            res: this.res,
        } as ResponseObject)

        try {
            // KFIT
            const orgId = 'FC2p4MVCkTT2EtHhE0n8'
            const athletes = await this.db
                .collection(FirestoreCollection.Athletes)
                .where('organizationId', '==', orgId)
                .get()
                .then(docs => {
                    return docs.docs.map(doc => {
                        return { uid: doc.id, ...doc.data() } as Athlete
                    })
                })
            for (const athlete of athletes) {
                await liveAdmin
                    .auth()
                    .deleteUser(athlete.uid)
                    .then(() => {
                        console.log(`User account ${athlete.uid} deleted!`)
                    })
                    .catch(err => {
                        console.error(
                            `Error deleting user object ${athlete.uid}`
                        )
                    })
                await this.db
                    .collection(FirestoreCollection.Athletes)
                    .doc(athlete.uid)
                    .delete()
                    .then(() => {
                        console.log(`Athlete object ${athlete.uid} deleted!`)
                    })
                    .catch(err => {
                        console.error(
                            `Error deleting athlete object ${athlete.uid}`
                        )
                    })
            }

            await this.db
                .collection(FirestoreCollection.Organizations)
                .doc(orgId)
                .delete()
                .then(() => {
                    console.log(`Organization object ${orgId} deleted!`)
                })

            return this.handleResponse({
                message: `Successfully deleted org and all org users.`,
                data: {},
                responseCode: 200,
                res: this.res,
            } as ResponseObject)
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred removeAllOrgUsers',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in removeAllOrgUsers',
                    exception: exception,
                },
                method: {
                    name: 'removeAllOrgUsers',
                    line: 297,
                },
                res,
            })
        }
    }

    copyNotificationTemplates = async (req: Request, res: Response) => {
        this.res = res
        debugger

        try {
            const docId = 'distraughtMood'
            const distraughtMood: NotificationTemplate = await this.db
                .collection(FirestoreCollection.NotificationTemplates)
                .doc(docId)
                .get()
                .then(doc => {
                    return doc.data() as NotificationTemplate
                })

            await this.db
                .collection(FirestoreCollection.NotificationTemplates)
                .doc('newContentFeed')
                .set({
                    ...distraughtMood,
                    notificationType: 40,
                    name: 'newContentFeed',
                })
                .then(doc => {
                    console.log('newContentFeed added')
                })

            // await this.db.collection(FirestoreCollection.NotificationTemplates)
            //           .doc('extremeFatigue')
            //           .set({
            //             ...distraughtMood,
            //             notificationType: 23,
            //             name: 'extremeFatigue'
            //           })
            //           .then((doc) => {
            //             console.log('extremeFatigue updated');
            //           });
            // await this.db.collection(FirestoreCollection.NotificationTemplates)
            //           .doc('moderatePainOrAbove')
            //           .set({
            //             ...distraughtMood,
            //             notificationType: 24,
            //             name: 'moderatePainOrAbove'
            //           })
            //           .then((doc) => {
            //             console.log('moderatePainOrAbove updated');
            //           });

            // await this.db.collection(FirestoreCollection.NotificationTemplates)
            //           .doc('sleepDeprivation')
            //           .set({
            //             ...distraughtMood,
            //             notificationType: 22,
            //             name: 'sleepDeprivation'
            //           })
            //           .then((doc) => {
            //             console.log('sleepDeprivation updated');
            //           });

            // await this.db.collection(FirestoreCollection.NotificationTemplates)
            //           .doc('multipleDailyTraining')
            //           .set({
            //             ...distraughtMood,
            //             notificationType: 25,
            //             name: 'multipleDailyTraining'
            //           })
            //           .then((doc) => {
            //             console.log('multipleDailyTraining updated');
            //           });

            // await this.db.collection(FirestoreCollection.NotificationTemplates)
            //           .doc('periodTurnedOn')
            //           .set({
            //             ...distraughtMood,
            //             notificationType: 26,
            //             name: 'periodTurnedOn'
            //           })
            //           .then((doc) => {
            //             console.log('periodTurnedOn updated');
            //           });

            // await this.db.collection(FirestoreCollection.NotificationTemplates)
            //           .doc('periodTurnedOff')
            //           .set({
            //             ...distraughtMood,
            //             notificationType: 27,
            //             name: 'periodTurnedOff'
            //           })
            //           .then((doc) => {
            //             console.log('periodTurnedOff updated');
            //           });

            // await this.db.collection(FirestoreCollection.NotificationTemplates)
            //           .doc('sicknessTurnedOn')
            //           .set({
            //             ...distraughtMood,
            //             notificationType: 28,
            //             name: 'sicknessTurnedOn'
            //           })
            //           .then((doc) => {
            //             console.log('sicknessTurnedOn updated');
            //           });

            // await this.db.collection(FirestoreCollection.NotificationTemplates)
            //           .doc('sicknessTurnedOff')
            //           .set({
            //             ...distraughtMood,
            //             notificationType: 29,
            //             name: 'sicknessTurnedOff'
            //           })
            //           .then((doc) => {
            //             console.log('sicknessTurnedOff updated');
            //           });

            return this.handleResponse({
                message: `Successfully copied notification templates.`,
                data: {},
                responseCode: 200,
                res: this.res,
            } as ResponseObject)
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred copyPaymentFeatures',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in copyPaymentFeatures',
                    exception: exception,
                },
                method: {
                    name: 'copyPaymentFeatures',
                    line: 297,
                },
                res,
            })
        }
    }

    fixIpScoreConfig = async (req: Request, res: Response) => {
        this.res = res
        debugger

        try {
            const existing = await this.db
                .collection(FirestoreCollection.ConsumerModelConfiguration)
                .doc('VsS805gRB3BbmlURO86K')
                .get()
                .then(doc => {
                    return doc.data()
                })
            const newDoc = {
                ipScoreConfiguration: existing,
            }

            await this.db
                .collection(FirestoreCollection.ConsumerModelConfiguration)
                .add(newDoc)
                .then(() => {
                    console.log('ConsumerModelConfiguration updated')
                })

            return this.handleResponse({
                message: `Successfully fixed config.`,
                data: {},
                responseCode: 200,
                res: this.res,
            } as ResponseObject)
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred copyPaymentFeatures',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in copyPaymentFeatures',
                    exception: exception,
                },
                method: {
                    name: 'copyPaymentFeatures',
                    line: 297,
                },
                res,
            })
        }
    }

    private getUniqueLocations(athletes: Array<Athlete>): Array<string> {
        const locations: Array<string> = []
        for (const athlete of athletes) {
            const location = athlete.profile
                ? athlete.profile.location || ''
                : ''
            const idx = locations.findIndex((loc: string) => {
                return loc === location
            })

            if (idx === -1) {
                locations.push(location)
            }
        }

        return locations
    }

    private async ProcessRequest(body: DoReadPayload) {
        const { entry, options } = body
        let { count: theCount } = options
        if (!theCount) {
            theCount = 1
        }

        const athleteUID = 'NEEDED-HERE'
        debugger
        // TODO: CHEKC TRANSACTION
        const t: FirebaseFirestore.Transaction = undefined
        return this.aggregateDocumentManager
            .doRead(
                entry,
                athleteUID,
                DocMngrActionTypes.Read,
                'api/addBiomentricEntryMultiple',
                'Info',
                t
            )
            .then(data => {
                if (data) {
                    console.log(data)
                    this.res.status(200).json({
                        data,
                    })
                    return Promise.resolve(data)
                } else {
                    console.log('empty data')
                    this.res.status(201).json({
                        data,
                    })
                    return Promise.resolve(null)
                }
            })
            .catch(err => {
                console.log('Eooro Occured!!!!', err)
                return Promise.reject(err)
            })
    }

    private cleanString(input: string): string {
        return input.replace("'", "''").replace('"', '').trim()
    }

    private capitalize = s => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }
}
