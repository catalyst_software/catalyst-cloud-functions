import { UserRecord } from "firebase-functions/lib/providers/auth";

import { theMainApp } from "../../../../shared/init/initialise-firebase";

export const getUserByEmail = async (email: string): Promise<UserRecord> => {
    // List batch of users, 1000 at a time.

    if (!email) {
        return undefined
    }
    return await theMainApp
        .auth()
        .getUserByEmail(email)
        .then((userRecord) => {
            // See the UserRecord reference doc for the contents of userRecord.
            console.log("Successfully fetched user data getUserByEmail:", userRecord.toJSON());
            console.log("Successfully fetched user data Email:", email);
            return userRecord.toJSON() as UserRecord;
        })
        .catch((error) => {
            console.log("Error fetching user data By EMail:" + ' ' + email, error);
            return undefined
        });
};
