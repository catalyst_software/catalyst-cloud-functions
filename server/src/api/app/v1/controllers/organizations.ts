import { getDocumentFromSnapshot } from './../../../../analytics/triggers/utils/get-document-from-snapshot';
import { Response, Request } from 'express'
import { firestore } from 'firebase-admin'

import BaseCtrl from './base'
import { Organization, OrganizationInterface } from '../../../../models/organization.model'
import { initialiseFirebase } from '../../../../shared/init/initialise-firebase'
import {
    FirestoreCollection,
} from '../../../../db/biometricEntries/enums/firestore-collections'
import { OnboardingValidationRequest } from '../interfaces/onboarding-validation-request'
import { Program } from '../../../../db/programs/models/program.model'
import { validateOrgCode } from '../callables/utils/onboarding/validate-org-code';

import { sanitizeOnboardingCode } from '../callables/utils/onboarding/sanitize-onboarding-code';
import { createEntities } from '../callables/utils/onboarding/create-entities';
import { getConsumerModelConfig } from '../callables/utils/getConsumerModelConfig';
import { SubscriptionType } from '../../../../models/enums/enums.model';
import { getOrgCollectionRef, getOrgDocRef } from '../../../../db/refs';
import { saveAthleteProfile } from '../callables/saveAthleteProfile';
import { getUserByID } from './get-user-by-id';
import { OnboardingValidationResponse } from '../interfaces/onboarding-validation-response';

/**
* The controller handles the requests passed along by the router
// * @param logger: winston.Logger
*/
export default class OrganizationsCtrl extends BaseCtrl {
    res: Response

    model = Organization
    collection = FirestoreCollection.Organizations

    // db = initialiseFirebase('API').db;

    constructor() {
        super()
        // this.db = initialiseFirebase('API').db;
    }

    /**
     * Validates the registration code submitted by the user
     *
     * @memberof OrganizationCtrl
     */

    /**
     * Validates the registration code submitted by the user
     *
     * @memberof OrganizationsCtrl
     */
    validateOrganizationCode = async (req: Request, res: Response) => {
        this.res = res

        try {
            const { organizationCode: code, athlete, profile, firebaseMessagingId } = <
                OnboardingValidationRequest
                >req.body
            // return Promise.resolve({})

            const theCode = sanitizeOnboardingCode(code)

            if (!this.db) {
                this.db = initialiseFirebase('API').db
            }

            const athleteIndicator = {
                // organizationId: undefined,
                // // organizationName: string;
                // organizations: undefined,

                // uid: '6eIhSgMOWnRprLql6mWF6QpOboB2',
                // firstName: 'Ian Insp 2 Coach',
                // lastName: 'Gouws',
                // email: 'ian.inspire.2@gmx.com',

                // profileImageURL: '',

                // isCoach: undefined,
                // includeGroupAggregation: undefined,

                // ipScore: 0,
                // isIpScoreIncrease: false,
                // isIpScoreDecrease: false,
                runningTotalIpScore: 0,

                // recentEntries: undefined,

                // subscriptionType: undefined,

                // // // TODO: IG - Needed here?
                firebaseMessagingIds: [firebaseMessagingId || 'context.instanceIdToken'],
                // commsConfig: undefined,

                ...athlete,

                creationTimestamp: firestore.Timestamp.now(),
            }

            const user = await getUserByID(athleteIndicator.uid)




            athleteIndicator.ipScore = athleteIndicator.ipScore || 0
            athleteIndicator.runningTotalIpScore = athleteIndicator.runningTotalIpScore || 0
            const force = false
            let theResponse
            if (theCode === 'INSPIRE') {
                theResponse = await createEntities(athleteIndicator, theCode)

                if (force && profile && theResponse.responseCode === 200) {
                    const responseWithAthlete: OnboardingValidationResponse = await saveAthleteProfile(user, theResponse, athleteIndicator, profile, firebaseMessagingId || 'context.instanceIdToken'
                        // athleteIndicator.firebaseMessagingIds[0]
                    );

                    const theOnboardingResponse = {
                        ...responseWithAthlete,
                        data: {
                            ...responseWithAthlete.data,
                            groups: responseWithAthlete.data.groups.map((group) => {
                                return {
                                    ...group,
                                    creationTimestamp: group.creationTimestamp.toDate().toJSON()
                                }
                            })
                        }
                    }

                    return this.handleResponse({ ...theResponse, ...theOnboardingResponse, res: this.res })
                } else
                    return this.handleResponse({
                        ...theResponse,
                        data: {
                            ...theResponse
                        },
                        res: this.res
                    })
            } else {
                theResponse = await validateOrgCode(theCode, athleteIndicator, 0)



                if (force && profile && theResponse.responseCode === 200) {
                    const saveResponse = await saveAthleteProfile(user, theResponse, athleteIndicator, profile, firebaseMessagingId || 'context.instanceIdToken'
                        // athleteIndicator.firebaseMessagingIds[0]
                    );
                    const responseWithAthlete = { ...theResponse, ...saveResponse }

                    // responseWithAthlete.data = { athlete: responseWithAthlete.data.athlete }

                    const theOnboardingResponse = {
                        ...responseWithAthlete,
                        data: {
                            ...responseWithAthlete.data,
                            athleteIndicator: {
                                ...responseWithAthlete.data.athleteIndicator,
                                creationTimestamp: responseWithAthlete.data.athleteIndicator.creationTimestamp.toDate().toJSON()
                            },
                            groups: responseWithAthlete.data.groups.map((group) => {
                                return {
                                    ...group,
                                    creationTimestamp: group.creationTimestamp.toDate().toJSON()
                                }
                            }),
                            subscription: {
                                ...responseWithAthlete.data.subscription,
                                commencementDate: responseWithAthlete.data.subscription.commencementDate.toDate().toJSON(),
                                expirationDate: responseWithAthlete.data.subscription.expirationDate.toDate().toJSON(),
                            }
                        }
                    }

                    return this.handleResponse({
                        ...theOnboardingResponse,
                        // TODO: Needed here???
                        // data: {
                        //     ...responseWithAthlete,
                        //     onboardingProgram: {
                        //         ...responseWithAthlete.onboardingProgram,
                        //         activeDate: responseWithAthlete.onboardingProgram.activeDate.toDate().toJSON()
                        //     }
                        // },
                        res: this.res
                    })
                } else if (theResponse.responseCode === 200) {


                    if (theResponse.message === 'Valid Media Key') {
                        return res.status(200).json(theResponse)
                    } else {
                        const { athleteIndicator: theAthleteIndicator, ...theRestData } = theResponse.data

                        const theResponseData = this.handleResponse({
                            ...theResponse,
                            data: {
                                ...theRestData,
                                athleteIndicator: {
                                    ...theAthleteIndicator,
                                    creationTimestamp: theAthleteIndicator.creationTimestamp.toDate().toJSON()
                                },
                                groups: theRestData.groups.map((group) => {
                                    return {
                                        ...group,
                                        creationTimestamp: group.creationTimestamp.toDate().toJSON()
                                    }
                                }),
                                subscription: {
                                    ...theRestData.subscription,
                                    commencementDate: !!theRestData.subscription && !!theRestData.subscription.commencementDate ? theRestData.subscription.commencementDate.toDate().toJSON() : undefined,
                                    expirationDate: !!theRestData.subscription && !!theRestData.subscription.expirationDate ? theRestData.subscription.expirationDate.toDate().toJSON() : undefined,
                                }
                            },
                            res: this.res
                        })

                        return theResponseData
                    }

                } else {
                    // const { athleteIndicator: toBeRemoved, ...theRestData } = theResponse.data

                    const theResponseData = this.handleResponse({
                        ...theResponse,
                        responseCode: theResponse.responseCode,
                        data: {
                            ...theResponse.data,
                            groups: !theResponse.data ? [] : theResponse.data.groups.map((group) => {
                                return {
                                    ...group,
                                    creationTimestamp: group.creationTimestamp.toDate().toJSON()
                                }
                            }),
                            subscription: !theResponse.data ? undefined : {
                                ...theResponse.data.subscription,
                                commencementDate: theResponse.data.subscription.commencementDate ? theResponse.data.subscription.commencementDate.toDate().toJSON() : null,
                                expirationDate: theResponse.data.subscription.expirationDate ? theResponse.data.subscription.expirationDate.toDate().toJSON() : null
                            }
                        },
                        res: this.res
                    })

                    return theResponseData

                }
            }
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /organizations/code/:code',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /organizations/code/:code',
                    exception: exception,
                },
                method: {
                    name: 'validateOrganizationCode',
                    line: 76,
                },
                res,
            })
        }
    }

    // addProgramToOrganization = (req: Request, res: Response) => {
    //     this.res = res

    //     try {
    //         const { organizationID, program } = <
    //             { organizationID: string; program: Program }
    //             >req.body
    //         return this.db
    //             .collection(this.collection)
    //             .doc(organizationID)
    //             .get()
    //             .then((snapshot: firestore.DocumentSnapshot) => {
    //                 if (snapshot.exists) {
    //                     return res.json({
    //                         writeResult: snapshot.ref.update({
    //                             onboardingProgram: program,
    //                         }),
    //                     })
    //                 } else {
    //                     return this.handleResponse({
    //                         message: `Could not find Organization with id ${organizationID}`,
    //                         responseCode: 404,
    //                         data: undefined,
    //                         error: {
    //                             message: `Could not find Organization with id ${organizationID}`,
    //                         },
    //                         method: {
    //                             name: 'addProgramToOrganization',
    //                             line: 82,
    //                         },
    //                         res,
    //                     })
    //                 }
    //             })
    //             .catch(exception => {
    //                 console.log('exception!!!! => ', exception)
    //                 return this.handleResponse({
    //                     message:
    //                         'An error exception in /organizations/program: err ->' +
    //                         { ...exception },
    //                     responseCode: 500,
    //                     data: undefined,
    //                     error: {
    //                         message:
    //                             'An exception occurred in /organizations/program: err ->' +
    //                             { ...exception },
    //                         exception,
    //                     },
    //                     method: {
    //                         name: 'addProgramToOrganization',
    //                         line: 80,
    //                     },
    //                     res,
    //                 })
    //             })
    //     } catch (exception) {
    //         console.log('catchAll exception!!!! => ', exception)
    //         // return res.status(505).send(exception);
    //         return this.handleResponse({
    //             message:
    //                 'An unanticipated exception occurred in /organizations/code/:code',
    //             responseCode: 500,
    //             data: undefined,
    //             error: {
    //                 message:
    //                     'An unanticipated exception occurred in /organizations/code/:code',
    //                 exception: exception,
    //             },
    //             method: {
    //                 name: 'validateOrganizationCode',
    //                 line: 76,
    //             },
    //             res,
    //         })
    //     }
    // }

    updateOrgNonEngagementConfiguration = async (req: Request, res: Response) => {
        this.res = res

        const consumerConfig = await getConsumerModelConfig(
            // onboardingProgram
            // onboardingProgramIndicator
        )

        // TODO: Check not empty
        // if (program)

        // if (consumerConfig) {
        //     const org = {
        //         licensesConsumed: 1,
        //         licensesPurchased: 1,
        //         contactNumber: '',
        //         contactPerson: athlete.email,
        //         organizationCode,
        //         name: `${athlete.firstName}'s Organization`,
        //         uid: '',
        //         coaches: [athlete.email],
        //         creationTimestamp: firestore.Timestamp.now(),
        //         ...consumerConfig,
        //         onboardingProgram,
        //     }

        //     const newOrg: Organization = await createNewOrg(org)

        const promisses = []
        const organizations = await getOrgCollectionRef().get().then((orgsQuerySnap) => {
            return orgsQuerySnap.docs.map((queryDocSnap) => {
                return getDocumentFromSnapshot(queryDocSnap) as OrganizationInterface
            })
        })
        organizations.filter((org) => !org.nonEngagementConfiguration && org.uid).forEach(organization => {
            if (consumerConfig) {

                const org: OrganizationInterface = {
                    ...consumerConfig,
                    // TODO: Check with TB
                    // onboardingConfiguration: {
                    //     autoCreateGroups: true,
                    //     prePaidNumberOfMonths: 0,
                    //     isPrePaid: false,
                    //     subscriptionType: SubscriptionType.Free,
                    //     ...consumerConfig.onboardingConfiguration,
                    // },
                    nonEngagementConfiguration: {
                        dayCountNonEngagement: 3,
                        ...consumerConfig.nonEngagementConfiguration,
                        // TODO
                        // utcRunHour: 12
                    },
                    // onboardingProgram,
                    ...organization,
                }
                let promise
                if (org.uid) {
                    promise = getOrgDocRef(org.uid)
                        .update(org)
                        .then((writeResult: FirebaseFirestore.WriteResult) => {
                            // docRef.update({ uid: docRef.id })

                            return { ...organization, saveTime: writeResult.writeTime.toDate() }
                        })
                        .catch(exception => {
                            console.log('exception!!!! => ', { ...exception })
                            return this.handleResponse({
                                message:
                                    'An error exception in updateOrgNonEngagementConfiguration: err ->' +
                                    { ...exception },

                                responseCode: 500,
                                data: undefined,
                                error: {
                                    message:
                                        'An exception occurred in updateOrgNonEngagementConfiguration: err ->' +
                                        { ...exception },
                                    exception,
                                },
                                method: {
                                    name: 'updateOrgNonEngagementConfiguration',
                                    line: 564,
                                },
                                res: this.res,
                            })
                        })
                } else {
                    promise = Promise.resolve('UID null')
                    //  getOrgDocRef(org.uid)
                    // .update(org)
                    // .then((writeResult: FirebaseFirestore.WriteResult) => {
                    //     // docRef.update({ uid: docRef.id })

                    //     return { ...organization, saveTime: writeResult.writeTime.toDate() }
                    // })
                    // .catch(exception => {
                    //     console.log('exception!!!! => ', { ...exception })
                    //     return this.handleResponse({
                    //         message:
                    //             'An error exception in updateOrgNonEngagementConfiguration: err ->' +
                    //             { ...exception },

                    //         responseCode: 500,
                    //         data: undefined,
                    //         error: {
                    //             message:
                    //                 'An exception occurred in updateOrgNonEngagementConfiguration: err ->' +
                    //                 { ...exception },
                    //             exception,
                    //         },
                    //         method: {
                    //             name: 'updateOrgNonEngagementConfiguration',
                    //             line: 564,
                    //         },
                    //         res: this.res,
                    //     })
                    // })
                }



                promisses.push(promise)
            } else {
                return this.handleResponse({
                    message: 'No consumner Configuration Found',
                    responseCode: 404,
                    data: undefined,
                    error: {
                        message: 'No consumner Configuration Found',
                    },
                    method: {
                        name: 'updateOrgNonEngagementConfiguration',
                        line: 564,
                    },
                    res: this.res,
                })
            }
        })

        return Promise.all(promisses).then(results => {
            return this.handleResponse({
                message: 'Organizations NonEngagementConfiguration updated',
                responseCode: 200,
                data: results,
                method: {
                    name: 'updateOrgNonEngagementConfiguration',
                    line: 564,
                },
                res: this.res,
            })
        })
    }

    addOrganizations = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        console.log(req.body)
        this.res = res

        const { document: organizations } = <{ document: Array<Organization> }>(
            req.body
        )
        // const onboardingProgram = await getOnboardingProgram(this.db)

        // const { content, ...onboardingProgramIndicator } = onboardingProgram
        const consumerConfig = await getConsumerModelConfig(
            // onboardingProgram
            // onboardingProgramIndicator
        )

        // TODO: Check not empty
        // if (program)

        // if (consumerConfig) {
        //     const org = {
        //         licensesConsumed: 1,
        //         licensesPurchased: 1,
        //         contactNumber: '',
        //         contactPerson: athlete.email,
        //         organizationCode,
        //         name: `${athlete.firstName}'s Organization`,
        //         uid: '',
        //         coaches: [athlete.email],
        //         creationTimestamp: firestore.Timestamp.now(),
        //         ...consumerConfig,
        //         onboardingProgram,
        //     }

        //     const newOrg: Organization = await createNewOrg(org)

        const promisses = []

        organizations.forEach(organization => {
            if (consumerConfig) {

                const org: OrganizationInterface = {
                    ...consumerConfig,
                    // TODO: Check with TB
                    // onboardingConfiguration: {
                    //     autoCreateGroups: true,
                    //     prePaidNumberOfMonths: 0,
                    //     isPrePaid: false,
                    //     subscriptionType: SubscriptionType.Free,
                    //     ...consumerConfig.onboardingConfiguration,
                    // },
                    nonEngagementConfiguration: {
                        dayCountNonEngagement: 3,
                        ...consumerConfig.nonEngagementConfiguration,
                        // TODO
                        // utcRunHour: 12
                    },
                    // onboardingProgram,
                    ...organization,
                }
                const promise = this.db
                    .collection(this.collection)
                    .add(org)
                    .then((docRef: FirebaseFirestore.DocumentReference) => {
                        // docRef.update({ uid: docRef.id })

                        return { ...organization, uid: docRef.id }
                    })
                    .catch(exception => {
                        console.log('exception!!!! => ', { ...exception })
                        return this.handleResponse({
                            message:
                                'An error exception in addOrganizations: err ->' +
                                { ...exception },

                            responseCode: 500,
                            data: undefined,
                            error: {
                                message:
                                    'An exception occurred in addOrganizations: err ->' +
                                    { ...exception },
                                exception,
                            },
                            method: {
                                name: 'addOrganization',
                                line: 564,
                            },
                            res: this.res,
                        })
                    })

                promisses.push(promise)
            } else {
                return this.handleResponse({
                    message: 'No consumner Configuration Found',
                    responseCode: 404,
                    data: undefined,
                    error: {
                        message: 'No consumner Configuration Found',
                    },
                    method: {
                        name: 'addOrganizations',
                        line: 564,
                    },
                    res: this.res,
                })
            }
        })

        return Promise.all(promisses).then(results => {
            return this.handleResponse({
                message: 'Organizations Added',
                responseCode: 200,
                data: results,
                method: {
                    name: 'addOrganizations',
                    line: 564,
                },
                res: this.res,
            })
        })
    }

    /**
     * Adds a Organization to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof OrganizationsCtrl
     */
    addOrganization = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        console.log(req.body)
        this.res = res

        const { document: organization } = <{ document: Organization }>req.body
        // const onboardingProgram = await getOnboardingProgram(this.db)

        // const { content, ...onboardingProgramIndicator } = onboardingProgram
        const consumerConfig = await getConsumerModelConfig(
            // onboardingProgram
            // onboardingProgramIndicator
        )

        // TODO: Check not empty
        // if (program)

        // if (consumerConfig) {
        //     const org = {
        //         licensesConsumed: 1,
        //         licensesPurchased: 1,
        //         contactNumber: '',
        //         contactPerson: athlete.email,
        //         organizationCode,
        //         name: `${athlete.firstName}'s Organization`,
        //         uid: '',
        //         coaches: [athlete.email],
        //         creationTimestamp: firestore.Timestamp.now(),
        //         ...consumerConfig,
        //         onboardingProgram,
        //     }

        //     const newOrg: Organization = await createNewOrg(org)

        if (consumerConfig) {
            const org: OrganizationInterface = {
                ...consumerConfig,
                // onboardingProgram,
                nonEngagementConfiguration: {
                    dayCountNonEngagement: 3,
                    ...consumerConfig.nonEngagementConfiguration,
                    // TODO
                    // utcRunHour: 12
                },
                ...organization,
                creationTimestamp: organization.creationTimestamp
                    ? firestore.Timestamp.fromDate(new Date(organization.creationTimestamp as any))
                    : firestore.Timestamp.now()


            }

            return this.db
                .collection(this.collection)
                .add(org)
                .then(async (docRef: FirebaseFirestore.DocumentReference) => {
                    await docRef.update({ uid: docRef.id })
                    return this.handleResponse({
                        message: 'Organization Added',
                        responseCode: 200,
                        data: { ...organization, uid: docRef.id },
                        method: {
                            name: 'addOrganization',
                            line: 564,
                        },
                        res: this.res,
                    })
                })
                .catch(exception => {
                    console.log('exception!!!! => ', { ...exception })
                    return this.handleResponse({
                        message:
                            'An error exception in addOrganization: err ->' +
                            { ...exception },

                        responseCode: 500,
                        data: undefined,
                        error: {
                            message:
                                'An exception occurred in addOrganization: err ->' +
                                { ...exception },
                            exception,
                        },
                        method: {
                            name: 'addOrganization',
                            line: 564,
                        },
                        res: this.res,
                    })
                })
        } else {
            return this.handleResponse({
                message: 'No consumner Configuration Found',
                responseCode: 404,
                data: undefined,
                error: {
                    message: 'No consumner Configuration Found',
                },
                method: {
                    name: 'addOrganization',
                    line: 564,
                },
                res: this.res,
            })
        }
    }
}
