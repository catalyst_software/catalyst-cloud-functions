import { AndroidPurchaseReceipt } from '../../../../models/purchase/purchase.model'
import { SaveEventDirective } from '../../../../models/purchase/interfaces/save-event-directive'

import { Response, Request } from 'express'

import BaseCtrl from './base'
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'
import { db } from '../../../../shared/init/initialise-firebase'
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'

import { General } from './general'
import { SportsNutrition } from './sportsNutrition'
import { SportsPsychology } from './sportsPsychology'
import { SportsScience } from './sportsScience'
import { updateAthleteSubscriptionStatus } from './utils/update-subscription-object-status'
import {
    SubscriptionStatus,
    SubscriptionType,
} from '../../../../models/enums/enums.model'

import { extractPersistData } from "../callables/utils/validate-transaction-receipt/extract-persist-data";
import { persistAthleteReceipt } from "../callables/utils/validate-transaction-receipt/persist-athlete-receipt";
import { sendMessage } from "../../../../pubsub/subscriptions/utils/send-message";
import { PushNotificationType } from '../../../../models/enums/push-notification-type'
import { PlatformType } from '../../../../models/purchase/enums/platfom_type'
import { AndroidNotificationType } from '../../../../models/enums/android-notification-type'
import { PlayStoreNotification } from '../../../../models/play-store-notification/interfaces/play-store-notification'
import { getAndroidRecieptFromToken } from './getAndroidRecieptFromToken'
import { getAthleteByTransactionId } from './utils/get-athlete-by-transactionId';
import { setAnalytics } from './utils/send-notification';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot'

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class AndroidTestsCtrl
 * @extends {BaseCtrl}
 */
export default class AndroidTestsCtrl extends BaseCtrl {
    res: Response

    // logger: Logger;
    model = General
    sportsNutritionModel = SportsNutrition
    sportsPsychologyModel = SportsPsychology
    sportsScienceModel = SportsScience
    entries

    collection = FirestoreCollection.Tests
    aggregateDocumentManager: AggregateDocumentManager

    constructor() {
        super()
    }

    updateSurveys = async (req: Request, res: Response) => {
        const result = db
            .collection(FirestoreCollection.Surveys)
            .get()
            .then((querySnap: FirebaseFirestore.QuerySnapshot) => {
                querySnap.docs.map(docSnap => {
                    const survey = docSnap.data()
                    if (survey.survey) {
                        return docSnap.ref.update({ questions: survey.survey })
                    } else return undefined
                })
            })

        return result
    }
    handleNotificationEvent = async (req: Request, res: Response) => {
        // const purchaseDataExtractedFromReceipt = {

        // }

        // const notificationType4 = {
        //     version: '1.0',
        //     packageName: 'com.inspiresportonline.dev',
        //     eventTimeMillis: '1551555493865',
        //     subscriptionNotification: {
        //         version: '1.0',
        //         notificationType: 4,
        //         purchaseToken:
        //             'bjhinehaofkabfacbeiokahm.AO-J1OyUXVxZXuu_hyrP86ttSRSO9pLFvOS1srR5OnqIKaCFiZtMg-0EoJgqsUi_EEa6Y1IU9_-nmhhDPYIDMvLDPmWBbDr3WS6x6sq3dT5Yb0Gu33wEMwKQqGiZ6wCljP2mPtdEPhOTkw69iJ_HsgZqK3V-xH8I9xvRlSd1Yk-x3lw3ncdT4Sw',
        //         subscriptionId: 'com.inspiresportonline.dev.subscriptions',
        //     },
        // }

        // const notificationType2 = {
        //     version: '1.0',
        //     packageName: 'com.inspiresportonline.dev',
        //     eventTimeMillis: '1551608669236',
        //     subscriptionNotification: {
        //         version: '1.0',
        //         notificationType: 2,
        //         purchaseToken:
        //             'fngnocfmpckghidopcdbacnh.AO-J1Ows8DN3ZoS86kUaUNEt18GWQ26OcuLQinhQPy8YC_GGUWpABiBqv54Lz-qHAeYCIk_rL2m-G5MTfBDx16ZepTH_I12g04ahqgKGg9J1VLzujvJ6kv9bSDI_NSJbDXCSupW5c8mxFYO0-fiWnr0YcK4lYOOj6Yb0GezbWSBl4pAi1Ynh-FM',
        //         subscriptionId: 'com.inspiresportonline.dev.subscriptions',
        //     },
        // }
        const { docUID } = req.body

        let notification

        if (docUID) {
            notification = (getDocumentFromSnapshot(await db.collection('androidNotifications').doc(docUID).get()) as any).notification
        } else {
            notification = <PlayStoreNotification>req.body
        }

        const keys: {
            private_key: string
            client_email: string
        } = {
            private_key:
                '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCkr9ZRjXhZImyJ\nExcfXVkFqNlnvfoSdfW4bTGYWDmcOad8iJvv7T3v1W14zTCQjNcKRibMeNBoDrYW\neekUsbfYOeA834ltUE/WE1w0BucRJbojZarwe0O6+1C5QrOL43Hio8Iq74PlFLKk\n9O5rOKXUHrVHwTFjS8/lY2aPUimSZYXtDBU/OBpM+2HzBeP82LacUnaecJbtlrym\nf1nifbG6nR/UWjHgMPf+lrR0Zn2a9lIUn02Acm3G/1yA+hD/u/3bSEX+TMQtmQOv\nzgvfb66LPfUVWe8q96vsATl0VWwvMagjfqh6LoGff1OjAL1uj4i5o6nPrqzEHDfa\n0kEosWBBAgMBAAECggEAC/knww6exxY4X1/msLZT9FQiGEeI4KI4XvP7ZNjXOWs8\nqNJyyecM93ykJKIAa6X9tWbDx95pwoL9TJWI82L3W45bpflXj73EzCrkq3isAIRm\na8/mtWy00CmY5Rs7HArJeyGOSppW08clNNaE5gE8lzczVVfvrAk1QHdxW66szJKc\nlD9jUF2QjHQBURy9cvEq4oO5gAbmcYsmLumjQnxEo0i6lmK/Tc3HJoypgRYHAtPD\nS7aFQyptpHur/hBg/Xiv7BTg0Mj4DiQW8Qaci/6A07ya4IK2M357Xq64dNZEezQO\neU7wf83WVHwr4883UQGt24YojcqHoUW/jHuDRwVkRQKBgQDcB+KsmSHDc99r2jgH\naf0VAg73UABtVnBEwwipAtPcFBNrKneXwLjfmlxoob/Q+jYiQ2Z+8EXfFcpJJOFC\nzHCPlTCBeTOwxf4dU8zbxtZsfmyHkj3B30DCzJGk4Ax9MSCy+ewtw/xoR6KdOhlr\nqPCKV2TMGVCXSLeWk9hv1X1rXQKBgQC/m9oQFAH0QaW7ZJIjx7u9V+CAuugckH8X\n/o1XZY1FhcxEnxFgG4QGe4dhIEXTF2vCndq+fRBEOuiRg3yCE3OLWl9Wt9sD5fce\njXVUi3daFJXdQo0dcsqtJ1Q5tXaZPoml2pVZxt+iBenh8Z85JxH9+7tc45amc1O3\nHmGQ0qZeNQKBgQCWHuMu84OvwN0MzuQvWscLkE35uqGv96u9nnvIJF+75g6hrWXP\nKfR4yu6FjOY8hJpuoiHKNdDWNh2/7eOrGaUqsZVYoQL9dvi7tbMtt+oQN+mATezI\n27NptP0hyqN6vwwaUJ4tU2xhEY8HSt6RL8B+AsaI4jS0Iy7vE4w2MSjTGQKBgHoJ\nfLjS1W/JxBH3ezC4zPVKnB3BbYaL7bbNlR49+t1122U1Xu60d8FdOht9X5uUBjld\nKu46X3rlfiz37vw2AViXRbPIxADWni9ib4FalrjT9aOH+LLx4u6n5vgegJwX/bmZ\n35ffl53tYEpdB0lyff4jL/F4rwHy4DX4brG7yOSlAoGBAIIPm2UQZPHTxazyJKVr\n1GwQQpiKuhRdR03VwqieEDr1bQi/tVzyhEZLp+Mb8NXfJNnIGRyA77iU9OUVAiN3\nKe4ewkquXavMEIlXUli5Jtljqz/3ldXHUxaMlnciGAopnBI2XoYZOyHUMfTkuROg\nUhKwZFKHM8UJ5hMxPu4NWbO2\n-----END PRIVATE KEY-----\n',
            client_email:
                'gooleplayapi@inspire-1540046634666.iam.gserviceaccount.com',
        }

        if (!notification.packageName) {
            notification = JSON.parse(notification)
        }

        const { packageName, subscriptionNotification } = notification
        const {
            // version,
            notificationType,
            purchaseToken,
            subscriptionId,
        } = subscriptionNotification

        const receipt = {
            packageName, // com.inspiresportonline.dev
            productId: subscriptionId, // com.inspiresportonline.dev.subscriptions
            purchaseToken,
        }

        console.log('subscriptionNotification', subscriptionNotification)
        console.log('packageName', packageName)
        console.log('receipt', receipt)

        return getAndroidRecieptFromToken(keys, receipt).then(
            async (theReceipt: AndroidPurchaseReceipt) => {
                console.log('androidNotification - theReceipt', theReceipt)

                // TODO:
                // if (!theReceipt.isSuccessful) {

                // }

                const parsedReceipt = extractPersistData(theReceipt)
                // parsedReceipt.porductId = productId
                console.log(
                    'androidNotification - parcedReceipt',
                    parsedReceipt
                )
                const {
                    // kind, //: 'androidpublisher#subscriptionPurchase',
                    // startTimeMillis, // '1551616565120',
                    // expiryTimeMillis, // '1551616981829',
                    // autoRenewing, // true,
                    // priceCurrencyCode, // 'PHP',
                    // priceAmountMicros, //'74000000',
                    // countryCode, // 'PH',
                    // paymentState, // 1,
                    orderId, // 'GPA.3361-7091-1006-54015',
                    // purchaseType, //  0,
                } = parsedReceipt

                // TODO:
                // if (!receipt.isSuccessful) {
                //     return receipt.errorMessage
                // } else

                console.log('-------------------> parcedReceipt', theReceipt)
          
                console.log(
                    '-------------------> notificationType',
                    notificationType
                )
                console.log('-------------------> purchaseToken', purchaseToken)
                console.log('-------------------> orderId', orderId)
                console.log(
                    '-------------------> orderId sliced',
                    orderId.slice(0, 24)
                )

                const originalTransactionId = parsedReceipt.orderId.slice(
                    0,
                    24
                );
                const theAthlete = await getAthleteByTransactionId(
                    originalTransactionId
                )

                console.log(
                    'Notification Type',
                    AndroidNotificationType[notificationType]
                )

                if (theAthlete) {
                    console.log('theReceipt', parsedReceipt)

                    const {
                        expiryTime: expirationDate,
                        startTime: startDate
                        // isExpired,
                    } = parsedReceipt

                    const receiptPersistDetails: SaveEventDirective = {
                        platform: PlatformType.Android,
                        transactionId: parsedReceipt.orderId,
                        originalTransactionId: parsedReceipt.orderId.slice(
                            0,
                            24
                        ),
                        expirationDate,
                        startDate,
                        notificationType: AndroidNotificationType[
                            notificationType
                        ] as any,
                        androidReceipt: parsedReceipt,
                    }

                    console.log(
                        'ANDROID receipt Persist Details',
                        receiptPersistDetails
                    )

                    let actionAnalytics
                    switch (notificationType) {
                        case AndroidNotificationType.SubscriptionPurchased:
                            // A new subscription was purchased.

                            // todo: Store the latest_receipt on your server as a token to verify the user’s subscription status at any time, by validating it with the App Store.

                            await persistAthleteReceipt(
                                theAthlete.uid,
                                receiptPersistDetails
                            )

                            return updateAthleteSubscriptionStatus(
                                theAthlete,
                                parsedReceipt,
                                expirationDate,
                                undefined,
                                undefined,
                                undefined,
                                subscriptionId,
                                packageName,
                                originalTransactionId,
                                undefined,
                                undefined
                            )
                        // .then((updateResult: boolean) => {
                        //     return sendMessage(
                        //         theAthlete,
                        //         PushNotificationType.ThanksForBasicSubscription
                        //         // 'PURCHASED'
                        //     )
                        // })

                        case AndroidNotificationType.SubscriptionCanceled:
                            // A subscription was either voluntarily or involuntarily cancelled. For voluntary cancellation, sent when the user cancels.

                            // todo: Check Cancellation Date to know the date and time when the subscription was canceled.

                            const currentSubscriptionType =
                                theAthlete.profile.subscription.type

                            await persistAthleteReceipt(
                                theAthlete.uid,
                                receiptPersistDetails
                            )

                            // change type to free
                            // historicalSubscriptionType === currentSubscriptionType
                            return updateAthleteSubscriptionStatus(
                                theAthlete,
                                parsedReceipt,
                                expirationDate,
                                SubscriptionStatus.Canceled, // as is
                                SubscriptionType.Free,
                                undefined,
                                subscriptionId,
                                packageName,
                                originalTransactionId
                            ).then((updateResult: boolean) => {
                                if (
                                    currentSubscriptionType !==
                                    SubscriptionType.Free
                                ) {
                                    actionAnalytics = setAnalytics(
                                        PushNotificationType.SubscriptionCanceled,
                                        packageName
                                    )
                                    return sendMessage(
                                        theAthlete,
                                        PushNotificationType.SubscriptionCanceled,
                                        actionAnalytics
                                    )
                                } else {
                                    return Promise.resolve(true)
                                }
                            })

                        case AndroidNotificationType.SubscriptionRenewed:
                            // An active subscription was renewed.
                            // Customer renewed a subscription interactively after it lapsed, either by using your app’s interface or on the App Store in account settings. Service is made available immediately.

                            // todo: Check Subscription Expiration Date to determine the next renewal date and time.

                            await persistAthleteReceipt(
                                theAthlete.uid,
                                receiptPersistDetails
                            )

                            return updateAthleteSubscriptionStatus(
                                theAthlete,
                                parsedReceipt,
                                expirationDate,
                                SubscriptionStatus.ValidRenewed,
                                undefined,
                                undefined,
                                subscriptionId,
                                packageName,
                                originalTransactionId
                            ).then((updateResult: boolean) => {
                                actionAnalytics = setAnalytics(
                                    PushNotificationType.SubscriptionRenewed,
                                    packageName
                                )
                                return sendMessage(
                                    theAthlete,
                                    PushNotificationType.SubscriptionRenewed,
                                    actionAnalytics
                                )
                            })
                        case AndroidNotificationType.SubscriptionInGracePeriod:
                            // // A subscription has entered grace period (if enabled).
                            return sendMessage(
                                theAthlete,
                                PushNotificationType.SubscriptionLapsed
                            )
                        case AndroidNotificationType.SubscriptionExpired:
                            // A subscription has expired.
                            actionAnalytics = setAnalytics(
                                PushNotificationType.SubscriptionExpired,
                                packageName
                            )
                            return sendMessage(
                                theAthlete,
                                PushNotificationType.SubscriptionExpired,
                                actionAnalytics
                            )

                        case AndroidNotificationType.SubscriptionRecovered:
                        // A subscription was recovered from account hold.

                        case AndroidNotificationType.SubscriptionOnHold:
                        //A subscription has entered account hold (if enabled).

                        // case AndroidNotificationType.SubscriptionRestarted:
                        // // User has reactivated their subscription from Play > Account > Subscriptions (requires opt-in for subscription restoration)

                        // case AndroidNotificationType.SubscriptionPriceChangeConfirmed:
                        // // A subscription price change has successfully been confirmed by the user.

                        // case AndroidNotificationType.SubscriptionDeferred:
                        // // A subscription's recurrence time has been extended.

                        case AndroidNotificationType.SubscriptionRevoked:
                        // A subscription has been revoked from the user before the expiration time.

                        default:
                            console.info(
                                'Nitification not handled',
                                notificationType
                            )
                            return true
                    }
                } else {
                    console.log(`querySnap.empty - Can't find athlete doc`)
                    return undefined
                }
            }
        )
    }
}


