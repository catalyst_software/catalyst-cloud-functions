import { Response, Request } from 'express'
// import { firestore } from 'firebase-admin';
const globalAny: any = global;
globalAny.atob = require("atob");

import { GoogleStoreNotification } from '../../../../models/google-store-notification/google-store-notification.model';
import BaseCtrl from './base';

// const updateReceiptCollection = (athleteUid: string, validationResponse: GoogleStoreNotificationResponse): Promise<FirebaseFirestore.DocumentReference> => {
//     return getAthleteDocRef(athleteUid).collection(FirestoreCollection.PurchaseTransactions).add(validationResponse)
// };

export default class GoogleStoreNotificationsCtrl extends BaseCtrl {
    res: Response;

    // logger: Logger;
    model = GoogleStoreNotification;
    collection = undefined;

    constructor() {
        super()
    }

    handleNotificationEvent = async (req: Request, res: Response) => {

        const iosNotification = <GoogleStoreNotification>req.body;
        console.log(iosNotification);

        res.status(200).send(iosNotification)
    }
}
