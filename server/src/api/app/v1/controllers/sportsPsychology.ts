import { ProgramCatalogue } from '../../../../db/programs/models/interfaces/program-catalogue'

export const SportsPsychology: ProgramCatalogue = {
    title: 'Sports Psychology',
    categoryType: 1,
    images: [
        {
            url:
                'INSPIRE%2Fimages%2F08696912-1579-4aff-9e07-353212dd3104.png?alt=media',
        },
    ],
    // programs: [
    //     {
    //         programGuid: '2d7291d6-d62a-4bdb-84f6-36a35b893b03',
    //         name: 'Sports Psychology Basics',
    //         publisher: 'iNSPIRE',
    //         description:
    //             '4 week module consisting of Goal Setting, Self-Talk and Mindfulness. This is your foundation iNSPIRE Sports Psychology program and is a prerequisite for Sports Psychology  Basics 2. ',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2F2d7291d6-d62a-4bdb-84f6-36a35b893b03.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-01-01').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 1,
    //         subscriptionType: 1,
    //         programDays: 28,
    //         tags: ['psychology'],
    //         creationTimestamp: firestore.Timestamp.now()
    //     },
    //     {
    //         programGuid: 'a4296fbe-369a-4e7a-9b4f-cc74252d3415',
    //         name: 'Sports Psychology Basics 2',
    //         publisher: 'iNSPIRE',
    //         description: '',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2Fa4296fbe-369a-4e7a-9b4f-cc74252d3415.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-04-30').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 1,
    //         subscriptionType: 1,
    //         programDays: 28,
    //         tags: ['psychology'],
    //         creationTimestamp: firestore.Timestamp.now()
    //     },
    //     {
    //         programGuid: '0f907330-fa5f-4bc9-8202-5a02fbfbe830',
    //         name: 'Performance Anxiety',
    //         publisher: 'iNSPIRE',
    //         description:
    //             '4 week module that takes you through the steps of Beating Competition and Performance Anxiety.',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2F0f907330-fa5f-4bc9-8202-5a02fbfbe830.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-01-01').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 1,
    //         subscriptionType: 2,
    //         programDays: 28,
    //         tags: ['psychology'],
    //         creationTimestamp: firestore.Timestamp.now()
    //     },
    //     {
    //         programGuid: '9cffa2a4-cd4e-44f2-a813-0b2730511904',
    //         name: 'Motivation and Focus',
    //         publisher: 'iNSPIRE',
    //         description: '',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2F9cffa2a4-cd4e-44f2-a813-0b2730511904.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-07-31').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 1,
    //         subscriptionType: 2,
    //         programDays: 28,
    //         tags: ['psychology'],
    //         creationTimestamp: firestore.Timestamp.now()
    //     },
    //     {
    //         programGuid: 'b7921da1-a46e-465a-976d-932c0f738e65',
    //         name: 'Coping With Pressure',
    //         publisher: 'iNSPIRE',
    //         description: '',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2Fb7921da1-a46e-465a-976d-932c0f738e65.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-08-30').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 1,
    //         subscriptionType: 2,
    //         programDays: 28,
    //         tags: ['psychology'],
    //         creationTimestamp: firestore.Timestamp.now()
    //     },
    // ],
}
