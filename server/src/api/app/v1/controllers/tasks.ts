import { Response, Request } from 'express';
import { isString } from 'lodash';
import moment from 'moment';
import { firestore } from 'firebase-admin';

import BaseCtrl from './base'
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'
import { db } from '../../../../shared/init/initialise-firebase'

import { Athlete } from '../../../../models/athlete/interfaces/athlete';

import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { ReportRequest } from '../../../../models/report-model';
import { createTask } from '../../../../db/reports/createTask';
import { isProductionProject } from '../is-production';

import { generateReportRequest } from './utils/reports/generateReportRequest';
import { getBiometricEntriesDeprecated } from './utils/reports/helpers/getBiometricEntriesDeprecated';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { Organization } from '../../../../models/organization.model';
import { listTasks } from '../../../../db/reports/listTasks';
import { Program } from '../../../../db/programs/models/program.model';
import { ProgramCatalogue } from '../../../../db/programs/models/interfaces/program-catalogue';
import { getNotificationCardRef } from '../../../../db/refs';

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class ReportCtrl
 * @extends {BaseCtrl}
 */
export default class TasksCtrl extends BaseCtrl {
    res: Response;

    // logger: Logger;
    model = ReportRequest;
    entries;

    collection = FirestoreCollection.VideoConversions;
    aggregateDocumentManager: AggregateDocumentManager;

    constructor() {
        super()
    }

    private generateTaskCode() {
        const code = Math.random().toString(36).substring(6, 12);
        return Promise.resolve(code)
        // return this.db.collection(this.collection)
        //   .where('organizationCode', '==', code)
        //   .get()
        //   .then((snapshot) => {

        //     if (snapshot.empty) {
        //       return code
        //     }

        //     else {
        //       return undefined
        //     }
        //   })
    }

    /**
     * Get all Reports from the database
     *
     * @memberof TasksCtrl
     */
    getAllTasks = async (req: Request, res: Response): Promise<Response> => {
        // const { id } = <{ id: any }>req.params

        this.res = res;
        const me = this
        const taskId = req.body.taskId

        const videoConversionRequest = {
            taskId: taskId,
            creationTimestamp: new Date(),
            status: 'conversionQueued',
            message: 'Video convertion Queued',
        } as any
        console.log('taskId', taskId);

        // return db
        //     .collection(FirestoreCollection.VideoConversions)
        //     .add(videoConversionRequest)
        //     .then(() => {

        const isProd = false;
        let project = '';
        let queue = '';
        let location = '';
        let options: { payload: ReportRequest; };
        // // PRODUCTION
        if (isProductionProject() || isProd) {
            project = 'inspire-219716';
            queue = 'convert-video-queue';
            location = 'europe-west3';
            options = { payload: videoConversionRequest };
        } else {
            project = 'inspire-1540046634666';
            queue = 'convert-video-queue';
            location = 'us-central1';
            options = { payload: videoConversionRequest };
        }

        console.log('options', options);

        return listTasks(project, location, queue, options)
            .then((data) => {
                console.log('Video Conversion Tasks Listed', { taskId, data })
                return me.handleResponse({
                    message: 'Video Conversion Tasks Listed',
                    responseCode: 201,
                    data: { taskId, data: data, videoConversion: videoConversionRequest },
                    error: undefined,
                    method: {
                        name: 'getAllTasks',
                        line: 6,
                    },
                    res: this.res
                })
            })


        // })
        // .catch(err => {
        //     return res.status(502).json({
        //         message: 'Error adding Video Conversion Task',
        //         responseCode: 501,
        //         data: {
        //             report: videoConversionRequest,
        //         },
        //         error: err,
        //         method: {
        //             name: 'getReport',
        //             line: 58,
        //         },
        //     })
        // })


        // })

        // const snapshot = await listTasks();

        // console.log({snapshot})

        // return res.json(snapshot);

    };

    /**
     * Gets a Report by uid from the database
     *
     * @memberof TasksCtrl
     */
    getVideo = async (req: Request, res: Response): Promise<any> => {
        const { uid } = <{ uid: any }>req.params;
        const me = this;

        this.res = res;

        const docRef = db.collection(FirestoreCollection.Reports).doc(uid);

        return docRef
            .get()
            .then(function (doc) {
                if (doc.exists) {
                    console.log('Document data:', doc.data());
                    return me.handleResponse({
                        message: 'Report Retrieved',
                        responseCode: 201,
                        data: {
                            report: doc.data(),
                        },
                        method: {
                            name: 'getReport',
                            line: 52,
                        },
                        res: this.res
                    })
                } else {
                    console.log('No such document!');
                    return me.handleResponse({
                        message: 'Unable to retrieve Report - No such document!',
                        responseCode: 204,
                        method: {
                            name: 'getReport',
                            line: 52,
                        },
                        res: me.res
                    })
                }
            })
            .catch(function (error) {
                console.log('Error getting document:', error);
                return me.handleResponse({
                    message: 'Unable to retrieve Report - An error occured',
                    responseCode: 500,
                    method: {
                        name: 'getReport',
                        line: 52,
                    },
                    error,
                    res: this.res
                })
            })
    };

    /**
     * Adds a Report to the database
     *
     * @memberof ReportCtrl
     */
    addVideo = async (
        req: Request,
        res: Response
    ) => {

        // const { document: report } = <{
        //     document: ReportRequest
        // }>req.body;
        const videoConversionRequest = req.body;

        console.log(req.body);
        const me = this;

        this.res = res;

        return this.generateTaskCode().then((taskId) => {

            videoConversionRequest.taskId = taskId;
            videoConversionRequest.creationTimestamp = new Date();
            videoConversionRequest.status = 'conversionQueued'
            videoConversionRequest.message = 'Video convertion Queued'

            console.log('taskId', taskId);

            return db
                .collection(FirestoreCollection.VideoConversions)
                .add(videoConversionRequest)
                .then(() => {

                    const isProd = false;
                    let project = '';
                    let queue = '';
                    let location = '';
                    let options: { payload: ReportRequest; };
                    // // PRODUCTION
                    if (isProductionProject() || isProd) {
                        project = 'inspire-219716';
                        queue = 'convert-video-queue';
                        location = 'europe-west3';
                        options = { payload: videoConversionRequest };
                    } else {
                        project = 'inspire-1540046634666';
                        queue = 'convert-video-queue';
                        location = 'us-central1';
                        options = { payload: videoConversionRequest };
                    }

                    console.log('options', options);
                    return createTask(project, location, queue, options)
                        .then((data) => {
                            console.log('Video Conversion Task Created', { taskId, data })
                            return me.handleResponse({
                                message: 'Video Conversion Task Created',
                                responseCode: 201,
                                data: { taskId, data: data, videoConversion: videoConversionRequest },
                                error: undefined,
                                method: {
                                    name: 'addVideo',
                                    line: 58,
                                },
                                res: this.res
                            })
                        })


                })
                .catch(err => {
                    return res.status(502).json({
                        message: 'Error adding Video Conversion Task',
                        responseCode: 501,
                        data: {
                            report: videoConversionRequest,
                        },
                        error: err,
                        method: {
                            name: 'getReport',
                            line: 58,
                        },
                    })
                })
        })

    };


    updateProvisioning = async (
        req: Request,
        res: Response
    ) => {

        // const { document: report } = <{
        //     document: ReportRequest
        // }>req.body;
        const videoConversionRequest = req.body;

        console.log(req.body);
        const me = this;

        this.res = res;
        const theCardRef = getNotificationCardRef().doc('0719537a-91c0-bcf3-ad96-e252db482581').get().then((d) => {
            return d.data()
        })

        const progGuids: string[] = [
            'dd773724-98f6-42d9-9eac-445d1c687750',
            'dd773724-98f6-42d9-9eac-445d1c68-7750',
            'd4154476-8e1f-10ed-95e6-027342e1-2db1',
            '171bdc23-9869-4c10-342c-6362a7a8-a329',
            '3efb98a1-6571-7b68-5aa2-24fe1fd8-9ec8',
            '01778162-6ec5-3e18-2719-b7671824-290a',
            '4f596cd7-7de6-50e5-a45a-d39f4ba5-911d',
            'f1cced44-14f1-0f0f-1e4e-41ccbf8a-10ae',
            'a3519cd9-4379-1b17-307c-7b289fcd-5301',
            'b0b1dbc4-a1b9-4718-3edd-746d5a62-58fb',
            '7e1e7a80-00f4-6025-8d65-226430df-5b15',
            '86c11a75-038b-033c-0255-ed36bafc-251c',
            '5859fbc6-876b-2ef1-3dee-7db1a5ec-4f7c',
            '71fcd6e5-a505-0bfd-6d0c-2b680076-8715',
            'c9302427-99dd-8ae5-9789-212c3a15-a2e7',
            '0772433a-6af1-a5dc-3bca-561760dd-5d5a',
            '5ee9e861-2ae7-8402-9fbb-0099890b-2b78',
            '814955bd-8b45-1830-6ff8-6389fe07-8060',
            '0fc1d8fc-7f6d-3157-8f38-58680834-1b92',
            'a3d763a0-86ba-7174-81d6-01d3de0a-7ec4'

        ]


        // console.log('taskId', taskId);

        return db
            .collection(FirestoreCollection.ProgramCatalogue)
            .doc('general')
            .get()
            .then(async (docSnap) => {

                const generalCatalogue = {
                    // uid: docSnap.id,
                    ...docSnap.data(),
                } as ProgramCatalogue
                const theProgsToUpdate = generalCatalogue.programs.filter((prog) =>
                    progGuids.indexOf(prog.programGuid) >= 0)
                    const sizeof = require('sizeof'); 
                    console.log(sizeof.sizeof(generalCatalogue, true));
                // const isProd = false;
                // let project = '';
                // let queue = '';
                // let location = '';
                // let options: { payload: ReportRequest; };
                // // // PRODUCTION
                // if (isProductionProject() || isProd) {
                //     project = 'inspire-219716';
                //     queue = 'convert-video-queue';
                //     location = 'europe-west3';
                //     options = { payload: videoConversionRequest };
                // } else {
                //     project = 'inspire-1540046634666';
                //     queue = 'convert-video-queue';
                //     location = 'us-central1';
                //     options = { payload: videoConversionRequest };
                // }


                theProgsToUpdate.forEach((prog) => {

                    let { contentProvisioning } = prog
                    delete contentProvisioning.elementsyoga

                    contentProvisioning['elementsyoga-archived'] = true

                })

                const theUpdateRes = await docSnap.ref.update({
                    programs: generalCatalogue.programs
                })
                return me.handleResponse({
                    message: 'Video Conversion Task Created',
                    responseCode: 201,
                    // data: { taskId, data: data, videoConversion: videoConversionRequest },
                    error: undefined,
                    method: {
                        name: 'addVideo',
                        line: 58,
                    },
                    res: this.res
                })



            })
            .catch(err => {
                return res.status(502).json({
                    message: 'Error adding Video Conversion Task',
                    responseCode: 501,
                    data: {
                        report: videoConversionRequest,
                    },
                    error: err,
                    method: {
                        name: 'getReport',
                        line: 58,
                    },
                })
            })


    };

    deleteReport = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        console.log(req.body);

        this.res = res;

        // TODO:
        return true
        // return admin
        //     .auth()
        //     .deleteReport(id)
        //     .then(reportRecord => {
        //         // See the ReportRecord reference doc for the contents of reportRecord.
        //         console.log('...: ', reportRecord.uid);

        //         return me.handleResponse({
        //             message: 'Report added',
        //             responseCode: 201,
        //             data: {
        //                 report: reportRecord,
        //             },
        //             error: undefined,
        //             method: {
        //                 name: 'deleteReport',
        //                 line: 129,
        //             },
        //             res: this.res
        //         })

        //         // return res.status(200).json({ report: reportRecord });
        //     })
        //     .catch(function (error) {
        //         console.log('Error creating new report: ', error);
        //         return res.status(502).json({ error })
        //     })
    }

    getBiometricsDeprecated = async (
        req: Request,
        res: Response
    ) => {
        console.log(req.body);

        this.res = res;

        const { entity } = req.body


        // TODO:
        return getBiometricEntriesDeprecated({ athleteId: entity.entityId })
        // return admin
        //     .auth()
        //     .deleteReport(id)
        //     .then(reportRecord => {
        //         // See the ReportRecord reference doc for the contents of reportRecord.
        //         console.log('...: ', reportRecord.uid);

        //         return me.handleResponse({
        //             message: 'Report added',
        //             responseCode: 201,
        //             data: {
        //                 report: reportRecord,
        //             },
        //             error: undefined,
        //             method: {
        //                 name: 'deleteReport',
        //                 line: 129,
        //             },
        //             res: this.res
        //         })

        //         // return res.status(200).json({ report: reportRecord });
        //     })
        //     .catch(function (error) {
        //         console.log('Error creating new report: ', error);
        //         return res.status(502).json({ error })
        //     })
    }
}
function verifyTimestamp(at: Athlete) {

    let updateAth: boolean;
    try {
        if (typeof at.profile.subscription.expirationDate === 'string' && (at as any).profile.subscription.expirationDate !== 'UNKNOWN') {
            at.profile.subscription.expirationDate = firestore.Timestamp.fromDate(moment(at.profile.subscription.expirationDate).toDate());
            updateAth = true;
        }
        if (typeof at.profile.subscription.commencementDate === 'string' && (at as any).profile.subscription.commencementDate !== 'UNKNOWN') {
            at.profile.subscription.commencementDate = firestore.Timestamp.fromDate(moment(at.profile.subscription.commencementDate).toDate());
            updateAth = true;
        }
    }
    catch (error) {
        console.warn(error);
    }
    return updateAth;
}

