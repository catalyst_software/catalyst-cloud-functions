import moment from 'moment';

import { Response, Request } from 'express'
import * as  faker from 'faker';

import BaseCtrl from './base'

// import { CollectionHistoryDocumentManager } from '../../../../db/biometricHistory/collections/managers/history-document-manager'

import { db, theMainApp, catalystDB } from '../../../../shared/init/initialise-firebase'

import { Athlete } from './../../../../models/athlete/athlete.model';
import { Group } from './../../../../models/group/group.model';
import { AthleteBiometricEntryInterface } from '../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry';
import { createNewIpScoreTracking } from './../callables/utils/maintenance/resetIPTracking';
import { IpScoreAggregateHistoryCollection } from '../../../../db/ipscore/enums/firestore-ipscore-aggregate-history-collection';
import { IpScoreAggregateCollection } from '../../../../db/ipscore/enums/firestore-ipscore-aggregate-collection';
import { isProductionProject, isRunningLocally } from '../is-production';
import { purgeAllIPScoreAggregateDocuments, purgeAthleteAggregateDocuments } from './utils/purgeAggregateDocuments';
import { resetAthletesIpScore } from '../callables/utils/reset-athletes-ip-score';
import { getOrgCollectionRef, getAthleteCollectionRef, getOrgDocRef, getAthleteDocRef, getGroupDocRef } from '../../../../db/refs';
import { Organization } from '../../../../models/organization.model';
import { SubscriptionType, SubscriptionStatus } from '../../../../models/enums/enums.model';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { getUserFullName, getUserFirstName } from '../../../utils/getUserFullName';

import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { getOrgIdFromGroupId } from './utils/getOrgIdFromGroupId';
import { firestore } from 'firebase-admin';
import { getUserByID } from './get-user-by-id';

import { cloneAggregateDocuments } from './utils/cloneAggregateDocuments';
import { copyTheTheme } from './copyTheTheme';
import { insertRowsAsStream } from './insertBQRowsAsStream';
import { composeWeeklyAllAthleteReportData } from '../callables/composeWeeklyAllAthleteReportData';
import { isUndefined } from 'lodash';


export const insertStringAtIndex = (str, index, value) => {
    return str.substr(0, index) + value + str.substr(index);
}

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class CollectionCtrl
 * @extends {BaseCtrl}
 */
export default class CollectionsCtrl extends BaseCtrl {
    res: Response;

    // logger: Logger;
    model = undefined;

    collection = 'FirestoreCollection.Collections';

    // collectionHistoryDocumentManager: CollectionHistoryDocumentManager

    constructor() {
        super()
    }


    deleteIPScoreCollections = async (req: Request, res: Response) => {
        // const { collection } = <{ collection: string }>req.body;
        console.log(req.body);


        const { uid } = req.body
        const promises = [];

        if (!isProductionProject()) {

            if (uid) {
                return await purgeAthleteAggregateDocuments(uid).then((results) => res.status(200).json({ message: 'Collection deleted', results }))
            } else {
                return await purgeAllIPScoreAggregateDocuments().then((results) => res.status(200).json({ message: 'Collection deleted', results }))
            }


            for (const item in IpScoreAggregateCollection) {
                // if (isNaN(Number(item))) {
                //     console.log(item);
                // }

                // let {collection} = <{ a: number, b: string }>groupBy(x);
                // See the CollectionRecord reference doc for the contents of collectionRecord.
                // console.log('...: ', item)
                // See the CollectionRecord reference doc for the contents of collectionRecord.
                console.log('Collection to be deleted ', IpScoreAggregateCollection[item]);
                promises.push(deleteCollection(db, IpScoreAggregateCollection[item], 10).then((response) => {
                    return { message: 'Collection deleted', response }
                }).catch((err) => {
                    return { message: 'Unable to delete collection', error: err }
                }))
            }

            for (const item in IpScoreAggregateHistoryCollection) {
                // if (isNaN(Number(item))) {
                //     console.log(item);
                // }

                // let {collection} = <{ a: number, b: string }>groupBy(x);
                // See the CollectionRecord reference doc for the contents of collectionRecord.
                // console.log('... ', item)
                // See the CollectionRecord reference doc for the contents of collectionRecord.
                console.log('Collection to be deleted ', IpScoreAggregateHistoryCollection[item]);
                promises.push(deleteCollection(db, IpScoreAggregateHistoryCollection[item], 10).then((response) => {
                    return { message: 'Collection deleted', response }
                }))
            }
        }
        return Promise.all(promises).then((response) => {
            res.status(200).json({ message: 'Collection deleted', response })
        }).catch((err) => {
            res.status(500).json({ message: 'Unable to delete collection', error: err })
        })

    }

    resetIPScores = async (req: Request, res: Response) => {
        // const { collection } = <{ collection: string }>req.body;
        console.log(req.body);

        // if (!isProductionProject()) {

        const {
            // athleteUID,
            utcOffset, orgUID, ind } = req.body


        // const athleteIds = await getAthleteCollectionRef().where('organizationId', '==', orgUID)
        //     .get()
        //     .then((querySnap) => {
        //         if (querySnap.size) {
        //             return querySnap.docs.map((queryDocSnap) => {
        //                 return queryDocSnap.id
        //             })
        //         } else {
        //             return []
        //         }
        //     })


        let athleteIds = [
            // 'qPHMNj3ahOUWrfRENVkDXwLY8ua2',
            // 'jZ5phyKZrkOXfsM1lTuDWHieKKm1',
            'wq7BjbB30iOZoS2zhVFQg5w7g6R2',
            // 'WHi2zsAa6XZX0gxqqGtejhHYM6R2',
            // 'h0xzlJNonfdkxL7YyxHp3VeYkl83',
            // 'l3nVCohZhafPGlolNdfpuiO871J3'
        ]


        const startDate = moment('2020-07-27').toDate()
        let allRes = await Promise.all(athleteIds.map((athleteUID) => resetAthletesIpScore(athleteUID, utcOffset, startDate, true)))
     
    //  let index = ind
    //     let allRes = await resetAthletesIpScore(athleteIds[index], utcOffset, startDate)

        return res.status(200).json({ message: 'IPScore total updated', allRes })
        // }

        // return res.status(200).json({ message: 'Live Project, not run' })
    }


    resetSubscriptionDates = async (req: Request, res: Response) => {

        // const batch = db.batch();

        const subAthetes2 = await getAthleteCollectionRef()
            // .where('profile.subscription.type', '>', 0)
            // .limit(10)
            .get()
            .then((athQuerySnap) => {
                return Promise.all(athQuerySnap.docs.map(async (athQueryDoscSnap) => {

                    if (athQueryDoscSnap.exists) {

                        // const athlete = getDocumentFromSnapshot(athQueryDoscSnap) as Athlete

                        const athleteDocRef = athQueryDoscSnap.ref


                        const theAth = await athleteDocRef.get().then(docSnap => getDocumentFromSnapshot(docSnap) as Athlete)

                        let saveDoc = false
                        // if (user) {

                        if (!theAth.metadata) {
                            theAth.metadata = undefined
                        }
                        if (!theAth.metadata || !theAth.metadata.creationTimestamp) {
                            saveDoc = true;

                            const user = await getUserByID(theAth.uid)
                            if (user) {
                                if (!theAth.metadata) {
                                    theAth.metadata = {
                                        creationTimestamp: firestore.Timestamp.fromDate(moment(user.metadata.creationTime).toDate())
                                    }
                                } else {
                                    theAth.metadata.creationTimestamp = firestore.Timestamp.fromDate(moment(user.metadata.creationTime).toDate())
                                }
                            } else {

                                return { ...theAth, themsg: 'Firebase User does not exist' }
                            }


                        } if (!theAth.metadata.creationTimestamp) {
                            saveDoc = true;
                        }


                        let organizationId
                        if (!theAth.organizations) {

                            console.error(`athlete.organizations not set for ${getUserFirstName(theAth)}`, theAth.uid)
                            organizationId = theAth.organizationId


                            if (!organizationId) {
                                organizationId = theAth.groups && theAth.groups.length ? getOrgIdFromGroupId(theAth) : 'UNKNOWN'
                            }
                            if (organizationId && organizationId !== 'UNKNOWN') {

                                const theOrg = await getOrgDocRef(organizationId).get().then((docSnap) => getDocumentFromSnapshot(docSnap) as Organization)


                                if (!theOrg) {
                                    // debugger
                                } else {
                                    saveDoc = true;

                                    theAth.organizations = [{
                                        organizationId: theOrg.uid,
                                        organizationName: theOrg.name,
                                        organizationCode: theOrg.organizationCode,
                                        active: true,
                                        joinDate: theAth.metadata.creationTimestamp
                                    }]
                                }
                            }


                        }


                        if (saveDoc) {

                            const { lastSignInTimestamp, ...rest } = theAth.metadata


                            // batch.update(athleteDocRef, { ...theAth, metadata: rest });

                            try {
                                return await athleteDocRef.update({ ...theAth, metadata: rest }).then(() => theAth).catch((err) => {
                                    console.log(err)
                                })
                            } catch (error) {
                                console.warn({ ...theAth, themsg: 'Could not update document', error })

                                return { ...theAth, themsg: 'Could not update document', error }
                            }

                            // return theAth
                        } else {
                            return theAth
                        }
                        // } else {
                        //     return { ...theAth, error: 'Firebase User does not exist' }
                        // }


                    } else {
                        return undefined
                    }
                }))
            })




        // Commit the batch
        // const results = await batch.commit()
        // .then((www) => {
        //     // ...
        // });
        return res.status(200).json({ message: 'Subscribed Athletes', subAthetes: subAthetes2 })

    }

    checkThis = async () => {

        // // RESET ATHLETE sub expiry type and data //


        // const thegroupRef = getGroupDocRef('96WTqSOTgxSBMYDMBsrs-1')

        // const thegroup = await thegroupRef.get().then(docsnap => getDocumentFromSnapshot(docsnap) as Group)



        // const updatedAthletes = await Promise.all(thegroup.athletes.filter((there) => there.ipScore > 0).map(async (ath) => {
        //     ath.ipScore = 0
        //     // const dsdffd = await getAthleteDocRef(ath.uid).update(ath).then(() => ath)
        //     // .catch((err) =>
        //     //     console.log(err)
        //     // )

        //     return ath
        // }))
        // // .catch((err) =>
        // //     console.log(err)
        // // )

        // await thegroupRef.update({ athletes: updatedAthletes })







        // return res.status(200).json({ message: 'Updated Athletes', updatedAthletes })

        // RESET IP-SCORE ON GROUP ATHLETE INDICATORS //


        // const thegroupRef = getGroupDocRef('96WTqSOTgxSBMYDMBsrs-1')

        // const thegroup = await thegroupRef.get().then(docsnap => getDocumentFromSnapshot(docsnap) as Group)



        // const updatedAthletes = await Promise.all(thegroup.athletes.filter((there) => there.ipScore > 0).map(async (ath) => {
        //     ath.ipScore = 0
        //     // const dsdffd = await getAthleteDocRef(ath.uid).update(ath).then(() => ath)
        //     // .catch((err) =>
        //     //     console.log(err)
        //     // )

        //     return ath
        // }))
        // // .catch((err) =>
        // //     console.log(err)
        // // )

        // await thegroupRef.update({ athletes: updatedAthletes })


        // return res.status(200).json({ message: 'Updated Athletes', updatedAthletes })

        // return await admin.auth().updateUser('3bO37Eomi7Yco06PcqrT8hFz0ju2', {
        //     email: 'devops@inspiresportonline.com',
        //     // phoneNumber: '+11234567890',
        //     emailVerified: false,
        //     // password: 'newPassword',
        //     // displayName: 'Jane Doe',
        //     // photoURL: 'http://www.example.com/12345678/photo.png',
        //     // disabled: true
        // })
        //     .then(function (userRecord) {
        //         // See the UserRecord reference doc for the contents of userRecord.
        //         console.log('Successfully updated user', userRecord.toJSON());

        //         return userRecord.toJSON()
        //     })
        //     .catch(function (error) {
        //         console.log('Error updating user:', error);
        //     });

        // const allAthletes = await getAthleteCollectionRef()
        //     // .where('profile.subscription.type', '>', 0)
        //     // .limit(50)
        //     .get().then((athQuerySnap) => {
        //         return Promise.all(athQuerySnap.docs.map(async (athQueryDoscSnap) => {

        //             // if (athQueryDoscSnap.exists) {
        //             const athlete = getDocumentFromSnapshot(athQueryDoscSnap) as Athlete

        //             const { subscription } = athlete.profile

        //             let commencementDate
        //             try {
        //                 commencementDate = subscription && subscription.commencementDate ? subscription.commencementDate.toDate() : undefined
        //             } catch (error) {
        //                 commencementDate = undefined
        //             }

        //             let expirationDate
        //             try {
        //                 expirationDate = subscription && subscription.expirationDate ? subscription.expirationDate.toDate() : undefined
        //             } catch (error) {
        //                 expirationDate = undefined
        //             }


        //             return {
        //                 uid: athlete.uid,
        //                 organizationUID: (<any>athlete).organizationId,
        //                 organizationName: undefined,
        //                 firstName: getUserFirstName(athlete),
        //                 lastName: getUserLastName(athlete),
        //                 fullName: getUserFullName(athlete),
        //                 email: getUserEmail(athlete),
        //                 isCoach: athlete.isCoach,
        //                 currentPrograms: athlete.currentPrograms,
        //                 isPrePaid: subscription && subscription.isPrePaid !== undefined ? subscription.isPrePaid : undefined,
        //                 subscriptionStatus: subscription && subscription.status !== undefined ? subscription.status : undefined,
        //                 subscriptionCommencementDate: commencementDate,
        //                 subscriptionExpiry: expirationDate
        //             }

        //             // }

        //         }))
        //     })


        // const athletesSansPrograms = allAthletes.filter((ath) => !ath.currentPrograms)


        // const fixme = allAthletes.filter((ath) => ath.subscriptionExpiry !== undefined)


        // if (fixme.length) {
        //     const onboardingProgram = await getOnboardingProgram()

        //     fixme.map(async (ath) => {

        //         const theAthleteRef = getAthleteDocRef(ath.uid)

        //         const theAthleteDoc = await theAthleteRef.get().then((docSnap) => getDocumentFromSnapshot(docSnap) as Athlete)
        //         // ath.currentPrograms = [onboardingProgram]
        //         const { organizationUID, firstName, subscriptionExpiry, lastName, fullName, email, isPrePaid, subscriptionStatus, subscriptionCommencementDate, ...theAthlete } = theAthleteDoc as any


        //         await theAthleteRef.set(theAthlete).catch((err) => {
        //             console.log('ERROR', err)
        //         })
        //     })

        // }

        // if (athletesSansPrograms.length) {
        //     const onboardingProgram = await getOnboardingProgram()

        //     athletesSansPrograms.map(async (ath) => {
        //         ath.currentPrograms = [onboardingProgram]

        //         await getAthleteDocRef(ath.uid).update({
        //             currentPrograms: [onboardingProgram]
        //         }).catch((err) => {
        //             console.log('ERROR', err)
        //         })
        //     })

        // }

        // return res.status(200).json({ message: 'Athletes Without currentPrograms', athetes: athletesSansPrograms })

        // Get a new write batch


    }


    getAllAthletes = async (req: Request, res: Response) => {
        // const { collection } = <{ collection: string }>req.body;
        console.log(req.body);

        const { limit, athletesWithErrors, usersWithoutDocs } = req.body
        const orgData: Array<{ orgID: string, name: string }> = [

        ]

        if (isRunningLocally) {

            const { payloadData, allUsers } = await composeWeeklyAllAthleteReportData(limit, orgData)

            // console.log(JSON.stringify(payloadData))
            // const dfddf = updatedResponse
            // usersWithoutDocs

            console.log('payloadData.theAthletes length', payloadData.theAthletes.length)

            await insertRowsAsStream(payloadData.theAthletes as any)

            return res.status(200).json({
                message: 'Subscribed Athletes',
                athetes: payloadData.theAthletes,
                athletesWithErrors: athletesWithErrors ? payloadData.athletesWithErrors : undefined,
                // issues: payloadIssueData, 
                usersWithoutDocs: usersWithoutDocs ? allUsers : undefined
            })
            // await admin.auth().getUser('OlBhSYpO0Bgw3Npd4Kn7TQDgW5R2')
            //     .then(function (userRecord) {
            //         // See the UserRecord reference doc for the contents of userRecord.
            //         console.log('Successfully updated user', userRecord.toJSON());

            //         return userRecord.toJSON()
            //     })
            //     .catch(function (error) {
            //         console.log('Error updating user:', error);
            //     });


            // await admin.auth().updateUser('OlBhSYpO0Bgw3Npd4Kn7TQDgW5R2', {
            //     // email: 'ian.inspire@gmx.com',
            //     // phoneNumber: '+11234567890',
            //     emailVerified: false,
            //     // password: 'newPassword',
            //     // displayName: 'Jane Doe',
            //     // photoURL: 'http://www.example.com/12345678/photo.png',
            //     // disabled: true
            // })
            //     .then(function (userRecord) {
            //         // See the UserRecord reference doc for the contents of userRecord.
            //         console.log('Successfully updated user', userRecord.toJSON());

            //         return userRecord.toJSON()
            //     })
            //     .catch(function (error) {
            //         console.log('Error updating user:', error);
            //     });






            // return await admin.auth().updateUser('ebQnLkCCvAM7XmKxpwQuWq9fjpU2', {
            //     // email: 'ian.inspire@gmx.com',
            //     // phoneNumber: '+11234567890',
            //     emailVerified: false,
            //     // password: 'newPassword',
            //     // displayName: 'Jane Doe',
            //     // photoURL: 'http://www.example.com/12345678/photo.png',
            //     // disabled: true
            // })
            //     .then(function (userRecord) {
            //         // See the UserRecord reference doc for the contents of userRecord.
            //         console.log('Successfully updated user', userRecord.toJSON());

            //         return userRecord.toJSON()
            //     })
            //     .catch(function (error) {
            //         console.log('Error updating user:', error);
            //     });

            // const orgUIDs = [
            //     '1WeXDuIn2iPlRm59q5T9',
            //     'rPSzxsM4vAZ2aNgD15qG',
            //     'nk5wL1g5iklmfxCs3VZT'
            // ]
            const orgUIDs = [
                '96WTqSOTgxSBMYDMBsrs' //tristar
            ]
            // const athleteUIDs = [
            //     'c40uPWjDnEgKD8VAoT6T67taVWE3',
            //     'jtrn52OB89SAhvmuJXbuC3dHM0J2',
            //     'TZwjm4JEerOTgnhy6Jwldte5JK42',
            //     'ziIoE5xfRbNqo7f45D2MKAgGQTs2',
            //     '85CWYEBwZ9WgUqXssznOpuuGPJ72',
            //     'QXowPzdYL7dZeZcKSgcGLKnMQKM2',
            //     'WWmPCgnpqJYKzvf3bO9RPPRuk2q2',
            //     'nFajoTD4dITTJhrmzqEWMTmp2i43',
            //     'E0RxmYdfXobQVQTit4SSwXaGZ892',
            //     'txxFZxxN7mTeJFV2KnIoSJ1t9B43',
            //     'qqNycDg5fOOtfSCrthTaUA4NR0R2',
            //     'ZfghmBYw0CNWZkH7w7lrGLoSl0L2',
            //     'v9I0Fm03lBfftcq9e9Q4t6np1AA2',
            //     'mFoQK53hnrbtzZdTWgYWXDO06iH3',
            //     '2aadoSSQ4xXQEy9i9wyKNbk1rlT2',
            //     'oNOcZ3cPUEU7nNl7WEijlTNw0ow2',
            //     'PICBzwQvIyNIp6BanF373ElKlkm2'
            // ]


            // const rrrrrr = await athleteUIDs.map(async (athUID) => {
            //     // const ref = getOrgCollectionRef().doc(orgUID);
            //     // groupDocsRefs.push(ref);

            //     const athleteDocSnap = await getAthleteCollectionRef().doc(athUID)

            //     const theResults = await athleteDocSnap.update({
            //         'profile.subscription': {
            //             status: SubscriptionStatus.ValidInitial, // 1
            //             type: SubscriptionType.Basic, // 1
            //             commencementDate: new Date(),
            //             expirationDate: moment().add(365, 'days').toDate(),
            //             isPrePaid: true
            //         }
            //     })

            //     return theResults

            // })
            // return res.status(200).json({ message: 'IPScore total updated', results: await Promise.all(rrrrrr) })

            const groupDocsRefs = new Array<FirebaseFirestore.DocumentReference>();

            const allResults = []
            orgUIDs.forEach(async (orgUID) => {
                const ref = getOrgCollectionRef().doc(orgUID);
                groupDocsRefs.push(ref);

                const athletesQuerySnap = await getAthleteCollectionRef().where('organizationId', '==', orgUID).get().then((queryDocsnaps) => queryDocsnaps)

                const theResults = await athletesQuerySnap.docs.map(async (queryDocSnap) => {
                    return await queryDocSnap.ref.update({
                        'profile.subscription': {
                            status: SubscriptionStatus.ValidInitial, // 1
                            type: SubscriptionType.Basic, // 1
                            commencementDate: new Date(),
                            expirationDate: moment().add(1, 'month').toDate(),
                            // expirationDate: moment().add(100, 'mon').toDate(),
                            isPrePaid: true
                        }
                    })

                })

                allResults.push(theResults)

            })




            return res.status(200).json({ message: 'IPScore total updated', allResults })
            if (!isProductionProject()) {

                const { athleteUID, utcOffset } = req.body

                const results = await resetAthletesIpScore(athleteUID, utcOffset)
                return res.status(200).json({ message: 'IPScore total updated', results })
            }

            return res.status(200).json({ message: 'Live Project, not run' })
        } else {

            return res.status(200).json({ message: 'Online Project, not run' })
        }
    }

    cloneOrg = async (req: Request, res: Response) => {
        // const { collection } = <{ collection: string }>req.body;
        console.log(req.body);

        const { orgID } = req.body

        const liveAthletes = await catalystDB.collection(FirestoreCollection.Athletes)
            .where('organizationId', '==', orgID)
            .get().then((querySnap => {
                return querySnap.docs.map(docSnap => getDocumentFromSnapshot(docSnap) as Athlete)
            }))




        const newusers = await Promise.all(liveAthletes.map(async (athlete) => {



            const newAthlete = await getAthleteDocRef(athlete.uid).set(athlete).then(() => athlete)

            return newAthlete

        }))


        return res.status(200).json({
            message: 'Org Cloned',
            //  org: newOrg, groups: newGroups, 
            athletes: newusers,
        })

        const allAthletes = await getAthleteCollectionRef()
            // .where('profile.subscription.type', '>', 0)
            // .limit(50)
            .get().then((athQuerySnap) => {
                return Promise.all(athQuerySnap.docs.map(async (athQueryDoscSnap) => {

                    // if (athQueryDoscSnap.exists) {
                    const athlete = getDocumentFromSnapshot(athQueryDoscSnap) as Athlete

                    const { subscription } = athlete.profile

                    let commencementDate
                    try {
                        commencementDate = subscription && subscription.commencementDate ? subscription.commencementDate.toDate() : undefined
                    } catch (error) {
                        commencementDate = undefined
                    }

                    let expirationDate
                    try {
                        expirationDate = subscription && subscription.expirationDate ? subscription.expirationDate.toDate() : undefined
                    } catch (error) {
                        expirationDate = undefined
                    }


                    return {
                        uid: athlete.uid,
                        organizationUID: (<any>athlete).organizationId,
                        // organizationName: undefined,
                        // firstName: getUserFirstName(athlete),
                        firstName: (<any>athlete).firstName,
                        subscriptionExpiry: (<any>athlete).subscriptionExpiry,
                        // lastName: getUserLastName(athlete),
                        // fullName: getUserFullName(athlete),
                        // email: getUserEmail(athlete),
                        // isCoach: athlete.isCoach,
                        currentPrograms: athlete.currentPrograms,
                        // isPrePaid: subscription && subscription.isPrePaid !== undefined ? subscription.isPrePaid : undefined,
                        // subscriptionStatus: subscription && subscription.status !== undefined ? subscription.status : undefined,
                        // subscriptionCommencementDate: commencementDate,
                        // subscriptionExpiry: expirationDate
                    }

                    // }

                }))
            })


        const fixme = allAthletes.filter((ath) => ath.subscriptionExpiry !== undefined)


        if (fixme.length) {

            fixme.map(async (ath) => {

                const theAthleteRef = getAthleteDocRef(ath.uid)

                const theAthleteDoc = await theAthleteRef.get().then((docSnap) => getDocumentFromSnapshot(docSnap) as Athlete)
                // ath.currentPrograms = [onboardingProgram]
                const { organizationUID, firstName, subscriptionExpiry, lastName, fullName, email, isPrePaid, subscriptionStatus, subscriptionCommencementDate, ...theAthlete } = theAthleteDoc as any


                await theAthleteRef.set(theAthlete).catch((err) => {
                    console.log('ERROR', err)
                })
            })

        }
        return res.status(200).json({ message: 'Athletes Without currentPrograms' })

    }


    cloneOrgs = async (req: Request, res: Response) => {
        // const { collection } = <{ collection: string }>req.body;
        console.log(req.body);

        const { orgIDs, setFonyEmail, cloneAggregateDocs, cloneUsers = false }: {
            orgIDs: Array<string>, setFonyEmail?: boolean,
            cloneAggregateDocs?: boolean, cloneUsers: boolean
        } = req.body
        const avatar = faker.internet.avatar()

        if (orgIDs && orgIDs.length) {

            // Travis Coach
            // let travIndicator = {
            //     uid: '4RQHC5ct5OdPrrvqjDd7AEGjb5k2',
            //     firstName: 'Travis Coach',
            //     lastName: 'Brannon',
            //     email: 'travisbrannon71@gmail.com',
            //     runningTotalIpScore: undefined,
            //     subscriptionType: undefined,
            //     creationTimestamp: undefined
            // }

            // const travRef = getAthleteDocRef(travIndicator.uid)

            // const trav = await travRef.get().then((docSnap => getDocumentFromSnapshot(docSnap) as Athlete))

            // travIndicator = {
            //     ...travIndicator,
            //     runningTotalIpScore: trav.runningTotalIpScore || 0,
            //     subscriptionType: trav.profile.subscription.type,
            //     creationTimestamp: trav.metadata.creationTimestamp

            // }

            // Ian Coach
            // let ianIndicator = {
            //     uid: 'oH3a3vSErSQcSXRM0JMhwvHTLEC2',
            //     firstName: 'Ian Coach',
            //     lastName: 'Gouws',
            //     email: 'ian@inspiresportonline.com',
            //     runningTotalIpScore: undefined,
            //     subscriptionType: undefined,
            //     creationTimestamp: undefined,
            //     bio: undefined,
            //     orgTools: undefined
            // }

            // const ianRef = getAthleteDocRef(ianIndicator.uid)

            // const ian = await ianRef.get().then((docSnap => getDocumentFromSnapshot(docSnap) as Athlete))
            // ianIndicator = {
            //     ...ianIndicator,
            //     runningTotalIpScore: ian.runningTotalIpScore || 0,
            //     subscriptionType: ian.profile.subscription.type,
            //     creationTimestamp: ian.metadata.creationTimestamp

            // }

            // const travGroups = trav.groups
            // const ianGroups = ian.groups

            const entities = await Promise.all(orgIDs.map(async (orgID) => {

                const liveOrg = await catalystDB.collection(FirestoreCollection.Organizations).doc(orgID).get().then((docSnap => getDocumentFromSnapshot(docSnap) as Organization))
                    .catch((err) => {
                        console.warn(err)
                    })
                if (liveOrg) {
                    if (isUndefined(setFonyEmail) || setFonyEmail)
                        liveOrg.coaches = liveOrg.coaches.map(coachEMail => insertStringAtIndex(coachEMail, coachEMail.indexOf('@'), '_fony_'))
                    // liveOrg.coaches.push(travIndicator.email)
                    // liveOrg.coaches.push(ianIndicator.email)
                    // Create Dev Org 
                    const newOrg = await getOrgDocRef(orgID).set(liveOrg).then(() => liveOrg)

                    const themeUID = newOrg.theme || 'INSPIRE'
                    if (themeUID.toUpperCase() !== 'INSPIRE') {
                        const themeCopyResult = await copyTheTheme(themeUID)

                        if (themeCopyResult.data) {
                            console.log('Theme copied', themeUID);
                        }
                    }

                    const liveGroups = await catalystDB.collection(FirestoreCollection.Groups)
                        .where('organizationId', '==', orgID)
                        .get().then((querySnap => {
                            if (!querySnap.empty) {
                                return querySnap.docs.map(docSnap => getDocumentFromSnapshot(docSnap) as Group)
                            } else {
                                return []
                            }
                        })).catch((err) => {
                            console.warn(err)
                        })

                    let newGroups: void | Group[] = []
                    // Create Dev Groups 
                    if (liveGroups) {

                        newGroups = await Promise.all(liveGroups.map((group) => {


                            // if (!travGroups.find(grp => grp.groupId === groupIndicator.groupId)) {
                            //     travGroups.push(groupIndicator)
                            // }
                            // if (!ianGroups.find(grp => grp.groupId === groupIndicator.groupId)) {
                            //     ianGroups.push(groupIndicator)
                            // }
                            if (isUndefined(setFonyEmail) || setFonyEmail)
                                try {
                                    group.athletes = group.athletes.map(groupAthlete => {
                                        return {
                                            ...groupAthlete,
                                            email: insertStringAtIndex(groupAthlete.email, groupAthlete.email.indexOf('@'), '_fony_')
                                        }
                                    });
                                    // group.athletes.push(travIndicator)
                                    // group.athletes.push(ianIndicator)

                                } catch (error) {
                                    console.warn(error)
                                }

                            return getGroupDocRef(group.groupId).set(group).then(() => group)

                        })).catch((err) => {
                            console.warn(err)
                        })
                    }


                    let newusers: void | Athlete[] = []
                    const cloneResults = []

                    if (cloneUsers) {

                        const liveAthletes = await catalystDB.collection(FirestoreCollection.Athletes)
                            .where('organizationId', '==', orgID)
                            .get().then((querySnap => {
                                if (!querySnap.empty) {
                                    const theAthletes = querySnap.docs.map(docSnap => getDocumentFromSnapshot(docSnap) as Athlete)

                                    return theAthletes
                                } else {
                                    return []
                                }

                            })).catch((err) => {
                                console.warn(err)
                            })

                        // Create Dev Athletes and Users 
                        if (liveAthletes) {

                            newusers = await Promise.all(liveAthletes.map(async (athlete) => {

                                const mail = athlete.profile.email

                                console.log('TTHHHEEE EEMMMAAAIIILLL', mail)

                                let fonyEmail
                                if (isUndefined(setFonyEmail) || setFonyEmail)
                                    fonyEmail = insertStringAtIndex(mail, mail.indexOf('@'), '_fony_');
                                else
                                    fonyEmail = mail

                                let newUser = await getUserByID(athlete.uid).catch((err) => {
                                    if (err.code === 'auth/user-not-found') {
                                        return undefined
                                    } else {
                                        return err
                                    }
                                })

                                if (!newUser) {

                                    const userPayloed = {
                                        uid: athlete.uid,
                                        email: fonyEmail,
                                        emailVerified: true,
                                        // phoneNumber: '+11234567890',
                                        password: 'H3lloW0rld',
                                        displayName: getUserFullName(athlete),
                                        photoURL: avatar,
                                        disabled: false
                                    }
                                    newUser = await theMainApp.auth().createUser(userPayloed)
                                        .then((userRecord) => {
                                            // See the UserRecord reference doc for the contents of userRecord.
                                            console.log('Successfully created NEW user:', userRecord.uid);
                                            return userRecord
                                        })
                                        .catch((error) => {
                                            if (error.code === 'auth/email-already-exists') {
                                                return true
                                            } else {
                                                console.error('Error creating NEW user:', error);
                                                console.log('user:', userPayloed);
                                                return false
                                                // throw error

                                            }

                                        })
                                } else {
                                    if (newUser.email !== fonyEmail) {

                                        const updateUserPayloed = {
                                            email: fonyEmail,
                                            emailVerified: true,
                                            // phoneNumber: '+11234567890',
                                            password: 'H3lloW0rld',
                                            displayName: getUserFullName(athlete),
                                            photoURL: avatar,
                                            disabled: false
                                        }
                                        newUser = await theMainApp.auth().updateUser(athlete.uid, updateUserPayloed)
                                            .then(function (userRecord) {
                                                // See the UserRecord reference doc for the contents of userRecord.
                                                console.log('Successfully updated EXISTING user:', userRecord.uid);
                                                return userRecord
                                            })
                                            .catch(function (error) {
                                                console.log('Error updating EXISTING user:', error);

                                                console.log('user:', updateUserPayloed);
                                                return undefined
                                                // throw error
                                            })
                                    }
                                }

                                athlete.profile.email = fonyEmail

                                const resetIPScoreTracking = true;

                                if (resetIPScoreTracking) {
                                    await getAthleteDocRef(athlete.uid).collection('ipScoreTrackingBackup').add({ ...athlete.currentIpScoreTracking, creationTimestamp: new Date() }).then(() => true)
                                    athlete.currentIpScoreTracking = createNewIpScoreTracking()

                                }

                                const newAthlete = await getAthleteDocRef(athlete.uid).set(athlete).then(() => athlete)

                                if (cloneAggregateDocs) {
                                    const biometricEntries = await catalystDB.collection(FirestoreCollection.Athletes).doc(athlete.uid).collection(FirestoreCollection.AthleteBiometricEntries).get().then((querySnap) => {
                                        return querySnap.docs.map((queryDocSnap) => {
                                            return getDocumentFromSnapshot(queryDocSnap) as AthleteBiometricEntryInterface
                                        })
                                    })
                                    const biometricAddResult = await Promise.all(biometricEntries.map(async (biometricEntry) => {
                                        return await getAthleteDocRef(athlete.uid).collection(FirestoreCollection.AthleteBiometricEntries).doc(biometricEntry.uid).set(biometricEntry)
                                    }))
                                    cloneResults.push(biometricAddResult)

                                    const athleteAggregateDocsCloneResult = await cloneAggregateDocuments(athlete.uid)
                                    cloneResults.push(athleteAggregateDocsCloneResult)

                                    const groupAggregateDocsCloneResult = await Promise.all(athlete.groups.map(async (group) => {

                                        return await cloneAggregateDocuments(group.groupId || group.uid)
                                    }))
                                    cloneResults.push(groupAggregateDocsCloneResult)
                                }
                                return newAthlete

                            }))

                        } else {
                            return {
                                status: 502,
                                message: 'Org Has No Athletes',
                                entities
                            }
                        }
                    }

                    return { status: 200, newOrg, newGroups, newusers, cloneResults }
                } else {
                    return {
                        status: 501,
                        message: 'Org Doc Does Not Exist',
                        //  org: newOrg, groups: newGroups, 
                        entities
                    }
                }

            }))

            // await travRef.update(trav)
            // await ianRef.update(ian)


            return res.status(200).json({
                message: 'Org Cloned',
                //  org: newOrg, groups: newGroups, 
                entities
            })
        } else {
            return res.status(500).json({
                message: 'Org ID\'s not specified',
                //  org: newOrg, groups: newGroups, 
                // athletes: newusers,
            })
        }







        const allAthletes = await getAthleteCollectionRef()
            // .where('profile.subscription.type', '>', 0)
            // .limit(50)
            .get().then((athQuerySnap) => {
                return Promise.all(athQuerySnap.docs.map(async (athQueryDoscSnap) => {

                    // if (athQueryDoscSnap.exists) {
                    const athlete = getDocumentFromSnapshot(athQueryDoscSnap) as Athlete

                    const { subscription } = athlete.profile

                    let commencementDate
                    try {
                        commencementDate = subscription && subscription.commencementDate ? subscription.commencementDate.toDate() : undefined
                    } catch (error) {
                        commencementDate = undefined
                    }

                    let expirationDate
                    try {
                        expirationDate = subscription && subscription.expirationDate ? subscription.expirationDate.toDate() : undefined
                    } catch (error) {
                        expirationDate = undefined
                    }


                    return {
                        uid: athlete.uid,
                        organizationUID: (<any>athlete).organizationId,
                        // organizationName: undefined,
                        // firstName: getUserFirstName(athlete),
                        firstName: (<any>athlete).firstName,
                        subscriptionExpiry: (<any>athlete).subscriptionExpiry,
                        // lastName: getUserLastName(athlete),
                        // fullName: getUserFullName(athlete),
                        // email: getUserEmail(athlete),
                        // isCoach: athlete.isCoach,
                        currentPrograms: athlete.currentPrograms,
                        // isPrePaid: subscription && subscription.isPrePaid !== undefined ? subscription.isPrePaid : undefined,
                        // subscriptionStatus: subscription && subscription.status !== undefined ? subscription.status : undefined,
                        // subscriptionCommencementDate: commencementDate,
                        // subscriptionExpiry: expirationDate
                    }

                    // }

                }))
            })



        const fixme = allAthletes.filter((ath) => ath.subscriptionExpiry !== undefined)


        if (fixme.length) {

            fixme.map(async (ath) => {

                const theAthleteRef = getAthleteDocRef(ath.uid)

                const theAthleteDoc = await theAthleteRef.get().then((docSnap) => getDocumentFromSnapshot(docSnap) as Athlete)
                // ath.currentPrograms = [onboardingProgram]
                const { organizationUID, firstName, subscriptionExpiry, lastName, fullName, email, isPrePaid, subscriptionStatus, subscriptionCommencementDate, ...theAthlete } = theAthleteDoc as any


                await theAthleteRef.set(theAthlete).catch((err) => {
                    console.log('ERROR', err)
                })
            })

        }

        return res.status(200).json({ message: 'Athletes Without currentPrograms' })

    }
}

function deleteCollection(theDbdb, collectionPath, batchSize) {
    const collectionRef = theDbdb.collection(collectionPath);
    const query = collectionRef.orderBy('__name__').limit(batchSize);

    return new Promise((resolve, reject) => {
        return deleteQueryBatch(theDbdb, query, batchSize, resolve, reject);
    });
}

function deleteQueryBatch(theDb, query, batchSize, resolve, reject) {
    return query.get()
        .then((snapshot) => {
            // When there are no documents left, we are done
            if (snapshot.size === 0) {
                return 0;
            }

            // Delete documents in a batch
            const batch = theDb.batch();
            snapshot.docs.forEach((doc) => {
                batch.delete(doc.ref);
            });

            return batch.commit().then(() => {
                return snapshot.size;
            });
        }).then((numDeleted) => {
            if (numDeleted === 0) {
                resolve();
                return numDeleted;
            }

            // Recurse on the next process tick, to avoid
            // exploding the stack.
            process.nextTick(() => {
                return deleteQueryBatch(theDb, query, batchSize, resolve, reject);
            });
        })
        .catch(reject);
}
