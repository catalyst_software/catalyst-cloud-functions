import { db } from "../../../../../shared/init/initialise-firebase";
import { FirestoreCollection } from "../../../../../db/biometricEntries/enums/firestore-collections";
import moment from "moment";

export type MSSubscription = {
  userId: any;
  subscriptionId: any;
  accessToken: any;
  resource: any;
  clientState: any;
  changeType: any;
  notificationUrl: any;
  subscriptionExpirationDateTime: any;
};

export const getSubscription = (subscriptionId) => {


  const subscriptionExpirationDateTime =  moment().toDate()
  console.warn('querying', {
    subscriptionId,
    subscriptionExpirationDateTime
  })
  return db
    .collection(FirestoreCollection.MSCalendarEvents)
    .where('subscriptionId', '==', subscriptionId)
    .where('subscriptionExpirationDateTime', '>', moment().toDate())
    .get().then((docQuerySnap) => {

      const subData = docQuerySnap.docs.map((docSnap) => {
        return {
          uid: docSnap.id,
          ...docSnap.data() as MSSubscription
        }
      })

      if (subData && subData.length) {
        return subData[0]
      } else {
        return undefined
      }

    });

}

export const saveSubscription = (subscriptionData, callback) => {

  const data = {
    userId: subscriptionData.userId,
    subscriptionId: subscriptionData.id,
    accessToken: subscriptionData.accessToken,
    resource: subscriptionData.resource,
    clientState: subscriptionData.clientState,
    changeType: subscriptionData.changeType,
    notificationUrl: subscriptionData.notificationUrl,
    subscriptionExpirationDateTime: subscriptionData.expirationDateTime
  }

  return db
    .collection(FirestoreCollection.MSCalendarEvents).doc()
    .set(data)


}

export const deleteSubscription = (subscriptionId, callback) => {


  return db
    .collection(FirestoreCollection.MSCalendarEvents)
    .where('subscriptionId', '==', subscriptionId)

    .get().then((docRef) => {
      return docRef.docs.forEach((docSnap) => {
        return docSnap.ref.delete()
      })
    });

}
