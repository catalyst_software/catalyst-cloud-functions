import { AthleteBiometricEntryModel } from "../../../../../db/biometricEntries/well/athelete-biometric-entry.model";

export interface DoReadPayload {
  options: {
    logLevel: string;
    apiHandlesDoRead: boolean;
    handleDoWriteEntry: boolean;
    count: number;
  };
  entry: AthleteBiometricEntryModel;
}
