import { Response, Request } from 'express'
import rp from 'request-promise'
import BaseCtrl from './base';

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class SendGridCtrl
 * @extends {BaseCtrl}
 */
export default class SendGridCtrl extends BaseCtrl {
    res: Response

    // logger: Logger;
    model = undefined // SendGrid

    collection = undefined //FirestoreCollection.SendGrid

    // sendGridHistoryDocumentManager: SendGridHistoryDocumentManager

    constructor() {
        super()
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof SendGridCtrl
     */

    sendEmail = async (req: Request, res: Response) => {
        // const { sendGrid } = <{ sendGrid: SendGrid }>req.body
        console.log(req.body)

        const options = {
            method: 'POST',
            uri: 'https://us-central1-inspire-1540046634666.cloudfunctions.net/sendgridEmail?sg_key=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',
            body: {
                "to": "ian.gouws@kaapisoft.com",
                "from": "athleteservices@inspiresportonline.com",
                "subject": "Hello from Sendgrid!",
                "body": "<h1>Hello</h1> <p>World!</p>"
            },
            json: true // Automatically stringifies the body to JSON
        };

        const options2 = {
            method: 'POST',
            uri: 'https://us-central1-inspire-1540046634666.cloudfunctions.net/sendgridEmail?sg_key=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',
            body: {
                "to": "ian.gouws@kaapisoft.com",
                "from": "athleteservices@inspiresportonline.com",
                "subject": "Hello from Sendgrid!",
                "body": { message: 'Hi There', content: "<h1>Hello</h1> <p>World!</p>" }
            },
            json: true // Automatically stringifies the body to JSON
        };

        rp(options2)
            .then((parsedBody) => {
                // POST succeeded...
                console.log('parsedBody', parsedBody)
                return res.status(200).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {
                        parsedBody,
                    },
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            })
            .catch((err) => {
                // POST failed...
                console.log('err', err)
                return res.status(500).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {},
                    error: err,
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            });
        return await rp(options)
            .then((parsedBody) => {
                // POST succeeded...
                console.log('parsedBody', parsedBody)
                return res.status(200).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {
                        parsedBody,
                    },
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            })
            .catch((err) => {
                // POST failed...
                console.log('err', err)
                return res.status(500).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {},
                    error: err,
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            });


        // const config = {
        //     apiKey: process.env.apiKey,
        //     authDomain: process.env.authDomain,
        //     databaseURL: process.env.databaseURL,
        //     projectId: process.env.projectId,
        //     storageBucket: process.env.storageBucket,
        //     messagingSenderId: process.env.messagingSenderId
        // }

        // const sendgridKey = functions.config()

        // console.log(sendgridKey)

        // const testKey = process.env.SENDGRID_API_KEY
        // sgMail.setApiKey('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');
        // const msg = {
        //     to: 'ian.gouws@kaapisoft.com',
        //     from: 'athleteservices@inspiresportonline.com',
        //     subject: 'Sending with SendGrid is Fun',
        //     text: 'and easy to do anywhere, even with Node.js',
        //     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
        // };
        // const tttt = await sgMail.send(msg)
        //     .then((parsedBody) => {
        //         // POST succeeded...
        //         console.log('parsedBody', parsedBody)
        //         return res.status(200).json({
        //             message: 'sendEmail',
        //             responseCode: 201,
        //             data: {
        //                 parsedBody,
        //             },
        //             method: {
        //                 name: 'sendEmail',
        //                 line: 58,
        //             },
        //         })
        //     })
        //     .catch((err) => {
        //         // POST failed...
        //         console.log('err', err)
        //         return res.status(500).json({
        //             message: 'sendEmail',
        //             responseCode: 201,
        //             data: {},
        //             error: err,
        //             method: {
        //                 name: 'sendEmail',
        //                 line: 58,
        //             },
        //         })
        //     });
        // return tttt

    }
}
