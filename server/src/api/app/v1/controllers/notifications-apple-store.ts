import { Response, Request } from 'express'
import moment from 'moment'

import BaseCtrl from './base'

import { SaveEventDirective } from '../../../../models/purchase/interfaces/save-event-directive'
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'
import {
    AppleStoreNotification,
    // AppleStoreNotificationResponse,
    // AppleStoreExpiredNotification,
} from '../../../../models/apple-store-notification/apple-store-notification.model'
import { IOSNotificationType } from '../../../../models/enums/ios-notification-type'
import { db } from '../../../../shared/init/initialise-firebase'
import {
    SubscriptionType,
    SubscriptionStatus,
} from '../../../../models/enums/enums.model'
import { updateAthleteSubscriptionStatus } from './utils/update-subscription-object-status'
import { Athlete } from '../../../../models/athlete/athlete.model'
import { sendMessage } from "../../../../pubsub/subscriptions/utils/send-message";
import { PushNotificationType } from '../../../../models/enums/push-notification-type'
import { persistAthleteReceipt } from "../callables/utils/validate-transaction-receipt/persist-athlete-receipt";
import { PlatformType } from '../../../../models/purchase/enums/platfom_type'
import { extractIOSData } from './utils/extract-ios-data';
import { setAnalytics } from './utils/send-notification';

export default class AppleStoreNotificationsCtrl extends BaseCtrl {
    res: Response

    // logger: Logger;
    model = AppleStoreNotification
    collection = undefined

    constructor() {
        super()
    }

    handleNotificationEvent = async (req: Request, res: Response) => {


        const { docUID } = req.body
        let iosNotification

        if (docUID) {

            iosNotification = await db.collection('iosNotifications')
                .doc(docUID)

                .get()
                .then((docSnap) => {
                    if (docSnap.exists) {
                        return {
                            ...docSnap.data().iosNotification,
                            uid: docSnap.id
                        }
                    } else {
                        return undefined
                    }
                })
            // .add({
            //     platform: PlatformType.IOS,
            //     isExpired,
            //     iosNotification,
            //     creationTimestamp: moment().toDate(),
            // })
        } else {

            iosNotification = <AppleStoreNotification>req.body
        }

        if (iosNotification) {
            const {
                environment,
                // // Not posted for notification_type CANCEL
                latest_receipt_info: latestReceiptInfo,
                // //  Posted only if the subscription expired.
                // // Contains the base-64 encoded transaction receipt for the most recent renewal transaction. Posted only if the subscription expired.
                // latest_expired_receipt
                // // Posted only if the notification_type is RENEWAL or CANCEL or if renewal failed and subscription expired.
                latest_expired_receipt_info: latestExpiredReceiptInfo,
                notification_type: notificationType,
                // cancellation_date: cancellationDate,
                auto_renew_status: autoRenewStatus,
                auto_renew_product_id: productIdentifier,



                //new
                unified_receipt: unifiedReceipt,
                // auto_renew_product_id:"com.inspiresportonline.prod.subscriptions.annual.basic"
                // auto_renew_status:"true"
                // environment:"PROD"
                latest_receipt: latestReceipt, //"ewoJInNpZ25hdHVyZSIgPSAiQTFneXZaZWt0eVVNL0UvNG1tNzFWUVdoTEZ3Um5mT1FxQmRoTVN4SGVjTzZja2lwYXhQYTBjbVoxeW5WTWZ0ZVRBNTdHUkpKMHU4Rm0yQ3BOWXBJWnA2SHRldW8yOHBRRitrdzlqbWlLUU9iWDdIY3NjZFZuZGwyUU1YeFJDSTRKYjY4MzkzRkJTM1pLbGFic3ZHNU9mTmlOaUdOblYvN2dmV1RlamVuRUQyZ3paN05meTl4SjZ2NFAwNHlQbGoxNytiVXk5UGsxY04vdkVhREs5S2pxaGhCZGRCeDJqdWg1STF2VDF5YUhoWjdTaGdPS1pWSExpN1FYZTlvbStuMzF4Z3puY3pYZHFOSC9CQ1BsU2JtYVQwem92NG5xb2U3dys3M3NSMllSOGZKTXd3elhBRlNsL0hhT0N6cDhlTzJFTGthZWRNUUl6SDhTUkJtRHRlVHJ2SUFBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1Y...
                // latest_receipt_info:Object {bid: "com.inspiresportonline.ios", original_transaction_id: "490000600001954", original_purchase_date: "2020-04-12 06:12:48 Etc/GMT", …}
                // notification_type: "INITIAL_BUY"
                // password:"0e5b544e5da74ab48ffc00dc6600bd29"
            } = iosNotification

            let isExpired = false
            // todo: use the ‘original_transaction_id’ in the notification to check for a matching subscription in our database:
            // If there is one, validate that it’s not already been marked as cancelled / finished.

            let receiptInfo
            if (latestReceiptInfo) {
                receiptInfo = latestReceiptInfo
            } else {
                receiptInfo = latestExpiredReceiptInfo
                isExpired = true
            }

            if (receiptInfo) {
                const {
                    original_transaction_id: originalTransactionId,
                    transaction_id: transactionId,
                    bid: appIdentifier,
                    is_trial_period: isTrialPeriod
                } = receiptInfo

                // if (!docUID) {
                //     await db.collection('iosNotifications').add({
                //         platform: PlatformType.IOS,
                //         isExpired,
                //         iosNotification,
                //         creationTimestamp: moment().toDate(),
                //     })
                // }

                if (transactionId) {
                    console.log(
                        `Retrieving Athlete with having originalTransactionId ${originalTransactionId}. Notification Type: ${notificationType}`
                    )

                    const result = db
                        .collection(FirestoreCollection.Athletes)
                        .where(
                            'transactionIds',
                            'array-contains',
                            originalTransactionId
                        )
                        .get()
                        .then(
                            async (querySnap: FirebaseFirestore.QuerySnapshot) => {
                                if (!querySnap.empty) {
                                    const snap = querySnap.docs[0]

                                    const data = snap.data()

                                    const athlete = {
                                        uid: snap.id,
                                        ...data,
                                    } as Athlete

                                    if (!docUID) {
                                        await db.collection('iosNotifications').add({
                                            athleteUid: athlete.uid,
                                            platform: PlatformType.IOS,
                                            isExpired,
                                            iosNotification,
                                            creationTimestamp: moment().toDate(),
                                        })
                                    }

                                    //  todo: don’t process the cancellation unless the environment is ‘PROD’
                                    if (environment !== 'PROD') {
                                        //  If it’s something else, return a response with the status code 200.
                                    }

                                    const receiptObject = extractIOSData(
                                        receiptInfo
                                    )

                                    const expiryDate = receiptObject.expirationDate
                                        ? new Date(+receiptObject.expirationDate)
                                        : moment().toDate()

                                    isExpired = expiryDate < new Date(new Date())


                                    let theNotificationType = IOSNotificationType[notificationType]
                                    const receiptPersitDirective: SaveEventDirective = {
                                        platform: PlatformType.IOS,
                                        transactionId,
                                        originalTransactionId,
                                        expirationDate: expiryDate,
                                        startDate: receiptInfo.purchaseDate,
                                        notificationType: theNotificationType || notificationType,
                                        iosReceipt: receiptObject,
                                    }

                                    // TODO:

                                    // // Indicates the user's preferred Apple ID for renewing the auto-renewable subscription in App Store Connect.
                                    // auto_renew_adam_id

                                    console.log('-------------------------->>>>>>>>>>>>>>>>>>>>>>>>>> IOS Notification Type', notificationType)

                                    switch (notificationType) {
                                        //// INITIAL BUY
                                        case IOSNotificationType.InitialBuy:
                                            // Occurs at the initial purchase of the subscription.
                                            // Store the latest_receipt on your server as a token to
                                            // verify the user’s subscription status at any time, by validating it with the App Store.

                                            // Initial purchase of the subscription.

                                            await persistAthleteReceipt(
                                                athlete.uid,
                                                receiptPersitDirective,
                                                !docUID
                                            )

                                            // todo: Store the latest_receipt on your server as a token to verify the user’s subscription status at any time, by validating it with the App Store.
                                            return updateAthleteSubscriptionStatus(
                                                athlete,
                                                receiptObject,
                                                expiryDate,
                                                isTrialPeriod ? SubscriptionStatus.TrailPeriod : SubscriptionStatus.ValidInitial,
                                                productIdentifier.indexOf('premium') > 0 ? SubscriptionType.Premium : SubscriptionType.Basic,
                                                undefined,
                                                productIdentifier,
                                                appIdentifier,
                                                originalTransactionId,
                                                !docUID,
                                                notificationType
                                            ).then(() => true)
                                        //// CANCEL
                                        case IOSNotificationType.Cancel:
                                            console.warn('CANCELED', {
                                                uid: athlete.uid,
                                                name: athlete.profile.firstName
                                            })
                                            // Indicates that the subscription was canceled either by Apple customer support
                                            // or by the App Store when the user upgraded their subscription.
                                            // The cancellation_date key contains the date and time when the subscription was canceled or upgraded.
                                            // // Posted only if the notification_type is CANCEL
                                            // cancellation_date // Specifies the date and time that Apple customer support canceled a transaction or the customer upgraded their subscription
                                            // web_order_line_item_id

                                            // Subscription was canceled by Apple customer support.

                                            // todo: Check Cancellation Date to know the date and time when the subscription was canceled.

                                            // let originalTransactionId
                                            // let cancellationReason

                                            if (latestExpiredReceiptInfo) {
                                                /* TODO
        
                                                    latest_expired_receipt :
        
                                                    If we’ve have a currently ‘active’ subscription that we need to cancel:
        
                                                    Use the ‘latest_expired_receipt’ key in the notification, and call Apple’s ‘verifyReceipt’ endpoint just to make sure we’ve got the latest information, 
                                                    so then we know we’re working with the latest receipt information.
        
                                                    Then look for a ‘cancellation_date’ key:
        
                                                    If there is one, we can proceed with the cancellation.
        
                                                    Mark the subscription as cancelled and finished in our database and remove the user’s premium access.
        
                                                    ‘cancellation_reason’
        
                                                    This key can have two different values and indicates why the user contacted Apple and asked for their subscription to be cancelled / refunded:
        
                                                    "1" — Customer canceled their transaction due to an actual or perceived issue within your app.
        
                                                    "0" — Transaction was canceled for another reason, for example, if the customer made the purchase accidentally
        
                                                */

                                                const {
                                                    // original_transaction_id: originalTransactionId,
                                                    // cancellation_reason: cancellationReason,
                                                } = latestExpiredReceiptInfo

                                                // originalTransactionId = latestExpiredReceiptInfo.original_transaction_id
                                                // cancellationReason = latestExpiredReceiptInfo.cancellation_reason
                                            }

                                            await persistAthleteReceipt(
                                                athlete.uid,
                                                receiptPersitDirective,
                                                !docUID
                                            )

                                            const actionAnalyticsCancel = setAnalytics(
                                                PushNotificationType.SubscriptionCanceled,
                                                productIdentifier
                                            )
                                            return updateAthleteSubscriptionStatus(
                                                athlete,
                                                receiptObject,
                                                expiryDate,
                                                SubscriptionStatus.Canceled,
                                                athlete.profile.subscription.type,
                                                actionAnalyticsCancel,
                                                productIdentifier,
                                                appIdentifier,
                                                originalTransactionId,
                                                !docUID
                                            ).then(
                                                (updateStatusResult: boolean) => {
                                                    const currentSubscriptionType =
                                                        athlete.profile.subscription
                                                            .type

                                                    if (
                                                        currentSubscriptionType !==
                                                        SubscriptionType.Free
                                                    ) {


                                                        return sendMessage(
                                                            athlete,
                                                            PushNotificationType.SubscriptionCanceled,
                                                            actionAnalyticsCancel
                                                        )
                                                    } else {
                                                        return Promise.resolve(true)
                                                    }
                                                }
                                            )
                                        //// RENEWAL
                                        case IOSNotificationType.Renewal:
                                            // Indicates successful automatic renewal of an expired subscription that failed to renew in the past.
                                            // Check subscription_expiraton_date to determine the next renewal date and time.

                                            // // Posted only if the notification_type is RENEWAL or INTERACTIVE_RENEWAL
                                            // latest_receipt
                                            // // Not posted for notification_type CANCEL
                                            // latest_receipt_info

                                            // // Indicates the reason a subscription expired.
                                            // This is the same as the expiration_intent in the receipt.
                                            // Posted only if notification_type is RENEWAL or INTERACTIVE_RENEWAL
                                            // expiration_intent

                                            // Automatic renewal was successful for an expired subscription.
                                            // // Check subscription_expiraton_date to determine the next renewal date and time

                                            await persistAthleteReceipt(
                                                athlete.uid,
                                                receiptPersitDirective,
                                                !docUID
                                            )
                                            const actionAnalyticsRenewal = setAnalytics(
                                                PushNotificationType.SubscriptionRenewed,
                                                productIdentifier
                                            )
                                            return updateAthleteSubscriptionStatus(
                                                athlete,
                                                receiptObject,
                                                expiryDate,
                                                SubscriptionStatus.ValidRenewed,
                                                undefined,
                                                actionAnalyticsRenewal,
                                                productIdentifier,
                                                appIdentifier,
                                                originalTransactionId,
                                                !docUID
                                            ).then((updateResult: boolean) => {


                                                return sendMessage(
                                                    athlete,
                                                    PushNotificationType.SubscriptionRenewed,
                                                    actionAnalyticsRenewal
                                                )
                                            })
                                        //// INTERACTIVE RENEWAL
                                        case IOSNotificationType.InteractiveRenewal:
                                            // Indicates the customer renewed a subscription interactively, either by using your app’s interface,
                                            // or on the App Store in account settings. Make service available immediately.

                                            // // Posted only if the notification_type is RENEWAL or INTERACTIVE_RENEWAL
                                            // latest_receipt
                                            // // Not posted for notification_type CANCEL
                                            // latest_receipt_info

                                            // // Indicates the reason a subscription expired.
                                            // This is the same as the expiration_intent in the receipt.
                                            // Posted only if notification_type is RENEWAL or INTERACTIVE_RENEWAL
                                            // expiration_intent

                                            await persistAthleteReceipt(
                                                athlete.uid,
                                                receiptPersitDirective,
                                                !docUID
                                            )
                                            const actionAnalytics = setAnalytics(
                                                PushNotificationType.SubscriptionRenewed,
                                                productIdentifier
                                            )


                                            return updateAthleteSubscriptionStatus(
                                                athlete,
                                                receiptObject,
                                                expiryDate,
                                                SubscriptionStatus.ValidRenewed,
                                                undefined,
                                                actionAnalytics,
                                                productIdentifier,
                                                appIdentifier,
                                                originalTransactionId,
                                                !docUID
                                            ).then(
                                                (updateStatusResult: boolean) => {

                                                    return sendMessage(
                                                        athlete,
                                                        PushNotificationType.SubscriptionRenewed,
                                                        actionAnalytics
                                                    )
                                                }
                                            )
                                        //// DID CHANGE RENEWAL REF
                                        case IOSNotificationType.DidChangeRenewalRef:
                                            // Indicates the customer made a change in their subscription plan that takes effect at
                                            // the next renewal. The currently active plan is not affected.

                                            // TODO:

                                            await persistAthleteReceipt(
                                                athlete.uid,
                                                receiptPersitDirective,
                                                !docUID
                                            )
                                            return updateAthleteSubscriptionStatus(
                                                athlete,
                                                receiptObject,
                                                expiryDate,
                                                undefined,
                                                undefined,
                                                undefined,
                                                productIdentifier,
                                                appIdentifier,
                                                originalTransactionId,
                                                !docUID
                                            ).then(() => true)
                                        // DID CHANGE RENEWAL STATUS
                                        case IOSNotificationType.DidChangeRenewalStatus:
                                            // Indicates a change in the subscription renewal status.

                                            // TODO:
                                            // Check the timestamp for the data and time of the latest status update,
                                            // and the auto_renew_status for the current renewal status.
                                            // if (autoRenewStatus) {
                                            // } else {
                                            // }

                                            await persistAthleteReceipt(
                                                athlete.uid,
                                                receiptPersitDirective,
                                                !docUID
                                            )
                                            // TODO: Chech expired
                                            if (isExpired) {
                                                // TODO; send message?

                                                console.warn('EXPIRED', {
                                                    uid: athlete.uid,
                                                    name: athlete.profile.firstName
                                                })
                                                const actionAnalyticsRenewalLapsed = setAnalytics(
                                                    PushNotificationType.SubscriptionLapsed,
                                                    productIdentifier
                                                )
                                                return updateAthleteSubscriptionStatus(
                                                    athlete,
                                                    receiptObject,
                                                    expiryDate,
                                                    SubscriptionStatus.CanceledForceLogout,
                                                    SubscriptionType.Free,
                                                    actionAnalyticsRenewalLapsed,
                                                    productIdentifier,
                                                    appIdentifier,
                                                    originalTransactionId,
                                                    !docUID
                                                ).then(() => true)
                                            } else {
                                                return updateAthleteSubscriptionStatus(
                                                    athlete,
                                                    receiptObject,
                                                    expiryDate,
                                                    undefined,
                                                    undefined,
                                                    undefined,
                                                    productIdentifier,
                                                    appIdentifier,
                                                    originalTransactionId,
                                                    !docUID
                                                ).then(() => true)
                                            }
                                        // TODO:

                                        default:
                                            return true
                                    }
                                } else {
                                    // TODO:

                                    const message = `can't find Athlete with Originall TransactionId ${originalTransactionId}, and TransactionId ${transactionId}`
                                    console.log(
                                        message
                                    )

                                    if (!docUID) {
                                        await db.collection('iosNotifications').add({
                                            error: { message },
                                            platform: PlatformType.IOS,
                                            isExpired,
                                            iosNotification,
                                            creationTimestamp: moment().toDate(),
                                        })
                                    }

                                    return undefined
                                }
                            }
                        )

                    // TODO:
                    return result
                        .then(success => {
                            if (success) {
                                return res.status(200).send(success)
                            } else {
                                return res
                                    .status(203)
                                    .send(
                                        success
                                            ? success
                                            : 'could not find document'
                                    )
                            }
                        })
                        .catch(err => {
                            return res.status(500).send(err)
                        })
                } else {
                    const errorMessage = `Error Retrieving Athlete with having Originall TransactionId ${originalTransactionId}, and TransactionId ${transactionId}`

                    console.log(errorMessage)
                    console.log('iosNotification', JSON.stringify(iosNotification))

                    if (!docUID) {
                        await db.collection('iosNotifications').add({
                            error: { message: errorMessage },
                            platform: PlatformType.IOS,
                            isExpired,
                            iosNotification,
                            creationTimestamp: moment().toDate(),
                        })

                        await db.collection('notificationErrors').add({
                            platform: PlatformType.IOS,
                            isExpired,
                            iosNotification,
                            errorMessage,
                            originalTransactionId: originalTransactionId,
                            transactionId: transactionId,
                            creationTimestamp: moment().toDate(),
                        })
                    }


                    // TODO:
                    return false
                }
            } else {
                console.log('latestReceiptInfo is undefined', latestReceiptInfo)

                if (!docUID) {
                    await db.collection('iosNotifications').add({

                        platform: PlatformType.IOS,
                        isExpired,
                        iosNotification,
                        creationTimestamp: moment().toDate(),
                    })

                    await db.collection('notificationTests').add({
                        iosNotification: iosNotification,
                        errorMessage: 'latestReceiptInfo undefined',
                        payload: req.body,
                        creationTimestamp: moment().toDate(),
                    })
                }

                return res
                    .status(500)
                    .send({ err: 'latestReceiptInfo undefined', reg: req.body })
            }
        } else {
            return res
                .status(500)
                .send({ err: 'iosNotification undefined', reg: req.body })
        }


    }
}
