const useLive = true;

const biometricEntriesSQLQuery = [

    `SELECT 
                        event_name as eventName,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'USER_FULL_NAME') as userFullName,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'USER_FIRST_NAME') as userFirstName,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'USER_ID') as athleteID,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'ORGANIZATION_ID') as organizationID,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'GROUP_1_ID') as groupID,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'GROUP_IDS') as groupIDs,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'ANALYTICS_FAMILY') as analysticsFamily,
                        
                        (SELECT value.int_value FROM UNNEST(event_params) WHERE key = 'VALUE') as value,
                        
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'LIFESTYLE_ENTRY_TYPE') as lifestyleEntryType,
                        (SELECT value.int_value FROM UNNEST(event_params) WHERE key = 'LIFESTYLE_ENTRY_TYPE_ID') as lifestyleEntryTypeID,
                        
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'SPORT_TYPE') as sportType,
                        (SELECT value.int_value FROM UNNEST(event_params) WHERE key = 'SPORT_TYPE_ID') as sportTypeID,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'INTENSITY_LEVEL') as intensityLevel,
                        (SELECT value.int_value FROM UNNEST(event_params) WHERE key = 'INTENSITY_LEVEL_ID') as intensityLevelID,
                        
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'PAIN_TYPE') as painType,
                        (SELECT value.int_value FROM UNNEST(event_params) WHERE key = 'PAIN_TYPE_ID') as painTypeID,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'BODY_LOCATION') as bodyLocation,
                        (SELECT value.int_value FROM UNNEST(event_params) WHERE key = 'BODY_LOCATION_ID') as bodyLocationID,
                        
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'MEAL_TYPE') as mealType,
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'FOOD_TYPE') as foodType,
                        
                        (SELECT value.string_value FROM UNNEST(event_params) WHERE key = 'CREATION_TIMESTAMP') as creationTimestamp
                        `,

    `FROM \`inspire-1540046634666.analytics_193010412.events_*\``, // DEV5
    // `FROM \`inspire-219716.analytics_186608196.events_*\``, // LIVE

    // `FROM \`inspire-1540046634666.analytics_193010412.events_intraday_*\``,
    // `WHERE _TABLE_SUFFIX BETWEEN @startDate AND @endDate
    `WHERE 
        _TABLE_SUFFIX BETWEEN @startDate AND @endDate AND
        (SELECT value.int_value FROM UNNEST(event_params) WHERE key = 'LIFESTYLE_ENTRY_TYPE_ID') is not null AND
        (SELECT value.int_value FROM UNNEST(event_params) WHERE key = 'VALUE') is not null`,

    `ORDER BY creationTimestamp`

]
// .join(' ');

export const getBQSQL = (type?: string) => {

    if (type === 'biometricEntriesSQLQuery') {
        console.log(type)
    }
    console.warn('useLive', useLive)
    if (useLive) {
        console.warn('useLive', useLive)
        
        console.warn('  biometricEntriesSQLQuery[1]',   biometricEntriesSQLQuery[1])
        
        console.log('setting biometricEntriesSQLQuery[1] = `FROM \`inspire-219716.analytics_186608196.events_*\``')
        biometricEntriesSQLQuery[1] = `FROM \`inspire-219716.analytics_186608196.events_*\``
    }
    return biometricEntriesSQLQuery;
}

 // const theSleepLessThanFourHoursQuery = [
        //     `SELECT 
        //                 t0.event_name as eventName, 
        //                 COUNT(UserProperties.value.int_value) AS value,
        //                 t0.user_id as userId`,
        //     `FROM 
        //                 (
        //                   SELECT * FROM \`inspire-1540046634666.analytics_193010412.events_*\` 
        //                   WHERE _TABLE_SUFFIX BETWEEN @startDate AND @endDate
        //                   AND  event_name = '@eventName'
        //                 ) AS t0 CROSS JOIN 
        //                 UNNEST(t0.user_properties) AS UserProperties`,
        //     `WHERE REGEXP_CONTAINS(t0.user_id, '^.*@uid.*$')

        //                 GROUP BY eventName, userId 
        //                 ORDER BY value DESC;`
        // ].join(' ')


        // const biometricsSleepLessThanFourHours = await bigqueryClient.query({
        //     query: theSleepLessThanFourHoursQuery,
        //     params: {
        //         //   uid: athleteUID,
        //         eventName: EventTypes.biometricsSleepLessThanFourHours,
        //         startDate: threeDaysBackDate.format("YYYYMMDD"),
        //         endDate: latestReportDate.format("YYYYMMDD")

        //     }
        // }).catch((err) => {
        //     console.log(`Error retrieving BigQuery (theQuery) query inspire-1540046634666.analytics_193010412.events_${latestReportDate.format("YYYYMMDD")}.${EventTypes.biometricTrainingCreated}`, err)
        //     return err
        // }) as Array<BiometricEntrySchema>;

        // console.log(`BigQuery Query (theQuery) Row Count for inspire-1540046634666.analytics_193010412.events_20190330.${EventTypes.biometricTrainingCreated}`, rows.length)
