import { Athlete } from './../../../../models/athlete/athlete.model';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { Response, Request } from 'express';
import { firestore } from 'firebase-admin';
import { bigqueryClient } from '../callables/utils/big-query/initBQ';
import { BigQuery } from '@google-cloud/bigquery';

import BaseCtrl from './base'
import { initialiseFirebase } from '../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';

import { deleteTable } from '../callables/utils/big-query/delete-table';
import { createDatasetIfNotExists, tableExists } from '../callables/bigQSqlQuery';
import { createTable } from '../callables/utils/big-query/create-table';
import { ResponseObject } from '../../../../shared/ResponseObject';
import { SegmentDimension } from '../callables/utils/warehouse/segment-dimension.interface';
import { SegmentAthleteDimension } from '../callables/utils/warehouse/segment-athlete-dimension.interface';
import { getAthleteSegments } from '../callables/utils/warehouse/get-athlete-segments';
import { ProgramContentTagDimension } from '../callables/utils/warehouse/program-content-tag-dimension.interface';
import { ProgramCatalogue } from '../../../../db/programs/models/interfaces/program-catalogue';
import { isArray } from 'lodash';
import { ProgramContent } from '../../../../db/programs/models/interfaces/program-content';
import { ProgramDay } from '../../../../db/programs/models/interfaces/program-day';
import { ProgramDayEntry } from '../../../../db/programs/models/interfaces/program-day-entry';
import { ProgramContentType } from '../../../../models/enums/enums.model';
import { ProgramContentDimension } from '../callables/utils/warehouse/program-content-dimension.interface';
import { AthleteDimension } from '../callables/utils/warehouse/athlete-dimension.interface';
import { ProgramDimension } from '../callables/utils/warehouse/program-dimension.interface';
import { getDistanceLeaderboardFromCubeJs } from '../callables/get-bq-leaderboard-distance-data';
import { OlapGetLeaderboardRequest } from '../callables/utils/warehouse/olap/interfaces/olap-get-leaderboard-request.interface';
import { OlapSecurityProfile } from '../callables/utils/warehouse/olap/interfaces/olap-security-profile.interface';
import { getDistanceStatisticsFromCubeJs } from '../callables/get-bq-distance-statistics-data';

/**
* The controller handles the requests passed along by the router
// * @param logger: winston.Logger
*/
export default class WarehouseCtrl extends BaseCtrl {
    model = Athlete;
    collection = FirestoreCollection.Athletes;
    res: Response;

    private datasetId: string = 'catalystDW';
    private athleteDimTable = 'athleteDim';

    private segmentDimTable = 'segmentDim';
    private segmentAthleteDimTable = 'segmentAthleteDim';

    private programDimTable = 'programDim';
    private programContentTagDimTable = 'programContentTagDim';
    private programContentDimTable = 'programContentDim';

    private athleteDimSchema = [
        { "name": "athlete_id", type: "STRING", "mode": "REQUIRED" },
        { "name": "first_name", type: 'STRING', "mode": "NULLABLE" },
        { "name": "last_name", type: "STRING", "mode": "NULLABLE" },
        { "name": "local_birth_date", type: "INT64", "mode": "NULLABLE" },
        { "name": "sex", type: "STRING", "mode": "NULLABLE" },
        { "name": "location", type: "STRING", "mode": "NULLABLE" }
    ]

    private segmentDimSchema = [
        { "name": "segment_id", type: "STRING", "mode": "REQUIRED" },
        { "name": "parent_segment_id", type: 'STRING', "mode": "NULLABLE" },
        { "name": "segment_name", type: "STRING", "mode": "NULLABLE" },
        { "name": "segment_type", type: "STRING", "mode": "NULLABLE" },
        { "name": "bundle_id", type: "STRING", "mode": "NULLABLE" }
    ]

    private segmentAthleteDimSchema = [
        { "name": "segment_id", type: "STRING", "mode": "REQUIRED" },
        { "name": "athlete_id", type: 'STRING', "mode": "REQUIRED" }
    ]

    constructor() {
        super()
    }

    resetAthleteDimensionTable = async (req: Request, res: Response) => {
        this.res = res;

        try {
            const response = await this.resetTable(this.datasetId, this.athleteDimTable, this.athleteDimSchema);

            return this.handleResponse(response);
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /warehouse/dimension/athlete/reset-table',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /warehouse/dimension/athlete/reset-table',
                    exception: exception,
                },
                method: {
                    name: 'migrateAthleteDimension',
                    line: 76,
                },
                res
            })
        }
    }

    resetSegmentDimensionTables = async (req: Request, res: Response) => {
        this.res = res;

        try {
            let response = await this.resetTable(this.datasetId, this.segmentDimTable, this.segmentDimSchema);
            if (response.responseCode === 200) {
                response = await this.resetTable(this.datasetId, this.segmentAthleteDimTable, this.segmentAthleteDimSchema);

                return this.handleResponse(response);
            } else {
                return this.handleResponse(response);
            }
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /warehouse/dimension/athlete/reset-table',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /warehouse/dimension/athlete/reset-table',
                    exception: exception,
                },
                method: {
                    name: 'migrateAthleteDimension',
                    line: 76,
                },
                res
            })
        }
    }

    migrateAthleteDimension = async (req: Request, res: Response) => {
        this.res = res;

        try {

            const rows: Array<AthleteDimension> = [];
         
            return this.db.collection(FirestoreCollection.Athletes)
                .get()
                .then(async (docs) => {
                    const ln = docs.docs.length;
                    console.log(`Current athletes length: ${ln}`);

                    if (ln) {
                        docs.forEach((doc) => {
                            if (doc.exists) {
                                const a = doc.data() as Athlete;
                                a.uid = doc.id;
                                const dim = this.getAthleteDimension(a);
                                if (dim) {
                                    rows.push(dim);
                                }
                            }
                        });

                        let batch1 = [];
                        let batch2 = [];
                        if (rows.length > 10000) {
                            batch1 = rows.slice(0, 10000);
                            batch2 = rows.slice(10000);
                        } else {
                            batch1 = rows;
                        }

                        if (batch1.length) {
                            let theRes = await bigqueryClient
                                .dataset(this.datasetId)
                                .table(this.athleteDimTable)
                                .insert(batch1)
                                .then((rowCount) => {
                                    return { 
                                        rowCount, 
                                        errors: undefined }
                                })
                                .catch((e) => {
                                    console.log(e.errors);
                                    return {
                                        rowCount: undefined,
                                        errors: e.errors
                                    }
                                });

                            if (batch2.length > 0) {
                                theRes = await bigqueryClient
                                    .dataset(this.datasetId)
                                    .table(this.athleteDimTable)
                                    .insert(batch2)
                                    .then((rowCount) => {
                                        return { 
                                            rowCount, 
                                            errors: undefined }
                                    })
                                    .catch((e) => {
                                        console.log(e.errors);
                                        return {
                                            rowCount: undefined,
                                            errors: e.errors
                                        }
                                    });
                            }

                            if (theRes.rowCount !== undefined) {
                                console.log(`Inserted ${rows.length} rows`);
                        
                                return this.handleResponse({
                                    message: `Successfully migrated ${rows.length} athletes to athlete dimension in BigQuery`,
                                    responseCode: 200,
                                    res: this.res
                                } as ResponseObject);
                            } else {
                                return this.handleResponse({
                                    message: 'Errors occurred writing athlete dimension to BigQuery',
                                    data: theRes.errors,
                                    responseCode: 400,
                                    res: this.res
                                } as ResponseObject);
                            }
                    
                        } else {
                            // Return response
                            return this.handleResponse({
                                message: `No athletes to migrate to athlete dimension in BigQuery`,
                                responseCode: 200,
                                res: this.res
                            } as ResponseObject);
                        }
                    } else {
                        // Return response
                        return this.handleResponse({
                            message: `No athletes to migrate to athlete dimension in BigQuery`,
                            responseCode: 200,
                            res: this.res
                        } as ResponseObject);
                    }
                });
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /warehouse/dimension/athlete/migrate',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /warehouse/dimension/athlete/migrate',
                    exception: exception,
                },
                method: {
                    name: 'migrateAthleteDimension',
                    line: 76,
                },
                res
            })
        }
    }

    migrateSegmentDimension = async (req: Request, res: Response) => {
        this.res = res;

        try {

            const athletes: Array<Athlete> = [];
            const segmentDimRows: Array<SegmentDimension> = [];
            const segmentAthleteDimRows: Array<SegmentAthleteDimension> = [];
         
            return this.db.collection(FirestoreCollection.Athletes)
                .get()
                .then(async (docs) => {
                    const ln = docs.docs.length;
                    console.log(`Current athletes length: ${ln}`);

                    if (ln) {
                        let i = 1;
                        for (const doc of docs.docs) {
                            if (doc.exists) {
                                const a = doc.data() as Athlete;
                                a.uid = doc.id;

                                const compoundResponse = await getAthleteSegments(a);
                                compoundResponse.orgSegmentResponse.segments.forEach((s: SegmentDimension) => {
                                    const existing = segmentDimRows.find((sd: SegmentDimension) => {
                                        return !!sd && sd.segment_id === s.segment_id;
                                    });

                                    if (!existing) {
                                        segmentDimRows.push(s);
                                    }
                                });

                                compoundResponse.orgSegmentResponse.segmentAthletes.forEach((s: SegmentAthleteDimension) => {
                                    const existing = segmentAthleteDimRows.find((sa: SegmentAthleteDimension) => {
                                        return !!sa && sa.segment_id === s.segment_id && sa.athlete_id === s.athlete_id;
                                    });

                                    if (!existing) {
                                        segmentAthleteDimRows.push(s);
                                    }
                                });

                                compoundResponse.groupSegmentResponse.segments.forEach((s: SegmentDimension) => {
                                    const existing = segmentDimRows.find((sd: SegmentDimension) => {
                                        return !!sd && sd.segment_id === s.segment_id;
                                    });

                                    if (!existing) {
                                        segmentDimRows.push(s);
                                    }
                                });

                                compoundResponse.groupSegmentResponse.segmentAthletes.forEach((s: SegmentAthleteDimension) => {
                                    const existing = segmentAthleteDimRows.find((sa: SegmentAthleteDimension) => {
                                        return !!sa && sa.segment_id === s.segment_id && sa.athlete_id === s.athlete_id;
                                    });

                                    if (!existing) {
                                        segmentAthleteDimRows.push(s);
                                    }
                                });
                                console.log(`Processed ${i} records`);
                                i++;
                            }
                        }

                        if (segmentDimRows.length) {
                            let theRes = await bigqueryClient
                                .dataset(this.datasetId)
                                .table(this.segmentDimTable)
                                .insert(segmentDimRows)
                                .then((rowCount) => {
                                    return { 
                                        rowCount, 
                                        errors: undefined }
                                })
                                .catch((e) => {
                                    console.log(e.errors);
                                    return {
                                        rowCount: undefined,
                                        errors: e.errors
                                    }
                                });

                            if (segmentAthleteDimRows.length) {
                                let batch1 = [];
                                let batch2 = [];
                                let batch3 = [];
                                let batch4 = [];
                                if (segmentAthleteDimRows.length < 10000) {
                                    batch1 = segmentAthleteDimRows;
                                }
                                else if (segmentAthleteDimRows.length > 30000) {
                                    batch1 = segmentAthleteDimRows.slice(0, 10000);
                                    batch2 = segmentAthleteDimRows.slice(10000, 10000);
                                    batch3 = segmentAthleteDimRows.slice(20000, 10000);
                                    batch4 = segmentAthleteDimRows.slice(30000);
                                }
                                else if (segmentAthleteDimRows.length > 20000) {
                                    batch1 = segmentAthleteDimRows.slice(0, 10000);
                                    batch2 = segmentAthleteDimRows.slice(10000, 20000);
                                    batch3 = segmentAthleteDimRows.slice(20000);
                                }
                                else if (segmentAthleteDimRows.length > 10000) {
                                    batch1 = segmentAthleteDimRows.slice(0, 10000);
                                    batch2 = segmentAthleteDimRows.slice(10000);
                                } else {
                                    batch1 = segmentAthleteDimRows;
                                }
                                if (batch1.length > 0) {
                                    theRes = await bigqueryClient
                                        .dataset(this.datasetId)
                                        .table(this.segmentAthleteDimTable)
                                        .insert(batch1)
                                        .then((rowCount) => {
                                            return { 
                                                rowCount, 
                                                errors: undefined }
                                        })
                                        .catch((e) => {
                                            console.log(e.errors);
                                            return {
                                                rowCount: undefined,
                                                errors: e.errors
                                            }
                                        });
                                }

                                if (batch2.length > 0) {
                                    theRes = await bigqueryClient
                                        .dataset(this.datasetId)
                                        .table(this.segmentAthleteDimTable)
                                        .insert(batch2)
                                        .then((rowCount) => {
                                            return { 
                                                rowCount, 
                                                errors: undefined }
                                        })
                                        .catch((e) => {
                                            console.log(e.errors);
                                            return {
                                                rowCount: undefined,
                                                errors: e.errors
                                            }
                                        });
                                }

                                if (batch3.length > 0) {
                                    theRes = await bigqueryClient
                                        .dataset(this.datasetId)
                                        .table(this.segmentAthleteDimTable)
                                        .insert(batch3)
                                        .then((rowCount) => {
                                            return { 
                                                rowCount, 
                                                errors: undefined }
                                        })
                                        .catch((e) => {
                                            console.log(e.errors);
                                            return {
                                                rowCount: undefined,
                                                errors: e.errors
                                            }
                                        });
                                }

                                if (batch4.length > 0) {
                                    theRes = await bigqueryClient
                                        .dataset(this.datasetId)
                                        .table(this.segmentAthleteDimTable)
                                        .insert(batch4)
                                        .then((rowCount) => {
                                            return { 
                                                rowCount, 
                                                errors: undefined }
                                        })
                                        .catch((e) => {
                                            console.log(e.errors);
                                            return {
                                                rowCount: undefined,
                                                errors: e.errors
                                            }
                                        });
                                }
                        
                                return this.handleResponse({
                                    message: `Successfully migrated ${segmentDimRows.length} segments and ${segmentAthleteDimRows.length} segmentAthletes to segment dimensions in BigQuery`,
                                    responseCode: 200,
                                    res: this.res
                                } as ResponseObject);
                            } else {
                                return this.handleResponse({
                                    message: 'Errors occurred writing segment dimension to BigQuery',
                                    data: theRes.errors,
                                    responseCode: 400,
                                    res: this.res
                                } as ResponseObject);
                            }
                    
                        } else {
                            // Return response
                            return this.handleResponse({
                                message: `No segments to migrate to segment dimension in BigQuery`,
                                responseCode: 200,
                                res: this.res
                            } as ResponseObject);
                        }
                    } else {
                        // Return response
                        return this.handleResponse({
                            message: `No segments to migrate to segment dimension in BigQuery`,
                            responseCode: 200,
                            res: this.res
                        } as ResponseObject);
                    }
                });
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /warehouse/dimension/segment/migrate',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /warehouse/dimension/segment/migrate',
                    exception: exception,
                },
                method: {
                    name: 'migrateSegmentDimension',
                    line: 76,
                },
                res
            })
        }
    }

    migrateProgramDimension = async (req: Request, res: Response) => {
        this.res = res;

        try {

            const programCategories: Array<string> = ['general', 'sportsNutrition', 'sportsPsychology', 'sportsScience'];
            const programRows: Array<ProgramDimension> = [];
            const programContentRows: Array<ProgramContentDimension> = [];
            const programContentTagRows: Array<ProgramContentTagDimension> = [];

            for (const category of programCategories) {
                console.log(`Calling getProgramContentDimensionValuesForCategory with category ${category}`);
                console.log(`programContentRows length: ${programContentRows.length}`);
                console.log(`programContentTagRows length: ${programContentTagRows.length}`);
                const response = await this.getProgramContentDimensionValuesForCategory(category, programRows, programContentRows, programContentTagRows);

                console.log(`Response: ${response}`);
            }

            console.log('getProgramContentDimensionValuesForCategory complete');
            console.log(`programRows length: ${programRows.length}`);
            console.log(`programContentRows length: ${programContentRows.length}`);
            console.log(`programContentTagRows length: ${programContentTagRows.length}`);

            if (programRows.length) {
                let theRes = await bigqueryClient
                        .dataset(this.datasetId)
                        .table(this.programDimTable)
                        .insert(programRows)
                        .then((rowCount) => {
                            return { 
                                rowCount, 
                                errors: undefined }
                        })
                        .catch((e) => {
                            console.log(e.errors);
                            return {
                                rowCount: undefined,
                                errors: e.errors
                            }
                        });
                if (programContentRows.length) {
                    theRes = await bigqueryClient
                        .dataset(this.datasetId)
                        .table(this.programContentDimTable)
                        .insert(programContentRows)
                        .then((rowCount) => {
                            return { 
                                rowCount, 
                                errors: undefined }
                        })
                        .catch((e) => {
                            console.log(e.errors);
                            return {
                                rowCount: undefined,
                                errors: e.errors
                            }
                        });

                    if (programContentTagRows.length) {
                        theRes = await bigqueryClient
                            .dataset(this.datasetId)
                            .table(this.programContentTagDimTable)
                            .insert(programContentTagRows)
                            .then((rowCount) => {
                                return { 
                                    rowCount, 
                                    errors: undefined }
                            })
                            .catch((e) => {
                                console.log(e.errors);
                                return {
                                    rowCount: undefined,
                                    errors: e.errors
                                }
                            });
                
                        return this.handleResponse({
                            message: `Successfully migrated ${programContentRows.length} program content rows and ${programContentTagRows.length} program content tag rows program content dimensions in BigQuery`,
                            responseCode: 200,
                            res: this.res
                        } as ResponseObject);
                    } else {
                        return this.handleResponse({
                            message: 'Errors occurred writing program content dimensions to BigQuery',
                            data: theRes.errors,
                            responseCode: 400,
                            res: this.res
                        } as ResponseObject);
                    }
                }
            }
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /warehouse/dimension/program/migrate',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /warehouse/dimension/program/migrate',
                    exception: exception,
                },
                method: {
                    name: 'migrateProgramDimension',
                    line: 76,
                },
                res
            })
        }
    }

    testOlapQuery = async (req: Request, res: Response) => {
        this.res = res;
        const request = req.body as OlapGetLeaderboardRequest;
        try {
            const data = await getDistanceStatisticsFromCubeJs(request);

            return this.handleResponse({
                message: '',
                res: this.res,
                responseCode: 200,
                data
            });
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /warehouse/test/olap/query',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /warehouse/test/olap/query',
                    exception: exception,
                },
                method: {
                    name: 'migrateProgramDimension',
                    line: 76,
                },
                res
            })
        }
    }

    private resetTable = async (datasetId: string, tableId: string, tableSchema: any) => {
        const success = await createDatasetIfNotExists(this.datasetId);
            
        if (success) {
            console.log(`Dataset ${datasetId} exists or was created!`);
            const exists = await tableExists(datasetId, tableId);

            if (exists) {
                const sqlQuery = `DELETE FROM ${datasetId}.${tableId} WHERE 1 = 1;`;
                // Query options list: https://cloud.google.com/bigquery/docs/reference/v2/jobs/query
                const options = {
                    query: sqlQuery,
                    timeoutMs: 100000, // Time out after 100 seconds.
                    useLegacySql: false, // Use standard SQL syntax for queries.
                };

                const theRes = await bigqueryClient
                        .query(options)
                        .then(() => {
                            return { 
                                success: true, 
                                error: undefined }
                        })
                        .catch((e) => {
                            console.log(e);
                            return {
                                success: false, 
                                error: e
                            }
                        });

                return {
                    message: theRes.success ? 'Dimension table successfully reset in BigQuery' : `Dimension table reset failed in BigQuery.  Message: ${theRes.error.message}`,
                    responseCode: theRes.success ? 200 : 400,
                    res: this.res
                } as ResponseObject;
            } else {
                const dimTable = await createTable(datasetId, tableId, tableSchema);

                return {
                    message: `Athlete dimension table successfully reset in BigQuery`,
                    responseCode: 200,
                    res: this.res
                } as ResponseObject;
            }
        } else {
            throw new Error('catalystDW warehouse dataset creation failed.');
        }
    }

    private getAthleteDimension(athlete: Athlete): AthleteDimension {
        if (athlete) {
            const first_name = athlete.profile.firstName || '';
            const last_name = athlete.profile.lastName || '';
            return {
                athlete_id: athlete.uid,
                first_name,
                last_name,
                full_name: `${first_name} ${last_name}`,
                local_birth_date: this.getDateDimensionValueFromStringDate(athlete.profile.dob),
                sex: (athlete.profile.sex) ? athlete.profile.sex.toLowerCase() : null,
                location: athlete.profile.location || null
            } as AthleteDimension;
        } else {
            return undefined;
        }
    }

    private async getProgramContentDimensionValuesForCategory(category: string, programRows: Array<ProgramDimension>, programContentRows: Array<ProgramContentDimension>, programContentTagRows: Array<ProgramContentTagDimension>): Promise<ProgramCategoryResponse> {
        const progDoc = await this.getProgramCategoryDocument(category);

        if (progDoc) {
            if (isArray(progDoc.programs)) {
                for (const prog of progDoc.programs) {
                    programRows.push({
                        program_id: prog.programGuid,
                        program_name: prog.name,
                        program_category: category
                    });
            
                    if (isArray(prog.tags)) {
                        prog.tags.forEach((tag: string) => {
                            programContentTagRows.push({
                                program_id: prog.programGuid,
                                tag_name: tag
                            } as ProgramContentTagDimension);
                        });
                    }

                    const contentDoc = await this.getProgramCategoryContentDocument(category, prog.programGuid);
                    if (isArray(contentDoc.content)) {
                        contentDoc.content.forEach((day: ProgramDay) => {
                            if (isArray(day.entries)) {
                                day.entries.forEach((entry: ProgramDayEntry) => {
                                    if (isArray(entry.tags)) {
                                        entry.tags.forEach((tag: string) => {
                                            programContentTagRows.push({
                                                program_id: prog.programGuid,
                                                content_entry_id: entry.fileGuid,
                                                tag_name: tag
                                            } as ProgramContentTagDimension);
                                        });
                                    }

                                    programContentRows.push({
                                        program_id: prog.programGuid,
                                        program_name: prog.name,
                                        program_category: category,
                                        content_entry_id: entry.fileGuid,
                                        content_entry_name: entry.title,
                                        content_entry_type: ProgramContentType[entry.type]

                                    } as ProgramContentDimension);
                                });
                            }
                        });
                    }

                }
            }
        }

        return Promise.resolve({
            programContentRows,
            programContentTagRows
        } as ProgramCategoryResponse);
    }

    private getProgramCategoryDocument(category: string): Promise<ProgramCatalogue> {
        return this.db.collection(FirestoreCollection.ProgramCatalogue)
                   .doc(category)
                   .get()
                   .then((doc) => {
                       if (doc.exists) {
                           const progDoc = doc.data() as ProgramCatalogue;

                           return progDoc;
                       } else {

                           console.log(`Program category ${category} does not exist!`);
                           return undefined;
                       }
                   })
                   .catch((err) => {

                       console.log(`Error raised when querying program category ${category}.  Message: ${err.message}`);
                       return undefined;
                   });
    }

    private getProgramCategoryContentDocument(category: string, contentGuid: string): Promise<ProgramContent> {
        return this.db.collection(`${FirestoreCollection.ProgramCatalogue}/${category}/content`)
                   .doc(contentGuid)
                   .get()
                   .then((doc) => {
                       if (doc.exists) {
                           const contentDoc = doc.data() as ProgramContent;

                           return contentDoc;
                       } else {

                           console.log(`Program content for category ${category} and content guid ${contentGuid} does not exist!`);
                           return undefined;
                       }
                   })
                   .catch((err) => {

                       console.log(`Error raised when querying program category ${category} content ${contentGuid}.  Message: ${err.message}`);
                       return undefined;
                   });
    }


    private getDateDimensionValueFromStringDate(date: string): number {
        if (date) {
            const parts = date.split('/');
            if (parts.length === 3) {
                const formatted = `${parts[2]}${parts[1]}${parts[0]}`;
                console.log(`Converted date dimension value: ${formatted}`);

                return parseInt(formatted);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}

interface ProgramCategoryResponse {
    programContentRows: Array<ProgramContentDimension>;
    programContentTagRows: Array<ProgramContentTagDimension>;
}
