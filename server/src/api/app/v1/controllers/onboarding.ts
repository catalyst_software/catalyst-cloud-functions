import { Response, Request } from 'express'

import { cloneDeep, isArray } from 'lodash';

import { db } from '../../../../shared/init/initialise-firebase';

import BaseCtrl from './base'
import { Organization } from '../../../../models/organization.model'

import { getAthleteDocRef, getGroupDocFromUID, getAthleteDocFromUID, getOrganizationDocFromUID } from '../../../../db/refs';

import { WhiteLabelTheme } from './../interfaces/WhiteLabelTheme';
import { OrganizationInterface } from './../../../../models/organization.model';

import { GroupIndicator } from './../../../../db/biometricEntries/well/interfaces/group-indicator';
import { Athlete, OrganizationIndicator } from './../../../../models/athlete/interfaces/athlete';
import { getSharedSystemConfiguration } from './../callables/utils/warehouse/olap/get-olap-jwt-package';


import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager';
import { Group } from '../../../../models/group/interfaces/group';
import { AthleteIndicator } from '../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { CustomClaimsPayload, ConsentRequestDocument, ConsentRequestResponse } from '../callables/get-custom-auth-token';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { manageDataWarehouseSegmentDimension, manageDataWarehouseAthleteSegmentDimension } from '../../../../pubsub/subscriptions/taskWorkers/helpers/helpers';
import { SegmentAthleteDimension } from '../callables/utils/warehouse/segment-athlete-dimension.interface';
import { SegmentDimension } from '../callables/utils/warehouse/segment-dimension.interface';
import { SegmentType } from '../callables/utils/warehouse/segment-type.enum';
import { bigqueryClient } from './../callables/utils/big-query/initBQ';

const jwt = require('jwt-simple');

const enum OnboardingInvitationOperationType {
    Noop,
    SimpleGroupAddition,
    SimpleOrganizationSwap,
    SimpleOrganizationSwapWithGroupAddition,
    NewOrganizationSwap
}

export interface OnboardingInvitationResponse {
    accepted: boolean;
    athleteId: string;
    groupId: string;
    userName: string;
    groupName: string;
}

/**
* The controller handles the requests passed along by the router
// * @param logger: winston.Logger
*/
export default class OnboardingCtrl extends BaseCtrl {
    res: Response;

    model = Organization;
    collection = undefined;// FirestoreCollection.Organizations
    aggregateDocumentManager: AggregateDocumentManager;
    // db = initialiseFirebase('API').db;

    constructor() {
        super();
        // this.db = initialiseFirebase('API').db;
    }

    processInvitation = async (req: Request, res: Response) => {
        this.res = res;
        debugger;

        try {

            console.log({ 'req': req })
            console.log({ 'req.query': req.query })
            console.log({ 'req.body': req.body })

            const athleteId = req.body.athleteId.toString();

            let msg;

            if (!!athleteId) {
                const orgId = req.body.orgId.toString();
                const groupId = req.body.groupId.toString();
                const token = req.body.token.toString();
                const accepted = req.body.accepted ? req.body.accepted.toString().toLowerCase() === 'true' : false;

                const config = await getSharedSystemConfiguration();
                const decodedJwt: CustomClaimsPayload = jwt.decode(token, config.jwtSecret);

                // const db = initialiseFirebase('API').db;
                const secDoc = await db.collection(FirestoreCollection.ConsentRequestQueue)
                    .doc(decodedJwt.guid)
                    .get()
                    .then((doc) => {
                        if (doc.exists) {
                            return doc.data() as ConsentRequestDocument
                        } else {
                            return undefined;
                        }
                    });

                if (!!secDoc && secDoc.guid === decodedJwt.guid && secDoc.valid && secDoc.consentResponse === ConsentRequestResponse.RequestPending) {
                    // 24 hours in milliseconds
                    const maxTime = (24 * 60 * 60 * 1000);
                    const now = (new Date()).getTime();
                    const diff = now - decodedJwt.creationTimestamp;
                    let updatedAthlete;

                    if (diff < maxTime && secDoc.valid) {
                        const athlete: Athlete = await getAthleteDocFromUID(athleteId);
                        const group: Group = await getGroupDocFromUID(groupId);
                        if (accepted) {
                            const opType = this.getOnboardingInvitationOperationType(athlete, orgId, groupId);

                            switch (opType) {
                                case OnboardingInvitationOperationType.SimpleGroupAddition:
                                    updatedAthlete = await this.runSimpleGroupAddition(secDoc, athlete, groupId);
                                    await this.invalidateSecurityToken(secDoc, true);
                                    return res.json({
                                        accepted: true,
                                        athleteId,
                                        groupId, 
                                        userName: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
                                        groupName: group.groupName 
                                    } as OnboardingInvitationResponse);
                                case OnboardingInvitationOperationType.SimpleOrganizationSwap:
                                    updatedAthlete = await this.runSimpleOrganizationSwap(secDoc, athlete, orgId);
                                    await this.invalidateSecurityToken(secDoc, true);
                                    return res.json({
                                        accepted: true,
                                        athleteId,
                                        groupId, 
                                        userName: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
                                        groupName: group.groupName 
                                    } as OnboardingInvitationResponse);
                                case OnboardingInvitationOperationType.SimpleOrganizationSwapWithGroupAddition:
                                    updatedAthlete = await this.runSimpleOrganizationSwapWithGroupAddition(secDoc, athlete, orgId, groupId);
                                    await this.invalidateSecurityToken(secDoc, true);
                                    return res.json({
                                        accepted: true,
                                        athleteId,
                                        groupId, 
                                        userName: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
                                        groupName: group.groupName 
                                    } as OnboardingInvitationResponse);
                                case OnboardingInvitationOperationType.NewOrganizationSwap:
                                    updatedAthlete = await this.runNewOrganizationSwap(secDoc, athlete, orgId, groupId);
                                    await this.invalidateSecurityToken(secDoc, true);
                                    return res.json({
                                        accepted: true,
                                        athleteId,
                                        groupId, 
                                        userName: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
                                        groupName: group.groupName 
                                    } as OnboardingInvitationResponse);
                                default:
                                    msg = 'No operation type specified for this request';
                                    console.error(msg);

                                    await this.invalidateSecurityToken(secDoc, false, true);

                                    return this.handleResponse({
                                        message: msg,
                                        responseCode: 400,
                                        data: undefined,
                                        error: {
                                            message: msg,
                                            exception: undefined
                                        },
                                        method: {
                                            name: 'processInvitation',
                                            line: 30,
                                        },
                                        res
                                    });
                            }
                        } else {
                            // User declined invite
                            await this.invalidateSecurityToken(secDoc, false, false);
                            return res.json({
                                accepted: false,
                                athleteId,
                                groupId, 
                                userName: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
                                groupName: group.groupName 
                            });
                        }
                    } else {
                        msg = 'Forbidden.  Expired Token.';
                        console.error(msg);

                        return this.handleResponse({
                            message: msg,
                            responseCode: 403,
                            data: undefined,
                            error: {
                                message: msg,
                                exception: undefined
                            },
                            method: {
                                name: 'processInvitation',
                                line: 30,
                            },
                            res
                        });
                    }
                } else {
                    msg = 'Forbidden.  Invalid Token.';
                    console.error(msg);

                    return this.handleResponse({
                        message: msg,
                        responseCode: 403,
                        data: undefined,
                        error: {
                            message: msg,
                            exception: undefined
                        },
                        method: {
                            name: 'processInvitation',
                            line: 30,
                        },
                        res
                    });
                }
            } else {
                msg = 'ERROR. No AthleteId specified.';
                console.error(msg);

                return this.handleResponse({
                    message: msg,
                    responseCode: 503,
                    data: undefined,
                    error: {
                        message: msg,
                        exception: undefined
                    },
                    method: {
                        name: 'processInvitation',
                        line: 30,
                    },
                    res
                });
            }

        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception);
            // return res.status(505).send(exception);

            const { message, stack } = exception;
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred processInvitationLink',
                responseCode: 500,
                data: undefined,
                error: {
                    message,
                    exception: stack,
                },
                method: {
                    name: 'processInvitationLink',
                    line: 30,
                },
                res,
            });
        }

    }

    performMaintenanceMove = async (req: Request, res: Response) => {
        this.res = res;
        debugger;

        try {

            console.log({ 'req': req })
            console.log({ 'req.query': req.query })
            console.log({ 'req.body': req.body })

            const athleteId = req.body.athleteId.toString();

            let msg;

            if (!!athleteId) {
                const orgId = req.body.orgId.toString();
                const groupId = req.body.groupId.toString();
                
                // 24 hours in milliseconds
                let updatedAthlete;

                const athlete: Athlete = await getAthleteDocFromUID(athleteId);
                const group: Group = await getGroupDocFromUID(groupId);
                
                const opType = this.getOnboardingInvitationOperationType(athlete, orgId, groupId);

                switch (opType) {
                    case OnboardingInvitationOperationType.SimpleGroupAddition:
                        updatedAthlete = await this.runSimpleGroupAddition(undefined, athlete, groupId);
                        return res.json({
                            accepted: true,
                            athleteId,
                            groupId, 
                            userName: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
                            groupName: group.groupName 
                        } as OnboardingInvitationResponse);
                    case OnboardingInvitationOperationType.SimpleOrganizationSwap:
                        updatedAthlete = await this.runSimpleOrganizationSwap(undefined, athlete, orgId);
                        return res.json({
                            accepted: true,
                            athleteId,
                            groupId, 
                            userName: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
                            groupName: group.groupName 
                        } as OnboardingInvitationResponse);
                    case OnboardingInvitationOperationType.SimpleOrganizationSwapWithGroupAddition:
                        updatedAthlete = await this.runSimpleOrganizationSwapWithGroupAddition(undefined, athlete, orgId, groupId);
                        return res.json({
                            accepted: true,
                            athleteId,
                            groupId, 
                            userName: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
                            groupName: group.groupName 
                        } as OnboardingInvitationResponse);
                    case OnboardingInvitationOperationType.NewOrganizationSwap:
                        updatedAthlete = await this.runNewOrganizationSwap(undefined, athlete, orgId, groupId);
                        return res.json({
                            accepted: true,
                            athleteId,
                            groupId, 
                            userName: `${athlete.profile.firstName} ${athlete.profile.lastName}`,
                            groupName: group.groupName 
                        } as OnboardingInvitationResponse);
                    default:
                        msg = 'No operation type specified for this request';
                        console.error(msg);

                        return this.handleResponse({
                            message: msg,
                            responseCode: 400,
                            data: undefined,
                            error: {
                                message: msg,
                                exception: undefined
                            },
                            method: {
                                name: 'processInvitation',
                                line: 30,
                            },
                            res
                        });
                }
                
            } else {
                msg = 'ERROR. No AthleteId specified.';
                console.error(msg);

                return this.handleResponse({
                    message: msg,
                    responseCode: 503,
                    data: undefined,
                    error: {
                        message: msg,
                        exception: undefined
                    },
                    method: {
                        name: 'processInvitation',
                        line: 30,
                    },
                    res
                });
            }

        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception);
            // return res.status(505).send(exception);

            const { message, stack } = exception;
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred processInvitationLink',
                responseCode: 500,
                data: undefined,
                error: {
                    message,
                    exception: stack,
                },
                method: {
                    name: 'processInvitationLink',
                    line: 30,
                },
                res,
            });
        }

    }

    getOnboardingInvitationOperationType = (athlete: Athlete, newOrgId: string, newGroupId: string): OnboardingInvitationOperationType => {
        const org = athlete.organizations.find((o: OrganizationIndicator) => {
            return o.organizationId === newOrgId;
        });

        if (!!org && athlete.organizationId === newOrgId) {
            if (isArray(athlete.groups)) {
                const i = athlete.groups.findIndex((g: Group) => {
                    return g.groupId === newGroupId;
                });

                if (i < 0) {
                    return OnboardingInvitationOperationType.SimpleGroupAddition;
                }
            } else {
                return OnboardingInvitationOperationType.SimpleGroupAddition;
            }
        } else {
            if (!!org) {
                const i = org.groups.findIndex((g: Group) => {
                    return g.groupId === newGroupId;
                });

                return (i < 0) ? OnboardingInvitationOperationType.SimpleOrganizationSwapWithGroupAddition : OnboardingInvitationOperationType.SimpleOrganizationSwap;
            } else {
                return OnboardingInvitationOperationType.NewOrganizationSwap;
            }
        }

        return OnboardingInvitationOperationType.Noop;
    }

    runSimpleGroupAddition = async (secDoc: ConsentRequestDocument, athlete: Athlete, newGroupId: string): Promise<Athlete> => {
        const msg = `Attempting to run simple group addition for athlete ${athlete.uid} and group ${newGroupId}`;
        console.log(msg);

        const group: Group = await getGroupDocFromUID(newGroupId);
        if (!!group) {
            return this.addGroupToAthlete(athlete, group)
                .then(async (updatedAthlete) => {
                    return updatedAthlete;
                });
        } else {
            console.error(`Specified group ${newGroupId} does not exist!  Failed to run simple group addition for athlete ${athlete.uid} and group ${newGroupId}`);
            return Promise.resolve(athlete);
        }
    }

    runSimpleOrganizationSwap = async (secDoc: ConsentRequestDocument, athlete: Athlete, newOrgId: string): Promise<Athlete> => {
        const msg = `Attempting to run simple organization swap for athlete ${athlete.uid} and organization ${newOrgId}`;
        console.log(msg);

        const org: OrganizationIndicator = athlete.organizations.find((o: OrganizationIndicator) => {
            return o.organizationId === newOrgId;
        });

        if (!!org) {
            const newOrgDoc: OrganizationInterface = await getOrganizationDocFromUID(newOrgId);
            const athleteEmail = athlete.profile.email;
            if (!!athleteEmail) {
                const orgEmail = newOrgDoc.coaches.find((email: string) => {
                    return athleteEmail.trim().toLowerCase() === email.trim().toLowerCase();
                });

                athlete.isCoach = !!orgEmail;
            }
            const updatedAthlete = await this.activateExistingOrganizationOnAthlete(athlete, org, newOrgDoc);
            if (isArray(org.groups)) {
                for (const g of org.groups) {
                    const group = await getGroupDocFromUID(g.groupId);
                    await this.addAthleteToGroup(updatedAthlete, group);
                }
            }
            return Promise.resolve(updatedAthlete);
        } else {
            console.error(`Specified organization ${newOrgId} does not exist in athlete organizations array!  Failed to run simple organization swap for athlete ${athlete.uid} and organization ${newOrgId}`);
            return Promise.resolve(athlete);
        }
    }

    runSimpleOrganizationSwapWithGroupAddition = async (secDoc: ConsentRequestDocument, athlete: Athlete, newOrgId: string, newGroupId: string): Promise<Athlete> => {
        const msg = `Attempting to run simple organization swap with group addition for athlete ${athlete.uid}, organization ${newGroupId} and group ${newGroupId}`;
        console.log(msg);

        if (!isArray(athlete.organizations)) {
            athlete.organizations = [];
        }

        const org: OrganizationIndicator = athlete.organizations.find((o: OrganizationIndicator) => {
            return o.organizationId === newOrgId;
        });

        const newOrgDoc: OrganizationInterface = await getOrganizationDocFromUID(newOrgId);
        const newGroup: Group = await getGroupDocFromUID(newGroupId);

        if (!!org && !!newGroup) {
            const existingGroup = org.groups.find((g: GroupIndicator) => {
                g.groupId === newGroupId;
            });
            if (!existingGroup) {
                newGroup.organizationId = org.organizationId;
                newGroup.organizationName = org.organizationName || '';
                const groupIndicator = this.getGroupIndicator(newGroup);
                const newGroups = cloneDeep(org.groups);
                newGroups.push(groupIndicator);
                org.groups = newGroups;
            }

            const athleteEmail = athlete.profile.email;
            if (!!athleteEmail) {
                const orgEmail = newOrgDoc.coaches.find((email: string) => {
                    return athleteEmail.trim().toLowerCase() === email.trim().toLowerCase();
                });

                athlete.isCoach = !!orgEmail;
            }

            await this.addAthleteToGroup(athlete, newGroup);

            const updatedAthlete = await this.activateExistingOrganizationOnAthlete(athlete, org, newOrgDoc);
            return Promise.resolve(updatedAthlete);
        } else {
            console.error(`Specified organization ${newOrgId} does not exist in athlete organizations array or group ${newGroup}!  Failed to run simple organization swap for athlete ${athlete.uid} and organization ${newOrgId}`);
            return Promise.resolve(athlete);
        }
    }

    runNewOrganizationSwap = async (secDoc: ConsentRequestDocument, athlete: Athlete, newOrgId: string, newGroupId: string): Promise<Athlete> => {
        const msg = `Attempting to run new organization swap for athlete ${athlete.uid}, organization ${newGroupId} and group ${newGroupId}`;
        console.log(msg);

        const newOrg: OrganizationInterface = await getOrganizationDocFromUID(newOrgId);
        const newGroup: Group = await getGroupDocFromUID(newGroupId);
        if (!!newOrg && !!newGroup) {
            return this.addOrgAndGroupToAthlete(athlete, newOrg, newGroup)
                .then(async (updatedAthlete) => {
                    return updatedAthlete;
                });
        } else {
            console.error(`Specified organization ${newOrgId} or group ${newGroupId} does not exist!  Failed to run new organization swap for athlete ${athlete.uid}, organization ${newOrgId} and group ${newGroupId}`);
            return Promise.resolve(athlete);
        }
    }

    private addOrgAndGroupToAthlete = async (athlete: Athlete, newOrg: OrganizationInterface, group: Group): Promise<Athlete> => {

        let response = await this.manageDataWarehouseSegmentDimension(newOrg.uid, newOrg.name, newOrg.parentOrganizationId, SegmentType.Organization);
        console.log({ response });

        response = await this.manageDataWarehouseAthleteSegmentDimension(newOrg.uid, athlete);
        console.log({ response });

        if (!isArray(athlete.organizations)) {
            athlete.organizations = [];
        }

        const orgIndicator = this.getOrganizationIndicator(newOrg, [group], this.getCurrentAppIdentifier(athlete));
        athlete.organizationId = orgIndicator.organizationId;
        athlete.groups = isArray(orgIndicator.groups) ? cloneDeep(orgIndicator.groups) : [];
        athlete.orgTools = isArray(orgIndicator.orgTools) ? cloneDeep(orgIndicator.orgTools) : [];
        const newOrgs = cloneDeep(athlete.organizations);
        newOrgs.push(orgIndicator);
        athlete.organizations = newOrgs;

        const newOrgDoc: OrganizationInterface = await getOrganizationDocFromUID(newOrg.uid);
        const athleteEmail = athlete.profile.email;
        console.log(`******** TRAVIS DEBUG ATHLETE EMAIL: ${athleteEmail}`);
        if (!!athleteEmail) {
            const orgEmail = newOrgDoc.coaches.find((email: string) => {
                return athleteEmail.trim().toLowerCase() === email.trim().toLowerCase();
            });

            console.log(`******** TRAVIS DEBUG ORG EMAIL FOUND: ${!!orgEmail}`);

            athlete.isCoach = !!orgEmail;
        }

        await this.addAthleteToGroup(athlete, group);

        const updatedAthlete = await this.manageAthleteTheme(athlete, orgIndicator);
        const athleteDocRef = getAthleteDocRef(updatedAthlete.uid);
        return athleteDocRef
            .update(updatedAthlete)
            .then(() => {
                return updatedAthlete;
            })
            .catch((err) => {
                console.error(err);
                return updatedAthlete;
            });
    }

    private addGroupToAthlete = async (athlete: Athlete, group: Group): Promise<Athlete> => {

        let response = await this.manageDataWarehouseSegmentDimension(group.groupId, group.groupName, group.organizationId, SegmentType.Group);
        console.log({ response });

        response = await this.manageDataWarehouseAthleteSegmentDimension(group.groupId, athlete);
        console.log({ response });

        if (!isArray(athlete.groups)) {
            athlete.groups = [];
        }

        const indicator = this.getGroupIndicator(group);
        let newGroups = cloneDeep(athlete.groups);
        newGroups.push(indicator);
        athlete.groups = newGroups;

        if (!isArray(athlete.organizations)) {
            athlete.organizations = [];
        }

        const org = athlete.organizations.find((o: OrganizationIndicator) => {
            return o.organizationId === group.organizationId;
        });

        if (!!org) {
            newGroups = cloneDeep(org.groups);
            newGroups.push(indicator);
            org.groups = newGroups;
        }

        await this.addAthleteToGroup(athlete, group);

        const athleteDocRef = getAthleteDocRef(athlete.uid);
        return athleteDocRef
            .update(athlete)
            .then(() => {
                return athlete;
            })
            .catch((err) => {
                console.error(err);
                return athlete;
            });
    }

    private addAthleteToGroup = async (athlete: Athlete, group: Group): Promise<void> => {
        const indicator = this.getAthleteIndicator(athlete);
        console.log(`******** TRAVIS DEBUG ISCOACH FROM INDICATOR: ${indicator.isCoach}`);
        const subCollPath = `${FirestoreCollection.Groups}/${group.groupId}/athletes`;
        const exists = await db.collection(subCollPath)
            .doc(indicator.uid)
            .get()
            .then((doc) => {
                return (doc.exists);
            })
            .catch((err) => {
                console.error({ err });
                return false;
            });
        if (!exists) {
            console.log(`Athlete ${indicator.uid} does not exist in group ${group.groupId}.  Proceeding to add athlete indicator to group object.`);
            let response = await this.manageDataWarehouseSegmentDimension(group.groupId, group.groupName, group.organizationId, SegmentType.Group);
            console.log({ response });
    
            response = await this.manageDataWarehouseAthleteSegmentDimension(group.groupId, athlete);
            console.log({ response });

            if (!group.athleteIndex) {
                group.athleteIndex = {};
            }
            group.athleteIndex[athlete.uid] = true;
            await db.collection(FirestoreCollection.Groups)
                    .doc(group.groupId)
                    .update(group)
                    .then(() => {
                        return;
                    })
                    .catch((err) => {
                        console.error( { err} );
                        return;
                    });


            return db.collection(subCollPath)
                .doc(indicator.uid)
                .set(indicator)
                .then(() => {
                    return;
                })
                .catch((err) => {
                    console.error({ err });
                    return;
                });
        } else {
            console.log(`Athlete ${indicator.uid} already exists in group ${group.groupId}.`);
            return Promise.resolve();
        }
    }

    private manageDataWarehouseSegmentDimension = async (segmentId: string, segmentName: string, parentSegmentId: string, segmentType: SegmentType): Promise<any> => {
        return manageDataWarehouseSegmentDimension(segmentId, segmentName, parentSegmentId, segmentType);
    }

    private manageDataWarehouseAthleteSegmentDimension = async (segmentId: string, athlete: Athlete): Promise<any> => {
        return manageDataWarehouseAthleteSegmentDimension(segmentId, athlete.uid);
    }

    private activateExistingOrganizationOnAthlete = async (athlete: Athlete, newOrg: OrganizationIndicator, orgDoc: OrganizationInterface): Promise<Athlete> => {
        debugger;
        athlete.organizationId = newOrg.organizationId;
        athlete.groups = isArray(newOrg.groups) ? cloneDeep(newOrg.groups) : [];
        athlete.orgTools = isArray(newOrg.orgTools) ? cloneDeep(newOrg.orgTools) : [];

        const updatedAthlete = await this.manageAthleteTheme(athlete, newOrg);

        let response = await this.manageDataWarehouseSegmentDimension(orgDoc.uid, orgDoc.name, orgDoc.parentOrganizationId, SegmentType.Organization);
        console.log({ response });

        response = await this.manageDataWarehouseAthleteSegmentDimension(orgDoc.uid, athlete);
        console.log({ response });

        for (const group of athlete.groups) {
            response = await this.manageDataWarehouseSegmentDimension(group.groupId, group.groupName, orgDoc.uid, SegmentType.Group);
            console.log({ response });

            response = await this.manageDataWarehouseAthleteSegmentDimension(group.groupId, athlete);
            console.log({ response });
        }

        return db.collection(FirestoreCollection.Athletes)
            .doc(updatedAthlete.uid)
            .update(updatedAthlete)
            .then(() => {
                debugger;
                return updatedAthlete;
            })
            .catch((err) => {
                console.error(`Error on update athlete: ${{ err }}`);
                return updatedAthlete;
            });
    }

    private manageAthleteTheme = async (athlete: Athlete, newOrg: OrganizationIndicator): Promise<Athlete> => {
        let theme: WhiteLabelTheme;

        if (isArray(athlete.themes) && athlete.themes[0].uid === newOrg.themeId) {
            theme = athlete.themes[0] as WhiteLabelTheme;
        }

        if (!theme) {
            const newTheme = await db.collection(FirestoreCollection.WhiteLabelThemes)
                .doc(newOrg.themeId)
                .get()
                .then((doc) => {
                    if (doc.exists) {
                        return doc.data() as WhiteLabelTheme;
                    } else {
                        return undefined;
                    }
                })
                .catch((err) => {
                    console.error(`Error on theme query: ${{ err }}`);
                    return undefined;
                });

            if (!!newTheme) {
                athlete.forceThemeUpdate = true;
                athlete.themeId = newTheme.uid;
                athlete.themes = [newTheme];
            }
        }

        return Promise.resolve(athlete);
    }

    private getCurrentAppIdentifier = (athlete: Athlete): string => {
        const orgId = athlete.organizationId;
        const org = athlete.organizations.find((o: OrganizationIndicator) => {
            return o.organizationId === orgId;
        });

        return (!!org) ? org.appIdentifier : isArray(athlete.organizations) && athlete.organizations.length ? athlete.organizations[0].appIdentifier : '';
    }

    private getOrganizationIndicator = (org: OrganizationInterface, groups: Array<GroupIndicator>, appIdentifier: string): OrganizationIndicator => {
        const indicator = {
            organizationId: org.uid,
            organizationName: org.name || '',
            groups,
            organizationCode: org.organizationCode,
            themeId: org.theme || 'default',
            orgTools: org.orgTools || [],
            appIdentifier
        } as OrganizationIndicator;

        return indicator;
    }

    private getGroupIndicator = (group: Group): GroupIndicator => {
        const indicator = {
            organizationId: group.organizationId,
            organizationName: group.organizationName || '',
            uid: group.uid,
            entityName: group.entityName || '',
            groupId: group.groupId,
            groupName: group.groupName || '',
            creationTimestamp: group.creationTimestamp,
            logoUrl: group.logoUrl || ''
        } as GroupIndicator;

        return indicator;
    }

    private getAthleteIndicator = (athlete: Athlete): AthleteIndicator => {
        return {
            organizationId: athlete.organizationId,
            organizations: athlete.organizations || [],
            uid: athlete.uid,
            firstName: athlete.profile.firstName || '',
            lastName: athlete.profile.lastName || '',
            email: athlete.profile.email || '',
            profileImageURL: athlete.profile.imageUrl || '',
            isCoach: athlete.isCoach || false,
            includeGroupAggregation: athlete.includeGroupAggregation || true,
            ipScore: athlete.currentIpScoreTracking.ipScore || 0,
            runningTotalIpScore: athlete.runningTotalIpScore || 0,
            recentEntries: [],
            subscriptionType: 1,
            firebaseMessagingIds: athlete.firebaseMessagingIds,
            creationTimestamp: athlete.metadata.creationTimestamp,
            orgTools: athlete.orgTools || [],
            bio: athlete.profile.bio || '',
            sport: athlete.profile.sport || ''
        } as AthleteIndicator;
    }

    private invalidateSecurityToken = async (secDoc: ConsentRequestDocument, accepted: boolean, invalid?: boolean): Promise<ConsentRequestDocument> => {
        secDoc.valid = false;
        secDoc.consentResponse = accepted ? ConsentRequestResponse.RequestGranted : (invalid) ? ConsentRequestResponse.RequestInvalid : ConsentRequestResponse.RequestDenied;
        return db.collection(FirestoreCollection.ConsentRequestQueue)
            .doc(secDoc.guid)
            .update(secDoc)
            .then(() => {
                return secDoc;
            })
            .catch((err) => {
                console.error(err);
                return secDoc;
            });
    }
}