import { getAthleteDocFromUID } from './../../../../db/refs/index';
import { Response, Request } from 'express'

import BaseCtrl from './base'
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'
import { db } from '../../../../shared/init/initialise-firebase'
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { Athlete } from '../../../../models/athlete/interfaces/athlete'
import { Survey } from '../../../../db/programs/models/surveys/survey.model'
import { getCollectionDocs } from '../../../../db/refs';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { SurveyResult } from '../../../../db/programs/models/interfaces/Survey-result';
import { groupByMap } from '../../../../shared/utils/groupByMap';

// import csvParser from 'csv-parser';
// const csvParser = require('csv-parser');

const csv = require('csv-parser')
/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class SurveyCtrl
 * @extends {BaseCtrl}
 */
export default class SurveysCtrl extends BaseCtrl {

    res: Response

    // logger: Logger;
    model = Survey
    entries

    collection = FirestoreCollection.Surveys
    aggregateDocumentManager: AggregateDocumentManager

    constructor() {
        super()
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof SurveysCtrl
     */
    getAllSurveys = async (req: Request, res: Response): Promise<any> => {
        const { id } = <{ id: any }>req.params
        console.log(req.params)
        const me = this

        this.res = res

        const snapshot = await db.collection(FirestoreCollection.Surveys).get()
        return res.json(snapshot.docs.map(doc => doc.data()))

        // See the SurveyRecord reference doc for the contents of surveyRecord.
        console.log('...: ', id)

        const docRef = db.collection(FirestoreCollection.Surveys)

        return docRef
            .get()
            .then((querySnap) => {
                if (!querySnap.empty) {
                    return me.handleResponse({
                        message: 'Survey Retrieved',
                        responseCode: 201,
                        data: {
                            surveys: querySnap.docs,
                        },
                        method: {
                            name: 'getSurveys',
                            line: 38,
                        },
                        res: this.res,
                    })
                } else {
                    console.log('No documents found!')
                    return me.handleResponse({
                        message:
                            'Unable to retrieve Surveys - No such documents!',
                        responseCode: 204,
                        method: {
                            name: 'getSurveys',
                            line: 38,
                        },
                        res: this.res,
                    })
                }
            })
            .catch((error) => {
                console.log('Error getting document:', error)
                return me.handleResponse({
                    message: 'Unable to retrieve Surveys - An error occured',
                    responseCode: 500,
                    method: {
                        name: 'getSurveys',
                        line: 38,
                    },
                    error,
                    res: this.res,
                })
            })
    }

    getAllAthleteSurveys = async (req: Request, res: Response): Promise<any> => {

        this.res = res

        const allSurveys = await getAllSurveys()


        const theCompletedSurveys = await getCompletedSurveys();

        const groupedAthleteBiometricEntryDocs = groupByMap(theCompletedSurveys, (theSurvey: SurveyResult) => theSurvey.athleteUid) as Map<string, Array<SurveyResult>>;

        const athletes: Array<Athlete> = await getAthletes(groupedAthleteBiometricEntryDocs);


        const allTheRes = getMappedData(groupedAthleteBiometricEntryDocs, athletes.filter((ath) => ath !== undefined), allSurveys);

        const tada = csv(allTheRes)



        // const theResults = await db.collectionGroup('completedSurveys')
        return {...this.handleResponse({
            message: 'Survey Results Retrieved',
            responseCode: 201,
            data: allTheRes,
            method: {
                name: 'getAllAthleteSurveys',
                line: 99,
            },
            res: this.res,
        }), tada}

    }

    /**
     * Gets a Survey by uid to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof SurveysCtrl
     */
    getSurvey = async (req: Request, res: Response): Promise<any> => {
        const { uid } = <{ uid: any }>req.params
        console.log(req.params)
        const me = this

        this.res = res

        // See the SurveyRecord reference doc for the contents of surveyRecord.
        console.log('...: ', uid)

        const docRef = db.collection(FirestoreCollection.Surveys).doc(uid)

        return docRef
            .get()
            .then(function (doc) {
                if (doc.exists) {
                    console.log('Document data:', doc.data())
                    return me.handleResponse({
                        message: 'Survey Retrieved',
                        responseCode: 201,
                        data: {
                            survey: doc.data(),
                        },
                        method: {
                            name: 'getSurvey',
                            line: 35,
                        },
                        res: this.res,
                    })
                } else {
                    console.log('No such document!')
                    return me.handleResponse({
                        message:
                            'Unable to retrieve Survey - No such document!',
                        responseCode: 204,
                        method: {
                            name: 'getSurvey',
                            line: 35,
                        },
                        res: this.res,
                    })
                }
            })
            .catch(function (error) {
                console.log('Error getting document:', error)
                return me.handleResponse({
                    message: 'Unable to retrieve Survey - An error occured',
                    responseCode: 500,
                    method: {
                        name: 'getSurvey',
                        line: 35,
                    },
                    error,
                    res: this.res,
                })
            })
    }

    /**
     * Adds a Survey to the database
     *
     * @memberof SurveyCtrl
     */
    addSurvey = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        const { document: survey } = <
            {
                document: Survey
            }
            >req.body

        console.log(req.body)
        const me = this

        this.res = res

        return db
            .collection(FirestoreCollection.Surveys)
            .add(survey)
            .then(writeResult => {
                return me.handleResponse({
                    message: 'Survey added',
                    responseCode: 201,
                    data: {
                        survey: survey,
                        writeResult,
                    },
                    error: undefined,
                    method: {
                        name: 'addSurvey',
                        line: 58,
                    },
                    res: this.res,
                })
            })
            .catch(err => {
                return res.status(502).json({
                    message: 'Error adding Survey',
                    responseCode: 201,
                    data: {
                        survey: survey,
                    },
                    error: err,
                    method: {
                        name: 'getSurvey',
                        line: 58,
                    },
                })
            })
    }

    deleteSurvey = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        console.log(req.body)

        this.res = res

        // TODO:
        return true
        // return admin
        //     .auth()
        //     .deleteSurvey(id)
        //     .then(surveyRecord => {
        //         // See the SurveyRecord reference doc for the contents of surveyRecord.
        //         console.log('...: ', surveyRecord.uid)

        //         return me.handleResponse({
        //             message: 'Survey added',
        //             responseCode: 201,
        //             data: {
        //                 survey: surveyRecord,
        //             },
        //             error: undefined,
        //             method: {
        //                 name: 'deleteSurvey',
        //                 line: 129,
        //             },
        //             res: this.res,
        //         })

        //         // return res.status(200).json({ survey: surveyRecord });
        //     })
        //     .catch(function (error) {
        //         console.log('Error creating new survey: ', error)
        //         return res.status(502).json({ error })
        //     })
    }
}
async function getAllSurveys() {
    return await getCollectionDocs('surveys').then((surveysQuerySnap) => {
        return surveysQuerySnap.docs.map((surveyQueryDocSnap) => {
            return getDocumentFromSnapshot(surveyQueryDocSnap) as Survey;
        });
    });
}

async function getCompletedSurveys() {
    const allTheCompletedSurveys = await db.collectionGroup('completedSurveys')
        // .where('songName', '==', 'X')
        .get()
        .then(async (theSurveyResultsQuerySnap) => {
            return await Promise.all(theSurveyResultsQuerySnap.docs.map(async (docSnap) => {
                const surveyResult = getDocumentFromSnapshot(docSnap) as SurveyResult;
                return surveyResult;
            }));
            // return theSurveyResultsQuerySnap
        });
    const theCompletedSurveys = allTheCompletedSurveys.filter((completedSurvey) => completedSurvey.athleteUid !== undefined && completedSurvey.athleteUid !== null);
    return theCompletedSurveys;
}

async function getAthletes(groupedAthleteBiometricEntryDocs: Map<string, SurveyResult[]>) {
    const ahletePromises: Array<Promise<Athlete>> = [];
    groupedAthleteBiometricEntryDocs.forEach(async (completesSurveys, athlteUID) => {
        const surveysWithoutAthleteUID = completesSurveys.filter((survey) => survey.athleteUid === undefined && survey.athleteUid === null);
        surveysWithoutAthleteUID.map((tehSurvbe) => {
            tehSurvbe.athleteUid = athlteUID;
        });
        const selectedAthlete = getAthleteDocFromUID(athlteUID);
        // selectedAthlete = await getAthleteDocFromSnapshot(surveyResult.athleteUid)
        ahletePromises.push(selectedAthlete);
    });
    const athletes: Array<Athlete> = await Promise.all(ahletePromises);
    return athletes;
}

function getMappedData(groupedAthleteBiometricEntryDocs: Map<string, SurveyResult[]>, athletes: Athlete[], allSurveys: Survey[]) {

    // const allTheRes: {
    //     athlteUID: string;
    //     firstName: string;
    //     lastName: string;
    //     groupName: string;
    //     survey: string;
    //     fileGuid: string;
    //     score: number;
    //     completionDate: string;
    //     responses: {
    //         question: string;
    //         answer: string;
    //     }[];
    // }[] = [];

    const allTheRes = [];

    groupedAthleteBiometricEntryDocs.forEach((completesSurveys, athlteUID) => {

        const allRes = [].concat(completesSurveys.map((completedSurvey) => {

            const selectedAthlete = athletes.find((athlete) => athlete !== undefined && athlete.uid === completedSurvey.athleteUid);
            if (!selectedAthlete) {
                if (completedSurvey.athleteUid) {
                    console.log(`No Athlete Found with UID ${completedSurvey.athleteUid} For Survey`, completedSurvey);
                }
                else {
                    console.log('No Athlete Guid For Survey', completedSurvey);
                }
            }
            const survey = allSurveys.find((theSurvey) => theSurvey.docGuid === completedSurvey.docGuid);

            if (!survey) {
                console.log('SURVEY NOT FOUND', completedSurvey.docGuid || 'NO docGuid found either');
            }
            const mappedAnswers = completedSurvey.responses.map((response) => {
                const question = survey ? survey.questions[response.index] : undefined;
                let answer = question && !question['ignore_metadata'] && survey.metadata && survey.metadata.answers
                    ? survey.metadata.answers[+response.value]
                    : question && question.answers
                        ?
                        question.answers[+response.value]
                        : undefined;

                let checkAnswer = false;
                if (!answer && (question && question.answers && question.answers.length && question.answers.length <= +response.value && +response.value < survey.metadata.answers.length)) {
                    answer = survey.metadata.answers[+response.value]
                    checkAnswer = true
                }

                const theMap = {
                    responseValue: response.value,
                    responseIndex: response.index,
                    question: question ? question.text : 'QUESTION NOT FOUND',
                    answer: answer
                        ? answer.text
                        : question && question.answers && !question.answers.length
                            ? 'SURVEY QUESTION HAS NO ANSWERS'
                            : 'SURVEY  HAS NO QUESTIONS',
                    checkAnswer,
                    useMetadata: question && question['ignore_metadata'] !== undefined ? !question['ignore_metadata'] : false,

                };
                return theMap;
            });


            let mappedAnswer = mappedAnswers.shift()

            const theAtleteResults = {
                athlteUID,
                firstName: selectedAthlete ? selectedAthlete.profile.firstName : 'ATHLETE DOC NOT FOUND',
                lastName: selectedAthlete ? selectedAthlete.profile.lastName : 'ATHLETE DOC NOT FOUND',
                groupName: selectedAthlete ? selectedAthlete.groups && selectedAthlete.groups.length ? selectedAthlete.groups[0].groupName || 'UNKNOWN' : 'NOT KNOWN' : 'ATHLETE DOC NOT FOUND',
                survey: survey ? survey.title : 'SURVEY NOT FOUND',
                surveyFileGuid: survey ? survey.fileGuid : 'SURVEY NOT FOUND',
                completedSurveyFileGuid: completedSurvey.fileGuid,
                surveyDocGUID: survey ? survey.docGuid : 'SURVEY NOT FOUND',
                completedSurveyDocGUID: completedSurvey.docGuid,
                score: completedSurvey.score,
                completionDate: completedSurvey.completionDate.toDate().toISOString(),
                ...mappedAnswer,
            };

            const allSurveyResults = [theAtleteResults]
            while (mappedAnswers.length) {
                mappedAnswer = mappedAnswers.shift()
                allSurveyResults.push({
                    ...theAtleteResults,
                    ...mappedAnswer
                })
            }

            return allSurveyResults;
        }));

        allRes.forEach((res) => {
            allTheRes.push(...res);
        })

    });

    return allTheRes;
}

