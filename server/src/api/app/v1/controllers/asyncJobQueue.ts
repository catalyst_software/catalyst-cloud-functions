import BaseCtrl from './base';
import * as uuid from 'uuid';
import { Response, Request } from 'express';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { Athlete } from '../../../../models/athlete/athlete.model';
import { AsyncJobTaskStatusType } from '../../../../db/asyncJobQueue/enums/async-job-task-status-type.enum';
import { AsyncJob } from '../../../../db/asyncJobQueue/interfaces/async-job.interface';
import { AsyncJobTask } from '../../../../db/asyncJobQueue/interfaces/async-job-task.interface';
import { AsyncJobTaskType } from '../../../../db/asyncJobQueue/enums/async-job-task-type.enum';
import { ConvertVideoJobTaskPayload } from '../../../../db/asyncJobQueue/interfaces/async-job-task-payloads.interface';
import { DocumentReference, DocumentSnapshot } from '@google-cloud/firestore';
import { ResponseObject } from '../../../../shared/ResponseObject';

export default class AsyncJobQueueCtrl extends BaseCtrl {
    model = Athlete;
    collection = FirestoreCollection.AsyncJobQueue;
    res: Response;

    constructor() {
        super();
    }

    addTestJob = async (req: Request, res: Response) => {
        this.res = res;

        try {
            const inputPath = req.body.query.inputPath;
            const inputFile = req.body.query.inputFile;
            const outputPath = req.body.query.outputPath;
            const outputFile = req.body.query.outputFile || `${uuid.v4()}.m3u8`;
            const thumbnailOutputPath = req.body.query.thumbnailOutputPath;
            const thumbnailTimeLocation = req.body.query.thumbnailTimeLocation || '00:00:01.000';

            const asyncJob = {
                creatorId: 'testUser',
                status: AsyncJobTaskStatusType.Pending,
                jobTasks: [],
                percentComplete: 0,
                logs: []
            } as AsyncJob;

            asyncJob.jobTasks.push({
                type: AsyncJobTaskType.ConvertVideo,
                status: AsyncJobTaskStatusType.Pending,
                payload: {
                    inputPath,
                    inputFile,
                    outputPath,
                    outputFile,
                    thumbnailOutputPath,
                    thumbnailTimeLocation
                } as ConvertVideoJobTaskPayload,
                percentComplete: 0,
                logs: []
            } as AsyncJobTask);

            return this.db.collection(FirestoreCollection.AsyncJobQueue)
                .add(asyncJob)
                .then((doc: DocumentReference) => {
                    console.log(`Successfully added document ${doc.id} to asyncJobQueue collection.`);

                    // Return response
                    return this.handleResponse({
                        message: 'Successfully added test asyncJob to queue',
                        responseCode: 200,
                        res: this.res
                    } as ResponseObject);
                })
                .catch((err) => {
                    console.error(err);
                    return this.handleResponse({
                        message: `Error: ${JSON.stringify(err)}`,
                        responseCode: 500,
                        res: this.res
                    } as ResponseObject);
                });
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /asyncJobs/addTestJob',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /asyncJobs/addTestDocument',
                    exception: exception,
                },
                method: {
                    name: 'addTestJob',
                    line: 76,
                },
                res
            })
        }
    }

    getTestJob = async (req: Request, res: Response) => {
        this.res = res;

        const { docUid } = req.body;
        try {

            return this.db.collection(FirestoreCollection.AsyncJobQueue)
                .doc(docUid)
                .get()
                .then((doc: DocumentSnapshot) => {
                    console.log(`Successfully added document ${doc.id} to asyncJobQueue collection.`);

                    // Return response
                    return this.handleResponse({
                        message: 'Successfully grabbed doc',
                        data: {
                            ...doc.data(),
                            uid: doc.id
                        },
                        responseCode: 200,
                        res: this.res
                    } as ResponseObject);
                })
                .catch((err) => {
                    console.error(err);
                    return this.handleResponse({
                        message: `Error: ${JSON.stringify(err)}`,
                        responseCode: 500,
                        res: this.res
                    } as ResponseObject);
                });
        } catch (exception) {
            console.log('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /asyncJobs/addTestJob',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /asyncJobs/addTestDocument',
                    exception: exception,
                },
                method: {
                    name: 'addTestJob',
                    line: 76,
                },
                res
            })
        }
    }
}