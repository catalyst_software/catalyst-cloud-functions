import { scheduleEssentialComms, scheduleEssentialCommsTest } from './../callables/schedule-essential-comms';
import { runGetLookupValues } from './../callables/get-lookup-values';
import { NonEngagementConfiguration } from './../../../../models/organization.model';
// import { FieldValue } from '@google-cloud/firestore'
import { firestore } from 'firebase-admin';

import { Group } from './../../../../models/group/interfaces/group';
import { SaveEventDirective } from '../../../../models/purchase/interfaces/save-event-directive'

// import { ProgramCatalogue } from '../../../../db/programs/models/interfaces/program-catalogue'
import { Response, Request } from 'express'
import { isString } from 'util';
// import moment = require('moment')

// const { auth } = require('google-auth-library')

// const Archiver = require('archiver')
// const request = require('request')
// const http = require('http')
// const { JWT } = require('google-auth-library')

// import * as jwt from 'jsonwebtoken'
// import * as fs from 'fs'
// // const keys = require('./jwt.keys.json');
// const { auth } = require('google-auth-library')
// const axios = require('axios')
const globalAny: any = global
globalAny.atob = require('atob')
// const crypto = require('crypto')
// const jws = require('jws')
// const Verifier = require('google-play-purchase-validator')
const Verifier = require('google-play-billing-validator')
const sharp = require('sharp');
// const keys = {
//     type: 'service_account',
//     project_id: 'inspire-1540046634666',
//     private_key_id: '2c7bfe648c9a06a2020f61250b7db8f33d953736',
//     private_key:
//         '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDwv3dnLHbrqO22\nkmL5tt12XeAnBMAKzq9TrE96XvLWoHrOwMahVGiww5w9ABiDxC49PJtQi5HsRwoH\nJPvR7u/Orrz7LllfDWtUP8q8w5pvxBmonakFdWZwlOtta2ihpLGbVhiuECQVwiHD\nXwQHOQdc/ZRalFtezC40SV2j7NQi+AIVbYzqSytnFMfBBlO68vuSRLnrHBA6ZTnU\nwL9+6XQy3sDDlHvyQiut/6wXjSEAznhi/7BAYWBuaFvYEzIvlrguNe1LUz44YotF\nO2qnRnG0eubcVZN80s3+hdfMphgDu5w+1UxjvIMkC8l56rFYwrmCDJNAU/XzTicO\nPY+6wfOxAgMBAAECggEAAXzdawjOT1SC647qB6sGoVWhLfyMh0SRts2ozBPMImFp\ngN+WGqcf4n/C9mmKvT24H27BcvT52ODzgenLmJeGaduQlWjK6ph1olamlRYMXUZz\n9iPLSJpaILX64nk4c09GPbSBgIne/zds5/0tVDIZBo0WnpyuF97hJjeR2ZJ8GR82\nKoVFw4U0Y6qzbrYbx2tW7HGjFBmYZyllF7YSXmBFQU6ZEeZai39H4wT4Jv2PkWIY\nTFu1DgI86o1iHj0oPXjNXaLOzYUhDqvKmvR3WHqR4gFAgAHMbq3S15axwh/PrX8y\nPyAPdbVxfoz3gL2k0gm/4IfsjP0LMQfCPqEfMmcrsQKBgQD7ctVLuhar9u1vyEcF\nBytkv5zEVWhTbaZ7ssD3at7W2fwQ4Ze+ViLcJUV7iXXp0HEEmj50/cLsLtDUqMfS\nlNCaP8SRDEiVT70tBMvNs/ZQt61zPee359Tgk6rnQgjIlQg5GUdtiI5IQLMj5lIk\nPXoHDLPqQ1QI9PeTzvXcheQvRQKBgQD1GwxhjmhjEy2aFNmLrqXfBN6fJ0p9vlWn\nT2gHAU57exyaE6fCIPkkXJJCR3oaJdUey+lsvlNIIlN4QNG1LCE95DgxsDr3alJj\neAP3YBZhGSlrPOWtxN6gCm/lR0CEVX8V09HEdDWv1IQ6TfjoVHHWT4vO+DMDEfb0\nWFUVe23TfQKBgQCTs/vJNPGWaEFHNW1KbdC2qLvMtKLIxxNRfEpPzA5ERN4FiI7b\ndruZd6deDvwdsZyB6J0hmeLDXCi9krt2XoY+5ZBw57dHRgTJChUxqzHYblzL4iRY\nai0pEnUEzUxdjvW6nLfRhrKVjLPuNaAwu5APzg1tYcleTY+U2rCfrmLAxQKBgQD0\nVdFi0uaGlVCpP82ZrDFX/++48h8U+I9EKSlHimrlN1XM1nkuUVZrhAidH8m9lSc9\n+wIn0BcPWoCg6OTkWSiOHRpTlh1CFPAqjjJOALBufPnIGD3RmuZe3eirw/qxsNTu\nOmjrEPmP3ScvRBkz6TJN9KAnY07e6aKn9AJlVtSltQKBgCBAEuV3L9hfpkVP5dvg\nMuPKPN4JibKpqq04rXQAnKxZcTebljTzvkr4ffZrCUdk/rt8dHL4XBaSxrWFOLeq\nq6vliF0PPOKNNrthNV5epIU62WCWISMKpkvNIijR4IYzwjEdYW7s4bMqhrNNGjNW\nHLT+NOTAsrG97X5ApNhYLOk8\n-----END PRIVATE KEY-----\n',
//     client_email:
//         'google-play-api@inspire-1540046634666.iam.gserviceaccount.com',
//     client_id: '106509797390148302867',
//     auth_uri: 'https://accounts.google.com/o/oauth2/auth',
//     token_uri: 'https://oauth2.googleapis.com/token',
//     auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
//     client_x509_cert_url:
//         'https://www.googleapis.com/robot/v1/metadata/x509/google-play-api%40inspire-1540046634666.iam.gserviceaccount.com',
// }

// const { JWT } = require('google-auth-library')

import { ProgramContent } from './../../../../db/programs/models/interfaces/program-content'

import BaseCtrl from './base'
import {
    FirestoreCollection,
    // FirestoreProgramsCollection,
} from '../../../../db/biometricEntries/enums/firestore-collections'
import { liveAdmin } from '../../../../shared/init/initialise-firebase'
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
// import { firestore } from 'firebase-admin'
// import { createTask } from '../../../../db/reports/createTask'
// import { isProductionProject } from '../is-production'
import { General } from './general'
import { SportsNutrition } from './sportsNutrition'
import { SportsPsychology } from './sportsPsychology'
import { SportsScience } from './sportsScience'
import { ProgramIndicator } from '../../../../db/programs/models/interfaces/program-indicator'
import { updateAthleteSubscriptionStatus } from './utils/update-subscription-object-status'
import { IOSNotificationType } from '../../../../models/enums/ios-notification-type'
import {
    SubscriptionStatus,
    SubscriptionType,
} from '../../../../models/enums/enums.model'
import { Athlete } from '../../../../models/athlete/athlete.model'

// import { AppleStoreNotification } from '../../../../models/apple-store-notification/interfaces/apple-store-notification';
import { AppleStoreNotification } from '../../../../models/apple-store-notification/apple-store-notification.model'
import { persistAthleteReceipt } from '../callables/utils/validate-transaction-receipt/persist-athlete-receipt'
import { sendMessage } from "../../../../pubsub/subscriptions/utils/send-message";
import { PushNotificationType } from '../../../../models/enums/push-notification-type'
import { PlatformType } from '../../../../models/purchase/enums/platfom_type'
import { extractIOSData } from './utils/extract-ios-data'
import { setAnalytics } from './utils/send-notification';
import { WellAggregateCollection } from '../../../../db/biometricEntries/enums/firestore-aggregate-collection';
import { GroupWeeklyAggregate } from '../../../../models/documents/well';
import { Organization } from '../../../../models/organization.model';
import { EmailMessagePayload, EmailMessage, OnboardingInviteEmailPayload } from '../sendGrid/conrollers/email';
import { EmailMessageTemplate } from "../sendGrid/templates/email-message-template";
import { getAthleteCoaches } from '../../../../analytics/triggers/utils/get-athlete-coaches';
import { notifyCoaches } from '../callables/utils/comms/notify-coaches';
import { getOrgCollectionRef, getGroupCollectionRef, getAthleteCollectionRef, getAthleteDocRef, getCollectionRef, getAthleteDocFromUID } from '../../../../db/refs';

import { SetBucketMetadataResponse, Storage } from '@google-cloud/storage';

import axios from 'axios';
import { BiometricStatisticsResponse, LifestyleEntryStatisticsResponse } from '../callables/utils/warehouse/olap/interfaces/biometrics-statistics-response.interface';
import { getAthleteBiometricStatisticsFromCubeJs, getSegmentBiometricStatisticsFromCubeJs } from '../callables/get-bq-biometric-statistics-data';
import { OlapGetLifestyleEntryTypeRequest } from '../callables/utils/warehouse/olap/interfaces/olap-get-statistics-request.interface';
import { sendOnboardingInviteComms } from '../callables/utils/comms/send-onboarding-invite-comms';
import { createAthleteIndicator } from '../callables/utils/onboarding/create-athlete-indicator';
import { AthleteIndicator } from '../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { KeyValuePair } from '../../../../models/notifications/interfaces/schedule-notification-payload';
import { OrganizationIndicator } from '../../../../models/athlete/interfaces/athlete';

// const stream = require('stream'),
//     dataStream = new stream.PassThrough(),
//     gcFile = cloudStorage.bucket(bucketName).file(fileName)

export const emailMessageDefault: EmailMessage = {
    personalizations: [
        {
            to: [
                {
                    name: "Ian Kaapisoft",
                    email: "ian.gouws@kaapisoft.com"
                },
                {
                    name: "Ian iNSPIRE",
                    email: "ian@inspiresportonline.com"
                }
            ]
        }
    ],
    from: {
        name: "iNSPIRE",
        email: "athleteservices@inspiresportonline.com"
    },
    templateId: "d-93756f51e55b4734a5b93d23e4310750",
    emailTemplateName: "badMood",
    // dynamic_template_data: {

    //     name: '',

    //     // subject: "Encoding bit's!!!",
    //     // message: "Mail TGemplate Update>",

    //     showDownloadButton: false,
    //     barType: {
    //         danger: true
    //     }
    // }
}


const processImage = (imageUrl) => {
    return new Promise((resolve, reject) => {

        // Your Google Cloud Platform project ID
        const projectId = 'inspire-1540046634666';

        // Creates a client
        const thestorage = new Storage({
            projectId,
            // clientEmail:
            //     'firebase-adminsdk-yabfi@inspire-1540046634666.iam.gserviceaccount.com',
            // privateKey:
            //     '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDEa8dBKl8pFlrH\nEtlOFu5Pajeg6dhmCOKwISfdO2WYEaWtM80kzGxrxfkw+ePJO4+WzigOm9FcSwhk\nMTlJovU4S1G1vP/tF3uAeNstqgQiENnDuQtbFb2FzhUFMXptkcTfBuoe2vmyn4pk\neBTn63HOUmaD6doU8HtikdQsb5uaJJCpH5VKaPCPAYtLelV/h/8eAu+1dQcoShEY\noY808KXJzzwtWnLHUBnb/s7iUQUVfWH1JP5OTujkxdM7B11KRB3JX9PsexS/+tVJ\niZVbxPIr5CzmM+fjKD/Gico/07IqyloPYzBZ0CSaTRJrEtpiebWXBMHPODsHsI9p\na1KZdkwbAgMBAAECggEAC6WQZQ7MIbWPoGo/tF+rtc3IiqQTDsnMF0GACaAx//hb\n6I8/xMTSUPmmMv8+QHploz2KJoOawGw6jSZWDUW6YKImsC4KxtYznoSMCoMgR5zU\nLMTWJYp+eGal3G74oXKZR9gzHa1CTRMks5xjMPVHBELijUPaiI9R4aBgAlqD0Xnq\n1buKaF9Mvtha3jvHsrWk0mJeyoR5SsQ5o718RVJ5k/xuS4/nc5uOz33SG/xxKyBa\nMuFomisEP92QpTbksut87/FlsQd7XgnPkp+r7UL9olGd8gByII3islpsmEhgrOPT\nmnGpCBjD9pzifTUMQLcZKCmnymfRFRevv3CS5Gk+cQKBgQD4rY8XU/IMoFPiE6FD\nbvHv8+2b0P54uka7GN49Yx7riw73EG9SmgZCTT6V2jhlxXv8kV023085RTXQdPCr\ng0s2kvIJxU4quiZeJBquG1Scji2DSlMKv9tOZNswb4K2x+ZSDZ9GTvfXj/09NDnk\n2jJMRp8K826bI7RwHp03F90+ZwKBgQDKNFN68CjRWGe3zd/LaW6yAwFa8t9yJwdk\nwgcKfKgIm+a5UkQgE8sQ7wWvGMrxSuYJFH1fHIuVFRmpQsJgfydKVHJs16ZdvDun\nvDXy6psYhvggm9pEf+xqct4702oVhaQY/Ee0K6pQgcH966VYwmz4USJj+woWJYxW\nr9cLf76MLQKBgQDP6QPOhC/F4LHhPXpBj9uVO8L32Dc5prwGJ1d/yYSLd/ruE36P\neBkti7l8vjMS25a65qohe3iYMEY639pr+1yB5z+Xba/Zx0LWyKbJ1C3cqn5g214s\niZWIqIgdqc2GlgD5r0vwE4vhXRBkAGs67DbLUOwd0sMx0BtG9kGJU1l1lwKBgQDE\nrCCGcxFAjbxUCuqh7uq8OjAXRiQP4+ZNGmu+x4Co3vqLRnj8ukPJNLNSm8rI5xDX\nxBYtbJZXay6Kc2ScdxDAO2MQerBWe7+KZoYSwB4avSyaivzBo6tP3mpJxlholpQF\nuVwE4nPF2m/Vil5I9tMGs+O/W210HRFjP6TqilXMAQKBgGfbZaGFD3xqSYt+3dA+\nyF7w2/4Bj8xzBmKDQQj2NPan/5V1cWXxGfXNqLDgmxqzsJ+cD6yhPd6t55l2yIcV\nuEWg8XhOq3l0jU+Fh8hD0lHqeFH7yxpp2ymzMjCg3nbq90h9fZqNvHzZSI2cUoCJ\n8UAojg1vDxolRkag4gmGVa7s\n-----END PRIVATE KEY-----\n',
        });

        // Configure axios to receive a response type of stream, and get a readableStream of the image from the specified URL
        axios({
            method: 'get',
            url: imageUrl,
            responseType: 'stream'
        })
            .then((response) => {

                // Create the image manipulation function
                const transformer = sharp()
                    .resize(300)
                    .jpeg();

                const gcFile = thestorage.bucket('INSPIRE/images/').file(`my-file${new Date().getTime()}.jpg`)

                // Pipe the axios response data through the image transformer and to Google Cloud
                response.data
                    .pipe(transformer)
                    .pipe(gcFile.createWriteStream({
                        resumable: false,
                        validation: false,
                        contentType: "auto",
                        metadata: {
                            'Cache-Control': 'public, max-age=31536000'
                        }
                    }))
                    .on('error', (error) => {
                        reject(error)
                    })
                    .on('finish', () => {
                        resolve(true)
                    });
            })
            .catch(err => {
                debugger;
                reject("Image transfer error. " + err);
            });
    })
}

const initeEmailPayload = {
    "taskID": "yABNgMHj3xblKojC20bV",
    "organizationID": "CwfFQiT6wSjAxg3EABSg",
    "organizationName": "GS Org 1",
    "groupID": "CwfFQiT6wSjAxg3EABSg-G1",
    "groupName": "GSTEST1-G1",
    "athleteID": "MOoENLBiHtdhFbnnFZAKwemJr2B3",
    "actionUrl": "https://us-central1-inspire-1540046634666.cloudfunctions.net/api/app/v1/onboarding/invite/",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVmVyaWZpZWQiOnRydWUsImNyZWF0aW9uVGltZXN0YW1wIjoxNjA0MTU2Mzc3NjkwLCJndWlkIjoiYzhmM2FiMDctZWEyYy00MTczLWFmZTItZDE0ZDI3YzQzODcxIiwidWlkIjoiTU9vRU5MQmlIdGRoRmJubkZaQUt3ZW1KcjJCMyJ9.2-CX2ks1VSRGzbRclEjapig-FlxcF65Y9u2Bs4nygF8",
    "sendPushNotification": false
}

const initeEmailPayloadUpdate = {
    "payload": {
        "taskID": "yABNgMHj3xblKojC20bV",
        "organizationID": "CwfFQiT6wSjAxg3EABSg",
        "organizationName": "GS Org 1",
        "groupID": "CwfFQiT6wSjAxg3EABSg-G1",
        "groupName": "GSTEST1-G1",
        "athleteID": "MOoENLBiHtdhFbnnFZAKwemJr2B3",
        "actionUrl": "https://us-central1-inspire-1540046634666.cloudfunctions.net/api/app/v1/onboarding/invite/",
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVmVyaWZpZWQiOnRydWUsImNyZWF0aW9uVGltZXN0YW1wIjoxNjA0MTU2Mzc3NjkwLCJndWlkIjoiYzhmM2FiMDctZWEyYy00MTczLWFmZTItZDE0ZDI3YzQzODcxIiwidWlkIjoiTU9vRU5MQmlIdGRoRmJubkZaQUt3ZW1KcjJCMyJ9.2-CX2ks1VSRGzbRclEjapig-FlxcF65Y9u2Bs4nygF8",
        "sendPushNotification": true,
        "isOnboarding": true,
    },
    "emailMessageTemplate": {
        "includePushNotification": true,
        "notificationTemplateType": 23,
        "notificationTemplate": "groupInvite",
        "notificationPath": "/jind/jindfatigue/wqtIJNDY7aPaRp0qhOVqpLaQRpr1",
        "emailTemplateName": "groupInvite",
        "templateId": "d-de993ceb05174e69aec4b236e34fd8ac"
    },
    "templateReplacements": [
        {
            "email": [],
            "notifications": [
                {
                    "value": "wqtIJNDY7aPaRp0qhOVqpLaQRpr1",
                    "key": "$USER_ID"
                },
                {
                    "value": "Test",
                    "key": "$USER_FIRST_NAME"
                },
                {
                    "value": "Test Bob",
                    "key": "$USER_FULL_NAME"
                },
                {
                    "value": "HAS4gsdJsYAgmPjPhJyC",
                    "key": "$ORGANIZATION_ID"
                },
                {
                    "value": "C67A5Y-G1",
                    "key": "$USER_ONBOARDING_CODE"
                },
                {
                    "value": "HAS4gsdJsYAgmPjPhJyC-G1",
                    "key": "$GROUP_1_ID"
                },
                {
                    "value": "GSTEST1-G1",
                    "key": "$GROUP_NAME"
                },
                {
                    "value": [
                        "HAS4gsdJsYAgmPjPhJyC-G1"
                    ],
                    "key": "$GROUP_IDS"
                },
                {
                    "value": "BIOMETRICS",
                    "key": "$ANALYTICS_FAMILY"
                },
                {
                    "value": 5,
                    "key": "$VALUE"
                }
            ]
        }
    ],
    "key": "BIOMETRICS_FATIGUE_EXTREME"
}

const initeEmailPayloadFull = {
    "payload": {
        //    "organizationID": [
        //       "HAS4gsdJsYAgmPjPhJyC",
        //       "CwfFQiT6wSjAxg3EABSg"
        //    ],
        "userOnboardingCode": "C67A5Y-G1",
        "groupIDs": [
            "HAS4gsdJsYAgmPjPhJyC-G1"
        ],
        "userFullName": "Test Bob",
        "userFirstName": "Test",
        "analyticsFamily": "BIOMETRICS",
        "userID": "MOoENLBiHtdhFbnnFZAKwemJr2B3",
        "value": "5",
        "isOnboarding": true,

        "subject": "You're invited to join group GSTEST1-G1",
        "taskID": "yABNgMHj3xblKojC20bV",
        "organizationID": "CwfFQiT6wSjAxg3EABSg",
        "organizationName": "GS Org 1",
        "groupID": "CwfFQiT6wSjAxg3EABSg-G1",
        "groupName": "GSTEST1-G1",
        "athleteID": "MOoENLBiHtdhFbnnFZAKwemJr2B3",
        "actionUrl": "https://us-central1-inspire-1540046634666.cloudfunctions.net/api/app/v1/onboarding/invite/",
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVmVyaWZpZWQiOnRydWUsImNyZWF0aW9uVGltZXN0YW1wIjoxNjA0MTU2Mzc3NjkwLCJndWlkIjoiYzhmM2FiMDctZWEyYy00MTczLWFmZTItZDE0ZDI3YzQzODcxIiwidWlkIjoiTU9vRU5MQmlIdGRoRmJubkZaQUt3ZW1KcjJCMyJ9.2-CX2ks1VSRGzbRclEjapig-FlxcF65Y9u2Bs4nygF8",
        "sendPushNotification": true,



    },
    "emailMessageTemplate": {
        "includePushNotification": true,
        "notificationTemplateType": 23,
        "notificationTemplate": "groupInvite",
        "notificationPath": "/jind/jindfatigue/wqtIJNDY7aPaRp0qhOVqpLaQRpr1",
        "emailTemplateName": "groupInvite",
        "templateId": "d-de993ceb05174e69aec4b236e34fd8ac"
    },
    "templateReplacements": [
        {
            "email": [],
            "notifications": [
                {
                    "value": "wqtIJNDY7aPaRp0qhOVqpLaQRpr1",
                    "key": "$USER_ID"
                },
                {
                    "value": "Test",
                    "key": "$USER_FIRST_NAME"
                },
                {
                    "value": "Test Bob",
                    "key": "$USER_FULL_NAME"
                },
                {
                    "value": "HAS4gsdJsYAgmPjPhJyC",
                    "key": "$ORGANIZATION_ID"
                },
                {
                    "value": "C67A5Y-G1",
                    "key": "$USER_ONBOARDING_CODE"
                },
                {
                    "value": "HAS4gsdJsYAgmPjPhJyC-G1",
                    "key": "$GROUP_1_ID"
                },
                {
                    "value": "GSTEST1-G1",
                    "key": "$GROUP_NAME"
                },
                {
                    "value": [
                        "HAS4gsdJsYAgmPjPhJyC-G1"
                    ],
                    "key": "$GROUP_IDS"
                },
                {
                    "value": "BIOMETRICS",
                    "key": "$ANALYTICS_FAMILY"
                },
                {
                    "value": 5,
                    "key": "$VALUE"
                }
            ]
        }
    ],
    "key": "BIOMETRICS_FATIGUE_EXTREME"
}

const setCors = async (bucketName, responseHeader, method, maxAgeSeconds, origins) => {


    // Your Google Cloud Platform project ID
    const projectId = 'inspire-1540046634666';

    // Creates a client
    const thestorage: Storage = new Storage({
        projectId,
        // clientEmail:
        //     'firebase-adminsdk-yabfi@inspire-1540046634666.iam.gserviceaccount.com',
        // privateKey:
        //     '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDEa8dBKl8pFlrH\nEtlOFu5Pajeg6dhmCOKwISfdO2WYEaWtM80kzGxrxfkw+ePJO4+WzigOm9FcSwhk\nMTlJovU4S1G1vP/tF3uAeNstqgQiENnDuQtbFb2FzhUFMXptkcTfBuoe2vmyn4pk\neBTn63HOUmaD6doU8HtikdQsb5uaJJCpH5VKaPCPAYtLelV/h/8eAu+1dQcoShEY\noY808KXJzzwtWnLHUBnb/s7iUQUVfWH1JP5OTujkxdM7B11KRB3JX9PsexS/+tVJ\niZVbxPIr5CzmM+fjKD/Gico/07IqyloPYzBZ0CSaTRJrEtpiebWXBMHPODsHsI9p\na1KZdkwbAgMBAAECggEAC6WQZQ7MIbWPoGo/tF+rtc3IiqQTDsnMF0GACaAx//hb\n6I8/xMTSUPmmMv8+QHploz2KJoOawGw6jSZWDUW6YKImsC4KxtYznoSMCoMgR5zU\nLMTWJYp+eGal3G74oXKZR9gzHa1CTRMks5xjMPVHBELijUPaiI9R4aBgAlqD0Xnq\n1buKaF9Mvtha3jvHsrWk0mJeyoR5SsQ5o718RVJ5k/xuS4/nc5uOz33SG/xxKyBa\nMuFomisEP92QpTbksut87/FlsQd7XgnPkp+r7UL9olGd8gByII3islpsmEhgrOPT\nmnGpCBjD9pzifTUMQLcZKCmnymfRFRevv3CS5Gk+cQKBgQD4rY8XU/IMoFPiE6FD\nbvHv8+2b0P54uka7GN49Yx7riw73EG9SmgZCTT6V2jhlxXv8kV023085RTXQdPCr\ng0s2kvIJxU4quiZeJBquG1Scji2DSlMKv9tOZNswb4K2x+ZSDZ9GTvfXj/09NDnk\n2jJMRp8K826bI7RwHp03F90+ZwKBgQDKNFN68CjRWGe3zd/LaW6yAwFa8t9yJwdk\nwgcKfKgIm+a5UkQgE8sQ7wWvGMrxSuYJFH1fHIuVFRmpQsJgfydKVHJs16ZdvDun\nvDXy6psYhvggm9pEf+xqct4702oVhaQY/Ee0K6pQgcH966VYwmz4USJj+woWJYxW\nr9cLf76MLQKBgQDP6QPOhC/F4LHhPXpBj9uVO8L32Dc5prwGJ1d/yYSLd/ruE36P\neBkti7l8vjMS25a65qohe3iYMEY639pr+1yB5z+Xba/Zx0LWyKbJ1C3cqn5g214s\niZWIqIgdqc2GlgD5r0vwE4vhXRBkAGs67DbLUOwd0sMx0BtG9kGJU1l1lwKBgQDE\nrCCGcxFAjbxUCuqh7uq8OjAXRiQP4+ZNGmu+x4Co3vqLRnj8ukPJNLNSm8rI5xDX\nxBYtbJZXay6Kc2ScdxDAO2MQerBWe7+KZoYSwB4avSyaivzBo6tP3mpJxlholpQF\nuVwE4nPF2m/Vil5I9tMGs+O/W210HRFjP6TqilXMAQKBgGfbZaGFD3xqSYt+3dA+\nyF7w2/4Bj8xzBmKDQQj2NPan/5V1cWXxGfXNqLDgmxqzsJ+cD6yhPd6t55l2yIcV\nuEWg8XhOq3l0jU+Fh8hD0lHqeFH7yxpp2ymzMjCg3nbq90h9fZqNvHzZSI2cUoCJ\n8UAojg1vDxolRkag4gmGVa7s\n-----END PRIVATE KEY-----\n',
    });

    // const bucketName = 'INSPIRE'; // 'Name of a bucket, e.g. my-bucket';
    // const maxAgeSeconds = 3600; // 'The maximum amount of time the browser can make requests before
    // it must repeat preflighted requests, e.g. 3600';
    // const method = 'PUT,POST,GET';
    const origin = 'https://growth-studio-ian-dot-inspire-1540046634666.appspot.com';
    // const responseHeader = '*'; // 'response header to share across the origins e.g. content-type';
    // const responseHeader = 'Access-Control-Allow-Headers,Origin, X-Requested-With, Content-Type, Accept'; // 'response header to share across the origins e.g. content-type';

    // res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888')

    // res.header('Access-Control-Allow-Origin', '*')
    // res.header(
    //     'Access-Control-Allow-Headers',
    //     'Origin, X-Requested-With, Content-Type, Accept'
    // )

    // res.setHeader('Access-Control-Allow-Origin', '*')
    // res.setHeader('Access-Control-Request-Method', '*')
    // res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET')
    // res.setHeader('Access-Control-Allow-Headers', '*')

    return await thestorage.bucket(bucketName).setCorsConfiguration([
        {
            maxAgeSeconds,
            method: [method],
            // origin: [origin, 'http://localhost:4200'],
            // origin: [origin],
            origin: [origin, 'http://localhost:4200'],
            responseHeader: [responseHeader],
        },
    ])
        .then((setBucketMetadataResponse: SetBucketMetadataResponse) => {
            console.log({ setBucketMetadataResponse })


            const resp = {
                "setBucketMetadataResponse": [
                    {
                        "kind": "storage#bucket",
                        "selfLink": "https://www.googleapis.com/storage/v1/b/inspire-1540046634666.appspot.com",
                        "id": "inspire-1540046634666.appspot.com",
                        "name": "inspire-1540046634666.appspot.com",
                        "projectNumber": "671901850733",
                        "metageneration": "4",
                        "location": "US",
                        "storageClass": "STANDARD",
                        "etag": "CAQ=",
                        "timeCreated": "2019-01-24T14:36:48.387Z",
                        "updated": "2020-10-28T19:53:39.701Z",
                        "acl": [
                            {
                                "kind": "storage#bucketAccessControl",
                                "id": "inspire-1540046634666.appspot.com/project-editors-671901850733",
                                "selfLink": "https://www.googleapis.com/storage/v1/b/inspire-1540046634666.appspot.com/acl/project-editors-671901850733",
                                "bucket": "inspire-1540046634666.appspot.com",
                                "entity": "project-editors-671901850733",
                                "role": "OWNER",
                                "etag": "CAQ=",
                                "projectTeam": {
                                    "projectNumber": "671901850733",
                                    "team": "editors"
                                }
                            },
                            {
                                "kind": "storage#bucketAccessControl",
                                "id": "inspire-1540046634666.appspot.com/project-owners-671901850733",
                                "selfLink": "https://www.googleapis.com/storage/v1/b/inspire-1540046634666.appspot.com/acl/project-owners-671901850733",
                                "bucket": "inspire-1540046634666.appspot.com",
                                "entity": "project-owners-671901850733",
                                "role": "OWNER",
                                "etag": "CAQ=",
                                "projectTeam": {
                                    "projectNumber": "671901850733",
                                    "team": "owners"
                                }
                            },
                            {
                                "kind": "storage#bucketAccessControl",
                                "id": "inspire-1540046634666.appspot.com/project-viewers-671901850733",
                                "selfLink": "https://www.googleapis.com/storage/v1/b/inspire-1540046634666.appspot.com/acl/project-viewers-671901850733",
                                "bucket": "inspire-1540046634666.appspot.com",
                                "entity": "project-viewers-671901850733",
                                "role": "READER",
                                "etag": "CAQ=",
                                "projectTeam": {
                                    "projectNumber": "671901850733",
                                    "team": "viewers"
                                }
                            }
                        ],
                        "defaultObjectAcl": [
                            {
                                "kind": "storage#objectAccessControl",
                                "entity": "project-owners-671901850733",
                                "role": "OWNER",
                                "etag": "CAQ=",
                                "projectTeam": {
                                    "projectNumber": "671901850733",
                                    "team": "owners"
                                }
                            },
                            {
                                "kind": "storage#objectAccessControl",
                                "entity": "project-editors-671901850733",
                                "role": "OWNER",
                                "etag": "CAQ=",
                                "projectTeam": {
                                    "projectNumber": "671901850733",
                                    "team": "editors"
                                }
                            },
                            {
                                "kind": "storage#objectAccessControl",
                                "entity": "project-viewers-671901850733",
                                "role": "READER",
                                "etag": "CAQ=",
                                "projectTeam": {
                                    "projectNumber": "671901850733",
                                    "team": "viewers"
                                }
                            }
                        ],
                        "owner": {
                            "entity": "project-owners-671901850733"
                        },
                        "cors": [
                            {
                                "origin": [
                                    "https://growth-studio-ian-dot-inspire-1540046634666.appspot.com",
                                    "http://localhost:4200"
                                ],
                                "method": [
                                    "[PUT,POST,GET]"
                                ],
                                "responseHeader": [
                                    "content-type"
                                ],
                                "maxAgeSeconds": 3600
                            }
                        ],
                        "iamConfiguration": {
                            "bucketPolicyOnly": {
                                "enabled": false
                            },
                            "uniformBucketLevelAccess": {
                                "enabled": false
                            }
                        },
                        "locationType": "multi-region"
                    },
                    {
                        "headers": {
                            "alt-svc": "h3-Q050=\":443\"; ma=2592000,h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-T050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"",
                            "cache-control": "no-cache, no-store, max-age=0, must-revalidate",
                            "content-length": "3283",
                            "content-type": "application/json; charset=UTF-8",
                            "date": "Wed, 28 Oct 2020 19:53:39 GMT",
                            "etag": "CAQ=",
                            "expires": "Mon, 01 Jan 1990 00:00:00 GMT",
                            "pragma": "no-cache",
                            "server": "UploadServer",
                            "vary": "Origin, X-Origin",
                            "x-guploader-uploadid": "ABg5-UxVfiVDWUXCZz4QtZytPRlFnU1a5nMqNozS2kCJapNE_4e9PKkb0NU9UASD_4WpuaK3RgIetbjhtqq2ETQXF_E"
                        }
                    }
                ]
            }
            console.log(`Bucket ${bucketName} was updated with a CORS config to allow ${method} requests from ${origin} sharing  ${responseHeader} responses across origins`);
            return setBucketMetadataResponse
        })
        .catch((e) => {
            console.log({ e })

            throw e;
        });



}
/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class TestCtrl
 * @extends {BaseCtrl}
 */
export default class TestsCtrl extends BaseCtrl {

    res: Response

    // logger: Logger;
    model = General
    sportsNutritionModel = SportsNutrition
    sportsPsychologyModel = SportsPsychology
    sportsScienceModel = SportsScience
    entries

    collection = FirestoreCollection.Tests
    aggregateDocumentManager: AggregateDocumentManager

    constructor() {
        super()

    }

    setStorageCors = async (req: Request, res: Response): Promise<any> => {

        const { bucketName, responseHeader, method, maxAgeSeconds, origins } = req.body
        setCors(bucketName, responseHeader, method, maxAgeSeconds, origins)
            .then(result => {
                console.log("Complete.", result);

                return res.status(200).send({ message: 'CORS Set', result })
            })
            .catch(err => {
                console.log("Error", err);
                return res.status(500).send({ message: 'CORS NOT Set', err })
            });

    }


    downloadFromURL = async (req: Request, res: Response): Promise<any> => {

        const { imageURL } = req.body

        console.log('downloadFromURL called', { imageURL })
        return res.status(200).json({ imageURL })
        // return processImage(imageURL)
        //     .then(result => {
        //         console.log("Complete.", result)
        //         return this.res.status(200).json(result)
        //     })
        //     .catch(err => {
        //         console.log("Error", err);
        //         return this.res.status(500).json(err)
        //     });
        // // // const athleteGroups =

        // // // Create a root reference
        // // var storageRef = firebase.storage().ref();

        // // // Create a reference to 'mountains.jpg'
        // // var mountainsRef = storageRef.child('mountains.jpg');

        // // // Create a reference to 'images/mountains.jpg'
        // // var mountainImagesRef = storageRef.child('images/mountains.jpg');

        // // // While the file names are the same, the references point to different files
        // // mountainsRef.name === mountainImagesRef.name            // true
        // // mountainsRef.fullPath === mountainImagesRef.fullPath


        // // // Upload file and metadata to the object 'images/mountains.jpg'
        // // var uploadTask = storageRef.child('images/' + file.name).put(file, metadata);

        // // // Listen for state changes, errors, and completion of the upload.
        // // uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        // //     function (snapshot) {
        // //         // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        // //         var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        // //         console.log('Upload is ' + progress + '% done');
        // //         switch (snapshot.state) {
        // //             case firebase.storage.TaskState.PAUSED: // or 'paused'
        // //                 console.log('Upload is paused');
        // //                 break;
        // //             case firebase.storage.TaskState.RUNNING: // or 'running'
        // //                 console.log('Upload is running');
        // //                 break;
        // //         }
        // //     }, function (error) {

        // //         // A full list of error codes is available at
        // //         // https://firebase.google.com/docs/storage/web/handle-errors
        // //         switch (error.code) {
        // //             case 'storage/unauthorized':
        // //                 // User doesn't have permission to access the object
        // //                 break;

        // //             case 'storage/canceled':
        // //                 // User canceled the upload
        // //                 break;



        // //             case 'storage/unknown':
        // //                 // Unknown error occurred, inspect error.serverResponse
        // //                 break;
        // //         }
        // //     }, function () {
        // //         // Upload completed successfully, now we can get the download URL
        // //         uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
        // //             console.log('File available at', downloadURL);
        // //         });
        // //     });

    }


    emailCallable = async (req: Request, res: Response): Promise<any> => {
        // const athleteGroups =

        const { emailMessageTemplate, payload, templateReplacements = { email: [], notifications: [] } }
            = <{
                emailMessageTemplate: EmailMessageTemplate,
                templateReplacements: {
                    email: KeyValuePair[],
                    notifications: KeyValuePair[]
                },
                payload: EmailMessagePayload
            }>req.body

        const { athleteID, userOnboardingCode, organizationID, groupID, isOnboarding, userFirstName, userFullName, groupName, sendPushNotification } = payload;

        if (isOnboarding) {
            const ath = await getAthleteDocFromUID(athleteID)
            const org = ath.organizations[0]

            const userToEmail: AthleteIndicator = this.createathleteIndicator(ath, org)

            const theUserFullName = userFullName || `${userToEmail.firstName} ${userToEmail.lastName}`;

            const onboardingPayload: OnboardingInviteEmailPayload = {
                ...payload,
                userFirstName: userFirstName || userToEmail.firstName,
                userFullName: userFullName || theUserFullName,
            }

            if (userToEmail) {

                setNotificationTemplateOptions(templateReplacements, groupName);

                const theEmailMessageTemplate = setEmailMessageTemplateOptions(emailMessageTemplate, sendPushNotification);

                const response = await sendOnboardingInviteComms(
                    onboardingPayload,
                    theEmailMessageTemplate,
                    templateReplacements,
                    userToEmail,
                    athleteID,
                    theUserFullName,
                    userToEmail.firstName
                )

                console.log('the scheduleEssentialComms Result', response)

                return res.status(200).json(response)

            }
            else {
                console.warn(`No Coach found for Athlete having uid ${athleteID}`)

                return false
            }

        } else {

            const theGroupIds: Array<string> = [groupID] // TODO:
            const coachesToEmailMap = await getAthleteCoaches(athleteID, organizationID, theGroupIds, userOnboardingCode)
                .catch((ex) => {
                    console.log('...............ex', ex)
                    throw ex
                });

            let allowComms = true
            if (allowComms && coachesToEmailMap.length) {


                const orgConfigs = [].concat(...coachesToEmailMap.map((confSet) => confSet.orgConfig)) as Array<{
                    orgID: string;
                    nonEngagementConfiguration: NonEngagementConfiguration;
                }>
                const coachesToEmail = coachesToEmailMap.filter((coach) => coach.coachRest.uid !== undefined && !coach.skipComms).map((oeg) => oeg.coachRest)


                // return await notifyCoaches(emailMessageTemplate, coachesToEmail, athleteUID, userFullName, userFirstName);

                const response = await notifyCoaches(emailMessageTemplate, templateReplacements.notifications, coachesToEmail, athleteID, userFullName, userFirstName, orgConfigs);

                return await Promise.all(response).then((docs) => {
                    console.log('the scheduleEssentialComms Result', docs)

                    return res.status(200).json(docs)
                    // return true
                })

                // const response = await notifyCoaches(emailMessageTemplate, coachesToEmail.map((grp) => grp.coachRest), athleteUID, userFullName, userFirstName, [].concat(coachesToEmail.map((grp) => grp.orgConfig)))

                // return await Promise.all(response).then((docs) => {
                //     console.log('theResult', docs)

                //     return res.status(200).json(docs)
                // })

            }
            else {
                console.warn(`No Coach found for Athlete having uid ${athleteID}`);

                return false
            }

        }

    }


    scheduleEssentialCommsTest = async (req: Request, res: Response): Promise<any> => {
        debugger;
        const body = req.body;
        const result = await scheduleEssentialCommsTest(req.body);

        return res.status(200).json({});
    }

    private createathleteIndicator(ath: Athlete, org: OrganizationIndicator): AthleteIndicator {
        return {
            uid: ath.uid,
            firstName: ath.profile.firstName,
            lastName: ath.profile.lastName,
            creationTimestamp: ath.metadata.creationTimestamp,
            isIpScoreIncrease: false,
            ipScore: ath.currentIpScoreTracking.ipScore,
            isIpScoreDecrease: false,
            email: ath.profile.email,
            profileImageURL: ath.profileImageURL || ath.profile.imageUrl || '',
            bio: ath.profile.bio || ath.profile.sport || 'Athlete',
            runningTotalIpScore: ath.runningTotalIpScore,
            subscriptionType: ath.profile.subscription.type,
            orgTools: org.orgTools || ath.orgTools || [],
            sport: ath.profile.sport || '',
            includeGroupAggregation: false,
            organizationId: org.organizationId,
            isCoach: false,
            firebaseMessagingIds: ath.firebaseMessagingIds
        };
    }

    async fixIsCoach(res: Response): Promise<any> {
        const allOrgs = await getOrgCollectionRef()
            .get().then((querySnap) => {
                const orgs = querySnap.docs.map((queryDocSnap) => {
                    const org = {
                        uid: queryDocSnap.id, ...queryDocSnap.data() as Organization
                    }
                    return org
                })

                return orgs

            });

        const updates = allOrgs.map(async (org) => {
            const orgGroups = await getGroupCollectionRef()
                .where('organizationId', '==', org.uid)
                .get().then(async (querySnap) => {

                    const { coaches: orgCoaches } = org

                    const groups = await querySnap.docs.map(async (queryDocSnap) => {
                        const group = {
                            uid: queryDocSnap.id, ...queryDocSnap.data() as Group
                        }

                        if (!group.athletes) {
                            group.athletes = []
                            console.log('Group has no athletes', group)
                        }

                        let updateGroup = false;
                        await group.athletes.map((athlete) => {
                            if (orgCoaches.indexOf(athlete.email) !== -1 && !athlete.isCoach) {
                                console.warn('Setting Athelete as coach', {
                                    athlete,
                                    group,
                                    coaches: org.coaches
                                })
                                athlete.isCoach = true
                                updateGroup = true
                            }

                        })

                        if (updateGroup) {
                            console.log('updatingGroup', group)
                            await queryDocSnap.ref.update({ athletes: group.athletes })
                        }


                        return group
                    })

                    return groups

                });

            return orgGroups
        })

        return res.status(200).send({ message: 'Org Groups', updates })
    }

    async addMessagingIdToAthleteIndicator(res: Response): Promise<any> {
        const allOrgs = await getOrgCollectionRef()
            .get().then((querySnap) => {
                const orgs = querySnap.docs.map((queryDocSnap) => {
                    const org = {
                        uid: queryDocSnap.id, ...queryDocSnap.data() as Organization
                    }
                    return org
                })

                return orgs

            });

        const updates = await allOrgs.map(async (org) => {
            const orgGroups = await getGroupCollectionRef()
                .where('organizationId', '==', org.uid)
                .get().then(async (orgQuerySnap) => {

                    // const { coaches: orgCoaches } = org

                    const athleteIndicators = await orgQuerySnap.docs.map(async (queryDocSnap) => {
                        const group = {
                            uid: queryDocSnap.id, ...queryDocSnap.data() as Group
                        }

                        if (!group.athletes) {
                            group.athletes = []
                            console.log('Group has no athletes', group)
                        }

                        const updateGroup = true;
                        const athleteUpdates = await group.athletes.map(async (athlete) => {

                            return await getAthleteDocRef(athlete.uid)
                                .get().then(async (athleteQuerySnap) => {

                                    const ath = {
                                        uid: athleteQuerySnap.id,
                                        ...athleteQuerySnap.data() as Athlete
                                    }
                                    athlete.firebaseMessagingIds = ath.metadata.firebaseMessagingIds || [ath.metadata.firebaseMessagingId]
                                    return athlete
                                })

                        })
                        const ereere = await Promise.all(athleteUpdates)
                        if (updateGroup) {
                            console.log('updatingGroup', group)
                            await queryDocSnap.ref.update({ athletes: group.athletes })
                        }


                        return ereere
                    })


                    return await Promise.all(athleteIndicators)

                });

            return orgGroups
        })
        const allUpdates = await Promise.all(updates)
        return res.status(200).send({ message: 'Org Groups', results: [].concat(...allUpdates) })
    }

    getAthleteGroups = async (req: Request, res: Response): Promise<any> => {
        // const athleteGroups =

        const orgGroups = await getGroupCollectionRef()
            .where('organizationId', '==', '4B8sHmqfFHOzerPqVA82')
            .get().then((querySnap) => {
                const groups = querySnap.docs.map((docSnap) => {
                    const group = {
                        uid: docSnap.id,
                        ...docSnap.data()
                    } as Group

                    return group
                })

                return groups
            })

        const athleteGroups = orgGroups
            .filter((group) => group.athletes
                .filter((athlete) => athlete.uid === 'npAD8qnmzVcnjjMJPHYpgucAMB82').length > 0 ? true : false)

        return athleteGroups
        return res.status(200).send({ message: 'Athlete Groups', athleteGroups })

    }

    fixGroupWeeklyEntityNames = async (req: Request, res: Response): Promise<any> => {

        const groupsSNaps = await getGroupCollectionRef()
            .get()
        const groups = groupsSNaps.docs.map((groupSnap) => {
            return { uid: groupSnap.id, ...groupSnap.data() as Group }
        })

        const result = getCollectionRef(WellAggregateCollection.GroupWeekly)
            .get()
            .then((querySnap: FirebaseFirestore.QuerySnapshot) => {

                const promises = []
                querySnap.docs.map(docSnap => {
                    if (docSnap.exists) {
                        const aggregateDoc = docSnap.data() as GroupWeeklyAggregate

                        const entityId = aggregateDoc.uid.split('_')[0]
                        const entityName = groups.find((grp) => grp.uid === entityId).groupName

                        promises.push(docSnap.ref.update({ entityId, entityName }))
                    } else promises.push(Promise.resolve('Not Found'))
                })
                return Promise.all(promises).then((results) => {

                    return res.status(200).send({ message: 'Documents Updated', results })
                }).catch((error) => {

                    return res.status(500).send(error)
                })
            })
        return result
    }


    getAllUsers = async (req: Request, res: Response): Promise<any> => {
        liveAdmin
            .auth()
            .listUsers()
            .then(function (listUsersResult) {
                listUsersResult.users.forEach(function (userRecord) {
                    console.log('user', userRecord.toJSON())
                })
                // if (listUsersResult.pageToken) {
                //     // List next batch of users.
                //     listAllUsers(listUsersResult.pageToken)
                // }

                return res.status(200).send(listUsersResult.users)
            })
            .catch(function (error) {
                console.log('Error listing users:', error)
                return res.status(500).send(error)
            })
    }

    updateSurveys = async (req: Request, res: Response) => {

        return res.status(500).send('NOT IMPLIMENTED')
    }

    getIOSNotifications = async (req: Request, res: Response) => {
        const { date } = req.body

        const query = getCollectionRef('iosNotifications')
        let result

        if (date) {
            const start = new Date(date)

            result = await query
                .where('creationTimestamp', '>', start)
                .get()
                .then((querySnap: FirebaseFirestore.QuerySnapshot) => {
                    const notifications = querySnap.docs.map(docSnap => {
                        const data = docSnap.data()
                        data.creationTimestamp = data.creationTimestamp.toDate()
                        return data
                    })

                    return notifications
                })
        } else {
            result = await query
                .get()
                .then((querySnap: FirebaseFirestore.QuerySnapshot) => {
                    const notifications = querySnap.docs.map(docSnap => {
                        const data = docSnap.data()

                        return data
                    })

                    return notifications
                })
        }

        return res.status(200).send(result)
    }

    getAndroidNotifications = async (req: Request, res: Response) => {
        const result = getCollectionRef('androidNotifications')
            .get()
            .then((querySnap: FirebaseFirestore.QuerySnapshot) => {
                const notifications = querySnap.docs.map(docSnap => {
                    return docSnap.data()
                })

                return notifications
            })

        return res.status(200).send(result)
    }

    handleNotificationEvent = async (req: Request, res: Response) => {

        const iosNotification = <AppleStoreNotification>req.body

        const {
            environment,
            // // Not posted for notification_type CANCEL
            latest_receipt_info: latestReceiptInfo,
            // //  Posted only if the subscription expired.
            // // Contains the base-64 encoded transaction receipt for the most recent renewal transaction. Posted only if the subscription expired.
            // latest_expired_receipt
            // // Posted only if the notification_type is RENEWAL or CANCEL or if renewal failed and subscription expired.
            latest_expired_receipt_info: latestExpiredReceiptInfo,
            notification_type: notificationType,
            auto_renew_product_id: productIdentifier
        } = iosNotification

        let isExpired = false
        // todo: use the ‘original_transaction_id’ in the notification to check for a matching subscription in our database:
        // If there is one, validate that it’s not already been marked as cancelled / finished.

        let receiptInfo
        if (latestReceiptInfo) {
            receiptInfo = latestReceiptInfo
        } else {
            receiptInfo = latestExpiredReceiptInfo
            isExpired = true
        }

        if (receiptInfo) {
            const {
                original_transaction_id: originalTransactionId,
                transaction_id: transactionId,
                bid: appIdentifier
            } = receiptInfo

            // getCollectionRef('notificationTests').add({
            //     platform: 'IOS',
            //     isExpired,
            //     iosNotification,
            //     creationTimestamp: moment().toDate(),
            // })

            if (transactionId) {
                console.log(
                    `Retrieving Athlete with having originalTransactionId ${originalTransactionId}. Notification Type: ${notificationType}`
                )

                console.log('iosNotification', JSON.stringify(iosNotification))
                // getCollectionRef('tests').add(iosNotification)

                // getCollectionRef('tests').add({
                //     iosNotification: iosNotification,

                //     iosNotificationStringified: JSON.stringify(iosNotification),
                //     originalTransactionId: originalTransactionId,
                //     transactionId: transactionId,
                // })

                const result = getAthleteCollectionRef()
                    .where(
                        'transactionIds',
                        'array-contains',
                        originalTransactionId
                    )
                    .get()
                    .then(
                        async (querySnap: FirebaseFirestore.QuerySnapshot) => {
                            if (!querySnap.empty) {
                                const snap = querySnap.docs[0]

                                const data = snap.data()

                                const athlete = {
                                    uid: snap.id,
                                    ...data,
                                } as Athlete
                                // const originalTransactionId
                                // const cancellationReason
                                // if (latestExpiredReceiptInfo) {
                                //     /* TODO

                                //     latest_expired_receipt :

                                //     If we’ve have a currently ‘active’ subscription that we need to cancel:

                                //     Use the ‘latest_expired_receipt’ key in the notification, and call Apple’s ‘verifyReceipt’ endpoint just to make sure we’ve got the latest information, so then we know we’re working with the latest receipt information.

                                //     Then look for a ‘cancellation_date’ key:

                                //     If there is one, we can proceed with the cancellation.

                                //     Mark the subscription as cancelled and finished in our database and remove the user’s premium access.

                                //     ‘cancellation_reason’

                                //     This key can have two different values and indicates why the user contacted Apple and asked for their subscription to be cancelled / refunded:

                                //     "1" — Customer canceled their transaction due to an actual or perceived issue within your app.

                                //     "0" — Transaction was canceled for another reason, for example, if the customer made the purchase accidentally

                                //     */

                                //     const {
                                //         original_transaction_id: originalTransactionId,
                                //         cancellation_reason: cancellationReason,
                                //     } = latestExpiredReceiptInfo

                                //     // originalTransactionId = latestExpiredReceiptInfo.original_transaction_id
                                //     // cancellationReason = latestExpiredReceiptInfo.cancellation_reason
                                // }

                                //  todo: don’t process the cancellation unless the environment is ‘PROD’
                                if (environment !== 'PROD') {
                                    //  If it’s something else, return a response with the status code 200.
                                }

                                console.log(
                                    `>>>>>>>>>>>>>>>>>>>>>>> notificationType ${notificationType}`
                                )
                                const receiptObject = extractIOSData(
                                    receiptInfo
                                )

                                const expiryDate = receiptObject.expirationDate
                                    ? new Date(+receiptObject.expirationDate)
                                    : new Date(new Date().setHours(0, 0, 0, 0))


                                const startDate = receiptObject.purchaseDate
                                switch (notificationType) {
                                    //// INITIAL BUY
                                    case IOSNotificationType.InitialBuy:
                                        // Occurs at the initial purchase of the subscription.
                                        // Store the latest_receipt on your server as a token to
                                        // verify the user’s subscription status at any time, by validating it with the App Store.

                                        // Initial purchase of the subscription.

                                        const initialBuyReceiptDetails: SaveEventDirective = {
                                            // uid: string
                                            platform: PlatformType.IOS,
                                            transactionId,
                                            originalTransactionId,
                                            expirationDate: expiryDate,
                                            startDate,
                                            webOrderLineItemId: '',
                                            notificationType:
                                                IOSNotificationType.Cancel,
                                            iosReceipt: receiptObject as any,
                                        }

                                        await persistAthleteReceipt(
                                            athlete.uid,
                                            initialBuyReceiptDetails
                                        )

                                        // todo: Store the latest_receipt on your server as a token to verify the user’s subscription status at any time, by validating it with the App Store.
                                        return updateAthleteSubscriptionStatus(
                                            athlete,
                                            receiptObject,
                                            expiryDate,
                                            undefined,
                                            undefined,
                                            undefined,
                                            productIdentifier,
                                            appIdentifier,
                                            originalTransactionId
                                        ).then(() => true)
                                    //// CANCEL
                                    case IOSNotificationType.Cancel:
                                        // Indicates that the subscription was canceled either by Apple customer support
                                        // or by the App Store when the user upgraded their subscription.
                                        // The cancellation_date key contains the date and time when the subscription was canceled or upgraded.
                                        // // Posted only if the notification_type is CANCEL
                                        // cancellation_date // Specifies the date and time that Apple customer support canceled a transaction or the customer upgraded their subscription
                                        // web_order_line_item_id

                                        // Subscription was canceled by Apple customer support.

                                        // todo: Check Cancellation Date to know the date and time when the subscription was canceled.

                                        // const originalTransactionId
                                        // const cancellationReason

                                        if (latestExpiredReceiptInfo) {
                                            /* TODO
     
                                                latest_expired_receipt :
     
                                                If we’ve have a currently ‘active’ subscription that we need to cancel:
     
                                                Use the ‘latest_expired_receipt’ key in the notification, and call Apple’s ‘verifyReceipt’ endpoint just to make sure we’ve got the latest information, so then we know we’re working with the latest receipt information.
     
                                                Then look for a ‘cancellation_date’ key:
     
                                                If there is one, we can proceed with the cancellation.
     
                                                Mark the subscription as cancelled and finished in our database and remove the user’s premium access.
     
                                                ‘cancellation_reason’
     
                                                This key can have two different values and indicates why the user contacted Apple and asked for their subscription to be cancelled / refunded:
     
                                                "1" — Customer canceled their transaction due to an actual or perceived issue within your app.
     
                                                "0" — Transaction was canceled for another reason, for example, if the customer made the purchase accidentally
     
                                            */

                                            const {
                                                // original_transaction_id: originalTransactionId,
                                                // cancellation_reason: cancellationReason,
                                            } = latestExpiredReceiptInfo

                                            // originalTransactionId = latestExpiredReceiptInfo.original_transaction_id
                                            // cancellationReason = latestExpiredReceiptInfo.cancellation_reason
                                        }

                                        const currentSubscriptionType =
                                            athlete.profile.subscription.type


                                        await persistAthleteReceipt(
                                            athlete.uid,
                                            initialBuyReceiptDetails
                                        )
                                        return updateAthleteSubscriptionStatus(
                                            athlete,
                                            receiptObject,
                                            expiryDate,
                                            SubscriptionStatus.Canceled,
                                            SubscriptionType.Free,
                                            undefined,
                                            productIdentifier,
                                            appIdentifier,
                                            originalTransactionId
                                        ).then(
                                            () => {
                                                if (
                                                    currentSubscriptionType !==
                                                    SubscriptionType.Free
                                                ) {

                                                    const actionAnalytics = setAnalytics(
                                                        PushNotificationType.SubscriptionCanceled,
                                                        productIdentifier
                                                    )

                                                    return sendMessage(
                                                        athlete,
                                                        PushNotificationType.SubscriptionCanceled,
                                                        actionAnalytics
                                                    )
                                                } else {
                                                    return Promise.resolve(true)
                                                }
                                            }
                                        )
                                    //// RENEWAL
                                    case IOSNotificationType.Renewal:
                                        // Indicates successful automatic renewal of an expired subscription that failed to renew in the past.
                                        // Check subscription_expiraton_date to determine the next renewal date and time.

                                        // // Posted only if the notification_type is RENEWAL or INTERACTIVE_RENEWAL
                                        // latest_receipt
                                        // // Not posted for notification_type CANCEL
                                        // latest_receipt_info

                                        // // Indicates the reason a subscription expired.
                                        // This is the same as the expiration_intent in the receipt.
                                        // Posted only if notification_type is RENEWAL or INTERACTIVE_RENEWAL
                                        // expiration_intent

                                        // Automatic renewal was successful for an expired subscription.
                                        // // Check subscription_expiraton_date to determine the next renewal date and time

                                        const renewalStatusReceiptDetails: SaveEventDirective = {
                                            // uid: string
                                            platform: PlatformType.IOS,
                                            transactionId,
                                            originalTransactionId,
                                            startDate,
                                            expirationDate: expiryDate,
                                            // webOrderLineItemId: '',
                                            notificationType:
                                                IOSNotificationType.Renewal,
                                            iosReceipt: receiptObject as any,
                                        }

                                        await persistAthleteReceipt(
                                            athlete.uid,
                                            renewalStatusReceiptDetails
                                        )
                                        return updateAthleteSubscriptionStatus(
                                            athlete,
                                            receiptObject,
                                            expiryDate,
                                            SubscriptionStatus.ValidRenewed,
                                            undefined,
                                            undefined,
                                            productIdentifier,
                                            appIdentifier,
                                            originalTransactionId
                                        ).then(() => {
                                            const actionAnalytics = setAnalytics(
                                                PushNotificationType.SubscriptionRenewed,
                                                productIdentifier
                                            )

                                            return sendMessage(
                                                athlete,
                                                PushNotificationType.SubscriptionRenewed,
                                                actionAnalytics
                                            )
                                        })
                                    //// INTERACTIVE RENEWAL
                                    case IOSNotificationType.InteractiveRenewal:
                                        // Indicates the customer renewed a subscription interactively, either by using your app’s interface,
                                        // or on the App Store in account settings. Make service available immediately.

                                        // // Posted only if the notification_type is RENEWAL or INTERACTIVE_RENEWAL
                                        // latest_receipt
                                        // // Not posted for notification_type CANCEL
                                        // latest_receipt_info

                                        // // Indicates the reason a subscription expired.
                                        // This is the same as the expiration_intent in the receipt.
                                        // Posted only if notification_type is RENEWAL or INTERACTIVE_RENEWAL
                                        // expiration_intent

                                        const interactiveRenewalReceiptDetails: SaveEventDirective = {
                                            // uid: string
                                            platform: PlatformType.IOS,
                                            transactionId,
                                            originalTransactionId,
                                            startDate,
                                            expirationDate: expiryDate,
                                            webOrderLineItemId: '',
                                            notificationType:
                                                IOSNotificationType.InteractiveRenewal,
                                            iosReceipt: receiptObject as any,
                                        }

                                        await persistAthleteReceipt(
                                            athlete.uid,
                                            interactiveRenewalReceiptDetails
                                        )

                                        return updateAthleteSubscriptionStatus(
                                            athlete,
                                            receiptObject,
                                            expiryDate,
                                            SubscriptionStatus.ValidRenewed,
                                            undefined,
                                            undefined,
                                            productIdentifier,
                                            appIdentifier,
                                            originalTransactionId
                                        ).then(
                                            () => {
                                                const actionAnalytics = setAnalytics(
                                                    PushNotificationType.SubscriptionRenewed,
                                                    productIdentifier
                                                )

                                                return sendMessage(
                                                    athlete,
                                                    PushNotificationType.SubscriptionRenewed,
                                                    actionAnalytics
                                                )
                                            }
                                        )
                                    //// DID CHANGE RENEWAL REF
                                    case IOSNotificationType.DidChangeRenewalRef:
                                        // Indicates the customer made a change in their subscription plan that takes effect at
                                        // the next renewal. The currently active plan is not affected.

                                        // Customer changed the plan that takes affect at the next subscription renewal.
                                        // Current active plan is not affected.

                                        // TODO:

                                        const didChangeRenewalRefReceiptDetails: SaveEventDirective = {
                                            // uid: string
                                            platform: PlatformType.IOS,
                                            transactionId,
                                            originalTransactionId,
                                            startDate,
                                            expirationDate: expiryDate,
                                            webOrderLineItemId: '',
                                            notificationType:
                                                IOSNotificationType.DidChangeRenewalRef,
                                            iosReceipt: receiptObject as any,
                                        }

                                        await persistAthleteReceipt(
                                            athlete.uid,
                                            didChangeRenewalRefReceiptDetails
                                        )
                                        return updateAthleteSubscriptionStatus(
                                            athlete,
                                            receiptObject,
                                            expiryDate,
                                            undefined,
                                            undefined,
                                            undefined,
                                            productIdentifier,
                                            appIdentifier,
                                            originalTransactionId
                                        ).then(() => true)
                                    // DID CHANGE RENEWAL STATUS
                                    case IOSNotificationType.DidChangeRenewalStatus:
                                        // Indicates a change in the subscription renewal status.

                                        // TODO:
                                        // Check the timestamp for the data and time of the latest status update,
                                        // and the auto_renew_status for the current renewal status.
                                        // if (autoRenewStatus) {
                                        // } else {
                                        // }

                                        const didChangeRenewalStatusReceiptDetails: SaveEventDirective = {
                                            // uid: string
                                            platform: PlatformType.IOS,
                                            transactionId,
                                            originalTransactionId,
                                            startDate,
                                            expirationDate: expiryDate,
                                            webOrderLineItemId: '',
                                            notificationType:
                                                IOSNotificationType.DidChangeRenewalStatus,
                                            iosReceipt: receiptObject as any,
                                        }

                                        await persistAthleteReceipt(
                                            athlete.uid,
                                            didChangeRenewalStatusReceiptDetails
                                        )
                                        // TODO: Chech expired
                                        if (isExpired) {
                                            // TODO; send message?
                                            return updateAthleteSubscriptionStatus(
                                                athlete,
                                                receiptObject,
                                                expiryDate,
                                                SubscriptionStatus.Free,
                                                SubscriptionType.Free,
                                                undefined,
                                                productIdentifier,
                                                appIdentifier,
                                                originalTransactionId
                                            ).then(() => true)
                                        } else {
                                            return updateAthleteSubscriptionStatus(
                                                athlete,
                                                receiptObject,
                                                expiryDate,
                                                undefined,
                                                undefined,
                                                undefined,
                                                productIdentifier,
                                                appIdentifier,
                                                originalTransactionId
                                            ).then(() => true)
                                        }
                                    // TODO:

                                    default:
                                        return true
                                }
                            } else {
                                // TODO:
                                console.log(
                                    `can't find Athlete with Originall TransactionId ${originalTransactionId}, and TransactionId ${transactionId}`
                                )
                                return undefined
                            }
                        }
                    )

                // TODO:
                return result
                    .then(success => {
                        if (success) {
                            return res.status(200).send(success)
                        } else {
                            return res
                                .status(203)
                                .send(
                                    success
                                        ? success
                                        : 'could not find document'
                                )
                        }
                    })
                    .catch(err => {
                        return res.status(500).send(err)
                    })
            } else {
                const errorMessage = `Error Retrieving Athlete with having Original TransactionId ${originalTransactionId}, and TransactionId ${transactionId}`

                console.log(errorMessage)
                console.log('iosNotification', JSON.stringify(iosNotification))

                await getCollectionRef('tests').add({
                    iosNotification: iosNotification,
                    errorMessage,
                    originalTransactionId: originalTransactionId,
                    transactionId: transactionId,
                })
                // TODO:
                return false
            }
        } else {
            console.log('latestReceiptInfo is undefined', latestReceiptInfo)
            await getCollectionRef('tests').add({
                iosNotification: iosNotification,
                errorMessage: 'latestReceiptInfo undefined',
                payload: req.body,
            })
            return res
                .status(500)
                .send({ err: 'latestReceiptInfo undefined', reg: req.body })
        }
    }
    /**
     * Get all Tests from the database
     *
     * @memberof TestsCtrl
     */
    testGoogleToken = async (req: Request, res: Response): Promise<any> => {
        // const { id } = <{ id: any }>req.params
        // console.log(req.params)

        this.res = res

        const { keys: theKeys } = req.body

        const keys: {
            type: string
            project_id: string
            private_key_id: string
            private_key: string
            client_email: string
            client_id: string
            auth_uri: string
            token_uri: string
            auth_provider_x509_cert_url: string
            client_x509_cert_url: string
        } = theKeys

        const googleNotification = {
            version: '1.0',
            packageName: 'com.inspiresportonline.dev',
            eventTimeMillis: '1551041270264',
            subscriptionNotification: {
                version: '1.0',
                notificationType: 3,
                purchaseToken:
                    'obmapelacmaallbipllnkgmn.AO-J1OwiR9UsGLoFrpcqki1GUYj3sqhp8sFEvF-eaHe8mGzG7sjtlsmzIFBu4IoNcBbCT5Db8oVtBtWqcmVtrgfRTLdgVwWGMS1nU0Zx6xe90RRR2jZjexPmO6NSRnbqb8GYiQcR_Vpz40Q3Bt2hFk4T1_UH945Q8K7bdCE7tvbd6u1EtCkLvOI',
                subscriptionId: 'com.inspiresportonline.dev.subscriptions',
            },
        }

        const { packageName, subscriptionNotification } = googleNotification

        const options = {
            email: keys.client_email, // 'gmailservice@accountemail',
            key: keys.private_key, // '-----BEGIN PRIVATE KEY-----your private key-----END PRIVATE KEY-----'
            // keyFile: 'alternatively path to key file'
        }

        const verifier = new Verifier(options)

        const receipt = {
            packageName,
            productId: subscriptionNotification.subscriptionId, //'sku / subscription id',
            purchaseToken: subscriptionNotification.purchaseToken,
        }


        // Validate Subscription
        const promiseSubData = verifier.verifySub(receipt)

        const results = promiseSubData
            .then(function (response) {
                // Yay! Subscription is valid
                console.log(response)

                return response
            })
            // .then(function(response) {
            //     // Here for example you can chain your work if subscription is valid
            //     console.log(response)
            // })
            .catch(function (error) {
                // Subscription is not valid or API error
                // See possible error messages below
                console.log(error)
            })

        return res.json(results)

        //   Validate In-app purchase
        // const promiseInAppData = verifier.verifyINAPP(receipt)

        // promiseInAppData
        //     .then(function(response) {
        //         // Yay! Purchase is valid
        //         console.log(response)
        //     })
        //     .then(function(response) {
        //         // Here for example you can chain your work if purchase is valid
        //         console.log(response)
        //     })
        //     .catch(function(error) {
        //         // Purchase is not valid or API error
        //         console.log(error)
        //     })
    }

    addProgram = async (req: Request, res: Response): Promise<any> => {
        // const { id } = <{ id: any }>req.params
        console.log(req.params)

        this.res = res


        const programCategory: ProgramIndicator = {
            uid: '',
            contentProvisioning: {}, // TODO:
            creationTimestamp: undefined,
            programGuid: 'a1cd3351-715d-443a-a23d-d32bad1ef9de',
            name: 'iNSPIRE Onboarding Program',
            publisher: 'iNSPIRE',
            description:
                'Welcome to iNSPIRE, this is your onboarding program that will run for the next week! After you complete this program you will have the choice of a number of programs to continue your iNSPIRE journey.',
            images: [
                {
                    url:
                        'iNSPIRE%2Fimages%2Fa1cd3351-715d-443a-a23d-d32bad1ef9de.png',
                },
            ],
            // startDate: null,
            activeDate: firestore.Timestamp.fromDate(new Date('2019-01-01')),
            callToActionText: 'Kick it off!',
            categoryType: 0,
            subscriptionType: 0,
            programDays: 7,
            tags: ['onboarding'],
            content: [
                {
                    label: 'Day 1',
                    description:
                        'Welcome to day 1! Enjoy learning about the business and watching your first iNSPIRE video with Annie.',
                    entries: [
                        {
                            title: 'Introducing the Business',
                            description: '',
                            type: 1,
                            strategyGuid:
                                'a61d081a-f73e-4646-ab09-09bf2fc6e5d9',
                            docGuid: '97ed5897-303b-446c-ba29-35a91cd0eb57',
                            fileGuid: 'd97b11b3-fb0b-4210-b067-a809cd363607',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title: 'Who is Annie?',
                            description:
                                'This video is all about Annie and her background, enjoy!',
                            type: 1,
                            strategyGuid:
                                'c73f204e-074d-4f05-b960-e7024fa13074',
                            docGuid: '4f9f8651-4414-4141-aee0-b69d2cbb9b14',
                            fileGuid: '492e16e2-dd90-40b9-92ca-5a059e7647c6',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                    ],
                },
                {
                    label: 'Day 2',
                    description:
                        "Welcome to day 2! Today, you're going to learn how to use the basic features on the app, watch the video then give it a go!",
                    entries: [
                        {
                            title: 'How to use the app',
                            description: '',
                            type: 1,
                            strategyGuid:
                                'b087669a-8c4b-43ee-a32d-3d3375dfd92b',
                            docGuid: '40360a3c-8528-4350-bd43-eb5657c3ebfc',
                            fileGuid: '8c9247a3-3510-4367-a71e-b40f58abdf3c',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title: 'Tracking your wellness on the app',
                            description:
                                'Today Annie takes you through how to track your wellness on the app, give it a go and try to do it everyday!',
                            type: 1,
                            strategyGuid:
                                '2597a5a7-0af8-4d27-97c6-c0c3ec0fc160',
                            docGuid: '55605da1-5271-491a-b2ac-0c396e6abfea',
                            fileGuid: '017548b5-e4c7-4e6c-be04-566fd55cb28d',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                    ],
                },
                {
                    label: 'Day 3',
                    description:
                        'Welcome to day 3! Today, Annie takes you through worksheets and how to get the most out of them.',
                    entries: [
                        {
                            title: 'How to access work sheets',
                            description: '',
                            type: 1,
                            strategyGuid:
                                '23ab8410-a8c7-4281-9652-e517098193b6',
                            docGuid: '75e01fe4-9807-4152-bff1-2399dee9095d',
                            fileGuid: '24910952-8f46-4b14-a376-5dcd4eadeec0',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title: 'Social Media',
                            description:
                                'Today Annie talks to you about Social Media, do you follow us yet? Watch the video then give us a like and a follow!',
                            type: 1,
                            strategyGuid:
                                'ab79b0f9-d3fb-4435-aaea-4db4fbd0bf01',
                            docGuid: '4f863394-9624-4158-ae24-050209d80ed2',
                            fileGuid: '08874c26-e721-4809-810f-a8331fa0be80',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                    ],
                },
                {
                    label: 'Day 4',
                    description:
                        'Welcome to day 4! Today, Annie talks to you about the coaches dashboard & how it can help you!',
                    entries: [
                        {
                            title: 'Coaches Dashboard',
                            description: '',
                            type: 1,
                            strategyGuid:
                                '096a6fa9-828e-4733-80e6-461cfa0fbbba',
                            docGuid: '0971120e-fc98-4347-a075-3706b891feaf',
                            fileGuid: '46d8c80e-08ed-4cc8-b9f5-52a67187a16f',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title: 'Sports Psych',
                            description:
                                'Watch this video to learn about the Sports Psychology Programs.',
                            type: 1,
                            strategyGuid:
                                'cae8b972-f8e9-475f-9b3f-2abd149d8022',
                            docGuid: 'bde193b0-915a-4d59-a846-a2dbd269ff26',
                            fileGuid: '3e33169a-eb3e-48e5-aca7-cc868147d709',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                    ],
                },
                {
                    label: 'Day 5',
                    description:
                        'Welcome to day 5! Watch todays video to learn about the Sports Nutrition Programs.',
                    entries: [
                        {
                            title: 'Sports Nutrition',
                            description:
                                'Watch this video to learn about the Sports Nutrition Programs.',
                            type: 1,
                            strategyGuid:
                                '18b8cb09-f038-45ad-9ebb-493561fab8a3',
                            docGuid: '4cf56d7d-ef89-410b-b09b-0e7e7d9f63fa',
                            fileGuid: '4596808f-89b3-4892-b139-ec2b7f553e33',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title: 'Why nutrition is important ',
                            description: '',
                            type: 3,
                            strategyGuid:
                                '53960391-ff75-46ef-a396-8439e7d037aa',
                            docGuid: 'e093807e-b27a-4f62-8002-41306161f5b4',
                            fileGuid: '5265d86f-31cb-4054-b687-c18405b422b8',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title: 'Sports Science',
                            description:
                                'Watch this video to learn about the Sports Science Programs.',
                            type: 1,
                            strategyGuid:
                                '200431da-a1e5-4f22-9c7d-f5c2a923601a',
                            docGuid: 'f022f7a5-6f88-4219-9242-21be73c00ce7',
                            fileGuid: '41c74a80-73b0-4945-9fa4-3a51d563aade',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                    ],
                },
                {
                    label: 'Day 6',
                    description:
                        "Welcome to day 6! Today Annie talks to you about Stress and how to manage it, watch the video & don't forget to take some time to breathe and relax today! ",
                    entries: [
                        {
                            title: 'Managing stress and what it feels like',
                            description: '',
                            type: 1,
                            strategyGuid:
                                'dbcd0819-5b33-4226-9bca-87273ea51ec3',
                            docGuid: '9f6eb595-06e9-4ef5-8261-0223805caab2',
                            fileGuid: '65208b83-3b54-4204-b658-eb2bcfba6941',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title: 'DASS 21 Survey',
                            description: '',
                            type: 2,
                            strategyGuid:
                                '19b1aa85-2cc8-473e-b436-168ad530fafc',
                            docGuid: '95e4f846-7989-4264-8a59-e05490375b17',
                            fileGuid: '922d4433-7f3f-488f-8d45-bef9be9616aa',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title: "Sleep and why it's important",
                            description:
                                'Today Annie talks to you about the importance of Sleep, a super important factor to your performance and health as an athlete so pay extra attention!  ',
                            type: 1,
                            strategyGuid:
                                '262f433f-a45f-4333-ae3a-f4c924951548',
                            docGuid: '3e98db13-6a13-46b3-bc8d-abb4d96757e9',
                            fileGuid: 'f54dd0f5-1097-4d18-90eb-3e232375464f',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                    ],
                },
                {
                    label: 'Day 7',
                    description:
                        "Welcome to day 7! Today Annie talks to you about self doubt and how to manage it, watch the video and remember… you're amazing!",
                    entries: [
                        {
                            title:
                                'How to manage self doubt (positive self talk)',
                            description: '',
                            type: 1,
                            strategyGuid:
                                '97e3eb2d-5b60-4782-9285-b5f1bbf39ae8',
                            docGuid: 'f175a8ca-3fa6-4eaa-9cfb-a5a8519463c3',
                            fileGuid: 'c7f1ea19-b536-4744-9bbd-8e3f82bbb4fe',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title:
                                "Final reminder on how to choose programs on the app starting tomorrow. Make sure you've got the latest version",
                            description:
                                'Enjoy this video and get excited for your next iNSPIRE Program.',
                            type: 1,
                            strategyGuid:
                                'e8fd8bf3-388a-40fd-bb6b-ee50614062f0',
                            docGuid: 'f0c95a38-34c6-4534-998a-2c4b31a52e37',
                            fileGuid: '27cfe353-bf2b-4cba-a2cf-f96831f07766',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                        {
                            title: 'iNSPIRE Sport Survey',
                            description: '',
                            type: 2,
                            strategyGuid:
                                '48dfd694-528d-4a9a-a32d-fcd6904da7f9',
                            docGuid: 'dfb165de-b370-42da-9016-68b91171262e',
                            fileGuid: '50baf808-a2b0-4490-8742-dd1e8834270f',
                            engagement: false,
                            version: 1,
                            embed: true,
                        },
                    ],
                },
            ],
        }
        const { content, ...programIndicator } = programCategory

        const { programGuid, categoryType } = programIndicator
        const programCatalogueRef = await getCollectionRef(
            FirestoreCollection.ProgramCatalogue
        )

        try {
            const result = await programCatalogueRef
                .where('categoryType', '==', categoryType)
                .get()
                .then(
                    async (
                        programCatQuerySnap: FirebaseFirestore.QuerySnapshot
                    ) => {
                        if (!programCatQuerySnap.empty) {
                            // Selected Catalogue i.e. Sports Nutrition
                            const programCatSnap = programCatQuerySnap.docs[0]

                            // const ProgramCatalogue = {
                            //     uid: programCatSnap.id,
                            //     ...(programCatSnap.data() as ProgramCatalogue),
                            // }

                            // Add the program indicator to the Catalogue
                            const updateResult = await programCatSnap.ref
                                .update({
                                    programs: firestore.FieldValue.arrayUnion(
                                        programIndicator
                                    ),
                                })
                                .then(async () => {
                                    // const { programGuid } = programIndicator
                                    // } as ProgramContent
                                    return true
                                })

                            if (updateResult) {
                                const programContent: ProgramContent = {
                                    programGuid,
                                    content,
                                }

                                // Save the program Content
                                const programContentDocSnap = await programCatSnap.ref
                                    .collection('content')
                                    .doc(programGuid)
                                    .get()

                                let updateContentResult = false
                                if (programContentDocSnap.exists) {
                                    // Update Excisting
                                    updateContentResult = await programContentDocSnap.ref
                                        .update({
                                            content: firestore.FieldValue.arrayUnion(
                                                content
                                            ),
                                        })
                                        .then(() => {
                                            return true
                                        })
                                        .catch(err => {
                                            console.log(
                                                `Error occured updating content for programGuid ${programGuid}`,
                                                err
                                            )
                                            throw err
                                        })
                                } else {
                                    // Create New
                                    updateContentResult = await programContentDocSnap.ref
                                        .set(programContent)
                                        .then(() => {
                                            return true
                                        })
                                        .catch(err => {
                                            console.log(
                                                `Error occured creating content for programGuid ${programGuid}`,
                                                err
                                            )
                                            throw err
                                        })
                                }

                                return updateContentResult
                            }
                            // TODO:
                            else return false
                        } else {
                            return false
                        }
                    }
                )
                .catch(err => {
                    console.log(err)
                    return err
                })

            // return 'sportsNutritionPrograms'

            return res.json(result)

            // const snapshot = await getCollectionRef(FirestoreCollection.ProgramCatalogue).doc('general').get()
            // return res.json(snapshot.docs.map(doc => doc.data()))
        } catch (err) {
            console.log(
                `Error saving content for ProgramGuid ${programGuid}`,
                err
            )
            throw err
        }
    }

    importPrograms = async (req: Request, res: Response): Promise<any> => {
        // const { id } = <{ id: any }>req.params
        console.log(req.params)

        this.res = res

        const program = getCollectionRef(FirestoreCollection.Programs)
            .doc('u2vTH4Kw1CaGRuvMpsAr')
            .get()
            .then(async () => {

                // const content = (<any>theProgram).content

                // const ptogCnt: ProgramContent = {
                //     programGuid: '71bad20f-8712-4bb7-986b-daab777ba80c', //program.uid,
                //     content,
                // }

                // const sportsNutritionPrograms = await getCollectionRef(FirestoreCollection.ProgramCatalogue)
                //     .doc('sportsNutrition')
                //     .collection('content')
                //     .doc('71bad20f-8712-4bb7-986b-daab777ba80c')
                //     .set(ptogCnt)
                //     // const sportsNutritionIndicator = this.sportsNutritionModel

                //     .then(doc => doc)
                //     .catch(err => {
                //         console.log(err)
                //         return err
                //     })
                return 'sportsNutritionPrograms'
            })
            .catch(err => console.log(err))


        // const portsPsychologyPrograms = await getCollectionRef(FirestoreCollection.ProgramCatalogue).doc('sportsPsychology')
        // const sportsPsychologyIndicator = this.sportsPsychologyModel
        // const test2 = portsPsychologyPrograms.set(sportsPsychologyIndicator).then((doc) => doc.writeTime).catch((err) => {console.log(err); return err})

        // const sportsSciencePrograms = await getCollectionRef(FirestoreCollection.ProgramCatalogue).doc('sportsScience')
        // const sportsScienceIndicator = this.sportsScienceModel
        // const tes3 = sportsSciencePrograms.set(sportsScienceIndicator).then((doc) => doc.writeTime).catch((err) => {console.log(err); return err})

        return res.json(program)

        // const snapshot = await getCollectionRef(FirestoreCollection.ProgramCatalogue).doc('general').get()
        // return res.json(snapshot.docs.map(doc => doc.data()))
    }

    getScalarBiometrics = async (req: Request, res: Response): Promise<any> => {
        // const { id } = <{ id: any }>req.params
        console.log(req.body);
        const request = req.body as OlapGetLifestyleEntryTypeRequest;
        debugger;

        this.res = res
        let response: LifestyleEntryStatisticsResponse = undefined;
        try {
            if (request.entityType === 'athlete') {
                response = await getAthleteBiometricStatisticsFromCubeJs(request);
            } else {
                response = await getSegmentBiometricStatisticsFromCubeJs(request);
            }
            return res.json(response);
        }
        catch(err) {
            console.log(err);
            return res.json(err);
        }
    }

    // validateReceiptTest = async (req: Request, res: Response) => {
    //     const {
    //         athleteUid,
    //         transactionReceipt,
    //         //  packageName,
    //         ...theRest
    //     } = <TransactionReceiptValidationRequest>req.body
    //     console.log(req.body)
    //     const applePassword = req.body.applePassword

    //     // const clientEmail = process.env.CLIENT_EMAIL
    //     // const privateKey = process.env.PRIVATE_KEY
    //     // if (!clientEmail || !privateKey) {
    //     //     throw new Error(`
    //     //     The CLIENT_EMAIL and PRIVATE_KEY environment constiables are required for
    //     //     this sample.
    //     //     `)
    //     // }

    //     // const client = new JWT(
    //     //     keys.client_email,
    //     //     null,
    //     //     keys.private_key,
    //     //     ['https://www.googleapis.com/auth/cloud-platform'],
    //     // );
    //     // const url = `https://www.googleapis.com/dns/v1/projects/${keys.project_id}`;
    //     // const result = await client.request({url})
    //     // .then((res) => {

    //     //     console.log(res);
    //     // })
    //     // .catch((err) => {
    //     //     console.log(err);

    //     // });
    //     // console.log(result.data);
    //     // return res.status(200).json(result)

    //     // const client = await auth.getClient({
    //     //     credentials: {
    //     //         client_email: keys.client_email,
    //     //         private_key: keys.private_key,
    //     //     },
    //     //     scopes: 'https://www.googleapis.com/auth/cloud-platform',
    //     // })
    //     // const projectId = await auth.getProjectId()
    //     // const url = `https://www.googleapis.com/dns/v1/projects/${projectId}`
    //     // const res3 = await client.request({ url })
    //     // console.log('DNS Info:')
    //     // console.log(res3.data)

    //     // return res3

    //     const res2 = {
    //         version: '1.0',
    //         packageName: 'com.inspiresportonline.dev',
    //         eventTimeMillis: '1551041270264',
    //         subscriptionNotification: {
    //             version: '1.0',
    //             notificationType: 3,
    //             purchaseToken:
    //                 'obmapelacmaallbipllnkgmn.AO-J1OwiR9UsGLoFrpcqki1GUYj3sqhp8sFEvF-eaHe8mGzG7sjtlsmzIFBu4IoNcBbCT5Db8oVtBtWqcmVtrgfRTLdgVwWGMS1nU0Zx6xe90RRR2jZjexPmO6NSRnbqb8GYiQcR_Vpz40Q3Bt2hFk4T1_UH945Q8K7bdCE7tvbd6u1EtCkLvOI',
    //             subscriptionId: 'com.inspiresportonline.dev.subscriptions',
    //         },
    //     }

    //     const { packageName, subscriptionNotification } = res2

    //     const options3 = {
    //         host: 'https://www.googleapis.com',
    //         path: `/androidpublisher/v3/applications/${packageName}/purchases/products/${
    //             subscriptionNotification.subscriptionId
    //         }/tokens/${subscriptionNotification.purchaseToken}`,
    //     }
    //     //    return axios
    //     //         // .get('https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY')
    //     //         .get(options3.host + options3.path)
    //     //         .then(response => {
    //     //             console.log(response.data.url)
    //     //             console.log(response.data.explanation)
    //     //             return response
    //     //         })
    //     //         .catch(error => {
    //     //             console.log(error)
    //     //             return error
    //     //         })

    //     // const options = {
    //     //     host: 'https://www.googleapis.com',
    //     //     path: `/androidpublisher/v3/applications/${packageName}/purchases/products/${
    //     //         subscriptionNotification.subscriptionId
    //     //     }/tokens/${subscriptionNotification.purchaseToken}`,
    //     // }

    //     // const theRequest = http.get(options, function(res3) {
    //     //     console.log('STATUS: ' + res3.statusCode)
    //     //     console.log('HEADERS: ' + JSON.stringify(res3.headers))

    //     //     // Buffer the body entirely for processing as a whole.
    //     //     const bodyChunks = []
    //     //     res3.on('data', function(chunk) {
    //     //         // You can process streamed parts here...
    //     //         bodyChunks.push(chunk)
    //     //     })
    //     //         .on('end', function() {
    //     //             const body = Buffer.concat(bodyChunks)
    //     //             console.log('BODY: ' + body)
    //     //             return body
    //     //             // ...and/or process the entire body here.
    //     //         })
    //     //         .on('error', function(err) {
    //     //             console.log(err)
    //     //         })
    //     // })
    //     try {
    //         // const privateKey = fs.readFileSync(
    //         //     '/inspire-1540046634666-8a646218e67a---pw-notasecret.p12'
    //         // )
    //         // const oauthToken = jwt.sign(
    //         //     {
    //         //         iss:
    //         //             '761326798069-r5mljlln1rd4lrbhg75efgigp36m78j5@developer.gserviceaccount.com',
    //         //         scope:
    //         //             'https://www.googleapis.com/auth/devstorage.readonly',
    //         //         aud: 'https://www.googleapis.com/oauth2/v4/token',
    //         //         exp: 1328554385,
    //         //         iat: 1328550785,
    //         //     },
    //         //     privateKey,
    //         //     {
    //         //         algorithm: 'RS256',
    //         //     }
    //         // )

    //         // const dataString = await Buffer.from(
    //         //     message.data,
    //         //     'base64'
    //         // ).toString()

    //         const client = new JWT(keys.client_email, null, keys.private_key, [
    //             // 'https://www.googleapis.com/auth/cloud-platform',
    //             'https://www.googleapis.com/auth/androidpublisher',
    //         ])
    //         const url = `https://www.googleapis.com/dns/v1/projects/${
    //             keys.project_id
    //         }`
    //         const result = await client
    //             .request({ url })
    //             .then(res => {
    //                 console.log(res)
    //             })
    //             .catch(err => {
    //                 console.log(err)
    //             })
    //         console.log(result.data)
    //         return res.status(200).json(result)
    //     } catch (error) {
    //         return res.status(500).json(error)
    //     }
    // }

    // validateReceipt = async (req: Request, res: Response) => {
    //     const {
    //         athleteUid,
    //         transactionReceipt,
    //         //  packageName,
    //         ...theRest
    //     } = <TransactionReceiptValidationRequest>req.body
    //     console.log(req.body)
    //     const applePassword = req.body.applePassword
    //     try {
    //         // // const clientEmail = process.env.CLIENT_EMAIL;
    //         // // const privateKey = process.env.PRIVATE_KEY;

    //         const client = new JWT(keys.client_email, null, keys.private_key, [
    //             'https://www.googleapis.com/auth/cloud-platform',
    //         ])
    //         const url = `https://www.googleapis.com/dns/v1/projects/${
    //             keys.project_id
    //         }`
    //         const result = await client
    //             .request({ url })
    //             .then(response => {
    //                 console.log(response.data.url)
    //                 console.log(response.data.explanation)
    //                 return res.status(500).json(response)
    //             })
    //             .catch(error => {
    //                 console.log(error)
    //                 return res.status(500).json(error)
    //             })
    //         console.log(result.data)

    //         return result

    //         const clientEmail = keys.client_email
    //         const privateKey = keys.private_key
    //         if (!clientEmail || !privateKey) {
    //             throw new Error(`
    //             The CLIENT_EMAIL and PRIVATE_KEY environment constiables are required for
    //             this sample.
    //           `)
    //         }
    //         // const client = await auth.getClient({
    //         //     credentials: {
    //         //         client_email: clientEmail,
    //         //         private_key: privateKey,
    //         //     },
    //         //     scopes: 'https://www.googleapis.com/auth/cloud-platform',
    //         // })
    //         // const projectId = await auth.getProjectId()
    //         // const url = `https://www.googleapis.com/dns/v1/projects/${projectId}`
    //         // const resp = await client.request({ url })
    //         // console.log('DNS Info:')
    //         // console.log(resp.data)

    //         return res.status(200).json('result')

    //         // const client = new JWT(
    //         //     keys.client_email,
    //         //     null,
    //         //     keys.private_key,
    //         //     ['https://www.googleapis.com/auth/cloud-platform'],
    //         // );
    //         // const url = `https://www.googleapis.com/dns/v1/projects/${keys.project_id}`;
    //         // const result = await client.request({url});
    //         // console.log(result.data);

    //         // const privateKey = fs.readFileSync(
    //         //     '/inspire-1540046634666-8a646218e67a---pw-notasecret.p12'
    //         // )

    //         // const time = moment()
    //         // const oauthToken = jwt.sign(
    //         //     {
    //         //         iss: 'client_email',
    //         //         scope:
    //         //             // 'https://www.googleapis.com/auth/devstorage.readonly',
    //         //             'https://www.googleapis.com/auth/cloud-platform',
    //         //         aud: 'https://www.googleapis.com/oauth2/v4/token',
    //         //         exp: time.add(1, 'hour'),
    //         //         iat: time.toDate(),
    //         //         sub: 'annie@inspiresportonline.com'
    //         //     },
    //         //     privateKey,
    //         //     {
    //         //         algorithm: 'RS256',
    //         //     }
    //         // )
    //         // const headers = {
    //         //     'Content-Type': 'application/json',
    //         //     'Authorization': 'JWT fefege...'
    //         // }

    //         // const res2 = {
    //         //     version: '1.0',
    //         //     packageName: 'com.inspiresportonline.dev',
    //         //     eventTimeMillis: '1551041270264',
    //         //     subscriptionNotification: {
    //         //         version: '1.0',
    //         //         notificationType: 3,
    //         //         purchaseToken:
    //         //             'obmapelacmaallbipllnkgmn.AO-J1OwiR9UsGLoFrpcqki1GUYj3sqhp8sFEvF-eaHe8mGzG7sjtlsmzIFBu4IoNcBbCT5Db8oVtBtWqcmVtrgfRTLdgVwWGMS1nU0Zx6xe90RRR2jZjexPmO6NSRnbqb8GYiQcR_Vpz40Q3Bt2hFk4T1_UH945Q8K7bdCE7tvbd6u1EtCkLvOI',
    //         //         subscriptionId: 'com.inspiresportonline.dev.subscriptions',
    //         //     },
    //         // }

    //         // const { packageName, subscriptionNotification } = res2

    //         // const options3 = {
    //         //     host: 'https://www.googleapis.com',
    //         //     path: `/androidpublisher/v3/applications/${packageName}/purchases/products/${
    //         //         subscriptionNotification.subscriptionId
    //         //     }/tokens/${subscriptionNotification.purchaseToken}`,
    //         // }
    //         // axios.post(options3.host + options3.path, data, {headers: headers})

    //         //     .then((response) => {
    //         //         dispatch({type: FOUND_USER, data: response.data[0]})
    //         //     })
    //         //     .catch((error) => {
    //         //         dispatch({type: ERROR_FINDING_USER})
    //         //     })

    //         axios({
    //             method: 'post',
    //             url: 'http://localhost:2002/token',
    //             data: {
    //                 grant_type:
    //                     'urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=',
    //             },
    //             headers: {
    //                 'Content-Type': 'application/x-www-form-urlencoded',
    //                 'Cache-Control': 'no-cache',
    //                 'Postman-Token': '42e6c291-9a09-c29f-f28f-11872e2490a5',
    //             },
    //         })
    //             .then(function(response) {
    //                 console.log('Heade With Authentication :' + response)
    //             })
    //             .catch(function(error) {
    //                 console.log('Post Error : ' + error)
    //             })

    //         //    return axios
    //         //         // .get('https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY')
    //         //         .post(options3.host + options3.path)
    //         //         .then(response => {
    //         //             console.log(response.data.url)
    //         //             console.log(response.data.explanation)
    //         //             return response
    //         //         })
    //         //         .catch(error => {
    //         //             console.log(error)
    //         //             return error
    //         //         })

    //         return res.status(200).json('result')
    //     } catch (error) {
    //         return res.status(500).json(error)
    //     }

    //     iap.config({
    //         /* Configurations for Apple */
    //         // appleExcludeOldTransactions: true, // if you want to exclude old transaction, set this to true. Default is false

    //         // the App-Specific Shared Secret: https://appstoreconnect.apple.com/WebObjects/iTunesConnect.woa/ra/ng/app/1453032120/addons
    //         applePassword: applePassword
    //             ? applePassword
    //             : 'bbfdc220bd9445bbae3543f6974940fc', // '7329723d1bf3452fbeb26aace103b5ac'

    //         /* Configurations for Google Service Account validation: You can validate with just packageName, productId, and purchaseToken */
    //         googleServiceAccount: {
    //             clientEmail:
    //                 'firebase-adminsdk-yabfi@inspire-1540046634666.iam.gserviceaccount.com',
    //             privateKey:
    //                 '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDEa8dBKl8pFlrH\nEtlOFu5Pajeg6dhmCOKwISfdO2WYEaWtM80kzGxrxfkw+ePJO4+WzigOm9FcSwhk\nMTlJovU4S1G1vP/tF3uAeNstqgQiENnDuQtbFb2FzhUFMXptkcTfBuoe2vmyn4pk\neBTn63HOUmaD6doU8HtikdQsb5uaJJCpH5VKaPCPAYtLelV/h/8eAu+1dQcoShEY\noY808KXJzzwtWnLHUBnb/s7iUQUVfWH1JP5OTujkxdM7B11KRB3JX9PsexS/+tVJ\niZVbxPIr5CzmM+fjKD/Gico/07IqyloPYzBZ0CSaTRJrEtpiebWXBMHPODsHsI9p\na1KZdkwbAgMBAAECggEAC6WQZQ7MIbWPoGo/tF+rtc3IiqQTDsnMF0GACaAx//hb\n6I8/xMTSUPmmMv8+QHploz2KJoOawGw6jSZWDUW6YKImsC4KxtYznoSMCoMgR5zU\nLMTWJYp+eGal3G74oXKZR9gzHa1CTRMks5xjMPVHBELijUPaiI9R4aBgAlqD0Xnq\n1buKaF9Mvtha3jvHsrWk0mJeyoR5SsQ5o718RVJ5k/xuS4/nc5uOz33SG/xxKyBa\nMuFomisEP92QpTbksut87/FlsQd7XgnPkp+r7UL9olGd8gByII3islpsmEhgrOPT\nmnGpCBjD9pzifTUMQLcZKCmnymfRFRevv3CS5Gk+cQKBgQD4rY8XU/IMoFPiE6FD\nbvHv8+2b0P54uka7GN49Yx7riw73EG9SmgZCTT6V2jhlxXv8kV023085RTXQdPCr\ng0s2kvIJxU4quiZeJBquG1Scji2DSlMKv9tOZNswb4K2x+ZSDZ9GTvfXj/09NDnk\n2jJMRp8K826bI7RwHp03F90+ZwKBgQDKNFN68CjRWGe3zd/LaW6yAwFa8t9yJwdk\nwgcKfKgIm+a5UkQgE8sQ7wWvGMrxSuYJFH1fHIuVFRmpQsJgfydKVHJs16ZdvDun\nvDXy6psYhvggm9pEf+xqct4702oVhaQY/Ee0K6pQgcH966VYwmz4USJj+woWJYxW\nr9cLf76MLQKBgQDP6QPOhC/F4LHhPXpBj9uVO8L32Dc5prwGJ1d/yYSLd/ruE36P\neBkti7l8vjMS25a65qohe3iYMEY639pr+1yB5z+Xba/Zx0LWyKbJ1C3cqn5g214s\niZWIqIgdqc2GlgD5r0vwE4vhXRBkAGs67DbLUOwd0sMx0BtG9kGJU1l1lwKBgQDE\nrCCGcxFAjbxUCuqh7uq8OjAXRiQP4+ZNGmu+x4Co3vqLRnj8ukPJNLNSm8rI5xDX\nxBYtbJZXay6Kc2ScdxDAO2MQerBWe7+KZoYSwB4avSyaivzBo6tP3mpJxlholpQF\nuVwE4nPF2m/Vil5I9tMGs+O/W210HRFjP6TqilXMAQKBgGfbZaGFD3xqSYt+3dA+\nyF7w2/4Bj8xzBmKDQQj2NPan/5V1cWXxGfXNqLDgmxqzsJ+cD6yhPd6t55l2yIcV\nuEWg8XhOq3l0jU+Fh8hD0lHqeFH7yxpp2ymzMjCg3nbq90h9fZqNvHzZSI2cUoCJ\n8UAojg1vDxolRkag4gmGVa7s\n-----END PRIVATE KEY-----\n',
    //         },

    //         /* Configurations for Google Play */
    //         // googlePublicKeyPath: 'path/to/public/key/directory/', // this is the path to the directory containing iap-sanbox/iap-live files
    //         // googlePublicKeyStrSandBox: 'publicKeySandboxString', // this is the google iap-sandbox public key string
    //         // googlePublicKeyStrLive: 'publicKeyLiveString', // this is the google iap-live public key string
    //         // googleAccToken: 'abcdef...', // optional, for Google Play subscriptions
    //         // googleRefToken: 'dddd...', // optional, for Google Play subscritions
    //         // googleClientID: '109738660367933675398', // optional, for Google Play subscriptions
    //         // googleClientSecret: 'bbbb', // optional, for Google Play subscriptions

    //         /* Configurations all platforms */
    //         test: true, // For Apple and Googl Play to force Sandbox validation only
    //         verbose: true, // Output debug logs to stdout stream
    //     })

    //     const res2 = {
    //         version: '1.0',
    //         packageName: 'com.inspiresportonline.dev',
    //         eventTimeMillis: '1551041270264',
    //         subscriptionNotification: {
    //             version: '1.0',
    //             notificationType: 3,
    //             purchaseToken:
    //                 'obmapelacmaallbipllnkgmn.AO-J1OwiR9UsGLoFrpcqki1GUYj3sqhp8sFEvF-eaHe8mGzG7sjtlsmzIFBu4IoNcBbCT5Db8oVtBtWqcmVtrgfRTLdgVwWGMS1nU0Zx6xe90RRR2jZjexPmO6NSRnbqb8GYiQcR_Vpz40Q3Bt2hFk4T1_UH945Q8K7bdCE7tvbd6u1EtCkLvOI',
    //             subscriptionId: 'com.inspiresportonline.dev.subscriptions',
    //         },
    //     }

    //     const { packageName, subscriptionNotification } = res2

    //     // const options = {
    //     //     host: 'https://www.googleapis.com',
    //     //     path: `/androidpublisher/v3/applications/${packageName}/purchases/products/${
    //     //         subscriptionNotification.subscriptionId
    //     //     }/tokens/${subscriptionNotification.purchaseToken}`,
    //     // }
    //     // const request = http.get(options, function(res3) {
    //     //     console.log('STATUS: ' + res3.statusCode)
    //     //     console.log('HEADERS: ' + JSON.stringify(res3.headers))

    //     //     // Buffer the body entirely for processing as a whole.
    //     //     const bodyChunks = []
    //     //     res3.on('data', function(chunk) {
    //     //         // You can process streamed parts here...
    //     //         bodyChunks.push(chunk)
    //     //     })
    //     //         .on('end', function() {
    //     //             const body = Buffer.concat(bodyChunks)
    //     //             console.log('BODY: ' + body)
    //     //             return body
    //     //             // ...and/or process the entire body here.
    //     //         })
    //     //         .on('error', function(err) {
    //     //             console.log(err)
    //     //         })
    //     // })

    //     return request
    //     const parts = transactionReceipt.split('.')
    //     if (parts.length) {
    //         parts.forEach(p => {
    //             const test2 = atob(p)
    //         })
    //     }
    //     const test = atob(transactionReceipt)

    //     try {
    //         return iap.setup(function(error) {
    //             if (error) {
    //                 console.error('something went wrong...', error)
    //                 return res.status(510).json({
    //                     message: 'validateReceipt',
    //                     responseCode: 510,
    //                     // data: response,
    //                     error,
    //                     method: {
    //                         name: 'validateReceipt',
    //                         line: 42,
    //                     },
    //                 })
    //             }

    //             const theReceipt = transactionReceipt
    //                 ? transactionReceipt
    //                 : '34566'
    //             // As of v1.4.0+ .validate and .validateOnce detects service automatically from the receipt
    //             // iap.validate(receipt, function (error, response) {

    //             const decoded = atob(theReceipt)

    //             const pubKeyString =
    //                 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnxCINeUK9Yav17dM1LTmlmPExPeSTbg/yWRy0H/bP6sdAquYTlcGox0x85C+j5Bu8FD3duWU/xH/F0wxiYWEHG+nPKoyuIzo30Qrdy+8bbtbKS0ZdRvZ04TftXB8+eKnk9K57SZlR7Q+99zZh6t0OqrQeuYmyBuaqwJfLKFoOhnLb4zdI0yuMYx8bni4qcpQCuWrpRED5YveHegFVVezuVNuVmv9Etgm9XUPcL/E8SxBNyzj4jmv2vp2Ql03X32pXgYWiT6iyv/6WwAAX6Qa7mq3e4S4Tb3zKGWOGleiv+ZUPzeavXRC5gbu6GEEA9HTBSdXzyexqy0DA5MS+D4jfwIDAQAB'

    //             // iap.validateOnce(theReceipt, pubKeyString)
    //             //     .then(onSuccess)
    //             //     .catch((erro, data) => {
    //             //         onError(erro, data)
    //             //     })

    //             // iap.validateOnce(decoded, pubKeyString)
    //             //     .then(onSuccess)
    //             //     .catch((erro, data) => {
    //             //         onError(erro, data)
    //             //     })
    //             return iap.validate(theReceipt, function(err, validatedData) {
    //                 const response: TransactionReceiptValidationResponse = {
    //                     ...req.body,
    //                 }
    //                 if (err) {
    //                     // Failed to validate
    //                     console.error(err)
    //                     if (validatedData)
    //                         console.error(
    //                             'ERROR:Validating Receipt ======>>>>>> validateResponse',
    //                             validatedData
    //                         )

    //                     return updateReceiptCollection(
    //                         athleteUid,
    //                         response
    //                     ).then(theRes => {
    //                         const message = validatedData.message
    //                             ? err.message + ' - ' + validatedData.message
    //                             : err.message
    //                         response = {
    //                             ...theRest,
    //                             isValid: false,
    //                             message,
    //                         }
    //                         console.log(response)
    //                         // TODO: Check this
    //                         // const data = validateResponse.data();
    //                         // validateResponse.send(data)
    //                         return res.status(500).json({
    //                             message: 'validateReceipt',
    //                             responseCode: 500,
    //                             data: response,
    //                             // error: undefined,
    //                             method: {
    //                                 name: 'validateReceipt',
    //                                 line: 42,
    //                             },
    //                         })
    //                         // return res.status(500).json(response) //(validateResponse)
    //                     })
    //                 }
    //                 if (iap.isValidated(validatedData)) {
    //                     // validatedData: the actual content of the validated receipt
    //                     // validatedData also contains the original receipt
    //                     const options2 = {
    //                         ignoreCanceled: true, // Apple ONLY (for now...): purchaseData will NOT contain cancceled items
    //                         ignoreExpired: true, // purchaseData will NOT contain exipired subscription items
    //                     }
    //                     // validatedData contains sandbox: true/false for Apple and Amazon
    //                     const purchaseData = iap.getPurchaseData(
    //                         validatedData,
    //                         options2
    //                     )
    //                     return {
    //                         message: 'validateReceipt',
    //                         responseCode: 510,
    //                         data: purchaseData,
    //                         // error,
    //                         method: {
    //                             name: 'validateReceipt',
    //                             line: 42,
    //                         },
    //                     }
    //                     // Successful validation
    //                     // const data = transactionReceipt;
    //                     // validateResponse.send(data)
    //                 } else {
    //                     response = {
    //                         isValid: false,
    //                         message: 'Receipt Not Valid',
    //                     }

    //                     return updateReceiptCollection(
    //                         athleteUid,
    //                         response
    //                     ).then(theRes => {
    //                         response = {
    //                             ...theRest,
    //                             isValid: false,
    //                             message: 'Receipt Not Valid',
    //                         }
    //                         console.log(response)

    //                         return res.status(501).json({
    //                             message: 'validateReceipt',
    //                             responseCode: 501,
    //                             data: response,
    //                             // error: undefined,
    //                             method: {
    //                                 name: 'validateReceipt',
    //                                 line: 42,
    //                             },
    //                         })
    //                         // return res.status(501).json(response) //(validateResponse)
    //                     })
    //                 }
    //             })
    //         })
    //     } catch (error) {
    //         atob(transactionReceipt)

    //         console.log(error)
    //         res.status(500).send(error)
    //     }
    // }
}
function setNotificationTemplateOptions(templateReplacements: { email: KeyValuePair[]; notifications: KeyValuePair[]; }, groupName: string) {
    if (!templateReplacements.notifications.length) {
        templateReplacements.notifications = [
            {
                "value": groupName,
                "key": "$GROUP_NAME"
            }
        ];
    }
}

function setEmailMessageTemplateOptions(emailMessageTemplate: EmailMessageTemplate, sendPushNotification: boolean): EmailMessageTemplate {
    let theEmailMessageTemplate: EmailMessageTemplate
        ;
    if (!emailMessageTemplate) {
        theEmailMessageTemplate = {
            templateId: 'd-de993ceb05174e69aec4b236e34fd8ac',
            emailTemplateName: 'groupInvite',

            includePushNotification: sendPushNotification,
            notificationPath: '',
            notificationTemplate: 'groupInvite',
        };
    } else {
        theEmailMessageTemplate.templateId = emailMessageTemplate.templateId || 'd-de993ceb05174e69aec4b236e34fd8ac';
        theEmailMessageTemplate.emailTemplateName = emailMessageTemplate.emailTemplateName || 'groupInvite';


        theEmailMessageTemplate.includePushNotification = emailMessageTemplate.includePushNotification || sendPushNotification;
        theEmailMessageTemplate.notificationPath = emailMessageTemplate.notificationPath || '/jind/jindfatigue/wqtIJNDY7aPaRp0qhOVqpLaQRpr1';
        theEmailMessageTemplate.notificationTemplate = emailMessageTemplate.notificationTemplate || 'groupInvite';
    }

    return theEmailMessageTemplate
}

