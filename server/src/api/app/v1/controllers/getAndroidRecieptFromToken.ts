import { AndroidPurchaseReceipt } from '../../../../models/purchase/purchase.model';
const Verifier = require('google-play-billing-validator');

export const getAndroidRecieptFromToken = (keys: {
    private_key: string;
    client_email: string;
}, receipt: {
    packageName: string; // com.inspiresportonline.dev
    productId: string; // com.inspiresportonline.dev.subscriptions
    purchaseToken: string;
    // TODO: 
}) => { // Promise<AndroidPurchaseReceipt>
    const options = {
        email: keys.client_email,
        key: keys.private_key,
    };

    console.log('verifying receipt ');

    const verifier = new Verifier(options);
    // Validate Subscription
    const promiseSubData: Promise<AndroidPurchaseReceipt> = verifier.verifySub(receipt);
    return promiseSubData
        .then((response) => {
            // Yay! Subscription is valid
            console.log('receipt validation response', response);
            // console.log('receipt validation response.packageName', (<any>response).packageName);
            // const theResponse = {
            //     isSuccessful: true,
            //     errorMessage: null,
            //     payload: {
            //         kind: 'androidpublisher#subscriptionPurchase',
            //         startTimeMillis: '1551039170322',
            //         expiryTimeMillis: '1551041266268',
            //         autoRenewing: false,
            //         priceCurrencyCode: 'EUR',
            //         priceAmountMicros: '1250000',
            //         countryCode: 'MT',
            //         cancelReason: 1,
            //         orderId: 'GPA.3340-2776-4142-42066..5',
            //         purchaseType: 0,
            //     },
            // };


            response.payload.acknowledgementStateString = response.payload.acknowledgementState === 0 ? 'Yet to be acknowledged' : 'Acknowledged'

            // let paymentStateString = ''

            // switch (response.payload.paymentState) {
            //     case 0:
            //         paymentStateString = 'Payment pending'
            //         break;
            //     case 1:
            //         paymentStateString = 'Payment received'
            //         break;
            //     case 2:
            //         paymentStateString = 'Free trial'
            //         break;
            //     case 3:
            //         paymentStateString = 'Pending deferred upgrade/downgrade'
            //         break;

            //     default:
            //         break;
            // }
            // response.payload.paymentStateString = paymentStateString
            // if (response.payload.purchaseType !== undefined) {
            //     response.payload.purchaseTypeString = response.payload.purchaseType === 0
            //         ? 'Test (i.e. purchased from a license testing account)'
            //         : 'Promo (i.e. purchased using a promo code)'
            // }
            response.payload.packageName = receipt.packageName
            response.payload.productId = receipt.productId
            return response;
        })
        // .then(function(response) {
        //     // Here for example you can chain your work if subscription is valid
        //     console.log(response)
        // })
        .catch(function (error: any) {
            // Subscription is not valid or API error
            // See possible error messages below
            console.error('verifySub ERROR', error);
            throw error // return { response: error };
        });
    //   Validate In-app purchase
    // let promiseInAppData = verifier.verifyINAPP(receipt)
    // promiseInAppData
    //     .then(function(response) {
    //         // Yay! Purchase is valid
    //         console.log(response)
    //     })
    //     .then(function(response) {
    //         // Here for example you can chain your work if purchase is valid
    //         console.log(response)
    //     })
    //     .catch(function(error) {
    //         // Purchase is not valid or API error
    //         console.log(error)
    //     })
};
