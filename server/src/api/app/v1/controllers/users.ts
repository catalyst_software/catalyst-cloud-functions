import { Response, Request } from 'express'

import BaseCtrl from './base'
import { Organization } from '../../../../models/organization.model'
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'
import { db, theMainApp } from '../../../../shared/init/initialise-firebase'
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { Athlete } from '../../../../models/athlete/interfaces/athlete'
import { listAllUsers } from './listAllUsers';

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class UserCtrl
 * @extends {BaseCtrl}
 */
export default class UsersCtrl extends BaseCtrl {
    res: Response;

    // logger: Logger;
    model = Organization;
    entries;
    // TODO: 
    collection: FirestoreCollection.Athletes;
    aggregateDocumentManager: AggregateDocumentManager;

    constructor() {
        super()
    }


    setEmail = (req: Request, res: Response) => {

        const { uid, email } = req.body

        return theMainApp.auth().updateUser('awXpu6KHRTOlV9dSXV1zwY8YPTC3', {
            email: '81301@stpeters.qld.edu.au'
            // emailVerified: isVerified,
            // password: 'newPassword',
            // displayName: 'Jane Doe',
            // photoURL: 'http://www.example.com/12345678/photo.png',
            // disabled: true
        })
            .then(function (userRecord) {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('Successfully updated user', userRecord.toJSON());
                return true
            })
            .catch(function (error) {
                console.log('Error updating user:', error);
                return error
            });
    }

    setEmailVerified = (req: Request, res: Response) => {

        const { uid, isVerified } = req.body

        return theMainApp.auth().updateUser(uid, {

            emailVerified: isVerified,
            // password: 'newPassword',
            // displayName: 'Jane Doe',
            // photoURL: 'http://www.example.com/12345678/photo.png',
            // disabled: true
        })
            .then(function (userRecord) {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('Successfully updated user', userRecord.toJSON());
                return true
            })
            .catch(function (error) {
                console.log('Error updating user:', error);
                return error
            });
    }

    getAllUsers = (req: Request, res: Response) => {

        this.res = res;
        return listAllUsers(1).then((users) => {
            res.json(users);
        });
    };

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof UsersCtrl
     */
    addUser = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        const { user, profile, isVerified } = <{ user: any; profile: Athlete, isVerified?: boolean }>req.body;
        console.log(req.body);
        const me = this;


        const emailVerified = isVerified !== undefined ? isVerified : false

        return theMainApp
            .auth()
            .createUser({
                uid: user.uid,
                email: user.email,
                password: user.password ? user.password : 'password',
                emailVerified
            })
            .then(userRecord => {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('...: ', userRecord.uid);


                if (profile)

                    // save profile
                    return db
                        .collection(FirestoreCollection.Athletes)
                        .doc(userRecord.uid)
                        .set(profile)
                        .then(async writeResult => {
                            debugger;
                            const writeResults = await Promise.all(profile.groups.map(async group => {
                                if (group.groupId && group.groupId !== '') {
                                    //Create an empty document, or do nothing if it exists.
                                    return await db.collection(FirestoreCollection.Groups)
                                        .doc(group.groupId)
                                        .set(group, { merge: true })

                                        .then(groupWriteResult => {
                                            return groupWriteResult
                                        })
                                } else {
                                    return undefined
                                }
                            }));
                            return res.status(200).json({
                                message: 'User added and profile created',
                                responseCode: 201,
                                data: {
                                    user: userRecord,
                                    athlete: profile,
                                    result: writeResults,
                                },
                                // error: undefined,
                                // method: {
                                //     name: 'addUser',
                                //     line: 58,
                                // },
                            })
                        })
                        .catch(err => {
                            return res.status(502).json({
                                message: 'Errror adding Athlete profile',
                                responseCode: 201,
                                data: {
                                    user: userRecord,
                                },
                                error: err,
                                method: {
                                    name: 'addUser',
                                    line: 58,
                                },
                            })
                        });


                return res.status(200).json({
                    message: 'User added',
                    responseCode: 201,
                    data: {
                        user: userRecord,
                        // athlete: profile,
                        // result: writeResult,
                    },
                    // error: undefined,
                    // method: {
                    //     name: 'addUser',
                    //     line: 58,
                    // },
                })
                return me.handleResponse({
                    message: 'User added',
                    responseCode: 201,
                    data: {
                        user: userRecord,
                    },
                    error: undefined,
                    method: {
                        name: 'addUser',
                        line: 58,
                    },
                })

                // return res.status(200).json({ user: userRecord });
            })
            .catch(function (error) {
                console.log('Error creating new user: ', error);
                return res.status(502).json({ error })
            })
    };
    // addUser = async (
    //     req: Request,
    //     res: Response
    // ): Promise<boolean | string> => {
    //     const { email, password, isVerified } = <{ user: any; profile: Athlete }>req.body;
    //     console.log(req.body);
    //     const me = this;
    //     return admin
    //         .auth()
    //         .createUser({
    //             uid: user.uid,
    //             email: user.email,
    //             password: user.password ? user.password : 'password',
    //         })
    //         .then(userRecord => {
    //             // See the UserRecord reference doc for the contents of userRecord.
    //             console.log('...: ', userRecord.uid);

    //             // save profile
    //             return db
    //                 .collection(FirestoreCollection.Athletes)
    //                 .doc(userRecord.uid)
    //                 .set(profile)
    //                 .then(writeResult => {
    //                     profile.groups.map(group => {
    //                         if (group.groupId && group.groupId !== '') {
    //                             //Create an empty document, or do nothing if it exists.
    //                             db.collection(FirestoreCollection.Groups)
    //                                 .doc(group.groupId)
    //                                 .set(group, { merge: true })

    //                                 .then(groupWriteResult => {
    //                                     return groupWriteResult
    //                                 })
    //                         }
    //                     });
    //                     return res.status(200).json({
    //                         message: 'User added',
    //                         responseCode: 201,
    //                         data: {
    //                             user: userRecord,
    //                             athlete: profile,
    //                             result: writeResult,
    //                         },
    //                         error: undefined,
    //                         method: {
    //                             name: 'addUser',
    //                             line: 58,
    //                         },
    //                     })
    //                 })
    //                 .catch(err => {
    //                     return res.status(502).json({
    //                         message: 'Errror adding Athlete profile',
    //                         responseCode: 201,
    //                         data: {
    //                             user: userRecord,
    //                         },
    //                         error: err,
    //                         method: {
    //                             name: 'addUser',
    //                             line: 58,
    //                         },
    //                     })
    //                 });

    //             return me.handleResponse({
    //                 message: 'User added',
    //                 responseCode: 201,
    //                 data: {
    //                     user: userRecord,
    //                 },
    //                 error: undefined,
    //                 method: {
    //                     name: 'addUser',
    //                     line: 58,
    //                 },
    //             })

    //             // return res.status(200).json({ user: userRecord });
    //         })
    //         .catch(function (error) {
    //             console.log('Error creating new user: ', error);
    //             return res.status(502).json({ error })
    //         })
    // };
    // // deleteHero (id: number): Observable<{}> {
    // //   const url = `${this.heroesUrl}/${id}`; // DELETE api/heroes/42
    // //   return this.http.delete(url, httpOptions)
    // //     .pipe(
    // //       catchError(this.handleError('deleteHero'))
    // //     );
    // // }
    deleteUser = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        const { id } = <{ id: string }>req.params;
        console.log(req.body);
        const me = this;

        // return admin
        //     .auth()
        //     .deleteUser(id)
        //     .then(function() {
        //         console.log('Successfully deleted user')

        //         return db
        //             .collection(FirestoreCollection.Athlete)
        //             .doc(id)
        //             .delete()
        //             .then(writeResult => {
        //                 // TODO: Update groups?!
        //                 // profile.groups.map(group => {
        //                 //     if (group.groupId && group.groupId !== '') {
        //                 //         //Create an empty document, or do nothing if it exists.
        //                 //         db.collection(FirestoreCollection.Group)
        //                 //             .doc(group.groupId)
        //                 //             .set(group, { merge: true })

        //                 //             .then(groupWriteResult => {
        //                 //                 return groupWriteResult
        //                 //             })
        //                 //     }
        //                 // })
        //                 return res.status(200).json({
        //                     message: 'User deleted',
        //                     responseCode: 201,
        //                     data: {
        //                         userId: id,
        //                         result: writeResult,
        //                     },
        //                     method: {
        //                         name: 'deleteUser',
        //                         line: 129,
        //                     },
        //                 })
        //             })
        //             .catch(err => {
        //                 return res.status(502).json({
        //                     message: 'Unable to delete Athlete Profile',
        //                     responseCode: 201,
        //                     data: {
        //                         userId: id,
        //                     },
        //                     error: err,
        //                     method: {
        //                         name: 'deleteUser',
        //                         line: 129,
        //                     },
        //                 })
        //             })
        //     })
        //     .catch(function(error) {
        //         console.log('Error deleting user:', error)
        //     })

        // Start listing users from the beginning, 1000 at a time.

        return theMainApp
            .auth()
            .deleteUser(id)
            .then(() => {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('...: ', id);

                return me.handleResponse({
                    message: 'User added',
                    responseCode: 201,
                    data: {
                        userID: id,
                    },
                    error: undefined,
                    method: {
                        name: 'deleteUser',
                        line: 129,
                    },
                })

                // return res.status(200).json({ user: userRecord });
            })
            .catch(function (error) {
                console.log('Error creating new user: ', error);
                return res.status(502).json({ error })
            })
    }
}


