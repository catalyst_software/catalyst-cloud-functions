import { Response, Request } from 'express'
import { isDate } from 'lodash'
import { firestore } from 'firebase-admin'
// import { FieldValue } from '@google-cloud/firestore'
import { Guid } from 'guid-typescript'
import { isNumber } from 'util'

import BaseCtrl from './base'
import { Organization } from '../../../../models/organization.model'
import {
    FirestoreCollection,
    FirestoreProgramsCollection,
} from '../../../../db/biometricEntries/enums/firestore-collections'
import { db, liveDB } from '../../../../shared/init/initialise-firebase'
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { Athlete } from '../../../../models/athlete/athlete.model'
import { Group } from '../../../../models/group/group.model'
import { Program } from '../../../../db/programs/models/program.model'
import { ProgramIndicator } from '../../../../db/programs/models/interfaces/program-indicator'
import { ProgramContent } from '../../../../db/programs/models/interfaces/program-content'
import { ProgramCatalogue } from '../../../../db/programs/models/interfaces/program-catalogue'
import { SportsNutrition } from './sportsNutrition'
import { SportsPsychology } from './sportsPsychology'
import { SportsScience } from './sportsScience'
import { General } from './general'
import { ProgramDay } from '../../../../db/programs/models/interfaces/program-day'
import { ProgramCategoryType } from '../../../../models/enums/program-category-type'
import { ProgramDayEntry } from '../../../../db/programs/models/interfaces/program-day-entry.interface'
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot'

// import { Program } from '../../../../models/athlete/interfaces/athlete'

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class ProgramCtrl
 * @extends {BaseCtrl}
 */
export default class ProgramsCtrl extends BaseCtrl {
    res: Response

    generalModel = General
    sportsNutritionModel = SportsNutrition
    sportsPsychologyModel = SportsPsychology
    sportsScienceModel = SportsScience

    // logger: Logger;
    model = ProgramsCtrl
    collection = FirestoreCollection.Programs
    entries
    aggregateDocumentManager: AggregateDocumentManager

    constructor() {
        super()
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof ProgramsCtrl
     */
    getAllPrograms = async (req: Request, res: Response): Promise<any> => {
        this.res = res

        const snapshot = await db.collection(FirestoreCollection.Programs).get()
        return res.json(snapshot.docs.map(doc => doc.data()))
    }
    copyProgramsToDev = async (req: Request, res: Response): Promise<any> => {
        this.res = res





        const ct = [
            "general",
            "sportsPsychology",
            "sportsNutrition",
            "sportsScience",
        ]

        ct.forEach(async (c) => {
            const snapshot = await liveDB.collection(FirestoreCollection.ProgramCatalogue).doc(c).collection(FirestoreCollection.ProgramContent).get()


            const cats = snapshot.docs.map(doc => doc.data())
            cats.forEach(async (cat) => {
                const snapshot2 = await db.collection(FirestoreCollection.ProgramCatalogue).doc(c).collection(FirestoreCollection.ProgramContent).add(cat)
                console.log(snapshot2)
            })
        })



        return res.json()
    }

    getProgramAndAddToConfig = async (req: Request, res: Response): Promise<any> => {
        // const { id } = <{ id: any }>req.params

        this.res = res



        const programCatalogueRef = await db.collection(
            FirestoreCollection.ProgramCatalogue
        )

        try {
            const result = await programCatalogueRef
                .doc('general')
                .get()
                .then(
                    async (
                        programCatSnap: firestore.DocumentSnapshot
                    ) => {
                        if (programCatSnap.exists) {
                            // Selected Catalogue Query Document Snaphost i.e. Sports Nutrition

                            const prog = {
                                ...programCatSnap.data()
                            } as ProgramCatalogue

                            const theProg = prog.programs[1];

                            return theProg


                        } else {


                            return false
                        }
                    }
                )
                .catch(err => {
                    console.log(err)
                    return err
                })

            return res.json(result)

            // const snapshot = await db.collection(FirestoreCollection.ProgramCatalogue).doc('general').get()
            // return res.json(snapshot.docs.map(doc => doc.data()))
        } catch (err) {
            console.log(
                `Error getting content for Basic`,
                err
            )
            throw err
        }
    }

    /**
     * Adds a Historic Program to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof ProgramsCtrl
     */

    addProgram = async (req: Request, res: Response): Promise<any> => {
        // const { id } = <{ id: any }>req.params

        this.res = res

        const { document: program } = <{ document: ProgramIndicator }>(
            req.body
        )

        const { startDate, content, ...rest } = program

        const programIndicator: Program = {
            ...rest,
            programGuid: rest.programGuid ? rest.programGuid : Guid.raw(),
            // TODO: Programs dates
            activeDate: rest.activeDate,
            // creationTimestamp: firestore.Timestamp.now(),
        }

        const { programGuid, categoryType } = programIndicator

        const programCatalogueRef = await db.collection(
            FirestoreCollection.ProgramCatalogue
        )

        try {
            const result = await programCatalogueRef
                .where('categoryType', '==', categoryType)
                .get()
                .then(
                    async (
                        programCatQuerySnap: FirebaseFirestore.QuerySnapshot
                    ) => {
                        if (!programCatQuerySnap.empty) {
                            // Selected Catalogue Query Document Snaphost i.e. Sports Nutrition
                            const programCatSnap = programCatQuerySnap.docs[0]

                            // Add the program indicator to the Catalogue
                            const updateResult = await this.addProgramToCatalogue(
                                programCatSnap,
                                programIndicator
                            )

                            if (updateResult && content && content.length) {
                                const programContent: ProgramContent = {
                                    programGuid,
                                    content,
                                }

                                // Save the program Content
                                const programContentDocSnap = await programCatSnap.ref
                                    .collection('content')
                                    .doc(programGuid)
                                    .get()
                                // // TODO: Skipping Appending Content, we're overwriting it for now
                                const appendContent = false
                                if (
                                    programContentDocSnap.exists &&
                                    appendContent
                                ) {
                                    // Append content to the array
                                    return await this.updateProgramContent(
                                        programContentDocSnap,
                                        content,
                                        programGuid
                                    )
                                } else {
                                    // Create sub content collection and add program content
                                    return await this.setProgramContent(
                                        programContentDocSnap,
                                        programContent
                                    )
                                }
                            } else {
                                // TODO:
                                if (!updateResult) {
                                    return false
                                } else {
                                    return true
                                }
                            }
                        } else {

                            const collection =
                                FirestoreProgramsCollection[
                                ProgramCategoryType[categoryType]
                                ]

                            let programCatalogue: firestore.DocumentReference = programCatalogueRef.doc(
                                collection
                            )
                            let programCategory: ProgramCatalogue

                            if (typeof categoryType === 'number') {
                                switch (+categoryType) {
                                    case ProgramCategoryType.General:
                                        programCategory = this.generalModel
                                        break
                                    case ProgramCategoryType.SportsNutrition:
                                        programCategory = this
                                            .sportsNutritionModel
                                        break
                                    case ProgramCategoryType.SportsPsychology:
                                        programCategory = this
                                            .sportsPsychologyModel
                                        break
                                    case ProgramCategoryType.SportsScience:
                                        programCategory = this
                                            .sportsScienceModel
                                        break

                                    default:
                                        // // TODO: check !categoryName
                                        // programCatalogue = await db
                                        // .collection(FirestoreCollection.ProgramCatalogue)
                                        // .doc(categoryName)
                                        programCatalogue = programCatalogueRef.doc(
                                            FirestoreProgramsCollection.General
                                        )
                                        programCategory = this.generalModel
                                        break
                                }
                            } else {
                                try {
                                    programCatalogue = programCatalogueRef.doc(<any>categoryType)
                                    programCategory = this.generalModel
                                } catch (err) {
                                    console.log(
                                        `Error saving content for ProgramGuid ${programGuid}`,
                                        err
                                    )
                                    throw err
                                }
                            }

                            const theResult: boolean = await this.setProgramCatalogue(programCatalogue, programCategory, programIndicator, content)
                            return theResult
                        }
                    }
                )
                .catch(err => {
                    console.log(err)
                    return err
                })

            return res.json(result)

            // const snapshot = await db.collection(FirestoreCollection.ProgramCatalogue).doc('general').get()
            // return res.json(snapshot.docs.map(doc => doc.data()))
        } catch (err) {
            console.log(
                `Error saving content for ProgramGuid ${programGuid}`,
                err
            )
            throw err
        }
    }

    private setProgramCatalogue(
        programCatalogueRef: firestore.DocumentReference,
        programCategory: ProgramCatalogue,
        programIndicator: Program,
        content: ProgramDay[]): boolean | PromiseLike<boolean> {
        return programCatalogueRef
            .set({
                ...programCategory,
                programs: firestore.FieldValue.arrayUnion(programIndicator),
            })
            .then(async (updateResult: FirebaseFirestore.WriteResult) => {
                if (updateResult && content && content.length) {
                    return await this.updateDB(programCatalogueRef, programIndicator, content);
                }
                else {
                    // TODO:
                    if (!updateResult) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            })
            .catch(err => {
                console.log(err);
                return err;
            });
    }

    private async updateDB(
        programCatalogue: firestore.DocumentReference,
        programIndicator: Program,
        content: ProgramDay[]
    ) {
        const { programGuid } = programIndicator

        const programContent: ProgramContent = {
            programGuid,
            content,
        }
        // Save the program Content
        const programContentDocSnap = await programCatalogue
            .collection('content')
            .doc(programGuid)
            .get()
        const appendContent = false
        if (programContentDocSnap.exists && appendContent) {
            // Add content to the array
            const addContentResult = await this.updateProgramContent(
                programContentDocSnap,
                content,
                programGuid
            )
            return addContentResult
        } else {
            // Create sub content collection and add program content
            const setContentResult = await this.setProgramContent(
                programContentDocSnap,
                programContent
            )
            return setContentResult
        }

    }

    private async updateProgramContent(
        programContentDocSnap: firestore.DocumentSnapshot,
        content: ProgramDay[],
        programGuid: string
    ) {
        const updateContentResult = await programContentDocSnap.ref
            .update({
                content: firestore.FieldValue.arrayUnion(...content),
            })
            .then(() => {
                console.log(
                    `Successfully updated content for programGuid ${programGuid}`,
                    content
                )
                return true
            })
            .catch(err => {
                console.log(
                    `Error occured updating content for programGuid ${programGuid}`,
                    err
                )
                throw err
            })
        return updateContentResult
    }

    private async setProgramContent(
        programContentDocSnap: firestore.DocumentSnapshot,
        programContent: ProgramContent
    ) {
        const setContentResult = await programContentDocSnap.ref
            .set(programContent)
            .then(() => {
                console.log(
                    `Successfully created content for program ${programContent.programGuid}`,
                    programContent
                )
                return true
            })
            .catch(err => {
                console.log(
                    `Error occured creating content for programGuid ${programContent.programGuid}`,
                    { programContent, err }
                )
                throw err
            })
        return setContentResult
    }

    private async addProgramToCatalogue(
        programCatSnap: firestore.QueryDocumentSnapshot,
        programIndicator: ProgramIndicator // {
        //     programGuid?: string
        //     name?: string
        //     publisher?: string
        //     description?: string
        //     images?: ImageLocator[]
        //     imageSource?: string
        //     comingSoon?: boolean
        //     activeDate?: Date
        //     callToActionText: string
        //     categoryType: ProgramCategoryType
        //     subscriptionType: SubscriptionType
        //     programDays?: number
        //     isResctricted?: boolean
        //     restrictedOrganizationIds?: string[]
        //     tags?: string[]
        // }
    ) {
        return await programCatSnap.ref
            .update({
                programs: firestore.FieldValue.arrayUnion(programIndicator),
            })
            .then(async () => {
                console.log(
                    `Successfully updated program details in ${programCatSnap.ref.id
                    }. ${programIndicator.name}`,
                    programIndicator
                )
                return true
            })
            .catch(err => {
                console.log(
                    `Error occured updating program details in ${programCatSnap.ref.id
                    } for programGuid ${programIndicator.programGuid}`,
                    { programIndicator, err }
                )
                throw err
            })
    }

    addHistoricProgram = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        console.log(req.body)
        const me = this
        this.res = res

        const { document: program } = <{ document: Program }>req.body

        const { images, ...theProgram } = program

        return db
            .collection(FirestoreCollection.Programs)
            .add({ ...theProgram, images })
            .then(writeResult => {
                return me.handleResponse({
                    message: 'Program added',
                    responseCode: 201,
                    data: {
                        program: program,
                        writeResult,
                    },
                    error: undefined,
                    method: {
                        name: 'addProgram',
                        line: 58,
                    },
                    res: this.res,
                })
            })
            .catch(err => {
                return res.status(502).json({
                    message: 'Errror adding Program',
                    responseCode: 201,
                    data: {
                        program: program,
                    },
                    error: err,
                    method: {
                        name: 'addProgram',
                        line: 58,
                    },
                })
            })
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof ProgramsCtrl
     */
    getProgram = async (req: Request, res: Response): Promise<any> => {
        const { id } = <{ id: any }>req.params

        console.log(req.params)
        const me = this

        this.res = res

        // See the ProgramRecord reference doc for the contents of programRecord.
        console.log('...: ', id)

        const docRef = db.collection(FirestoreCollection.Programs).doc(id)

        return docRef
            .get()
            .then(function (doc) {
                if (doc.exists) {
                    console.log('Document data:', doc.data())
                    return me.handleResponse({
                        message: 'Program Retrieved',
                        responseCode: 201,
                        data: {
                            program: doc.data(),
                        },
                        method: {
                            name: 'getProgram',
                            line: 35,
                        },
                        res: this.res,
                    })
                } else {
                    console.log('No such document!')
                    return me.handleResponse({
                        message:
                            'Unable to retrieve Program - No such document!',
                        responseCode: 204,
                        method: {
                            name: 'getProgram',
                            line: 35,
                        },
                        res: this.res,
                    })
                }
            })
            .catch(function (error) {
                console.log('Error getting document:', error)
                return me.handleResponse({
                    message: 'Unable to retrieve Program - An error occured',
                    responseCode: 500,
                    method: {
                        name: 'getProgram',
                        line: 35,
                    },
                    error,
                    res: this.res,
                })
            })
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof ProgramsCtrl
     */
    resgisterProgramToAthlete = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        const { programDocId, entityDocId, startDate } = <
            { programDocId: string; entityDocId: string; startDate: Date }
            >req.body

        console.log(req.body)

        this.res = res
        const me = this
        const realStartDate = isDate(startDate) ? startDate : new Date()

        return db
            .collection(FirestoreCollection.Athletes)
            .doc(entityDocId)
            .get()
            .then((documentSnapshot: FirebaseFirestore.DocumentSnapshot) => {
                const athlete = {
                    ...documentSnapshot.data(),
                    uid: documentSnapshot.id,
                } as Athlete

                const registredProgram = athlete.currentPrograms.filter(
                    prog => prog.uid === programDocId
                )

                if (!registredProgram.length) {
                    return db
                        .collection(FirestoreCollection.Programs)
                        .doc(programDocId)
                        .get()
                        .then((docSnapshot: firestore.DocumentSnapshot) => {
                            console.log('realStartDate', realStartDate)

                            athlete.currentPrograms.push({
                                ...docSnapshot.data(),
                                // startDate: realStartDate, // TODO: !!!
                                uid: docSnapshot.id,
                            } as Program)

                            return documentSnapshot.ref
                                .update(athlete)

                                .then(() => {
                                    return me.handleResponse({
                                        message: 'Program added',
                                        responseCode: 201,
                                        data: {
                                            // program: theProgram,
                                            // writeResult,
                                        },
                                        error: undefined,
                                        method: {
                                            name: 'addProgram',
                                            line: 58,
                                        },
                                        res: this.res,
                                    })
                                })
                        })
                } else {
                    return me.handleResponse({
                        message: 'Already registered with Program',
                        responseCode: 204,
                        method: {
                            name: 'addProgram',
                            line: 100,
                        },
                        res: this.res,
                    })
                }
            })
            .catch(err => {
                return res.status(502).json({
                    message: 'Errror adding Program to Athlete',
                    responseCode: 201,
                    data: {
                        // program: program,
                    },
                    error: err,
                    method: {
                        name: 'addProgram',
                        line: 100,
                    },
                })
            })
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof ProgramsCtrl
     */

    addContentToAthleteProgram = async (
        req: Request,
        res: Response
    ) => {
        const { programDocId, contentItems, contentIndex } = <
            { programDocId: string; contentItems: Array<ProgramDayEntry>, contentIndex: number }
            >req.body

        console.log(req.body)

        try {
            this.res = res




            const allUsers = await db.collection(FirestoreCollection.Athletes).get().then((querySnap) => {
                return querySnap.docs.map((docSnap) => getDocumentFromSnapshot(docSnap) as Athlete)
            })





            // await admin.auth().listUsers(maxResults).then((userRecords) => {
            //     allUsers = userRecords.users.map((user) => user.toJSON());
            //     // res.end('Retrieved users list successfully.');
            // }).catch((error) => console.log(error));

            type programContentUpdateResult = {
                message: string
                responseCode: number
                data: {}
                error: any
                method: {
                    name: string
                    line: number
                }
                res: Response
            }

            const allDocs: Promise<programContentUpdateResult>[] = [].concat(...allUsers.map(async (user: Athlete) => {

                const updateResponse = await db.collection(FirestoreCollection.Athletes)
                    .doc(user.uid)
                    .collection(FirestoreCollection.CurrentPrograms)
                    .doc(programDocId)

                    .get().then((snap) => {
                        const program = { ...snap.data(), uid: snap.id } as Program

                        if (program.content) {
                            let itemsAdded = false

                            const dayIndexLength = program.content.length

                            contentItems.forEach((item, index) => {
                                if (!program.content[contentIndex].entries.find((c) => c.docGuid === item.docGuid)) {
                                    itemsAdded = true;
                                    program.content[contentIndex].entries.push({
                                        ...item,
                                        dayIndex: dayIndexLength + 1 + index
                                    })
                                }
                            })


                            if (itemsAdded) {
                                return snap.ref.update({
                                    content: program.content
                                }).then(() => {
                                    return {
                                        message: 'Program Content added',
                                        responseCode: 201,
                                        data: {
                                            athlteUID: user.uid
                                        },
                                        error: undefined,
                                        method: {
                                            name: 'addContentToAthleteProgram',
                                            line: 674,
                                        },
                                        // res: this.res,
                                    }
                                }).catch(err => {
                                    return {
                                        message: 'Errror adding Program Content to Athlete',
                                        responseCode: 500,
                                        data: {
                                            // program: program,
                                        },
                                        error: err,
                                        method: {
                                            name: 'addContentToAthleteProgram',
                                            line: 674,
                                        },
                                    }
                                })
                            } else {
                                return {
                                    message: 'Content Items Already Excists',
                                    responseCode: 201,
                                    data: {
                                        athlteUID: user.uid
                                    },
                                    error: undefined,
                                    method: {
                                        name: 'addContentToAthleteProgram',
                                        line: 100,
                                    },
                                }
                            }



                        } else {
                            return {
                                message: 'Program not found for Athlete',
                                responseCode: 201,
                                data: {
                                    athlteUID: user.uid
                                },
                                error: undefined,
                                method: {
                                    name: 'addContentToAthleteProgram',
                                    line: 100,
                                },
                            }
                        }
                    })

                return updateResponse
            }))


            const allRes = await Promise.all(allDocs)

            return res.status(200).json({
                message: 'Program Content Update Results',
                responseCode: 201,
                data: allRes,

                method: {
                    name: 'addContentToAthleteProgram',
                    line: 672,
                },
            })
        } catch (error) {
            return res.status(502).json({
                message: 'Errror Updating Program content',
                responseCode: 201,
                data: {
                    // program: program,
                },
                error,
                method: {
                    name: 'addContentToAthleteProgram',
                    line: 672,
                },
            })
        }

    }


    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof ProgramsCtrl
     */
    registerAthlete = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        const { programDocId, entiryId } = <
            { programDocId: string; entiryId: string }
            >req.body

        console.log(req.body)

        const me = this

        this.res = res
        return db
            .collection(FirestoreCollection.Athletes)
            .doc(entiryId)
            .get()
            .then((documentSnapshot: FirebaseFirestore.DocumentSnapshot) => {
                const athlete = {
                    ...documentSnapshot.data(),
                    uid: documentSnapshot.id,
                } as Athlete

                const registredProgram = athlete.currentPrograms.filter(
                    prog => prog.uid === programDocId
                )

                if (!registredProgram.length) {
                    return db
                        .collection(FirestoreCollection.Programs)
                        .doc(programDocId)
                        .get()
                        .then((docSnapshot: firestore.DocumentSnapshot) => {
                            const theProgram = docSnapshot.data() as Program

                            // theProgram.startDate = new Date((theProgram.startDate as any));

                            athlete.currentPrograms.push({
                                ...theProgram,
                                uid: docSnapshot.id,
                            } as Program)

                            return documentSnapshot.ref
                                .update(athlete)

                                .then(writeResult => {
                                    return me.handleResponse({
                                        message: 'Program added',
                                        responseCode: 201,
                                        data: {
                                            // program: program,
                                            writeResult,
                                        },
                                        error: undefined,
                                        method: {
                                            name: 'addProgram',
                                            line: 58,
                                        },
                                        res: this.res,
                                    })
                                })
                        })
                } else {
                    return me.handleResponse({
                        message: 'Already registered with Program',
                        responseCode: 204,
                        method: {
                            name: 'addProgram',
                            line: 100,
                        },
                        res: this.res,
                    })
                }
            })
            .catch(err => {
                return res.status(502).json({
                    message: 'Errror adding Program to Athlete',
                    responseCode: 201,
                    data: {
                        // program: program,
                    },
                    error: err,
                    method: {
                        name: 'addProgram',
                        line: 100,
                    },
                })
            })
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof ProgramsCtrl
     */
    registerGroup = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        const { programDocId, entityId } = <
            { programDocId: string; entityId: string }
            >req.body

        console.log(req.body)

        const me = this

        this.res = res

        return db
            .collection(FirestoreCollection.Groups)
            .doc(entityId)
            .get()
            .then((documentSnapshot: FirebaseFirestore.DocumentSnapshot) => {
                const group = {
                    ...documentSnapshot.data(),
                    uid: documentSnapshot.id,
                } as Group

                const registredProgram = group.currentPrograms.filter(
                    prog => prog.uid === programDocId
                )

                if (!registredProgram.length) {
                    return db
                        .collection(FirestoreCollection.Programs)
                        .doc(programDocId)
                        .get()
                        .then((docSnapshot: firestore.DocumentSnapshot) => {
                            const theProgram = docSnapshot.data() as Program

                            // theProgram.startDate = new Date((theProgram.startDate as any));

                            group.currentPrograms.push({
                                ...theProgram,
                                uid: docSnapshot.id,
                            } as Program)

                            return documentSnapshot.ref
                                .update(group)

                                .then(writeResult => {
                                    return me.handleResponse({
                                        message: 'Program added To Group',
                                        responseCode: 201,
                                        data: {
                                            // group: group,
                                            writeResult,
                                        },
                                        error: undefined,
                                        method: {
                                            name: 'registerGroup',
                                            line: 342,
                                        },
                                        res: this.res,
                                    })
                                })
                        })
                } else {
                    return me.handleResponse({
                        message: 'Group Already registered with Program',
                        responseCode: 204,
                        method: {
                            name: 'registerGroup',
                            line: 342,
                        },
                        res: this.res,
                    })
                }
            })
            .catch(err => {
                return res.status(502).json({
                    message: 'Errror adding Program to Group',
                    responseCode: 201,
                    data: {
                        // program: program,
                    },
                    error: err,
                    method: {
                        name: 'registerGroup',
                        line: 342,
                    },
                })
            })
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof ProgramsCtrl
     */
    registerOrganization = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        const { entityId } = <
            { programDocId: string; entityId: string }
            >req.body

        console.log(req.body)

        const me = this

        this.res = res

        return db
            .collection(FirestoreCollection.Organizations)
            .doc(entityId)
            .get()
            .then((documentSnapshot: FirebaseFirestore.DocumentSnapshot) => {

                // const registredProgram = organization.currentPrograms.filter(
                //     prog => prog.uid === programDocId
                // )

                // if (!registredProgram.length) {
                //     return db
                //         .collection(FirestoreCollection.Programs)
                //         .doc(programDocId)
                //         .get()
                //         .then((docSnapshot: firestore.DocumentSnapshot) => {
                //             const theProgram = docSnapshot.data() as Program

                //             // theProgram.startDate = new Date((theProgram.startDate as any));

                //             organization.currentPrograms.push({
                //                 ...theProgram,
                //                 uid: docSnapshot.id,
                //             } as Program)

                //             return documentSnapshot.ref
                //                 .update(organization)

                //                 .then(writeResult => {
                //                     return me.handleResponse({
                //                         message:
                //                             'Program added to Organization',
                //                         responseCode: 201,
                //                         data: {
                //                             //  organization: organization,
                //                             writeResult,
                //                         },
                //                         error: undefined,
                //                         method: {
                //                             name: 'resgisterOrganization',
                //                             line: 431,
                //                         },
                //                         res: this.res,
                //                     })
                //                 })
                //         })
                // } else {
                return me.handleResponse({
                    message: 'Organization Already registered with Program',
                    responseCode: 204,
                    method: {
                        name: 'resgisterOrganization',
                        line: 431,
                    },
                    res: this.res,
                })
                // }
            })
            .catch(err => {
                return res.status(502).json({
                    message: 'Errror adding Program to Organization',
                    responseCode: 201,
                    data: {
                        // program: program,
                    },
                    error: err,
                    method: {
                        name: 'resgisterOrganization',
                        line: 431,
                    },
                })
            })
    }
}
