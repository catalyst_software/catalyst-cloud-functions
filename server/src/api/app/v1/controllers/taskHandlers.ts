import { InAppMessageTemplate } from './../callables/schedule-send-notifications';
import BaseCtrl from './base';
import { Response, Request } from 'express';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { Athlete } from '../../../../models/athlete/athlete.model';
import { DocumentReference, DocumentSnapshot } from '@google-cloud/firestore';
import { ResponseObject } from '../../../../shared/ResponseObject';
import { ContentFeedCardPackageBase } from '../../../../models/report-model';
import { ScheduleNotificationCardDeliveryPayload } from '../../../../db/taskQueue/interfaces/scheduled-task-payload.interface';
import { sendNotification } from './utils/send-message';
import { ScheduleNotificationPayload } from '../../../../models/notifications/interfaces/schedule-notification-payload';
import { PushNotificationType } from '../../../../models/enums/push-notification-type';
import { handleInAppNotifications } from '../callables/utils/comms/handle-comms';

export default class TaskHandlerCtrl extends BaseCtrl {
    model = Athlete;
    collection = FirestoreCollection.TaskQueue;
    res: Response;

    constructor() {
        super();
    }

    scheduledNotiticationCardTaskHandler = async (req: Request, res: Response) => {
        this.res = res;

        try {
            // const dataBuffer = Buffer.from(req.body, 'base64');
            // const card = JSON.parse(dataBuffer.toString('utf8')) as ContentFeedCardPackageBase;
            const payload = req.body as ScheduleNotificationCardDeliveryPayload;
            const card = payload.card;
            console.log(`Cloud task http handler scheduledNotiticationCardTaskHandler called with payload: ${JSON.stringify(card)}`);
            card.creationTimestamp = new Date();

            const result = await this.db.collection(FirestoreCollection.NotificationCardRegistry)
                                    .add(card)
                                    .then(() => {
                                        console.log('Card successfully addded to notification registry!');
                                        return true;
                                    })
                                    .catch((err) => {
                                        console.error(`Card write to notification registry failed: ${err.message}`);
                                        return false;
                                    })

            if (result) {
                const template = payload.essentialCommsTemplate;
                const response = await handleInAppNotifications({
                    senderName: template.senderName,
                    recipients: template.recipients,
                    cardType: template.cardType,
                    cardTitle: template.cardTitle,
                    notificationPath: template.notificationPath,
                    notificationType: PushNotificationType.NewContentFeedItem,
                    icon: template.icon,
                    sendSMS: template.sendSMS || false,
                    useTopic: template.useTopic || false,
                    topicIds: template.topicIds
    
                });
                res.sendStatus(200).end();
            } else {
                res.sendStatus(400).end();
            }
        } catch (exception) {
            console.error('catchAll exception!!!! => ', exception)
            // return res.status(505).send(exception);
            res.sendStatus(400).end();
        }
    }
}