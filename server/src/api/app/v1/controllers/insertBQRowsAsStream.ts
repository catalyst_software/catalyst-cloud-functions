// Import the Google Cloud client library
// const { BigQuery } = require('@google-cloud/bigquery');

import { listTables } from "../callables/utils/big-query/list-tables";
import { createTable } from "../callables/utils/big-query/create-table";
import { removeUndefinedProps } from "../../../../db/ipscore/services/save-aggregates";
import moment from "moment";
import { BigQuery } from "@google-cloud/bigquery";
import { bigqueryClient } from "../callables/utils/big-query/initBQ";


const asas = {
    "ipScoreConfiguration": {},
    "firebaseMessagingIds": [
        "ck378Yae1BI:APA91bH66FJRUsykhgoMpUzC2thoC-G9Q2qexq03bsOTV-t1vAhcbfsR-DuM4rHF3u6TFX_ujgxr47_QwibQRNsqoWGlD6jVxAt4zLH3nJxKnNjBpf6cH_kal1foz0qjHCcW3mVqb3w8"
    ],
    "firebaseMessagingId": "ck378Yae1BI:APA91bH66FJRUsykhgoMpUzC2thoC-G9Q2qexq03bsOTV-t1vAhcbfsR-DuM4rHF3u6TFX_ujgxr47_QwibQRNsqoWGlD6jVxAt4zLH3nJxKnNjBpf6cH_kal1foz0qjHCcW3mVqb3w8",
    "transactionIds": [
        "580000333698574"
    ],
    "currentPrograms": [
        {
            "categoryType": 0,
            "description": "Welcome to iNSPIRE, we do not want to take up much of your time but we have a quick video just to introduce you to iNSPIRE and get you moving on your iNSPIRE journey.",
            "name": "Welcome to iNSPIRE!",
            "callToActionText": "Kick it off!",
            "activeDate": {
                "_seconds": 1546300800,
                "_nanoseconds": 0
            },
            "subscriptionType": 0,
            "programDays": 1,
            "tags": [],
            "programGuid": "bc1f3586-14d4-42a7-a6a6-ce9e251bb15e",
            "isPlaceHolder": true,
            "creationTimestamp": {
                "_seconds": 1558679697,
                "_nanoseconds": 822000000
            },
            "publisher": "INSPIRE",
            "images": [
                {
                    "url": "INSPIRE%2Fimages%2Fbc1f3586-14d4-42a7-a6a6-ce9e251bb15e.png"
                }
            ],
            "content": [
                {
                    "description": "Before you pick your first program have a look at our introduction video to get you rolling",
                    "entries": [
                        {
                            "embed": true,
                            "title": "Welcome to iNSPIRE!",
                            "docGuid": "bfaf8614-c841-45c6-9855-0862ad5d5ee8",
                            "fileGuid": "cc0ec6b9-61da-4a37-a049-768818a8fed3",
                            "description": "I am going to take you through how to use the app to get the most out of it! Ready?",
                            "engagement": false,
                            "strategyGuid": "a61d081a-f73e-4646-ab09-09bf2fc6e5d9",
                            "type": 1,
                            "version": 1
                        }
                    ],
                    "label": "Day 1"
                }
            ]
        }
    ],
    "isCoach": false,
    "uid": "ljSnZu2DRrOVW35AP7LBgxff4wj2",
    "runningTotalIpScore": 2.5357144,
    "currentIpScoreTracking": {
        "programs": {
            "surveyConsumed": false,
            "videoConsumed": false,
            "recordedDailyEntries": 0,
            "worksheetConsumed": false
        },
        "directive": {
            "paths": [],
            "includeGroupAggregation": false,
            "execute": false,
            "utcOffset": 570
        },
        "qualitativeWellness": {
            "include": false
        },
        "quantitativeWellness": {
            "food": {
                "components": {
                    "tracking": [],
                    "recordedDailyEntries": 0
                }
            },
            "brain": {
                "sleep": {
                    "tracking": [],
                    "recordedDailyEntries": 0
                },
                "mood": {
                    "tracking": [],
                    "recordedDailyEntries": 0
                }
            },
            "body": {
                "pain": {
                    "tracking": [],
                    "recordedDailyEntries": 0
                },
                "fatigue": {
                    "tracking": [],
                    "recordedDailyEntries": 0
                },
                "wearables": {
                    "tracking": [],
                    "recordedDailyEntries": 0
                }
            },
            "training": {
                "components": {
                    "tracking": [],
                    "recordedDailyEntries": 0
                }
            },
            "include": true
        },
        "ipScore": 0,
        "dateTime": {
            "_seconds": 1562682600,
            "_nanoseconds": 0
        }
    },
    "organizations": [
        {
            "organizationName": "Simon Black Academy",
            "organizationId": "EENrKqTlFw8i9MgC3gWh",
            "organizationCode": "SIMONBLACK",
            "active": true
        }
    ],
    "profile": {
        "firstName": "Jayden",
        "dob": "06/02/1998",
        "bio": "Healthy living ",
        "fullName": "Jayden Guy",
        "imageUrl": "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=3072097539482706&height=100&width=100&ext=1556346708&hash=AeQVwgieQ1rqw_3I",
        "sport": "AFL",
        "subscription": {
            "analytics": {
                "action": "IAP_RENEWAL_COM_INSPIRESPORTONLINE_PROD_SUBSCRIPTIONS_BASIC_SUCCESS",
                "payload": {
                    "productIdentifier": "com.inspiresportonline.prod.subscriptions.basic"
                },
                "pending": true
            },
            "commencementDate": {
                "_seconds": 1571768956,
                "_nanoseconds": 845000000
            },
            "status": 2,
            "expirationDate": {
                "_seconds": 1574191717,
                "_nanoseconds": 0
            },
            "type": 1,
            "isPrePaid": false
        },
        "onboardingSurveryCompleted": true,
        "onboardingCode": "SIMONBLACK",
        "sex": "Male",
        "email": "jayden_raw@live.com.au",
        "location": "Australia",
        "lastName": "Guy"
    },
    "metadata": {
        "firebaseMessagingId": "ck378Yae1BI:APA91bH66FJRUsykhgoMpUzC2thoC-G9Q2qexq03bsOTV-t1vAhcbfsR-DuM4rHF3u6TFX_ujgxr47_QwibQRNsqoWGlD6jVxAt4zLH3nJxKnNjBpf6cH_kal1foz0qjHCcW3mVqb3w8",
        "creationTimestamp": {
            "_seconds": 1553163108,
            "_nanoseconds": 0
        }
    },
    "organizationId": "EENrKqTlFw8i9MgC3gWh",
}
const schema = [
    { "name": "uid", type: "STRING", "mode": "REQUIRED" },
    { "name": "emailVerified", type: 'BOOL', "mode": "NULLABLE" },
    { "name": "organizationUID", type: "STRING", "mode": "REQUIRED" },
    { "name": "organizationName", type: "STRING", "mode": "NULLABLE" },
    { "name": "groupId", type: "STRING", "mode": "REQUIRED" },
    { "name": "groupName", type: "STRING", "mode": "NULLABLE" },
    { "name": "firstName", type: "STRING", "mode": "NULLABLE" },
    { "name": "lastName", type: "STRING", "mode": "NULLABLE" },
    { "name": "fullName", type: "STRING", "mode": "NULLABLE" },
    { "name": "email", type: "STRING", "mode": "REQUIRED" },
    { "name": "isPrePaid", type: 'BOOL', "mode": "NULLABLE" },
    { "name": "subscriptionStatus", type: "STRING", "mode": "NULLABLE" },
    { "name": "subscriptionCommencementDate", type: "DATETIME", "mode": "NULLABLE" },
    { "name": "subscriptionExpiry", type: "DATETIME", "mode": "NULLABLE" },
    { "name": "runningIPScore", type: 'FLOAT64', "mode": "NULLABLE" },
    { "name": "creationTimestamp", type: "DATETIME", "mode": "NULLABLE" },
    { "name": "daysSinceLastEngagement", type: 'FLOAT64', "mode": "NULLABLE" },
    { "name": "weeksSinceLastEngagement", type: 'FLOAT64', "mode": "NULLABLE" },
    { "name": "monthsSinceLastEngagement", type: 'FLOAT64', "mode": "NULLABLE" },
    { "name": "validateSubscription", type: 'BOOL', "mode": "NULLABLE" },
    { "name": "programName", type: 'STRING', "mode": "NULLABLE" },



    // {
    //     "name": "transactionIds", type: 'RECORD', "mode": "REPEATED",
    //     "fields": [
    //         {
    //             "name": "status",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "address",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "city",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "state",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "zip",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "numberOfYears",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         }
    //     ]
    // },
    // {
    //     "name": "currentPrograms", type: 'RECORD', "mode": "REPEATED",
    //     "fields": [
    //         {
    //             "name": "status",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "address",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "city",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "state",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "zip",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "numberOfYears",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         }
    //     ]
    // },

    // {
    //     "name": "addresses",
    //     "type": "RECORD",
    //     "mode": "REPEATED",
    //     "fields": [
    //         {
    //             "name": "status",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "address",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "city",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "state",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "zip",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         },
    //         {
    //             "name": "numberOfYears",
    //             "type": "STRING",
    //             "mode": "NULLABLE"
    //         }
    //     ]
    // }
]

export const insertRowsAsStream = async (rows: [{ creationTimestamp, subscriptionCommencementDate, subscriptionExpiry }]) => {
    // Inserts the JSON objects into my_dataset:my_table.

    /**
     * TODO(developer): Uncomment the following lines before running the sample.
     */
    const datasetId = 'weekly_athletes_export';
    const tableId = moment().format('YYYYMMDD') //'040306';
    const tables = await listTables(datasetId)

    if (!tables.find((t) => t.id === tableId)) {

        // const schema = "Name:string, Age:integer, Weight:float, IsMagic:boolean"

        await createTable(
            datasetId,
            tableId,
            schema
        )
    }

    // rows = rows.filter((s) => s)


    rows.forEach((athlete) => {


        const datetime = BigQuery.datetime({
            year: 2017,
            month: 1,
            day: 1,
            hours: 14,
            minutes: 0,
            seconds: 0
        });
        const m = moment;
        // const  ts = firestore.Timestamp.fromDate(athlete.creationTimestamp)
        const creationTimestamp = moment(athlete.creationTimestamp).format('YYYY-M-D h:m:s')
        const subscriptionCommencementDate = athlete.subscriptionCommencementDate !== 'UNKNOWN' ? moment(athlete.subscriptionCommencementDate).format('YYYY-M-D h:m:s') : undefined
        const subscriptionExpiry = athlete.subscriptionExpiry !== 'UNKNOWN' ? moment(athlete.subscriptionExpiry).format('YYYY-M-D h:m:s') : undefined

        // const ttt = BigQuery.datetime(creationTimestamp) 
        athlete.creationTimestamp = creationTimestamp// datetime// BigQuery.datetime(creationTimestamp) // BigQuery.datetime('2017-01-01 13:00:00')// / 1000//moment(athlete.creationTimestamp).format('YYYY-[M]M-[D]D[( |T)[H]H:[M]M:[S]S[.DDDDDD]]')
        athlete.subscriptionExpiry = subscriptionExpiry //BigQuery.datetime(subscriptionExpiry) // moment(athlete.subscriptionExpiry).toDate().toJSON() /// 1000//moment(athlete.subscriptionExpiry).format('YYYY-[M]M-[D]D[( |T)[H]H:[M]M:[S]S[.DDDDDD]]')
        athlete.subscriptionCommencementDate = subscriptionCommencementDate; //BigQuery.datetime(subscriptionCommencementDate) // moment(athlete.subscriptionCommencementDate).toDate().toJSON()// / 1000 //moment(athlete.subscriptionCommencementDate).format('YYYY-[M]M-[D]D[( |T)[H]H:[M]M:[S]S[.DDDDDD]]')
        (athlete as any).name = undefined;
        (athlete as any).ipScoreConfiguration = undefined;
        (athlete as any).firebaseMessagingIds = undefined;
        (athlete as any).firebaseMessagingId = undefined;


        (athlete as any).runningIPScore = (athlete as any).runningIPScore === 'UNKNOWN' ? null : (athlete as any).runningIPScore
        // if (athlete.subscriptionCommencementDate.indexOf("Invalid") >= 0) {
        //     athlete.subscriptionCommencementDate = undefined
        // }
        // if (athlete.subscriptionExpiry.indexOf("Invalid") >= 0) {
        //     athlete.subscriptionExpiry = undefined
        // }
    })

    removeUndefinedProps(rows)
    // Insert data into a table


    // function chunk(array, size) {
    //     const chunked_arr = [];
    //     let copied = [...array]; // ES6 destructuring
    //     const numOfChild = Math.ceil(copied.length / size); // Round up to the nearest integer
    //     for (let i = 0; i < numOfChild; i++) {
    //         chunked_arr.push(copied.splice(0, size));
    //     }
    //     return chunked_arr;
    // }
    // const chunked = chunk(rows, 1000)
    // const allRes = await Promise.all(chunked.map(async (chunkedRows) => {
    const res = await bigqueryClient
        .dataset(datasetId)
        .table(tableId)
        .insert(rows).catch((e) => {
            console.log(e.errors)
            return e.errors
        });


    //     return res
    // }))


    console.log(`Inserted ${rows.length} rows`);
}
