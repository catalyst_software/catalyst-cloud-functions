import { ProgramCatalogue } from '../../../../db/programs/models/interfaces/program-catalogue'

export const SportsScience: ProgramCatalogue = {
    title: 'Sports Science',
    categoryType: 3,
    images: [
        {
            url:
                'INSPIRE%2Fimages%2F08696912-1579-4aff-9e07-353212dd3104.png?alt=media',
        },
    ],
    // programs: [
    //     {
    //         programGuid: 'cbe7730e-d0a0-4365-ad27-b24dd2017701',
    //         name: 'Self Recovery',
    //         publisher: 'iNSPIRE',
    //         description:
    //             '8 week module consisting of vital Recovery methods you can do at home or training. This is your foundation iNSPIRE Sports Science program and a prerequisite for Competition Preparation. ',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2Fcbe7730e-d0a0-4365-ad27-b24dd2017701.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-03-08').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 3,
    //         subscriptionType: 1,
    //         programDays: 56,
    //         tags: ['recovery'],
    //     },
    //     {
    //         programGuid: 'f451f32c-b74b-45fc-9725-a9060f5a2d02',
    //         name: 'Competition Preparation',
    //         publisher: 'iNSPIRE',
    //         description:
    //             '4 week module consisting of methods for Mental and Physical competition preparation. ',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2Ff451f32c-b74b-45fc-9725-a9060f5a2d02.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-03-30').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 3,
    //         subscriptionType: 1,
    //         programDays: 28,
    //         tags: ['competition'],
    //     },
    //     {
    //         programGuid: '7c1fa726-f161-4ace-a095-a738f42dc0db',
    //         name: 'Jump Higher',
    //         publisher: 'iNSPIRE',
    //         description: '',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2F7c1fa726-f161-4ace-a095-a738f42dc0db.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-05-31').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 3,
    //         subscriptionType: 2,
    //         programDays: 28,
    //         tags: ['psychology'],
    //     },
    //     {
    //         programGuid: '22c3a8ee-80ef-4978-ae0d-40e4d9c80b27',
    //         name: 'Get Stronger',
    //         publisher: 'iNSPIRE',
    //         description: '',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2F22c3a8ee-80ef-4978-ae0d-40e4d9c80b27.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-06-30').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 3,
    //         subscriptionType: 2,
    //         programDays: 28,
    //         tags: ['psychology'],
    //     },
    //     {
    //         programGuid: 'a293bceb-ba16-4818-8d41-3d61cb3a3b65',
    //         name: 'Protect my Landings',
    //         publisher: 'iNSPIRE',
    //         description: '',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2Fa293bceb-ba16-4818-8d41-3d61cb3a3b65.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-07-30').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 3,
    //         subscriptionType: 2,
    //         programDays: 28,
    //         tags: ['psychology'],
    //     },
    // ],
}
