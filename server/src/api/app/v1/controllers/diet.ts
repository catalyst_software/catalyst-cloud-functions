export const foodLexicon = {
    vegetables: [
        {
            name: 'kale',
            description: '1 cup cooked or raw',
            calorieCount: 10,
            definition:
                'Of all the super healthy greens, kale is king.  It is definitely one of the healthiest and most nutritious plant foods in existence.  Kale is loaded with all sorts of beneficial compounds, some of which have powerful medicinal properties.  One serving is composed of one cup cooked or raw of kale and comprises 10 calories.',
        },
        {
            name: 'spinach',
            description: '1 cup cooked or raw',
            calorieCount: 42,
            definition:
                'Eating spinach may benefit eye health, reduce oxidative stress, help prevent cancer, and reduce blood pressure levels.  One serving is composed of one cup cooked or raw of green leafy spinach and comprises 42 calories.',
        },
        {
            name: 'brussels sprouts',
            description: '1 cup chopped or 5 medium',
            calorieCount: 38,
            definition:
                'Brussels sprouts are low in calories but high in fiber, vitamins and minerals.  One serving is composed of one cup cooked or raw of brussels sprouts and comprises 38 calories.',
        },
        {
            name: 'broccoli',
            description: '1 cup',
            calorieCount: 31,
            definition:
                'Broccoli is a good source of fibre and protein, and contains iron, potassium, calcium, selenium and magnesium as well as the vitamins A, C, E, K and a good array of B vitamins including folic acid.  One serving is composed of one cup of broccoli and comprises 31 calories.',
        },
        {
            name: 'asparagus',
            description: '10 large spears',
            calorieCount: 40,
            definition:
                'Asparagus is a great source of nutrients, including fiber, folate and vitamins A, C and K. Additionally, eating asparagus has a number of potential health benefits, including weight loss, improved digestion, healthy pregnancy outcomes and lower blood pressure.  One serving is composed of 10 large spears and comprises 40 calories.',
        },
        {
            name: 'beetroot',
            description: '2 medium',
            calorieCount: 58,
            definition:
                'Packed with essential nutrients, beetroots are a great source of fiber, folate (vitamin B9), manganese, potassium, iron, and vitamin C. Beetroots and beetroot juice have been associated with numerous health benefits, including improved blood flow, lower blood pressure, and increased exercise performance.  One serving is composed of 2 medium beetroots and comprises 58 calories.',
        },
        {
            name: 'tomatoes',
            description: '1 cup chopped, 1 cup cherry, or 2 medium',
            calorieCount: 32,
            definition:
                'Tomatoes are the major dietary source of the antioxidant lycopene, which has been linked to many health benefits, including reduced risk of heart disease and cancer. They are also a great source of vitamin C, potassium, folate, and vitamin K.  One serving is composed of 1 cup chopped, 1 cup cherry, or 2 medium tomatoes and comprises 32 calories.',
        },
        {
            name: 'squash',
            description: '1 cup cubed',
            calorieCount: 62,
            definition:
                "Also known as summer squash, yellow varieties of squash provide numerous health benefits. The vegetable is high in vitamins A, B6, and C, folate, magnesium, fiber, riboflavin, phosphorus, and potassium. That's a serious nutritional power-packed veggie.  One serving is composed of 1 cup cubed of squash and comprises 62 calories.",
        },
        {
            name: 'string beans',
            description: '1 cup',
            calorieCount: 31,
            definition:
                "String beans are high in vitamin K, and they also contain a decent amount of calcium. These nutrients are important for maintaining strong, healthy bones and reducing your risk of fractures. Getting enough folate isn't just important during pregnancy. The B vitamin is also important for reducing depression.  One serving is composed of 1 cup and comprises 31 calories.",
        },
        {
            name: 'peppers',
            description: '1 cup sliced',
            calorieCount: 60,
            definition:
                "Red, Orange, and Yellow Bell Peppers are full of great health benefits—they're packed with vitamins and low in calories! They are an excellent source of vitamin A, vitamin C, and potassium. Bell Peppers also contain a healthy dose of fiber, folate, and iron.  One serving is composed of 1 cup sliced of peppers and comprises 60 calories.",
        },
        {
            name: 'carrots',
            description: '1 cup sliced or 10 medium baby',
            calorieCount: 50,
            definition:
                "Carrots are a good source of beta carotene, fiber, vitamin K1, potassium, and antioxidants ( 1 ). They also have a number of health benefits. They're a weight-loss-friendly food and have been linked to lower cholesterol levels and improved eye health.  One serving is composed of 1 cup sliced or 10 medium baby carrots and comprises 50 calories.",
        },
        {
            name: 'cauliflower',
            description: '1 cup chopped',
            calorieCount: 30,
            definition:
                'Cauliflower is a cruciferous vegetable that is naturally high in fiber and B-vitamins.  It provides antioxidants and phytonutrients that can protect against cancer. Cauliflower also contains fiber to enhance weight loss and digestion, choline that is essential for learning and memory, and many other important nutrients.  One serving is composed of 1 cup chopped and comprises 30 calories.',
        },
        {
            name: 'artichokes',
            description: '1/2 Large',
            calorieCount: 36,
            definition:
                'Artichokes are a versatile food, and although some would consider them a vegetable, they are actually a variety of thistle. For nutritional purposes, they are primarily consumed due to their associated benefits of bolstering the immune system strength, lowering cholesterol, and protecting against diseases such as atherosclerosis, heart attack, and stroke. Artichokes have also long been famous for detoxifying the body and improving the health of the liver and aiding in digestive issues like indigestion, constipation, irritable bowel syndrome (IBS), and diarrhea. They also have anti-cancer potential, besides reducing blood pressure, eliminating hangovers, and stimulating urination.  One serving is composed of 1 to 2 large artichokes and comprises 36 calories.',
        },
        {
            name: 'eggplant',
            description: '1/2 Medium',
            calorieCount: 50,
            definition:
                'Eggplants have a range of health benefits, including an ability to help build strong bones, reduce the symptoms of anemia, and increase cognition. Eggplant is also good for weight loss, managing diabetes,  improving cardiovascular health and the digestive system.  One serving is composed of 1 to 2 medium eggplants and comprises 50 calories.',
        },
        {
            name: 'turnip',
            description: '1 cup sliced',
            calorieCount: 50,
            definition:
                'Turnips are loaded with fiber and vitamins K, A, C, E, B1, B3, B5, B6, B2 and folate (one of the B vitamins), as well as minerals like manganese, potassium, magnesium, iron, calcium and copper. They are also a good source of phosphorus, omega-3 fatty acids and protein.  One serving is composed of 1 cup of sliced turnip and comprises 36 calories.',
        },
        {
            name: 'peas',
            description: '1 cup',
            calorieCount: 118,
            definition:
                'Peas are a good source of vitamins C and E, zinc, and other antioxidants that strengthen your immune system. Other nutrients, such as vitamins A and B and coumestrol, help reduce inflammation and lower your risk of chronic conditions, including diabetes, heart disease, and arthritis.  One serving is composed of 1 cup and comprises 118 calories.',
        },
        {
            name: 'cabbage',
            description: '1 cup chopped',
            calorieCount: 30,
            definition:
                'Cruciferous vegetables like cabbage, kale, and broccoli are notorious for being chock-full of beneficial nutrients. If you are trying to improve your diet, cruciferous vegetables are a good place to start.  The cabbage may help protect against radiation, prevent cancer, and reduce heart disease risk.  One serving is composed of 1 cup of chopped cabbage and comprises 30 calories.',
        },
        {
            name: 'cucumber',
            description: '1 cup sliced',
            calorieCount: 15,
            definition:
                'Cucumbers contain 96% of water. They are ideal for detoxification and preventing dehydration. Cucumbers are rich in phytonutrients and vitamin K. They are also a very good source of pantothenic acid and Molybdenum. They also contain copper, potassium, manganese, vitamin C, phosphorus, magnesium and vitamin B1.  One serving is composed of 1 cup of sliced cucumber and comprises 15 calories.',
        },
        {
            name: 'celery',
            description: '1 cup sliced',
            calorieCount: 15,
            definition:
                'Celery is a great source of important antioxidants.  Antioxidants protect cells, blood vessels, and organs from oxidative damage.  Celery contains vitamin C, beta carotene, and flavonoids, but there are at least 12 additional kinds of antioxidant nutrients found in a single stalk. It’s also a wonderful source of phytonutrients, which have been shown to reduce instances of inflammation in the digestive tract, cells, blood vessels, and organs.  One serving is composed of 1 cup of sliced celery and comprises 15 calories.',
        },
        {
            name: 'lettuce',
            description: '1 cup chopped',
            calorieCount: 10,
            definition:
                'Lettuce is one of the most widely consumed vegetables worldwide, but its nutritional value has been underestimated. Lettuce is low in calories, fat and sodium. It is a good source of fiber, iron, folate, and vitamin C. Lettuce is also a good source of various other health-beneficial bioactive compounds.  One serving is composed of 1 cup of chopped lettuce and comprises 10 calories.',
        },
        {
            name: 'mushrooms',
            description: '1 cup sliced',
            calorieCount: 15,
            definition:
                "Mushrooms are a rich, low calorie source of fiber, protein, and antioxidants. They may also mitigate the risk of developing serious health conditions, such as Alzheimer's, heart disease, cancer, and diabetes.  They're also great sources of selenium, copper, thiamin, magnesium and phosphorous.  One serving is composed of 1 cup of sliced mushrooms and comprises 15 calories.",
        },
        {
            name: 'radishes',
            description: '1 cup sliced',
            calorieCount: 19,
            definition:
                'Radishes are rich in antioxidants and minerals like calcium and potassium. Together, these nutrients help lower high blood pressure and reduce your risks for heart disease. The radish is also a good source of natural nitrates that improve blood flow.  One serving is composed of 1 cup of sliced radishes and comprises 19 calories.',
        },
        {
            name: 'onions',
            description: '1 cup chopped',
            calorieCount: 50,
            definition:
                'Onions contain antioxidants and compounds that fight inflammation, decrease triglycerides and reduce cholesterol levels — all of which may lower heart disease risk. Their potent anti-inflammatory properties may also help reduce high blood pressure and protect against blood clots.  One serving is composed of 1 cup of chopped onions and comprises 50 calories.',
        },
        {
            name: 'sprouts',
            description: '1 cup',
            calorieCount: 38,
            definition:
                'Sprouts are rich in a number of important nutrients. While the specific ratio of nutrients varies depending on the type of sprout, they generally contain high levels of folate, magnesium, phosphorus, and vitamin K. In fact, they have higher amounts of these nutrients than fully-grown versions of the same plants.  One serving is composed of 1 cup of sprouts and comprises 38 calories.',
        },
    ],
    fruits: [
        {
            name: 'raspberries',
            description: '1 cup',
            calorieCount: 64,
            definition:
                'Raspberries are low in calories but high in fiber, vitamins, minerals and antioxidants. They may protect against diabetes, cancer, obesity, arthritis and other conditions and may even provide anti-aging effects. Raspberries are easy to add to your diet and make a tasty addition to breakfast, lunch, dinner or dessert.  One serving is composed of 1 cup of raspberries and comprises 64 calories.',
        },
        {
            name: 'blueberries',
            description: '1 cup',
            calorieCount: 60,
            definition:
                "Blueberries have anti-aging and disease protection powers.  Research shows that among commonly consumed fruits and vegetables, blueberries rank as one of the highest in antioxidant activity.  The antioxidants in blueberries have been shown to curb inflammation and reduce oxidative stress. The latter occurs when there is an imbalance between the production of cell-damaging free radicals and the body's ability to counter their harmful effects. For this reason, blueberries are tied to fending off DNA damage and aging, and lowering the risk of several chronic diseases, including obesity, cancer, and type 2 diabetes.  One serving is composed of 1 cup of blueberries and comprises 60 calories.",
        },
        {
            name: 'blackberries',
            description: '1 cup',
            calorieCount: 62,
            definition:
                'Blackberries contain a wide array of important nutrients including potassium, magnesium and calcium, as well as vitamins A, C, E and most of our B vitamins. They are also a rich source of anthocyanins, powerful antioxidants that give blackberries their deep purple colour.  One serving is composed of 1 cup of blackberries and comprises 62 calories.',
        },
        {
            name: 'strawberries',
            description: '1 cup',
            calorieCount: 53,
            definition:
                'Strawberries an excellent source of vitamin C and manganese and also contain decent amounts of folate (vitamin B9) and potassium. Strawberries are very rich in antioxidants and plant compounds, which may have benefits for heart health, blood sugar control, and diabetes.  One serving is composed of 1 cup of strawberries and comprises 53 calories.',
        },
        {
            name: 'apple',
            description: '1 cup or one small',
            calorieCount: 57,
            definition:
                'Apples contain many essential nutrients, vitamins, and minerals. They are free of fat, sodium, and cholesterol. Antioxidants, potassium, dietary fiber, vitamin C, and a few vitamin B (niacin, vitamin B6) are responsible for the health benefits attributed to apples. The other important nutrients in apple include calcium, vitamin K, iron, copper, phosphorus, and magnesium. They are also packed with phytonutrients and flavonoids, like quercetin, epicatechin, phloridzin, and other polyphenolic compounds. They are energy-dense and water-rich fruits which helps you feel full. They are rightly called nutritional powerhouses.  One serving is composed of 1 cup of apple and comprises 57 calories.',
        },
        {
            name: 'orange',
            description: '1 cup sliced sections or 1 medium',
            calorieCount: 85,
            definition:
                "Oranges contain citrus phytochemicals that help support the body and protect us from conditions such as heart disease and cancer – they're also thought to have some anti-inflammatory, antiviral and antimicrobial benefits. Oranges are also a good source of fibre, B vitamins, vitamin A, calcium and potassium.  One serving is composed of 1 cup sliced sections or 1 medium orange and comprises 85 calories.",
        },
        {
            name: 'watermelon',
            description: '1 cup diced',
            calorieCount: 46,
            definition:
                'Watermelon is rich in an amino acid called citrulline that may help move blood through your body and can lower your blood pressure. Your heart also enjoys the perks of all the lycopene watermelon contains. Studies show that it may lower your risk of heart attacks.  One serving is composed of 1 cup diced watermelon and comprises 46 calories.',
        },
        {
            name: 'cantaloupe',
            description: '1 cup diced',
            calorieCount: 53,
            definition:
                'Cantaloupe is a rich food source of vitamins A and C. Vitamins A and C are both antioxidants that work to keep your body healthy. Antioxidants can have protective effects by neutralizing free radicals, which can damage DNA in cells and promote chronic inflammation in the body.  One serving is composed of 1 cup diced cantaloupe and comprises 53 calories.',
        },
        {
            name: 'apricots',
            description: '4 small',
            calorieCount: 60,
            definition:
                "Apricots are a great source of many antioxidants, including beta carotene and vitamins A, C, and E. What's more, they're high in a group of polyphenol antioxidants called flavonoids, which have been shown to protect against illnesses, including diabetes and heart disease.  One serving is composed of 4 small apricots and comprises 60 calories.",
        },
        {
            name: 'grapefruit',
            description: '1 cup sections or 1/2 large',
            calorieCount: 80,
            definition:
                'While low in calories, grapefruit contains a whole host of nutrients. In particular, they’re a good source of vitamin A, which is important for supporting the immune system and keeping the eyes and skin healthy, folate, which is key for a baby’s development during pregnancy, and vitamin C, helping to maintain healthy skin.  Grapefruit also contains some magnesium, potassium and calcium.  One serving is composed of 1 cup sections or one half of a large grapefruit and comprises 80 calories.',
        },
        {
            name: 'cherries',
            description: '1 cup',
            calorieCount: 51,
            definition:
                'Whether you like them sweet or tart, cherries pack a healthful punch. Cherries are low in calories and chock full of fiber, vitamins, minerals, nutrients, and other good-for-you ingredients. You’ll get vitamins C, A, and K. Each long-stemmed fruit delivers potassium, magnesium, and calcium too. They also bring antioxidants, like beta-carotene, and the essential nutrient choline.  One serving is composed of 1 cup of cherries and comprises 51 calories.',
        },
        {
            name: 'grapes',
            description: '1 cup',
            calorieCount: 62,
            definition:
                'Both red and green grapes contain a mix of vitamins including B vitamins and especially folate, which is the food-based form of folic acid, and is important for the health of our blood cells and cardiovascular health. They also contain some vitamin C, which is a potent antioxidant and helps support the function of the immune system, and vitamin E which also plays a role in skin and eye health.  One serving is composed of 1 cup of grapes and comprises 62 calories.',
        },
        {
            name: 'kiwi',
            description: '2 medium',
            calorieCount: 80,
            definition:
                'Kiwis are high in Vitamin C and dietary fiber and provide a variety of health benefits. This tart fruit can support heart health, digestive health, and immunity. The kiwi is a healthy choice of fruit and is rich with vitamins and antioxidants.  One serving is composed of 2 medium kiwis and comprises 80 calories.',
        },
        {
            name: 'mango',
            description: '1 cup sliced',
            calorieCount: 99,
            definition:
                'Mangoes contain high level of vitamin C, fibre and pectin making it a perfect fruit that helps in controlling high cholesterol level. Another benefit of eating mangoes is that it cleanses your skin from deep inside your body. It treats pores and gives a glow to your skin. Hence, eat mangoes to get a flawless skin.  One serving is composed of 1 cup of sliced mango and comprises 99 calories.',
        },
        {
            name: 'peach',
            description: '1 cup sliced or 1 large',
            calorieCount: 66,
            definition:
                'Fresh peaches are a moderate source of antioxidants and vitamin C which is required for the building of connective tissue inside the human body. Consumption of foods that are rich in vitamin C helps a person develop resistance against infections and helps to eliminate harmful free radicals that cause certain cancers.  One serving is composed of 1 cup sliced or 1 large peach and comprises 66 calories.',
        },
        {
            name: 'nectarine',
            description: '1 cup sliced or 1 large',
            calorieCount: 63,
            definition:
                'Scientifically called Prunus persica, Nectarine is the edible fruit of the peach tree. Great sources of vitamin C, beta-carotene, and lutein – nutrients, nectarines improve immunity, protect vision, prevent many fatal diseases, improve skin health, aid in digestion and fight against numerous abdominal ailments.  One serving is composed of 1 cup sliced or 1 large nectarine and comprises 63 calories.',
        },
        {
            name: 'pear',
            description: '1 cup sliced or 1 large',
            calorieCount: 96,
            definition:
                "Pears are rich in essential antioxidants, plant compounds, and dietary fiber. They pack all of these nutrients in a fat free, cholesterol free, 96 calorie package. As part of a balanced, nutritious diet, consuming pears could support weight loss and reduce a person's risk of cancer, diabetes, and heart disease.  One serving is composed of 1 cup sliced or 1 large pear and comprises 96 calories.",
        },
        {
            name: 'pineapple',
            description: '1 cup diced',
            calorieCount: 82,
            definition:
                'Pineapples are packed with nutrients, antioxidants and other helpful compounds, such as enzymes that can fight inflammation and disease. Pineapple and its compounds have been linked to many health benefits, including aiding digestion, boosting immunity and speeding up recovery from surgery, among others.  One serving is composed of 1 cup diced pineapple and comprises 82 calories.',
        },
        {
            name: 'banana',
            description: '1/2 large',
            calorieCount: 60,
            definition:
                'Bananas are an excellent source of potassium and supply vitamin B6, fibre and carbohydrate, and some vitamin C. Since they have a lower water content than most fruit, bananas typically have more calories as well as a higher sugar content compared to other non-tropical fruits.  One serving is composed of 1 half large banana and comprises 60 calories.',
        },
        {
            name: 'papaya',
            description: '1 cup diced',
            calorieCount: 62,
            definition:
                "Papayas contain high levels of antioxidants vitamin A, vitamin C, and vitamin E. Diets high in antioxidants may reduce the risk of heart disease. The antioxidants prevent the oxidation of cholesterol. When cholesterol oxidizes, it's more likely to create blockages that lead to heart disease.  One serving is composed of 1 cup diced papaya and comprises 62 calories.",
        },
        {
            name: 'figs',
            description: '2 small',
            calorieCount: 60,
            definition:
                'Figs are high in natural sugars, minerals and soluble fibre. Figs are rich in minerals including potassium, calcium, magnesium, iron and copper and are a good source of antioxidant vitamins A and K that contribute to health and wellness.  One serving is composed of 2 small figs and comprises 60 calories.',
        },
    ],
    protein: [
        {
            name: 'sardines',
            description: '7 medium, (fresh or canned in water)',
            calorieCount: 175,
            definition:
                'Sardines are an excellent source of vitamin B-12. This vitamin helps your cardiovascular system and gives you energy. In addition, these fish contain a healthy amount of vitamin D. Along with B-12, D is necessary for good bone health throughout your life.  Sardines may not be the first food that comes to mind when you think of brain health, but these fatty fish are brimming with docosahexaenoic acid (DHA) and eicosapentaenoic acid (EPA), forms of omega-3 fats also found in salmon and mackerel.   One serving is composed of 7 medium fresh or canned sardines and comprises 175 calories.',
        },
        {
            name: 'boneless, skinless chicken or turkey breast',
            description: '100gr cooked',
            calorieCount: 106,
            definition:
                'Chicken and turkey breast are lean and have the most protein by weight, making them ideal for people who want to lose weight, maintain muscle mass and improve recovery. Fattier cuts like the thigh, drumstick and wings have more calories, which make them better for people wanting to build muscle or gain weight.  One serving is 100 grams cooked chicken or turkey breast and comprises 106 calories.',
            aliases: ['chicken breast', 'turkey breast'],
        },
        {
            name: 'lean ground chicken or turkey',
            description: '100gr cooked',
            calorieCount: 160,
            definition:
                'Ground chicken and ground turkey are the real jack of all trades in the kitchen. From lasagna to burgers to chili, ground chicken and turkey work wonders in any recipe. Offering a lean, protein-packed alternative to the more commonly used ground beef or pork, they are lighter in taste, fat, and calories, and hold up in even the heartiest of recipes.  One serving is 100 grams cooked ground chicken or turkey and comprises 160 calories.',
            aliases: ['ground chicken', 'ground turkey'],
        },
        {
            name: 'fish',
            description: '100grs cooked',
            calorieCount: 180,
            definition:
                'Fish is a low-fat high quality protein. Fish is filled with omega-3 fatty acids and vitamins such as D and B2 (riboflavin). Fish is rich in calcium and phosphorus and a great source of minerals, such as iron, zinc, iodine, magnesium, and potassium. The American Heart Association recommends eating fish at least two times per week as part of a healthy diet. Fish is packed with protein, vitamins, and nutrients that can lower blood pressure and help reduce the risk of a heart attack or stroke.  One serving is composed of 100 grams cooked fish and comprises 180 calories.  Recommended types of fish include salmon, tuna, cod and halibut',
            aliases: ['tuna', 'salmon', 'cod', 'halibut'],
        },
        {
            name: 'lean ground beef',
            description: '100gr cooked',
            calorieCount: 175,
            definition:
                'Ground beef is a convenient way to include protein in your diet, and it contains important vitamins and minerals. It is the primary ingredient in many favorite foods from hamburgers to meatballs. But ground beef can be high in calories and saturated fat, and a high intake of red meat can come with health risks. The key to including it in your diet is moderation and managing portion size.  One serving is 100 grams cooked ground beef and comprises 175 calories.',
            aliases: ['ground beef'],
        },
        {
            name: 'eggs',
            description: '2 large',
            calorieCount: 156,
            definition:
                'Eggs have been a part of our diet for millennia, yet we’re still learning just how beneficial they can be to human health. Loaded with nutrients - some of them hard to come by from other food sources - eggs are often said to be the original superfood because of their many health benefits.  Without a doubt, there are a multitude of benefits to eating eggs every day. Not only do eggs provide high quality protein, they also contain 11 vitamins and minerals, omega-3 fatty acids and antioxidants. And this means they can make a valuable contribution to daily nutrient requirements.  Most of the protein in an egg can be found in the egg white, while the yolk contains healthy fats, vitamins, minerals and antioxidants.  One serving is composed of 2 large eggs and comprises 156 calories.',
        },
        {
            name: 'egg whites',
            description: '8 large',
            calorieCount: 136,
            definition:
                'Egg whites are high in protein yet low in calories, fat, and cholesterol — making them a good weight loss food.  In fact, egg whites pack around 67% of all the protein found in eggs.  Egg whites may also benefit those who have high protein requirements but need to watch their calorie intake, such as athletes or bodybuilders.  One serving is composed of egg white from 8 large eggs and comprise 136 calories.',
        },
        {
            name: 'greek yogurt',
            description: '3/4 cup plain',
            calorieCount: 133,
            definition:
                'Greek yogurt is an excellent source of calcium, which can help improve bone health. It also contains probiotics, which support a healthy bacterial balance in the gut. Eating Greek yogurt may be associated with lower blood pressure and a lower risk of type 2 diabetes.  One serving is composed of a 3/4 cup of plain greek yogurt and comprises 133 calories.',
        },
        {
            name: 'shellfish',
            description: '100gr cooked',
            calorieCount: 150,
            definition:
                'Shellfish — which can be divided into crustaceans and mollusks — are loaded with lean protein, healthy fats, and micronutrients.  They may aid weight loss, boost immunity, and promote brain and heart health. Shellfish may contain heavy metals and cause foodborne illness and allergic reactions.  Nevertheless, shellfish can be a nutritious and delicious addition to a balanced diet for the most healthy people.  One serving is composed of 100 grams cooked shellfish and comprises 150 calories.  Recommended types of shellfish include shrimp, crab, lobster and clams',
            aliases: ['shrimp', 'crab', 'lobster', 'clam'],
        },
        {
            name: 'red meat',
            description: '100gr cooked',
            calorieCount: 250,
            definition:
                'Red meat is very nutritious, especially if it comes from animals that have been naturally fed and raised. It’s a great source of protein, iron, B12, zinc, creatine and various other nutrients.  However, it’s important to make a distinction between different kinds of red meat. For example, grass-fed and organic meat is nutritionally different than factory-farmed, processed meat, which may in gact be harmful to your health.  One serving is composed of 100 grams of cooked red meat and comprises 250 calories.',
        },
        {
            name: 'tofu',
            description: '3/4 cup diced',
            calorieCount: 210,
            definition:
                'Tofu is a good source of protein and contains all nine essential amino acids.   It is an excellent meat alternative for those on a plant-based diet.  Tofu is also a valuable plant source of iron and calcium and the minerals manganese and phosphorous. In addition to this, it also contains magnesium, copper, zinc and vitamin B1.  One serving is composed of a 3/4 cup diced tofu and comprises 210 calories.',
        },
        {
            name: 'pork tenderloin',
            description: '100gr cooked',
            calorieCount: 160,
            definition:
                "When you're looking for lean protein to include in your diet, or when you simply crave some healthy meat, pork tenderloin is a good choice. It's low in fat, high in protein and a rich source of B vitamins and selenium. Pork tenderloin works well with any cooking method -- just be sure to cook it well enough to eliminate any parasites or bacteria that may be in the raw meat.  One serving is composed of 100 grams of cooked pork tenderloin and comprises 160 calories.",
        },
        {
            name: 'canned tuna',
            description: '3/4 cup drained',
            calorieCount: 135,
            definition:
                'There are many benefits of eating canned tuna.  It is an inexpensive source of protein.  If you are looking to lose weight, canned tuna is a good option because it is low in calories yet high in protein.  Diets that are high in protein have been associated with benefits for weight loss, including increased feelings of fullness and reduced cravings.  Despite being low in fat, tuna is still considered a good source of omega-3 fatty acids.  Omega-3s are essential dietary fats that are beneficial for heart, eye, and brain health. Fish is considered an important source of these healthy fats in the diet, though you can also get omega-3s from plant foods.  One serving is composed of a 3/4 cup drained canned tuna and comprises 135 calories.',
        },
        {
            name: 'sliced turkey',
            description: '6 slices',
            calorieCount: 144,
            definition:
                'Turkey is a very rich source of protein, niacin, vitamin B6 and the amino acid tryptothan. Apart from these nutrients, it also contains zinc and vitamin B12. The skinless white meat of turkey is low in fat and is an excellent source of protein.  Turkey also contains anti-cancer properties. It is a very good source of the trace mineral selenium, which is an essential component required for thyroid hormone metabolism, antioxidant defence systems, and immune function. Scientific studies have suggested that selenium intake can bring down cancer incidence.  One serving is composed of 6 slices of turkey and comprises 144 calories.',
        },
        {
            name: 'ricotta cheese',
            description: '3/4 cup',
            calorieCount: 300,
            definition:
                'Ricotta is an Italian cheese made from the watery parts of cow, goat, sheep, or Italian water buffalo milk that are left over from making other cheeses. Ricotta has a creamy texture and is often described as a lighter version of cottage cheese.  The protein in ricotta cheese is mostly whey, a milk protein that contains all of the essential amino acids that humans need to obtain from food.  Whey is easily absorbed and may promote muscle growth, help lower blood pressure, and reduce high cholesterol levels.  One serving is composed of a 3/4 cup of ricotta cheese and comprises 300 calories.',
        },
        {
            name: 'cottage cheese',
            description: '3/4 cup',
            calorieCount: 122,
            definition:
                'Cottage cheese is a soft, white cheese made from the loose curds of cow’s milk. It’s thought to have originated in the United States.  Cottage cheese is much higher in protein than other cheeses.  Since cottage cheese is high in protein but low in calories, it is often recommended for weight loss.  Several studies indicate that eating high-protein foods like cottage cheese can increase feelings of fullness and help decrease overall calorie intake, which in turn may lead to weight loss.  One serving is composed of a 3/4 cup of cottage cheese and comprises 122 calories.',
        },
        {
            name: 'protein powder',
            description: 'typically 2 scoops',
            calorieCount: 242,
            definition:
                'Protein powder is a popular nutritional supplement. Protein is an essential macronutrient that helps build muscle, repair tissue, and make enzymes and hormones. Using protein powder may also aid weight loss and help people tone their muscles.  One serving is typically composed of two scoops of protein powder and comprises 242 calories.  Recommended types of protein powder include whey, hemp, rice and pea.',
        },
        {
            name: 'veggie burger',
            description: '1 medium patty',
            calorieCount: 130,
            definition:
                'Due to plant-based burgers being free from saturated and trans fat, they generally have a lower calorie content per gram when compared to a meat burger. People who choose a plant based diet tend to have a lower body fat content, less cholesterol and are able to maintain a healthy weight without really even trying!  One serving is composed of 1 medium patty veggie burger and comprises 133 calories.',
        },
        {
            name: 'turkey bacon',
            description: '4 slices',
            calorieCount: 110,
            definition:
                "Because turkey is leaner than pork belly, turkey bacon contains fewer calories and less fat than pork bacon. Both products come from animal proteins, so they're relatively good sources of B vitamins and minerals like zinc, selenium and phosphorus.  One serving is composed of 4 slices of turkey bacon and comprises 110 calories.",
        },
    ],
    carbohydrates: [
        {
            name: 'sweet potato',
            description: '1/2 cup diced',
            calorieCount: 72,
            definition:
                "Sweet potatoes are a rich source of fibre as well as containing an array of vitamins and minerals including iron, calcium, selenium, and they're a good source of most of our B vitamins and vitamin C.  One serving is composed of 1/2 cup of diced sweet potato and comprises 72 calories.",
        },
        {
            name: 'quinoa',
            description: '1/2 cup diced',
            calorieCount: 111,
            definition:
                'Quinoa is gluten-free, high in protein and one of the few plant foods that contain sufficient amounts of all nine essential amino acids.  It is also high in fiber, magnesium, B vitamins, iron, potassium, calcium, phosphorus, vitamin E and various beneficial antioxidants.  One serving is composed of 1/2 cup of diced quinoa and comprises 111 calories.',
        },
        {
            name: 'beans',
            description: '1/2 cup cooked and drained',
            calorieCount: 109,
            definition:
                'They are commonly eaten around the world and are a rich source of fiber and B vitamins.  They are also a great replacement for meat as an alternative source of protein.  Beans and legumes have a number of health benefits, including reducing cholesterol, decreasing blood sugar levels and increasing healthy gut bacteria.  One serving is composed of 1/2 cup cooked and drained beans and comprises 109 calories.',
            aliases: [
                'kidney beans',
                'black beans',
                'garbanzo beans',
                'white beans',
                'lima beans',
            ],
        },
        {
            name: 'lentils',
            description: '1/2 cup cooked and drained',
            calorieCount: 130,
            definition:
                'Brown, green, yellow, red or black, lentils are low in calories, rich in iron and folate and an excellent source of protein.  They pack health-promoting polyphenols and may reduce several heart disease risk factors.  One serving is composed of 1/2 cup of cooked and drained lentils and comprises 130 calories.',
        },
        {
            name: 'edamame',
            description: '1/2 cup shelled',
            calorieCount: 120,
            definition:
                'In addition to being a decent source of soy protein, edamame is rich in healthy fiber, antioxidants and vitamin K.  These plant compounds may reduce the risk of heart disease and improve the blood lipid profile, a measure of fats including cholesterol and triglycerides.  One serving is composed of 1/2 cup of cooked shelled edamame and comprises 120 calories.',
        },
        {
            name: 'peas',
            description: '1/2 cup cooked',
            calorieCount: 62,
            definition:
                'Peas are a good source of vitamins C and E, zinc, and other antioxidants that strengthen your immune system.  Other nutrients, such as vitamins A and B and coumestrol, help reduce inflammation and lower your risk of chronic conditions, including diabetes, heart disease, and arthritis.  One serving is composed of 1/2 cup of cooked peas and comprises 62 calories.',
        },
        {
            name: 'refined beans',
            description: '1/2 cup cooked',
            calorieCount: 120,
            definition:
                "Eating healthy refried beans two or three times a week is a great way to reduce the overall saturated fat and cholesterol in your diet, especially when you're following a vegan or vegetarian diet.  Rice and beans are also a classic combination that provides a complete protein source for vegetarians and vegans.  One serving is composed of 1/2 cup of cooked refined beans and comprises 120 calories.",
        },
        {
            name: 'brown rice or wild rice',
            description: '1/2 cup cooked',
            calorieCount: 108,
            definition:
                "Many of the nutrients in brown rice help keep your heart healthy.  It's a rich source of dietary fiber.  Brown rice also contains high levels of magnesium, which can help make you less vulnerable to heart disease and stroke.  Wild rice is also a wonderfully balanced food source, providing a healthy mix of protein and fiber.  What's more, it's also relatively low in calories.  One micronutrient abundant in wild rice is Manganese.  Manganese is an antioxidant, and plays a role in keeping the mitochondria in your cells healthy.  One serving is composed of 1/2 cup of cooked brown or wild rice and comprises 108 calories.",
            aliases: ['Brown rice', 'Wild rice'],
        },
        {
            name: 'potato',
            description: '65 grams cooked',
            calorieCount: 68,
            definition:
                'Potatoes are rich in vitamins, minerals and antioxidants, which make them very healthy.  Studies have linked potatoes and their nutrients to a variety of impressive health benefits, including improved blood sugar control and higher immunity.  One serving is composed of 65 grams of cooked potatoes and comprises 68 calories.',
        },
        {
            name: 'corn on the cob',
            description: '1 ear',
            calorieCount: 155,
            definition:
                'Corn has several health benefits.  Because of the high fiber content, it can aid with digestion.  It also contains valuable B vitamins, which are important to your overall health.  Corn also provides our bodies with essential minerals such as zinc, magnesium, copper, iron and manganese.  One serving is composed of 1 ear of cooked corn on the cob and comprises 155 calories.',
        },
        {
            name: 'millet',
            description: '1/2 cup cooked',
            calorieCount: 103,
            definition:
                'Millet is rich in dietary fiber, both soluble and insoluble.  The insoluble fiber in millet is known as a prebiotic, which means it supports good bacteria in your digestive system.  This type of fiber is also important for adding bulk to stools, which helps keep you regular and reduces your risk of colon cancer.  One serving is composed of 1/2 cup of cooked millet and comprises 103 calories.',
        },
        {
            name: 'amaranth',
            description: '1/2 cup cooked',
            calorieCount: 126,
            definition:
                'Amaranth is a nutritious, gluten-free grain that provides plenty of fiber, protein and micronutrients.  It has also been associated with a number of health benefits, including reduced inflammation, lower cholesterol levels and increased weight loss.  One serving is composed of 1/2 cup of cooked amaranth and comprises 126 calories.',
        },
        {
            name: 'buckwheat',
            description: '1/2 cup cooked',
            calorieCount: 77,
            definition:
                'Buckwheat is a highly nutritious whole grain that many people consider to be a superfood.  Among its health benefits, buckwheat may improve heart health, promote weight loss, and help manage diabetes.  Buckwheat is a good source of protein, fiber, and energy.  One serving is composed of 1/2 cup of cooked buckwheat and comprises 77 calories.',
        },
        {
            name: 'barely',
            description: '1/2 cup cooked',
            calorieCount: 99,
            definition:
                'Barley is high in fiber, especially beta-glucan, which may reduce cholesterol and blood sugar levels.  It may also aid weight loss and improve digestion.  Whole-grain, hulled barley is more nutritious than refined, pearled barley.  It can be substituted for any whole grain and easily added to your diet.  One serving is composed of 1/2 cup of cooked barely and comprises 99 calories.',
        },
        {
            name: 'bulgur',
            description: '1/2 cup cooked',
            calorieCount: 56,
            definition:
                "Bulgur is a whole grain made from cracked wheat.  It's packed with vitamins, minerals and fiber.  Fiber-rich foods like bulgur may reduce chronic disease risk, promote weight loss and improve digestion and gut health.  It's easy to cook and can be added to many dishes, including salads, stews and breads.  One serving is composed of 1/2 cup of cooked bulgur and comprises 56 calories.",
            aliases: ['bulgur wheat'],
        },
        {
            name: 'oatmeal',
            description: 'Steel cut or rolled, 1/2 cup cooked',
            calorieCount: 185,
            definition:
                "Oats are among the healthiest grains on earth.  They're a gluten-free whole grain and a great source of important vitamins, minerals, fiber and antioxidants.  Studies show that oats and oatmeal have many health benefits.  These include weight loss, lower blood sugar levels and a reduced risk of heart disease.  One serving is composed of 1/2 cup of cooked oatmeal, steel cut or rolled and comprises 185 calories.",
        },
        {
            name: 'pasta',
            description: 'Whole grain, 1/2 cup cooked',
            calorieCount: 100,
            definition:
                'Whole grain pasta is typically high in fiber, manganese, selenium, copper and phosphorus, while refined, enriched pasta tends to be higher in iron and B vitamins.  Whole-grain pasta is also lower in calories and higher in fiber and certain micronutrients than refined pasta.  One serving is composed of 1/2 cup of cooked whole grain pasta and comprises 100 calories.',
        },
        {
            name: 'couscous',
            description: 'Whole wheat, 1/2 cup cooked',
            calorieCount: 156,
            definition:
                'Whole-grain couscous is a good source of fiber.  Fiber is good for you in a lot of ways.  It can stop your blood sugar from spiking and can keep you fuller longer.  It also can help lower cholesterol, which can reduce your chances of heart disease.  One serving is composed of 1/2 cup of cooked whole wheat couscous and comprises 156 calories.',
        },
        {
            name: 'crackers',
            description: 'Whole grain, 8 small crackers',
            calorieCount: 150,
            definition:
                'Crackers are low in calories and pack in a good amount of dietary fiber to keep you feeling full.  One serving is composed of 8 small crackers and comprises 150 calories.',
        },
        {
            name: 'bread',
            description: 'Whole grain, 1 slice',
            calorieCount: 70,
            definition:
                "Bread is high in carbs, low in micronutrients, and its gluten and anti nutrient contents may cause issues for some people.   Still, it's often enriched with extra nutrients, and whole-grain or sprouted varieties may bestow several health benefits.  In moderation, bread can be enjoyed as part of a healthy diet.  Therefore opt for whole grains to provide more vitamins, minerals, and fiber than refined.  One serving is composed of 1 slice bread and comprises 70 calories.",
        },
        {
            name: 'pita bread',
            description: 'Whole wheat, 1 small 11 centimeters',
            calorieCount: 130,
            definition:
                'Whole wheat flour used in making pita bread is excellent for diabetics as they will not shoot up your blood sugar levels as they are a low GI food.  Whole wheat flour is rich in phosphorus which is a major mineral which works closely with calcium to build our bones.  One serving is composed of 1 small pita bread and comprises 130 calories.',
        },
        {
            name: 'waffles',
            description: 'Whole grain, 1 waffle',
            calorieCount: 130,
            definition:
                'Waffles can make a healthy breakfast when you build them up with high quality ingredients for nutrition and all around deliciousness.  One serving is composed of 1 whole grain waffle and comprises 130 calories.',
        },
        {
            name: 'pancakes',
            description: 'Whole grain, 1 small 11 centimetres',
            calorieCount: 100,
            definition:
                'Although plain pancakes are a good source of vitamins and minerals, whole-wheat pancakes have higher levels of calcium, iron, phosphorus and riboflavin.  One serving is composed of 1 whole grain pancake and comprises 100 calories.',
        },
        {
            name: 'english muffins',
            description: 'Whole grain, 1/2 muffin',
            calorieCount: 100,
            definition:
                "They're richer in unprocessed fiber, magnesium, zinc, potassium, and vitamins E and B-6, and they may lower the risk of heart disease and stroke.  One serving is composed of 1/2 whole grain English muffin and comprises 100 calories.",
        },
        {
            name: 'bagel',
            description: 'Whole grain, 1/2 small',
            calorieCount: 130,
            definition:
                'Some varieties of bagels are made from whole grains.  Whole grains are high in dietary fiber and antioxidants.  They have been found to offer some protection against diabetes, cancer, and obesity.  Whole grains also have B vitamins, iron, selenium, and magnesium.  One serving is composed of 1/2 small whole grain bagel and comprises 130 calories.',
        },
        {
            name: 'Tortilla',
            description: 'Whole wheat, 1 small 16 centimetres',
            calorieCount: 100,
            definition:
                'Whole wheat tortillas are healthiest of all!  They provide complex carbohydrates and fiber.  One serving is composed of 1 small whole wheat tortilla and comprises 100 calories.',
        },
    ],
    healthyFats: [
        {
            name: 'avocado',
            description: '35 grams or 1/4 cup',
            calorieCount: 58,
            definition:
                "Avocados are a great source of vitamins C, E, K, and B-6, as well as riboflavin, niacin, folate, pantothenic acid, magnesium, and potassium. They also provide lutein, beta-carotene, and omega-3 fatty acids. Although most of the calories in an avocado come from fat, don't shy away!  One serving is composed of 35 grams or 1/4 cup of avocado and comprises 58 calories.",
        },
        {
            name: 'almonds',
            description: '10 almonds',
            calorieCount: 70,
            definition:
                'Almonds contain lots of healthy fats, fiber, protein, magnesium and vitamin E. The health benefits of almonds include lower blood sugar levels, reduced blood pressure and lower cholesterol levels. They can also reduce hunger and promote weight loss.  One serving is composed of 10 almonds and comprises 70 calories.',
        },
        {
            name: 'cashews',
            description: '8 cashews',
            calorieCount: 72,
            definition:
                "Cashews are low in sugar and rich in fiber, heart-healthy fats, and plant protein. They're also a good source of copper, magnesium, and manganese nutrients important for energy production, brain health, immunity, and bone health.  One serving is composed of 8 cashews and comprises 72 calories.",
        },
        {
            name: 'pecan',
            description: '10 pecan halves',
            calorieCount: 98,
            definition:
                'Pecans are a good source of calcium, magnesium, and potassium, which help lower blood pressure. Most of the fat found in pecans is a healthy type called monounsaturated fat. Eating foods with monounsaturated fat instead of foods high in saturated fats, like potato chips, can help lower levels of bad LDL cholesterol.  One serving is composed of 10 pecan halves and comprises 98 calories.',
        },
        {
            name: 'walnuts',
            description: '8 walnuts',
            calorieCount: 104,
            definition:
                "Walnuts are rich in heart-healthy fats and high in antioxidants. What's more, regularly eating walnuts may improve brain health and reduce your risk of heart disease and cancer. These nuts are easily incorporated into your diet, as they can be eaten on their own or added to many different foods.  One serving is composed of 8 walnuts and comprises 104 calories.",
        },
        {
            name: 'pistachios',
            description: '12 pistachios',
            calorieCount: 48,
            definition:
                'Pistachios contain high levels of unsaturated fatty acids and potassium. Both have antioxidant and anti-inflammatory traits. They can lower your chances for cardiovascular disease. Pistachios are bursting with the fiber, minerals, and unsaturated fat that can help keep your blood sugar, blood pressure, and cholesterol in check.  One serving is composed of 12 pistachios and comprises 48 calories.',
        },
        {
            name: 'hummus',
            description: '35 grams or 1/4 cup',
            calorieCount: 165,
            definition:
                'Hummus is a popular Middle Eastern dip and spread that is packed with vitamins and minerals. Research has linked hummus and its ingredients to a variety of impressive health benefits, including helping fight inflammation, improving blood sugar control, better digestive health, lower heart disease risk and weight loss.  One serving is composed of 35 grams equivalent to 1/4 cup hummus and comprises 165 calories.',
        },
        {
            name: 'coconut milk',
            description: '1/4 cup canned',
            calorieCount: 142,
            definition:
                'People who consume coconut milk lowered their LDL or bad cholesterol while their HDL or good cholesterol increases. Healthy cholesterol levels are important for heart health. Coconut milk and cream are sources of healthy fats called medium-chain triglycerides. One serving is composed of 1/4 cup canned coconut and comprises 142 calories.',
        },
        {
            name: 'feta cheese',
            description: '1/4 cup crumbled',
            calorieCount: 99,
            definition:
                'While feta cheese provides you with an excellent source of nutrients like calcium and protein, it also contains high amounts of sodium and saturated fat. Feta is lower in fat than many other cheeses, however, and is considered a reasonable option to eat in moderation.  One serving is composed of 1/4 cup crumbled feta cheese and comprises 99 calories.',
        },
        {
            name: 'goat cheese',
            description: '1/4 cup crumbled',
            calorieCount: 126,
            definition:
                'Goat cheese is a nutritious dairy product that is packed with vitamins, minerals and healthy fats. Eating goat cheese may benefit your health in several ways, including increasing satiety and reducing inflammation. One serving is composed of 1/4 cup crumbled goat cheese and comprises 126 calories.',
        },
        {
            name: 'mozzarella',
            description: '1/4 cup shredded',
            calorieCount: 85,
            definition:
                "Mozzarella is a soft cheese that's lower in sodium and calories than most other cheeses. It also contains probiotics that may boost your immune system. Even if Mozzarella cheese can be high in fat, so you should only eat it in moderation. Pair it with some fruit for a great snack that will keep you full for a long time. One serving is composed of 1/4 cup shredded mozzarella and comprises 85 calories.",
        },
        {
            name: 'cheddar',
            description: '1/4 cup shredded',
            calorieCount: 114,
            definition:
                'Cheddar cheese is a good source of calcium, one of the most important nutrients for promoting bone health. People who maintain a diet rich in calcium are less likely to develop osteoporosis. The vitamin K in cheddar cheese also plays a valuable role in promoting bone health.  However, other dairy products, such as greek yoghurt, are just as good for the bones and much lower in fat and salt.  One serving is composed of 1/4 cup shredded cheddar and comprises 144 calories.',
        },
        {
            name: 'parmesan',
            description: '1/4 cup shredded',
            calorieCount: 108,
            definition:
                "Naturally low in fat, free of carbs, and lactose-free, Parmesan has plenty of health benefits.   Parmesan cheese is a good source of protein and fat.  It's rich in vitamins and minerals like calcium, vitamin A, vitamins B6 and B12, phosphorus, zinc, and copper.  One serving is composed of 1/4 cup shredded Parmesan and comprises 108 calories.",
        },
    ],
    seedsAndDressings: [
        {
            name: 'raw nuts',
            description: '2 tablespoons chopped',
            calorieCount: 78,
            definition:
                'Raw nuts are very healthy, but they might contain harmful bacteria. However, even if they do, it is unlikely to cause an illness. High in Omega-3 fatty acids, fiber and vitamin e. One serving is composed of 2 tablespoons of chopped nuts and comprises 78 calories.',
        },
        {
            name: 'raw seeds',
            description: '2 tablespoons chopped',
            calorieCount: 112,
            definition:
                ' Seeds are great sources of fiber. They also contain healthy monounsaturated fats, polyunsaturated fats and many important vitamins, minerals and antioxidants. When consumed as part of a healthy diet, seeds can help reduce blood sugar, cholesterol and blood pressure.  One serving is composed of 2 tablespoons of chopped seeds and comprises 112 calories.',
            aliases: ['pumpkin seeds', 'sesame seeds', 'sunflower seeds'],
        },
        {
            name: 'flaxseed',
            description: '2 tablespoons ground',
            calorieCount: 74,
            definition:
                'Though tiny, they are rich in the omega-3 fatty acid ALA, lignans and fiber, all of which have been shown to have many potential health benefits. They can be used to improve digestive health, lower blood pressure and bad cholesterol, reduce the risk of cancer and may benefit people with diabetes.  One serving is composed of 2 tablespoons of ground flaxseed and comprises 74 calories.',
        },
        {
            name: 'olives',
            description: '10 medium olives',
            calorieCount: 60,
            definition:
                "Olives are very high in vitamin E and other powerful antioxidants. Studies show that they are good for the heart and may protect against osteoporosis and cancer. The healthy fats in olives are extracted to produce olive oil, one of the key components of the incredibly healthy Mediterranean diet.  Their low calorie density means that they may aid weight loss by helping you feel full.  All the same, you should control for portion sizes because olives' calories can add up quickly.  One serving is composed of 10 medium olives and comprises 60 calories.",
        },
        {
            name: 'peanuts',
            description: '2 tablespoons',
            calorieCount: 104,
            definition:
                'Peanuts help prevent heart disease by lowering cholesterol levels.  They can also stop small blood clots from forming and reduce your risk of having a heart attack or stroke.  One serving is composed of 2 tablespoons of peanuts and comprises 104 calories.',
        },
        {
            name: 'coconut',
            description: 'Unsweetened, 2 tablespoons shredded',
            calorieCount: 90,
            definition:
                "Rich in fiber, it may offer a number of benefits, including improved heart health, weight loss, and digestion. Yet, it's high in calories and saturated fat, so you should eat it in moderation. Overall, unsweetened coconut meat makes a great addition to a balanced diet.  One serving is composed of 2 tablespoons shredded unsweetened coconut and comprises 90 calories.",
        },
    ],
    oilsAndNutButters: [
        {
            name: 'extra virgin olive oil',
            description: '1 teaspoon',
            calorieCount: 40,
            definition:
                'At the end of the day, quality extra virgin olive oil is incredibly healthy. Due to its powerful antioxidants, it benefits your heart, brain, joints and more. In fact, it may be the healthiest fat on the planet.  One serving is composed of one teaspoon of extra virgin olive oil and comprises 40 calories.',
            aliases: ['olive oil'],
        },
        {
            name: 'coconut oil',
            description: '1 teaspoon',
            calorieCount: 39,
            definition:
                'Coconut oil contains natural saturated fats that increase HDL good cholesterol levels in your body. They may also help turn LDL bad cholesterol into a less harmful form. By increasing HDL, many experts believe that coconut oil may boost heart health compared with many other fats.  One serving is composed of one teaspoon of coconut oil and comprises 39 calories.',
        },
        {
            name: 'flaxseed oil',
            description: '1 teaspoon',
            calorieCount: 40,
            definition:
                "Flaxseed oil is high in omega-3 fatty acids and has been shown to have several health benefits, such as reduced blood pressure and improved regularity. What's more, flaxseed oil can be used in a variety of ways. It can be used as a replacement for other types of oils, added to foods or applied to your skin and hair.  One serving is composed of one teaspoon of flaxseed oil and comprises 40 calories.",
        },
        {
            name: 'walnut oil',
            description: '1 teaspoon',
            calorieCount: 40,
            definition:
                'It has a nutty, delicate flavour and contains some of the beneficial nutrients and compounds found in walnuts, including unsaturated fatty acids and plant compounds called polyphenols. Consuming walnut oil may improve heart health, lower blood sugar, and have anticancer effects.  One serving is composed of one teaspoon of walnut oil and comprises 40 calories.',
        },
        {
            name: 'pumpkin seed oil',
            description: '1 teaspoon',
            calorieCount: 43,
            definition:
                'Pumpkin seed oil can improve heart health by lowering cholesterol and reducing high blood pressure, both of which are risk factors for heart disease. This may occur because pumpkin seed oil is a healthier alternative to saturated and trans fats.  One serving is composed of one teaspoon of pumpkin seed oil and comprises 43 calories.',
        },
        {
            name: 'peanut butter',
            description: '1 teaspoon',
            calorieCount: 94,
            definition:
                "Peanut butter is rich in a variety of nutrients, but it's also rich in calories and fat. While the healthy fats in peanut butter are nutritious, you should consume them in moderation to avoid unwanted weight gain or potential health problems.  One serving is composed of one teaspoon of peanut butter and comprises 94 calories.",
        },
        {
            name: 'almond butter',
            description: '1 teaspoon',
            calorieCount: 45,
            definition:
                'Like the raw almonds it is made from, almond butter is heart-healthy (thanks to the vitamin E and plant antioxidants), a great source of healthy fats, and a good way to get more protein into your diet. Vegans and vegetarians can use almond butter as a protein source, but anyone can enjoy it as extra protein.  One serving is composed of one teaspoon of almond butter and comprises 45 calories.',
        },
        {
            name: 'cashew butter',
            description: '1 teaspoon',
            calorieCount: 31,
            definition:
                "Although it doesn't contain omega-3 fatty acids, it's one of the best sources of monounsaturated fatty acids, essential amino acids, and magnesium. These nutrients are beneficial for blood pressure, sugar and cholesterol control, bone health, the immune system, and your metabolism.  One serving is composed of one teaspoon of cashew butter and comprises 31 calories.",
        },
        {
            name: 'pumpkin seed butter',
            description: '1 teaspoon',
            calorieCount: 27,
            definition:
                'Pumpkin seed butter is a great source of zinc and magnesium, two nutrients that are important for countless functions throughout the body, not the least of which is maintaining strong and healthy bones and muscles.  One serving is composed of one teaspoon of pumpkin seed butter and comprises 27 calories.',
        },
        {
            name: 'sunflower butter',
            description: '1 teaspoon',
            calorieCount: 34,
            definition:
                'Sunflower butter is made from little nutrient-dense sunflower seeds, which are a great source of protein, healthy fats, vitamin E and magnesium.  The protein and healthy fats keep you satiated and feeling full and energized.  One serving is composed of one teaspoon of sunflower butter and comprises 32 calories.',
        },
        {
            name: 'sesame butter',
            description: '1 teaspoon',
            calorieCount: 30,
            definition:
                "It typically has a smooth texture similar to nut butter but a stronger, more savory taste that's often described as bitter.  It is rich in antioxidants, contains anti-inflammatory compounds, may strengthen your central nervous system and helps protect liver and kidney function. One serving is composed of one teaspoon of sesame butter and comprises 30 calories.",
        },
        {
            name: 'tahini',
            description: '1 teaspoon',
            calorieCount: 30,
            definition:
                'Tahini is a tasty way to add powerful antioxidants and healthy fats to your diet, as well as several vitamins and minerals. It has antioxidant and anti-inflammatory properties, and its health benefits may include reducing risk factors for heart disease and protecting brain health.  One serving is composed of one teaspoon of tahini and comprises 30 calories.',
        },
    ],
}
