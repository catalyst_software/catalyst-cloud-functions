
import { Response, Request } from 'express';
import { firestore } from 'firebase-admin';
// import { Logger } from 'winston';

import BaseCtrl from './base';
import { Organization } from '../../../../models/organization.model';
import { initialiseFirebase } from '../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { ConsumerModelConfiguration } from '../interfaces/consumer-model-configuration';
import { Program } from '../../../../db/programs/models/program.model';
import { getOrgCollectionRef, getAthleteCollectionRef, getCollectionDocs } from '../../../../db/refs';

/**
* The controller handles the requests passed along by the router
// * @param logger: winston.Logger
*/
export default class ConsumerModelCtrl extends BaseCtrl {

  res: Response;

  model = Organization;


  collection = FirestoreCollection.ConsumerModelConfiguration;
  db: FirebaseFirestore.Firestore;

  constructor() {
    super();
    this.db = initialiseFirebase('API').db;
  }

  /**
   * Validates the registration code submitted by the user
   *
   * @memberof OrganizationCtrl
   */



  /**
   * Validates the registration code submitted by the user
   *
   * @memberof OrganizationsCtrl
   */
  getConsumerModel = (req: Request, res: Response) => {

    this.res = res;

    try {
      return getCollectionDocs(this.collection)
        .then((snapshot) => {
          if (snapshot.size === 1) {
            const snap = snapshot.docs.pop();
            const consumerModel = <ConsumerModelConfiguration>snap.data();
            return this.handleResponse({
              message: 'Consumer Configuration found',
              responseCode: 200,
              data: consumerModel,
              method: {
                name: 'getConsumerModel',
                line: 70
              },
              res
            });
          }
          else {
            return this.handleResponse({
              message: 'Consumer Configuration not found',
              responseCode: 400,
              data: undefined,
              method: {
                name: 'getConsumerModel',
                line: 70
              },
              res
            });
          }
        })
        .catch((exception) => {
          console.log('exception!!!! => ', exception);
          return this.handleResponse({
            message: 'An error exception in /organizations/code/:code: err ->' + exception,
            responseCode: 500,
            data: undefined,
            error: {
              message: 'An exception occurred in /organizations/code/:code: err ->' + exception,
              exception
            },
            method: {
              name: 'validateOrganizationCode',
              line: 72
            },
            res
          });
        });
    }
    catch (exception) {

      console.log('catchAll exception!!!! => ', exception);
      // return res.status(505).send(exception);
      return this.handleResponse({
        message: 'An unanticipated exception occurred in /organizations/code/:code',
        responseCode: 500,
        data: undefined,
        error: {
          message: 'An unanticipated exception occurred in /organizations/code/:code',
          exception: exception
        },
        method: {
          name: 'validateOrganizationCode',
          line: 76
        },
        res
      });
    }
  };

  saveConsumerModel = (req: Request, res: Response) => {

    this.res = res;

    try {

      const consumerModel = <ConsumerModelConfiguration>req.body;
      return this.db.collection(this.collection).doc()
        .set(consumerModel)
        .then((writeResult: firestore.WriteResult) => {

          return res.json(writeResult)
        })
        .catch((exception) => {
          console.log('exception!!!! => ', exception);
          return this.handleResponse({
            message: 'Could not save Consumer Configuration',
            responseCode: 500,
            data: undefined,
            error: {
              message: 'An exception occurred in /consumermodel/saveConsumerModel: err ->' + exception,
              exception
            },
            method: {
              name: 'saveConsumerModel',
              line: 100
            },
            res
          });
        });
    }
    catch (exception) {

      console.log('catchAll exception!!!! => ', exception);
      // return res.status(505).send(exception);
      return this.handleResponse({
        message: 'An unanticipated exception occurred in /organizations/code/:code',
        responseCode: 500,
        data: undefined,
        error: {
          message: 'An unanticipated exception occurred in /organizations/code/:code',
          exception: exception
        },
        method: {
          name: 'validateOrganizationCode',
          line: 76
        },
        res
      });
    }
  };

  updateProgram = (req: Request, res: Response) => {

    this.res = res;

    try {

      const { document: program, options } = <{
        document: Program,
        options: {
          updateOrgs: boolean,
          updateAthletes: boolean
        }
      }>req.body;

      const {
        images,
        ...theProgram
      } = program;

      return getCollectionDocs(this.collection)
        .then((snapshot) => {
          if (snapshot.size === 1) {
            const snap = snapshot.docs.pop();
            const consumerModel = <ConsumerModelConfiguration>snap.data();

            if (theProgram.activeDate)
              theProgram.activeDate = theProgram.activeDate;

              // TODO: !!!
            // if (theProgram.startDate)
            //   theProgram.startDate = new Date(theProgram.startDate);


            return snap.ref.update({ onboardingProgram: { ...theProgram, images } }).then((updateResult: firestore.WriteResult) => {
              if (options && options.updateOrgs) {
                return getOrgCollectionRef().get().then((snapQ: firestore.QuerySnapshot) => {
                  snapQ.docs.map((orgDoc) => {
                    return orgDoc.ref.update({ onboardingProgram: { ...theProgram, images } })
                  })
                })
              } if (options && options.updateAthletes) {
                return getAthleteCollectionRef().get().then((snapQ: firestore.QuerySnapshot) => {
                  snapQ.docs.map((athleteDoc) => {
                    return athleteDoc.ref.update({ onboardingProgram: { ...theProgram, images } })
                  })
                })
              } else return Promise.resolve()

            }).then((result) => {
              return this.handleResponse({
                message: 'Consumer Configuration Onboarding Program updated',
                responseCode: 200,
                data: consumerModel,
                method: {
                  name: 'updateProgram',
                  line: 172
                },
                res
              });
            })

          }
          else {
            return this.handleResponse({
              message: 'Consumer Configuration not found',
              responseCode: 400,
              data: undefined,
              method: {
                name: 'updateProgram',
                line: 172
              },
              res
            });
          }
        })
        .catch((exception) => {
          console.log('exception!!!! => ', { ...exception });
          
          return this.handleResponse({
            message: 'Unable to update Consumer Model Onboarding Program' + { ...exception },
            responseCode: 500,
            data: undefined,
            error: {
              message: 'An exception occurred in /consumermodel/program: err ->' + { ...exception },
              exception: { ...exception }
            },
            method: {
              name: 'updateProgram',
              line: 172
            },
            res
          });
        });
    }
    catch (exception) {

      console.log('catchAll exception!!!! => ', { ...exception });
      // return res.status(505).send(exception);
      return this.handleResponse({
        message: 'An unanticipated exception occurred updating Consumer Model Onboarding Program',
        responseCode: 500,
        data: undefined,
        error: {
          message: 'An unanticipated exception occurred in /consumermodel/program',
          exception: { ...exception }
        },
        method: {
          name: 'updateProgram',
          line: 172
        },
        res
      });
    }
  };
}


export const ipScoreConfiguration = {
  programs: {
    include: true,
    includeVideoConsumption: true,
    includeWorkSheetConsumption: true,
    includeSurveyConsumption: true,
  },
  quantitativeWellness: {
    include: true,
    training: {
      components: {
        include: true,
        allowedSkipDays: [5, 6],
        expectedDailyEntries: 1,
      },
    },
    body: {
      fatigue: {
        include: true,
        allowedSkipDays: [],
        expectedDailyEntries: 1,
      },
      pain: {
        include: true,
        allowedSkipDays: [],
        expectedDailyEntries: 1,
      },
      wearables: {
        include: true,
        allowedSkipDays: [],
        expectedDailyEntries: 1,
      },
    },
    brain: {
      sleep: {
        include: true,
        allowedSkipDays: [],
        expectedDailyEntries: 1,
      },
      mood: {
        include: true,
        allowedSkipDays: [],
        expectedDailyEntries: 4,
      },
    },
    food: {
      components: {
        include: true,
        allowedSkipDays: [],
        expectedDailyEntries: 4,
      },
    },
  },
  qualitativeWellness: {
    include: false,
  },
};


/**
* Populate db.collection(FirestoreCollection.Organizations) with two dummy accounts
* Currently using it so that I can test the onCreate events! ;P
*/
  // populateOrganizations(req: Request, res: Response): any {
  //   try {

  //     const organizationRef = getOrgCollectionRef();

  //     const Organization1 = new Organization({
  //       name: 'Organization1',
  //       contactPerson: 'Blake Simmonds',
  //       contactNumber: '07953 234 776',
  //       code: '12345',
  //       licensesConsumed: 10,
  //       licensesPurchased: 20,

  //     });

  //     const Organization2 = new Organization({
  //       name: 'Organization1',
  //       contactPerson: 'Lisa Wells',
  //       contactNumber: '07205 204 209',
  //       code: '67890',
  //       licensesConsumed: 10,
  //       licensesPurchased: 10,

  //     })

  //     // TODO: quick fix to sort 'Promises must be handled appropriately' error when running
  //     // 'firebase deploy --only functions'
  //     // Just for testing purposes, will be deleted most likely in the end though!
  //     return organizationRef.doc('Organization1').set(Organization1).then(() => {
  //       return organizationRef.doc('Organization2').set(Organization2).then(() => {
  //         this.handleResponse({
  //           message: 'Organisazions saved. Organization1 included with this response',
  //           responseCode: 200,
  //           data: Organization1,
  //           method: {
  //             name: 'populateOrganizations',
  //             line: 76
  //           }
  //           // res
  //         });
  //       });
  //     });

  //   }
  //   catch (exception) {
  //     this.handleResponse({
  //       message: 'An unanticipated exception occurred in api/organizations/code/:code',
  //       responseCode: 500,
  //       data: undefined,
  //       // res,
  //       error: {
  //         message: 'An unanticipated exception occurred in api/organizations/code/:code',
  //         exception
  //       },
  //       method: {
  //         name: 'populateOrganizations',
  //         line: 76
  //       }
  //     });

  //   }
  // }
