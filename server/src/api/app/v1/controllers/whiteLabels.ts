import { Response, Request } from 'express'
// import { FieldValue } from '@google-cloud/firestore';

import BaseCtrl from './base'

import { Group } from '../../../../models/group/group.model';
import { getWhiteLabelDocRef } from '../../../../db/refs';
import { firestore } from 'firebase-admin';
import { Organization, OrganizationInterface } from '../../../../models/organization.model';
import { getConsumerModelConfig } from '../callables/utils/getConsumerModelConfig';

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class GroupCtrl
 * @extends {BaseCtrl}
 */
export default class WhiteLabelsCtrl extends BaseCtrl {
    res: Response;

    // logger: Logger;
    model = Group;
    collection = undefined;

    constructor() {
        super()
    }
    
    /**
    * Adds a biometric entry to the database
    * Currently using so that I can test the onCreate events! ;P
    *
    * @memberof GroupsCtrl
    */
    getTemplates = async (req: Request, res: Response) => {

        return getWhiteLabelDocRef('default')
            .get()
            .then(writeResult => {
                return res.status(200).json({
                    message: 'Default Theme',
                    responseCode: 201,
                    data: writeResult.data(),
              
                })
            })
            .catch(() => {
                return res.status(502).json({
                    message: 'Unable to add Group',
                    responseCode: 401,
                    data: {  },
                })
            })

    };
}
