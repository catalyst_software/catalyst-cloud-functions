import { Organization } from '../../../../models/organization.model';
import { catalystDB, db } from '../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
export const copyTheTheme = async (themeUID) => {
    const theTheme = await catalystDB.collection(FirestoreCollection.WhiteLabelThemes).doc(themeUID).get()
        .then((docSnap => getDocumentFromSnapshot(docSnap) as Organization))
        .catch((err) => {
            console.warn(err);
            return undefined;
        });
    if (theTheme) {
        const savedTheme = await db.collection(FirestoreCollection.WhiteLabelThemes).doc(themeUID).set(theTheme);
        
        return {
            message: 'Theme Copied',
            responseCode: 200,
            data: savedTheme,
            error: undefined,
        };
    }
    else {
        console.error('Theme not found with uid', themeUID);
        // return res.status(505).send(exception);
        return {
            message: 'Theme not found with uid ' + themeUID,
            responseCode: 500,
            data: undefined,
            error: {
                message: 'Theme not found with uid ' + themeUID,
            }
        };
    }
};
