import { Request, Response } from 'express'
// import { Logger } from 'winston';
import { firestore } from 'firebase-admin'

import BaseCtrl from './base'
import { Organization } from '../../../../models/organization.model'

import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'


import { DoReadPayload } from './interfaces/do-read-payload'


import { AthleteBiometricEntryInterface } from '../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry'


import { AggregateLogLevel } from '../../../../db/biometricEntries/enums/aggregate-log-level'
import { AggregateDocumentManager, docManagerService } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { socket } from '../../../../shared/logger/logger'
import { AggregateDirectiveType } from '../../../../models/types/biometric-aggregate-types';
import { AggregateDocumentDirective } from '../../../../db/biometricEntries/services/interfaces/aggregate-document-directive';
import { LogLevel } from '../../../../db/biometricEntries/enums/log-level';
import { convertAllDatesStringsToTimeStamps } from '../callables/run-group-wellness-data-aggregation';


export interface WellGroupAggregationDirective {
    entries: Array<AthleteBiometricEntryInterface>;
    utcOffset: number;
    options?: {
        logLevel: LogLevel
    }
}

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class BiometricsCtrl
 * @extends {BaseCtrl}
 */
export default class BiometricsCtrl extends BaseCtrl {
    res: Response;

    // logger: Logger;
    model = Organization;
    entries;
    aggregateDocumentManager: AggregateDocumentManager;

    collection = FirestoreCollection.Athletes;

    constructor() {
        super()
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof BiometricsCtrl
     */
    addBiomentricEntryMultiple = async (
        req: Request,
        res: Response
    ): Promise<boolean | {
        updateAthleteDirectives: AggregateDirectiveType[];
        groupDocUpdates: AggregateDocumentDirective[];
    }> => {
        this.res = res;
        try {
            socket.emit(AggregateLogLevel.Info, {
                type: AggregateLogLevel.Info,
                data: { message: 'addBiomentricEntryMultiple called!!!' },
                // AggregateLogLevel
            });

            this.aggregateDocumentManager = new AggregateDocumentManager();

            const theRes = await this.ProcessRequest(req.body as DoReadPayload)

            return theRes

        } catch (exception) {
            // TODO: Exception not being passed to handleResponse()??
            socket.emit('evt-exception', {
                message: 'Could not add Biometric Entry',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in  addBiomentricEntryMultiple()',
                    // exception
                },
                exception,
                method: {
                    name: 'addBiomentricEntryMultiple',
                    line: 60,
                },
            });
            console.log('exception', exception);

            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /add/biometrics/:lifestyleEntryType/:value',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /add/biometrics/:lifestyleEntryType/:value',
                    exception: { ...exception },
                },
                method: {
                    name: 'addBiomentricEntry',
                    line: 60,
                },
                res: this.res
            })
        }
    };

    private async ProcessRequest(body: DoReadPayload) {
        const { entry, options } = body;
        let { count } = options;
        if (!count) {
            count = 1
        }
        // const utcOffset = 0;
        convertAllDatesStringsToTimeStamps(entry)
        // const transactionResult = await db.runTransaction(async t => {
        debugger;
        const resultNEW = await docManagerService.updateAggregates(undefined, entry, entry.userId).then((data) => {
            console.log('complete?!.....................', { documents: data })
            return data
        })    // }).then((results) => {
            //     console.log('Transaction success!', results);

            //     return true
            // })
            .catch(err => {
                console.log('Transaction failure:', err);

                return false
            });

        return resultNEW

        // return this.aggregateDocumentManager
        //     // .doRead(entry, userId, DocMngrActionTypes.Read, 'api/addBiomentricEntryMultiple', 'Info', t)
        //     .doGroupRead(entry, userId, DocMngrActionTypes.Read, 'api/addBiomentricEntryMultiple', 'Info', t)
        //     .then((data) => {
        //         if (data) {
        //             console.log(data);
        //             this.res.status(201).json({
        //                 data
        //             });
        //             return Promise.resolve(data)
        //         } else {
        //             LogInfo('empty data');
        //             this.res.status(201).json({
        //                 data
        //             });
        //             return Promise.resolve(null)
        //         }

        //     }).catch((err) => {
        //         LogInfo('Eooro Occured!!!!', err);
        //         return Promise.reject(err)
        //     });
        //     return resultNEW

        // }).then((results) => {
        //     console.log('Transaction success!', results);

        //     return true
        // }).catch(err => {
        //     console.log('Transaction failure:', err);

        //     return false
        // });


        // return transactionResult

        // const logLevel = options.logLevel ? options.logLevel : 'Info';

        // const athleteBiometricEntryRef = db
        //     .collection(FirestoreCollection.Athletes)
        //     .doc(userId)
        //     .collection('biometricEntries');
        // let result: boolean | void;
        // throttle(this.saveEntry, 1001, { trailing: true });
        // for (let index = 5; index < count + 5; index++) {
        //     result = await this.saveEntry(
        //         lifestyleEntryType,
        //         value,
        //         entry,
        //         athleteBiometricEntryRef,
        //         apiHandlesDoRead,
        //         handleDoWriteEntry,
        //         logLevel
        //     )
        // }

        // console.log(result);
        // console.log('return this.handleResponse()');

        // return this.handleResponse({
        //     message: 'Athlete Biometric Entry Saved',
        //     responseCode: 201,
        //     data: {
        //         docs: this.entries,
        //     },
        //     error: undefined,
        //     method: {
        //         name: 'addBiomentricEntry',
        //         line: 146,
        //     },
        //     res: this.res
        // })
    }

}
