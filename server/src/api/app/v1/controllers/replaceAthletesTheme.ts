import { Organization } from '../../../../models/organization.model';
import { catalystDB, db } from '../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { WhiteLabelTheme } from '../interfaces/WhiteLabelTheme';
import { Athlete } from '../../../../models/athlete/athlete.model';
import { chunk } from 'lodash';

export const replaceAthletesTheme = async (orgUID, athleteUID?, themeUID?, skipContent = false) => {

    let themeUid = themeUID

    let theTheme: WhiteLabelTheme

    if (!skipContent) {
        if (!themeUID && orgUID) {
            const theOrg: Organization = await db.collection(FirestoreCollection.Organizations).doc(orgUID).get()
                .then((docSnap => getDocumentFromSnapshot(docSnap) as Organization))
                .catch((err) => {
                    console.warn(err);
                    return undefined;
                });

            if (theOrg) {
                themeUid = theOrg.theme
            }
            if (!themeUid) {
                throw new Error('The themeUID not found')
            }
        }

        if (!orgUID) {
            throw new Error('The orgUID not supplied')
        } else {
            theTheme = await db.collection(FirestoreCollection.WhiteLabelThemes).doc(themeUid).get()
                .then((docSnap => getDocumentFromSnapshot(docSnap) as WhiteLabelTheme))
                .catch((err) => {
                    console.warn(err);
                    return undefined;
                });
        }
    }

    if (theTheme || skipContent) {


        const theWriteResults = await db.collection(FirestoreCollection.Athletes).where(!!athleteUID ? 'uid' : 'organizationId', '==', athleteUID || orgUID).get()
            .then(async (querySnap) => {

                if (querySnap.size) {

                    const chunkSize = 450

                    const athltetesChunked = chunk(querySnap.docs, chunkSize);
                    // const length = 3

                    // let chunkGroup = athltetesChunked.slice(0, athltetesChunked.length < length ? athltetesChunked.length : length)

                    // let numberOfUsers = numAthCount

                    const allUpdates = await Promise.all(athltetesChunked.map(async (chunkDocSnap, i) => {

                        const batch = db.batch();

                        chunkDocSnap.forEach((docSnap) => {

                            const ath = getDocumentFromSnapshot(docSnap) as Athlete

                            if (skipContent) {

                                const athOrgIndex = (ath.organizations || []).findIndex((o) => o.organizationId === orgUID)
                                if (athOrgIndex >= 0) {

                                    if (ath.organizations[athOrgIndex].themeId !== themeUID) {
                                        console.log('ath.organizations[athOrgIndex].themeId', ath.organizations[athOrgIndex].themeId)
                                        ath.organizations[athOrgIndex].themeId = themeUID
                                        console.log('ath.organizations[athOrgIndex].themeId Now', ath.organizations[athOrgIndex].themeId)

                                        batch.update(docSnap.ref, {
                                            themeId: themeUID,
                                            organizations: ath.organizations || [],
                                        });
                                    }
                                }

                            } else {

                                const athThemes = ath.themes || []
                                const themeToUpdateIndex = athThemes.findIndex((at) => at.uid === theTheme.uid)

                                if (themeToUpdateIndex > -1) {
                                    athThemes[themeToUpdateIndex] = {
                                        ...athThemes[themeToUpdateIndex],
                                        ...theTheme
                                    }
                                } else {
                                    athThemes.push(theTheme)
                                }

                                const athOrgIndex = (ath.organizations || []).findIndex((o) => o.organizationId === orgUID)

                                if (athOrgIndex >= 0) {

                                    if (ath.organizations[athOrgIndex].themeId !== themeUID) {
                                        console.log('ath.organizations[athOrgIndex].themeId', ath.organizations[athOrgIndex].themeId)
                                        ath.organizations[athOrgIndex].themeId = themeUID
                                        console.log('ath.organizations[athOrgIndex].themeId Now', ath.organizations[athOrgIndex].themeId)

                                        batch.update(docSnap.ref, {
                                            themeId: themeUID,
                                            organizations: ath.organizations || [],
                                        });
                                    }

                                }
                                batch.update(docSnap.ref, {
                                    themeId: themeUID,
                                    organizations: ath.organizations || [],
                                    themes: athThemes
                                });
                                // return docSnap.ref.update({
                                //     themeId: themeUID,
                                //     organizations: ath.organizations || [],
                                //     themes: athThemes
                                // }).then((r) => {
                                //     return {
                                //         athleteUID: ath.uid,
                                //         name: ath.profile.firstName + ' ' + ath.profile.lastName,
                                //         message: 'User Theme Updated'
                                //     }
                                // }).catch((error) => {
                                //     return {
                                //         athleteUID: ath.uid,
                                //         name: ath.profile.firstName + ' ' + ath.profile.lastName,
                                //         message: 'User Theme NOT Updated',
                                //         error
                                //     }
                                // })
                            }

                        })

                        // return rest



                        // numberOfUsers = numberOfUsers + athletes.length
                        // batch.update(orgDocRef, {
                        //     numberUsersOnboarded: numberOfUsers
                        // });

                        return batch.commit();
                        // return theGroup
                        // athletes.map((ath) => {
                        //     if (!ath.groups && !!ath.groups.length) {
                        //         ath.groups = [group]
                        //     } else {
                        //         ath.groups[0] = group
                        //     }

                        // })
                        // // const update = await executeMoveAthleteToGroup(athlete.uid, theGroup, false, false)
                        // return
                    }))

                    return allUpdates

                }

                return undefined

            })
            .catch((err) => {
                console.warn(err);
                return undefined;
            });


        return {
            message: 'Theme Replaced',
            responseCode: 200,
            data: theWriteResults,
            error: undefined,
        };
    }
    else {
        console.error('Theme not found with uid', themeUID);
        // return res.status(505).send(exception);
        return {
            message: 'Theme not found with uid ' + themeUID,
            responseCode: 500,
            data: undefined,
            error: {
                message: 'Theme not found with uid ' + themeUID,
            }
        };
    }
};
