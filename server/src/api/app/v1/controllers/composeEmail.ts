import { EmailMessage } from '../sendGrid/conrollers/email';
import { EmailMessageTemplate } from "../sendGrid/templates/email-message-template";
import { emailMessageDefault } from './tests';
import { IPScoreNonEngagementTemplate, ReportMessagingTemplate } from '../sendGrid/templates/ipscore-non-engagement';

export const composeEmail = (
    emailMessageTemplate: EmailMessageTemplate,
    userFullName: string
) => {
    const message: EmailMessage = {
        ...emailMessageDefault,
        templateId: emailMessageTemplate.templateId || "d-45b9b00534944c5aa4eadd3c3ead0b3e",
        emailTemplateName: emailMessageTemplate.emailTemplateName || "badMood",
        notificationTemplateType: emailMessageTemplate.notificationTemplateType,
        notificationTemplate: emailMessageTemplate.notificationTemplate,
        includePushNotification: emailMessageTemplate.includePushNotification,
        dynamic_template_data: {
            ...emailMessageTemplate.emailTemplateData,
            name: userFullName
        }
    };

    return { message };
};

export const composeEmailV2 = (emailMessageTemplate: IPScoreNonEngagementTemplate) => {
    const message: EmailMessage = {
        ...emailMessageDefault,
        templateId: emailMessageTemplate.templateId || "d-93756f51e55b4734a5b93d23e4310750",
        emailTemplateName: emailMessageTemplate.emailTemplateName || "badMood",
        notificationTemplateType: emailMessageTemplate.notificationTemplateType,
        includePushNotification: emailMessageTemplate.includePushNotification,
        dynamic_template_data: {
            ...emailMessageTemplate.emailTemplateData
        }
    };

    return { message };
};
export const composeEmailV3 = (emailMessageTemplate: ReportMessagingTemplate) => {
    const message: EmailMessage = {
        ...emailMessageDefault,
        templateId: emailMessageTemplate.templateId || "d-93756f51e55b4734a5b93d23e4310750",
        emailTemplateName: emailMessageTemplate.emailTemplateName || "badMood",
        notificationTemplateType: emailMessageTemplate.notificationTemplateType,
        includePushNotification: emailMessageTemplate.includePushNotification,
        dynamic_template_data: {
            ...emailMessageTemplate.emailTemplateData
        }
    };

    return { message };
};
