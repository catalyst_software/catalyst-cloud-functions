import { Athlete } from './../../../../models/athlete/interfaces/athlete'
import { Response, Request } from 'express'
// import { Logger } from 'winston';
import { firestore } from 'firebase-admin'
import _ = require('lodash')

// import { admin } from '../../../../shared/init/initialise-firebase';

import { db } from '../../../../shared/init/initialise-firebase'

import BaseCtrl from './base'
import { Organization } from '../../../../models/organization.model'

import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'

import { DoReadPayload } from './interfaces/do-read-payload'

import { AthleteBiometricEntryInterface } from '../../../../db/biometricEntries/well/aggregate/interfaces/athlete/athelete-biometric-entry'

import { AthleteBiometricEntryModel } from '../../../../db/biometricEntries/well/athelete-biometric-entry.model'
import { LifestyleEntryType } from '../../../../models/enums/lifestyle-entry-type'

import { AggregateLogLevel } from '../../../../db/biometricEntries/enums/aggregate-log-level'
import { DocMngrActionTypes } from '../../../../db/biometricEntries/enums/document-manager-action-types'
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager'
import { LogInfo, socket } from '../../../../shared/logger/logger'

import { resetAthletesIpScore } from '../callables/utils/reset-athletes-ip-score'
import { removeUndefinedProps } from '../../../../db/ipscore/services/save-aggregates'
import { AthleteBiometricEntryDocument } from '../../../../db/biometricEntries/well/documents/athelete-biometric-entry.document'
import { updateIPScoreAggregates } from '../../../../db/ipscore/update-ip-score-aggregates'

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class IpScoreCtrl
 * @extends {BaseCtrl}
 */
export default class IPScoresCtrl extends BaseCtrl {
    res: Response

    // logger: Logger;
    model = Organization
    collection = FirestoreCollection.Organizations
    entries
    aggregateDocumentManager: AggregateDocumentManager

    constructor() {
        super()
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof IpScoresCtrl
     */
    aggregateIpScore = async (req: Request, res: Response) => {
        this.res = res
        try {
            socket.emit(AggregateLogLevel.Info, {
                type: AggregateLogLevel.Info,
                data: { message: 'addBiomentricEntryMultiple called!!!' },
                // AggregateLogLevel
            })

            const {
                // thePath,
                uid,
                // groupId,
                organizationId,
            } = req.body

            // let path = "quantitative.brain.sleep";

            // if (thePath) {
            //     path = thePath
            // }

            const athlete = req.body as Athlete

            // const athleteUID = athlete.uid

            // const purgeAggregateRestults = await purgeAggregateDocuments(athleteUID)

            // const documentUpdatesResults = await cleanUpAthleteRelatedDocuments(athleteUID)

            // return this.handleResponse({
            //     message:
            //         'Athlete Collections Purged',
            //     responseCode: 200,
            //     data: {
            //         purgeAggregateRestults,
            //         documentUpdatesResults
            //     },
            //     method: {
            //         name: 'purgeCollections',
            //         line: 60,
            //     },
            //     res
            // })

            const at2 = {
                // "uid": "oHEU0MRLM1bxiHLOYKY0amC1Nnt1",
                // "uid": "1B3H3hhUtmWDx8ERHihmkpogdVf2",
                // "uid": "mUHQzOCUDhOhORuw17oZHHkQzQw2",
                uid,
                organizationId,
                ...athlete,
                userProperties: undefined,

                // "isCoach": true,
                // "ipScore": 0,
                groups: athlete.groups.map(group => {
                    return {
                        ...group,
                        creationTimestamp: firestore.Timestamp.fromDate(
                            new Date('2019-02-13T14:24:45.979Z')
                        ),
                    }
                }),
                // "emailVerified": false,
                // "anonymous": false,
                // "isAnonymous": false,
                // "providers": [
                //     {
                //         "id": "firebase"
                //     },
                //     {
                //         "id": "password"
                //     }
                // ],
                metadata: {
                    ...athlete.metadata,
                    creationTimestamp: firestore.Timestamp.fromDate(
                        new Date(athlete.metadata.creationTimestamp as any)
                    ),
                    // "lastSignInTimestamp": firestore.Timestamp.fromDate(new Date("2019-02-07T17:23:15.260Z")),
                    // "firebaseMessagingId": "fQQp8POojxU:APA91bEWjmWIErbqPCdbzKOgo4f64CKM1xp454DPiMINgIv4Ll7xrMCwex3m6FFZ2J9PHKpFCXWCrLhQWuHCy63bDlLHUgotJeQQ2ktR4TzLVDPhPqaaU1vaq2b4B_J_XKBs7Xu1r7Jm"
                },
                // "firebaseMessagingIds": ["fQQp8POojxU:APA91bEWjmWIErbqPCdbzKOgo4f64CKM1xp454DPiMINgIv4Ll7xrMCwex3m6FFZ2J9PHKpFCXWCrLhQWuHCy63bDlLHUgotJeQQ2ktR4TzLVDPhPqaaU1vaq2b4B_J_XKBs7Xu1r7Jm"],
                // "profile": {
                //     "firstName": "Ian",
                //     "lastName": "Gouws",
                //     "dob": "07/02/1977",
                //     "imageUrl": "",
                //     "sex": "Male",
                //     "bio": "Blue",
                //     "fullName": "Ian Gouws",
                //     "location": "UK",
                //     "onboardingCode": "iNSPIRE",
                //     "sport": "Cycling",
                //     "email": "ian@server.com"
                // },
                // "name": null,
                // "email": "ian@server.com",
                // "phoneNumber": null,
                // "profileImageURL": null,
                // "additionalUserInfo": {
                //     "providerId": null,
                //     "username": null,
                //     "isNewUser": true,
                //     "profile": null
                // },
                currentPrograms: [],
                // "ipScoreConfiguration": {
                //     "programs": {
                //         "include": true,
                //         "includeVideoConsumption": true,
                //         "includeWorkSheetConsumption": true,
                //         "includeSurveyConsumption": true
                //     },
                //     "quantitativeWellness": {
                //         "include": true,
                //         "training": {
                //             "components": {
                //                 "include": true,
                //                 "allowedSkipDays": [
                //                 ],
                //                 "expectedDailyEntries": 4
                //             }
                //         },
                //         "body": {
                //             "fatigue": {
                //                 "include": true,
                //                 "allowedSkipDays": [],
                //                 "expectedDailyEntries": 4
                //             },
                //             "pain": {
                //                 "include": true,
                //                 "allowedSkipDays": [],
                //                 "expectedDailyEntries": 4
                //             },
                //             "wearables": {
                //                 "include": true,
                //                 "allowedSkipDays": [],
                //                 "expectedDailyEntries": 4
                //             }
                //         },
                //         "brain": {
                //             "sleep": {
                //                 "include": true,
                //                 "allowedSkipDays": [],
                //                 "expectedDailyEntries": 4
                //             },
                //             "mood": {
                //                 "include": true,
                //                 "allowedSkipDays": [],
                //                 "expectedDailyEntries": 4
                //             }
                //         },
                //         "food": {
                //             "components": {
                //                 "include": true,
                //                 "allowedSkipDays": [],
                //                 "expectedDailyEntries": 4
                //             }
                //         }
                //     },
                //     "qualitativeWellness": {
                //         "include": false
                //     }
                // },
                currentIpScoreTracking: {
                    ...athlete.currentIpScoreTracking,

                    // TODO: Get the current value?!
                    // ipScore: 0, //ath.currentIpScoreTracking.dailyIpScore,

                    dateTime: firestore.Timestamp.fromDate(
                        new Date(athlete.currentIpScoreTracking.dateTime as any)
                    ),
                    // "programs": {
                    //     "videoConsumed": false,
                    //     "worksheetConsumed": false,
                    //     "surveyConsumed": false
                    // },
                    // "quantitativeWellness": {
                    //     "include": true,
                    //     "training": {
                    //         "components": {
                    //             "recordedDailyEntries": 1,
                    //             "tracking": []
                    //         }
                    //     },
                    //     "body": {
                    //         "fatigue": {
                    //             "recordedDailyEntries": 1,
                    //             "tracking": []
                    //         },
                    //         "pain": {
                    //             "recordedDailyEntries": 1,
                    //             "tracking": []
                    //         },
                    //         "wearables": {
                    //             "recordedDailyEntries": 1,
                    //             "tracking": []
                    //         }
                    //     },
                    //     "brain": {
                    //         "sleep": {
                    //             "recordedDailyEntries": 1,
                    //             "tracking": []
                    //         },
                    //         "mood": {
                    //             "recordedDailyEntries": 1,
                    //             "tracking": []
                    //         }
                    //     },
                    //     "food": {
                    //         "components": {
                    //             "recordedDailyEntries": 1,
                    //             "tracking": []
                    //         }
                    //     }
                    // },
                    // "qualitativeWellness": {
                    //     "include": false
                    // },
                    // "directive": {
                    //     "execute": true,
                    //     "includeGroupAggregation": true,
                    //     "paths": [
                    //         path
                    //     ]
                    // }
                },
            }

            // const at3 = {
            //     // "uid": "oHEU0MRLM1bxiHLOYKY0amC1Nnt1",
            //     "uid": "JZExobGcDkcmGTASIcZyT9myMsv2",
            //     "organizationId": "5KPZltwH0oTfbpLGkkeW",
            //     "isCoach": true,
            //     "ipScore": 0,
            //     "groups": [
            //         {
            //             //    uid: 'jGkPga1LLCxUBtCphoVS-1',
            //             // uid: '1Xat0moxWP2ZOK4uOmCa-1',
            //             uid: '5KPZltwH0oTfbpLGkkeW-1',
            //             "groupName": "Ian's group",
            //             "groupId": "5KPZltwH0oTfbpLGkkeW-1",
            //             "creationTimestamp": firestore.Timestamp.fromDate(new Date("2019-02-10T14:24:45.979Z")),
            //             "logoUrl": ""
            //         }
            //     ],
            //     "emailVerified": false,
            //     "anonymous": false,
            //     "isAnonymous": false,
            //     "providers": [
            //         {
            //             "id": "firebase"
            //         },
            //         {
            //             "id": "password"
            //         }
            //     ],
            //     "metadata": {
            //         "creationTimestamp": firestore.Timestamp.fromDate(new Date("2019-02-10T14:24:55.000Z")),
            //         "lastSignInTimestamp": firestore.Timestamp.fromDate(new Date("2019-02-07T17:23:15.260Z")),
            //         "firebaseMessagingId": "fQQp8POojxU:APA91bEWjmWIErbqPCdbzKOgo4f64CKM1xp454DPiMINgIv4Ll7xrMCwex3m6FFZ2J9PHKpFCXWCrLhQWuHCy63bDlLHUgotJeQQ2ktR4TzLVDPhPqaaU1vaq2b4B_J_XKBs7Xu1r7Jm"
            //     },
            //     "profile": {
            //         "firstName": "Ian",
            //         "lastName": "Gouws",
            //         "dob": "07/02/1977",
            //         "imageUrl": "",
            //         "sex": "Male",
            //         "bio": "Blue",
            //         "fullName": "Ian Gouws",
            //         "location": "UK",
            //         "onboardingCode": "iNSPIRE",
            //         "sport": "Cycling",
            //         "email": "ian@server.com"
            //     },
            //     "name": null,
            //     "email": "ian@server.com",
            //     "phoneNumber": null,
            //     "profileImageURL": null,
            //     "additionalUserInfo": {
            //         "providerId": null,
            //         "username": null,
            //         "isNewUser": true,
            //         "profile": null
            //     },
            //     "currentPrograms": [

            //     ],
            //     "ipScoreConfiguration": {
            //         "programs": {
            //             "include": true,
            //             "includeVideoConsumption": true,
            //             "includeWorkSheetConsumption": true,
            //             "includeSurveyConsumption": true
            //         },
            //         "quantitativeWellness": {
            //             "include": true,
            //             "training": {
            //                 "components": {
            //                     "include": true,
            //                     "allowedSkipDays": [
            //                     ],
            //                     "expectedDailyEntries": 10
            //                 }
            //             },
            //             "body": {
            //                 "fatigue": {
            //                     "include": true,
            //                     "allowedSkipDays": [],
            //                     "expectedDailyEntries": 10
            //                 },
            //                 "pain": {
            //                     "include": true,
            //                     "allowedSkipDays": [],
            //                     "expectedDailyEntries": 10
            //                 },
            //                 "wearables": {
            //                     "include": true,
            //                     "allowedSkipDays": [],
            //                     "expectedDailyEntries": 10
            //                 }
            //             },
            //             "brain": {
            //                 "sleep": {
            //                     "include": true,
            //                     "allowedSkipDays": [],
            //                     "expectedDailyEntries": 10
            //                 },
            //                 "mood": {
            //                     "include": true,
            //                     "allowedSkipDays": [],
            //                     "expectedDailyEntries": 10
            //                 }
            //             },
            //             "food": {
            //                 "components": {
            //                     "include": true,
            //                     "allowedSkipDays": [],
            //                     "expectedDailyEntries": 10
            //                 }
            //             }
            //         },
            //         "qualitativeWellness": {
            //             "include": false
            //         }
            //     },
            //     "currentIpScoreTracking": {
            // // TODO: Get the current value?!
            // dailyIpScore: 0, //ath.currentIpScoreTracking.dailyIpScore,

            //         "dateTime": firestore.Timestamp.fromDate(new Date("2019-02-10T00:00:00.000Z")),
            //         "programs": {
            //             "videoConsumed": false,
            //             "worksheetConsumed": false,
            //             "surveyConsumed": false
            //         },
            //         "quantitativeWellness": {
            //             "include": true,
            //             "training": {
            //                 "components": {
            //                     "recordedDailyEntries": 1,
            //                     "tracking": []
            //                 }
            //             },
            //             "body": {
            //                 "fatigue": {
            //                     "recordedDailyEntries": 1,
            //                     "tracking": []
            //                 },
            //                 "pain": {
            //                     "recordedDailyEntries": 1,
            //                     "tracking": []
            //                 },
            //                 "wearables": {
            //                     "recordedDailyEntries": 1,
            //                     "tracking": []
            //                 }
            //             },
            //             "brain": {
            //                 "sleep": {
            //                     "recordedDailyEntries": 1,
            //                     "tracking": []
            //                 },
            //                 "mood": {
            //                     "recordedDailyEntries": 1,
            //                     "tracking": []
            //                 }
            //             },
            //             "food": {
            //                 "components": {
            //                     "recordedDailyEntries": 1,
            //                     "tracking": []
            //                 }
            //             }
            //         },
            //         "qualitativeWellness": {
            //             "include": false
            //         },
            //         "directive": {
            //             "execute": true,
            //             "includeGroupAggregation": true,
            //             "paths": [
            //                 path
            //             ]
            //         }
            //     }

            // }

            // const at = {
            //     uid: 'ig-gdzdzyM8nkQ823oqEB3HhCVWyPo2',
            //     isCoach: false,
            //     ipScore: 0,
            //     emailVerified: false,
            //     anonymous: false,
            //     isAnonymous: false,
            //     providers: [
            //         {
            //             id: 'firebase',
            //         },
            //         {
            //             id: 'password',
            //         },
            //     ],
            //     metadata: {
            //         creationTimestamp: firestore.Timestamp.now(),
            //         lastSignInTimestamp: firestore.Timestamp.now(),
            //         firebaseMessagingId:
            //             'BSaSynmzXetUMW-1M0NuHSFZ4ImPA64zzdk90rN3',
            //     },
            //     profile: {
            //         firstName: 'Piet',
            //         lastName: 'Pompies',
            //         bio: 'IpScore tests',
            //         location: 'UK',
            //         secret: '123456',
            //         email: 'me@server2.com',
            //     },
            //     name: '',
            //     email: 'me@server2.com',
            //     phoneNumber: '',
            //     profileImageURL: 'null',
            //     currentIpScoreTracking: {
            //         dateTime: firestore.Timestamp.now(),
            //         programs: {
            //             videoConsumed: false,
            //             worksheetConsumed: false,
            //             surveyConsumed: false,
            //         },
            //         quantitativeWellness: {
            //             include: true,
            //             training: {
            //                 components: {
            //                     recordedDailyEntries: 0,
            //                     tracking: [],
            //                 },
            //             },
            //             body: {
            //                 fatigue: {
            //                     recordedDailyEntries: 0,
            //                     tracking: [],
            //                 },
            //                 pain: {
            //                     recordedDailyEntries: 0,
            //                     tracking: [],
            //                 },
            //                 wearables: {
            //                     recordedDailyEntries: 0,
            //                     tracking: [],
            //                 },
            //             },
            //             brain: {
            //                 sleep: {
            //                     recordedDailyEntries: 0,
            //                     tracking: [],
            //                 },
            //                 mood: {
            //                     recordedDailyEntries: 2,
            //                     tracking: [],
            //                 },
            //             },
            //             food: {
            //                 components: {
            //                     recordedDailyEntries: 0,
            //                     tracking: [],
            //                 },
            //             },
            //         },
            //         qualitativeWellness: {
            //             include: false,
            //         },
            //         directive: {
            //             execute: true,
            //             includeGroupAggregation: true,
            //             paths: ['quantitative.brain.mood'],
            //         },
            //     },
            //     ipScoreConfiguration: {
            //         programs: {
            //             include: true,
            //             includeVideoConsumption: true,
            //             includeWorkSheetConsumption: true,
            //             includeSurveyConsumption: true,
            //         },
            //         quantitativeWellness: {
            //             include: true,
            //             training: {
            //                 components: {
            //                     include: true,
            //                     allowedSkipDays: [5, 6],
            //                     expectedDailyEntries: 1,
            //                 },
            //             },
            //             body: {
            //                 fatigue: {
            //                     include: true,
            //                     allowedSkipDays: [],
            //                     expectedDailyEntries: 1,
            //                 },
            //                 pain: {
            //                     include: true,
            //                     allowedSkipDays: [],
            //                     expectedDailyEntries: 1,
            //                 },
            //                 wearables: {
            //                     include: true,
            //                     allowedSkipDays: [],
            //                     expectedDailyEntries: 1,
            //                 },
            //             },
            //             brain: {
            //                 sleep: {
            //                     include: true,
            //                     allowedSkipDays: [],
            //                     expectedDailyEntries: 1,
            //                 },
            //                 mood: {
            //                     include: true,
            //                     allowedSkipDays: [],
            //                     expectedDailyEntries: 4,
            //                 },
            //             },
            //             food: {
            //                 components: {
            //                     include: true,
            //                     allowedSkipDays: [],
            //                     expectedDailyEntries: 4,
            //                 },
            //             },
            //         },
            //         qualitativeWellness: {
            //             include: false,
            //         },
            //     },
            //     firebaseMessagingID: 'BSaSynmzXetUMW-1M0NuHSFZ4ImPA64zzdk90rN3',
            // }

            const theAthlete = at2

            if (theAthlete) {
                removeUndefinedProps(theAthlete)

                // TODO:
                const utcOffset = 0

                debugger

                const transactionResult = await db
                    .runTransaction(
                        async (t: FirebaseFirestore.Transaction) => {
                            const tehgUpdatesRTesult = await updateIPScoreAggregates(
                                t,
                                theAthlete,
                                true,
                                utcOffset
                            )
                                .then(docs => {
                                    console.log(docs)
                                    if (!docs) {
                                        return res.json({ sucess: true })
                                    } else return res.json(docs)
                                })
                                .catch(err => {
                                    console.log(err, theAthlete)
                                    res.status(500).json(err)
                                })

                            return tehgUpdatesRTesult
                        }
                    )
                    .then(result => {
                        console.log('Transaction success!')

                        return result
                    })
                    .catch(err => {
                        console.log('Transaction failure:', err)
                    })

                return transactionResult
            } else {
                // TODO:
                return res
                    .status(500)
                    .json({
                        error:
                            'An error occured whilst updating ipScore Documents',
                    })
            }

            // this.aggregateDocumentManager = new AggregateDocumentManager();

            // return await this.ProcessRequest(req.body as DoReadPayload)
        } catch (exception) {
            // TODO: Exception not being passed to handleResponse()??
            socket.emit('evt-exception', {
                message: 'Could not add Biometric Entry',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in  addBiomentricEntryMultiple()',
                    // exception
                },
                exception,
                method: {
                    name: 'addBiomentricEntry',
                    line: 60,
                },
            })
            console.log('exception', exception)
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /add/ipScore/:lifestyleEntryType/:value',
                responseCode: 500,
                data: undefined,
                error: {
                    message: exception.message,
                    stack: exception.stack,
                },
                method: {
                    name: 'addBiomentricEntry',
                    line: 60,
                },
                res,
            })
        }
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof IpScoresCtrl
     */
    runNonAggregateIpScore = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        this.res = res
        try {
            // socket.emit(AggregateLogLevel.Info, {
            //     type: AggregateLogLevel.Info,
            //     data: { message: 'addBiomentricEntryMultiple called!!!' },
            //     // AggregateLogLevel
            // })

            const at = {
                uid: 'JZExobGcDkcmGTASIcZyT9myMsv2',
                isCoach: false,
                ipScore: 0,
                emailVerified: false,
                anonymous: false,
                isAnonymous: false,
                providers: [
                    {
                        id: 'firebase',
                    },
                    {
                        id: 'password',
                    },
                ],
                metadata: {
                    creationTimestamp: firestore.Timestamp.now(),
                    lastSignInTimestamp: firestore.Timestamp.now(),
                    firebaseMessagingId:
                        'BSaSynmzXetUMW-1M0NuHSFZ4ImPA64zzdk90rN3',
                },
                profile: {
                    firstName: 'Piet',
                    lastName: 'Pompies',
                    bio: 'IpScore tests',
                    location: 'UK',
                    secret: '123456',
                    email: 'me@server2.com',
                },
                name: '',
                email: 'me@server2.com',
                phoneNumber: '',
                profileImageURL: 'null',
                currentIpScoreTracking: {
                    dateTime: firestore.Timestamp.now(),
                    programs: {
                        videoConsumed: false,
                        worksheetConsumed: false,
                        surveyConsumed: false,
                    },
                    quantitativeWellness: {
                        include: true,
                        training: {
                            components: {
                                recordedDailyEntries: 0,
                                tracking: [],
                            },
                        },
                        body: {
                            fatigue: {
                                recordedDailyEntries: 0,
                                tracking: [],
                            },
                            pain: {
                                recordedDailyEntries: 0,
                                tracking: [],
                            },
                            wearables: {
                                recordedDailyEntries: 0,
                                tracking: [],
                            },
                        },
                        brain: {
                            sleep: {
                                recordedDailyEntries: 0,
                                tracking: [],
                            },
                            mood: {
                                recordedDailyEntries: 2,
                                tracking: [],
                            },
                        },
                        food: {
                            components: {
                                recordedDailyEntries: 0,
                                tracking: [],
                            },
                        },
                    },
                    qualitativeWellness: {
                        include: false,
                    },
                    directive: {
                        execute: true,
                        paths: ['quantitative.brain.mood'],
                    },
                },
                ipScoreConfiguration: {
                    programs: {
                        include: true,
                        includeVideoConsumption: true,
                        includeWorkSheetConsumption: true,
                        includeSurveyConsumption: true,
                    },
                    quantitativeWellness: {
                        include: true,
                        training: {
                            components: {
                                include: true,
                                allowedSkipDays: [5, 6],
                                expectedDailyEntries: 1,
                            },
                        },
                        body: {
                            fatigue: {
                                include: true,
                                allowedSkipDays: [],
                                expectedDailyEntries: 1,
                            },
                            pain: {
                                include: true,
                                allowedSkipDays: [],
                                expectedDailyEntries: 1,
                            },
                            wearables: {
                                include: true,
                                allowedSkipDays: [],
                                expectedDailyEntries: 1,
                            },
                        },
                        brain: {
                            sleep: {
                                include: true,
                                allowedSkipDays: [],
                                expectedDailyEntries: 1,
                            },
                            mood: {
                                include: true,
                                allowedSkipDays: [],
                                expectedDailyEntries: 4,
                            },
                        },
                        food: {
                            components: {
                                include: true,
                                allowedSkipDays: [],
                                expectedDailyEntries: 4,
                            },
                        },
                    },
                    qualitativeWellness: {
                        include: false,
                    },
                },
            }

            const theAthlete = at

            if (theAthlete) {
                return true
                // return updateNonAggregates(theAthlete)
                // .then(docs => {
                //     console.log(docs)
                //     return undefined
                // })
                // .catch(err => {
                //     console.log(err)
                //     return err
                // })
            } else {
                // TODO:
                return 'No data found'
            }

            this.aggregateDocumentManager = new AggregateDocumentManager()

            return await this.ProcessRequest(req.body as DoReadPayload)
        } catch (exception) {
            // TODO: Exception not being passed to handleResponse()??
            socket.emit('evt-exception', {
                message: 'Could not add Biometric Entry',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in  addBiomentricEntryMultiple()',
                    // exception
                },
                exception,
                method: {
                    name: 'addBiomentricEntry',
                    line: 60,
                },
            })
            console.log('exception', exception)
            return this.handleResponse({
                message:
                    'An unanticipated exception occurred in /add/ipScore/:lifestyleEntryType/:value',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in /add/ipScore/:lifestyleEntryType/:value',
                    exception: { ...exception },
                },
                method: {
                    name: 'addBiomentricEntry',
                    line: 60,
                },
            })
        }
    }

    /**
     * Reset Athletes IpScore
     *
     * @memberof IpScoresCtrl
     */
    resetAthletesIpScore = async (
        req: Request,
        res: Response
    ): Promise<boolean | string> => {
        this.res = res
        try {
            // socket.emit(AggregateLogLevel.Info, {
            //     type: AggregateLogLevel.Info,
            //     data: { message: 'addBiomentricEntryMultiple called!!!' },
            //     // AggregateLogLevel
            // })

            // this.updateIPScore()

            const athleteWeeklyIpScoreDocs = await resetAthletesIpScore()

            return this.handleResponse({
                ...(athleteWeeklyIpScoreDocs as any),
                res,
            }) // TODO: SORT ANY
            // this.aggregateDocumentManager = new AggregateDocumentManager();

            // return await this.ProcessRequest(req.body as DoReadPayload)
        } catch (exception) {
            // TODO: Exception not being passed to handleResponse()??
            socket.emit('evt-exception', {
                message: 'Could not add Biometric Entry',
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred in  resetAthletesIpScore()',
                    // exception
                },
                exception,
                method: {
                    name: 'addBiomentricEntry',
                    line: 60,
                },
            })
            console.log('exception', exception)
            return this.handleResponse({
                message: 'resetAthletesIpScore',
                responseCode: 500,
                data: undefined,
                error: {
                    message: 'resetAthletesIpScore',
                    exception: { ...exception },
                },
                method: {
                    name: 'resetAthletesIpScore',
                    line: 60,
                },
            })
        }
    }

    private async ProcessRequest(body: DoReadPayload) {
        const { entry, options } = body
        const {
            // dev -- set dynamic logging level :)
            // logLevel,
            apiHandlesDoRead,
            handleDoWriteEntry,
        } = options
        let { count } = options
        // const userId = 'Pdiv4bIfhscWbahhIkO40EJzm6C2';
        const { userId, lifestyleEntryType, value } = entry
        if (!count) {
            count = 1
        }
        // if (entry.dateTime.seconds) {
        //     entry.dateTime = new firestore.Timestamp(
        //         entry.dateTime.seconds,
        //         entry.dateTime.nanoseconds
        //     )
        // } else {
        //     this.convertCreationDatesToTimestamp(entry)
        // }

        debugger
        // TODO: CHEKC TRANSACTION
        const t: FirebaseFirestore.Transaction = undefined

        return this.aggregateDocumentManager
            .doRead(
                entry,
                entry.userId,
                DocMngrActionTypes.Read,
                'api/addBiomentricEntryMultiple',
                'Info',
                t
            )
            .then(data => {
                if (data) {
                    console.log(data)
                    this.res.status(200).json({
                        data,
                    })
                    return Promise.resolve(data)
                } else {
                    LogInfo('empty data')
                    this.res.status(201).json({
                        data,
                    })
                    return Promise.resolve(null)
                }
            })
            .catch(err => {
                LogInfo('Eooro Occured!!!!', err)
                return Promise.reject(err)
            })

        const logLevel = options.logLevel ? options.logLevel : 'Info'

        const athleteBiometricEntryRef = db
            .collection(FirestoreCollection.Athletes)
            .doc(userId)
            .collection('biometricEntries')
        let result: boolean | void
        _.throttle(this.saveEntry, 1001, { trailing: true })
        for (let index = 5; index < count + 5; index++) {
            result = await this.saveEntry(
                lifestyleEntryType,
                value,
                entry,
                athleteBiometricEntryRef,
                apiHandlesDoRead,
                handleDoWriteEntry,
                logLevel
            )
        }

        console.log(result)
        console.log('return this.handleResponse()')

        return this.handleResponse({
            message: 'Athlete Biometric Entry Saved',
            responseCode: 201,
            data: {
                docs: this.entries,
            },
            error: undefined,
            method: {
                name: 'addBiomentricEntry',
                line: 146,
            },
        })
    }

    private async saveEntry(
        lifestyleEntryType: LifestyleEntryType,
        value: number,
        entry: AthleteBiometricEntryModel,
        athleteBiometricEntryRef: firestore.CollectionReference,
        apiHandlesDoRead: boolean,
        handleDoWriteEntry: boolean,
        logLevel: string
    ) {
        // new AthleteBiometricEntryModel(
        const athleteBiometricEntry: AthleteBiometricEntryModel = {
            ...AthleteBiometricEntryDocument,
            // ...entry,
            organizationId: 'idherePlease',
            // creationTimestamp: firestore.Timestamp.fromDate(getMonthStart(moment('10/7/2018').toDate())),
            // userId,
            // fullName: 'Athlete Test',
            groups: [
                //   {
                //     groupId: `groupID${index - 4}`,
                //     groupName: `Group ${index - 4}`,
                //     creationTimestamp: firestore.Timestamp.fromDate(getMonthStart(moment('5/3/2011').toDate()))
                //   }
                //   // ,
                //   // {
                //   //   groupId: `groupID${index - 3}`,
                //   //   groupName: `Group ${index - 3}`,
                //   //   creationTimestamp: firestore.Timestamp.fromDate(getMonthStart(moment('10/12/2015').toDate()))
                //   // }
            ],
            dateTime: firestore.Timestamp.now(),
            lifestyleEntryType,
            value,
            ...entry,
            uid: entry.userId,
        } // as AthleteBiometricEntry

        try {
            // socket.emit(AggregateLogLevel.Info, {
            //   type: 'readOrNot ',
            //   apiHandlesDoRead: apiHandlesDoRead,
            // });
            if (apiHandlesDoRead) {
                // socket.emit(AggregateLogLevel.Info, {
                //   type: 'reading ',
                //   apiHandlesDoRead: apiHandlesDoRead,
                // });
                socket.emit(AggregateLogLevel.Info, {
                    type: AggregateLogLevel.Info,
                    data: {
                        message: 'API does read!!!!!!!!!!! ',
                        entry,
                        handleDoWriteEntry:
                            !!handleDoWriteEntry || handleDoWriteEntry,
                    },
                })

                debugger
                // TODO: CHEKC TRANSACTION
                const t: FirebaseFirestore.Transaction = undefined

                return this.aggregateDocumentManager
                    .doRead(
                        athleteBiometricEntry,
                        athleteBiometricEntry.userId,
                        DocMngrActionTypes.Read,
                        'api/addBiomentricEntryMultiple',
                        logLevel,
                        t
                    )
                    .then(data => {
                        if (data) {
                            console.log(data)
                            return Promise.resolve(data)
                        } else {
                            LogInfo('empty data')
                            return Promise.resolve(null)
                        }
                    })
                    .catch(err => {
                        LogInfo('Eooro Occured!!!!', err)
                        return Promise.reject(err)
                    })
            }

            if (!!handleDoWriteEntry || handleDoWriteEntry) {
                socket.emit(AggregateLogLevel.Info, {
                    type: AggregateLogLevel.Info,
                    data: {
                        message: 'Inserting Biometric Entry ',
                        athleteBiometricEntry,
                        handleDoWriteEntry:
                            !!handleDoWriteEntry || handleDoWriteEntry,
                    },
                })
                const result = await this.insertRecord(
                    athleteBiometricEntry,
                    athleteBiometricEntryRef,
                    apiHandlesDoRead,
                    logLevel
                )

                console.log('Record Inserted?', {
                    entry: athleteBiometricEntry,
                    result,
                })

                return result
            } else return Promise.resolve(true)
        } catch (error) {
            console.error({
                message: 'Save Biometric Entry Error',
                data: { entry, error },
            })
            socket.emit('evt-exception', {
                type: 'biometric entryAdd ',
                message: 'entry',
                data: { entry, error },
            })

            throw error
        }
    }

    // private convertCreationDatesToTimestamp(entry: AthleteBiometricEntry) {
    //     entry.dateTime = this.timestampFromDate(<Date>(entry.dateTime as any))
    //     entry.creationTimestamp = this.timestampFromDate(<Date>(
    //         (entry.creationTimestamp as any)
    //     ))

    //     if (entry.groups) {
    //         entry.groups.map(group => {
    //             group.creationTimestamp = this.timestampFromDate(<Date>(
    //                 (group.creationTimestamp as any)
    //             ))
    //         })
    //     }
    // }

    // private timestampFromDate(creationTimestamp: Date) {
    //     socket.emit('evt-timestamp-conversion', {
    //         type: 'timestampFromDate ',
    //         creationTimestamp: creationTimestamp,
    //     });
    //     return creationTimestamp
    //         ? this.toTimestamp(moment(creationTimestamp).toDate())
    //         : this.toTimestamp(moment('10/12/2015').toDate())
    // }

    // private toTimestamp(date: Date): firestore.Timestamp {
    //     console.log('firestore.Timestamp.fromDate(date);', date);
    //     socket.emit('evt-timestamp-conversion', {
    //         type: 'toTimestamp ',
    //         date: date,
    //     });

    //     let d;
    //     try {
    //         d = firestore.Timestamp.fromDate(date)
    //     } catch (error) {
    //         console.log('error toTimestamp', error);
    //         d = firestore.Timestamp.fromDate(moment('10/2/2018').toDate())
    //     }
    //     return d
    // }

    private async insertRecord(
        athleteBiometricEntry: AthleteBiometricEntryInterface,
        athleteBiometricEntryRef: firestore.CollectionReference,
        apiHandlesDoRead: boolean,
        logLevel: string
    ): Promise<boolean> {
        try {
            const writeReult = await athleteBiometricEntryRef
                .doc()
                .set({ ...athleteBiometricEntry, logLevel, apiHandlesDoRead })
            if (this.entries) {
                return this.entries.push({
                    athleteBiometricEntry,
                    writeReult,
                })
            } else {
                this.entries = [
                    {
                        athleteBiometricEntry,
                        writeReult,
                    },
                ]
                return true
            }
            // console.log('this.entries', this.entries);
        } catch (exception) {
            console.log('exception', exception)
            return this.handleResponse({
                message: 'An unanticipated exception occurred => ' + exception,
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred => ' + exception,
                    exception: exception,
                },
                method: {
                    name: 'addBiomentricEntry',
                    line: 101,
                },
            })
        }
    }

    private async updateIPScore(
        athleteBiometricEntry: AthleteBiometricEntryInterface,
        athleteBiometricEntryRef: firestore.CollectionReference,
        apiHandlesDoRead: boolean,
        logLevel: string
    ): Promise<boolean> {
        try {
            const writeReult = await athleteBiometricEntryRef
                .doc()
                .set({ ...athleteBiometricEntry, logLevel, apiHandlesDoRead })
            if (this.entries) {
                return this.entries.push({
                    athleteBiometricEntry,
                    writeReult,
                })
            } else {
                this.entries = [
                    {
                        athleteBiometricEntry,
                        writeReult,
                    },
                ]
                return true
            }
            // console.log('this.entries', this.entries);
        } catch (exception) {
            console.log('exception', exception)
            return this.handleResponse({
                message: 'An unanticipated exception occurred => ' + exception,
                responseCode: 500,
                data: undefined,
                error: {
                    message:
                        'An unanticipated exception occurred => ' + exception,
                    exception: exception,
                },
                method: {
                    name: 'addBiomentricEntry',
                    line: 101,
                },
            })
        }
    }
}
