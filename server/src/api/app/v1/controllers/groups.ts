import { AthleteIndicator } from '../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { Response, Request } from 'express'
import { firestore } from 'firebase-admin';
import { cloneDeep, isArray } from 'lodash';
// import { FieldValue } from '@google-cloud/firestore';

import BaseCtrl from './base'

import { GroupHistoryDocumentManager } from '../../../../db/biometricHistory/groups/managers/history-document-manager'

import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'

import { db } from '../../../../shared/init/initialise-firebase'
import { LogInfo } from '../../../../shared/logger/logger';
import { Group } from '../../../../models/group/group.model';
import { Athlete, OrganizationIndicator } from '../../../../models/athlete/interfaces/athlete';
import { purgeAthleteAggregateDocuments } from './utils/purgeAggregateDocuments';
import { Organization, OrganizationInterface } from '../../../../models/organization.model';
import { getUserByEmail } from "./get-user-by-email";
import { updateAthleteIndicatorsIPScoreOnGroup, updateAthleteIndicatorsIPScoreOnGroups } from '../callables/utils/update-athlete-indicators-ipscore-on-group';
import { getGroupDocRef, getAthleteDocRef, getOrgDocRef } from '../../../../db/refs';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { createEntities } from '../callables/utils/onboarding/create-entities';
import { OnboardingValidationResponse } from '../interfaces/onboarding-validation-response';
import { getUserFirstName, getUserLastName } from '../../../utils/getUserFullName';
import { resetAthleteIndicatorsDailyIPScore } from '../../../../analytics/triggers/reset-athlete-indicators-daily-ipscore';
import { SubscriptionType } from '../../../../models/enums/enums.model';
import moment from 'moment';
import { IpScoreGroupAggregationDirective } from '../callables/run-group-ip-score-aggregation';
import { WellGroupAggregationDirective } from './biometrics';
import { AggregateDocumentManager } from '../../../../db/biometricEntries/managers/aggregate-document-manager';
import { convertAllDatesStringsToTimeStamps } from '../callables/run-group-wellness-data-aggregation';
import { DocMngrActionTypes } from '../../../../db/biometricEntries/enums/document-manager-action-types';
import { getUniqueListBy } from '../callables/utils/getUniqueListBy';
import { updateAthlete, removeAllAthleteIndicators, doesAthleteExistInGroup } from '../callables/moveAthleteToGroup';
import { GroupIndicator } from '../../../../db/biometricEntries/well/interfaces/group-indicator';
import { clearAthleteIndicatorsDailyIPScore } from '../../../../analytics/triggers/clear-athlete-indicators-daily-ipscore';

// import { removeAllAthleteIndicators } from '../callables/addAthleteToGroup';
import { WhiteLabelTheme } from '../interfaces/WhiteLabelTheme';


/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class GroupCtrl
 * @extends {BaseCtrl}
 */
export default class GroupsCtrl extends BaseCtrl {
    res: Response;

    // logger: Logger;
    model = Group;
    collection = FirestoreCollection.Groups;
    groupHistoryDocumentManager: GroupHistoryDocumentManager;

    constructor() {
        super()
    }

    arrayMove = (arr: Array<any>, oldIndex: number, newIndex: number) => {
        if (newIndex >= arr.length) {
            let k = newIndex - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
        return arr; 
    };

    updateIP = async (req: Request, res: Response) => {
        const { group, groupUId } = <{ group: Group, groupUId: string }>req.body
        console.log(req.body);
        // TODO: Pass utcOffset
        return await updateAthleteIndicatorsIPScoreOnGroup(groupUId, 10).then((results) => {
            return res.status(200).json({
                message: 'Group added',
                responseCode: 201,
                data: {
                    group,
                    result: results,
                },
                error: undefined,
                method: {
                    name: 'addGroup',
                    line: 58,
                },
            })
        }).catch((err) => {
            return res.status(200).json({
                message: 'Group added',
                responseCode: 201,
                data: {
                    group,
                },
                error: err,
                method: {
                    name: 'addGroup',
                    line: 58,
                },
            })
        })
        // const group = {
        //     athletes: [],
        //     creationTimestamp: new Date(),
        //     groupId: "8aRllwKnpOw01lvhB2j8-QLD1",
        //     groupIdentifier: "QLD1",
        //     groupName: "Diving Australia: Queensland",
        //     logoUrl: "https://s3-ap-southeast-2.amazonaws.com/piano.revolutionise.com.au/logos/yuojmbb9cnx1akfn.png",
        //     organizationId: "8aRllwKnpOw01lvhB2j8",
        //     uid: "8aRllwKnpOw01lvhB2j8-QLD1"
        // };

        // See the GroupRecord reference doc for the contents of groupRecord.
        console.log('...: ', group.uid);

        if (group && group.uid) {
            // save group

            group.creationTimestamp = firestore.Timestamp.fromDate(new Date(group.creationTimestamp as any))
            return getGroupDocRef(group.uid)
                .set(group)
                .then(writeResult => {
                    return res.status(200).json({
                        message: 'Group added',
                        responseCode: 201,
                        data: {
                            group,
                            result: writeResult,
                        },
                        error: undefined,
                        method: {
                            name: 'addGroup',
                            line: 58,
                        },
                    })
                })
                .catch(() => {
                    return res.status(502).json({
                        message: 'Unable to add Group',
                        responseCode: 201,
                        data: {
                            group,
                        },
                        error: undefined,
                        method: {
                            name: 'addGroup',
                            line: 58,
                        },
                    })
                })
        } else {
            return res.status(504).json({
                message: 'Unable to add Group = Group or uid Undefined',
                responseCode: 201,
                data: {
                    group,
                },
                error: undefined,
                method: {
                    name: 'addGroup',
                    line: 58,
                },
            })
        }

    };
    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof GroupsCtrl
     */


    resetAthleteIndicatorsDailyIPScore = async (req: Request, res: Response) => {

        try {

            const {
                // uid: athleteUID,
                ipScore, runningTotalIpScore, groupIds, utcOffset }
                = <{ uid: string, ipScore: string, runningTotalIpScore: string, groupIds: string | Array<string>, utcOffset: number }>req.body


            const theGroupIds = isArray(groupIds)
                ? () => {
                    console.log('isArray(groupIds) => ', isArray(groupIds));
                    return groupIds;
                }
                : () => {
                    console.log('isArray(groupIds) => ', isArray(groupIds));
                    return JSON.parse(groupIds);
                }

            const theDailyIPScore = (ipScore !== undefined) ? +ipScore : 0
            const therunningTotalIpScore = (runningTotalIpScore !== undefined) ? +runningTotalIpScore : undefined
            debugger;
            const updateResults = await resetAthleteIndicatorsDailyIPScore(theGroupIds(), therunningTotalIpScore, theDailyIPScore, utcOffset || 0);

            if (updateResults.length) {

                // const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName


                return res.status(200).json({
                    message: `IPScore Group Indicators update results for ${updateResults[0].groupUID}`,
                    responseCode: 201,
                    data: updateResults,
                    error: undefined,
                    method: {
                        name: 'addGroup',
                        line: 58,
                    },
                })
                // return {
                //     message: `IPScore Group Indicators update results for ${updateResults[0].groupUID}`,
                //     updateResults
                // }

            }
            else {
                console.warn(`No ipscore GroupIndicator update results ${groupIds}`);

                return {
                    message: `No ipscore GroupIndicator update results ${groupIds}`
                }
            }


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }

    };

    clearIPScore = async (req: Request, res: Response) => {

        try {

            const {
                // uid: athleteUID,
          groupIds }
                = <{  groupIds: string | Array<string> }>req.body


            const theGroupIds = isArray(groupIds)
                ? () => {
                    console.log('isArray(groupIds) => ', isArray(groupIds));
                    return groupIds;
                }
                : () => {
                    console.log('isArray(groupIds) => ', isArray(groupIds));
                    // return JSON.parse(groupIds as string) as Array<string>
                    return [groupIds];
                }

            const theDailyIPScore = 0
            const therunningTotalIpScore = 0
            debugger;
            const updateResults = await clearAthleteIndicatorsDailyIPScore(theGroupIds());

            if (updateResults.length) {

                // const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName


                return res.status(200).json({
                    message: `IPScore Group Indicators update results for ${updateResults[0].groupUID}`,
                    responseCode: 201,
                    data: updateResults,
                    error: undefined,
                    method: {
                        name: 'addGroup',
                        line: 58,
                    },
                })
                // return {
                //     message: `IPScore Group Indicators update results for ${updateResults[0].groupUID}`,
                //     updateResults
                // }

            }
            else {
                console.warn(`No ipscore GroupIndicator update results ${groupIds}`);

                return {
                    message: `No ipscore GroupIndicator update results ${groupIds}`
                }
            }


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }

    };

    updateSubscription = async (req: Request, res: Response) => {

        try {
            const nowMoment = moment();
            const subscriptionEndDate = nowMoment.clone().add(12, 'months');


            const groupRef = getGroupDocRef('EENrKqTlFw8i9MgC3gWh-BRIS');
            const group = getDocumentFromSnapshot(await groupRef.get()) as Group


            const updates = group.athletes.map((athleteIndicator) => {
                athleteIndicator.subscriptionType = SubscriptionType.Basic;
                athleteIndicator.subType = 'Basic';




                return athleteIndicator
            })

            await groupRef.update({ athletes: updates })

            await Promise.all(updates.map(async (athleteIndicator) => {


                const atheteRef = getAthleteDocRef(athleteIndicator.uid)

                const athlete = getDocumentFromSnapshot(await atheteRef.get()) as Athlete

                if (athlete) {
                    if (!athlete.profile.subscription) {
                        athlete.profile.subscription = {
                            type: SubscriptionType.Basic,
                            status: 1,
                            commencementDate: firestore.Timestamp.fromDate(nowMoment.toDate()),
                            expirationDate: firestore.Timestamp.fromDate(subscriptionEndDate.toDate())
                        }
                    } else {

                        athlete.profile.subscription.type = SubscriptionType.Basic;
                        athlete.profile.subscription.status = 1;
                        athlete.profile.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
                        athlete.profile.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());
                    }
                    await atheteRef.update(athlete).catch((err) => {
                        console.warn(err)
                    })
                }
            }))

            return res.status(200).json({
                message: `IPScore Group Indicators update results for `,
                responseCode: 201,
                data: {},
                error: undefined,
                method: {
                    name: 'addGroup',
                    line: 58,
                },
            })
            // return {
            //     message: `IPScore Group Indicators update results for ${updateResults[0].groupUID}`,
            //     updateResults
            // }


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }
    };

    runGroupWellAggregation = async (req: Request, res: Response) => {

        try {


            const directive = <WellGroupAggregationDirective>req.body
            console.log('WellGroupAggregationDirective - data', req.body)

            const { userId: AthleteUID } = directive.entries[0];

            if (!!directive && !!directive.entries && isArray(directive.entries)) {
                const theUpdateResults = await Promise.all(directive.entries.map(async (biometricEntry, idx, allEntries) => {

                    const { logLevel } = directive.options;

                    const aggregateDocumentManager = new AggregateDocumentManager();

                    if (biometricEntry) {
                        console.error('biometricEntry', biometricEntry)

                        // const transactionResult = await db.runTransaction(async t => {
                        biometricEntry.skipAggregate = true
                        const t = undefined

                        convertAllDatesStringsToTimeStamps(biometricEntry)

                        const groupUpdateResults = await aggregateDocumentManager.doRead(biometricEntry, biometricEntry.userId || AthleteUID, DocMngrActionTypes.Read, 'onCreate', logLevel, t)

                            .then((docDirective) => {
                                LogInfo(`OnCreate Promise Received, aggregation all done for entry ${allEntries[idx].collection} (${idx + 1} of ${directive.entries.length})!!!!!!!!`, docDirective);
                                return Promise.resolve(docDirective);
                            })
                            .catch((err) => {
                                debugger;

                                LogInfo(`updateBiometricEntries.onCreate() execution failed! - err -> ${err}`);
                                return Promise.reject(
                                    `updateBiometricEntries.onCreate() execution failed! - err -> ${err}`
                                )

                            });

                        return groupUpdateResults

                        // }).then((results) => {
                        //     console.log('Transaction success!', results);
                        //     return true;
                        // }).catch(err => {
                        //     console.log('Transaction failure:', err);
                        //     return false
                        // });

                        // return transactionResult

                    } else {
                        console.error('No document to process')
                        // return false

                        return res.status(200).json({
                            message: `No document to process`,
                            responseCode: 201,
                            data: {},
                            error: undefined,
                            method: {
                                name: 'addGroup',
                                line: 58,
                            },
                        })
                    }

                })).catch((err) => {
                    console.error(
                        '.........  const theUpdateResults = await Promise.all() CATCH ERROR:',
                        err
                    )

                    // return false

                    return res.status(500).json({
                        message: `.........  const theUpdateResults = await Promise.all() CATCH ERROR:`,
                        responseCode: 201,
                        data: {},
                        error: undefined,
                        method: {
                            name: 'addGroup',
                            line: 58,
                        },
                        err
                    })

                }).then(() => true)

                return theUpdateResults

            } else {
                console.error(
                    '.........  No Entries in Payload',
                )

                // return false

                // console.error('No document to process')

                return res.status(200).json({
                    message: `No document to process`,
                    responseCode: 201,
                    data: {},
                    error: undefined,
                    method: {
                        name: 'addGroup',
                        line: 58,
                    },
                })
            }

        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )
            return res.status(200).json({
                message: `No document to process`,
                responseCode: 201,
                data: {},
                method: {
                    name: 'addGroup',
                    line: 58,
                },
                error
            })
            // return Promise.reject(error)
        }





    };

    runGroupIPScoreAggregation = async (req: Request, res: Response) => {

        try {
            debugger;
            const ipScoreGroupAggregationDirective = <IpScoreGroupAggregationDirective>req.body
            console.log('ipScoreGroupAggregationDirective', ipScoreGroupAggregationDirective)
            console.log('commands', JSON.stringify(ipScoreGroupAggregationDirective.commands))


            const { uid: athleteUID } = ipScoreGroupAggregationDirective as any;
            debugger;

            if (ipScoreGroupAggregationDirective) {

                console.log('ipScoreGroupAggregationDirective', ipScoreGroupAggregationDirective)
                console.log('commands', JSON.stringify(ipScoreGroupAggregationDirective.commands))

                const {
                    utcOffset } = ipScoreGroupAggregationDirective

                let { groupIds } = ipScoreGroupAggregationDirective

                if (!(groupIds && groupIds.length)) {
                    groupIds = []
                }


                debugger


                const allTheResults = await updateAthleteIndicatorsIPScoreOnGroups(groupIds, utcOffset, athleteUID).then(async (ipScoreUpdateResults) => {
                    console.log('=================>>>>> Athlete indicators updated', ipScoreUpdateResults)

                    return true // allResults
                })

                return allTheResults

                // // runDate: string;
                // // paths: Array<string>;
                // // currentIpScoreTracking
                // const result = updateAggregates(athlete, utcOffset
                //     // currentIpScoreTracking.directive.utcOffset
                // )
                //     .then(async docs => {
                //         console.log('----->>>>> PROMISE RESOLVED, Aggregation Complete');
                //         // console.log(docs[1])
                //         // console.log(docs)

                //         return docs
                //     })
                //     .catch(err => {
                //         console.log(err);

                //         return err
                //     });


                // return athletePromises;

                // return await aggregateDocumentManager.doRead(biometricEntry, DocMngrActionTypes.Read, 'onCreate', logLevel)

                // .then((docDirective) => {
                //     LogInfo('OnCreate Promise Received, aggregation all done!!!!!!!!', docDirective);
                //     return Promise.resolve(docDirective);
                // })

                // .catch((err) => {
                //     debugger;

                //     LogInfo(`updateBiometricEntries.onCreate() execution failed! - err -> ${err}`);
                //     return Promise.reject(
                //         `updateBiometricEntries.onCreate() execution failed! - err -> ${err}`
                //     )

                // });
            } else {
                // LogInfo(`updateBiometricEntries.onCreate() execution failed! - err -> ${err}`);
                console.error('No document to process')

                return res.status(200).json({
                    message: `No document to process`,
                    responseCode: 201,
                    data: {},
                    error: undefined,
                    method: {
                        name: 'addGroup',
                        line: 58,
                    },
                })
                // return false
            }



            // const nowMoment = moment();
            // const subscriptionEndDate = nowMoment.clone().add(12, 'months');


            // const groupRef = getGroupDocRef('EENrKqTlFw8i9MgC3gWh-BRIS');
            // const group = getDocumentFromSnapshot(await groupRef.get()) as Group


            // const updates = group.athletes.map((athleteIndicator) => {
            //     athleteIndicator.subscriptionType = SubscriptionType.Basic;
            //     athleteIndicator.subType = 'Basic';




            //     return athleteIndicator
            // })

            // await groupRef.update({ athletes: updates })

            // await Promise.all(updates.map(async (athleteIndicator) => {


            //     const atheteRef = getAthleteDocRef(athleteIndicator.uid)

            //     const athlete = getDocumentFromSnapshot(await atheteRef.get()) as Athlete

            //     if (athlete) {
            //         if (!athlete.profile.subscription) {
            //             athlete.profile.subscription = {
            //                 type: SubscriptionType.Basic,
            //                 status: 1,
            //                 commencementDate: firestore.Timestamp.fromDate(nowMoment.toDate()),
            //                 expirationDate: firestore.Timestamp.fromDate(subscriptionEndDate.toDate())
            //             }
            //         } else {

            //             athlete.profile.subscription.type = SubscriptionType.Basic;
            //             athlete.profile.subscription.status = 1;
            //             athlete.profile.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
            //             athlete.profile.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());
            //         }
            //         await atheteRef.update(athlete).catch((err) => {
            //             console.warn(err)
            //         })
            //     }
            // }))

            // return {
            //     message: `IPScore Group Indicators update results for ${updateResults[0].groupUID}`,
            //     updateResults
            // }


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }





    };

    runGroupIPScoreAggregationOld = async (req: Request, res: Response) => {

        try {
            debugger;
            const ipScoreGroupAggregationDirective = <IpScoreGroupAggregationDirective>req.body
            console.log('ipScoreGroupAggregationDirective', ipScoreGroupAggregationDirective)
            console.log('commands', JSON.stringify(ipScoreGroupAggregationDirective.commands))


            debugger;

            if (ipScoreGroupAggregationDirective) {

                console.log('ipScoreGroupAggregationDirective', ipScoreGroupAggregationDirective)
                console.log('commands', JSON.stringify(ipScoreGroupAggregationDirective.commands))


                let { groupIds } = ipScoreGroupAggregationDirective

                if (!(groupIds && groupIds.length)) {
                    groupIds = []
                }


                debugger
                const transactionResult = await db.runTransaction(
                    async (d) => {


                        console.log(d)

                    }
                ).then((results) => {
                    console.log('Transaction success!', results);
                    return true;
                }).catch(err => {
                    console.log('Transaction failure:', err);
                    return false
                });


                return transactionResult

                // // runDate: string;
                // // paths: Array<string>;
                // // currentIpScoreTracking
                // const result = updateAggregates(athlete, utcOffset
                //     // currentIpScoreTracking.directive.utcOffset
                // )
                //     .then(async docs => {
                //         console.log('----->>>>> PROMISE RESOLVED, Aggregation Complete');
                //         // console.log(docs[1])
                //         // console.log(docs)

                //         return docs
                //     })
                //     .catch(err => {
                //         console.log(err);

                //         return err
                //     });


                // return athletePromises;

                // return await aggregateDocumentManager.doRead(biometricEntry, DocMngrActionTypes.Read, 'onCreate', logLevel)

                // .then((docDirective) => {
                //     LogInfo('OnCreate Promise Received, aggregation all done!!!!!!!!', docDirective);
                //     return Promise.resolve(docDirective);
                // })

                // .catch((err) => {
                //     debugger;

                //     LogInfo(`updateBiometricEntries.onCreate() execution failed! - err -> ${err}`);
                //     return Promise.reject(
                //         `updateBiometricEntries.onCreate() execution failed! - err -> ${err}`
                //     )

                // });
            } else {
                // LogInfo(`updateBiometricEntries.onCreate() execution failed! - err -> ${err}`);
                console.error('No document to process')

                return res.status(200).json({
                    message: `No document to process`,
                    responseCode: 201,
                    data: {},
                    error: undefined,
                    method: {
                        name: 'addGroup',
                        line: 58,
                    },
                })
                // return false
            }



            // const nowMoment = moment();
            // const subscriptionEndDate = nowMoment.clone().add(12, 'months');


            // const groupRef = getGroupDocRef('EENrKqTlFw8i9MgC3gWh-BRIS');
            // const group = getDocumentFromSnapshot(await groupRef.get()) as Group


            // const updates = group.athletes.map((athleteIndicator) => {
            //     athleteIndicator.subscriptionType = SubscriptionType.Basic;
            //     athleteIndicator.subType = 'Basic';




            //     return athleteIndicator
            // })

            // await groupRef.update({ athletes: updates })

            // await Promise.all(updates.map(async (athleteIndicator) => {


            //     const atheteRef = getAthleteDocRef(athleteIndicator.uid)

            //     const athlete = getDocumentFromSnapshot(await atheteRef.get()) as Athlete

            //     if (athlete) {
            //         if (!athlete.profile.subscription) {
            //             athlete.profile.subscription = {
            //                 type: SubscriptionType.Basic,
            //                 status: 1,
            //                 commencementDate: firestore.Timestamp.fromDate(nowMoment.toDate()),
            //                 expirationDate: firestore.Timestamp.fromDate(subscriptionEndDate.toDate())
            //             }
            //         } else {

            //             athlete.profile.subscription.type = SubscriptionType.Basic;
            //             athlete.profile.subscription.status = 1;
            //             athlete.profile.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
            //             athlete.profile.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());
            //         }
            //         await atheteRef.update(athlete).catch((err) => {
            //             console.warn(err)
            //         })
            //     }
            // }))

            // return {
            //     message: `IPScore Group Indicators update results for ${updateResults[0].groupUID}`,
            //     updateResults
            // }


        } catch (error) {
            console.error(
                '.........CATCH ERROR:',
                error
            )

            return Promise.reject(error)
        }





    };

    addGroup = async (req: Request, res: Response) => {
        const { organizationId,
            organizationName,
            organizationCode,
            groupIdentifier,
            logoUrl = "" } = <{
                organizationId: string;
                organizationName: string;
                organizationCode: string;
                groupIdentifier: string;
                logoUrl: string;
            }>req.body
        console.log(req.body);

        // const group = {
        //     athletes: [],
        //     creationTimestamp: new Date(),
        //     groupId: "8aRllwKnpOw01lvhB2j8-QLD1",
        //     groupIdentifier: "QLD1",
        //     groupName: "Diving Australia: Queensland",
        //     logoUrl: "https://s3-ap-southeast-2.amazonaws.com/piano.revolutionise.com.au/logos/yuojmbb9cnx1akfn.png",
        //     organizationId: "8aRllwKnpOw01lvhB2j8",
        //     uid: "8aRllwKnpOw01lvhB2j8-QLD1"
        // };


        const group = {
            uid: `${organizationId}-${groupIdentifier}`,
            groupId: `${organizationId}-${groupIdentifier}`,
            groupName: `${organizationCode}-${groupIdentifier}`,
            organizationId,
            organizationName,
            groupIdentifier,
            logoUrl,
            creationTimestamp: firestore.Timestamp.now(),
            athletes: [],
        };

        // const group = {
        //     ...theGroup,
        //     creationTimestamp: firestore.Timestamp.now(),
        //     athletes: [],
        // };
        // See the GroupRecord reference doc for the contents of groupRecord.
        console.log('...: ', group.uid);

        if (group && group.uid) {
            // save group
            // group.creationTimestamp = firestore.Timestamp.fromDate(new Date(group.creationTimestamp as any))
            return getGroupDocRef(group.uid)
                .set(group)
                .then(writeResult => {
                    return res.status(200).json({
                        message: 'Group added',
                        responseCode: 201,
                        data: {
                            group,
                            result: writeResult,
                        },
                        error: undefined,
                        method: {
                            name: 'addGroup',
                            line: 58,
                        },
                    })
                })
                .catch(() => {
                    return res.status(502).json({
                        message: 'Unable to add Group',
                        responseCode: 201,
                        data: {
                            group,
                        },
                        error: undefined,
                        method: {
                            name: 'addGroup',
                            line: 58,
                        },
                    })
                })
        } else {
            return res.status(504).json({
                message: 'Unable to add Group = Group or uid Undefined',
                responseCode: 201,
                data: {
                    group,
                },
                error: undefined,
                method: {
                    name: 'addGroup',
                    line: 58,
                },
            })
        }

    };
    /**
        * Adds a biometric entry to the database
        * Currently using so that I can test the onCreate events! ;P
        *
        * @memberof GroupsCtrl
        */
    addAthlete = async (req: Request, res: Response) => {

        const { groupId, userId, coachFlag,
            //  moveUserOrg,
            orgUID: theOrgID, orgName } = <{ groupId: string, userId: string, coachFlag?: boolean, moveUserOrg: boolean, orgUID: string, orgName: string }>req.body;
        console.log(req.body);


        // const group = {
        //     athletes: [],
        //     creationTimestamp: new Date(),
        //     groupId: "8aRllwKnpOw01lvhB2j8-QLD1",
        //     groupIdentifier: "QLD1",
        //     groupName: "Diving Australia: Queensland",
        //     logoUrl: "https://s3-ap-southeast-2.amazonaws.com/piano.revolutionise.com.au/logos/yuojmbb9cnx1akfn.png",
        //     organizationId: "8aRllwKnpOw01lvhB2j8",
        //     uid: "8aRllwKnpOw01lvhB2j8-QLD1"
        // }

        let group = {} as Group;
        let oldAthlete: Athlete;
        if (groupId && userId) {
            return getGroupDocRef(groupId).get()
                .then(async (groupSnap) => {
                    if (groupSnap.exists) {

                        group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
                        if (!group.athletes) {
                            group.athletes = []
                        }
                        const atheleteRef = getAthleteDocRef(userId);// getAthleteDocRef('5FqVZGNUqVV9rPpo48CtqPB8xYE3').get()
                        await atheleteRef.get()
                            .then(async (athleteSnap) => {
                                if (athleteSnap.exists) {

                                    const orgUID = theOrgID || group.organizationId;
                                    const theAthlete = { ...athleteSnap.data(), uid: athleteSnap.id } as Athlete;
                                    oldAthlete = cloneDeep(theAthlete);
                                    // TODO: Test All this


                                    if (!theAthlete.organizations || theAthlete.organizations.find((org) => org.organizationId === group.organizationId) === undefined) {

                                        if (!theAthlete.organizations) {
                                            theAthlete.organizations = [{
                                                organizationId: orgUID || group.organizationId,
                                                organizationName: orgName || group.organizationName,
                                                organizationCode: 'UNKOWN',
                                                active: true,
                                                joinDate: firestore.Timestamp.now()
                                            }]
                                        } else {
                                            debugger;


                                            const currOrg = theAthlete.organizations.find((org) => org.organizationId === theAthlete.organizationId);

                                            if (currOrg && !currOrg.joinDate) {

                                                currOrg.joinDate = theAthlete.metadata.creationTimestamp;

                                            }
                                            const currNewOrg = theAthlete.organizations.find((org) => org.organizationId === orgUID);

                                            if (!currNewOrg) {
                                                theAthlete.organizations.unshift({
                                                    organizationId: group.organizationId,
                                                    organizationName: group.organizationName,
                                                    organizationCode: 'UNKOWN',
                                                    active: true,
                                                    joinDate: firestore.Timestamp.now()
                                                })
                                            } else {
                                                currNewOrg.active = true;
                                            }

                                        }
                                    }


                                    if (!theAthlete.groups) {
                                        theAthlete.groups = []
                                    }
                                    if (!theAthlete.metadata.creationTimestamp) {
                                        const user = await getUserByEmail(theAthlete.profile.email || theAthlete.email)
                                        theAthlete.metadata.creationTimestamp =
                                            firestore.Timestamp.fromDate(new Date(user.metadata.creationTime))
                                        theAthlete.metadata.lastSignInTimestamp =
                                            firestore.Timestamp.fromDate(new Date(user.metadata.lastSignInTime))
                                    }
                                    try {
                                        if (!theAthlete.metadata.creationTimestamp && theAthlete.profile.onboardingCode.toUpperCase() === 'INSPIRE') {
                                            theAthlete.metadata.creationTimestamp = theAthlete.groups[0].creationTimestamp
                                        }
                                    } catch (error) {
                                        console.error('Athlete Creation Timestamp Undefinded, setting to now', firestore.Timestamp.now().toDate())
                                        theAthlete.metadata.creationTimestamp = firestore.Timestamp.now()
                                    }
                                    // 96WTqSOTgxSBMYDMBsrs
                                    const newAthleteOrg = theAthlete.organizations[0]
                                    await getOrgDocRef(newAthleteOrg.organizationId).get()
                                        .then(async (orgSnapshot: firestore.DocumentSnapshot) => {
                                            if (orgSnapshot.exists) {
                                                const newOrg = { ...orgSnapshot.data(), uid: orgSnapshot.id } as Organization
                                                // const orgUID = newOrg.uid;
                                                // const newOrg = await getOrgDocFromSnapshot(orgUID)

                                                let isCoach = coachFlag ? coachFlag : false;

                                                const theEmail = theAthlete.email || theAthlete.profile.email
                                                const isCoachEmail = newOrg.coaches.find((email) => email.toLowerCase() === theEmail.toLowerCase())
                                                if (isCoachEmail && isCoachEmail.length !== 0) {
                                                    console.warn('isCoach !== isCoachEmail, setting to ${isCoachEmail}')
                                                    isCoach = true
                                                }

                                                // if (moveUserOrg) {
                                                //     newAthleteOrg.active = false;
                                                //     theAthlete.organizations.unshift({
                                                //         active: true,
                                                //         organizationId: currentOrg.uid,
                                                //         organizationName: currentOrg.name,
                                                //         organizationCode: currentOrg.organizationCode,
                                                //         joinDate: firestore.Timestamp.now()
                                                //     })
                                                // } else {
                                                const currOrg: OrganizationIndicator = theAthlete.organizations.find((org) => org.organizationId === newOrg.uid);

                                                currOrg.active = true;
                                                currOrg.organizationId = newOrg.uid;
                                                currOrg.organizationName = newOrg.name;
                                                currOrg.organizationCode = newOrg.organizationCode;
                                                // }


                                                const athlete: AthleteIndicator = {
                                                    uid: theAthlete.uid,
                                                    // TODO:
                                                    // organizations: moveUserOrg ? {organizationId: newOrg.uid} : theAthlete.organizations,
                                                    // organizations: theAthlete.organizations,
                                                    // organizationId: moveUserOrg ? orgUID || group.organizationId : newAthleteOrg.organizationId,
                                                    // organizationId: newAthleteOrg.organizationId,
                                                    // organizationName: moveUserOrg ? group.organizationName : currentAthleteOrg.organizationName,

                                                    firstName: theAthlete.profile.firstName,
                                                    lastName: theAthlete.profile.lastName,
                                                    email: theAthlete.profile.email || theAthlete.email,
                                                    profileImageURL: theAthlete.profile.imageUrl,

                                                    isCoach,

                                                    subscriptionType: theAthlete.profile.subscription.type,
                                                    includeGroupAggregation: !isCoach,
                                                    ipScore: theAthlete.currentIpScoreTracking.ipScore || 0,
                                                    runningTotalIpScore: theAthlete.runningTotalIpScore || 0,
                                                    isIpScoreDecrease: false,
                                                    isIpScoreIncrease: false,
                                                    orgTools: theAthlete.orgTools,
                                                    bio: theAthlete.profile.bio || '',
                                                    firebaseMessagingIds: theAthlete.firebaseMessagingIds || theAthlete.metadata.firebaseMessagingIds || [theAthlete.metadata.firebaseMessagingId || 'NOT-SET'],
                                                    creationTimestamp: theAthlete.metadata.creationTimestamp,
                                                    organizations: theAthlete.organizations
                                                };

                                                if (athlete.firebaseMessagingIds[0] === 'NOT-SET') {
                                                    // TODO: Sort Getting ID!!
                                                    console.error(`firebaseMessagingId NOT SET FOR USER ${athlete.firstName}, ${athlete.uid}`)

                                                }

                                                const allGroupAthletes = cloneDeep(group.athletes);
                                                const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

                                                if (allGroupAthletes.length !== groupAthletes.length) {
                                                    console.warn('dup athletes removed', {
                                                        groupAthletesWithDups: allGroupAthletes.map((a) => {
                                                            return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                                                        }),
                                                        groupAthletesCleaned: groupAthletes.map((a) => {
                                                            return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                                                        })
                                                    })
                                                }

                                                const currentAthlete = groupAthletes.find(ath => ath.uid === theAthlete.uid);
                                                if (!currentAthlete)
                                                    groupAthletes.push(athlete);


                                                await groupSnap.ref.update({ ...group, athletes: groupAthletes, organizationName: newOrg.name, sdsd: newOrg.organizationCode })
                                                    .then(() => {

                                                        // TODO: Check This
                                                        const onboardingCode = theAthlete.profile.onboardingCode
                                                        // if (moveUserOrg || onboardingCode.toUpperCase() === 'INSPIRE') {
                                                        //     onboardingCode = newOrg.organizationCode
                                                        // }

                                                        // if (onboardingCode.toUpperCase() === 'INSPIRE') {
                                                        // onboardingCode = `${newOrg.organizationCode}-${group.groupIdentifier}`
                                                        // }

                                                        const updatedAthlete: Athlete = {
                                                            ...theAthlete,
                                                            isCoach,
                                                            includeGroupAggregation: athlete.includeGroupAggregation,
                                                            // organizationId: athlete.organizationId,
                                                            organizations: theAthlete.organizations,
                                                            uid: athlete.uid,
                                                            profile: {
                                                                ...theAthlete.profile,
                                                                onboardingCode: onboardingCode
                                                            }
                                                        }

                                                        // TODO: Check orgID passed here

                                                        return updateAthlete({ ...group, athletes: groupAthletes, organizationName: group.organizationName || newOrg.name }, updatedAthlete, oldAthlete, atheleteRef, coachFlag, true, newAthleteOrg.organizationId)
                                                            .then(() => {
                                                                res.status(200).json({
                                                                    message: 'Athlete Group Updated',
                                                                    responseCode: 201,
                                                                    data: {
                                                                        group,
                                                                    },
                                                                    method: {
                                                                        name: 'UpdateAthlete',
                                                                        line: 58,
                                                                    },
                                                                })
                                                            })
                                                            .catch(() => {
                                                                res.status(500).json({ message: 'Unable to update athlete group details' })
                                                            })

                                                    });
                                                return 'Athlete Updated'

                                            } else {
                                                return 'Group Not Found'
                                            }

                                        })
                                        .then((writeResult: string) => {
                                            console.log(writeResult);
                                            return writeResult
                                        })
                                    // })

                                    //includeGroupAggregation: true


                                }

                            })

                    }
                }).catch((err) => {
                    return res.status(500).json({ error: err })
                })

        } else {
            return res.status(500).json({ message: 'No ID supplied' })
        }
    };


    moveAthleteOld = async (req: Request, res: Response) => {

        const { groupId, userId, coachFlag, moveUserOrg, orgUID, currentOrgName: orgName, organizationCode, forceGroupDelete } = <{
            groupId: string, userId: string, coachFlag?: boolean, moveUserOrg?: boolean,
            orgUID: string, currentOrgName: string, organizationCode: string, forceGroupDelete: boolean
        }>req.body;
        console.log(req.body);

        // const group = {
        //     athletes: [],
        //     creationTimestamp: new Date(),
        //     groupId: "8aRllwKnpOw01lvhB2j8-QLD1",
        //     groupIdentifier: "QLD1",
        //     groupName: "Diving Australia: Queensland",
        //     logoUrl: "https://s3-ap-southeast-2.amazonaws.com/piano.revolutionise.com.au/logos/yuojmbb9cnx1akfn.png",
        //     organizationId: "8aRllwKnpOw01lvhB2j8",
        //     uid: "8aRllwKnpOw01lvhB2j8-QLD1"
        // }

        let group = {} as Group;
        let pass = true;
        if (pass && groupId && userId) {
            return getGroupDocRef(groupId).get()
                .then((groupSnap) => {
                    if (groupSnap.exists) {

                        group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
                        // if (!group.athletes) {
                        //     group.athletes = []
                        // }
                        let oldAthlete: Athlete;
                        const atheleteRef = getAthleteDocRef(userId);// getAthleteDocRef('5FqVZGNUqVV9rPpo48CtqPB8xYE3').get()
                        return atheleteRef.get()
                            .then((athleteSnap) => {
                                if (athleteSnap.exists) {

                                    const theAthlete = { ...athleteSnap.data(), uid: athleteSnap.id } as Athlete;
                                    oldAthlete = cloneDeep(theAthlete);
                                    debugger;
                                    // TODO: Test All this
                                    if (!theAthlete.organizations
                                        // && !theAthlete.organizations.length 
                                        // || theAthlete.profile.onboardingCode.toUpperCase() === 'INSPIRE'
                                    ) {
                                        debugger;
                                        theAthlete.organizations = [{
                                            organizationId: theAthlete.organizationId || orgUID,
                                            organizationName: orgName || theAthlete.profile.firstName + "'s Org",
                                            organizationCode: theAthlete.profile.onboardingCode.split('-')[0] || organizationCode || 'UNKOWN',
                                            joinDate: theAthlete.metadata.creationTimestamp,
                                            active: true
                                        }]
                                    }

                                    if (theAthlete.profile.onboardingCode.toUpperCase() === 'INSPIRE'
                                    ) {
                                        debugger;
                                        theAthlete.organizationId = theAthlete.organizations[0].organizationId || orgUID || 'UNKNOWN'
                                        theAthlete.organizations[0].organizationCode = 'INSPIRE'
                                        theAthlete.organizations[0].active = false
                                    }

                                    // if ((!theAthlete.organizations) || (moveUserOrg && theAthlete.organizations.length)) {
                                    //     debugger;
                                    //     theAthlete.organizations = [{
                                    //         organizationId: group.organizationId,
                                    //         organizationName: group.organizationName,
                                    //         organizationCode: 'UNKOWN',
                                    //         active: true
                                    //     }]
                                    // } else {
                                    debugger;
                                    const currentAthleteOrg = theAthlete.organizations.find((o) => {
                                        return o.organizationId === theAthlete.organizationId;
                                    });
                            
                                    if (moveUserOrg || !theAthlete.organizations || theAthlete.organizations.find((org) => org.organizationId === group.organizationId) !== undefined) {
                                        debugger;
                                        if (!theAthlete.organizations) {
                                            debugger;
                                            theAthlete.organizations = [{
                                                organizationId: group.organizationId,
                                                organizationName: group.organizationName,
                                                organizationCode: 'UNKOWN',
                                                active: true,
                                                joinDate: firestore.Timestamp.now()
                                            }]
                                        } else {
                                            debugger;
                                            // if (moveUserOrg) {
                                            //     // TODO:  Current Org Movign From Needs To Be Passes In
                                            //     currentAthleteOrg = theAthlete.organizations.filter((o) => o.active).length ? theAthlete.organizations.filter((o) => o.active)[0] : theAthlete.organizations[0]

                                            //     theAthlete.organizations.map(org => org.active = false)
                                            // }
                                            // theAthlete.organizations.unshift({
                                            //     organizationId: group.organizationId,
                                            //     organizationName: group.organizationName,
                                            //     organizationCode: 'UNKOWN',
                                            //     active: true,
                                            //     joinDate: firestore.Timestamp.now()
                                            // })
                                        }
                                    }
                                    // }

                                    // if ((!theAthlete.organizationIds && theAthlete.organizationId) || (moveUserOrg && theAthlete.organizationId)) {
                                    //     theAthlete.organizationIds = [group.organizationId]
                                    // } else {
                                    //     if (!theAthlete.organizationIds || theAthlete.organizationIds.indexOf(group.organizationId) < 0) {
                                    //         if (moveUserOrg || !theAthlete.organizationIds) {
                                    //             theAthlete.organizationIds = [group.organizationId]
                                    //         } else {
                                    //             theAthlete.organizationIds.push(group.organizationId)
                                    //         }
                                    //     }
                                    // }

                                    if (!theAthlete.groups) {
                                        theAthlete.groups = []
                                    }

                                    try {
                                        if (!theAthlete.metadata.creationTimestamp && theAthlete.profile.onboardingCode.toUpperCase() === 'INSPIRE') {
                                            theAthlete.metadata.creationTimestamp = theAthlete.groups[0].creationTimestamp
                                        }
                                    } catch (error) {
                                        console.error('Athlete Creation Timestamp Undefinded, setting to now', firestore.Timestamp.now().toDate())
                                        theAthlete.metadata.creationTimestamp = firestore.Timestamp.now()
                                    }


                                    // theAthlete.groups.forEach(group => group.organizationId = theAthlete.organizationId)
                                    // TODO: Verify
                                    // return theAthlete.organizationIds.map((orgId: string) => {

                                    return getOrgDocRef(currentAthleteOrg.organizationId).get()
                                        .then(async (orgSnapshot: firestore.DocumentSnapshot) => {

                                            // // Remove athlete from groups if not a coach or swithing Organizations
                                            // if (!athlete.isCoach) {
                                            if (
                                                // !theAthlete.isCoach
                                                // moveUserOrg
                                                true
                                            ) {
                                                await removeAllAthleteIndicators(theAthlete, groupId, forceGroupDelete);
                                            }
                                            // }

                                            let org: Organization = undefined
                                            if (orgSnapshot.exists && !moveUserOrg) {

                                                org = { ...orgSnapshot.data(), uid: orgSnapshot.id } as Organization

                                            } else {
                                                console.log(`Org Not Found with UID = (orgUID) => ${currentAthleteOrg.organizationId} = (orgName) => ${currentAthleteOrg.organizationName}`)
                                                org = await getOrgDocRef(group.organizationId).get().then((docSnap) => { return getDocumentFromSnapshot(docSnap) as Organization })
                                                // return res.status(204).json({ message: `Org Not Found with UID = (orgUID) => ${currentAthleteOrgId}` })
                                            }

                                            const orgCode = org.organizationCode

                                            const isCoachEmailList = org.coaches.find((email) => email === theAthlete.profile.email)

                                            const isCoachEmail = isCoachEmailList && isCoachEmailList.length !== 0 ? true : false

                                            let isCoach = coachFlag ? coachFlag : false;
                                            if (isCoach !== isCoachEmail) {
                                                console.warn(`isCoach !== isCoachEmail, setting to ${isCoachEmail}`)
                                                isCoach = isCoachEmail
                                            }

                                            // const newOrg = await getOrgDocFromSnapshot(orgUID)

                                            if (moveUserOrg) {
                                                theAthlete.organizationId = org.uid;

                                                theAthlete.organizations.unshift({
                                                    active: true,
                                                    organizationId: org.uid,
                                                    organizationName: org.name,
                                                    organizationCode: org.organizationCode,
                                                    joinDate: firestore.Timestamp.now()
                                                })
                                            }
                                            const newAthleteIndicator: AthleteIndicator = {
                                                creationTimestamp: theAthlete.metadata.creationTimestamp,
                                                email: theAthlete.profile.email || theAthlete.email,
                                                firstName: theAthlete.profile.firstName,
                                                includeGroupAggregation: !isCoach,
                                                ipScore: theAthlete.currentIpScoreTracking.ipScore,
                                                runningTotalIpScore: theAthlete.runningTotalIpScore,
                                                // isCoach: coachFlag ? coachFlag : false,
                                                isCoach,
                                                isIpScoreDecrease: false,
                                                isIpScoreIncrease: false,
                                                orgTools: theAthlete.orgTools || null,
                                                bio: theAthlete.profile.bio || '',
                                                lastName: theAthlete.profile.lastName,
                                                organizations: theAthlete.organizations || [currentAthleteOrg],
                                                organizationId: moveUserOrg ? group.organizationId : currentAthleteOrg.organizationId,
                                                // organizationName: moveUserOrg ? group.organizationName : currentAthleteOrg.organizationName,
                                                // TODO:
                                                // organizationIds: [group.organizationId],
                                                profileImageURL: theAthlete.profile.imageUrl || '',

                                                firebaseMessagingIds: theAthlete.firebaseMessagingIds || theAthlete.metadata.firebaseMessagingIds || [theAthlete.metadata.firebaseMessagingId],
                                                subscriptionType: theAthlete.profile.subscription.type,
                                                uid: theAthlete.uid
                                            };

                                            // const currentAthletes = cloneDeep(group.athletes);
                                            // const currentAthlete = currentAthletes.find(ath => ath.uid === theAthlete.uid);
                                            // if (!currentAthlete)
                                            //     currentAthletes.push(athlete);
                                            if (!newAthleteIndicator.firebaseMessagingIds) {
                                                console.warn('firebaseMessagingIds not set for ' + newAthleteIndicator.firstName, newAthleteIndicator.uid)
                                                //   athleteIndicator.firebaseMessagingIds = firebaseMessagingIds || []
                                            }


                                            // const allGroupAthletes = group.athletes
                                            // const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

                                            // if (allGroupAthletes.length !== groupAthletes.length) {
                                            //     console.warn('dup athletes removed', {
                                            //         groupAthletesWithDups: allGroupAthletes.map((a) => {
                                            //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                                            //         }),
                                            //         groupAthletesCleaned: groupAthletes.map((a) => {
                                            //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                                            //         })
                                            //     })
                                            // }


                                            // let theAthleteIndicatorIndex = groupAthletes.findIndex((athInd) => athInd.uid === athInd.uid)


                                            // if (theAthleteIndicatorIndex > -1) {
                                            //     groupAthletes[theAthleteIndicatorIndex] = newAthleteIndicator
                                            // } else {
                                            //     groupAthletes.push(newAthleteIndicator)
                                            // }
                                            
                                            // return await groupSnap.ref.update({ ...group, athletes: groupAthletes })
                                            //     .then(() => {
                                            const exists = await doesAthleteExistInGroup(group.groupId, newAthleteIndicator.uid);

                                            if (!exists) {
                                                group.athleteIndex[newAthleteIndicator.uid]= true;
                                                await groupSnap.ref.update({ ...group });
                                            
                                                return await db.collection(FirestoreCollection.Groups)
                                                    .doc(group.groupId)
                                                    .collection('athletes')
                                                    .doc(newAthleteIndicator.uid)
                                                    .set(newAthleteIndicator)
                                                    .then((result) => {

                                                        let onboardingCode = theAthlete.profile.onboardingCode
                                                        if (moveUserOrg || onboardingCode.toUpperCase() === 'INSPIRE') {
                                                            onboardingCode = `${orgCode}-${group.uid.split('-')[1]}`
                                                        }

                                                        const updatedAthlete: Athlete = {
                                                            ...theAthlete,
                                                            isCoach,
                                                            includeGroupAggregation: newAthleteIndicator.includeGroupAggregation,
                                                            // organizationId: athleteIndicator.organizationId,
                                                            organizations: theAthlete.organizations,
                                                            uid: newAthleteIndicator.uid,
                                                            profile: {
                                                                ...theAthlete.profile,
                                                                onboardingCode: onboardingCode
                                                            }
                                                        }

                                                        const { isPrePaid: isOrgPrePaid, subscriptionType, prePaidNumberOfMonths: orgPrePaidNumberOfMonths } = org.onboardingConfiguration;


                                                        const prePaidGroup =
                                                            org.onboardingConfiguration &&
                                                            org.onboardingConfiguration.paidGroups &&
                                                            org.onboardingConfiguration.paidGroups.find((paidGroup) =>
                                                                paidGroup.groupName === `${org.organizationCode}-${group.groupIdentifier}`)

                                                        if (isOrgPrePaid || prePaidGroup || group.onboardingConfiguration && group.onboardingConfiguration.isPrePaid) {

                                                            if (prePaidGroup) {
                                                                const paidGroup = org.onboardingConfiguration.paidGroups.find((orgPaidGaidGroup) =>
                                                                    orgPaidGaidGroup.groupName === `${org.organizationCode}-${group.groupIdentifier}`)

                                                                const {
                                                                    prePaidNumberOfMonths: subscriptionLengthInMonths } = paidGroup

                                                                const nowMoment = moment();
                                                                const subscriptionEndDate = nowMoment.clone().add(subscriptionLengthInMonths, 'months');

                                                                updatedAthlete.profile.subscription.type = subscriptionType || 1

                                                                updatedAthlete.profile.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
                                                                updatedAthlete.profile.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());


                                                            } else {

                                                                const subscriptionLengthInMonths =
                                                                    group && group.onboardingConfiguration && group.onboardingConfiguration.prePaidNumberOfMonths ||
                                                                        prePaidGroup && prePaidGroup.prePaidNumberOfMonths ||
                                                                        isOrgPrePaid ? orgPrePaidNumberOfMonths || 1 : 0 ||
                                                                        0

                                                                const nowMoment = moment();
                                                                const subscriptionEndDate = nowMoment.clone().add(subscriptionLengthInMonths, 'months');

                                                                updatedAthlete.profile.subscription.type = 1 // subscriptionType || group.onboardingConfiguration.subscriptionType

                                                                updatedAthlete.profile.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
                                                                updatedAthlete.profile.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());
                                                            }

                                                        }

                                                        return updateAthlete(group, updatedAthlete, oldAthlete
                                                            , atheleteRef,
                                                            isCoach,
                                                            //  coachFlag, 
                                                            false, org.uid, org.name)
                                                            .then(() => {
                                                                return res.status(200).json({
                                                                    message: 'Athlete Group Updated',
                                                                    responseCode: 201,
                                                                    data: {
                                                                        group,
                                                                    },
                                                                    method: {
                                                                        name: 'UpdateAthlete',
                                                                        line: 58,
                                                                    },
                                                                })
                                                            })
                                                            .catch((err) => {
                                                                return res.status(500).json({ message: 'Unable to update athlete group details', err })
                                                            })

                                                    }).catch((e) => {
                                                        throw e
                                                    });
                                            } else {
                                                return res.status(200).json({
                                                    message: 'Athlete Already Exists In Group',
                                                    responseCode: 200,
                                                    data: {
                                                        group,
                                                    },
                                                    method: {
                                                        name: 'UpdateAthlete',
                                                        line: 58,
                                                    },
                                                });
                                            }
                                            // return 'Athlete Updated'

                                            // const currentGroup = { uid: orgSnapshot.id, ...orgSnapshot.data() } as Group
                                            // const clonedAthletes = cloneDeep(currentGroup.athletes)
                                            // const retainedAthletes = clonedAthletes.filter(currentAth => currentAth.uid !== theAthlete.uid)
                                            // return orgSnapshot.ref.update({ ...currentGroup, athletes: retainedAthletes }).then(() => {
                                            //     return 'Athlete removed from Group'
                                            // })


                                        });
                                    // .then((writeResult) => {
                                    //     console.log(writeResult);
                                    //     return writeResult
                                    // })
                                    // })

                                    //includeGroupAggregation: true


                                } else {
                                    return res.status(500).json({ error: `Can't find athlete with UID ${userId}` })
                                }

                            })

                    } else {
                        return res.status(503).json({
                            error: `Group Not Found with UID = (groupID) => ${groupId}`
                        })
                    }
                }).catch((err) => {
                    return res.status(500).json({ error: err })
                })

        } else {
            return res.status(500).json({ message: 'No ID supplied' })
        }
    };

    moveAthlete = async (req: Request, res: Response) => {

        const { groupId, userId, coachFlag, moveUserOrg, orgUID, currentOrgName: orgName, organizationCode, forceGroupDelete } = <{
            groupId: string, userId: string, coachFlag?: boolean, moveUserOrg?: boolean,
            orgUID: string, currentOrgName: string, organizationCode: string, forceGroupDelete: boolean
        }>req.body;
        console.log(req.body);

        let group = {} as Group;
        if (groupId && userId) {

            // Get the new group
            return getGroupDocRef(groupId).get()
                .then((groupSnap) => {
                    if (groupSnap.exists) {
                        group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
                        // Get the new org
                        return getOrgDocRef(group.organizationId).get()
                                .then((orgSnap) => {
                                    if (orgSnap.exists) {
                                        const newOrg = orgSnap.data() as OrganizationInterface;
                                        let oldAthlete: Athlete;
                                        let oldGroups: Array<GroupIndicator>;
                                        const atheleteRef = getAthleteDocRef(userId);// getAthleteDocRef('5FqVZGNUqVV9rPpo48CtqPB8xYE3').get()
                                        return atheleteRef.get()
                                                .then(async (athleteSnap) => {
                                                    if (athleteSnap.exists) {

                                                        const theAthlete = { ...athleteSnap.data(), uid: athleteSnap.id } as Athlete;
                                                        oldAthlete = cloneDeep(theAthlete);
                                                        oldGroups = oldAthlete.groups;

                                                        const exists = await doesAthleteExistInGroup(group.groupId, theAthlete.uid);

                                                        if (!exists) {
                                                            console.log(`Athlete ${theAthlete.uid} does not exist in group ${group.groupId}.  Proceeding with move operation.`);
                                                            if (oldGroups.length) {
                                                                await removeAllAthleteIndicators(theAthlete, oldGroups[0].groupId, false);
                                                            }

                                                            const oldOrgId = theAthlete.organizationId;
                                                            
                                                            if (!isArray(theAthlete.organizations)) {
                                                                theAthlete.organizations = [];
                                                                oldAthlete.organizations = [];
                                                            }

                                                            const appIdentifier = theAthlete.organizations.length ? theAthlete.organizations[0].appIdentifier : '';
                                                            theAthlete.organizations = theAthlete.organizations.filter((o: OrganizationIndicator) => {
                                                                return o.organizationId !== oldOrgId;
                                                            });

                                                            // Manage new org theme
                                                            const newThemeId = newOrg.theme || 'default';
                                                            const existingTheme = theAthlete.themes.find((t: WhiteLabelTheme) => {
                                                                return t.uid === newThemeId;
                                                            });

                                                            if (!existingTheme) {
                                                                const newTheme = await db.collection(FirestoreCollection.WhiteLabelThemes)
                                                                                         .doc(newThemeId)
                                                                                         .get()
                                                                                         .then((doc) => {
                                                                                             if (doc.exists) {
                                                                                                return doc.data() as WhiteLabelTheme;
                                                                                             } else {
                                                                                                 return undefined;
                                                                                             }
                                                                                         })
                                                                                         .catch((err) => {
                                                                                             console.error(`An error occurred when loading theme ${newThemeId}.  Error: ${err}`);
                                                                                             return undefined;
                                                                                         });
                                                                if (newTheme) {
                                                                    theAthlete.themes.unshift(newTheme);
                                                                }
                                                            } else {
                                                                const currentIndex = theAthlete.themes.indexOf(existingTheme);
                                                                if (currentIndex !== 0) {
                                                                    theAthlete.themes = this.arrayMove(theAthlete.themes, currentIndex, 0);
                                                                }
                                                            }

                                                            // Manage new orgTools
                                                            theAthlete.orgTools = newOrg.onboardingConfiguration.tools || [];

                                                            // Manage new group
                                                            const newGroupIndicator = {
                                                                groupId: group.groupId,
                                                                groupName: group.groupName,
                                                                organizationId: group.organizationId,
                                                                organizationName: group.organizationName,
                                                                creationTimestamp: group.creationTimestamp,
                                                                logoUrl: group.logoUrl
                                                            } as GroupIndicator;
                                                            theAthlete.groups = [newGroupIndicator];

                                                            // Manage new org
                                                            const newOrgIndicator = {
                                                                organizationId: newOrg.uid,
                                                                organizationName: newOrg.name,
                                                                appIdentifier,
                                                                themeId: newThemeId,
                                                                groups: [newGroupIndicator],
                                                                orgTools: newOrg.onboardingConfiguration.tools || []
                                                            } as OrganizationIndicator;
                                                            theAthlete.organizationId = newOrg.uid;
                                                            theAthlete.organizations.push(newOrgIndicator);

                                                            const isCoachEmailList = newOrg.coaches.find((email) => email === theAthlete.profile.email)
                                                            const isCoachEmail = isCoachEmailList && isCoachEmailList.length !== 0 ? true : false;
                                                            let isCoach = coachFlag ? coachFlag : false;
                                                            if (isCoach !== isCoachEmail) {
                                                                console.warn(`isCoach !== isCoachEmail, setting to ${isCoachEmail}`)
                                                                isCoach = isCoachEmail
                                                            }

                                                            const newAthleteIndicator: AthleteIndicator = {
                                                                creationTimestamp: theAthlete.metadata.creationTimestamp,
                                                                email: theAthlete.profile.email || theAthlete.email,
                                                                firstName: theAthlete.profile.firstName,
                                                                includeGroupAggregation: !isCoach,
                                                                ipScore: theAthlete.currentIpScoreTracking.ipScore,
                                                                runningTotalIpScore: theAthlete.runningTotalIpScore,
                                                                // isCoach: coachFlag ? coachFlag : false,
                                                                isCoach,
                                                                isIpScoreDecrease: false,
                                                                isIpScoreIncrease: false,
                                                                orgTools: theAthlete.orgTools || [],
                                                                bio: theAthlete.profile.bio || '',
                                                                lastName: theAthlete.profile.lastName,
                                                                organizations: theAthlete.organizations,
                                                                organizationId: theAthlete.organizationId,
                                                                profileImageURL: theAthlete.profile.imageUrl || '',
                
                                                                firebaseMessagingIds: theAthlete.firebaseMessagingIds || theAthlete.metadata.firebaseMessagingIds || [theAthlete.metadata.firebaseMessagingId],
                                                                subscriptionType: theAthlete.profile.subscription.type,
                                                                uid: theAthlete.uid
                                                            };
                
                                                            group.athleteIndex[newAthleteIndicator.uid]= true;
                                                            await groupSnap.ref.update({ ...group });

                                                            try {
                                                                await groupSnap.ref
                                                                    .collection(FirestoreCollection.Athletes)
                                                                    .doc(newAthleteIndicator.uid)
                                                                    .set(newAthleteIndicator)
                                                                    .then((result) => {
                                                                        console.log(result);
                                                                    })
                                                                    .catch((err) => {
                                                                        console.log(err);
                                                                    });
                                                            }
                                                            catch(e) {
                                                                console.error(e);
                                                            }

                                                            const orgCode = newOrg.organizationCode;
                                                            let onboardingCode = theAthlete.profile.onboardingCode
                                                            if (moveUserOrg || onboardingCode.toUpperCase() === 'INSPIRE') {
                                                                onboardingCode = `${orgCode}-${group.uid.split('-')[1]}`
                                                            }

                                                            const updatedAthlete: Athlete = {
                                                                ...theAthlete,
                                                                isCoach,
                                                                includeGroupAggregation: newAthleteIndicator.includeGroupAggregation,
                                                                // organizationId: athleteIndicator.organizationId,
                                                                organizations: theAthlete.organizations,
                                                                uid: newAthleteIndicator.uid,
                                                                profile: {
                                                                    ...theAthlete.profile,
                                                                    onboardingCode: onboardingCode
                                                                }
                                                            }

                                                            const { isPrePaid: isOrgPrePaid, subscriptionType, prePaidNumberOfMonths: orgPrePaidNumberOfMonths } = newOrg.onboardingConfiguration;
                                                            const prePaidGroup =
                                                                newOrg.onboardingConfiguration &&
                                                                newOrg.onboardingConfiguration.paidGroups &&
                                                                newOrg.onboardingConfiguration.paidGroups.find((paidGroup) =>
                                                                    paidGroup.groupName === `${newOrg.organizationCode}-${group.groupIdentifier}`)

                                                            if (isOrgPrePaid || prePaidGroup || group.onboardingConfiguration && group.onboardingConfiguration.isPrePaid) {

                                                                if (prePaidGroup) {
                                                                    const paidGroup = newOrg.onboardingConfiguration.paidGroups.find((orgPaidGaidGroup) =>
                                                                        orgPaidGaidGroup.groupName === `${newOrg.organizationCode}-${group.groupIdentifier}`)

                                                                    const {
                                                                        prePaidNumberOfMonths: subscriptionLengthInMonths } = paidGroup

                                                                    const nowMoment = moment();
                                                                    const subscriptionEndDate = nowMoment.clone().add(subscriptionLengthInMonths, 'months');

                                                                    updatedAthlete.profile.subscription.type = subscriptionType || 1

                                                                    updatedAthlete.profile.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
                                                                    updatedAthlete.profile.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());


                                                                } else {

                                                                    const subscriptionLengthInMonths =
                                                                        group && group.onboardingConfiguration && group.onboardingConfiguration.prePaidNumberOfMonths ||
                                                                            prePaidGroup && prePaidGroup.prePaidNumberOfMonths ||
                                                                            isOrgPrePaid ? orgPrePaidNumberOfMonths || 1 : 0 ||
                                                                            0

                                                                    const nowMoment = moment();
                                                                    const subscriptionEndDate = nowMoment.clone().add(subscriptionLengthInMonths, 'months');

                                                                    updatedAthlete.profile.subscription.type = 1 // subscriptionType || group.onboardingConfiguration.subscriptionType

                                                                    updatedAthlete.profile.subscription.commencementDate = firestore.Timestamp.fromDate(nowMoment.toDate());
                                                                    updatedAthlete.profile.subscription.expirationDate = firestore.Timestamp.fromDate(subscriptionEndDate.toDate());
                                                                }

                                                            }

                                                            return updateAthlete(group, updatedAthlete, oldAthlete, atheleteRef,
                                                                isCoach, false, newOrg.uid, newOrg.name)
                                                                .then(() => {
                                                                    return res.status(200).json({
                                                                        message: 'Athlete Group Updated',
                                                                        responseCode: 201,
                                                                        data: {
                                                                            group,
                                                                        },
                                                                        method: {
                                                                            name: 'UpdateAthlete',
                                                                            line: 58,
                                                                        },
                                                                    })
                                                                })
                                                                .catch((err) => {
                                                                    return res.status(500).json({ message: 'Unable to update athlete group details', err })
                                                                })

                                                                

                                                        } else {
                                                            return res.status(200).json({
                                                                message: 'Athlete Already Exists In Group',
                                                                responseCode: 200,
                                                                data: {
                                                                    group,
                                                                },
                                                                method: {
                                                                    name: 'UpdateAthlete',
                                                                    line: 58,
                                                                },
                                                            });
                                                        }
                                                    } else {
                                                        return res.status(500).json({ error: `Can't find athlete with UID ${userId}` })
                                                    }
                                                })
                                                .catch((err) => {
                                                    return res.status(500).json({ error: err });
                                                });
                                    } else {
                                        return res.status(503).json({
                                            error: `Organization Not Found with UID = (orgID) => ${group.organizationId}`
                                        });
                                    }
                                })
                                .catch((err) => {
                                    return res.status(500).json({ error: err });
                                });

                    } else {
                        return res.status(503).json({
                            error: `Group Not Found with UID = (groupID) => ${groupId}`
                        });
                    }
                })
                .catch((err) => {
                    return res.status(500).json({ error: err });
                });
        } else {
            return res.status(500).json({ message: 'No ID supplied' })
        }
    }

    moveAthleteToInspire = async (req: Request, res: Response) => {

        const { groupId, userId } =
            <{ groupId: string, userId: string, coachFlag?: boolean, moveUserOrg?: boolean }>req.body;
        console.log(req.body);

        // const group = {
        //     athletes: [],
        //     creationTimestamp: new Date(),
        //     groupId: "8aRllwKnpOw01lvhB2j8-QLD1",
        //     groupIdentifier: "QLD1",
        //     groupName: "Diving Australia: Queensland",
        //     logoUrl: "https://s3-ap-southeast-2.amazonaws.com/piano.revolutionise.com.au/logos/yuojmbb9cnx1akfn.png",
        //     organizationId: "8aRllwKnpOw01lvhB2j8",
        //     uid: "8aRllwKnpOw01lvhB2j8-QLD1"
        // }

        if (groupId && userId) {

            const athleteRef = getAthleteDocRef(userId)

            const athlete = await athleteRef
                .get().then(docSnap => getDocumentFromSnapshot(docSnap) as Athlete)
            if (athlete) {

                try {
                    if (!athlete.metadata.creationTimestamp && athlete.profile.onboardingCode.toUpperCase() === 'INSPIRE') {
                        athlete.metadata.creationTimestamp = athlete.groups[0].creationTimestamp
                    }
                } catch (error) {
                    console.error('Athlete Creation Timestamp Undefinded, setting to now', firestore.Timestamp.now().toDate())
                    athlete.metadata.creationTimestamp = firestore.Timestamp.now()
                }

                const groupRef = getGroupDocRef(groupId)

                const group = await groupRef
                    .get().then(docSnap => getDocumentFromSnapshot(docSnap) as Group)

                if (!group.athletes) {
                    group.athletes = []
                }

                let createInspireEntities = false
                if (!athlete.groups || athlete.groups.length <= 1) {
                    createInspireEntities = true
                }

                const athleteIndicatorOnGroup: AthleteIndicator = group.athletes.find(athIndicator => athIndicator.uid === userId)

                // const athleteIndicator: AthleteIndicator

                if (createInspireEntities) {
                    let results: OnboardingValidationResponse
                    if (athleteIndicatorOnGroup) {
                        const { organizationId, isCoach: legacyIsCoach, ...theRest } = athleteIndicatorOnGroup

                        results = await createEntities({ ...theRest, creationTimestamp: theRest.creationTimestamp.toDate().toJSON() as any }, 'INSPIRE')

                    } else {

                        const theAth = {
                            uid: athlete.uid,
                            email: athlete.profile.email,
                            firstName: getUserFirstName(athlete),
                            lastName: getUserLastName(athlete),
                            creationTimestamp: athlete.metadata.creationTimestamp.toDate().toJSON(),
                            profileImageURL: athlete.profile.imageUrl,
                            ipScore: athlete.ipScore,
                            runningTotalIpScore: athlete.runningTotalIpScore || 0,
                            isIpScoreIncrease: false,
                            isIpScoreDecrease: false,
                            organizations: athlete.organizations || [athlete.organizationId]

                        }

                        results = await createEntities(theAth as any, 'INSPIRE')
                    }

                    const { data } = results

                    await getGroupDocRef('lFW8A6ItriYr7fPH1jl3-G1').delete()
                    await getOrgDocRef('lFW8A6ItriYr7fPH1jl3').delete()


                    if (!athlete.organizations) {
                        debugger;
                        athlete.organizations = data.organizations

                        let theOrgName = group.organizationName
                        if (!theOrgName) {
                            const theCurrentOrg = await getOrgDocRef(group.organizationId).get().then((docSnap) => getDocumentFromSnapshot(docSnap) as Organization)
                            theOrgName = theCurrentOrg.name
                        }

                        athlete.organizations.push({
                            organizationId: group.organizationId,
                            organizationName: theOrgName,
                            organizationCode: athlete.profile.onboardingCode.split('-')[0],
                            active: false,
                            joinDate: firestore.Timestamp.now()
                        })
                    } else {
                        debugger;
                        await Promise.all(athlete.organizations.map(async (org) => {

                            const theOrg = await getOrgDocRef(org.organizationId).get().then((docSnap) => getDocumentFromSnapshot(docSnap) as Organization)

                            org.active = false
                            org.organizationCode = theOrg.organizationCode

                        }))
                        athlete.organizations.unshift(...data.organizations)
                    }

                    const currentAthleteOrg = athlete.organizations[0]

                    const orgCode = currentAthleteOrg.organizationCode
                    const onboardingCode = `${orgCode}-${data.groups[0].groupId.split('-')[1]}`
                    const newAthleteIndicator = data.athleteIndicator

                    await athleteRef.update({
                        isCoach: newAthleteIndicator.isCoach,
                        groups: data.groups,
                        organizations: athlete.organizations,
                        organizationId: currentAthleteOrg.organizationId,
                        profile: {
                            ...athlete.profile,
                            onboardingCode: onboardingCode,
                            subscription: {
                                ...athlete.profile.subscription,
                                isPrePaid: false,
                            }
                        },
                        runningTotalIpScore: newAthleteIndicator.runningTotalIpScore
                    })
                    // const currentAthletes = cloneDeep(group.athletes);
                    // const currentAthlete = currentAthletes.find(ath => ath.uid === athlete.uid);
                    // if (!currentAthlete)
                    //     currentAthletes.push(athlete);


                    // return await groupRef.update({ ...group, athletes: firestore.FieldValue.arrayUnion(athleteIndicator) })
                    //     .then((data) => {

                    //         let onboardingCode = athlete.profile.onboardingCode
                    //         if (moveUserOrg || onboardingCode.toUpperCase() === 'INSPIRE') {
                    //             onboardingCode = `${orgCode}-${group.uid.split('-')[1]}`
                    //         }

                    //         const updatedAthlete: Athlete = {
                    //             ...athlete,
                    //             isCoach,
                    //             includeGroupAggregation: athleteIndicator.includeGroupAggregation,
                    //             // organizationId: athleteIndicator.organizationId,
                    //             organizations: athlete.organizations,
                    //             uid: athleteIndicator.uid,
                    //             profile: {
                    //                 ...athlete.profile,
                    //                 onboardingCode: athlete.profile.onboardingCode
                    //             }
                    //         }

                    //         return this.updateAthlete(group, updatedAthlete
                    //             , athleteRef, coachFlag)
                    //             .then((writeResult) => {
                    //                 return res.status(200).json({
                    //                     message: 'Athlete Group Updated',
                    //                     responseCode: 201,
                    //                     data: {
                    //                         group,
                    //                     },
                    //                     method: {
                    //                         name: 'UpdateAthlete',
                    //                         line: 58,
                    //                     },
                    //                 })
                    //             })
                    //             .catch((err) => {
                    //                 return res.status(500).json({ message: 'Unable to update athlete group details', err })
                    //             })

                    //     });
                    // return 'Athlete Updated'

                    // const currentGroup = { uid: orgSnapshot.id, ...orgSnapshot.data() } as Group
                    // const clonedAthletes = cloneDeep(currentGroup.athletes)
                    // const retainedAthletes = clonedAthletes.filter(currentAth => currentAth.uid !== athlete.uid)
                    // return orgSnapshot.ref.update({ ...currentGroup, athletes: retainedAthletes }).then(() => {
                    //     return 'Athlete removed from Group'
                    // })



                    // .then((writeResult) => {
                    //     console.log(writeResult);
                    //     return writeResult
                    // })
                    // })

                } else {
                    await athleteRef.update({
                        groups: athlete.groups.filter((athGroup) => athGroup.groupId !== groupId)
                    })
                }


                if (athleteIndicatorOnGroup) {

                    await groupRef.update({
                        athletes: firestore.FieldValue.arrayRemove(athleteIndicatorOnGroup)
                    })
                    // await groupRef.update({
                    //     athletes: group.athletes.filter((athInd) => athInd.uid !== athlete.uid)
                    // })
                }
                return res.status(201).json({ message: 'athlete', athlete })


                //includeGroupAggregation: true

            } else {
                return res.status(503).json({
                    error: `Athlete Not Found with UID = (userID) => ${userId}`
                })
            }



        } else {
            return res.status(503).json({
                error: `Group Not Found with UID = (groupID) => ${groupId}`
            })
        }

    };
    /**
        * Removes an Athlete Indicator from specific group and Group indicator from Athlete
        *
        * @memberof GroupsCtrl
        */
    removeAthlete = async (req: Request, res: Response) => {

        const { groupId, userId } = <{ groupId: string, userId: string }>req.body;
        console.log(req.body);

        // const group = {
        //     athletes: [],
        //     creationTimestamp: new Date(),
        //     groupId: "8aRllwKnpOw01lvhB2j8-QLD1",
        //     groupIdentifier: "QLD1",
        //     groupName: "Diving Australia: Queensland",
        //     logoUrl: "https://s3-ap-southeast-2.amazonaws.com/piano.revolutionise.com.au/logos/yuojmbb9cnx1akfn.png",
        //     organizationId: "8aRllwKnpOw01lvhB2j8",
        //     uid: "8aRllwKnpOw01lvhB2j8-QLD1"
        // }

        let group = {} as Group;

        if (groupId && userId) {


            return await getGroupDocRef(groupId).get()
                .then(async (groupSnap) => {
                    if (groupSnap.exists) {

                        group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
                        if (!group.athletes) {
                            group.athletes = []
                        }
                        const atheleteRef = getAthleteDocRef(userId);// getAthleteDocRef('5FqVZGNUqVV9rPpo48CtqPB8xYE3').get()
                        return await atheleteRef.get()
                            .then(async (athleteSnap) => {

                                if (athleteSnap.exists) {

                                    const theAthlete = { ...athleteSnap.data(), uid: athleteSnap.id } as Athlete;


                                    return await removeAthleteIndicator(theAthlete, groupId)
                                        // groupSnap.ref.update({ ...group, athletes: currentAthletes })
                                        .then(async () => {


                                            // if (currentGrp.groupId !== group.uid) {
                                            const clonedGroups = cloneDeep(theAthlete.groups);
                                            const athleteGroups = getUniqueListBy(clonedGroups, 'groupId') as GroupIndicator[]

                                            const retainedGroups = athleteGroups.filter(currentGroup => currentGroup.uid ? currentGroup.uid : currentGroup.groupId !== groupId);
                                            const message = await atheleteRef.update({ groups: retainedGroups }).then(() => {
                                                return 'Group Indicator removed from Athlete'
                                            });

                                            return res.status(200).json({ message })

                                            // this.updateAthlete(group, { ...theAthlete, groups: theAthlete.groups }, atheleteRef, theAthlete.isCoach, false, group.organizationId)
                                            //     .then((theDdata) => {
                                            //         res.status(200).json({
                                            //             message: 'Athlete Group Updated',
                                            //             responseCode: 201,
                                            //             data: {
                                            //                 group,
                                            //             },
                                            //             method: {
                                            //                 name: 'UpdateAthlete',
                                            //                 line: 58,
                                            //             },
                                            //         })
                                            //     })

                                        })


                                    //includeGroupAggregation: true
                                } else {
                                    return res.status(501).json({ message: `Can't find Athlete with UID ${athleteSnap.id}` })
                                }

                            })
                            .catch(() => {
                                return res.status(505).json({ message: 'Unable to update athlete group details' })
                            });

                    } else {

                        return res.status(503).json({ message: `Can't find Group with UID ${groupId}` })
                    }

                }).catch((err) => {
                    return res.status(500).json({ error: err })
                })

        } else {
            return res.status(500).json({ message: 'No ID supplied' })
        }
    };


    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof GroupsCtrl
     */
    // deleteGroup = async (req: Request, res: Response) => {
    //     const { group } = <{ group: Group }>req.body
    //     console.log(req.body)

    //     // See the GroupRecord reference doc for the contents of groupRecord.
    //     console.log('...: ', group.uid)

    //     var citiesRef = db.collection(AggregateHistoryCollection.GroupMonthlyHistory);


    //     const date = new Date((<any>group).creationTimestamp)


    //     group.creationTimestamp = firestore.Timestamp.fromDate(date)

    //     const monthNumber = getMonthNumber(group.creationTimestamp, firestore.Timestamp.now())
    //     let promises = [];
    //     for (let monthNumber = 0; monthNumber <= monthNumber; monthNumber++) {
    //         var query = citiesRef

    //         for (let lifestyleEntryType = 0; lifestyleEntryType < 10; lifestyleEntryType++) {
    //             const promise = query.doc(`${group.uid}-${monthNumber}_${lifestyleEntryType}`).get().then((docsQuerySnapshot: FirebaseFirestore.DocumentSnapshot) => {

    //                 // const docs = docsQuerySnapshot.docs.map((doc) => ({ ...doc.data(), uid: doc.id }))


    //                 return { ...docsQuerySnapshot.data(), uid: docsQuerySnapshot.id }
    //             })
    //             promises.push(promise)
    //         }



    //     }
    //     return Promise.all(promises).then((docs) => [
    //         res.status(200).json(docs)
    //     ])
    //     // save profile
    //     return getGroupDocRef(group.uid)
    //         .set(group)
    //         .then(writeResult => {
    //             return res.status(200).json({
    //                 message: 'Group added',
    //                 responseCode: 201,
    //                 data: {
    //                     group,
    //                     result: writeResult,
    //                 },
    //                 error: undefined,
    //                 method: {
    //                     name: 'addGroup',
    //                     line: 58,
    //                 },
    //             })
    //         })
    //         .catch(() => {
    //             return res.status(502).json({
    //                 message: 'Group added',
    //                 responseCode: 201,
    //                 data: {
    //                     group,
    //                 },
    //                 error: undefined,
    //                 method: {
    //                     name: 'addGroup',
    //                     line: 58,
    //                 },
    //             })
    //         })
    // }
    createGroupHistoryDocuments = async (req: Request, res: Response) => {

        // const { group } = req.body
        // console.log(req.body)
        // console.log('...: ', group.uid)

        this.groupHistoryDocumentManager = new GroupHistoryDocumentManager();

        return this.groupHistoryDocumentManager.doRead(
            req.body.entry.groups[0],
            'api/add Entity History Docs',
            'info'
        )
            .then(data => {
                if (data) {
                    console.log(data);
                    return res.send(data)
                    // return Promise.resolve(data)
                } else {
                    LogInfo('empty data');
                    return res.send('empty data');
                    return Promise.resolve(null)
                }
            })
            .catch(err => {

                LogInfo('Eooro Occured!!!!', err);
                res.status(500).send(err)
                // return Promise.reject(err)
            })

    }

    removeGroup = async (req: Request, res: Response) => {

        const { groupId,
            // purgeAgrregates 
        } = <{ groupId: string, purgeAgrregates: boolean }>req.body;
        console.log(req.body);

        const group = {} as Group;

        if (groupId) {
            return getGroupDocRef(groupId).delete()
                .then((writeResult) => {
                    if (writeResult) {

                        return purgeAthleteAggregateDocuments(groupId).then((results) => {
                            if (!results.error) {
                                return res.status(200).json({
                                    message: 'Group Aggregate Documents Removed',
                                    responseCode: 201,
                                    data: {
                                        group,
                                    },
                                    method: {
                                        name: 'removeGroup',
                                        line: 58,
                                    },
                                })

                            } else {
                                return res.status(504).json({ message: 'Unable to delete group aggregate doc', error: results.error })

                            }
                        })
                    }
                    else {
                        return res.status(502).json({ message: 'Unable to delete group' })
                    }

                }).catch((err) => {
                    return res.status(500).json({ message: 'Error occured while trying to delete group', error: err })
                })

        } else {
            return res.status(500).json({ message: 'No ID supplied' })
        }
    };


}

// function removeAthleteIndicatorFromAllGroups(theAthlete: Athlete, orgId: string) {
//     theAthlete.groups.filter(gr => gr.organizationId ? gr.organizationId : gr.groupId.split('-')[0] === orgId).forEach((currentGroup) => {
//         db.collection(FirestoreCollection.Groups)
//             .doc(currentGroup.groupId).get()
//             .then((groupSnapshot: firestore.DocumentSnapshot) => {
//                 if (groupSnapshot.exists) {
//                     const currentGrp = { uid: groupSnapshot.id, ...groupSnapshot.data() } as Group;
//                     // if (currentGrp.groupId !== group.uid) {
//                     const clonedAthletes = cloneDeep(currentGrp.athletes);
//                     const retainedAthletes = clonedAthletes.filter(currentAth => currentAth.uid !== theAthlete.uid);
//                     if (retainedAthletes.length) {
//                         return groupSnapshot.ref.update({ ...currentGrp, athletes: retainedAthletes }).then(() => {
//                             return 'Athlete removed from Group';
//                         });
//                     }
//                     else {
//                         return db.collection('removedEntities').add({
//                             currentGrp
//                         }).then(() => {
//                             return 'Athlete removed from Group - Manual Cleanup Needed';
//                         });
//                         // TODO: Remove Org and Group Documents
//                         // groupSnapshot.ref.delete()
//                     }
//                     // } else {
//                     return 'Athlete not an any of the Organization Groups';
//                     // }
//                 }
//                 else {
//                     return 'Group Not Found';
//                 }
//             })
//             .then((writeResult) => {
//                 console.log(writeResult);
//             });
//     });
// }


async function removeAthleteIndicator(athlete: Athlete, groupId: string): Promise<string> {


    const groupSnapshot = await db.collection(FirestoreCollection.Groups)
        .doc(groupId).get();
    if (groupSnapshot.exists) {
        const currentGrp = ({ uid: groupSnapshot.id, ...groupSnapshot.data() } as Group);
        // if (currentGrp.groupId !== group.uid) {
        const clonedAthletes = cloneDeep(currentGrp.athletes);
        
        clonedAthletes.push(clonedAthletes[2])

        const groupAthletes = getUniqueListBy(clonedAthletes, 'uid') as AthleteIndicator[]

        const retainedAthletes = groupAthletes.filter(currentAth => currentAth.uid !== athlete.uid);
        return groupSnapshot.ref.update({ ...currentGrp, athletes: retainedAthletes }).then(() => {
            return 'Athlete Indicator removed from Group';
        });
        // } else {
        return 'Athlete not an any of the Organization Groups';
        // return groupSnapshot.ref.update({ ...currentGrp, athletes: retainedAthletes }).then(() => {
        //     return 'Athlete removed from Group'
        // })
        // }
    }
    else {
        return 'Group Not Found';
    }
    // .then((writeResult) => {
    //     console.log(writeResult);
    //     return 
    // });


}
