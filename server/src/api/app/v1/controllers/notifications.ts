import { Response, Request } from 'express'
// import { firestore } from 'firebase-admin';
import * as admin from 'firebase-admin'
import http from 'http';

import BaseCtrl from './base'
import {
    ScheduleNotificationPayload
} from '../../../../models/notifications/interfaces/schedule-notification-payload'
import { sendNotification } from './utils/send-notification'
import { db } from '../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
import moment from 'moment'

// const updateReceiptCollection = (athleteUid: string, validationResponse: TransactionReceiptValidationResponse): Promise<FirebaseFirestore.DocumentReference> => {
//     return getAthleteDocRef(athleteUid).collection(FirestoreCollection.NotificationTransactions).add(validationResponse)
// }




import { ioServer } from './helpers/socketHelper';
import { getData } from './helpers/requestHelper';
import { getSubscription } from './helpers/dbHelper';
// import { subscriptionConfiguration } from '../constants';
// const adalConfiguration = {
//     authority: 'https://login.microsoftonline.com/common',
//     clientID: 'ENTER_YOUR_CLIENT_ID',
//     clientSecret: 'ENTER_YOUR_SECRET',
//     redirectUri: 'http://localhost:3000/callback'
//   };

//   const subscriptionConfiguration = {
//     changeType: 'Created',
//     notificationUrl: 'https://NGROK_ID.ngrok.io/listen',
//     resource: 'me/mailFolders(\'Inbox\')/messages',
//     clientState: 'cLIENTsTATEfORvALIDATION'
//   };

const subscriptionConfiguration = {
    changeType: "created,updated,deleted",
    // notificationUrl: "https://webhook.azurewebsites.net/api/send/myNotifyClient",
    // notificationUrl: "https://us-central1-inspire-1540046634666.cloudfunctions.net/msCalendarSubscriptions",
    notificationUrl: "https://us-central1-inspire-1540046634666.cloudfunctions.net/api/app/v1/notifications/templates/msCalendarNotifications",
    // resource: "me/mailFolders('Inbox')/messages",
    // resource: "me/events",
    resource: "/me/events",
    // expirationDateTime: "2016-11-20T18:23:45.9356913Z",
    expirationDateTime: moment().add(1, 'week').toDate().toJSON(),
    clientState: "SecretClientState",
    // latestSupportedTlsVersion: "v1_2"
};


/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class NotificationCtrl
 * @extends {BaseCtrl}
 */
export default class NotificationsCtrl extends BaseCtrl {

    res: Response

    model = undefined
    collection = undefined

    constructor() {
        super()
    }

    /**
     * Sends a push notification to a device
     *      *
     * @memberof NotificationsCtrl
     */
    sendMessage = async (req: Request, res: Response) => {
        try {
            const { type, token, keyValuePairs } = <ScheduleNotificationPayload>req.body

            console.log('req.body', req.body)

            // const keyPar: KeyValuePair[] = [
            //     {
            //         key: '$NAME',
            //         value: 'Joe2',
            //     },
            // ]

            // return this.sendNotification(type, keyValuePairs ? keyValuePairs: keyPar , token, res)

            if (token) {
                return sendNotification(type, keyValuePairs, token).then(
                    ({ message, response }) => {
                        if (response) {
                            return res.send({
                                data: {
                                    message,
                                    response,
                                },
                            })
                        } else {
                            return res.status(500).send({
                                data: {
                                    message,
                                },
                            })
                        }
                    }
                )
            } else {
                console.error('Token is empty.... Message not sent')
                return res.status(500).json({
                    data: {
                        message: 'Token is empty.... Message not sent',
                    },
                })
            }
        } catch (error) {
            console.log('Error sending message:', error)
            return res.status(400).json(error)
        }
    }

    addTemplate = async (req: Request, res: Response) => {
        try {
            const { document: template, docUID } = req.body

            console.log('req.body', req.body)


            return db.collection(FirestoreCollection.NotificationTemplates).doc(docUID).set({ ...template, uid: docUID }).then((docRef) => {

                return res.json({ uid: docUID, ...template })
            })
                .catch((err) => {

                    return res.status(500).json(err)
                })

        } catch (error) {
            console.log('Error creating template:', error)
            return res.status(400).json(error)
        }
    }
    sendMessageSecure = async (req: Request, res: Response) => {
        // This registration token comes from the client FCM SDKs.
        // var registrationToken = 'YOUR_REGISTRATION_TOKEN'
        // const { registrationToken } = req.body

        console.log('req.body', req.body)

        // const message = {
        //     data: {
        //         score: '850',
        //         time: '2:45',
        //     },
        //     token: registrationToken,
        // }

        const tokenId = req.get('Authorization').split('Bearer ')[1]

        return admin
            .auth()
            .verifyIdToken(tokenId)
            .then(async decodedUser => {
                // Send a message to the device corresponding to the provided
                // registration token.
                try {
                    const { type, token, keyValuePairs } = <ScheduleNotificationPayload>req.body

                    return sendNotification(type, keyValuePairs, token)

                    // const response = await admin.messaging().send(message)
                    // // Response is a message ID string.
                    // console.log('Successfully sent message:', {
                    //     response,
                    //     decodedUser,
                    // })
                    // return res.json({ response, decodedUser })
                } catch (error) {
                    console.log('Error sending message:', error)
                    return res.status(500).json(error)
                }
            })
            .catch(err => res.status(401).send(err))
    }



    msCalendarNotifications = async (req: Request, res: Response, next) => {
        // This registration token comes from the client FCM SDKs.
        // var registrationToken = 'YOUR_REGISTRATION_TOKEN'
        // const { registrationToken } = req.body
        if (req.body) {
            console.log('The Body', req.body)
            console.log('The Body resourceData', JSON.stringify(req.body.value[0].resourceData))
        } else {
            console.warn('no Body')

        }

        let status;
        let clientStatesValid;

        // If there's a validationToken parameter in the query string,
        // then this is the request that Office 365 sends to check
        // that this is a valid endpoint.
        // Just send the validationToken back.
        if (req.query && req.query.validationToken) {
            console.log('req.query', {query: req.query})
            res.send(req.query.validationToken);
            // Send a status of 'Ok'
            status = 200;
        } else {
            clientStatesValid = false;

            // First, validate all the clientState values in array
            for (let i = 0; i < req.body.value.length; i++) {
                const clientStateValueExpected = subscriptionConfiguration.clientState;

                if (req.body.value[i].clientState !== clientStateValueExpected) {
                    // If just one clientState is invalid, we discard the whole batch
                    clientStatesValid = false;
                    break;
                } else {
                    clientStatesValid = true;
                }
            }

            // If all the clientStates are valid, then process the notification
            if (clientStatesValid) {
                for (let i = 0; i < req.body.value.length; i++) {
                    const resource = req.body.value[i].resource;
                    const subscriptionId = req.body.value[i].subscriptionId;

                    console.log({ resource, subscriptionId })
                    await processNotification(subscriptionId, resource, res, next);
                }
                // Send a status of 'Accepted'
                status = 202;
            } else {
                // Since the clientState field doesn't have the expected value,
                // this request might NOT come from Microsoft Graph.
                // However, you should still return the same status that you'd
                // return to Microsoft Graph to not alert possible impostors
                // that you have discovered them.
                status = 202;
            }
        }

        res.status(status).end(http.STATUS_CODES[status]);


        // return res.status(200).json({
        //     data: {
        //         message: 'Token is empty.... Message not sent',
        //     },
        // })
    }


}

async function processNotification(subscriptionId, resource, res, next) {

    const subscriptionData = await getSubscription(subscriptionId);


    if (subscriptionData) {


        getData(
            `/beta/${resource}`,
            subscriptionData.accessToken,
            (requestError, endpointData) => {
                if (endpointData) {
                    console.log('endpointData', { endpointData })
                    ioServer.to(subscriptionId).emit('notification_received', endpointData);
                    res.status(200);
                } else if (requestError) {
                    console.log('requestError', { requestError })
                    res.status(500);
                    next(requestError);
                }
            }
        );
    }

    else {
        console.log('subscriptionData not found in store')
        res.status(500);
        next('subscriptionData not found in store');


    }

    // else if (dbError) {
    //     res.status(500);
    //     next(dbError);
    // }
}