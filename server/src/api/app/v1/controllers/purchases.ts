import { Response, Request } from 'express'
import moment = require('moment')
// import { firestore } from 'firebase-admin';
const globalAny: any = global
globalAny.atob = require('atob')
// const iap = require('in-app-purchase')

import * as iap from 'in-app-purchase'
const packer = require('zip-stream')
const archive = new packer() // OR new packer(options)
archive.on('error', function (err: any) {
    throw err
})

import BaseCtrl from './base'
import { db } from '../../../../shared/init/initialise-firebase'

import {
    Purchase,
    TransactionReceiptValidationResponse,
} from '../../../../models/purchase/purchase.model'
import { TransactionReceiptValidationRequest } from '../../../../models/purchase/interfaces/transaction-receipt-validation-request'
import { getAndroidRecieptFromToken } from './getAndroidRecieptFromToken'

import { validateIOS } from '../callables/utils/validate-transaction-receipt/validate-ios';
import { initIOSPaymentValidationPlugin } from '../callables/utils/validate-transaction-receipt/init-ios-payment-validation-plugin';
import { validateAndroid } from '../callables/utils/validate-transaction-receipt/validate-android';
import { isProductionProject } from '../is-production'
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot'
import { sortByCreationTimestamp } from './utils/reports/sortByHelpers'

declare function atob(data: string): string;

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class PurchaseCtrl
 * @extends {BaseCtrl}
 */
export default class PurchasesCtrl extends BaseCtrl {
    res: Response

    // logger: Logger;
    model = Purchase
    collection = undefined

    constructor() {
        super()
    }

    addIOSReceiptValidationSecret = async (req: Request, res: Response) => {
        const {

            applePassword,
            packageName,

        } = <{ applePassword: string; packageName: string }>req.body


        console.log('validateReceipt - req.body', req.body)

        // db.collection('receiptTests').add({
        //     body: req.body,
        //     transactionReceipt: req.body.transactionReceipt,
        //     creationTimestamp: moment().toDate(),
        // }).catch((err) => console.log(err))


        const thePackageName = packageName
        console.log('thePackageName', thePackageName)


        try {
            const encryptedPassword = applePassword


            let saveResult

            const docRef = db.collection(FirestoreCollection.SystemConfiguration).doc(thePackageName);

            await docRef.get()
                .then(async (docSnap) => {
                    if (docSnap.exists) {
                        saveResult = await docRef.update({
                            applePassword: encryptedPassword
                        })
                            .then(async () => {
                                return true
                            }).catch((err) => {
                                console.error('Unable to Update config', err)
                                return false
                            })
                    } else {
                        saveResult = await docRef.set({ applePassword: encryptedPassword })
                            .then(async () => {
                                return true
                            }).catch((err) => {
                                console.error('Unable to Create config', err)
                                return false
                            })
                    }
                })


            return res.status(200).json(saveResult);

        } catch (error) {
            console.log(error)
            return res.status(500).send(error)
        }
    }

    validateReceipt = async (req: Request, res: Response) => {
        // const {
        //     athleteUid,
        //     platform,
        //     transactionReceipt,
        //     packageName,
        //     ...thePayload
        // } = <TransactionReceiptValidationRequest>req.body
        // console.log('validateReceipt - req.body', req.body)




        // const netballQ = await getGroupDocRef('bNei5o6gOiXQ7xQo4gma-G1').get().then((dna) => {

        //     const doc = dna.data()



        //     doc.athletes.forEach(async athInd => {



        //         const ath = await getAthleteDocFromUID(athInd.uid)

        //         if(ath && ath.metadata && ath.metadata.creationTimestamp){
        //             athInd.creationTimestamp = ath.metadata.creationTimestamp.toDate()
        //         }

        //     });


        //     doc.athletes.sort(sortByCreationTimestamp)

        //     console.log('NetballQ G1 athletes', doc.athletes.length)

        // })
        // const kfit = getGroupDocRef('FC2p4MVCkTT2EtHhE0n8-G1').get().then((dna) => {

        //     const doc = dna.data().athletes.length
        //     console.log('Kfir G1 athletes', doc)

        // })


        const {

            useReceiptFromDb,
            receiptID,
            saveReceipt = true

        } = <TransactionReceiptValidationRequest>req.body

        let theData = req.body

        if (useReceiptFromDb) {
            const theDataItem = await db.collection('receiptTests').doc(receiptID).get().then((docSnap) => getDocumentFromSnapshot(docSnap) as { uid: string; body: any })
            theData = theDataItem.body
        } else {
            saveReceipt && await db.collection('receiptTests').add({
                body: theData,
                transactionReceipt: theData.transactionReceipt,
                creationTimestamp: moment().toDate(),
            })
        }


        const {
            athleteUid,
            platform,
            transactionReceipt,
            packageName,
            ...thePayload
        } = <TransactionReceiptValidationRequest>theData



        const { productIdentifier } = <TransactionReceiptValidationRequest>theData
        console.log(' payload', { payload: theData })
        // db.collection('receiptTests').add({
        //     body: req.body,
        //     transactionReceipt: req.body.transactionReceipt,
        //     creationTimestamp: moment().toDate(),
        // }).catch((err) => console.log(err))
        const thePackageName = packageName ? packageName : isProductionProject() ? platform === 'Android' ? 'com.inspiresportonline.main' : 'com.inspiresportonline.ios' : 'com.inspiresportonline.dev';
        console.log('thePackageName', thePackageName)
        console.log('productIdentifier', productIdentifier)


        try {
            if (platform === 'Android') {
                if (!productIdentifier) {
                    console.warn({ productIdentifier })
                }
                // const packageName = 'com.inspiresportonline.main';
                return validateAndroid(theData, thePackageName, productIdentifier || '', athleteUid)
            } else {


                const applePasswordFromDB = await db.collection(FirestoreCollection.SystemConfiguration).doc(thePackageName)
                    .get().then(async (docSnap) => {

                        if (docSnap.exists) {
                            return {
                                uid: docSnap.id,
                                ...docSnap.data() as { applePassword: string }
                            }.applePassword;
                        } else {
                            console.warn(`Password not available for ${thePackageName}, defaulting to ${isProductionProject() ? platform === 'Android' ? 'com.inspiresportonline.main' : 'com.inspiresportonline.ios' : 'com.inspiresportonline.dev'}`)
                            const theConfig = await db.collection(FirestoreCollection.SystemConfiguration).doc(isProductionProject() ? platform === 'Android' ? 'com.inspiresportonline.main' : 'com.inspiresportonline.ios' : 'com.inspiresportonline.dev')
                                .get().then((settingsSnap) => {

                                    if (settingsSnap.exists) {
                                        return {
                                            uid: settingsSnap.id,
                                            ...settingsSnap.data() as { applePassword: string }
                                        };
                                    } else {
                                        return undefined
                                    }

                                })

                            return theConfig.applePassword
                        }

                    })

                const applePassword = theData.applePassword

                // const theAppelePassword = applePassword || decryptFunc(applePasswordFromDB)
                const theAppelePassword = applePassword || applePasswordFromDB

                console.log('applePasswordFromDB', { applePasswordFromDB })

                initIOSPaymentValidationPlugin(iap, theAppelePassword)

                try {
                    const result = await validateIOS(
                        transactionReceipt,
                        theData,
                        athleteUid,
                        thePayload
                    )

                    return res.status(result.responseCode).json(result);


                } catch (error) {
                    atob(transactionReceipt)

                    console.log(error)
                    return res.status(500).send(error)
                }
            }
        } catch (error) {
            return res.status(500).json(error)
        }
    }

    getReceipt = async (req: Request, res: Response) => {
        const {
            // athleteUid,
            platform,
            transactionReceipt,
            packageName,
            // ...theRest
        } = <TransactionReceiptValidationRequest>req.body
        console.log('validateReceipt - req.body', req.body)
        const applePassword = req.body.applePassword
        try {
            if (platform === 'Android') {
                // const { keys: theKeys } = req.body

                const keys: {
                    private_key: string
                    client_email: string
                } = {
                    private_key:
                        '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCkr9ZRjXhZImyJ\nExcfXVkFqNlnvfoSdfW4bTGYWDmcOad8iJvv7T3v1W14zTCQjNcKRibMeNBoDrYW\neekUsbfYOeA834ltUE/WE1w0BucRJbojZarwe0O6+1C5QrOL43Hio8Iq74PlFLKk\n9O5rOKXUHrVHwTFjS8/lY2aPUimSZYXtDBU/OBpM+2HzBeP82LacUnaecJbtlrym\nf1nifbG6nR/UWjHgMPf+lrR0Zn2a9lIUn02Acm3G/1yA+hD/u/3bSEX+TMQtmQOv\nzgvfb66LPfUVWe8q96vsATl0VWwvMagjfqh6LoGff1OjAL1uj4i5o6nPrqzEHDfa\n0kEosWBBAgMBAAECggEAC/knww6exxY4X1/msLZT9FQiGEeI4KI4XvP7ZNjXOWs8\nqNJyyecM93ykJKIAa6X9tWbDx95pwoL9TJWI82L3W45bpflXj73EzCrkq3isAIRm\na8/mtWy00CmY5Rs7HArJeyGOSppW08clNNaE5gE8lzczVVfvrAk1QHdxW66szJKc\nlD9jUF2QjHQBURy9cvEq4oO5gAbmcYsmLumjQnxEo0i6lmK/Tc3HJoypgRYHAtPD\nS7aFQyptpHur/hBg/Xiv7BTg0Mj4DiQW8Qaci/6A07ya4IK2M357Xq64dNZEezQO\neU7wf83WVHwr4883UQGt24YojcqHoUW/jHuDRwVkRQKBgQDcB+KsmSHDc99r2jgH\naf0VAg73UABtVnBEwwipAtPcFBNrKneXwLjfmlxoob/Q+jYiQ2Z+8EXfFcpJJOFC\nzHCPlTCBeTOwxf4dU8zbxtZsfmyHkj3B30DCzJGk4Ax9MSCy+ewtw/xoR6KdOhlr\nqPCKV2TMGVCXSLeWk9hv1X1rXQKBgQC/m9oQFAH0QaW7ZJIjx7u9V+CAuugckH8X\n/o1XZY1FhcxEnxFgG4QGe4dhIEXTF2vCndq+fRBEOuiRg3yCE3OLWl9Wt9sD5fce\njXVUi3daFJXdQo0dcsqtJ1Q5tXaZPoml2pVZxt+iBenh8Z85JxH9+7tc45amc1O3\nHmGQ0qZeNQKBgQCWHuMu84OvwN0MzuQvWscLkE35uqGv96u9nnvIJF+75g6hrWXP\nKfR4yu6FjOY8hJpuoiHKNdDWNh2/7eOrGaUqsZVYoQL9dvi7tbMtt+oQN+mATezI\n27NptP0hyqN6vwwaUJ4tU2xhEY8HSt6RL8B+AsaI4jS0Iy7vE4w2MSjTGQKBgHoJ\nfLjS1W/JxBH3ezC4zPVKnB3BbYaL7bbNlR49+t1122U1Xu60d8FdOht9X5uUBjld\nKu46X3rlfiz37vw2AViXRbPIxADWni9ib4FalrjT9aOH+LLx4u6n5vgegJwX/bmZ\n35ffl53tYEpdB0lyff4jL/F4rwHy4DX4brG7yOSlAoGBAIIPm2UQZPHTxazyJKVr\n1GwQQpiKuhRdR03VwqieEDr1bQi/tVzyhEZLp+Mb8NXfJNnIGRyA77iU9OUVAiN3\nKe4ewkquXavMEIlXUli5Jtljqz/3ldXHUxaMlnciGAopnBI2XoYZOyHUMfTkuROg\nUhKwZFKHM8UJ5hMxPu4NWbO2\n-----END PRIVATE KEY-----\n',
                    client_email:
                        'gooleplayapi@inspire-1540046634666.iam.gserviceaccount.com',
                }

                // const googleNotification = {
                //     version: '1.0',
                //     packageName: 'com.inspiresportonline.dev',
                //     eventTimeMillis: '1551041270264',
                //     subscriptionNotification: {
                //         version: '1.0',
                //         notificationType: 3,
                //         purchaseToken:
                //             'obmapelacmaallbipllnkgmn.AO-J1OwiR9UsGLoFrpcqki1GUYj3sqhp8sFEvF-eaHe8mGzG7sjtlsmzIFBu4IoNcBbCT5Db8oVtBtWqcmVtrgfRTLdgVwWGMS1nU0Zx6xe90RRR2jZjexPmO6NSRnbqb8GYiQcR_Vpz40Q3Bt2hFk4T1_UH945Q8K7bdCE7tvbd6u1EtCkLvOI',
                //         subscriptionId:
                //             'com.inspiresportonline.dev.subscriptions',
                //     },
                // }

                // const { subscriptionId: productId, purchaseToken } = googleNotification.subscriptionNotification

                // TODO:
                // const packageName = isProductionProject()
                //     ? 'com.inspiresportonline.dev'
                //     : 'com.inspiresportonline.dev'
                const {
                    productIdentifier: productId,
                    transactionReceipt: purchaseToken,
                } = <TransactionReceiptValidationRequest>req.body

                const receipt = {
                    packageName, // com.inspiresportonline.dev
                    productId, // com.inspiresportonline.dev.subscriptions
                    purchaseToken,
                }

                return getAndroidRecieptFromToken(keys, receipt).then(data => {
                    if (data.isSuccessful) {
                        console.log('data.isSuccessful')
                        // const message = 'validateReceipt'
                        // const theResponse = {
                        //     isValid: true,
                        //     message,
                        // }

                        const validatedData = {
                            platform: 'Android',
                            receipt: { transaction_id: data.payload.orderId },
                        }

                        const expirationDate = new Date(
                            data.payload.expiryTimeMillis
                        )

                        const responseData = {
                            ...data,
                            ...validatedData,
                            expirationDate,
                        }
                        return res.json({
                            responseCode: 201,
                            data: responseData,
                        })
                    } else {
                        const message = 'NOT!!!!!!!! data.isSuccessful'
                        console.log(message)
                        const responseData = {
                            ...data,
                            message,
                        }
                        return res.status(500).json({
                            responseCode: 501,
                            data: responseData,
                        })
                    }
                })
            } else {
                initIOSPaymentValidationPlugin(iap, applePassword)

                try {
                    return iap.setup(function (error: any) {
                        if (error) {
                            console.error('something went wrong...', error)
                            return res.status(510).json({
                                message: 'validateReceipt',
                                responseCode: 510,
                                // data: response,
                                error,
                                method: {
                                    name: 'validateReceipt',
                                    line: 42,
                                },
                            })
                        }

                        const theReceipt = transactionReceipt
                        // ? transactionReceipt
                        // : '34566'

                        // const decoded = atob(theReceipt)

                        return iap.validate(theReceipt, function (
                            err: { message: string },
                            validatedData: any
                            // {
                            //     message: string
                            //     receipt: { transaction_id: any }
                            // }
                        ) {
                            let response: TransactionReceiptValidationResponse = {
                                ...req.body,
                            }
                            if (err) {
                                // Failed to validate
                                console.error(err)
                                if (validatedData)
                                    console.error(
                                        'ERROR:Validating Receipt ======>>>>>> validateResponse',
                                        validatedData
                                    )
                                return res.status(500).json({
                                    message: 'validateReceipt',
                                    responseCode: 500,
                                    data: response,
                                    // error: undefined,
                                    method: {
                                        name: 'validateReceipt',
                                        line: 42,
                                    },
                                })
                            }
                            if (iap.isValidated(validatedData)) {
                                // validatedData: the actual content of the validated receipt
                                // validatedData also contains the original receipt
                                const options = {
                                    ignoreCanceled: true, // Apple ONLY (for now...): purchaseData will NOT contain cancceled items
                                    ignoreExpired: true, // purchaseData will NOT contain exipired subscription items
                                }
                                // validatedData contains sandbox: true/false for Apple and Amazon
                                const purchaseData = iap.getPurchaseData(
                                    validatedData,
                                    options
                                )
                                const {
                                    transactionReceipt: removed,
                                    ...rest
                                } = <any>response

                                response = {
                                    ...rest,
                                    expirationDate: new Date(
                                        +validatedData.receipt.expires_date
                                    ),
                                }

                                const theResponse = {
                                    ...response,
                                    validatedData,
                                    purchaseData,
                                }

                                return res.json({
                                    responseCode: 201,
                                    // data: { validatedData, purchaseData },
                                    data: theResponse,
                                })
                            } else {
                                response = {
                                    isValid: false,
                                    message: 'Receipt Not Valid',
                                }

                                return res.status(501).json({
                                    message: 'validateReceipt',
                                    responseCode: 501,
                                    data: response,
                                    // error: undefined,
                                    method: {
                                        name: 'validateReceipt',
                                        line: 42,
                                    },
                                })
                            }
                        })
                    })

                    // return tetst
                } catch (error) {
                    atob(transactionReceipt)

                    console.log(error)
                    return res.status(500).send(error)
                }
            }
        } catch (error) {
            return res.status(500).json(error)
        }
    }
}
