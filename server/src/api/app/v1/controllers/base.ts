import { Response, Request } from 'express';
import { firestore } from 'firebase-admin';
import { ResponseObject } from "../../../../shared/ResponseObject";
import { initialiseFirebase } from '../../../../shared/init/initialise-firebase'

/**
 * Base Class extended by the route controllers
 *
 * @abstract
 * @class BaseCtrl
 */
abstract class BaseCtrl {
  // /**
  //  *
  //  * @abstract
  //  * @type winston.Logger
  //  * @memberof BaseCtrl
  //  */
  // abstract logger: Logger;
  /**
   *
   * @abstract
   * @type Response
   * @memberof BaseCtrl
   */
  abstract res: Response;


  abstract model: any;
  abstract collection: any;

  db = initialiseFirebase('API').db;
  // abstract db: any;

  constructor() {
    // 
  }

  /**
  * Get all documents
  * @memberof BaseCtrl
  * @param req: Request
  * @param res: Response
  */
  getAll = (req: Request, res: Response) => {
    this.handleResponse(
      {
        message: 'Calling getAll From BaseCtrl',
        // res,
        responseCode: 200,
        method: {
          name: 'getAll',
          line: 38
        },
        res
      }
    );
  };


  getAllRecords = (req: Request, res: Response) => {


    return this.db.collection(this.collection)
      .get()
      .then((snapshot: firestore.QuerySnapshot) => {
        if (snapshot.docs) {
          return this.handleResponse({
            message: 'All ' + this.collection,
            responseCode: 200,
            data: snapshot.docs.map((doc) => {

              return {
                uid: doc.id,
                ...doc.data()
              }

            }),
            // error: { message: 'All Organizations' },
            method: {
              name: 'getAllRecords',
              line: 69
            },
            res
          });
        }
        else {
          return this.handleResponse({
            message: 'No docs found for ' + this.collection,
            responseCode: 404,
            data: undefined,
            error: { message: 'No docs found for ' + this.collection },
            method: {
              name: 'getAllRecords',
              line: 69
            },
            res
          });
        }
      })
      .catch((exception) => {
        console.log('exception!!!! => ', exception);
        return this.handleResponse({
          message: 'An error exception in /organizations err ->' + exception,
          responseCode: 500,
          data: undefined,
          error: {
            message: 'An exception occurred in /organizations err ->' + exception,
            exception
          },
          method: {
            name: 'getAllRecords',
            line: 256
          },
          res
        });
      });
  };

  /**
    * Adds a biometric entry to the database
    * Currently using so that I can test the onCreate events! ;P
    *
    * @memberof BaseCtrl
    */
  insertDocument = async (
    req: Request,
    res: Response
  ) => {
    // const { program } = <{ program: any }>req.body
    const { document } = <{ document: any }>req.body;
    console.log(req.body);
    const me = this;

    this.res = res;

    if (document) {
      return this.db
        .collection(this.collection)
        .add(document)
        .then(writeResult => {
          return me.handleResponse({
            message: 'Document added',
            responseCode: 201,
            data: {
              document,
              // writeResult,
            },
            error: undefined,
            method: {
              name: 'insertDocument',
              line: 66,
            },
            res: this.res
          })
        })
        .catch(err => {
          return res.status(502).json({
            message: 'Errror adding Program',
            responseCode: 201,
            data: {
              document,
            },
            error: err,
            method: {
              name: 'insertDocument',
              line: 66,
            },
          })
        })
    } else

      return res.status(500).json({ error: 'No Document Provided' })
  };

  /**
  * Count all documents
  * @memberof BaseCtrl
  * @param req: Request
  * @param res: Response
  */
  count = (req: Request, res: Response) => {
    this.handleResponse(
      {
        message: 'Calling count From BaseCtrl',
        // res,
        responseCode: 200,
        method: {
          name: 'count',
          line: 57
        },
        res
      }
    );
  };

  /**
   * Insert a document
   * @memberof BaseCtrl
   * @param req: Request
   * @param res: Response
   *  
   */
  insert = (req: Request, res: Response) => {
    const { document } = req.body;
    console.log(req.body);
    const me = this;

    this.res = res;

    if (document) {
      return this.db
        .collection(this.collection)
        .add(document)
        .then(writeResult => {
          return me.handleResponse({
            message: 'Document added',
            responseCode: 201,
            data: {
              document,
              // writeResult,
            },
            error: undefined,
            method: {
              name: 'insertDocument',
              line: 66,
            },
            res: this.res
          })
        })
        .catch(err => {
          return res.status(502).json({
            message: 'Errror adding Program',
            responseCode: 201,
            data: {
              document,
            },
            error: err,
            method: {
              name: 'insertDocument',
              line: 66,
            },
          })
        })
    } else

      return res.status(500).json({ error: 'No Document Provided' })
  };


  /**
  * Get document by id
  * @memberof BaseCtrl
  * @param req: Request
  * @param res: Response
  */
  get = (req: Request, res: Response) => {

    const { id } = req.params;

    if (!id)
      return this.handleResponse({
        message: 'No doc ID profided to query ' + this.collection,
        responseCode: 404,
        data: undefined,
        error: { message: 'No doc ID profided to query ' + this.collection },
        method: {
          name: 'get',
          line: 69
        },
        res
      });
    return this.db.collection(this.collection).doc(id)
      .get()
      .then((snapshot: firestore.DocumentSnapshot) => {
        if (snapshot.exists) {
          return this.handleResponse({
            message: 'The Qorganization found ',
            responseCode: 200,
            data: {
              uid: snapshot.id,
              ...snapshot.data()
            },
            // error: { message: 'All Organizations' },
            method: {
              name: 'get',
              line: 69
            },
            res
          });
        }
        else {
          return this.handleResponse({
            message: 'No docs found for ' + this.collection,
            responseCode: 404,
            data: undefined,
            error: { message: 'No docs found for ' + this.collection },
            method: {
              name: 'get',
              line: 69
            },
            res
          });
        }
      })
      .catch((exception) => {
        console.log('exception!!!! => ', exception);
        return this.handleResponse({
          message: 'An error exception in /organizations err ->' + exception,
          responseCode: 500,
          data: undefined,
          error: {
            message: 'An exception occurred in /organizations err ->' + exception,
            exception
          },
          method: {
            name: 'getAllRecords',
            line: 256
          },
          res
        });
      });
    this.handleResponse(
      {
        message: 'Calling get From BaseCtrl',
        // res,
        responseCode: 200,
        method: {
          name: 'get',
          line: 99
        },
        res
      }
    );
  };

  /**
   * Update document by Id
   *
   * @memberof BaseCtrl
   */
  update = (req: Request, res: Response) => {
    this.handleResponse(
      {
        message: 'Calling update From BaseCtrl',
        responseCode: 200,
        method: {
          name: 'update',
          line: 118
        },
        res
      }
    );
  };

  // - Delete by id
  delete = (req: Request, res: Response) => {
    this.handleResponse(
      {
        message: 'Calling delete From BaseCtrl',
        // res,
        responseCode: 200,
        method: {
          name: 'delete',
          line: 132
        },
        res
      }
    );
  };

  // - Response Handler
  handleResponse(responseObject: ResponseObject): any {

    //
    //
    // - ES7 Object Rest Operator to Omit Properties
    // - Here we pull all the properties before the spread out of the responseObject
    // - It’s essentially creating two new const variables and storing separate parts of our object in 
    // - and then the rest of the properties is left in the response constans that's also assigned
    //
    //

    const {
      message,
      method,
      responseCode,
      error,
      level,
      data,
      res,
      ...response } = responseObject;

    // !error

    // TODO: Sort Logger!!!!
    // // Remove error from response
    // ? this.logger.info(message, { error, ...response, method })
    // // Remove message from responseObject
    // : this.logger.error(message, { message, error, ...responseObject, method});

    // const parsedResponse = JSON.parse(JSON.stringify(response))


    // console.log('responseObject', responseObject)

    // const parsedObj = JSON.parse(JSON.stringify(responseObject))

    // if (!!error) console.info(message, { error, ...responseObject, method })
    // // Remove message from responseObject
    // else console.error(message, { message, error, ...responseObject, method });

    // // Remove error and data from response
    // if (error || error !== undefined) console.info(
    //   // 'console.info',
    //   message,
    //   {
    //     error,
    //     data,
    //     message,
    //     ...parsedResponse,
    //     method,
    //     level
    //   }
    // )
    // // Remove message from responseObject
    // else console.error(
    //   'else error',error, 'console.error',
    //   message, { message, error, data, ...parsedObj, method , level});

    // Remove error again from the response, and append the message


    const responseMessage = {
      ...response,
      message,
      data,
      // ...responseObject,
      method,
      level
    };

    console.log('api reponse', responseMessage);

    if (error)
      res.status(responseCode).json({ ...responseMessage, error });
    else
      res.status(responseCode || 201).json(responseMessage)

  }
}


export default BaseCtrl;
