import { ProgramCatalogue } from "../../../../db/programs/models/interfaces/program-catalogue";

export const SportsNutrition: ProgramCatalogue = {
    title: 'Sports Nutrition',
    categoryType: 1,
    images: [
        {
            'url': 'INSPIRE%2Fimages%2F08696912-1579-4aff-9e07-353212dd3104.png?alt=media'
        }
    ],
    // programs: [
    //     {
    //         programGuid: '71bad20f-8712-4bb7-986b-daab777ba80c',
    //         name: 'Nutrition Basics',
    //         publisher: 'iNSPIRE',
    //         description: '8 week module consisting of Carbohydrates, Protein, Fats, Vitamins and Minerals. This is the foundation iNSPIRE Sports Nutrition program and is a prerequisite for Nutrition Basics 2.',
    //         images: [
    //             {
    //                 url: 'INSPIRE%2Fimages%2F71bad20f-8712-4bb7-986b-daab777ba80c.png?alt=media'
    //             }
    //         ],
    //         activeDate: moment('2019-02-20').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 1,
    //         subscriptionType: 1,
    //         programDays: 56,
    //         tags: ['onboarding']
    //     },
    //     {
    //         programGuid: 'c0a5c2ff-cb2b-4c49-9e1b-bddbec542292',
    //         name: 'Nutrition Basics 2',
    //         publisher: 'iNSPIRE',
    //         description: '4 week module consisting of the main Food Groups. This program is a prerequisite for Nutrition Basics 3.',
    //         images: [
    //             {
    //                 url: 'INSPIRE%2Fimages%2Fc0a5c2ff-cb2b-4c49-9e1b-bddbec542292.png?alt=media'
    //             }
    //         ],
    //         activeDate: moment('2019-03-30').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 1,
    //         subscriptionType: 1,
    //         programDays: 28,
    //         tags: ['nutrition']
    //     },
    //     {
    //         programGuid: '1dbcd72a-8898-4b9c-bfe4-124fc5e16bbb',
    //         name: 'Nutrition Basics 3',
    //         publisher: 'iNSPIRE',
    //         description: '4 week module consisting of Hydration and minor Food Groups. ',
    //         images: [
    //             {
    //                 url: 'INSPIRE%2Fimages%2F1dbcd72a-8898-4b9c-bfe4-124fc5e16bbb.png?alt=media'
    //             }
    //         ],
    //         activeDate: moment('2019-05-30').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 1,
    //         subscriptionType: 1,
    //         programDays: 28,
            
    //         tags: ['nutrition']
    //     }
    // ]
};
