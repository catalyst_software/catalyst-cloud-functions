import { ProgramCatalogue } from '../../../../db/programs/models/interfaces/program-catalogue'

export const General: ProgramCatalogue = {
    title: 'General',
    categoryType: 0,
    images: [
        {
            url:
                'INSPIRE%2Fimages%2F08696912-1579-4aff-9e07-353212dd3104.png?alt=media',
        },
    ],
    // programs: [
    //     {
    //         programGuid: '08696912-1579-4aff-9e07-353212dd3104',
    //         name: 'iNSPIRE Onboarding Program',
    //         publisher: 'iNSPIRE',
    //         description:
    //             'Welcome to iNSPIRE, this is your onboarding program that will run for the next 4 weeks! After you complete this program you will have the choice of 4 programs to continue your iNSPIRE journey.',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2F08696912-1579-4aff-9e07-353212dd3104.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-01-01').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 0,
    //         subscriptionType: 0,
    //         programDays: 28,

    //         tags: ['onboarding'],
    //     },
    //     {
    //         programGuid: '61128366-33b5-4dfa-91a8-820bfcf03934',
    //         name: 'Managing Life as an Athlete',
    //         publisher: 'iNSPIRE',
    //         description: '',
    //         images: [
    //             {
    //                 url:
    //                     'INSPIRE%2Fimages%2F61128366-33b5-4dfa-91a8-820bfcf03934.png?alt=media',
    //             },
    //         ],
    //         activeDate: moment('2019-04-01').toDate(),
    //         callToActionText: 'Kick it off!',
    //         categoryType: 0,
    //         subscriptionType: 1,
    //         programDays: 28,
    //         tags: ['onboarding'],
    //     },
    // ],
}
