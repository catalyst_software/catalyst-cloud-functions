import { Program } from './../../../../db/programs/models/program.model';
// import firestore = require('@google-cloud/firestore')
import { firestore } from 'firebase-admin';
import * as functions from 'firebase-functions'
// import * as sgMail from '@sendgrid/mail'

// const sgMail = require('@sendgrid/mail');
import sgMail from '@sendgrid/mail';
import rp from 'request-promise'
// var rp = require('request-promise');
import BaseCtrl from './base'

import { Response, Request } from 'express'


import { createDataset } from './../callables/utils/big-query/create-dataset';
import { getDataset } from './../callables/utils/big-query/list-datasets';

import { AthleteIndicator } from '../../../../db/biometricEntries/well/interfaces/athlete-indicator';

import { AthleteHistoryDocumentManager } from '../../../../db/biometricHistory/athletes/managers/history-document-manager'

import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections'

import { LogInfo } from '../../../../shared/logger/logger'

import { Athlete } from '../../../../models/athlete/athlete.model'
import { Group } from '../../../../models/group/interfaces/group'
import { WellAggregateCollection } from '../../../../db/biometricEntries/enums/firestore-aggregate-collection'

import { WellAggregateHistoryCollection } from '../../../../db/biometricEntries/enums/firestore-aggregate-history-collection'
import { getUserByID } from "./get-user-by-id";
import { getUserByEmail } from "./get-user-by-email";
// import { updateAthleteIndicatorsIPScoreOnGroups } from '../../../../analytics/triggers/update-athlete-indicators-ipscore-on-groups';
import { updateMessagingIDInConfig } from '../callables/utils/comms/update-messaging-id-in-config';
import { setDefaulCommsConfig } from './utils/comms/set-default-comms-config';
import { updateAthleteIndicatorsIPScoreOnGroups } from '../../../../analytics/triggers/update-athlete-indicators-ipscore';
import { getGroupDocRef, getAthleteDocRef, getCollectionRef, getAthleteCollectionRef, getOrgDocRef, getDocFromRef, getGroupDocFromSnapshot, getOrgDocFromSnapshot } from '../../../../db/refs';
import { setAthletesOrgID } from './utils/set-athletes-orgID';
import { getAthleteDocFromUID } from './../../../../db/refs/index';
import { getDocumentFromSnapshot } from '../../../../analytics/triggers/utils/get-document-from-snapshot';
import { groupByMap } from "../../../../shared/utils/groupByMap";
import { isArray, cloneDeep, isString } from 'lodash';
import { removeUndefinedProps } from '../../../../db/ipscore/services/save-aggregates';
import { chunk } from './utils/chunk';
import { getGroupIndicator } from '../callables/utils/onboarding/link-athlete-to-group';
import { Organization } from '../../../../models/organization.model';
import { db, theMainApp } from '../../../../shared/init/initialise-firebase';
import { cleanUpAthleteRelatedDocuments } from './utils/purgeAggregateDocuments';
import moment from 'moment';
import { createAthleteIndicator } from '../callables/utils/onboarding/create-athlete-indicator';
// import { chunkBy } from '../callables/utils/onboarding/get-dynamic-group-id';

// import { getDataset } from '../callables/utils/big-query/list-datasets';
// import { createDataset } from '../callables/utils/big-query/create-dataset';


// Object.defineProperty(Array.prototype, 'chunk', {
//     value: function (chunkSize) {
//         var R = [];
//         for (var i = 0; i < this.length; i += chunkSize)
//             R.push(this.slice(i, i + chunkSize));
//         return R;
//     }
// });


export const createNewGroupDoc = (groupId: string, orgUID: string, groupIdentifier: string, org: Organization, athletes: AthleteIndicator[]) => {

    const newGroup: Group = {
        uid: groupId,
        groupId: `${orgUID}-${groupIdentifier}`,
        groupName: `${org.organizationCode}-${groupIdentifier}`,
        organizationId: orgUID,
        organizationName: org.name,
        groupIdentifier,
        logoUrl: '',
        creationTimestamp: firestore.Timestamp.now(),
        athletes,
    };

    return newGroup
}

export const testForLetter = (character) => {
    try {
        //Variable declarations can't start with digits or operators
        //If no error is thrown check for dollar or underscore. Those are the only nonletter characters that are allowed as identifiers
        eval("let " + character + ";");
        const regExSpecial = /[^\$_]/;
        return regExSpecial.test(character);
    } catch (error) {
        return false;
    }
}

export interface NotificationConfig {
    sendEmail: boolean;
    sendPushNotification: boolean;
    sendSMS: boolean;
}

export interface GroupNotificationConfig {
    groupId: string;
    // transmitRedFlagEmails: boolean;
    // transmitRedFlagNotifications: boolean;
    // transmitSMSotifications: boolean;
    danger: NotificationConfig
    warn: NotificationConfig;
    success: NotificationConfig;
    athletes?: Array<AthleteNotificationConfig>;
}
export interface AthleteNotificationConfig {

    uid: string;
    isCoach?: boolean;
    // transmitRedFlagEmails: boolean; transmitRedFlagNotifications: boolean; transmitSMSotifications: boolean;
    danger: NotificationConfig;
    warn: NotificationConfig;
    success: NotificationConfig;
}

export interface NotificationsConfig {
    uid: string;
    firebaseMessagingIDs: Array<string>;
    groups: Array<GroupNotificationConfig>;
    excludedNotificationTypes: number[];
}


// groupEssentialCommsConfiguration

/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class AthleteCtrl
 * @extends {BaseCtrl}
 */
export default class AthletesCtrl extends BaseCtrl {
    res: Response

    // logger: Logger;
    model = Athlete

    collection = FirestoreCollection.Athletes

    athleteHistoryDocumentManager: AthleteHistoryDocumentManager

    constructor() {
        super()
    }



    // private getAthleteIndicators(allOrgAthletes: Athlete[], org: Organization, orgId: any): AthleteIndicator[] {
    //     return cloneDeep(allOrgAthletes.map((ath) => {
    //         const athleteIndicator: AthleteIndicator = {
    //             uid: ath.uid,
    //             firstName: ath.profile.firstName,
    //             lastName: ath.profile.lastName,
    //             creationTimestamp: ath.metadata.creationTimestamp,
    //             isIpScoreIncrease: false,
    //             ipScore: ath.currentIpScoreTracking.ipScore,
    //             isIpScoreDecrease: false,
    //             email: ath.profile.email,
    //             profileImageURL: ath.profileImageURL || ath.profile.imageUrl || '',
    //             bio: ath.profile.bio || ath.profile.sport || 'Athlete',
    //             runningTotalIpScore: ath.runningTotalIpScore,
    //             subscriptionType: ath.profile.subscription.type,
    //             orgTools: org.onboardingConfiguration.tools || ath.orgTools || [],
    //             sport: ath.profile.sport || '',
    //             includeGroupAggregation: false,
    //             organizationId: orgId,
    //             isCoach: false,
    //         };
    //         // this.setBio(athleteIndicator);
    //         this.setLastnameToFirstCharacter(ath, athleteIndicator);
    //         return athleteIndicator;
    //     }));
    // }




    fixAthleteGroups = async (req: Request, res: Response) => {

        // TODO: Check case of parameters


        // const theGroupIds = isArray(groupIds)
        //     ? () => {
        //         console.log('isArray(groupIds) => ', isArray(groupIds));
        //         return groupIds
        //     }
        //     : () => {
        //         console.log('isArray(groupIds) => ', isArray(groupIds));
        //         return JSON.parse(groupIds) as Array<string>
        //     }

        // const updateResults = await updateMessagingIDInConfig(athleteUID, theGroupIds(), +ipScore);
        const { athleteUID, organizationId = 'bgRE62Gjr8vWfNDizBye', groupId = 'bgRE62Gjr8vWfNDizBye-ONLINE4', firstName, lastName, notificationIds }
            = <{
                athleteUID: string,
                organizationId: string,
                groupId: string,
                firstName: string,
                lastName: string,
                notificationIds: Array<string>
            }>req.body

        console.log('firstName ', firstName)
        // return res.status(500).json({
        //     message: `Unable to updateMessagingIDInConfig ${athleteUID}`,
        //     // err
        // })
        const groupAthletes = []
        const allAthPromises = db.collection(FirestoreCollection.Athletes).where('organizationId', '==', organizationId).get()
            .then((athDocQuerySnap) => {
                return athDocQuerySnap.docs.map((docSnap) => getDocumentFromSnapshot(docSnap) as Athlete)
            })

        const allAths = await allAthPromises
        const org = await getOrgDocFromSnapshot(organizationId)

        allAths.filter((a) => a.groups.find((g) => g.groupId === groupId)).forEach((ath) => {


            const athIndicator = this.getAthleteIndicators([ath], org, org.uid)[0]

            if (athIndicator.lastName !== 'Gouws') {
                groupAthletes.push(athIndicator)
            }

        })

        const groupRef = getGroupDocRef(groupId)


        return groupRef.update({
            athletes: groupAthletes
        })
        const updateResults = await updateMessagingIDInConfig(athleteUID, organizationId, firstName, lastName, notificationIds).catch((err) => {
            return res.status(500).json({
                message: `Unable to updateMessagingIDInConfig ${athleteUID}`,
                err
            })
        })

        if (updateResults.length) {

            // const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName

            return res.status(202).json({
                message: `IPScore Group Indicators update results for ${firstName} ${lastName}`,
                updateResults
            })

        }
        else {
            console.warn(`No ipscore GroupIndicator update results ${athleteUID}`);

            return res.status(204).json({
                message: `No ipscore GroupIndicator update results ${athleteUID}`
            })
        }
    }

    updateMessagingIDInConfig = async (req: Request, res: Response) => {

        // TODO: Check case of parameters


        // const theGroupIds = isArray(groupIds)
        //     ? () => {
        //         console.log('isArray(groupIds) => ', isArray(groupIds));
        //         return groupIds
        //     }
        //     : () => {
        //         console.log('isArray(groupIds) => ', isArray(groupIds));
        //         return JSON.parse(groupIds) as Array<string>
        //     }

        // const updateResults = await updateMessagingIDInConfig(athleteUID, theGroupIds(), +ipScore);
        const { athleteUID, organizationId, firstName, lastName, notificationIds }
            = <{
                athleteUID: string,
                organizationId: string,
                firstName: string,
                lastName: string,
                notificationIds: Array<string>
            }>req.body

        console.log('firstName ', firstName)
        // return res.status(500).json({
        //     message: `Unable to updateMessagingIDInConfig ${athleteUID}`,
        //     // err
        // })
        const updateResults = await updateMessagingIDInConfig(athleteUID, organizationId, firstName, lastName, notificationIds).catch((err) => {
            return res.status(500).json({
                message: `Unable to updateMessagingIDInConfig ${athleteUID}`,
                err
            })
        })

        if (updateResults.length) {

            // const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName

            return res.status(202).json({
                message: `IPScore Group Indicators update results for ${firstName} ${lastName}`,
                updateResults
            })

        }
        else {
            console.warn(`No ipscore GroupIndicator update results ${athleteUID}`);

            return res.status(204).json({
                message: `No ipscore GroupIndicator update results ${athleteUID}`
            })
        }
    }

    cloneAthlete = async (req: Request, res: Response) => {

        const { fromUID, toUID }
            = <{
                fromUID: string,
                toUID: string
            }>req.body


        const athleteToClone = await getAthleteDocFromUID(fromUID)

        return await getAthleteDocRef(toUID).set(athleteToClone).then(() => res.status(202).json({
            message: `Athlete Cloned`,
            fromUID,
            toUID
        }))
    }

    setOrgCode = async (req: Request, res: Response) => {

        // TODO: Check case of parameters


        // const theGroupIds = isArray(groupIds)
        //     ? () => {
        //         console.log('isArray(groupIds) => ', isArray(groupIds));
        //         return groupIds
        //     }
        //     : () => {
        //         console.log('isArray(groupIds) => ', isArray(groupIds));
        //         return JSON.parse(groupIds) as Array<string>
        //     }

        // const updateResults = await updateMessagingIDInConfig(athleteUID, theGroupIds(), +ipScore);
        // const { athleteUID, organizationId, firstName, lastName, notificationIds }
        //     = <{
        //         athleteUID: string,
        //         organizationId: string,
        //         firstName: string,
        //         lastName: string,
        //         notificationIds: Array<string>
        //     }>req.body

        // console.log('firstName ', firstName)
        // return res.status(500).json({
        //     message: `Unable to updateMessagingIDInConfig ${athleteUID}`,
        //     // err
        // })
        // const updateResults = await updateMessagingIDInConfig(athleteUID, organizationId, firstName, lastName, notificationIds).catch((err) => {
        //     return res.status(500).json({
        //         message: `Unable to updateMessagingIDInConfig ${athleteUID}`,
        //         err
        //     })
        // })

        const updateResults = await setAthletesOrgID()
        // .catch((err) => {
        //     return res.status(500).json({
        //         message: `Unable to updateMessagingIDInConfig ${'athleteUID'}`,
        //         err
        //     })
        // })

        if (updateResults.length) {

            // const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName

            return res.status(202).json({
                message: `Athletes Updates`,
                updateResults
            })

        }
        else {
            console.warn(`No Athletes Found To Update`);

            return res.status(204).json({
                message: `No Athletes Found To Update`
            })
        }
    }

    updateAthleteIndicatorIpscore = async (req: Request, res: Response) => {

        // TODO: Check case of parameters
        const {
            uid: athleteUID,
            ipScore, runningTotalIpScore, groupIds, utcOffset }
            = <{ uid: string, ipScore: string, runningTotalIpScore: string, groupIds: string | Array<string>, utcOffset?: number }>req.body


        const theGroupIds = isArray(groupIds)
            ? () => {
                console.log('isArray(groupIds) => ', isArray(groupIds));
                return groupIds
            }
            : () => {
                console.log('isArray(groupIds) => ', isArray(groupIds));
                return JSON.parse(groupIds) as Array<string>
            }

        const updateResults = await updateAthleteIndicatorsIPScoreOnGroups(athleteUID, theGroupIds(), +runningTotalIpScore, +ipScore, utcOffset || 0);

        if (updateResults.length) {

            const name = (updateResults[0].athlete as AthleteIndicator).firstName + ' ' + (updateResults[0].athlete as AthleteIndicator).lastName

            return res.status(202).json({
                message: `IPScore Group Indicators update results for ${name}`,
                updateResults
            })

        }
        else {
            console.warn(`No ipscore GroupIndicator update results ${athleteUID}`);

            return res.status(204).json({
                message: `No ipscore GroupIndicator update results ${athleteUID}`
            })
        }
    }

    removeStalePrograms = async (req: Request, res: Response) => {

        const allAthletes = await getAthleteCollectionRef()
            .get().then((qurySnap) => {

                console.log(`${qurySnap.docs.length} docs found`)
                return qurySnap.docs.map((queryDocSnap) => {

                    const athlete = getDocumentFromSnapshot(queryDocSnap) as Athlete;

                    return athlete
                })
            })

        return await Promise.all(allAthletes
            .map(async (ath) => {


                return {
                    code: 500,
                    message: `Athlete doc does not exist for athlete`,
                    ath
                }


            })).then((responses) => {

                const groupedEntries = groupByMap(responses, (entry: {
                    code: number;
                    message: string;
                    ath: Athlete;
                    oldProgram: Program;
                    currentProgram?: undefined;
                }) => entry.message + '_' + entry.code) as Map<string, Array<{
                    code: number;
                    message: string;
                    ath: Athlete;
                    oldProgram: Program;
                    currentProgram?: undefined;
                }>>;
                const theResults = []
                groupedEntries.forEach((athleteEntries, key) => {
                    const resp = {
                        code: key,
                        athleteEntries
                    };
                    theResults.push(resp)
                })

                return res.status(204).json({
                    message: `removeStalePrograms completed`,
                    // ath,
                    theResults
                })
            }).catch((err) => {
                return res.status(500).json({
                    message: `removeStalePrograms failed`,
                    // ath,
                    err
                })
            })

    }

    removeEmailFromGroupInd = async (req: Request, res: Response) => {


        const { groupId } = req.body
        if (groupId) {
            return getGroupDocRef(groupId).get()
                .then(async (groupSnap) => {
                    if (groupSnap.exists) {

                        const group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
                        if (!group.athletes) {
                            group.athletes = []
                        }


                        // // const groupedMoodEntriesByAthlete = groupByMap([].concat(...group.athletes.map((d) => d)), (entry: AthleteIndicator) => entry.uid);
                        // const athletesBackup = [...(group as any).athletesBackup]
                        // // groupedMoodEntriesByAthlete.forEach((athletes) => {
                        // //     console.log(athletes)
                        // //     allAthletes.push(athletes[0])
                        // // })
                        // // const chunkByBucketSize = chunkBy(500);

                        // let startSplice = 400
                        // let entSplice = 500

                        // return groupSnap.ref.update({
                        //     athletes: firestore.FieldValue.arrayUnion(...athletesBackup),
                        //     athletesBackup: []
                        // }).then((results) => {
                        //     // return results

                        //     return res.status(200).json({ results })
                        // }).catch((err) => {
                        //     console.error(err)

                        //     // return err
                        //     return res.status(500).json({ error: err })
                        // })
                        // let toReplace = athletesBackup.splice(startSplice, entSplice)
                        // let clearGroupAthletes = false

                        // if (clearGroupAthletes) {

                        //     await groupSnap.ref.update({
                        //         athletes: [],
                        //         // athletesBackup: toReplace
                        //     }).then((results) => {
                        //         return results

                        //         //   return res.status(200).json({ result: result })
                        //     }).catch((err) => {
                        //         console.error(err)

                        //         return err
                        //         // return res.status(500).json({ error: err })
                        //     })
                        // }
                        // let chunkSize = 500

                        // let chunkIndex = 0

                        // const cunks = ([...allAthletes] as any).chunk(chunkSize);

                        // const allResults = [cunks[chunkIndex]].map(async (athletes) => {

                        // const theUpdatedAthletes = await Promise.all(toReplace.map(async (a) => {



                        const allGroupAthletes = cloneDeep(group.athletes)



                        const groupedEntriesByAthlete = groupByMap(allGroupAthletes, (entry: Athlete) => entry.uid);
                        const dupes = []
                        groupedEntriesByAthlete.forEach((athleteEntries) => {
                            if (athleteEntries.lengh > 1) {
                                dupes.push(athleteEntries)
                            }
                        });



                        // const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

                        // if (allGroupAthletes.length !== groupAthletes.length) {
                        //     console.warn('dup athletes removed', {
                        //         groupAthletesWithDups: allGroupAthletes.map((a) => {
                        //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                        //         }),
                        //         groupAthletesCleaned: groupAthletes.map((a) => {
                        //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                        //         })
                        //     })
                        // }

                        let count = 0
                        // const allAthsWithoutBio = allGroupAthletes.filter((a) => !a.bio)
                        // const theUpdatedAthletes = await Promise.all((groupAthletes).map(async (a) => {
                        const theUpdatedAthletes = await Promise.all((allGroupAthletes).map(async (a) => {
                            // a.email = ''

                            let theStart = 0
                            let theAmount = 120
                            try {



                                // a.email !== 'mkruger09@icloud.com' && 
                                if (a.lastName.length > 1) {
                                    count++

                                    const run = true || count > theStart && count < theAmount

                                    if (run) {

                                        const ath = await getAthleteDocFromUID(a.uid)

                                        if (ath) {

                                            if (ath.profile) {

                                                try {
                                                    const lastName = ath.profile.lastName.charAt(0).toUpperCase()

                                                    if (testForLetter(lastName)) {
                                                        a.lastName = lastName || ''
                                                    } else {
                                                        console.error('Last name not a string', ath.profile.lastName)
                                                    }

                                                } catch (error) {
                                                    console.error('Unable to set NEBALLQ ath.profile.lastName.charAt(0).toUpperCase())', error)
                                                }

                                            } else {
                                                console.error('Athlete profile not set for user with UID ' + a.uid, {
                                                    athIndicator: a
                                                })
                                            }


                                        } else {
                                            console.error('Athlete doc not found with UID ' + a.uid, {
                                                athIndicator: a
                                            })
                                        }

                                    }

                                }

                                if (!a.bio || a.bio === '') {
                                    a.bio = a.sport || 'Athlete'
                                }


                            } catch (error) {
                                console.error(error)
                            }

                            return a
                        }))

                        console.log('Update Count', count)
                        let overWriteGroupAthletes = true


                        const updatedGroup = {
                            dupes: dupes.length ? dupes : undefined,
                            athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
                        };

                        removeUndefinedProps({ updatedGroup })

                        return groupSnap.ref.update({
                            athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
                        }).then((results) => {
                            // return results

                            return res.status(200).json({ results })
                        }).catch((err) => {
                            console.error(err)

                            // return err
                            return res.status(500).json({ error: err })
                        })

                        // })

                        // return res.status(200).json({ results: allResults })

                    } else {
                        return res.status(501).json({ error: 'Group Doc does not exist' })
                    }
                }).catch((err) => {
                    return res.status(500).json({ error: err })
                })

        } else {
            return res.status(503).json({ message: 'No ID supplied' })
        }

    }


    splitIntoMultipleGroups = async (req: Request, res: Response) => {


        const athsWithoutDos = []
        const { orgId, groupId } = req.body

        if (groupId) {

            // let allOrgAthletes = await getOrgAthletes(orgId)

            const group = await getGroupDocFromSnapshot(groupId)
            let allGroupAthletes = await Promise.all(group.athletes
                .filter((a) => a.organizationId === group.organizationId)
                .map((a) => {
                    return {
                        ...a,

                        creationTimestamp: a.creationTimestamp.toDate()

                    }
                })
                // .filter((a) => moment(a.creationTimestamp).isAfter(moment().subtract(3, 'days')))
                .map((a) => a.uid).map(async (uid) => {
                    const ath = await getAthleteDocFromUID(uid)

                    if (!ath) {
                        athsWithoutDos.push(uid)
                    }
                    return ath
                }))

            const theAThsWIthoutDOsx = group.athletes.filter((a) => athsWithoutDos.find((aud) => aud === a.uid))


            const allAthletesToRemove = allGroupAthletes.filter((a) => !!a && a.organizationId === group.organizationId && a.groups.find((g) => g.groupId !== 'bNei5o6gOiXQ7xQo4gma-G1'))

            group.athletes = group.athletes.filter((ga) => !allAthletesToRemove.find((a) => a.uid === ga.uid))

            const groupSnap = await db.collection(FirestoreCollection.Groups).doc(group.groupId).update({
                athletes: group.athletes
            }).then(() => {
                return true
            }).catch((e) => {
                console.error(e)
                return false
            })

            allGroupAthletes = allGroupAthletes.filter((a) => !!a && a.groups.find((g) => g.groupId === 'bNei5o6gOiXQ7xQo4gma-G1'))

            // let allOrgAthletes = await cleasrgAthletesGroups(orgId)

            const numAthCount = allGroupAthletes.length //allOrgAthletes.filter((a) => a.groups.length).length


            // allOrgAthletes = allOrgAthletes.filter((a) => !a.groups.length)



            // const grous = await getGroupCollectionRef().where('organizationId', '==', orgId).get().then(async (docSnap) => {
            //     const res = await Promise.all(docSnap.docs.map((doc) => doc.ref.delete()
            //         .then(() => {
            //             return {
            //                 groupID: doc.id,
            //                 deleted: true
            //             }
            //         }).catch((e) => {
            //             return {
            //                 groupID: doc.id,
            //                 deleted: false,
            //                 e
            //             }
            //         })
            //     ))

            //     return res
            // })

            // return res.json(grous)
            // await Promise.all(allAthletes).then(() => res.send(true)).catch(() => res.status(500).send(false))
            // await Promise.all(allAthletes)


            // return getGroupDocRef(groupId).get()
            //     .then(async (groupSnap) => {
            // if (groupSnap.exists) {

            // const group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
            // if (!group.athletes) {
            //     group.athletes = []
            // }

            // const { athletes, ...rest } = group
            // await getGroupDocRef(groupId + '_backup_group').set({ group: rest })
            //     .then(() => {
            //         return true
            //     }).catch((err) => {
            //         console.error(err)
            //     })


            // const res = await getGroupDocRef(groupId + '_backup_athletes').set({ athletes })
            //     .then(() => {
            //         return true
            //     }).catch((err) => {
            //         console.error(err)
            //     })

            // return res
            // // const groupedMoodEntriesByAthlete = groupByMap([].concat(...group.athletes.map((d) => d)), (entry: AthleteIndicator) => entry.uid);
            // const athletesBackup = [...(group as any).athletesBackup]
            // // groupedMoodEntriesByAthlete.forEach((athletes) => {
            // //     console.log(athletes)
            // //     allAthletes.push(athletes[0])
            // // })
            // // const chunkByBucketSize = chunkBy(500);

            // let startSplice = 400
            // let entSplice = 500

            // return groupSnap.ref.update({
            //     athletes: firestore.FieldValue.arrayUnion(...athletesBackup),
            //     athletesBackup: []
            // }).then((results) => {
            //     // return results

            //     return res.status(200).json({ results })
            // }).catch((err) => {
            //     console.error(err)

            //     // return err
            //     return res.status(500).json({ error: err })
            // })
            // let toReplace = athletesBackup.splice(startSplice, entSplice)
            // let clearGroupAthletes = false

            // if (clearGroupAthletes) {

            //     await groupSnap.ref.update({
            //         athletes: [],
            //         // athletesBackup: toReplace
            //     }).then((results) => {
            //         return results

            //         //   return res.status(200).json({ result: result })
            //     }).catch((err) => {
            //         console.error(err)

            //         return err
            //         // return res.status(500).json({ error: err })
            //     })
            // }
            // let chunkSize = 500

            // let chunkIndex = 0

            // const cunks = ([...allAthletes] as any).chunk(chunkSize);

            // const allResults = [cunks[chunkIndex]].map(async (athletes) => {

            // const theUpdatedAthletes = await Promise.all(toReplace.map(async (a) => {

            const orgUID = orgId

            const orgDocRef = getOrgDocRef(orgUID)

            const org = await orgDocRef.get().then((docSnap) => {
                return getDocumentFromSnapshot(docSnap) as Organization;
            });

            // const allOrgAthleteIndicators: AthleteIndicator[] = this.getAthleteIndicators(allOrgAthletes, org, orgId)
            // const allOrgAthleteIndicators: AthleteIndicator[] = this.getAthleteIndicators(allGroupAthletes, org, orgId)
            // const allOrgAthleteIndicators: AthleteIndicator[] = this.getAthleteIndicators(allGroupAthletes, org, orgId)

            // const groupedEntriesByAthlete = groupByMap(allOrgAthleteIndicators, (entry: Athlete) => entry.uid);
            // let dupes = []
            // groupedEntriesByAthlete.forEach((athleteEntries) => {
            //     if (athleteEntries.lengh > 1) {
            //         dupes.push(athleteEntries)
            //     }
            // });




            const chunkSize = org.config.bucketSize || 50

            // const athltetesChunked = chunk(allOrgAthletes, chunkSize);
            // const athltetesChunked = chunk(allGroupAthletes.slice(39, allGroupAthletes.length - 1), chunkSize);
            const athltetesChunked = chunk(allGroupAthletes.slice(0, 26), chunkSize);
            // const length = 3

            // let chunkGroup = athltetesChunked.slice(0, athltetesChunked.length < length ? athltetesChunked.length : length)

            let numberOfUsers = 0// org.numberUsersOnboarded
            // let numberOfUsers = numAthCount

            let addedUsers = 0

            const allUpdates = await Promise.all(athltetesChunked.map(async (athletes: Athlete[], i) => {
                const idx = i + 78 + Math.floor(numberOfUsers / chunkSize)
                const groupIdentifier = `G${idx.toString()}`
                const theGroupId = `${orgUID}-${groupIdentifier}`
                console.log({ groupId: theGroupId })
                const theGroupRef = getGroupDocRef(theGroupId)

                const batch = db.batch();
                const theGroup = await theGroupRef.get()
                    .then(async (docSnap) => {
                        if (docSnap.exists) {
                            const existingGroup = await getDocFromRef(theGroupRef) as Group

                            if (existingGroup.athletes.length < chunkSize) {

                                const diff = chunkSize - existingGroup.athletes.length

                                let updateExistingGroup = false
                                for (let index = 0; index < diff; index++) {

                                    updateExistingGroup = true

                                    const athlete = athletes.shift()

                                    const collectionRef = getAthleteCollectionRef();
                                    const athleteDocRef = collectionRef.doc(athlete.uid);

                                    const groupIndicator = getGroupIndicator(existingGroup)

                                    groupIndicator.creationTimestamp = existingGroup.creationTimestamp as any

                                    batch.update(athleteDocRef, {
                                        groups: [groupIndicator]
                                    });

                                    const athleteIndicator = group.athletes.find((ai) => ai.uid === athlete.uid);

                                    this.setBio(athleteIndicator);

                                    this.setLastnameToFirstCharacter(athlete, athleteIndicator);


                                    existingGroup.athletes.push(athleteIndicator)
                                }

                                if (updateExistingGroup) {
                                    batch.update(theGroupRef, {
                                        athletes: existingGroup.athletes
                                    });

                                }

                                if (athletes.length) {

                                    const athleteIndicators = athletes.map((ath) => {

                                        const collectionRef = getAthleteCollectionRef();

                                        // const theAth = await getAthleteDocFromUID(athlete.uid)
                                        const athleteDocRef = collectionRef.doc(ath.uid);

                                        const groupIndicator = getGroupIndicator(existingGroup)

                                        groupIndicator.creationTimestamp = existingGroup.creationTimestamp as any

                                        batch.update(athleteDocRef, {
                                            groups: [groupIndicator]
                                        });


                                        const athleteIndicator = group.athletes.find((ai) => ai.uid === ath.uid)

                                        this.setBio(athleteIndicator);

                                        this.setLastnameToFirstCharacter(ath, athleteIndicator);


                                        return athleteIndicator
                                    })

                                    const newGroupDoc = createNewGroupDoc(theGroupId, orgUID, groupIdentifier, org, athleteIndicators);

                                    batch.set(theGroupRef, newGroupDoc)

                                    return newGroupDoc

                                } else {
                                    return existingGroup
                                }

                            } else {

                                const athleteIndicators = athletes.map((ath) => {

                                    const collectionRef = getAthleteCollectionRef();

                                    // const theAth = await getAthleteDocFromUID(athlete.uid)
                                    const athleteDocRef = collectionRef.doc(ath.uid);

                                    const groupIndicator = getGroupIndicator(existingGroup)

                                    groupIndicator.creationTimestamp = existingGroup.creationTimestamp as any

                                    batch.update(athleteDocRef, {
                                        groups: [groupIndicator]
                                    });

                                    const athleteIndicator = group.athletes.find((ai) => ai.uid === ath.uid)
                                    this.setBio(athleteIndicator);

                                    this.setLastnameToFirstCharacter(ath, athleteIndicator);

                                    return athleteIndicator
                                })

                                const newGroupDoc = createNewGroupDoc(theGroupId, orgUID, groupIdentifier, org, athleteIndicators);
                                batch.set(theGroupRef, newGroupDoc)

                                return newGroupDoc
                            }
                        } else {

                            const athleteIndicators = athletes.map((ath) => {

                                // const athleteIndicator = allOrgAthleteIndicators.find((ai) => ai.uid === ath.uid)
                                const athleteIndicator = group.athletes.find((ai) => ai.uid === ath.uid)
                                this.setBio(athleteIndicator);

                                this.setLastnameToFirstCharacter(ath, athleteIndicator);

                                return athleteIndicator
                            })

                            const newGroup = await createNewGroupDoc(theGroupId, orgUID, groupIdentifier, org, athleteIndicators);

                            batch.set(theGroupRef, newGroup)

                            athletes.forEach((ath) => {
                                const collectionRef = getAthleteCollectionRef();

                                // const theAth = await getAthleteDocFromUID(athlete.uid)
                                const athleteDocRef = collectionRef.doc(ath.uid);

                                const groupIndicator = getGroupIndicator(newGroup)

                                groupIndicator.creationTimestamp = newGroup.creationTimestamp as any

                                batch.update(athleteDocRef, {
                                    groups: [groupIndicator]
                                });

                            })

                            return newGroup

                        }
                    })


                numberOfUsers = numberOfUsers + athletes.length
                // batch.update(orgDocRef, {
                //     numberUsersOnboarded: numberOfUsers
                // });

                batch.commit();
                return theGroup
                // athletes.map((ath) => {
                //     if (!ath.groups && !!ath.groups.length) {
                //         ath.groups = [group]
                //     } else {
                //         ath.groups[0] = group
                //     }

                // })
                // // const update = await executeMoveAthleteToGroup(athlete.uid, theGroup, false, false)
                // return
            }))

            // batch.update(orgDocRef, {
            //     numberUsersOnboarded: numberOfUsers
            // });

            // batch.commit();
            return res.json({ allUpdates })
            // const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

            // if (allGroupAthletes.length !== groupAthletes.length) {
            //     console.warn('dup athletes removed', {
            //         groupAthletesWithDups: allGroupAthletes.map((a) => {
            //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
            //         }),
            //         groupAthletesCleaned: groupAthletes.map((a) => {
            //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
            //         })
            //     })
            // }

            // let count = 0
            // // const allAthsWithoutBio = allGroupAthletes.filter((a) => !a.bio)
            // // const theUpdatedAthletes = await Promise.all((groupAthletes).map(async (a) => {
            // const theUpdatedAthletes = await Promise.all((allOrgAthleteIndicators).map(async (a) => {
            //     // a.email = ''

            //     let theStart = 0
            //     let theAmount = 120
            //     try {



            //         // a.email !== 'mkruger09@icloud.com' && 
            //         if (a.lastName.length > 1) {
            //             count++

            //             const run = true || count > theStart && count < theAmount

            //             if (run) {

            //                 const ath = await getAthleteDocFromUID(a.uid)

            //                 if (ath) {

            //                     if (ath.profile) {

            //                         try {
            //                             const lastName = ath.profile.lastName.charAt(0).toUpperCase()

            //                             if (testForLetter(lastName)) {
            //                                 a.lastName = lastName || ''
            //                             } else {
            //                                 console.error('Last name not a string', ath.profile.lastName)
            //                             }

            //                         } catch (error) {
            //                             console.error('Unable to set NEBALLQ ath.profile.lastName.charAt(0).toUpperCase())', error)
            //                         }

            //                     } else {
            //                         console.error('Athlete profile not set for user with UID ' + a.uid, {
            //                             athIndicator: a
            //                         })
            //                     }


            //                 } else {
            //                     console.error('Athlete doc not found with UID ' + a.uid, {
            //                         athIndicator: a
            //                     })
            //                 }

            //             }

            //         }

            //         if (!a.bio || a.bio === '') {
            //             a.bio = a.sport || 'Athlete'
            //         }


            //     } catch (error) {
            //         console.error(error)
            //     }

            //     return a
            // }))

            // console.log('Update Count', count)
            // let overWriteGroupAthletes = true


            // const updatedGroup = {
            //     // dupes: dupes.length ? dupes : undefined,
            //     athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
            // };

            // removeUndefinedProps({ updatedGroup })

            // // return groupSnap.ref.update({
            // //     athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
            // // }).then((results) => {
            // //     // return results

            // //     return res.status(200).json({ results })
            // // }).catch((err) => {
            // //     console.error(err)

            // //     // return err
            // //     return res.status(500).json({ error: err })
            // // })

            // // // })

            // // // return res.status(200).json({ results: allResults })

            // // // } else {
            // // //     return res.status(501).json({ error: 'Group Doc does not exist' })
            // // // }
            // // // }).catch((err) => {
            // // //     return res.status(500).json({ error: err })
            // // // })

        } else {
            return res.status(503).json({ message: 'No ID supplied' })
        }

    }

    updateGroupsInOrgArray = async (req: Request, res: Response) => {



        const { orgId, groupId } = req.body

        if (groupId) {

            let allOrgAthletes = await getOrgAthletes(orgId)



            const athNoOrgs = []
            const chunkSize = 400

            // const athltetesChunked = chunk(allOrgAthletes, chunkSize);
            const athltetesChunked = chunk(allOrgAthletes, chunkSize);
            // const length = 3

            // let chunkGroup = athltetesChunked.slice(0, athltetesChunked.length < length ? athltetesChunked.length : length)

            let numberOfUsers = 0// org.numberUsersOnboarded
            // let numberOfUsers = numAthCount

            let addedUsers = 0

            const allUpdates = await Promise.all(athltetesChunked.map(async (athletes: Athlete[], i) => {


                const batch = db.batch();
                athletes.forEach((ath) => {


                    const groupIndicators = ath.groups.map((grp) => {
                        return {
                            ...grp,
                            creationTimestamp: isString(grp.creationTimestamp) ? moment(grp.creationTimestamp).toDate() : grp.creationTimestamp
                        } as any
                    })


                    const orgIndex = (ath.organizations || []).findIndex((o) => o.organizationId === ath.organizationId)


                    if (orgIndex >= 0) {
                        ath.organizations[orgIndex].groups = groupIndicators
                    } else {
                        athNoOrgs.push({
                            athUID: ath.uid
                        })
                    }

                    const collectionRef = getAthleteCollectionRef();

                    // const theAth = await getAthleteDocFromUID(athlete.uid)
                    const athleteDocRef = collectionRef.doc(ath.uid);



                    batch.update(athleteDocRef, {
                        organizations: ath.organizations
                    });

                })



                numberOfUsers = numberOfUsers + athletes.length
                // batch.update(orgDocRef, {
                //     numberUsersOnboarded: numberOfUsers
                // });

                batch.commit();
                return true
                // athletes.map((ath) => {
                //     if (!ath.groups && !!ath.groups.length) {
                //         ath.groups = [group]
                //     } else {
                //         ath.groups[0] = group
                //     }

                // })
                // // const update = await executeMoveAthleteToGroup(athlete.uid, theGroup, false, false)
                // return
            }))

            // batch.update(orgDocRef, {
            //     numberUsersOnboarded: numberOfUsers
            // });

            // batch.commit();
            return res.json({ allUpdates })
            // const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

            // if (allGroupAthletes.length !== groupAthletes.length) {
            //     console.warn('dup athletes removed', {
            //         groupAthletesWithDups: allGroupAthletes.map((a) => {
            //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
            //         }),
            //         groupAthletesCleaned: groupAthletes.map((a) => {
            //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
            //         })
            //     })
            // }

            // let count = 0
            // // const allAthsWithoutBio = allGroupAthletes.filter((a) => !a.bio)
            // // const theUpdatedAthletes = await Promise.all((groupAthletes).map(async (a) => {
            // const theUpdatedAthletes = await Promise.all((allOrgAthleteIndicators).map(async (a) => {
            //     // a.email = ''

            //     let theStart = 0
            //     let theAmount = 120
            //     try {



            //         // a.email !== 'mkruger09@icloud.com' && 
            //         if (a.lastName.length > 1) {
            //             count++

            //             const run = true || count > theStart && count < theAmount

            //             if (run) {

            //                 const ath = await getAthleteDocFromUID(a.uid)

            //                 if (ath) {

            //                     if (ath.profile) {

            //                         try {
            //                             const lastName = ath.profile.lastName.charAt(0).toUpperCase()

            //                             if (testForLetter(lastName)) {
            //                                 a.lastName = lastName || ''
            //                             } else {
            //                                 console.error('Last name not a string', ath.profile.lastName)
            //                             }

            //                         } catch (error) {
            //                             console.error('Unable to set NEBALLQ ath.profile.lastName.charAt(0).toUpperCase())', error)
            //                         }

            //                     } else {
            //                         console.error('Athlete profile not set for user with UID ' + a.uid, {
            //                             athIndicator: a
            //                         })
            //                     }


            //                 } else {
            //                     console.error('Athlete doc not found with UID ' + a.uid, {
            //                         athIndicator: a
            //                     })
            //                 }

            //             }

            //         }

            //         if (!a.bio || a.bio === '') {
            //             a.bio = a.sport || 'Athlete'
            //         }


            //     } catch (error) {
            //         console.error(error)
            //     }

            //     return a
            // }))

            // console.log('Update Count', count)
            // let overWriteGroupAthletes = true


            // const updatedGroup = {
            //     // dupes: dupes.length ? dupes : undefined,
            //     athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
            // };

            // removeUndefinedProps({ updatedGroup })

            // // return groupSnap.ref.update({
            // //     athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
            // // }).then((results) => {
            // //     // return results

            // //     return res.status(200).json({ results })
            // // }).catch((err) => {
            // //     console.error(err)

            // //     // return err
            // //     return res.status(500).json({ error: err })
            // // })

            // // // })

            // // // return res.status(200).json({ results: allResults })

            // // // } else {
            // // //     return res.status(501).json({ error: 'Group Doc does not exist' })
            // // // }
            // // // }).catch((err) => {
            // // //     return res.status(500).json({ error: err })
            // // // })

        } else {
            return res.status(503).json({ message: 'No ID supplied' })
        }

    }

    removeEmailFromGroupInd2 = async (req: Request, res: Response) => {


        const { groupId } = req.body
        if (groupId) {
            return getGroupDocRef(groupId).get()
                .then(async (groupSnap) => {
                    if (groupSnap.exists) {

                        const group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
                        if (!group.athletes) {
                            group.athletes = []
                        }


                        // // const groupedMoodEntriesByAthlete = groupByMap([].concat(...group.athletes.map((d) => d)), (entry: AthleteIndicator) => entry.uid);
                        // const athletesBackup = [...(group as any).athletesBackup]
                        // // groupedMoodEntriesByAthlete.forEach((athletes) => {
                        // //     console.log(athletes)
                        // //     allAthletes.push(athletes[0])
                        // // })
                        // // const chunkByBucketSize = chunkBy(500);

                        // let startSplice = 400
                        // let entSplice = 500

                        // return groupSnap.ref.update({
                        //     athletes: firestore.FieldValue.arrayUnion(...athletesBackup),
                        //     athletesBackup: []
                        // }).then((results) => {
                        //     // return results

                        //     return res.status(200).json({ results })
                        // }).catch((err) => {
                        //     console.error(err)

                        //     // return err
                        //     return res.status(500).json({ error: err })
                        // })
                        // let toReplace = athletesBackup.splice(startSplice, entSplice)
                        // let clearGroupAthletes = false

                        // if (clearGroupAthletes) {

                        //     await groupSnap.ref.update({
                        //         athletes: [],
                        //         // athletesBackup: toReplace
                        //     }).then((results) => {
                        //         return results

                        //         //   return res.status(200).json({ result: result })
                        //     }).catch((err) => {
                        //         console.error(err)

                        //         return err
                        //         // return res.status(500).json({ error: err })
                        //     })
                        // }
                        // let chunkSize = 500

                        // let chunkIndex = 0

                        // const cunks = ([...allAthletes] as any).chunk(chunkSize);

                        // const allResults = [cunks[chunkIndex]].map(async (athletes) => {

                        // const theUpdatedAthletes = await Promise.all(toReplace.map(async (a) => {



                        const allGroupAthletes = cloneDeep(group.athletes)



                        const groupedEntriesByAthlete = groupByMap(allGroupAthletes, (entry: Athlete) => entry.uid);
                        let dupes = []
                        groupedEntriesByAthlete.forEach((athleteEntries) => {
                            if (athleteEntries.lengh > 1) {
                                dupes.push(athleteEntries)
                            }
                        });



                        // const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

                        // if (allGroupAthletes.length !== groupAthletes.length) {
                        //     console.warn('dup athletes removed', {
                        //         groupAthletesWithDups: allGroupAthletes.map((a) => {
                        //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                        //         }),
                        //         groupAthletesCleaned: groupAthletes.map((a) => {
                        //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
                        //         })
                        //     })
                        // }

                        let count = 0
                        // const allAthsWithoutBio = allGroupAthletes.filter((a) => !a.bio)
                        // const theUpdatedAthletes = await Promise.all((groupAthletes).map(async (a) => {
                        const theUpdatedAthletes = await Promise.all((allGroupAthletes).map(async (a) => {
                            // a.email = ''

                            let theStart = 0
                            let theAmount = 120
                            try {



                                // a.email !== 'mkruger09@icloud.com' && 
                                if (a.lastName.length > 1) {
                                    count++

                                    const run = true || count > theStart && count < theAmount

                                    if (run) {

                                        const ath = await getAthleteDocFromUID(a.uid)

                                        if (ath) {

                                            if (ath.profile) {

                                                try {
                                                    const lastName = ath.profile.lastName.charAt(0).toUpperCase()

                                                    if (testForLetter(lastName)) {
                                                        a.lastName = lastName || ''
                                                    } else {
                                                        console.error('Last name not a string', ath.profile.lastName)
                                                    }

                                                } catch (error) {
                                                    console.error('Unable to set NEBALLQ ath.profile.lastName.charAt(0).toUpperCase())', error)
                                                }

                                            } else {
                                                console.error('Athlete profile not set for user with UID ' + a.uid, {
                                                    athIndicator: a
                                                })
                                            }


                                        } else {
                                            console.error('Athlete doc not found with UID ' + a.uid, {
                                                athIndicator: a
                                            })
                                        }

                                    }

                                }

                                if (!a.bio || a.bio === '') {
                                    a.bio = a.sport || 'Athlete'
                                }


                            } catch (error) {
                                console.error(error)
                            }

                            return a
                        }))

                        console.log('Update Count', count)
                        let overWriteGroupAthletes = true


                        const updatedGroup = {
                            dupes: dupes.length ? dupes : undefined,
                            athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
                        };

                        removeUndefinedProps({ updatedGroup })

                        return groupSnap.ref.update({
                            athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
                        }).then((results) => {
                            // return results

                            return res.status(200).json({ results })
                        }).catch((err) => {
                            console.error(err)

                            // return err
                            return res.status(500).json({ error: err })
                        })

                        // })

                        // return res.status(200).json({ results: allResults })

                    } else {
                        return res.status(501).json({ error: 'Group Doc does not exist' })
                    }
                }).catch((err) => {
                    return res.status(500).json({ error: err })
                })

        } else {
            return res.status(503).json({ message: 'No ID supplied' })
        }

    }


    splitIntoMultipleGroups1 = async (req: Request, res: Response) => {



        const { orgId, groupId } = req.body

        if (groupId) {

            let allOrgAthletes = await getOrgAthletes(orgId)
            // let allOrgAthletes = await cleasrgAthletesGroups(orgId)

            const numAthCount = allOrgAthletes.filter((a) => a.groups.length).length


            allOrgAthletes = allOrgAthletes.filter((a) => !a.groups.length)


          
            // const grous = await getGroupCollectionRef().where('organizationId', '==', orgId).get().then(async (docSnap) => {
            //     const res = await Promise.all(docSnap.docs.map((doc) => doc.ref.delete()
            //         .then(() => {
            //             return {
            //                 groupID: doc.id,
            //                 deleted: true
            //             }
            //         }).catch((e) => {
            //             return {
            //                 groupID: doc.id,
            //                 deleted: false,
            //                 e
            //             }
            //         })
            //     ))

            //     return res
            // })

            // return res.json(grous)
            // await Promise.all(allAthletes).then(() => res.send(true)).catch(() => res.status(500).send(false))
            // await Promise.all(allAthletes)


            // return getGroupDocRef(groupId).get()
            //     .then(async (groupSnap) => {
            // if (groupSnap.exists) {

            // const group = { ...groupSnap.data(), uid: groupSnap.id } as Group;
            // if (!group.athletes) {
            //     group.athletes = []
            // }

            // const { athletes, ...rest } = group
            // await getGroupDocRef(groupId + '_backup_group').set({ group: rest })
            //     .then(() => {
            //         return true
            //     }).catch((err) => {
            //         console.error(err)
            //     })


            // const res = await getGroupDocRef(groupId + '_backup_athletes').set({ athletes })
            //     .then(() => {
            //         return true
            //     }).catch((err) => {
            //         console.error(err)
            //     })

            // return res
            // // const groupedMoodEntriesByAthlete = groupByMap([].concat(...group.athletes.map((d) => d)), (entry: AthleteIndicator) => entry.uid);
            // const athletesBackup = [...(group as any).athletesBackup]
            // // groupedMoodEntriesByAthlete.forEach((athletes) => {
            // //     console.log(athletes)
            // //     allAthletes.push(athletes[0])
            // // })
            // // const chunkByBucketSize = chunkBy(500);

            // let startSplice = 400
            // let entSplice = 500

            // return groupSnap.ref.update({
            //     athletes: firestore.FieldValue.arrayUnion(...athletesBackup),
            //     athletesBackup: []
            // }).then((results) => {
            //     // return results

            //     return res.status(200).json({ results })
            // }).catch((err) => {
            //     console.error(err)

            //     // return err
            //     return res.status(500).json({ error: err })
            // })
            // let toReplace = athletesBackup.splice(startSplice, entSplice)
            // let clearGroupAthletes = false

            // if (clearGroupAthletes) {

            //     await groupSnap.ref.update({
            //         athletes: [],
            //         // athletesBackup: toReplace
            //     }).then((results) => {
            //         return results

            //         //   return res.status(200).json({ result: result })
            //     }).catch((err) => {
            //         console.error(err)

            //         return err
            //         // return res.status(500).json({ error: err })
            //     })
            // }
            // let chunkSize = 500

            // let chunkIndex = 0

            // const cunks = ([...allAthletes] as any).chunk(chunkSize);

            // const allResults = [cunks[chunkIndex]].map(async (athletes) => {

            // const theUpdatedAthletes = await Promise.all(toReplace.map(async (a) => {

            const orgUID = orgId

            const orgDocRef = getOrgDocRef(orgUID)

            const org = await orgDocRef.get().then((docSnap) => {
                return getDocumentFromSnapshot(docSnap) as Organization;
            });

            const allOrgAthleteIndicators: AthleteIndicator[] = this.getAthleteIndicators(allOrgAthletes, org, orgId)

            // const groupedEntriesByAthlete = groupByMap(allOrgAthleteIndicators, (entry: Athlete) => entry.uid);
            // let dupes = []
            // groupedEntriesByAthlete.forEach((athleteEntries) => {
            //     if (athleteEntries.lengh > 1) {
            //         dupes.push(athleteEntries)
            //     }
            // });




            const chunkSize = 50

            const athltetesChunked = chunk(allOrgAthletes, chunkSize);
            // const length = 3

            // let chunkGroup = athltetesChunked.slice(0, athltetesChunked.length < length ? athltetesChunked.length : length)

            let numberOfUsers = numAthCount

            const allUpdates = await Promise.all(athltetesChunked.map(async (athletes: Athlete[], i) => {
                const idx = i + 1 + Math.floor(numberOfUsers / 50)
                const groupIdentifier = `G${idx.toString()}`
                const groupId2 = `${orgUID}-${groupIdentifier}`
                console.log({ groupId: groupId2 })
                const theGroupRef = getGroupDocRef(groupId2)
                const batch = db.batch();
                const theGroup = await theGroupRef.get()
                    .then(async (docSnap) => {
                        if (docSnap.exists) {
                            const existingGroup = await getDocFromRef(theGroupRef) as Group

                            if (existingGroup.athletes.length < chunkSize) {

                                const diff = chunkSize - existingGroup.athletes.length

                                for (let index = 0; index < diff; index++) {

                                    const athlete = athletes.shift()

                                    let collectionRef = getAthleteCollectionRef();
                                    const athleteDocRef = collectionRef.doc(athlete.uid);

                                    const groupIndicator = getGroupIndicator(existingGroup)

                                    groupIndicator.creationTimestamp = existingGroup.creationTimestamp as any

                                    batch.update(athleteDocRef, {
                                        groups: [groupIndicator]
                                    });

                                    const athleteIndicator = allOrgAthleteIndicators.find((ai) => ai.uid === athlete.uid);

                                    this.setBio(athleteIndicator);

                                    this.setLastnameToFirstCharacter(athlete, athleteIndicator);


                                    existingGroup.athletes.push(athleteIndicator)
                                }

                                if (athletes.length) {

                                    const athleteIndicators = athletes.map((ath) => {

                                        let collectionRef = getAthleteCollectionRef();

                                        // const theAth = await getAthleteDocFromUID(athlete.uid)
                                        const athleteDocRef = collectionRef.doc(ath.uid);

                                        const groupIndicator = getGroupIndicator(existingGroup)

                                        groupIndicator.creationTimestamp = existingGroup.creationTimestamp as any

                                        batch.update(athleteDocRef, {
                                            groups: [groupIndicator]
                                        });


                                        const athleteIndicator = allOrgAthleteIndicators.find((ai) => ai.uid === ath.uid)

                                        this.setBio(athleteIndicator);

                                        this.setLastnameToFirstCharacter(ath, athleteIndicator);


                                        return athleteIndicator
                                    })

                                    const newGroupDoc = createNewGroupDoc(groupId2, orgUID, groupIdentifier, org, athleteIndicators);

                                    batch.set(theGroupRef, newGroupDoc)

                                    return newGroupDoc

                                } else {
                                    return existingGroup
                                }

                            } else {

                                const athleteIndicators = athletes.map((ath) => {

                                    let collectionRef = getAthleteCollectionRef();

                                    // const theAth = await getAthleteDocFromUID(athlete.uid)
                                    const athleteDocRef = collectionRef.doc(ath.uid);

                                    const groupIndicator = getGroupIndicator(existingGroup)

                                    groupIndicator.creationTimestamp = existingGroup.creationTimestamp as any

                                    batch.update(athleteDocRef, {
                                        groups: [groupIndicator]
                                    });

                                    const athleteIndicator = allOrgAthleteIndicators.find((ai) => ai.uid === ath.uid)
                                    this.setBio(athleteIndicator);

                                    this.setLastnameToFirstCharacter(ath, athleteIndicator);

                                    return athleteIndicator
                                })

                                const newGroupDoc = createNewGroupDoc(groupId2, orgUID, groupIdentifier, org, athleteIndicators);
                                batch.set(theGroupRef, newGroupDoc)

                                return newGroupDoc
                            }
                        } else {

                            const athleteIndicators = athletes.map((ath) => {

                                const athleteIndicator = allOrgAthleteIndicators.find((ai) => ai.uid === ath.uid)
                                this.setBio(athleteIndicator);

                                this.setLastnameToFirstCharacter(ath, athleteIndicator);

                                return athleteIndicator
                            })

                            const newGroup = await createNewGroupDoc(groupId2, orgUID, groupIdentifier, org, athleteIndicators);

                            batch.set(theGroupRef, newGroup)

                            athletes.forEach((ath) => {
                                let collectionRef = getAthleteCollectionRef();

                                // const theAth = await getAthleteDocFromUID(athlete.uid)
                                const athleteDocRef = collectionRef.doc(ath.uid);

                                const groupIndicator = getGroupIndicator(newGroup)

                                groupIndicator.creationTimestamp = newGroup.creationTimestamp as any

                                batch.update(athleteDocRef, {
                                    groups: [groupIndicator]
                                });

                            })

                            return newGroup

                        }
                    })


                numberOfUsers = numberOfUsers + athletes.length
                // batch.update(orgDocRef, {
                //     numberUsersOnboarded: numberOfUsers
                // });

                batch.commit();
                return theGroup
                // athletes.map((ath) => {
                //     if (!ath.groups && !!ath.groups.length) {
                //         ath.groups = [group]
                //     } else {
                //         ath.groups[0] = group
                //     }

                // })
                // // const update = await executeMoveAthleteToGroup(athlete.uid, theGroup, false, false)
                // return
            }))


            return res.json({ allUpdates })
            // const groupAthletes = getUniqueListBy(allGroupAthletes, 'uid') as AthleteIndicator[]

            // if (allGroupAthletes.length !== groupAthletes.length) {
            //     console.warn('dup athletes removed', {
            //         groupAthletesWithDups: allGroupAthletes.map((a) => {
            //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
            //         }),
            //         groupAthletesCleaned: groupAthletes.map((a) => {
            //             return { uid: a.uid, name: a.firstName + ' ' + a.lastName }
            //         })
            //     })
            // }

            let count = 0
            // const allAthsWithoutBio = allGroupAthletes.filter((a) => !a.bio)
            // const theUpdatedAthletes = await Promise.all((groupAthletes).map(async (a) => {
            const theUpdatedAthletes = await Promise.all((allOrgAthleteIndicators).map(async (a) => {
                // a.email = ''

                let theStart = 0
                let theAmount = 120
                try {



                    // a.email !== 'mkruger09@icloud.com' && 
                    if (a.lastName.length > 1) {
                        count++

                        const run = true || count > theStart && count < theAmount

                        if (run) {

                            const ath = await getAthleteDocFromUID(a.uid)

                            if (ath) {

                                if (ath.profile) {

                                    try {
                                        const lastName = ath.profile.lastName.charAt(0).toUpperCase()

                                        if (testForLetter(lastName)) {
                                            a.lastName = lastName || ''
                                        } else {
                                            console.error('Last name not a string', ath.profile.lastName)
                                        }

                                    } catch (error) {
                                        console.error('Unable to set NEBALLQ ath.profile.lastName.charAt(0).toUpperCase())', error)
                                    }

                                } else {
                                    console.error('Athlete profile not set for user with UID ' + a.uid, {
                                        athIndicator: a
                                    })
                                }


                            } else {
                                console.error('Athlete doc not found with UID ' + a.uid, {
                                    athIndicator: a
                                })
                            }

                        }

                    }

                    if (!a.bio || a.bio === '') {
                        a.bio = a.sport || 'Athlete'
                    }


                } catch (error) {
                    console.error(error)
                }

                return a
            }))

            console.log('Update Count', count)
            let overWriteGroupAthletes = true


            const updatedGroup = {
                // dupes: dupes.length ? dupes : undefined,
                athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
            };

            removeUndefinedProps({ updatedGroup })

            // return groupSnap.ref.update({
            //     athletes: overWriteGroupAthletes ? theUpdatedAthletes : firestore.FieldValue.arrayUnion(...theUpdatedAthletes),
            // }).then((results) => {
            //     // return results

            //     return res.status(200).json({ results })
            // }).catch((err) => {
            //     console.error(err)

            //     // return err
            //     return res.status(500).json({ error: err })
            // })

            // // })

            // // return res.status(200).json({ results: allResults })

            // // } else {
            // //     return res.status(501).json({ error: 'Group Doc does not exist' })
            // // }
            // // }).catch((err) => {
            // //     return res.status(500).json({ error: err })
            // // })

        } else {
            return res.status(503).json({ message: 'No ID supplied' })
        }

    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof AthletesCtrl
     */
    addAthlete = async (req: Request, res: Response) => {
        const { athlete } = <{ athlete: Athlete }>req.body
        console.log(req.body)
        // let {athlete} = <{ a: number, b: string }>groupBy(x);
        // See the AthleteRecord reference doc for the contents of athleteRecord.
        console.log('...: ', athlete.uid)

        // save profile
        return getAthleteDocRef(athlete.uid)
            .set(athlete)
            .then(writeResult => {
                athlete.groups.map(async athGroup => {
                    if (athGroup.groupId && athGroup.groupId !== '') {
                        //Create an empty document, or do nothing if it exists.
                        await getGroupDocRef(athGroup.groupId)
                            .set(athGroup, { merge: true })
                            .then(groupWriteResult => {
                                return groupWriteResult
                            })
                    }
                })
                return res.status(200).json({
                    message: 'Athlete added',
                    responseCode: 201,
                    data: {
                        athlete,
                        result: writeResult,
                    },
                    error: undefined,
                    method: {
                        name: 'addAthlete',
                        line: 58,
                    },
                })
            })
            .catch(err => {
                return res.status(502).json({
                    message: 'Error adding Athlete',
                    responseCode: 201,
                    data: {
                        athlete,
                    },
                    error: err,
                    method: {
                        name: 'addAthlete',
                        line: 58,
                    },
                })
            })
    }


    setAthleteCreationTimestamp = async (req: Request, res: Response) => {
        const { athlete } = <{ athlete: Athlete }>req.body
        // console.log(req.body)
        // let {athlete} = <{ a: number, b: string }>groupBy(x);
        // See the AthleteRecord reference doc for the contents of athleteRecord.
        // console.log('...: ', athlete.uid)

        const hasEmail = athlete.profile.email || athlete.email !== ""
        let user

        if (hasEmail) {
            user = await getUserByEmail(athlete.profile.email || athlete.email)
            console.warn('Athlete by Email', { name: athlete.profile.fullName, uid: athlete.uid, email: user.email })
        } else if (athlete.uid) {
            user = await getUserByID(athlete.uid)
            if (user.email) {
                console.warn('Athlete by UID: Email not set on Athlete. Updating Email using User', { name: athlete.profile.fullName, uid: athlete.uid, email: user.email, profider: user.profider })
                athlete.profile.email = athlete.email = user.email
            } else {
                console.warn('Athlete by UID: Email not set on Athlete Nor User. Not Updating Email', { name: athlete.profile.fullName, uid: athlete.uid, profider: user.providerData[0].providerId })
            }
        } else {
            console.error('Athlete Email Nor UID set!!!! Unable to update Athlete Creation Timestamp', athlete)
            return res.status(503).json({
                message: 'Athlete Email Nor UID set ',
                responseCode: 504,
                data: {
                    athlete
                },
                error: undefined,
                method: {
                    name: 'setAthleteCreationTimestamp',
                    line: 115,
                },
            })
        }
        athlete.metadata.creationTimestamp =
            firestore.Timestamp.fromDate(new Date(user.metadata.creationTime))
        athlete.metadata.lastSignInTimestamp =
            firestore.Timestamp.fromDate(new Date(user.metadata.lastSignInTime))


        // save profile
        return getAthleteDocRef(athlete.uid)
            .update({ metadata: athlete.metadata })
            .then(writeResult => {

                return res.status(200).json({
                    message: 'Athlete CreationTimestamp Set',
                    responseCode: 201,
                    data: {
                        athlete,
                        result: writeResult,
                    },
                    error: undefined,
                    method: {
                        name: 'setAthleteCreationTimestamp',
                        line: 115,
                    },
                })
            })
            .catch(err => {
                return res.status(502).json({
                    message: 'Error setting Athlete CreationTimestamp',
                    responseCode: 201,
                    data: {
                        athlete,
                    },
                    error: err,
                    method: {
                        name: 'setAthleteCreationTimestamp',
                        line: 115,
                    },
                })
            })
    }

    purgeAthletes = async (req: Request, res: Response) => {
        // const collection =
        //                         FirestoreProgramsCollection[
        //                             ProgramCategoryType[categoryType]
        //                         ]

        //    this.deleteAggregateyDocuments(uid)

        //  : Promise<AggregateDocumentDirective[]

        const { athleteUid } = <{ athleteUid: string }>req.body

        const aggregateCollectionKeys = Object.keys(WellAggregateCollection).filter(
            k => typeof WellAggregateCollection[k as any] === 'string'
        )

        const aggregateHistoryCollectionKeys = Object.keys(
            WellAggregateHistoryCollection
        ).filter(k => typeof WellAggregateHistoryCollection[k as any] === 'string')
        // ["athleteBiometricWeeklyHistoryAggregates", "athleteBiometricMonthlyHistoryAggregates", ....]

        const coll = []
        aggregateCollectionKeys.forEach(collection => {
            coll.push(WellAggregateCollection[collection])
        })
        aggregateHistoryCollectionKeys.forEach(collection => {
            coll.push(WellAggregateHistoryCollection[collection])
        })

        const results = await coll.map(async collection => {
            const docRef = getCollectionRef(collection)
                .where('entityId', '==', athleteUid)

            await docRef
                .get()
                .then((docsQuerySnap: firestore.QuerySnapshot) => {
                    if (!docsQuerySnap.empty) {
                        docsQuerySnap.docs.forEach(doc =>
                            doc.ref.delete().then(writeResult => writeResult)
                        )
                    } else {
                        // doc.data() will be undefined in this case
                        console.log(
                            `No documents found for athleteUid ${athleteUid}!`
                        )
                    }
                })
                .catch(error => {
                    console.log('Error getting document:', error)
                })
        })

        return await Promise.all(results)
            .then(() => {
                return res.json({ deletedCount: results.length })
            })
            .catch(err => {
                return res.status(500).json({ error: err })
            })
    }


    updateAthlete = async (req: Request, res: Response) => {
        const { userId, coachFlag } = <{ userId: string; coachFlag?: boolean }>(
            req.body
        )

        const result = await getAthleteDocRef(userId)
            .get()
            // getAthleteDocRef('mUHQzOCUDhOhORuw17oZHHkQzQw2').get()
            .then(athleteSnap => {
                if (athleteSnap.exists) {
                    const athelete = {
                        ...athleteSnap.data(),
                        uid: athleteSnap.id
                    } as Athlete
                    const update = {
                        ...athelete,
                        isCoach: coachFlag ? coachFlag : false,
                    }
                    return athleteSnap.ref.update(update).then(() => true)
                } else return false
            })

        if (result) {
            return res.json({ yay: 'success' })
        } else {
            return res.status(500).json({ error: 'err' })
        }
    }

    updateAthleteOrg = async (req: Request, res: Response) => {
        const { groupId, userId, coachFlag } =
            <{ groupId: string; userId: string; coachFlag?: boolean }>req.body

        if (groupId && userId) {
            getGroupDocRef(groupId)
                .get()
                //getGroupDocRef('PxJA8zwoF2zr97UEz89R-G1').get()
                .then(async groupSnap => {
                    if (groupSnap.exists) {
                        const orgGroup = {
                            ...groupSnap.data(),
                            uid: groupSnap.id
                        } as Group

                        await getAthleteDocRef(userId)
                            .get()
                            // getAthleteDocRef('mUHQzOCUDhOhORuw17oZHHkQzQw2').get()
                            .then(async athleteSnap => {
                                if (athleteSnap.exists) {
                                    const athelete = {
                                        ...athleteSnap.data(),
                                        uid: athleteSnap.id
                                    } as Athlete
                                    const update = {
                                        ...athelete,
                                        isCoach: coachFlag ? coachFlag : false,
                                        profile: {
                                            ...athelete.profile,
                                            // onboardingCode: "DIVEAUS-QLD1",
                                        },
                                        organizationId: orgGroup.organizationId,
                                        groups: [
                                            {
                                                logoUrl: orgGroup.logoUrl,
                                                creationTimestamp: orgGroup.creationTimestamp.toDate(),
                                                groupName: orgGroup.groupName,
                                                groupId: orgGroup.groupId,
                                            },
                                        ],
                                        includeGroupAggregation: true,
                                    }
                                    await athleteSnap.ref.update(update)
                                }
                            })
                    }
                })
                .catch(err => {
                    return res.status(500).json({ error: err })
                })
        } else {
            res.status(500).json({ message: 'No ID supplied}' })
        }
    }

    updateAthletes = async (req: Request, res: Response) => {
        const { groupId, userIds } =
            <{ groupId: string; userIds: Array<string> }>req.body

        if (groupId && userIds && userIds.length) {
            await getGroupDocRef(groupId)
                .get()
                // getGroupDocRef('PxJA8zwoF2zr97UEz89R-G1').get()
                .then(groupSnap => {
                    if (groupSnap.exists) {
                        const theGroup = {
                            ...groupSnap.data(),
                            uid: groupSnap.id
                        } as Group

                        userIds.forEach(async userId => {
                            await getAthleteDocRef(userId)
                                .get()
                                // getAthleteDocRef('mUHQzOCUDhOhORuw17oZHHkQzQw2').get()
                                .then(async athleteSnap => {
                                    if (athleteSnap.exists) {
                                        const athelete = {
                                            ...athleteSnap.data(),
                                            uid: athleteSnap.id
                                        } as Athlete
                                        const update = {
                                            ...athelete,
                                            isCoach: false,
                                            profile: {
                                                ...athelete.profile,
                                                onboardingCode: 'DIVEAUS-QLD1',
                                            },
                                            organizationId:
                                                theGroup.organizationId,
                                            groups: [
                                                {
                                                    logoUrl: theGroup.logoUrl,
                                                    creationTimestamp: theGroup.creationTimestamp.toDate(),
                                                    groupName: theGroup.groupName,
                                                    groupId: theGroup.groupId,
                                                },
                                            ],
                                            includeGroupAggregation: true,
                                        }
                                        await athleteSnap.ref.update(update)
                                    }
                                })
                        })
                    }
                })
                .catch(err => {
                    return res.status(500).json({ error: err })
                })
        } else {
            res.status(500).json({ message: 'No ID supplied}' })
        }
    }

    getAthletes = async (req: Request, res: Response) => {
        // const { athlete } = <{ athlete: Athlete }>req.body
        console.log(req.body)

        // const datasets = await listDatasets()
        let ipscoreNonEngagementDataset = await getDataset('ip_score_non_engagement')
        if (!ipscoreNonEngagementDataset) {
            ipscoreNonEngagementDataset = await createDataset('ip_score_non_engagement').catch((err) => {
                console.error('Unsable to create Dataset', err)
                throw err
            })

        }


        // const emailLias = [
        //     '21millik@mbc.qld.edu.au',
        //     '21millid@mbc.qld.edu.au',
        //     'mad_gymnast@mail.com',
        //     'mackenzie.bowell@yahoo.com.au',
        //     'drookwood@gymqld.org.au',
        //     'retrofrog11@gmail.com',
        //     'stj1992@hotmail.com',
        //     'lcurran@lions.com.au',
        //     'lauren.king97@gmail.com',
        //     'ahighlands97@gmail.com',
        //     'hiaaron@bigpond.com',
        //     '2024051@allhallows.qld.edu.au',
        //     'georgiasheehann@gmail.com',
        //     'bigman_ash@hotmail.com',
        //     'holly.smith03@outlook.com',
        //     'sarah-lou_@hotmail.com',
        //     'kieran_donohue@hotmail.com',
        //     'sara.cameron@live.com.au',
        //     'annabellesimon@simon.com.au',
        //     'alistairjameshammond97@gmail.com'
        // ]

        const allAthletes = setDefaulCommsConfig('M3SghDPZrSjeZXyVIePW');

        return res.status(200).json({
            message: 'Athletes',
            responseCode: 201,
            data: {
                allAthletes,
            },
            error: undefined,
            method: {
                name: 'getAthletes',
                line: 58,
            },
        });

        // getOrgCollectionRef().where("organizationCode", "==", 'DIVEAUS').get()
        //     .then((org) => {
        //         if (org.size) {

        //             const orgs = org.docs.pop();

        //             const ddd = { uid: orgs.id, ...orgs.data() }
        //         }
        //     })

        // // let {athlete} = <{ a: number, b: string }>groupBy(x);
        // // See the AthleteRecord reference doc for the contents of athleteRecord.
        // console.log('...: ', athlete.uid)

        // // save profile
        // return getAthleteDocRef(athlete.uid)
        //     .set(athlete)
        //     .then(writeResult => {
        //         athlete.groups.map(group => {
        //             if (group.groupId && group.groupId !== '') {
        //                 //Create an empty document, or do nothing if it exists.
        //                 getGroupDocRef(group.groupId)
        //                     .set(group, { merge: true })

        //                     .then(groupWriteResult => {
        //                         return groupWriteResult
        //                     })
        //             }
        //         })
        //         return res.status(200).json({
        //             message: 'Athlete added',
        //             responseCode: 201,
        //             data: {
        //                 athlete,
        //                 result: writeResult,
        //             },
        //             error: undefined,
        //             method: {
        //                 name: 'addAthlete',
        //                 line: 58,
        //             },
        //         })
        //     })
        //     .catch(err => {
        //         return res.status(502).json({
        //             message: 'Error adding Athlete',
        //             responseCode: 201,
        //             data: {
        //                 athlete,
        //             },
        //             error: err,
        //             method: {
        //                 name: 'addAthlete',
        //                 line: 58,
        //             },
        //         })
        //     })
    }

    clearAthleteDocs = async (req: Request, res: Response) => {

        const { athleteUID, filter } = req.body


        // const fonyEmailUIDs = []
        // // await db.collection(FirestoreCollection.Athletes)
        // //     // .orderBy('profile.email')
        // //     // .startAt('fony').endAt('fony' + "\uf8ff")
        // //     // .where('', '>=', '--')
        // //     // .doc(orgID)
        // //     .get().then((queryDocSnap) => {

        // //         if (!queryDocSnap.empty) {
        // //             return queryDocSnap.docs.forEach((docSnap) => {

        // //                 const athleteDoc = getDocumentFromSnapshot(docSnap) as Athlete

        // //                 if (!!athleteDoc && athleteDoc.profile.email.indexOf('_fony_') > -1) {
        // //                     fonyEmailUIDs.push(athleteDoc.uid)
        // //                 }
        // //             })
        // //         }
        // //     }
        // //     )
        // //     .catch((err) => {
        // //         console.warn(err)

        // //         throw err
        // //     })



        // await admin.auth().listUsers()
        //     .then(async (users) => {
        //         console.log('Successfully returned users')


        //         users.users.forEach(user => {

        //             if (user.email.indexOf(filter) > -1) {
        //                 fonyEmailUIDs.push(user.uid)
        //             }
        //         });
        //         // const athleteDocSnap = await getAthleteDocRef(athleteUID).get()

        //         // if (athleteDocSnap.exists) {
        //         //     return await athleteDocSnap.ref.delete().then(() => true).catch((err) => err)
        //         // }
        //         return true
        //     })
        //     .catch(function (error) {
        //         console.log('Error deleting user:', error);

        //         throw error
        //     });



        // const resass = await Promise.all(fonyEmailUIDs.map(async (athleteUid) => {
        //     return await admin.auth().deleteUser(athleteUid)
        //         .then(async () => {
        //             console.log('Successfully deleted user')
        //             // const athleteDocSnap = await getAthleteDocRef(athleteUID).get()

        //             // if (athleteDocSnap.exists) {
        //             //     return await athleteDocSnap.ref.delete().then(() => true).catch((err) => err)
        //             // }
        //             return true
        //         })
        //         .catch(function (error) {
        //             console.log('Error deleting user:', error);

        //             throw error
        //         });
        // }))

        // console.error('Error Purging Athlete Documents')
        // return res.status(500).json({
        //     message: 'sendEmail',
        //     responseCode: 201,
        //     data: {},
        //     error: resass,
        //     method: {
        //         name: 'sendEmail',
        //         line: 58,
        //     },
        // })


        const purgeAthleteCollectionsResults = await cleanUpAthleteRelatedDocuments(athleteUID).catch((purgeAthleteCollectionsErr) => {
            removeUndefinedProps(purgeAthleteCollectionsErr)
            console.error('Error Purging Athlete Documents', purgeAthleteCollectionsErr)
            return res.status(500).json({
                message: 'sendEmail',
                responseCode: 201,
                data: {},
                error: purgeAthleteCollectionsErr,
                method: {
                    name: 'sendEmail',
                    line: 58,
                },
            })
        })

        return res.status(200).json({
            message: 'clearAthleteDocs',
            responseCode: 201,
            data: {
                purgeAthleteCollectionsResults,
            },
            method: {
                name: 'sendEmail',
                line: 58,
            },
        })
    }
    sendEmail = async (req: Request, res: Response) => {
        const options = {
            method: 'POST',
            uri: 'https://us-central1-inspire-1540046634666.cloudfunctions.net/sendgridEmail?sg_key=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',
            body: {
                "to": "ian.gouws@kaapisoft.com",
                "from": "athleteservices@inspiresportonline.com",
                "subject": "Hello from Sendgrid!",
                "body": "<h1>Hello</h1> <p>World!</p>"
            },
            json: true // Automatically stringifies the body to JSON
        };

        const options2 = {
            method: 'POST',
            uri: 'https://us-central1-inspire-1540046634666.cloudfunctions.net/sendgridEmail?sg_key=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',
            body: {
                "to": "ian.gouws@kaapisoft.com",
                "from": "athleteservices@inspiresportonline.com",
                "subject": "Hello from Sendgrid!",
                "body": { message: 'Hi There', content: "<h1>Hello</h1> <p>World!</p>" }
            },
            json: true // Automatically stringifies the body to JSON
        };

        rp(options2)
            .then((parsedBody) => {
                // POST succeeded...
                console.log('parsedBody', parsedBody)
                return res.status(200).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {
                        parsedBody,
                    },
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            })
            .catch((err) => {
                // POST failed...
                console.log('err', err)
                return res.status(500).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {},
                    error: err,
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            });
        return await rp(options)
            .then((parsedBody) => {
                // POST succeeded...
                console.log('parsedBody', parsedBody)
                return res.status(200).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {
                        parsedBody,
                    },
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            })
            .catch((err) => {
                // POST failed...
                console.log('err', err)
                return res.status(500).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {},
                    error: err,
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            });



        const sendgridKey = functions.config()

        console.log(sendgridKey)

        sgMail.setApiKey('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');
        const msg = {
            to: 'ian.gouws@kaapisoft.com',
            from: 'athleteservices@inspiresportonline.com',
            subject: 'Sending with SendGrid is Fun',
            text: 'and easy to do anywhere, even with Node.js',
            html: '<strong>and easy to do anywhere, even with Node.js</strong>',
        };
        const tttt = await sgMail.send(msg)
            .then((parsedBody) => {
                // POST succeeded...
                console.log('parsedBody', parsedBody)
                return res.status(200).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {
                        parsedBody,
                    },
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            })
            .catch((err) => {
                // POST failed...
                console.log('err', err)
                return res.status(500).json({
                    message: 'sendEmail',
                    responseCode: 201,
                    data: {},
                    error: err,
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            });
        return tttt
    }
    createAthleteHistoryDocuments = async (req: Request, res: Response) => {
        this.athleteHistoryDocumentManager = new AthleteHistoryDocumentManager()

        return this.athleteHistoryDocumentManager
            .doRead(req.body, 'api/add Entity History Docs', 'info')
            .then(data => {
                if (data) {
                    console.log(data[0])
                    return res.status(201).json({
                        message: 'History documents created',
                        responseCode: 201,
                        data,
                        error: undefined,
                        method: {
                            name: 'createAthleteHistoryDocuments',
                            line: 82,
                        },
                    })
                    // return Promise.resolve(data)
                } else {
                    LogInfo('empty data')

                    return res.status(205).json({
                        message: 'No hisory docs created',
                        responseCode: 205,
                        data,
                        error: undefined,
                        method: {
                            name: 'createAthleteHistoryDocuments',
                            line: 82,
                        },
                    })
                    return Promise.resolve(null)
                }
            })
            .catch(err => {
                LogInfo('Eooro Occured!!!!', err)
                // return res.status(501).json({
                //     message: 'Error creating History Docs',
                //     responseCode: 201,
                //     err,
                //     error: undefined,
                //     method: {
                //         name: 'addAthlete',
                //         line: 58,
                //     },
                // })
                return Promise.reject(err)
            })
    }

    private getAthleteIndicators(allOrgAthletes: Athlete[], org: Organization, orgId: any): AthleteIndicator[] {
        return cloneDeep(allOrgAthletes.map((ath) => {
            const athleteIndicator: AthleteIndicator = {
                uid: ath.uid,
                firstName: ath.profile.firstName,
                lastName: ath.profile.lastName,
                creationTimestamp: ath.metadata.creationTimestamp,
                isIpScoreIncrease: false,
                ipScore: ath.currentIpScoreTracking.ipScore,
                isIpScoreDecrease: false,
                email: ath.profile.email,
                profileImageURL: ath.profileImageURL || ath.profile.imageUrl || '',
                bio: ath.profile.bio || ath.profile.sport || 'Athlete',
                runningTotalIpScore: ath.runningTotalIpScore,
                subscriptionType: ath.profile.subscription.type,
                orgTools: org.onboardingConfiguration.tools || ath.orgTools || [],
                sport: ath.profile.sport || '',
                includeGroupAggregation: true,
                organizationId: orgId,
                isCoach: false,
                groups:[],
                firebaseMessagingIds: [],
            };
            // this.setBio(athleteIndicator);
            // this.setLastnameToFirstCharacter(ath, athleteIndicator);
            return athleteIndicator;
        }));
    }

    private setLastnameToFirstCharacter(athlete: Athlete, athleteIndicator: AthleteIndicator) {
        if (athlete.profile) {
            try {
                const lastName = athlete.profile.lastName.charAt(0).toUpperCase();
                if (testForLetter(lastName)) {
                    athleteIndicator.lastName = lastName || '';
                }
                else {
                    console.error('Last name not a string', athlete.profile.lastName);
                }
            }
            catch (error) {
                console.error('Unable to set NEBALLQ ath.profile.lastName.charAt(0).toUpperCase())', error);
            }
        }
        else {
            console.error('Athlete profile not set for user with UID ' + athleteIndicator.uid, {
                athIndicator: athleteIndicator
            });
        }
    }

    private setBio(athleteIndicator: AthleteIndicator) {
        if (!athleteIndicator.bio || athleteIndicator.bio === '') {
            athleteIndicator.bio = athleteIndicator.sport || 'Athlete';
        }
    }
}
async function getOrgAthletes(orgId: any) {
    return await getAthleteCollectionRef().where('organizationId', '==', orgId).get()
        .then((querySnap) => {
            if (querySnap.size) {
                const res = querySnap.docs.map((docSnap) => {
                    const theAth = getDocumentFromSnapshot(docSnap) as Athlete;
                    return theAth;
                    // return docSnap.ref.update({
                    //     groups: []
                    // }).then(() => true).catch((e) => {
                    //     console.error(e)
                    //     throw e
                    // })
                });
                return res;
            }
            else {
                return [];
            }
        });
}
