import Queue from 'bull';
import { db } from '../../../../shared/init/initialise-firebase';
import { FirestoreCollection } from '../../../../db/biometricEntries/enums/firestore-collections';
const videoQueue = new Queue('video transcoding');
// const models = require('../models');
const ffmpeg = require('fluent-ffmpeg');
// const fs = require('fs');

export type VideoConversionState = 'pending' | 'done' | 'cancelled'

export interface VideoConversion {
    uid: string;
    filePath: string;
    convertedFilePath: string;
    outputFormat: string;
    status: VideoConversionState // pending default
    createdAt: Date;
    updatedAt: Date;

}

const convertVideo = (path, format): Promise<{ convertedFilePath: string }> => {
    const fileName = path.replace(/\.[^/.]+$/, "");
    const convertedFilePath = `${fileName}_${+new Date()}.${format}`;
    return new Promise((resolve, reject) => {
        ffmpeg(`${__dirname}/../files/${path}`)
            .setFfmpegPath(process.env.FFMPEG_PATH)
            .setFfprobePath(process.env.FFPROBE_PATH)
            .toFormat(format)
            .on("start", commandLine => {
                console.log(`Spawned Ffmpeg with command: ${commandLine}`);
            })
            .on("error", (err, stdout, stderr) => {
                console.log(err, stdout, stderr);
                reject(err);
            })
            .on("end", (stdout, stderr) => {
                console.log(stdout, stderr);
                resolve({ convertedFilePath });
            })
            .saveToFile(`${__dirname}/../files/${convertedFilePath}`);
    });
};
videoQueue.process(async job => {
    const { id, path, outputFormat } = job.data;
    try {
        const convRef = db
            .collection(FirestoreCollection.VideoConversions)
            .doc(id)

        const conv = await convRef.get().then((docSnap) => {
            if (docSnap.exists) {
                return {
                    ...docSnap.data(),
                    uid: docSnap.id
                } as VideoConversion
            } else {
                return undefined
            }
        })
        if (!conv) {
            return Promise.reject({ message: 'Document not found' });
        }
        if (conv.status === "cancelled") {
            return Promise.resolve();
        }
        const pathObj = await convertVideo(path, outputFormat);
        const convertedFilePath = pathObj.convertedFilePath;
        const conversion = await convRef.update(
            { convertedFilePath, status: "done" }
        );
        return Promise.resolve(conversion);
    } catch (error) {
        return Promise.reject(error);
    }
});
export { videoQueue };

