import { FirestoreCollection } from './../../../../../db/biometricEntries/enums/firestore-collections';
import { Response, Request } from 'express'
import * as functions from 'firebase-functions'
// import * as sgMail from '@sendgrid/mail'
const sgMail = require('@sendgrid/mail');

import BaseCtrl from '../../controllers/base';
import { isProductionProject } from '../../is-production';
import { db } from '../../../../../shared/init/initialise-firebase';
import { KeyValuePair } from '../../../../../models/notifications/interfaces/schedule-notification-payload';
import { StackDriverTemplateIDs, LifestyleEntryType } from '../../../../../models/enums/lifestyle-entry-type';
import { IpScoreTracking } from '../../../../../db/ipscore/models/documents/tracking/ip-score-tracking.interface';

export type RedFlagType = 'Sad Entry' | 'Less Than 4 hrs Sleep' | 'High Level of Pain' | 'Extremely Fatigued';

export enum NotificationSeverity {
    Danger = 'danger',
    Warn = 'warn',
    Success = 'success',
}
export interface EmailMessage {

    // firstName: string;
    // lastName: string;

    // email: string;

    personalizations: [{
        to: {
            name: string;
            email: string;
        }[]
    }],
    from?: {
        name: string;
        email: string;
    },
    attachments?: [
        {
            content: string; // 'Some base 64 encoded attachment content',
            filename: string; // 'some-attachment.txt',
            type: string; //  'plain/text',
            disposition: string; //  'attachment',
            contentId: string; // 'mytext'
        }
    ],
    // subject: string;
    // text?: string;
    // html: string;
    dynamic_template_data?: {

        isActive?: boolean;
        name?: string;
        athletes?: Array<{
            uid: string;
            fullName: string;
            email: string;
            currentIpScoreTracking?: IpScoreTracking;
        }>;
        numberOfAthletes?: number;
        dayCountNonEngagement?: number;

        subject?: string;
        message?: string;

        title?: string;

        programsHeading?: string;
        programsSubHeading?: string;

        programs?: Array<Program>;
        program1?: Program;
        program2?: Program;

        showDownloadButton?: boolean;
        barType?: {
            danger?: boolean;
            warn?: boolean;
            success?: boolean;
        };

        userOnboardingCode?: string
        groupIDs?: string[];
        userFullName?: string;
        userFirstName?: string;
        analyticsFamily?: string;
        userID?: string;
        value?: number;
        isOnboarding?: boolean,

        taskID?: string;
        organizationID?: string;
        organizationName?: string;
        groupID?: string;
        groupName?: string;
        athleteID?: string;
        actionUrl?: string;
        token?: string;

        sendPushNotification?: boolean;
    }
    templateId: string;
    emailTemplateName: string;
    notificationTemplateType?: number;
    notificationTemplate?: string;
    includePushNotification?: boolean;
}

// export interface EmailMessageParams {

//     USER_ID?: string;
//     ORGANIZATION_ID?: string;
//     GROUP_IDS: string;

//     USER_FULL_NAME: string;
//     USER_ONBOARDING_CODE: string;
// }

export interface OnboardingInviteEmailPayload {
    taskID?: string;
    organizationName?: string;
    groupID?: string;
    groupName?: string;
    actionUrl?: string;
    athleteID?: string;
    token?: string;
    sendPushNotification?: boolean;
    isOnboarding?: boolean;

    userFirstName?: string;
    userFullName?: string;
    userFirebaseMessagingIds?: Array<string>;
    senderFullName?: string;
    senderInitials?: string;
    rootUrl?: string;
}

export interface RedFlagEmailPayload {
    athleteID?: string;
    userFirstName?: string;
    userFullName?: string;
    senderFullName?: string;
    redFlagType?: string;
}

export interface EmailMessagePayload extends OnboardingInviteEmailPayload {
    userID?: string;
    organizationID?: string;
    groupIDs?: string[];
    userFirstName: string;
    userFullName: string;
    userOnboardingCode?: string;
    value?: number;
    lifestyleEntryType?: LifestyleEntryType;
    analyticsFamily?: string;
    geoPoint?: any;
    flagType?: RedFlagType;
    creationTimestamp: string;
    creatorImageUrl?: string;
    lifestyleEntryValue?: string;
}

export enum NotificationBarType {
    Danger = 'danger',
    Warn = 'warn',
    Success = 'success'
}
export enum HeadingImageType {
    Mood = 'mood',
    Body = 'body',
    Training = 'training',
    Food = 'food'
}
export interface TemplateData {
    heading: string;
    showDangerBar: boolean;
    lifestyleEntryType: HeadingImageType;
    // dynamic_Template_data: {
    isActive?: boolean;
    name: string;
    programs?: Array<Program>,
    message: string;
    programsHeading: string;
    programsSubHeading: string;
    subject: string;
    title: string;

    showDownloadButton?: boolean;
    barType: {
        danger?: boolean;
        warn?: boolean;
        success?: boolean;
    };
    // }


}

export interface Program {
    description: string;
    image: string;
    link: string;
    title: string;
}

export const replaceParts = (
    emailTemplate: TemplateData,
    keyValuePairs: KeyValuePair[]
) => {
    let heading = emailTemplate.heading
    if (heading)
        keyValuePairs.forEach(pair => {
            heading = heading.replace(pair.key, pair.value)
        })

    let message = emailTemplate.message
    if (message)
        keyValuePairs.forEach(pair => {
            message = message.replace(pair.key, pair.value)
        })

    let subject = emailTemplate.subject
    if (subject)
        keyValuePairs.forEach(pair => {
            subject = subject.replace(pair.key, pair.value)
        })

    let title = emailTemplate.title
    if (title)
        keyValuePairs.forEach(pair => {
            title = title.replace(pair.key, pair.value)
        })

    let programsHeading = emailTemplate.programsHeading
    if (programsHeading)
        keyValuePairs.forEach(pair => {
            programsHeading = programsHeading.replace(pair.key, pair.value)
        })

    let programsSubHeading = emailTemplate.programsSubHeading
    if (programsSubHeading)
        keyValuePairs.forEach(pair => {
            programsSubHeading = programsSubHeading.replace(pair.key, pair.value)
        })

    const res: TemplateData = {
        ...emailTemplate,
        heading,
        message,
        subject,
        title,
        programsHeading,
        programsSubHeading

    }
    return res
}


/**
 * The controller is next in the pipeline to handle the requests,
 * as you can see the req and res has just been routed here from the router
 * @export
 * @class SendGridCtrl
 * @extends {BaseCtrl}
 */
export default class SendGridCtrl extends BaseCtrl {
    res: Response

    // logger: Logger;
    model = undefined // SendGrid

    collection = undefined //FirestoreCollection.SendGrid

    // sendGridHistoryDocumentManager: SendGridHistoryDocumentManager

    constructor() {
        super()
    }

    /**
     * Adds a biometric entry to the database
     * Currently using so that I can test the onCreate events! ;P
     *
     * @memberof SendGridCtrl
     */

    sendEmail = async (req: Request, res: Response) => {





        const { message } = <{ message: EmailMessage }>req.body

        const config = functions.config()



        const { id: key } = req.params

        // const message: EmailMessage = {
        //     personalizations: [
        //         {
        //             to: [
        //                 {
        //                     name: "Ian Kaapisoft",
        //                     email: "ian.gouws@kaapisoft.com"
        //                 },
        //                 {
        //                     name: "Ian iNSPIRE",
        //                     email: "ian@inspiresportonline.com"
        //                 }
        //             ]
        //         }
        //     ],
        //     from: {
        //         name: "iNSPIRE Dev",
        //         email: "athleteservices@inspiresportonline.com"
        //     },
        //     templateId: "d-93756f51e55b4734a5b93d23e4310750",
        //     emailTemplate: "badMood",
        //     dynamic_template_data: {

        //         name: "Ian Gouws!!",


        //         barType: {
        //             danger: true
        //         }
        //     }
        // }

        // return sendEmail(message, key)

        if (isProductionProject() && config.sendgrid && config.sendgrid.key) {

            const keyFromConfig = config.sendgrid.key
            sgMail.setApiKey(keyFromConfig);

        } else if (req.params.id) {
            if (key && key !== '') {
                sgMail.setApiKey(key);
            } else {
                return res.status(501).json({
                    message: 'sendEmail',
                    responseCode: 501,
                    data: {},
                    error: 'SendGrid API Key undefined',
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            }
        }


        // const template = {
        //     "uid": "badMood",
        //     "message": "<b> {{{name}}}<\/b> logged 3 consequitive \"very sad\" or \"sad\" mood entries and may need your help",
        //     "programsHeading": "Perhaps these programs can help???? :P",
        //     "programsSubHeading": "We have a wide range of wellness type programs that may be useful?!",
        //     "title": "Mood Entries!!!",
        //     "programs": [
        //         {
        //             "title": "Some Program",
        //             "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png",
        //             "description": "Some details here.",
        //             "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png"
        //         },
        //         {
        //             "description": "Some Other details here.",
        //             "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png",
        //             "title": "Some Other Program",
        //             "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png"
        //         }
        //     ],
        //     "subject": "An urgent message about {{{ name }}}!!!!"
        // }

        // sendEmail(message, keyFromConfig || key )
        const emailTemplate = message.emailTemplateName || 'badMood'

        const dbTemplateData = await db.collection(FirestoreCollection.EmailTemplates).doc(emailTemplate)
            .get().then((docSnap) => {

                return {
                    uid: docSnap.id,
                    ...docSnap.data() as TemplateData,
                }
            })
        // await db.collection(FirestoreCollection.EmailTemplates)
        // .doc('distraughtMood')
        // .set({
        //   ...dbTemplateData,
        //   message: '<b>{{{name}}}</b> logged a <b>DISTRAUGHT</b> mood entry and may need your help'
        // })

        const { programs: dbPrograms, ...restDBTemplateData } = dbTemplateData

        const { programs: customPrograms, ...restCustomTemplateData } = message.dynamic_template_data

        console.warn('Adding key: $NAME')

        const keyValuePairs: KeyValuePair[] = [
            {
                key: '{{{name}}}',
                value: restCustomTemplateData.name
            },
            {
                key: '$NAME',
                value: restCustomTemplateData.name
            }
        ];

        const parsedMessage = replaceParts({ ...restDBTemplateData, ...restCustomTemplateData }, keyValuePairs)


        const program1 = customPrograms && customPrograms[0]
            ? customPrograms[0] : dbPrograms[0]

        const program2 = customPrograms && customPrograms[1]
            ? customPrograms[1] : dbPrograms[1]


        message.templateId = message.templateId || StackDriverTemplateIDs.DoubleImageLayout;

        message.dynamic_template_data = { ...parsedMessage, program1, program2 }
        const from = {
            name: "iNSPIRE Dev",
            email: "athleteservices@inspiresportonline.com"
        }

        message.from = from

        return await sgMail.send({
            ...message,
            // subject: unescape(message.subject),
            // text: unescape(message.text),
            // title: unescape(message.title),
            // html: unescape(message.html),
            // personalizations: [{
            //     to: {
            //         name: unescape(message.personalizations[0].to[0].name),
            //         email: unescape(message.personalizations[0].to[0].email)
            //     }
            // }]
            // ,
            // from: {
            //     name: unescape(message.from.name),
            //     email: unescape(message.from.email)
            // }
        }, (resCB) => {
            if (resCB) {
                console.log('resCB', resCB)
            }
        })
            .then((response) => {
                // POST succeeded...
                // console.log('parsedBody', parsedBody)
                console.log('sendEmail statusCode', response[0].statusCode)
                return res.status(response[0].statusCode || 202).json({
                    message: 'sendEmail',
                    responseCode: response[0].statusCode || 202,
                    data: {
                        response,
                    },
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            })
            .catch((err) => {
                // POST failed...
                console.log('err', err)
                return res.status(500).json({
                    message: 'sendEmail',
                    responseCode: 500,
                    data: {},
                    error: err,
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            });


    }

    sendEmailTest = async (req: Request, res: Response) => {





        const { message } = <{ message: EmailMessage }>req.body

        const config = functions.config()



        const { id: key } = req.params

        // const message: EmailMessage = {
        //     personalizations: [
        //         {
        //             to: [
        //                 {
        //                     name: "Ian Kaapisoft",
        //                     email: "ian.gouws@kaapisoft.com"
        //                 },
        //                 {
        //                     name: "Ian iNSPIRE",
        //                     email: "ian@inspiresportonline.com"
        //                 }
        //             ]
        //         }
        //     ],
        //     from: {
        //         name: "iNSPIRE Dev",
        //         email: "athleteservices@inspiresportonline.com"
        //     },
        //     templateId: "d-93756f51e55b4734a5b93d23e4310750",
        //     emailTemplate: "badMood",
        //     dynamic_template_data: {

        //         name: "Ian Gouws!!",


        //         barType: {
        //             danger: true
        //         }
        //     }
        // }

        // return sendEmail(message, key)

        if (isProductionProject() && config.sendgrid && config.sendgrid.key) {

            const keyFromConfig = config.sendgrid.key
            sgMail.setApiKey(keyFromConfig);

        } else if (req.params.id) {
            if (key && key !== '') {
                sgMail.setApiKey(key);
            } else {
                return res.status(501).json({
                    message: 'sendEmail',
                    responseCode: 501,
                    data: {},
                    error: 'SendGrid API Key undefined',
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            }
        }


        // const template = {
        //     "uid": "badMood",
        //     "message": "<b> {{{name}}}<\/b> logged 3 consequitive \"very sad\" or \"sad\" mood entries and may need your help",
        //     "programsHeading": "Perhaps these programs can help???? :P",
        //     "programsSubHeading": "We have a wide range of wellness type programs that may be useful?!",
        //     "title": "Mood Entries!!!",
        //     "programs": [
        //         {
        //             "title": "Some Program",
        //             "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png",
        //             "description": "Some details here.",
        //             "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png"
        //         },
        //         {
        //             "description": "Some Other details here.",
        //             "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png",
        //             "title": "Some Other Program",
        //             "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png"
        //         }
        //     ],
        //     "subject": "An urgent message about {{{ name }}}!!!!"
        // }

        // sendEmail(message, keyFromConfig || key )
        const emailTemplate = message.emailTemplateName || 'badMood'

        const dbTemplateData = await db.collection(FirestoreCollection.EmailTemplates).doc(emailTemplate)
            .get().then((docSnap) => {

                return {
                    uid: docSnap.id,
                    ...docSnap.data() as TemplateData,
                }
            })
        // await db.collection(FirestoreCollection.EmailTemplates)
        // .doc('distraughtMood')
        // .set({
        //   ...dbTemplateData,
        //   message: '<b>{{{name}}}</b> logged a <b>DISTRAUGHT</b> mood entry and may need your help'
        // })

        const { programs: dbPrograms, ...restDBTemplateData } = dbTemplateData

        const { programs: customPrograms, ...restCustomTemplateData } = message.dynamic_template_data

        const keyValuePairs: KeyValuePair[] = [
            {
                key: '{{{name}}}',
                value: restCustomTemplateData.name
            }
        ]

        const parsedMessage = replaceParts({ ...restDBTemplateData, ...restCustomTemplateData }, keyValuePairs)


        const program1 = customPrograms && customPrograms[0]
            ? customPrograms[0] : dbPrograms[0]

        const program2 = customPrograms && customPrograms[1]
            ? customPrograms[1] : dbPrograms[1]


        message.templateId = message.templateId || StackDriverTemplateIDs.DoubleImageLayout;

        message.dynamic_template_data = { ...parsedMessage, program1, program2 }
        const from = {
            name: "iNSPIRE",
            email: "athleteservices@inspiresportonline.com"
        }

        message.from = from

        return await sgMail.send({
            ...message,
            // subject: unescape(message.subject),
            // text: unescape(message.text),
            // title: unescape(message.title),
            // html: unescape(message.html),
            // personalizations: [{
            //     to: {
            //         name: unescape(message.personalizations[0].to[0].name),
            //         email: unescape(message.personalizations[0].to[0].email)
            //     }
            // }]
            // ,
            // from: {
            //     name: unescape(message.from.name),
            //     email: unescape(message.from.email)
            // }
        }, (resCB) => {
            if (resCB) {
                console.log('resCB', resCB)
            }
        })
            .then((response) => {
                // POST succeeded...
                // console.log('parsedBody', parsedBody)
                console.log('sendEmail statusCode', response[0].statusCode)
                return res.status(response[0].statusCode || 202).json({
                    message: 'sendEmail',
                    responseCode: response[0].statusCode || 202,
                    data: {
                        response,
                    },
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            })
            .catch((err) => {
                // POST failed...
                console.log('err', err)
                return res.status(500).json({
                    message: 'sendEmail',
                    responseCode: 500,
                    data: {},
                    error: err,
                    method: {
                        name: 'sendEmail',
                        line: 58,
                    },
                })
            });


    }

    addEmailTemplateData = async () => {





        // const { message } = <{ message: EmailMessage }>req.body

        // const config = functions.config()



        // const { id: key } = req.params

        // const message: EmailMessage = {
        //     personalizations: [
        //         {
        //             to: [
        //                 {
        //                     name: "Ian Kaapisoft",
        //                     email: "ian.gouws@kaapisoft.com"
        //                 },
        //                 {
        //                     name: "Ian iNSPIRE",
        //                     email: "ian@inspiresportonline.com"
        //                 }
        //             ]
        //         }
        //     ],
        //     from: {
        //         name: "iNSPIRE Dev",
        //         email: "athleteservices@inspiresportonline.com"
        //     },
        //     templateId: "d-93756f51e55b4734a5b93d23e4310750",
        //     emailTemplate: "badMood",
        //     dynamic_template_data: {

        //         name: "Ian Gouws!!",


        //         barType: {
        //             danger: true
        //         }
        //     }
        // }

        // return sendEmail(message, key)
        const dbTemplateData = { "uid": "badMood", "programsSubHeading": "We have a wide range of wellness type programs that may be useful", "message": "<b>{{{name}}}</b> logged 3 consequitive <b>\"very sad\"</b> or <b>\"sad\"</b> mood entries and may need your help", "programsHeading": "Perhaps these programs can help???", "showDownloadButton": false, "heading": "An urgent message about  <br>{{{name}}}", "barType": { "danger": true }, "isActive": true, "lifestyleEntryType": { "body": false, "training": false, "food": false, "mood": true }, "title": "Mood Entries", "programs": [{ "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png", "title": "Some Program", "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png", "description": "Some details here." }, { "description": "Other details here.", "link": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png", "title": "Other Program", "image": "https://marketing-image-production.s3.amazonaws.com/uploads/cbfde94933b8907c1e2f1765f12bd04d65b87e01c764ff3bbffd93347577347bb5ddf77ab751c1d4149e3ae111b4240ef0afc6f08d5476cd2721fb333b4314b3.png" }], "subject": "An urgent message about {{{name}}}" }
        await db.collection(FirestoreCollection.EmailTemplates)
            .doc('sleepDeprivation')
            .set({
                ...dbTemplateData,
                message: '<b>{{{name}}}</b> is suffering from Sleep Deprivation',
                uid: 'sleepDeprivation'
            })
        await db.collection(FirestoreCollection.EmailTemplates)
            .doc('extremeFatigue')
            .set({
                ...dbTemplateData,
                message: '<b>{{{name}}}</b> is suffering from Extreme Fatigue',
                uid: 'extremeFatigue'
            })
        await db.collection(FirestoreCollection.EmailTemplates)
            .doc('moderatePainOrAbove')
            .set({
                ...dbTemplateData,
                message: '<b>{{{name}}}</b> is suffering from Moderate Pain Or Above',
                uid: 'moderatePainOrAbove'
            })
        await db.collection(FirestoreCollection.EmailTemplates)
            .doc('multipleDailyTraining')
            .set({
                ...dbTemplateData,
                message: '<b>{{{name}}}</b> is doing Multiple Daily Training',
                uid: 'multipleDailyTraining'
            })
        // await db.collection(FirestoreCollection.EmailTemplates)
        //     .doc('badMood')
        //     .set(dbTemplateData)

    }
}
