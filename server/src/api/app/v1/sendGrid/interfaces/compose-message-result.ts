import { EmailMessage } from "../conrollers/email";

export interface ComposeMessageResult {
    message: EmailMessage;
}

export interface ToList {
    name: string;
    email: string;
}