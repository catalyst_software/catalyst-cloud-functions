import { Program, NotificationSeverity, OnboardingInviteEmailPayload } from '../conrollers/email';

interface EmailTemplateOveride {
    isActive?: boolean;
    name?: string;
    subject?: string;
    message?: string;
    title?: string;
    numberOfAthletes?: number;
    dayCountNonEngagement?: number;
    programsHeading?: string;
    programsSubHeading?: string;
    programs?: Array<Program>;
    program1?: Program;
    program2?: Program;
    showDownloadButton?: boolean;
    barType?: {
        danger?: boolean;
        warn?: boolean;
        success?: boolean;
    };


    userOnboardingCode?: string
    groupIDs?: string[];
    userFullName?: string;
    userFirstName?: string;
    analyticsFamily?: string;
    userID?: string;
    value?: number;
    isOnboarding?: boolean,

    taskID?: string;
    organizationID?: string;
    organizationName?: string;
    groupID?: string;
    groupName?: string;
    athleteID?: string;
    actionUrl?: string;
    token?: string;
    senderFullName?: string;
    redFlagType?: string;

    sendPushNotification?: boolean;
}

export interface EmailMessageTemplate {
    to?: {
        name: string;
        email: string;
    }[];
    from?: {
        name: string;
        email: string;
    };
    emailTemplateData?: EmailTemplateOveride;
    templateId: string;
    emailTemplateName: string;
    notificationTemplateType?: number;
    notificationTemplate?: string;
    includePushNotification?: boolean;
    notificationIcon?: string;
    notificationPath?: string;
    notificationSeverity?: NotificationSeverity;
}
