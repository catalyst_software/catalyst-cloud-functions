import { Program, NotificationSeverity } from '../conrollers/email';
import { IpScoreTracking } from '../../../../../db/ipscore/models/documents/tracking/ip-score-tracking.interface';
export interface IPScoreNonEngagementTemplate {
    to: {
        name: string;
        email: string;
    }[];
    from?: {
        name: string;
        email: string;
    };
    emailTemplateData: {
        isActive?: boolean;
        name: string;
        athletes: Array<{
            uid: string;
            fullName: string;
            email: string;
            currentIpScoreTracking?: IpScoreTracking;
        }>;
        numberOfAthletes: number;
        dayCountNonEngagement: number;
        subject?: string;
        message?: string;
        title?: string;
        programsHeading?: string;
        programsSubHeading?: string;
        programs?: Array<Program>;
        program1?: Program;
        program2?: Program;
        showDownloadButton?: boolean;
        barType: {
            danger?: boolean;
            warn?: boolean;
            success?: boolean;
        };
    };
    templateId: string;
    emailTemplateName: string;
    notificationTemplateType?: number;
    includePushNotification?: boolean;
    notificationPath?: string;
    notificationSeverity: NotificationSeverity;
}


export interface ReportMessagingTemplate {
    to: {
        name: string;
        email: string;
    }[];
    from?: {
        name: string;
        email: string;
    };
    emailTemplateData: {
        isActive?: boolean;
        name: string;
        // athletes: Array<{
        //     uid: string;
        //     fullName: string;
        //     email: string;
        //     currentIpScoreTracking?: IpScoreTracking;
        // }>;
        // numberOfAthletes: number;
        // dayCountNonEngagement: number;
        subject?: string;
        message?: string;
        title?: string;
        programsHeading?: string;
        programsSubHeading?: string;
        programs?: Array<Program>;
        program1?: Program;
        program2?: Program;
        showDownloadButton?: boolean;
        barType: {
            danger?: boolean;
            warn?: boolean;
            success?: boolean;
        };
    };
    templateId: string;
    emailTemplateName: string;
    notificationTemplateType?: number;
    includePushNotification?: boolean;
    notificationPath?: string;
    notificationSeverity: NotificationSeverity;
}
