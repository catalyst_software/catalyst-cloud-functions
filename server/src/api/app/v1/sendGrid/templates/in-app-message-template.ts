import { AthleteIndicator } from '../../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { PushNotificationType } from '../../../../../models/enums/push-notification-type';
import { NotificationCardType } from '../../../../../models/enums/UpdateNotificationCardMetadataDirective';
import { PushNotificationsType } from '../../../../../models/enums/enums.model';

// export interface InAppMessageTemplate {
//     templateId: string;
//     emailTemplateName: string;
//     notificationTemplateType?: number;
//     includePushNotification?: boolean;
//     notificationPath?: string;
//     notificationSeverity: NotificationSeverity;
// }



export interface InAppMessageTemplate {
    senderName: string;
    recipients: Array<AthleteIndicator>;
    cardType: NotificationCardType;
    cardTitle: string,
    pushNotificationType: PushNotificationType;
    notificationPath: string;
    icon: PushNotificationsType;
    sendSMS: boolean;
    useTopic?: boolean
    topicIds?: string[];
}
// export interface NotificationMessagePayload {
//     userID: string;
//     userFirstName: string;
//     userFullName: string;
//     messageTemplate: InAppMessageTemplate;
// }
