// import * as firebaseHelper from 'firebase-functions-helper';

// var Firebase = require('firebase');
import express from 'express'
import cors from 'cors'
// import * as bodyParser from 'body-parser'
// Import the Firebase SDK for Google Cloud Functions.
import * as functions from 'firebase-functions'

import setRoutes from './routes'

/**
 * System wide logger
 */
// const logger = initialiseLoggers('info', 'warn', 'logger-service')

/**
 * Our express app
 */
// Create HTTP Server
const app = express()

//
// START: Inject Middleware.  Middleware intercepts the request and the response
//

/**
 * CORS is a node.js package for providing a Connect/Express middleware that can be used
 * to enable CORS with various options.
 */

// app.options('/products/:id', cors()) // enable pre-flight request for DELETE request
// app.del('/products/:id', cors(), function (req, res, next) {
//     res.json({ msg: 'This is CORS-enabled for all origins!' })
// })

const corsOptions = {
    origin: true,
    // origin: 'http://example.com',
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions))

app.use(function(req, res, next) {
    // Set CORS headers

    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888')

    res.header('Access-Control-Allow-Origin', '*')
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )

    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Request-Method', '*')
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET')
    res.setHeader('Access-Control-Allow-Headers', '*')
    // res.setHeader('Access-Control-Allow-Headers', req.headers.origin);

    // res.setHeader('Access-Control-Allow-Headers', req.headers.origin);
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )
    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if (req.method === 'OPTIONS') {
        res.writeHead(200)
        res.end()
        return
    }
    // res.header("Access-Control-Allow-Origin", "*");
    next()
})
// const server = require('http').createServer((req, res, next) => {
//     // Set CORS headers
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Request-Method', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
//     // res.setHeader('Access-Control-Allow-Headers', '*');
//     res.setHeader('Access-Control-Allow-Headers', req.headers.origin);
//     if (req.method === 'OPTIONS') {
//         res.writeHead(200);
//         res.end();
//         return;
//     }
//     //     next();

// });

/**
 * bodyParser Parse incoming request bodies in a middleware before your handlers,
 * available under the req.body property.
 */
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

/**
 * Set API Routes!!!
 */
setRoutes(app)

// if (isProduction()) {
//     console.log('app process.env', JSON.stringify(process.env))
// }
// console.log('production?', isProduction())
//
// END: Inject Middleware.
//


    // const server = require('http').createServer(app)

    // // Attach Socket.io server
    // const io = require('socket.io')(server)
    // // Indicate port 3000 as host
    // const port =
    // //  process.env.PORT || 
    // 3001

    // Create a new firebase reference
    // const firebaseRef = new Firebase(
    //     `https://inspire-219716.firebaseio.com/athleteBiometricWeeklyAggregates/FKORArPPLiRSjACLvqWQIaZWFJzo2_0_0/`
    // );




/**
 * Inspire HttpsFunction!
 * @returns functions.HttpsFunction
 */
export const sendgridEmail: functions.HttpsFunction = functions.https.onRequest(app)
