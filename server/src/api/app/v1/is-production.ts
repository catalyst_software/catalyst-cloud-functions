import { ExistingGoogleProjects, Config } from './../../../shared/init/config/config';

export const isProduction = (): boolean => {
    return Config.project === ExistingGoogleProjects.Production;
};

export const isDebugging = (): boolean => {
    return Config.isDebugging;
};

export const isProductionProject = (): boolean => {
    return Config.project === ExistingGoogleProjects.Production;
};

export const loadSecondaryProject = (): boolean => {
    return Config.loadSecondaryProject;
}

export const getSecondaryProject = (): ExistingGoogleProjects => {
    return Config.secondaryProject;
}

export const isDevProject = (): boolean => {
    return Config.project === ExistingGoogleProjects.Production;
};

export const isHCPProject = (): boolean => {
    return Config.project === ExistingGoogleProjects.Production;
}

export const isRunningLocally = (): boolean => {
    return process.env.GCP_PROJECT === undefined;
};
