import { ObjectCacheItem } from "../callables/run-group-ip-score-aggregation";

export interface WhiteLabelTheme extends ObjectCacheItem {
    uid: WhiteLabelThemeType;
    colors?: WhiteLabelColors;
    typography?: WhiteLabelTypography;
    iconSizing?: WhiteLabelIconSizing;
}

export interface WhiteLabelColors {
    themeBG: string;
    navigationBG: string;
    cardBG: string;
    neutralBG: string;
    iconsTextLinework: string;
    themeActive: string;
    themeAccent: string;
    alertGreen: string;
    alertAmber: string;
    alertRed: string;
}

export interface WhiteLabelTypography {
    headings: WhiteLabelPlatform;
    body: WhiteLabelPlatform;
}

export interface WhiteLabelIconSizing {
    uiNormal: string;
    uiMedium: string;
    uiLarge: string;
    uiOversize: string;
}

export interface WhiteLabelPlatform {
    ios: string;
    android: string;
}

// export enum WhiteLabelThemeType {
//     Default = 'default',
//     Blue = 'blue',
//     Red = 'red'
// }

export type WhiteLabelThemeType = 'default' | 'catalyst';
