import { IpScoreConfiguration } from "../../../../db/ipscore/models/documents/tracking/ip-score-configuration.interface";
import { NonEngagementConfiguration } from "../../../../models/organization.model";
import { OnboardingConfiguration } from "../../../../models/onboarding-configuration";

export interface ConsumerModelConfiguration {
    ipScoreConfiguration: IpScoreConfiguration;
    nonEngagementConfiguration?: NonEngagementConfiguration;
    onboardingConfiguration?: OnboardingConfiguration;
}
