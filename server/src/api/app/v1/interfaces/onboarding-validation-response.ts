import { firestore } from 'firebase-admin';

import { Athlete, OrganizationIndicator } from '../../../../models/athlete/interfaces/athlete';
import { SubscriptionType } from '../../../../models/enums/enums.model';
import { IpScoreConfiguration } from "../../../../db/ipscore/models/documents/tracking/ip-score-configuration.interface";

import { GroupIndicator } from "../../../../db/biometricEntries/well/interfaces/group-indicator";

import { AthleteIndicator } from '../../../../db/biometricEntries/well/interfaces/athlete-indicator';
import { WhiteLabelTheme } from './WhiteLabelTheme';
import { OrganizationTool } from '../../../../models/onboarding-configuration';



export interface OnboardingValidationResponseData {
    athlete: Athlete;
    athleteIndicator?: AthleteIndicator;
    organizationId: string;
    orgTools: OrganizationTool[];
    organizations: Array<OrganizationIndicator>;
    isCoach: boolean;
    isOrgRep: boolean;
    ipScoreConfiguration: IpScoreConfiguration;
    groups: Array<GroupIndicator>;
    subscription: OnboardingSubscription;
    includeGroupAggregation: boolean;    
    contentProviderId?: string;
    theme?: WhiteLabelTheme;

  
    storageRoot?: string;

    validationCodeType?: ValidationCodeType;
    contentProvisionKey?: string;
}

export interface OnboardingSubscription {
    isPrePaid: boolean;
    subscriptionType?: SubscriptionType;
    
    commencementDate?: firestore.Timestamp;
    expirationDate?: firestore.Timestamp;
}

export interface OnboardingValidationResponse {
    message: string;
    data: OnboardingValidationResponseData;
    responseCode?: number;
    error?: {
        message: string;
        // TODO: fix typing?
        exception?: any;
        stack?: any;
    };
    level?: string;
    method?: {
        name: string;
        line: number;
    };
    res?: any // Response;
}

export enum ValidationCodeType {
    Onboarding = 'onboarding',
    MediaBundle = 'media',
    NotificationStream = 'notification'
}
