import { AthleteIndicator } from "../../../../db/biometricEntries/well/interfaces/athlete-indicator";
import { AthleteProfile } from "../../../../models/athlete/interfaces/athlete-profile";

export interface OnboardingValidationRequest {
    organizationCode: string;
    athlete: AthleteIndicator;
    profile: AthleteProfile;
    
    firebaseMessagingId?: string;
}