import { Athlete } from '../../models/athlete/interfaces/athlete';

export const getUserFirstName = (athlete: Athlete): string => {
    return athlete.profile.firstName;
};

export const getUserLastName = (athlete: Athlete): string => {
    return athlete.profile.lastName;
};

export const getUserFullName = (athlete: Athlete): string => {
    return `${getUserFirstName(athlete)} ${getUserLastName(athlete)}`;
};

export const getUserEmail = (athlete: Athlete): string => {
    return athlete.profile.email || athlete.email || 'unknown';
};
